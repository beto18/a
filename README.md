# Ajustador Móvil 2.0

Aplicación de GNP para la atención de servicios de Ajuste asignados a los Ajustadores, tanto empleados de GNP, como externos (despacho).
La aplicación cuenta con los siguientes módulos:

* Inicio de Sesión
* Bandeja de entrada o charola donde se visualizan los Siniestros
* Historial de siniestros atendidos
* Consulta info. de la Póliza
* Consulta info. del Siniestro
* Módulo de Asegurado
* Módulo de Tercero-Auto
* Módulo de Tercero-No Auto
* Módulo de Tercero-Peatón
* Modulo de Declaración
* Módulo de Recuperaciones
* Módulo de Responsabilidad
* Módulo de Servicios de Cobertura



## Ambiente de Desarrollo
```
Android Studio: 3.0.1
Gradle: 4.6
Versión mínima SDK: 21 
Versión SDK: 26
```



## Celular de los Ajustadores

![SM-J730GM](https://drive.google.com/file/d/0BxnDXYaYxJE3RmR5TFJNY3dFUldJQzQtREpQWXd1Ri1UQWVj/view)



## Pasos para el mantenimiento del código productivo
```
1. Crear un clon del proyecto (` git clone http://<<usuario>>@git.gnp.mx/moviles/MVL_ANDROID_AJM.git`)
2. Crear una nueva rama para la modificación (`git checkout -b feature/fooBar`)
3. Versionar archivos modificados (git add . | -A)
4. Confirmar cambios (`git commit -am 'Add some fooBar'`)
5. Subir a la nueva rama (`git push origin feature/fooBar`)
6. Crear un *Pull Request* para que se una al master
```



## Configuración de los ambientes de desarrollo (QA, UAT y PRODUCCIÓN)

El proyecto utliza las *Variantes de Construcción* (Build Variants) de Android Studio
para cambiar el ambiente al que se apuntará la app. Se debe seleccionar la variante de construcción
desde Android Studio, antes de compilar y generar el APK.

Si se necesita modificar algún end-points hay que modificar el archivo *variants.gradle*.

La configuración del Firebase se encuentra en los archivos *google-services.jason*, hay uno por ambiente.



## Documentación 

* [JavaDoc] (https://git.gnp.com.mx/moviles/MVL_ANDROID_AJM/tree/QA/JavaDoc)

* [LucidChart] (https://www.lucidchart.com/documents/view/6dbaa609-fa60-4924-9aea-eb91a2861f3b)

* [Drive] (https://drive.google.com/drive/u/1/folders/1I9rZPeQ76qntu7LhXPu-JaQqJC2Lzizi)



## Autores

Lablet ext: 81292 y 80874

