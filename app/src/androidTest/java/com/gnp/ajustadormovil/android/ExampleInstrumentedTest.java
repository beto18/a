package com.gnp.ajustadormovil.android;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.EstatusAjuste;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Test
    public void useAppContext() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();

        Log.i("TEST", "1: " + UUID.fromString("ABCDEFGHIJKLMN").toString());
        Log.i("TEST", "2: " + UUID.fromString("ABCDEFGHIJKLMN").toString());

        Log.i("TEST", "1: " + UUID.fromString("ABCDEFGHIJKLM").toString());
        Log.i("TEST", "2: " + UUID.fromString("ABCDEFGHIJKLM").toString());

        assertEquals("com.ajustadormovil.gnp.android", appContext.getPackageName());
    }



    @Test
    public void pruebas(){
        AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();
        Ajuste ajuste = ajusteRepository.findById("289460");
        ajuste.setEstatus(EstatusAjuste.SINCRONIZANDO);
        ajusteRepository.save(ajuste);
    }


    @Test
    public void prueba(){
        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));
        Log.i("TEST", nf.format(100000L));
        nf = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-ES"));
        Log.i("TEST", nf.format(100000L));
        nf = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-US"));
        Log.i("TEST", nf.format(100000L));
    }

    @Test
    public void timeZoneTest() {
        TimeZone tz = TimeZone.getTimeZone("America/Mexico_City");
        Log.i("TEST", "TimeZone: " + tz);
        Log.i("TEST", ": " + tz.getOffset(System.currentTimeMillis()));
        Log.i("TEST", ": " + tz.getDSTSavings());
        Log.i("TEST", ": " + tz.getOffset(System.currentTimeMillis()) / (60 * 60 * 1000) );
    }

    @Test
    public void pruebaFirmaDigital(){
        String folio = "PROPRO10123051911223489B";
        String siniestroResponsable = "345678";
        String siniestroAfectado = "246346";
        String compañiaResponsable = "HDI";
        String compañiaAfectadad = "MULTIUSOS";
        String fechaAtencion = "23/05/2019";
        String polizaResponsable = "98765433";
        String polizaAfectado = "98765435";
        String digitalResponsable = "XXXXX";
        String digitalAfectado = "xxxxaf";
        List<String> folioPaseMedico = Arrays.asList("PROPRO101246346728","PROPRO101246346738", "PROPRO101246346748");
        StringBuilder valeDigital = new StringBuilder();
        valeDigital.append(folio)
                .append("|")
                .append(siniestroResponsable)
                .append("|")
                .append(siniestroAfectado)
                .append("|")
                .append(compañiaResponsable)
                .append("|")
                .append(compañiaAfectadad)
                .append("|")
                .append(fechaAtencion)
                .append("|")
                .append(polizaResponsable)
                .append("|")
                .append(polizaAfectado)
                .append("|")
                .append(digitalResponsable)
                .append("|")
                .append(digitalAfectado)
                .append("|")
                .append(folioPaseMedico);

        Log.i("Firma digital ", valeDigital.toString());
    }


    @Test
    public void desencriptarinfo(){


    }

}
