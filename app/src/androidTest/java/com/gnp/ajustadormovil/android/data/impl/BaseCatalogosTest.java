package com.gnp.ajustadormovil.android.data.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.interactors.catalogos.SyncInteractor;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.function.Supplier;

import io.reactivex.Single;

import static org.junit.Assert.fail;

public class BaseCatalogosTest {

    private String TAG = this.getClass().getSimpleName();

    protected void init(SyncInteractor syncInteractor){
        CompletableFuture future = new CompletableFuture();
        syncInteractor.synchronizeData()
                .subscribe(() -> future.complete(null),
                        throwable -> {
                            Log.e(TAG, "Error al cargar catálogos", throwable);
                            future.completeExceptionally(throwable);
                        });
        future.join();
    }

    protected <T> void testList(Supplier<Single<List<T>>> method, Function<T, String> print){
        CompletableFuture future = new CompletableFuture();
        method.get()
                .subscribe(list -> {
                    for (T element : list) {
                        Log.i(TAG, print.apply(element));
                    }
                    future.complete(null);
                }, throwable -> {
                    Log.e(TAG, "Error al cargar el catálogos", throwable);
                    fail(throwable.getMessage());
                    future.completeExceptionally(throwable);
                });
        future.join();
    }

    protected <T> void testList(Supplier<Single<List<T>>> method){
        testList(method, obj -> obj.toString());
    }

    protected <T> void testObject(Supplier<Single<T>> method, Function<T, String> print){
        CompletableFuture future = new CompletableFuture();
        method.get()
                .subscribe(element -> {
                        Log.i(TAG, print.apply(element));
                    future.complete(null);
                }, throwable -> {
                    fail(throwable.getMessage());
                    future.completeExceptionally(throwable);
                });
        future.join();
    }

    protected <T> void testObject(Supplier<Single<T>> method){
        testObject(method, obj -> obj.toString());
    }

}
