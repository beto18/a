package com.gnp.ajustadormovil.android.data.impl;


import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gnp.ajustadormovil.android.data.catalogos.CatData;
import com.gnp.ajustadormovil.android.data.catalogos.impl.CatDataSQLiteImpl;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class CatDataSQLiteImplTest extends BaseCatalogosTest {

    private CatData data;

    @Before
    public void before(){
        Context ctx = InstrumentationRegistry.getTargetContext();
        data = new CatDataSQLiteImpl(ctx);
        init(CatalogoInteractorsFactory.getCatInteractor());
    }

    @Test
    public void getEntidadesFinancierasTest(){
        testList(() -> data.getEntidadesFinancieras());
    }

    @Test
    public void getMonedasTest(){
        testList(() -> data.getMonedas());
    }

    @Test
    public void getEstatusPolizaTest(){
        testList(() -> data.getEstatusPoliza());
    }

    @Test
    public void getCategoriasTest(){
        testList(() -> data.getCategoriasVehiculo());
    }

    @Test
    public void getViasTest(){
        testList(() -> data.getVias());
    }

    @Test
    public void getOtrasAseguradorasTest(){
        testList(() -> data.getOtrasAseguradoras());
    }

    @Test
    public void getEstadosTest(){
        testList(() -> data.getEstados());
    }

    @Test
    public void getMunicipiosTest(){
        testList(() -> data.getMunicipios("09"));
    }

    @Test
    public void getColoniasTest(){
        testList(() -> data.getColonias("09", "003"));
    }

    @Test
    public void getColoniaTest(){
        testObject(() -> data.getColonia("09", "003", "0583"));
    }

}
