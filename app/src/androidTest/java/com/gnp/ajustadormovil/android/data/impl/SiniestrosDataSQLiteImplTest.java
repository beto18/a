package com.gnp.ajustadormovil.android.data.impl;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gnp.ajustadormovil.android.data.catalogos.SiniestrosData;
import com.gnp.ajustadormovil.android.data.catalogos.impl.SiniestrosDataSQLiteImpl;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class SiniestrosDataSQLiteImplTest extends BaseCatalogosTest {

    private SiniestrosData data;

    @Before
    public void before(){
        Context ctx = InstrumentationRegistry.getTargetContext();
        data = new SiniestrosDataSQLiteImpl(ctx);
        init(CatalogoInteractorsFactory.getSiniestrosInteractor());
    }


    @Test
    public void getTiposAbogadoTest(){
        testList(() -> data.getTiposAbogado());
    }

    @Test
    public void getGestionesAbogadoTest(){
        testList(() -> data.getGestionesAbogado());
    }

    @Test
    public void getMotivosCancelacionAjusteTest(){
        testList(() -> data.getMotivosCancelacionAjuste());
    }

    @Test
    public void getMotivosRechazoAjusteTest(){
        testList(() -> data.getMotivosRechazoAjuste());
    }

    @Test
    public void getMotivosCierreAjusteTest(){
        testList(() -> data.getMotivosCierreAjuste());
    }

    @Test
    public void getTiposAmbulanciaTest(){
        testList(() -> data.getTiposAmbulancia());
    }

    @Test
    public void getCentrosRemateTest(){
        testList(() -> data.getCentrosRemate());
    }

    @Test
    public void getContratistasTest(){
        testList(() -> data.getContratistas());
    }

    @Test
    public void getDocumentosPropiedadTest(){
        testList(() -> data.getDocumentosPropiedad());
    }

    @Test
    public void getTiposIdentificacionTest(){
        testList(() -> data.getTiposIdentificacion());
    }

    @Test
    public void getTiposGruaTest(){
        testList(() -> data.getTiposGrua());
    }

    @Test
    public void getHospitalTest(){
        testObject(() -> data.getHospital(100039L));
    }

    @Test
    public void getHospitalesTest(){
        testList(() -> data.getHospitales("1"));
    }

    @Test
    public void getMotivosInvestigacionTest(){
        testList(() -> data.getMotivosInvestigacion());
    }

    @Test
    public void getNivelLesionTest(){
        testList(() -> data.getNivelLesion());
    }

    @Test
    public void getAreasLesionadasTest(){
        testList(() -> data.getAreasLesionadas());
    }

    @Test
    public void getPartesLesionadasTest(){
        testList(() -> data.getPartesLesionadas(1L));
    }

    @Test
    public void getNacionalidadesTest(){
        testList(() -> data.getNacionalidades());
    }

    @Test
    public void getRazonesNoDeducibleTest(){
        testList(() -> data.getRazonesNoDeducible());
    }

    @Test
    public void getPosicionesOcupanteTest(){
        testList(() -> data.getPosicionesOcupante());
    }

    @Test
    public void getTiposPatrullaTest(){
        testList(() -> data.getTiposPatrulla());
    }

    @Test
    public void getTiposPolizaTest(){
        testList(() -> data.getTiposPoliza());
    }

    @Test
    public void getVolantesRecuperacionTest(){
        testList(() -> data.getVolantesRecuperacion());
    }

    @Test
    public void getTiposRecuperacionMonetariaTest(){
        testList(() -> data.getTiposRecuperacionMonetaria());
    }

    @Test
    public void getTiposRecuperacionMonetariaTarjetaTest(){
        testList(() -> data.getTiposRecuperacionMonetariaTarjeta());
    }

    @Test
    public void getTiposServicioTest(){
        testList(() -> data.getTiposServicio());
    }

    @Test
    public void getTiposSiniestroTest(){
        testList(() -> data.getTiposSiniestro());
    }

    @Test
    public void getTiposTelefonoTest(){
        testList(() -> data.getTiposTelefono());
    }

    @Test
    public void getTiposTerceroTest(){
        testList(() -> data.getTiposTercero());
    }

    @Test
    public void getTiposTerceroAutoTest(){
        testList(() -> data.getTiposTerceroAuto());
    }

    @Test
    public void getCategoriaTerceroNoAutoTest(){
        testList(() -> data.getCategoriasTerceroNoAuto());
    }

    @Test
    public void getTiposTerceroNoAutoTest(){
        testList(() -> data.getTiposTerceroNoAuto(1L));
    }

    @Test
    public void getVolantesTerceroNoAutoTest(){
        testList(() -> data.getVolantesTerceroNoAuto());
    }

    @Test
    public void getTiposUbicacionTest(){
        testList(() -> data.getTiposUbicacion());
    }

    @Test
    public void getAreasAfectadasTest(){
        testList(() -> data.getAreasAfectadas());
    }

    @Test
    public void getPartesAfectadasTest(){
        testList(() -> data.getPartesAfectadas(1L));
    }

    @Test
    public void getImpactosVehiculoTest(){
        testList(() -> data.getImpactosVehiculo());
    }

    @Test
    public void getTiposVolanteTest(){
        testList(() -> data.getTiposVolante());
    }

    @Test
    public void getCoberturasVolanteTest(){
        testList(() -> data.getCoberturasVolante(3, "DM"));
    }

}
