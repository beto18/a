package com.gnp.ajustadormovil.android.data.impl;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gnp.ajustadormovil.android.data.catalogos.TprData;
import com.gnp.ajustadormovil.android.data.catalogos.impl.TprDataSQLiteImpl;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 */
@RunWith(AndroidJUnit4.class)
public class TprDataSQLiteImplTest extends BaseCatalogosTest {

    private TprData data;

    @Before
    public void before(){
        Context ctx = InstrumentationRegistry.getTargetContext();
        data = new TprDataSQLiteImpl(ctx);
        init(CatalogoInteractorsFactory.getTprService());
    }

    @Test
    public void getCoberturasTest(){
        testList(() -> data.getCoberturas());
    }

    @Test
    public void getTiposVehiculoTest(){
        testList(() -> data.getTiposVehiculo());
    }

    @Test
    public void getArmadorasTest(){
        testList(() -> data.getArmadoras("AUT"));
    }

    @Test
    public void getCarroceriasTest(){
        testList(() -> data.getCarrocerias("AUT", "FR"));
    }

    @Test
    public void getModelosTest(){
        testList(() -> data.getModelos("AUT", "FR", "35"));
    }

    @Test
    public void getVersionesTest(){
        testList(() -> data.getVersiones("AUT", "FR", "35", 2018));
    }

    @Test
    public void getVersionTest(){
        testObject(() -> data.getVersion("AUT", "FR", "35", 2018, "20"));
    }

}
