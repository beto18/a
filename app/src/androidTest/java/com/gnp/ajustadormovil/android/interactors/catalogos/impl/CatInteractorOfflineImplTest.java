package com.gnp.ajustadormovil.android.interactors.catalogos.impl;


import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junit.framework.Assert;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.reactivex.disposables.CompositeDisposable;

@RunWith(AndroidJUnit4.class)
public class CatInteractorOfflineImplTest {

    private CatInteractor catInteractor;
    private CompositeDisposable compositeDisposable;
    private String TAG = this.getClass().getSimpleName();
    private static final Gson PARSER;

    static {
        PARSER = new GsonBuilder().setPrettyPrinting().create();
    }

    @Before
    public void before() {
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        compositeDisposable = new CompositeDisposable();
    }


    @After
    public void after() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }


    private void printData(List<?> list) {
        Log.i(TAG, "validateCatalogoIdData: \n" + PARSER.toJson(list));
    }

    private void printData(Object object){
        Log.i(TAG, "validateCatalogoIdData: \n" + PARSER.toJson(object));
    }

    @Test
    public void getEntidadesFinancierasTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getEntidadesFinancieras()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 111);
        printData(future.get());
    }

    @Test
    public void getMonedasTest() throws Exception {
        CompletableFuture<List<CatalogoClaveInfo>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getMonedas()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClaveInfo> result = future.get();
        Assert.assertEquals(result.size(), 4);
        printData(future.get());
    }

    @Test
    public void getEstatusPolizaTest() throws Exception {
        CompletableFuture<List<CatalogoClaveInfo>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getEstatusPoliza()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClaveInfo> result = future.get();
        Assert.assertEquals(result.size(), 24);
        printData(future.get());
    }

    @Test
    public void getCategoriasTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getCategoriasVehiculo()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 16);
        printData(future.get());
    }

    @Test
    public void getViasTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getVias()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 47);
        printData(future.get());
    }

    @Test
    public void getOtrasAseguradorasTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getOtrasAseguradoras()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 76);
        printData(future.get());
    }

    @Test
    public void getEstadosTest() throws Exception {
        CompletableFuture<List<CatalogoClaveInfo>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getEstados()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClaveInfo> result = future.get();
        Assert.assertEquals(result.size(), 32);
        printData(future.get());
    }

    @Test
    public void getMunicipiosTest() throws Exception {
        CompletableFuture<List<Municipio>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getMunicipios("09")
                .subscribe(future::complete, future::completeExceptionally));
        List<Municipio> result = future.get();
        Assert.assertEquals(result.size(), 16);
        printData(future.get());
    }

    @Test
    public void getColoniasTest() throws Exception {
        CompletableFuture<List<ColoniaResumen>> future = new CompletableFuture<>();
        compositeDisposable.add(catInteractor.getColonias("09","003")
                .subscribe(future::complete, future::completeExceptionally));
        List<ColoniaResumen> result = future.get();
        Assert.assertEquals(result.size(), 167);
        printData(future.get());
    }

    @Test
    public void getColoniaTest() throws Exception {
            CompletableFuture<CatalogoClaveInfo> future = new CompletableFuture<>();
            compositeDisposable.add(catInteractor.getColonia("09","003","0583")
                    .subscribe(future::complete, future::completeExceptionally));
            CatalogoClaveInfo result = future.get();
            Assert.assertEquals(result.getClaveInfo(),"00010727");
            Assert.assertEquals(result.getClave(),"0583");
            Assert.assertEquals(result.getNombre(),"Country Club");
            printData(future.get());
    }


}
