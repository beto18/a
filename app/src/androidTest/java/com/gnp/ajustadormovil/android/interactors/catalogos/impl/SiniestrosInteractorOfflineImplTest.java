package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

@RunWith(AndroidJUnit4.class)
public class SiniestrosInteractorOfflineImplTest {

    private SiniestrosInteractor siniestrosInteractor;
    private CompositeDisposable compositeDisposable;
    private String TAG = this.getClass().getSimpleName();
    private static final Gson PARSER;

    static {
        PARSER = new GsonBuilder().setPrettyPrinting().create();
    }

    @Before
    public void before() {
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        compositeDisposable = new CompositeDisposable();
        CompletableFuture future = new CompletableFuture();
        compositeDisposable.add(siniestrosInteractor.synchronizeData()
                .subscribe(() -> future.complete(null), future::completeExceptionally));
        future.join();
    }

    @After
    public void after() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Test
    public void getAreaAfectadaTest() throws Exception{
        CatalogoId result = get(siniestrosInteractor.getAreaAfectada(1L));
        Assert.assertEquals(result.getId(), new Long(1));
        printData(result);
    }

    private void printData(List<?> list) {
        Log.i(TAG, "validateCatalogoIdData: \n" + PARSER.toJson(list));
    }

    private void printData(Object object) {
        Log.i(TAG, "validateCatalogoIdData: \n" + PARSER.toJson(object));
    }

    public <T> T get(Single<T> single) throws Exception {
        CompletableFuture<T> future = new CompletableFuture<>();
        compositeDisposable.add(single
                .subscribe(future::complete, future::completeExceptionally));
        return future.get();
    }


    @Test
    public void getMunicipioTest() throws Exception{
        MunicipioAmis municipioAmis = get(siniestrosInteractor.getMunicipio("09","005"));
        printData(municipioAmis);
    }

    @Test
    public void getTiposAbogadosImplTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposAbogado());
        Assert.assertEquals(result.size(), 2);
        printData(result);
    }

    @Test
    public void getGenerosTest() throws Exception {
        List<CatalogoClaveAmis> result = get(siniestrosInteractor.getGeneros());
        Assert.assertEquals(result.size(), 2);
        printData(result);
    }

    @Test
    public void getGeneroTest() throws Exception {
        CatalogoClaveAmis result = get(siniestrosInteractor.getGenero("M"));
        Assert.assertEquals(result.getClaveAmis(), new Long(1));
        Assert.assertEquals(result.getNombre(), "Masculino");
        printData(result);
    }

    @Test
    public void getGeneroAmisTest() throws Exception {
        CatalogoClaveAmis result = get(siniestrosInteractor.getGeneroAmis("1L"));
        Assert.assertEquals(result.getNombre(), "Masculino");
        Assert.assertEquals(result.getClave(), "M");
        printData(result);
    }

    @Test
    public void getGestionesAbogadosTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getGestionesAbogado());
        Assert.assertEquals(result.size(), 3);
        printData(result);
    }

    @Test
    public void getAreasLesionadasTest() throws Exception {
        List<Area> result = get(siniestrosInteractor.getAreasPartesLesionadas());
        printData(result);
    }

    @Test
    public void getPartesLesionadasByIdTest() throws Exception {
        List<CatalogoIdAmis> result = get(siniestrosInteractor.getPartesLesionadas(2L));
        Assert.assertEquals(result.size(), 7);
        printData(result);
    }

    @Test
    public void getParteLesionadaTest() throws Exception {
        CatalogoIdAmis result = get(siniestrosInteractor.getParteLesionada(10L));
        Assert.assertEquals(result.getClaveAmis(), new Long(9));
        Assert.assertEquals(result.getNombre(), "Varias lesiones en la cabeza - cualquier combinación de lesiones en la cabeza");
        printData(result);
    }

    @Test
    public void getParteLesionadaAmisTest() throws Exception {
        CatalogoIdAmis result = get(siniestrosInteractor.getParteLesionadaAmis("9"));
        Assert.assertEquals(result.getId(), new Long(17));
        Assert.assertEquals(result.getNombre(), "Boca - incluye.- labios. lengua. gargant. sentido del gusto");
        printData(result);

    }

    @Test
    public void getPartesAfectadasTest() throws Exception {
       // List<CatalogoIdAmis> result = get(siniestrosInteractor.getPartesAfectadas(1L));
       //Assert.assertEquals(result.size(), 15);
        //printData(result);
    }

    @Test
    public void getParteAfectadaTest() throws Exception {
        CatalogoIdAmis result = get(siniestrosInteractor.getParteAfectada(7L, 299L));
        Assert.assertEquals(result.getNombre(), "Llantas de Tracción");
        Assert.assertEquals(result.getClaveAmis(), new Long(43));
        printData(result);
    }

    @Test
    public void getParteAfectadaAmisTest() throws Exception {
        CatalogoIdAmis result = get(siniestrosInteractor.getParteAfectadaAmis("43"));
        Assert.assertEquals(result.getNombre(), "Porta Placa Delantero");
        Assert.assertEquals(result.getId(), new Long(5));
        printData(result);
    }

    @Test
    public void getMotivosRechazoAjusteTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getMotivosRechazoAjuste());
        Assert.assertEquals(result.size(), 4);
        printData(result);
    }

    @Test
    public void getMotivosCierreAjusteTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getMotivosCierreAjuste());
        Assert.assertEquals(result.size(), 13);
        printData(result);
    }

    @Test
    public void getTiposAmbulanciaTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposAmbulancia());
        Assert.assertEquals(result.size(), 2);
        printData(result);
    }

    @Test
    public void getCentrosRemateTest() throws Exception {
        List<CentroRemate> result = get(siniestrosInteractor.getCentrosRemate());
        Assert.assertEquals(result.size(), 12);
        printData(result);
    }

    @Test
    public void getContratistasTest() throws Exception {
        List<Contratista> result = get(siniestrosInteractor.getContratistas());
        Assert.assertEquals(result.size(), 82);
        printData(result);
    }

    @Test
    public void getDocumentosPropiedadTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getDocumentosPropiedad());
        Assert.assertEquals(result.size(), 4);
        printData(result);
    }

    @Test
    public void getTiposIdentificacionTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposIdentificacion());
        Assert.assertEquals(result.size(), 5);
        printData(result);
    }

    @Test
    public void getTiposGruaTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposGrua());
        Assert.assertEquals(result.size(), 7);
        printData(result);
    }

    @Test
    public void getHospitalesTest() throws Exception {
        List<HospitalResumen> result = get(siniestrosInteractor.getHospitales("70"));
        Assert.assertEquals(result.size(), 8);
        printData(result);
    }


    @Test
    public void getMotivosInvestigacionTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getMotivosInvestigacion());
        Assert.assertEquals(result.size(), 11);
        printData(result);
    }

    @Test
    public void getNivelLesionTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getNivelLesion());
        Assert.assertEquals(result.size(), 4);
        printData(result);
    }


    @Test
    public void getNacionalidadesTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getNacionalidades());
        Assert.assertEquals(result.size(), 2);
        printData(result);
    }

    @Test
    public void getRazonesNoDeducibleTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getRazonesNoDeducible());
        Assert.assertEquals(result.size(), 8);
        printData(result);
    }

    @Test
    public void getPosicionesOcupanteTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getPosicionesOcupante());
        Assert.assertEquals(result.size(), 6);
        printData(result);
    }

    @Test
    public void getTiposPatrullaTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposPatrulla());
        Assert.assertEquals(result.size(), 3);
        printData(result);
    }

    @Test
    public void getTiposPolizaTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getTiposPoliza());
        Assert.assertEquals(result.size(), 4);
        printData(result);
    }

    @Test
    public void getVolantesRecuperacionTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getVolantesRecuperacion());
        Assert.assertEquals(result.size(), 3);
        printData(result);
    }

    @Test
    public void getTiposRecuperacionMonetariaTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getTiposRecuperacionMonetaria());
        Assert.assertEquals(result.size(), 4);
        printData(result);
    }

    @Test
    public void getTiposRecuperacionMonetariaTarjetaTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposRecuperacionMonetariaTarjeta());
        Assert.assertEquals(result.size(), 2);
        printData(result);
    }

    @Test
    public void getTiposServicioTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposServicio());
        Assert.assertEquals(result.size(), 10);
        printData(result);
    }

    @Test
    public void getTiposSiniestroTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposSiniestro());
        Assert.assertEquals(result.size(), 5);
        printData(result);
    }

    @Test
    public void getTiposTelefonoTest() throws Exception {
        List<CatalogoClaveAmis> result = get(siniestrosInteractor.getTiposTelefono());
        Assert.assertEquals(result.size(), 3);
        printData(result);
    }

    @Test
    public void getTipoTelefonoTest() throws Exception {
        CatalogoClaveAmis result = get(siniestrosInteractor.getTipoTelefono("T"));
        Assert.assertEquals(result.getNombre(), "Trabajo");
        Assert.assertEquals(result.getClaveAmis(), new Long(3));
        printData(result);
    }

    @Test
    public void getTipoTelefonoAmis() throws Exception {
        CatalogoClaveAmis result = get(siniestrosInteractor.getTipoTelefonoAmis("3L"));
        Assert.assertEquals(result.getNombre(), "Trabajo");
        Assert.assertEquals(result.getClave(), "T");
        printData(result);
    }


    @Test
    public void getTiposTerceroTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposTercero());
        Assert.assertEquals(result.size(), 5);
        printData(result);
    }

    @Test
    public void getTiposTerceroAutoTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposTerceroAuto());
        Assert.assertEquals(result.size(), 3);
        printData(result);
    }

    @Test
    public void getTiposLicenciaTest() throws Exception {
        List<CatalogoClaveAmis> result = get(siniestrosInteractor.getTiposLicencia());
        Assert.assertEquals(result.size(), 6);
        printData(result);
    }

    @Test
    public void getTipoLicenciaTest() throws Exception {
        CatalogoClaveAmis result = get(siniestrosInteractor.getTipoLicencia("A"));
        Assert.assertEquals(result.getClaveAmis(), new Long(51));
        Assert.assertEquals(result.getNombre(), "Tipo A");
        printData(result);
    }

    @Test
    public void getTipoLicenciaAmisTest() throws Exception {
        CatalogoClave result = get(siniestrosInteractor.getTipoLicenciaAmis("51L"));
        Assert.assertEquals(result.getClave(), "A");
        Assert.assertEquals(result.getNombre(), "Tipo A");
        printData(result);

    }

    @Test
    public void getCategoriaTerceroNoAutoTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getCategoriaTerceroNoAuto());
        Assert.assertEquals(result.size(), 5);
        printData(result);
    }

    @Test
    public void getTiposTerceroNoAutoTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getTiposTerceroNoAuto(4L));
        Assert.assertEquals(result.size(), 24);
        printData(result);
    }

    @Test
    public void getVolantesTerceroNoAutoTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getVolantesTerceroNoAuto());
        Assert.assertEquals(result.size(), 2);
        printData(result);
    }

    @Test
    public void getTiposUbicacionTest() throws Exception {
        List<CatalogoClaveAmis> result = get(siniestrosInteractor.getTiposUbicacion());
        Assert.assertEquals(result.size(), 4);
        printData(result);
    }

    @Test
    public void getAreasAfectadasTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getAreasAfectadas());
        Assert.assertEquals(result.size(), 8);
        printData(result);
    }

    @Test
    public void getImpactosVehiculoTest() throws Exception {
        List<ImpactoVehiculoAmis> result = get(siniestrosInteractor.getImpactosVehiculo());
        Assert.assertEquals(result.size(), 13);
        printData(result);
    }

    @Test
    public void getImpactoVehiculoTest() throws Exception {
        ImpactoVehiculoAmis result = get(siniestrosInteractor.getImpactoVehiculo(21L));
        Assert.assertEquals(result.getNombre(), "Costado der trasero");
        Assert.assertEquals(result.getAreaAmis(), new Long(37));
        printData(result);
    }

    @Test
    public void getImpactoVehiculoAmisTest() throws Exception {
        ImpactoVehiculoAmis result = get(siniestrosInteractor.getImpactoVehiculoAmis("37L"));
        Assert.assertEquals(result.getNombre(), "Costado der trasero");
        Assert.assertEquals(result.getId(), new Long(21));
        printData(result);
    }

    @Test
    public void getTiposVolanteTest() throws Exception {
        List<CatalogoClave> result = get(siniestrosInteractor.getTiposVolante());
        Assert.assertEquals(result.size(), 3);
        printData(result);
    }

    @Test
    public void getColoresVehiculoTest() throws Exception {
        List<CatalogoId> result = get(siniestrosInteractor.getColoresVehiculo());
        Assert.assertEquals(result.size(), 9);
        printData(result);
    }

    @Test
    public void getColorVehiculoTest() throws Exception {
        CatalogoId result = get(siniestrosInteractor.getColorVehiculo(1L));
        Assert.assertEquals(result.getNombre(), "Azul");
        printData(result);
    }

    @Test
    public void getCoberturasVolanteTest() throws Exception {
        List<Cobertura> result = get(siniestrosInteractor.getCoberturasVolante(3, "DM"));
        Assert.assertEquals(result.size(), 8);
        printData(result);
    }
}
