package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.reactivex.disposables.CompositeDisposable;

@RunWith(AndroidJUnit4.class)
public class TprServiceOfflineImplTest {

    private TprInteractor tprService;
    private CompositeDisposable compositeDisposable;
    private String TAG = this.getClass().getSimpleName();
    private static final Gson PARSER;

    static {
        PARSER = new GsonBuilder().setPrettyPrinting().create();
    }

    @Before
    public void before() {
        tprService = CatalogoInteractorsFactory.getTprService();
        compositeDisposable = new CompositeDisposable();
    }


    @After
    public void after() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    private void printData(List<?> list) {
        Log.i(TAG, "validateCatalogoIdData: \n" + PARSER.toJson(list));
    }

    private void printData(Object object){
        Log.i(TAG, "validateCatalogoIdData: \n" + PARSER.toJson(object));
    }

    @Test
    public void getTiposVehiculoTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(tprService .getTiposVehiculo()
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 17);
        printData(future.get());
    }

    @Test
    public void getArmadorasTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(tprService.getArmadoras("AUT")
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 44);
        printData(future.get());

    }

    @Test
    public void getCarroceriasTest() throws Exception {
        CompletableFuture<List<CatalogoClave>> future = new CompletableFuture<>();
        compositeDisposable.add(tprService.getCarrocerias("AUT","FR")
                .subscribe(future::complete, future::completeExceptionally));
        List<CatalogoClave> result = future.get();
        Assert.assertEquals(result.size(), 39);
        printData(future.get());
    }

    @Test
    public void getModelosTest() throws Exception {
        CompletableFuture<List<Short>> future = new CompletableFuture<>();
        compositeDisposable.add(tprService.getModelos("AUT","FR", "35")
                .subscribe(future::complete, future::completeExceptionally));
        List<Short> result = future.get();
        Assert.assertEquals(result.size(), 15);
        printData(future.get());
    }

    @Test
    public void getVersionesTest() throws Exception {
        CompletableFuture<List<VersionVehiculoResumen>> future = new CompletableFuture<>();
        compositeDisposable.add(tprService.getVersiones("AUT","FR","35",2018)
                .subscribe(future::complete, future::completeExceptionally));
        List<VersionVehiculoResumen> result = future.get();
        Assert.assertEquals(result.size(), 4);
        printData(future.get());
    }

    @Test
    public void getVersionTest() throws Exception {
        CompletableFuture<VersionVehiculo> future = new CompletableFuture<>();
        compositeDisposable.add(tprService.getVersion("AUT", "FR", "35", 2018, "20")
                .subscribe(future::complete, future::completeExceptionally));
        VersionVehiculo result = future.get();
        Assert.assertEquals(result.getVersion(),"20");
        Assert.assertEquals(result.getCategoria(),"08");
        Assert.assertEquals(result.getCarroceria().getClave(),"35");
        printData(future.get());
    }


}
