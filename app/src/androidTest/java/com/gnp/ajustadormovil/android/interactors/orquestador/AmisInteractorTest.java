package com.gnp.ajustadormovil.android.interactors.orquestador;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.interactors.amis.impl.AseguradoModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.CircunstanciaModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.DaniosModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.InvolucradoGeneralModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.NarrativaModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.PaseMedicoModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.PolizaModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.SiniestroModelHandler;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import io.reactivex.disposables.CompositeDisposable;

@RunWith(AndroidJUnit4.class)
public class AmisInteractorTest {

    private SiniestrosInteractor siniestrosInteractor;
    private CompositeDisposable compositeDisposable;
    private String TAG = this.getClass().getSimpleName();
    private CatInteractor catInteractor;
    private final ConnectionProvider connectionProvider = ConnectionProvider.getInstance();
    private final AjusteInteractor ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
    private static final Gson PARSER;
    private String id = "289257";
    private final AmisInteractor amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();

    static {
        PARSER = new GsonBuilder().setPrettyPrinting().create();
    }

    @Before
    public void before() throws Exception{
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        compositeDisposable = new CompositeDisposable();
        CompletableFuture future = new CompletableFuture();
        CompletableFuture future2 = new CompletableFuture();
        compositeDisposable.add(siniestrosInteractor.synchronizeData()
                .subscribe(() -> future.complete(null), future::completeExceptionally));
        compositeDisposable.add(catInteractor.synchronizeData()
                .subscribe(() -> future2.complete(null), future::completeExceptionally));
        future.join();
        future2.join();
        String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzYS1maXJlYmFzZS1qd3RAZ25wLWFqdXN0YWRvcm1vdmlsLXFhLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwic3ViIjoic2EtZmlyZWJhc2Utand0QGducC1hanVzdGFkb3Jtb3ZpbC1xYS5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9pZGVudGl0eXRvb2xraXQuZ29vZ2xlYXBpcy5jb21cL2dvb2dsZS5pZGVudGl0eS5pZGVudGl0eXRvb2xraXQudjEuSWRlbnRpdHlUb29sa2l0IiwiaWF0IjoxNTY4MzI3NTEyLCJleHAiOjE1Njg0MTM5MTIsInByb2plY3RpZCI6ImducC1hanVzdGFkb3Jtb3ZpbC1xYSIsInVpZCI6IkNPQ0hPQSIsImNsYWltcyI6eyJyb2wiOiJPQ0hPQSBDQVNUQU5FREEiLCJuZWdvY2lvc09wZXJhYmxlcyI6IiIsImlkcGFydGljaXBhbnRlIjoiIiwibWFpbCI6ImNhcmxvcy5vY2hvYUBnbnAuY29tLm14IiwiYXBlbWF0ZXJubyI6IkNBU1RBTkVEQSIsImdpdmVubmFtZSI6IkNBUkxPUyIsImFwZXBhdGVybm8iOiJPQ0hPQSIsImN1ZW50YWJsb3F1ZWFkYSI6ZmFsc2UsInRpcG91c3VhcmlvIjoiYWp1c3RhZG9yZXMiLCJjZWR1bGEiOiJDNTA2NSIsInJvbGVzIjpbIkFqdXN0YWRvciJdfX0.uhgg7EvYbxlg8ebDUQoCdTtZ8yi0G4D6n-yfhY0YD6lbCE9NbcwVptIJlyS6GpZoV6uxgnxpS9PYMskUOeaDXkSAU--HJatVlxRvoMxf6QaVhG7-0hF8Qe0mT__38PWMIpCxKt9XE544j6oZ1R0xDYlQx6oQ0V13a4okBoLdPVLGzvweaHtI5flV7IVwsFrN43z_s1PiwoB17b3ESO4DtVLyuiOGQkMxmZqJ5lKbQh-TpV6t5fUxdW_luSeOlpzR93FyYE3HLr8j5W5Pos1y5-3vJWUeq4gE_O7KqYdLxOG3Do21Hj719urMsvgDgqHurc3hCI2WzUdal2WmRhVwEA";
        String refreshToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJmaXJlYmFzZS1qd3RAZ25wLWF1dGguaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJzdWIiOiJmaXJlYmFzZS1qd3RAZ25wLWF1dGguaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJhdWQiOiJodHRwczpcL1wvaWRlbnRpdHl0b29sa2l0Lmdvb2dsZWFwaXMuY29tXC9nb29nbGUuaWRlbnRpdHkuaWRlbnRpdHl0b29sa2l0LnYxLklkZW50aXR5VG9vbGtpdCIsImlhdCI6MTU2ODMyNzUxMiwiZXhwIjo0NzIxOTI3NTEyLCJ1aWQiOiJDT0NIT0EiLCJwcm9qZWN0aWQiOiJnbnAtYWp1c3RhZG9ybW92aWwtcWEiLCJtb2JpbGUiOmZhbHNlLCJjbGFpbXMiOnsicm9sIjoiT0NIT0EgQ0FTVEFORURBIiwibmVnb2Npb3NPcGVyYWJsZXMiOiIiLCJpZHBhcnRpY2lwYW50ZSI6IiIsIm1haWwiOiJjYXJsb3Mub2Nob2FAZ25wLmNvbS5teCIsImFwZW1hdGVybm8iOiJDQVNUQU5FREEiLCJnaXZlbm5hbWUiOiJDQVJMT1MiLCJhcGVwYXRlcm5vIjoiT0NIT0EiLCJjdWVudGFibG9xdWVhZGEiOmZhbHNlLCJ0aXBvdXN1YXJpbyI6ImFqdXN0YWRvcmVzIiwiY2VkdWxhIjoiQzUwNjUiLCJyb2xlcyI6WyJBanVzdGFkb3IiXX19.e6e_F2_wc4yDfYPo6AmXy_xOeY_YhEbqXOO-Zb5etgrbF2ouT0q-xw7bCnMHUFfae9T027fVYNmps6S9ubK81TLUqYvqYRsrT0TQ80xmJ9D_vNRFQGvVJzzrZ4bzll3E54twb-Vrr8u9Vv5UeKzgLQ7GZRZtNlSItgnlqflTyV27ZfYXvz3oFKt5TjBkawGV6EseTk_FV75wgBjOEYxSsOTXl-EJ1fmim7X8-kgE3mTN5UYpRolDEePMJkAc_Q6QGyh1-u7qo9LoT1lQ7bLEl_2-RKIKVShbpphLGvdhUNQYKfGRQLYXVAxhWEQgwWbglxByf7Nd0bKqOJu8yFo0XQ";
        AppPreferences.setString(AppPreferences.USER_INFO_TOKEN, jwt);
        AppPreferences.setString(AppPreferences.USER_INFO_REFRESH_TOKEN, refreshToken);
        Log.i(TAG, "Inicio: Inicializar información");
        RXUtils.sync(ajusteInteractor.getAjustes());
        Log.i(TAG, "Fin: Inicializar información");
    }


    @Test
    public void getAmisDataTest(){
        connectionProvider.notConnected(id);
        AseguradoModelHandler aseguradoModelHandler = new AseguradoModelHandler();
        InvolucradoGeneralModelHandler involucradoModelHandler = new InvolucradoGeneralModelHandler();
        PolizaModelHandler polizaModelHandler = new PolizaModelHandler();
        SiniestroModelHandler siniestroModelHandler = new SiniestroModelHandler();
        PaseMedicoModelHandler paseMedicoModelHandler = new PaseMedicoModelHandler();
        NarrativaModelHandler narrativaModelHandler = new NarrativaModelHandler();
        CircunstanciaModelHandler circunstanciaModelHandler = new CircunstanciaModelHandler();
        DaniosModelHandler daniosModelHandler = new DaniosModelHandler();
        List<Modulo> modulos = new ArrayList<>();
        modulos.add(aseguradoModelHandler.get(id).get(0));
        printData(modulos.get(0),"Asegurado: ");
        modulos.add(involucradoModelHandler.get(id).get(0));
        printData(modulos.get(1),"Involucrado: ");
        modulos.add(polizaModelHandler.get(id).get(0));
        printData(modulos.get(2),"Poliza: ");
        modulos.add(siniestroModelHandler.get(id).get(0));
        printData(modulos.get(3),"Siniestro: ");
        modulos.add(paseMedicoModelHandler.get(id).get(0));
        printData(modulos.get(4),"Pase Medico: ");
        modulos.add(narrativaModelHandler.get(id).get(0));
        printData(modulos.get(5),"Narrativa: ");
        modulos.add(circunstanciaModelHandler.get(id).get(0));
        printData(modulos.get(6),"Circunstancia: ");
        modulos.add(daniosModelHandler.get(id).get(0));
        printData(modulos.get(7),"Danios: ");
        amisInteractor.saveModulos(id, modulos);
        amisInteractor.getModulos(id);
    }

    private <T> void printData(T object, String modulo){
        Log.i(TAG, "validateData: \n"+ modulo + PARSER.toJson(object));
    }

    private <T> void printData(List<T> object, String modulo){
        Log.i(TAG, "validateData: \n"+ modulo + PARSER.toJson(object));
    }

    @After
    public void after() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
