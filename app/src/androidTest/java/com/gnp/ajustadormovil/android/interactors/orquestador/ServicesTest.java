package com.gnp.ajustadormovil.android.interactors.orquestador;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Base64;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.views.splash.SplashActivity;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VolanteVehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PosicionOcupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Comprobante;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.amis.impl.AseguradoModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.InvolucradoGeneralModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.NarrativaModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.PaseMedicoModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.PolizaModelHandler;
import com.gnp.ajustadormovil.android.interactors.amis.impl.SiniestroModelHandler;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

@RunWith(AndroidJUnit4.class)
public class ServicesTest {

    private final String TAG = "TEST";

    @Rule
    public ActivityTestRule<SplashActivity> activity
            = new ActivityTestRule<>(SplashActivity.class);

    private final AjusteInteractor ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
    private final AseguradoInteractor aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
    private final TerceroAutoInteractor terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
    private final TerceroNoAutoInteractor terceroNoAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
    private final TerceroPersonaInteractor terceroPersonaService = OrquestadorInteractorFactory.getTerceroPersonaService();
    private final ResponsabilidadInteractor responsabilidadInteractor = OrquestadorInteractorFactory.getResponsabilidadesService();
    private final RecuperacionInteractor recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor();
    private final VolanteInteractor volanteService = OrquestadorInteractorFactory.getVolanteService();
    private final ConnectionProvider connectionProvider = ConnectionProvider.getInstance();
    private final SyncInteractor syncInteractor = OrquestadorInteractorFactory.getSyncService();
    private final AmisInteractor amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();

    private String id = "289299";    private static final Gson PARSER;

    static {
        PARSER = new GsonBuilder().setPrettyPrinting().create();
    }


    private int numeroFotografias = 5;

    @Before
    public void before() throws Exception {
        String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzYS1maXJlYmFzZS1qd3RAZ25wLWFqdXN0YWRvcm1vdmlsLXFhLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwic3ViIjoic2EtZmlyZWJhc2Utand0QGducC1hanVzdGFkb3Jtb3ZpbC1xYS5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9pZGVudGl0eXRvb2xraXQuZ29vZ2xlYXBpcy5jb21cL2dvb2dsZS5pZGVudGl0eS5pZGVudGl0eXRvb2xraXQudjEuSWRlbnRpdHlUb29sa2l0IiwiaWF0IjoxNTY4MzE1NDA3LCJleHAiOjE1Njg0MDE4MDcsInByb2plY3RpZCI6ImducC1hanVzdGFkb3Jtb3ZpbC1xYSIsInVpZCI6IkNPQ0hPQSIsImNsYWltcyI6eyJyb2wiOiJPQ0hPQSBDQVNUQU5FREEiLCJuZWdvY2lvc09wZXJhYmxlcyI6IiIsImlkcGFydGljaXBhbnRlIjoiIiwibWFpbCI6ImNhcmxvcy5vY2hvYUBnbnAuY29tLm14IiwiYXBlbWF0ZXJubyI6IkNBU1RBTkVEQSIsImdpdmVubmFtZSI6IkNBUkxPUyIsImFwZXBhdGVybm8iOiJPQ0hPQSIsImN1ZW50YWJsb3F1ZWFkYSI6ZmFsc2UsInRpcG91c3VhcmlvIjoiYWp1c3RhZG9yZXMiLCJjZWR1bGEiOiJDNTA2NSIsInJvbGVzIjpbIkFqdXN0YWRvciJdfX0.n8pUNygKOy6VgRvNjNJnVRDgYs9S_KUcp6oDiFMgDBO4m9xg0NaPAbCqylz27VmTXWkQJcEnJcSRlqts61e3jlhyO8aftzVqqqDIavdofrS3lqwDPUu9ZRhY4wZ488YJNnS_5SujcaxGfkxf2O2bHMBOz9emQge81G3RuBYIrFrHyui-WvLSoNqjROSy3qFgzLlknHLtGfONlGd-ioX-nzayR0XLxGNp9Xa3NSy7hsFGn0RHmBarKwiqA_biE5a6fN5qgAobrAoqKaLiB1yxn449wuXZQm0Kqn24PKA4Nmoowbb4haen8HvG67Op_WT4DmI_-JinPjwFNRFBruK4-A";
        String refreshToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJmaXJlYmFzZS1qd3RAZ25wLWF1dGguaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJzdWIiOiJmaXJlYmFzZS1qd3RAZ25wLWF1dGguaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJhdWQiOiJodHRwczpcL1wvaWRlbnRpdHl0b29sa2l0Lmdvb2dsZWFwaXMuY29tXC9nb29nbGUuaWRlbnRpdHkuaWRlbnRpdHl0b29sa2l0LnYxLklkZW50aXR5VG9vbGtpdCIsImlhdCI6MTU2ODMxNTQwNywiZXhwIjo0NzIxOTE1NDA3LCJ1aWQiOiJDT0NIT0EiLCJwcm9qZWN0aWQiOiJnbnAtYWp1c3RhZG9ybW92aWwtcWEiLCJtb2JpbGUiOmZhbHNlLCJjbGFpbXMiOnsicm9sIjoiT0NIT0EgQ0FTVEFORURBIiwibmVnb2Npb3NPcGVyYWJsZXMiOiIiLCJpZHBhcnRpY2lwYW50ZSI6IiIsIm1haWwiOiJjYXJsb3Mub2Nob2FAZ25wLmNvbS5teCIsImFwZW1hdGVybm8iOiJDQVNUQU5FREEiLCJnaXZlbm5hbWUiOiJDQVJMT1MiLCJhcGVwYXRlcm5vIjoiT0NIT0EiLCJjdWVudGFibG9xdWVhZGEiOmZhbHNlLCJ0aXBvdXN1YXJpbyI6ImFqdXN0YWRvcmVzIiwiY2VkdWxhIjoiQzUwNjUiLCJyb2xlcyI6WyJBanVzdGFkb3IiXX19.Pnom3FRNYWQXqNIfJNUSgzxMuspXsQurRgPKNeXOd4Cn2WFTx0_aOULvxy4a3GVyfTWQRK0nd_lEnwbXVfazrI_G8AZ5TfxzejHfUXwo3K7dXs1rgAmhLtU3Yrsu1yuxhcYQIhGA9kJZWBhoh15lmTddCuwYSwfg-8X-rwjkTC1RDYKjb_yf4o3plaYJQhTbuEFspGZ5aaJsAb_X3oZ3HitrEsHPFPG5opBTvTH72HPxRyMasDMzQy8qCWmjDY7CZ186M_X6zbkil4ehFNv4zyDR4iJT1LazNLjRlZS6qyesXKF7Nm45fsuJ7vmWZyjvxkOiG6a5dJnkJoViy4BSKw";
        AppPreferences.setString(AppPreferences.USER_INFO_TOKEN, jwt);
        AppPreferences.setString(AppPreferences.USER_INFO_REFRESH_TOKEN, refreshToken);
        Log.i(TAG, "Inicio: Inicializar información");
        RXUtils.sync(ajusteInteractor.getAjustes());
        Log.i(TAG, "Fin: Inicializar información");
    }

    @After
    public void after() throws Exception {
        Log.i(TAG, "Inicio: Limpiar información");
        //RXUtils.sync(syncInteractor.terminate(id));
        Log.i(TAG, "Fin: Limpiar información");
    }

    @Test
    public void offlineTest() throws Exception {
        connectionProvider.notConnected(id);
        data();
        AseguradoModelHandler aseguradoModelHandler = new AseguradoModelHandler();
        InvolucradoGeneralModelHandler involucradoModelHandler = new InvolucradoGeneralModelHandler();
        PolizaModelHandler polizaModelHandler = new PolizaModelHandler();
        SiniestroModelHandler siniestroModelHandler = new SiniestroModelHandler();
        PaseMedicoModelHandler paseMedicoModelHandler = new PaseMedicoModelHandler();
        NarrativaModelHandler narrativaModelHandler = new NarrativaModelHandler();
        List<Modulo> modulos = new ArrayList<>();
        modulos.add(aseguradoModelHandler.get(id).get(0));
        modulos.add(involucradoModelHandler.get(id).get(0));
        modulos.add(polizaModelHandler.get(id).get(0));
        modulos.add(siniestroModelHandler.get(id).get(0));
        modulos.add(paseMedicoModelHandler.get(id).get(0));
        modulos.add(narrativaModelHandler.get(id).get(0));
        amisInteractor.saveModulos(id,modulos);
        Log.i(TAG, "offlineTest: " + PARSER.toJson(amisInteractor.getModulos(id)));
        synchronize();
    }

    @Test
    public void hibridoTest() throws Exception {
        moduloAseguradoTest();
        moduloAjusteTest();
        connectionProvider.notConnected(id);
        moduloTerceroAutoTest();
        moduloTerceroNoAutoTest();
        synchronize();
        //moduloTerceroPersonaTest();
        //moduloResponsabilidadTest();
        //synchronize();
        //moduloRecuperacionTest();
        //moduloVolanteTest();
        //synchronize();
    }


    @Test
    public void onilineTest() throws Exception {
        data();
        synchronize();
    }

    private void data() throws Exception {
        try {
            Log.i(TAG, "Inicio: Generando Información");
            long start = System.currentTimeMillis();
            moduloAseguradoTest();
            moduloAjusteTest();
            moduloTerceroAutoTest();
            moduloTerceroNoAutoTest();
            moduloTerceroPersonaTest();
            //moduloResponsabilidadTest();
            //moduloRecuperacionTest();
            //moduloVolanteTest();
            long end = System.currentTimeMillis();
            Log.i(TAG, "Fin: Generando Información");
            Log.i(TAG, Thread.currentThread().getId() + "Tiempo transcurrido en generar informacion: " + (end - start));
        } catch (Exception e) {
            Log.e(TAG, "Error en agregar información: \n" + ErrorUtils.getErrorMessage(e), e);
            throw e;
        }
    }

    private void synchronize() {
        Runnable sync = () -> {
            try {
                long start = System.currentTimeMillis();
                RXUtils.sync(syncInteractor.synchronizeData(id));
                long end = System.currentTimeMillis();
                Log.i(TAG, Thread.currentThread().getId() + "Tiempo transcurrido: " + (end - start));
            } catch (Exception e) {
                Log.e(TAG, "Error en sincronizar información: \n" + ErrorUtils.getErrorMessage(e), e);
            }
        };
        Log.i(TAG, "Inicio: Sincronización Información");
        CompletableFuture.allOf(
                CompletableFuture.runAsync(sync),
                CompletableFuture.runAsync(sync)
        ).join();
        Log.i(TAG, "Fin: Sincronización Información");
    }

    public void moduloAjusteTest() throws Exception {
        // Declaración
        Log.i(TAG, "Inicio: Módulo Ajuste");
        //RXUtils.sync(ajusteInteractor.getDeclaracion(id));
        //RXUtils.sync(ajusteInteractor.addEvidenciasDeclaracion(id, getFotografia()));
        //RXUtils.sync(ajusteInteractor.addEvidenciasDeclaracion(id, getFotografia()));
        Log.i(TAG, "Fin: Módulo Ajuste");
    }

    public void moduloAseguradoTest() throws Exception {
        Log.i(TAG, "Inicio: Módulo Asegurado");
        // Conductor
        Persona conductor = getPersona("Conductor", "Asegurado", null);
        RXUtils.sync(aseguradoInteractor.modifyConductor(id, conductor));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(aseguradoInteractor.addFotografiaConductor(id, getFotografia()));
        List<Fotografia> fotografias = RXUtils.sync(aseguradoInteractor.getFotografiasConductor(id));
        RXUtils.sync(aseguradoInteractor.deleteFotografiaConductor(id, fotografias.get(0).getId()));
        conductor.setApellidoMaterno("Modificado");
        RXUtils.sync(aseguradoInteractor.modifyConductor(id, conductor));

        // Vehículo
        VehiculoAsegurado vehiculo = getVehiculoAsegurado();
        aseguradoInteractor.modifyVehiculo(id, vehiculo);
        RXUtils.sync(aseguradoInteractor.modifyVehiculo(id, vehiculo));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(aseguradoInteractor.addFotografiaVehiculo(id, getFotografia()));
        fotografias = RXUtils.sync(aseguradoInteractor.getFotografiasVehiculo(id));
        RXUtils.sync(aseguradoInteractor.deleteFotografiaVehiculo(id, fotografias.get(0).getId()));
        vehiculo.setObservacion("Modificado");
        aseguradoInteractor.modifyVehiculo(id, vehiculo);

        // Ocupantes
        Ocupante ocupante = getOcupante("Ocupante", "Asegurado", "Uno");
        ocupante = RXUtils.sync(aseguradoInteractor.addOcupante(id, ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(aseguradoInteractor.addFotografiaOcupante(id, ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(aseguradoInteractor.getFotografiasOcupante(id, ocupante.getId()));
        RXUtils.sync(aseguradoInteractor.deleteFotografiaOcupante(id, ocupante.getId(), fotografias.get(0).getId()));

        ocupante = getOcupante("Ocupante", "Asegurado", "Dos");
        ocupante = RXUtils.sync(aseguradoInteractor.addOcupante(id, ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(aseguradoInteractor.addFotografiaOcupante(id, ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(aseguradoInteractor.getFotografiasOcupante(id, ocupante.getId()));
        RXUtils.sync(aseguradoInteractor.deleteFotografiaOcupante(id, ocupante.getId(), fotografias.get(0).getId()));
        ocupante.setApellidoMaterno("Dos Modificado");
        RXUtils.sync(aseguradoInteractor.modifyOcupante(id, ocupante.getId(), ocupante));

        ocupante = getOcupante("Ocupante", "Asegurado", "Tres");
        ocupante = RXUtils.sync(aseguradoInteractor.addOcupante(id, ocupante));
        RXUtils.sync(aseguradoInteractor.addFotografiaOcupante(id, ocupante.getId(), getFotografia()));
        RXUtils.sync(aseguradoInteractor.deleteOcupante(id, ocupante.getId()));

        Log.i(TAG, "Fin: Módulo Asegurado");
    }


    public void moduloTerceroAutoTest() throws Exception {
        Log.i(TAG, "Inicio: Módulo Tercero Auto");

        TerceroAuto terceroAuto = RXUtils.sync(terceroAutoService.add(id));

        Persona conductor = getPersona("Conductor", "Tercero", "Uno");
        RXUtils.sync(terceroAutoService.modifyConductor(id, terceroAuto.getId(), conductor));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaConductor(id, terceroAuto.getId(), getFotografia()));

        List<Fotografia> fotografias = RXUtils.sync(terceroAutoService.getFotografiasConductor(id, terceroAuto.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaConductor(id, terceroAuto.getId(), fotografias.get(0).getId()));
        RXUtils.sync(terceroAutoService.modifyConductor(id, terceroAuto.getId(), conductor));

        // Vehículo
        VehiculoTercero vehiculo = getVehiculoTercero("Vehiculo del primer conductor");
        terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo);
        RXUtils.sync(terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaVehiculo(id, terceroAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiaVehiculo(id, terceroAuto.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaVehiculo(id, terceroAuto.getId(), fotografias.get(0).getId()));
        terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo);

        // Ocupantes
        Ocupante ocupante = getOcupante("Ocupante", "Tercero", "Uno");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), fotografias.get(0).getId()));

        ocupante = getOcupante("Ocupante", "Tercero", "Dos");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), fotografias.get(0).getId()));
        ocupante.setApellidoMaterno("Dos Modificado");
        RXUtils.sync(terceroAutoService.modifyOcupante(id, terceroAuto.getId(), ocupante.getId(), ocupante));

        ocupante = getOcupante("Ocupante", "Tercero", "Tres");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));
        RXUtils.sync(terceroAutoService.deleteOcupante(id, terceroAuto.getId(), ocupante.getId()));


        terceroAuto = RXUtils.sync(terceroAutoService.add(id));

        conductor = getPersona("Conductor", "Tercero", "dos");
        RXUtils.sync(terceroAutoService.modifyConductor(id, terceroAuto.getId(), conductor));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaConductor(id, terceroAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasConductor(id, terceroAuto.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaConductor(id, terceroAuto.getId(), fotografias.get(0).getId()));
        conductor.setApellidoMaterno("Modificado");
        RXUtils.sync(terceroAutoService.modifyConductor(id, terceroAuto.getId(), conductor));

        // Vehículo
        vehiculo = getVehiculoTercero("vehiculo del segundo conductor");
        terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo);
        RXUtils.sync(terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaVehiculo(id, terceroAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiaVehiculo(id, terceroAuto.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaVehiculo(id, terceroAuto.getId(), fotografias.get(0).getId()));
        vehiculo.setObservacion("Modificado");
        terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo);

        // Ocupantes
        ocupante = getOcupante("Ocupante", "TerceroDos", "Uno");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), fotografias.get(0).getId()));

        ocupante = getOcupante("Ocupante", "TerceroDos", "Dos");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));

        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), fotografias.get(0).getId()));
        ocupante.setApellidoMaterno("Dos Modificado");
        RXUtils.sync(terceroAutoService.modifyOcupante(id, terceroAuto.getId(), ocupante.getId(), ocupante));

        ocupante = getOcupante("Ocupante", "TerceroDos", "Tres");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));
        RXUtils.sync(terceroAutoService.deleteOcupante(id, terceroAuto.getId(), ocupante.getId()));


        terceroAuto = RXUtils.sync(terceroAutoService.add(id));

        conductor = getPersona("Conductor", "Tercero", "tree");
        RXUtils.sync(terceroAutoService.modifyConductor(id, terceroAuto.getId(), conductor));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaConductor(id, terceroAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasConductor(id, terceroAuto.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaConductor(id, terceroAuto.getId(), fotografias.get(0).getId()));
        RXUtils.sync(terceroAutoService.modifyConductor(id, terceroAuto.getId(), conductor));

        // Vehículo
        vehiculo = getVehiculoTercero("vehiculo del tercero conductor");
        terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo);
        RXUtils.sync(terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaVehiculo(id, terceroAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiaVehiculo(id, terceroAuto.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaVehiculo(id, terceroAuto.getId(), fotografias.get(0).getId()));
        terceroAutoService.modifyVehiculo(id, terceroAuto.getId(), vehiculo);

        // Ocupantes
        ocupante = getOcupante("Ocupante", "TerceroTres", "Uno");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), fotografias.get(0).getId()));

        ocupante = getOcupante("Ocupante", "TerceroTres", "Dos");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroAutoService.getFotografiasOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.deleteFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), fotografias.get(0).getId()));
        ocupante.setApellidoMaterno("Dos Modificado");
        RXUtils.sync(terceroAutoService.modifyOcupante(id, terceroAuto.getId(), ocupante.getId(), ocupante));

        ocupante = getOcupante("Ocupante", "TerceroTres", "Tres");
        ocupante = RXUtils.sync(terceroAutoService.addOcupante(id, terceroAuto.getId(), ocupante));
        RXUtils.sync(terceroAutoService.addFotografiaOcupante(id, terceroAuto.getId(), ocupante.getId(), getFotografia()));
        RXUtils.sync(terceroAutoService.deleteOcupante(id, terceroAuto.getId(), ocupante.getId()));
        RXUtils.sync(terceroAutoService.delete(id, terceroAuto.getId()));

        Log.i(TAG, "Fin: Módulo Tercero Auto");

    }

    public void moduloTerceroNoAutoTest() throws Exception {
        Log.i(TAG, "Inicio: Módulo Tercero No Auto");

        TerceroNoAuto tercero = getTerceroNoAuto("Afectado", "Tercero", "uno");
        TerceroNoAuto terceroNoAuto = RXUtils.sync(terceroNoAutoService.add(id, tercero));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroNoAutoService.addFotografia(id, terceroNoAuto.getId(), getFotografia()));

        List<Fotografia> fotografias = RXUtils.sync(terceroNoAutoService.getFotografias(id, terceroNoAuto.getId()));
        RXUtils.sync(terceroNoAutoService.deleteFotografia(id, terceroNoAuto.getId(), fotografias.get(0).getId()));
        RXUtils.sync(terceroNoAutoService.modify(id, terceroNoAuto.getId(), terceroNoAuto));

        terceroNoAuto = getTerceroNoAuto("Afectado", "Tercero", "Dos");
        terceroNoAuto = RXUtils.sync(terceroNoAutoService.add(id, terceroNoAuto));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroNoAutoService.addFotografia(id, terceroNoAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroNoAutoService.getFotografias(id, terceroNoAuto.getId()));
        RXUtils.sync(terceroNoAutoService.deleteFotografia(id, terceroNoAuto.getId(), fotografias.get(0).getId()));
        terceroNoAuto.getAfectado().setApellidoMaterno("Modificado");
        RXUtils.sync(terceroNoAutoService.modify(id, terceroNoAuto.getId(), terceroNoAuto));

        terceroNoAuto = getTerceroNoAuto("Afectado", "Tercero", "tres");
        terceroNoAuto = RXUtils.sync(terceroNoAutoService.add(id, terceroNoAuto));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroNoAutoService.addFotografia(id, terceroNoAuto.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroNoAutoService.getFotografias(id, terceroNoAuto.getId()));
        RXUtils.sync(terceroNoAutoService.deleteFotografia(id, terceroNoAuto.getId(), fotografias.get(0).getId()));
        RXUtils.sync(terceroNoAutoService.modify(id, terceroNoAuto.getId(), terceroNoAuto));
        RXUtils.sync(terceroNoAutoService.delete(id, terceroNoAuto.getId()));

        Log.i(TAG, "Fin: Módulo Tercero No Auto");

    }

    public void moduloTerceroPersonaTest() throws Exception {

        Log.i(TAG, "Inicio: Módulo Tercero Persona");

        TerceroPersona tercero = RXUtils.sync(terceroPersonaService.add(id, getPersona("Tercero", "Persona", "Uno")));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroPersonaService.addFotografia(id, tercero.getId(), getFotografia()));

        List<Fotografia> fotografias = RXUtils.sync(terceroPersonaService.getFotografias(id, tercero.getId()));
        RXUtils.sync(terceroPersonaService.deleteFotografia(id, tercero.getId(), fotografias.get(0).getId()));

        tercero = RXUtils.sync(terceroPersonaService.add(id, getPersona("Tercero", "Persona", "Dos")));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroPersonaService.addFotografia(id, tercero.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroPersonaService.getFotografias(id, tercero.getId()));
        RXUtils.sync(terceroPersonaService.deleteFotografia(id, tercero.getId(), fotografias.get(0).getId()));
        RXUtils.sync(terceroPersonaService.modify(id, tercero.getId(), getPersona("Tercero", "Persona", "DosModificado")));

        tercero = RXUtils.sync(terceroPersonaService.add(id, getPersona("Tercero", "Persona", "Tres")));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(terceroPersonaService.addFotografia(id, tercero.getId(), getFotografia()));

        fotografias = RXUtils.sync(terceroPersonaService.getFotografias(id, tercero.getId()));
        RXUtils.sync(terceroPersonaService.deleteFotografia(id, tercero.getId(), fotografias.get(0).getId()));
        RXUtils.sync(terceroPersonaService.delete(id, tercero.getId()));

        Log.i(TAG, "Fin: Módulo Tercero Persona");

    }

    public void moduloResponsabilidadTest() throws Exception {

        Log.i(TAG, "Inicio: Módulo Responsabilidad");

        List<TerceroAuto> terceros = RXUtils.sync(terceroAutoService.getAll(id));
        RXUtils.sync(responsabilidadInteractor.addResponsabilidad(id, getResponsabilidad(terceros.get(0))));
        RXUtils.sync(responsabilidadInteractor.addFotografia(id, getFotografia()));

        Log.i(TAG, "Fin: Módulo Responsabilidad");

    }

    public void moduloRecuperacionTest() throws Exception {

        Log.i(TAG, "Inicio: Módulo Recuperación");

        List<TerceroAuto> tercerosAuto = RXUtils.sync(terceroAutoService.getAll(id));
        List<TerceroNoAuto> tercerosNoAutos = RXUtils.sync(terceroNoAutoService.getAll(id));
        List<TerceroPersona> tercerosPersona = RXUtils.sync(terceroPersonaService.getAll(id));

        // Compañía Seguros
        Log.i(TAG, "Inicio: Recuperación Compañía Seguros");
        RecuperacionCompaniaSeguros compania = getRecuperacionCompaniaSeguros();
        compania = RXUtils.sync(recuperacionInteractor.addCompaniaSeguros(id, tercerosAuto.get(0).getId(), compania));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(recuperacionInteractor.addFotografia(id, tercerosAuto.get(0).getId(), compania.getId(), getFotografia()));
        compania = getRecuperacionCompaniaSeguros();

        compania = RXUtils.sync(recuperacionInteractor.addCompaniaSeguros(id, tercerosAuto.get(0).getId(), compania));
        for (int i = 0; i < numeroFotografias; i++)
            RXUtils.sync(recuperacionInteractor.addFotografia(id, tercerosAuto.get(0).getId(), compania.getId(), getFotografia()));

        RXUtils.sync(recuperacionInteractor.deleteRecuperacion(id, tercerosAuto.get(0).getId(), compania.getId()));
        Log.i(TAG, "Fin: Recuperación Compañía Seguros");

        // Garantía Prendaría
        Log.i(TAG, "Inicio: Garantía Prendaría");
        RecuperacionGarantiaPrendaria garantia = getRecuperacionGarantiaPrendaria();
        RXUtils.sync(recuperacionInteractor.addGarantiaPrendaria(id, tercerosAuto.get(1).getId(), garantia));
        garantia = getRecuperacionGarantiaPrendaria();
        garantia = RXUtils.sync(recuperacionInteractor.addGarantiaPrendaria(id, tercerosAuto.get(1).getId(), garantia));
        RXUtils.sync(recuperacionInteractor.deleteRecuperacion(id, tercerosAuto.get(1).getId(), garantia.getId()));
        Log.i(TAG, "Fin: Garantía Prendaría");

        // Mesa de controversía
        Log.i(TAG, "Inicio: Mesa de controversía");
        RecuperacionMesaControversia mesa = getRecuperacionMesaControversia();
        RXUtils.sync(recuperacionInteractor.addMesaControversia(id, tercerosNoAutos.get(0).getId(), mesa));
        mesa = getRecuperacionMesaControversia();
        mesa = RXUtils.sync(recuperacionInteractor.addMesaControversia(id, tercerosNoAutos.get(0).getId(), mesa));
        RXUtils.sync(recuperacionInteractor.deleteRecuperacion(id, tercerosNoAutos.get(0).getId(), mesa.getId()));
        Log.i(TAG, "Fin: Mesa de controversía");

        // Turnado asistencia Legal
        Log.i(TAG, "Inicio: Turnado asistencia Legal");
        RecuperacionTurnadoAsistenciaLegal turnado = getRecuperacionTurnadoAsistenciaLegal();
        RXUtils.sync(recuperacionInteractor.addTurnadoAsistenciaLegal(id, tercerosNoAutos.get(1).getId(), turnado));
        turnado = getRecuperacionTurnadoAsistenciaLegal();
        turnado = RXUtils.sync(recuperacionInteractor.addTurnadoAsistenciaLegal(id, tercerosNoAutos.get(1).getId(), turnado));
        RXUtils.sync(recuperacionInteractor.deleteRecuperacion(id, tercerosNoAutos.get(1).getId(), turnado.getId()));
        Log.i(TAG, "Fin: Turnado asistencia Legal");

        // Monetaría
        Log.i(TAG, "Inicio: Monetaría");
        RecuperacionMonetaria monetaria = getRecuperacionMonetaria();
        monetaria = RXUtils.sync(recuperacionInteractor.addMonetaria(id, tercerosPersona.get(0).getId(), monetaria));
        Comprobante comprobante = getComprobante("De alguien", "Observaciones");
        RXUtils.sync(recuperacionInteractor.addComprobante(id, tercerosPersona.get(0).getId(), monetaria.getId(), comprobante));
        monetaria = getRecuperacionMonetaria();
        monetaria = RXUtils.sync(recuperacionInteractor.addMonetaria(id, tercerosPersona.get(0).getId(), monetaria));
        RXUtils.sync(recuperacionInteractor.deleteRecuperacion(id, tercerosPersona.get(0).getId(), monetaria.getId()));
        Log.i(TAG, "Fin: Monetaría");

        Log.i(TAG, "Fin: Módulo Recuperación");

    }

    public void moduloVolanteTest() throws Exception {

        Log.i(TAG, "Inicio: Módulo Volantes");

        Asegurado asegurado = RXUtils.sync(aseguradoInteractor.get(id));
        List<TerceroAuto> tercerosAuto = RXUtils.sync(terceroAutoService.getAll(id));
        List<TerceroNoAuto> tercerosNoAutos = RXUtils.sync(terceroNoAutoService.getAll(id));
        List<TerceroPersona> tercerosPersona = RXUtils.sync(terceroPersonaService.getAll(id));

        // Asegurado
        Log.i(TAG, "Inicio: Volantes asegurado");
        RXUtils.sync(volanteService.generateAseguradoConductor(id, getVolanteMedicoAsegurado()));
        RXUtils.sync(volanteService.generateAseguradoVehiculo(id, getVolanteVehiculoAsegurado()));
        for (Ocupante ocupante : asegurado.getOcupantes()) {
            RXUtils.sync(volanteService.generateAseguradoOcupante(id, ocupante.getId(), getVolanteMedicoAsegurado()));
        }
        Log.i(TAG, "Fin: Volantes asegurado");

        // Tercero Auto
        Log.i(TAG, "Inicio: Volantes tercero auto");
        for (TerceroAuto tercero : tercerosAuto) {
            RXUtils.sync(volanteService.generateTerceroAutoConductor(id, tercero.getId(), getVolanteMedicoTercero()));
            RXUtils.sync(volanteService.generateTerceroAutoVehiculo(id, tercero.getId(), getVolanteVehiculoTercero()));
            for (Ocupante ocupante : tercero.getOcupantes()) {
                RXUtils.sync(volanteService.generateTerceroAutoOcupante(id, tercero.getId(), ocupante.getId(), getVolanteMedicoTercero()));
            }
        }
        Log.i(TAG, "Fin: Volantes tercero auto");

        // Tercero No Auto
        Log.i(TAG, "Inicio: Volantes tercero no auto");
        for (TerceroNoAuto tercero : tercerosNoAutos) {
            RXUtils.sync(volanteService.generateTerceroNoAuto(id, tercero.getId(), getVolanteTerceroNoAuto(tercero.getDescripcion(), "Volante")));
        }
        Log.i(TAG, "Fin: Volantes tercero auto");

        // Tercero Persona
        Log.i(TAG, "Inicio: Volantes tercero personas");
        for (TerceroPersona tercero : tercerosPersona) {
            RXUtils.sync(volanteService.generateTerceroPersona(id, tercero.getId(), getVolanteMedicoTercero()));
        }
        Log.i(TAG, "Fin: Volantes tercero personas");

        Log.i(TAG, "Fin: Módulo Volantes");

    }

    private VehiculoAsegurado getVehiculoAsegurado() {
        VehiculoAsegurado vehiculo = new VehiculoAsegurado();
        vehiculo.setVin("VSSPK46L65R069469");
        vehiculo.setTipoVehiculo("AUT");
        vehiculo.setArmadora("FI");
        vehiculo.setCarroceria("06");
        vehiculo.setVersion("02");
        vehiculo.setModelo((short) 2012);
        vehiculo.setNumeroMotor("AZL042582");
        vehiculo.setPlacas("890RMD");
        vehiculo.setColor(1L);
        vehiculo.setPrimerImpacto(27L);
        vehiculo.setDanoConsecuencia(true);
        vehiculo.setPerdidaTotalEvidente(false);
        vehiculo.setObservacion("Vehículo Asegurado");
        vehiculo.setDanos(getPiezas());
        return vehiculo;
    }

    private List<PiezaVehiculo> getPiezas() {
        List<PiezaVehiculo> piezas = new ArrayList<>();
        PiezaVehiculo pieza = new PiezaVehiculo();
        pieza.setAreaAfectada(9L);
        pieza.setParteAfectada(196L);
        piezas.add(pieza);
        pieza = new PiezaVehiculo();
        pieza.setAreaAfectada(9L);
        pieza.setParteAfectada(197L);
        piezas.add(pieza);
        return piezas;
    }

    private Persona getPersona(String nombre, String apePat, String apeMat) {
        Persona persona = new Persona();
        persona.setNombre(nombre);
        persona.setApellidoPaterno(apePat);
        persona.setApellidoMaterno(apeMat);
        persona.setSexo("M");
        persona.setFechaNacimiento(getDate("1980-01-01"));
        persona.setTipoLicencia("A");
        persona.setLicenciaPermanente(true);
        persona.setTipoIdentificacion("LC");
        persona.setCondicion(CondicionPersona.LESIONADO);
        persona.setRequiereAmbulancia(true);
        persona.setHospital(100008L);
        persona.setNivelLesion(4L);
        persona.setEstadoLicencia("01");
        persona.setAutorizaTratamiento(true);
        persona.setNumeroIdentificacion("ABCDEFGHIJK");
        persona.setTelefonos(getTelefonos());
        persona.setCorreos(Arrays.asList("francisco.montiel@gnp.com.mx"));
        persona.setLesiones(Arrays.asList(43L, 49L, 67L));
        return persona;
    }

    private Ocupante getOcupante(String nombre, String apePat, String apeMat) {
        Ocupante ocupante = new Ocupante();
        ocupante.setNombre(nombre);
        ocupante.setApellidoPaterno(apePat);
        ocupante.setApellidoMaterno(apeMat);
        ocupante.setSexo("M");
        ocupante.setFechaNacimiento(getDate("1980-01-01"));
        ocupante.setTipoIdentificacion("C");
        ocupante.setCondicion(CondicionPersona.LESIONADO);
        ocupante.setRequiereAmbulancia(true);
        ocupante.setHospital(100008L);
        ocupante.setNivelLesion(4L);
        ocupante.setAutorizaTratamiento(true);
        ocupante.setNumeroIdentificacion("ABCDEFGHIJK");
        ocupante.setTelefonos(getTelefonos());
        ocupante.setCorreos(Arrays.asList("francisco.montiel@gnp.com.mx"));
        ocupante.setLesiones(Arrays.asList(43L, 49L, 67L));
        ocupante.setPosicion(PosicionOcupante.ADD);
        return ocupante;
    }

    private List<Telefono> getTelefonos() {
        Telefono telefono = new Telefono();
        telefono.setTipo("C");
        telefono.setTelefono("5566778899");
        return Arrays.asList(telefono);
    }

    private Date getDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    private File getFotografia() {
        String base64 = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAUFBQUFBQUGBgUICAcICAsKCQkKCxEMDQwNDBEaEBMQEBMQGhcbFhUWGxcpIBwcICkvJyUnLzkzMzlHREddXX0BBQUFBQUFBQYGBQgIBwgICwoJCQoLEQwNDA0MERoQExAQExAaFxsWFRYbFykgHBwgKS8nJScvOTMzOUdER11dff/CABEIASwBLAMBIgACEQEDEQH/xAAdAAEAAwADAQEBAAAAAAAAAAAABwgJAwQGBQEC/9oACAEBAAAAALlgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB834wAB6Dt+b8L9733H5oDs+iAq9QgAA0yhin/U/ZhuvmMBKeooFXqG6A+MpZbWWqDTf0YP0E5XkaO3MppZizPlaMeujC3kqZ/S3aaqHR1FAq9Q3YCMsx9DrE1FlOvlW9gu6j7NGbq0at+9Z/1q/jRie8gbEX7r18O0IHz/AIvqYgzH0OsUUUq1rX2nY+nG2XEiWym/sfM/fR93IGxF+wAIfzH0OsUUUqlyE66Pq5Ui8jyzJeOTHFkDYi/YAEP5j6HWKKKVZvpyPVzd43xMuxVBFXWq/ouLIGxF+wAIfzH0OsUUUq3sF3RUilGwPdQNnBonYXiyBsRfsACH8x9DrFFFKt7Bd0VIo9eW2X7Uqjmks58WQNiL9gArVTyP9DrFFFKqyX/Q875H9k3vRTLOmXjM8Y1svfsAEJV5WflQrpC4HLKUUPZWu+n5KnH7Lll35+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//8QATRAAAAQDAQcOCgcHBQAAAAAAAQIDBQAEBgcIEBESVpSyExQWGDE2N1JVcnR1kdEXICEiMDM0NUFRMkJhYnOxsxUjJHGBgsJAU4CQo//aAAgBAQABPwD/AJ4uTw1NCZFnJxlpRI5sUp11ATKI/Lzo2dUblU152n3xs6o3KprztPvjZ1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xs6o3KprztPvjZ1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xs6o3KprztPvjZ1RuVTXnaffElOyTlKpzcjOJTEup9BVI4HIPNEt57rGlqb853fpOTHiKKgBx/pBrebK0zmIaqCfzBFQwQ0WqWePqiaMlVkkKp/IBTn1PSxYKYpygYpgEB3BAd2JmYl5RBVddUiSCZcY5zjigUOMYY2dUXlS152n3xs6o3KprztPvjZ1RuVTXnaffGzqjcqmvO0++NnVG5VNedp98bOqNyqa87T742dUblU152n3xKVbS0/MJS0rUbcsuoOKmklMEOc4/YAD6C6x3q0/wBY/wCH+gsBEAsopsekfqRbBdAzSU3N0/SEwCYJCZOZcS6KUTc5Nzy55iamVF1VBwmUUMJzCP8AMb3lAYoG2ir6FWQSLOHnmwB8+SXNoDukGHyrmWt7Iqnd2lfHRUalynJ9dI+J5SHD0Fi3CfSXS/QXWO9Wn+sf8IIAGOQB3BEAhvubLO5mQkF1AcMdaXTUHAvxwi1Gwih6Qod6em7XmvJUiYp463GOBb1i1jdH15SJ3Z3Cb1yE4sj+5VxAwFgbmWzgCj7xziHqVSkHh1k0sOpITSyRMO7gIYShFhFmtN2hrVCR7BfBKEQFLUT4n04t1s8YbPHlmk2UF9TmJQVVNWPjjCBQOskQdwxygPbErc02cqyssofX+FRJM/tHHCNrLZx8nHOI2slm/wAnHOIr+5/oOmqNqB4kdfa5lJUVEsde8hVi9KXNTYeUUEk3PnWk0jfEmqKGwiWBHCOEYsgsGUrSSSfH2YUlms/s6KfrV4SufrK0kgT2OmU++ZdSKjuZqFcJVb9jHmW2axfMNjiqnFS0840s9uLM4pYkzKKYhvkPyMH2GCLPavmmJGqGgVR1i7tUyioQR8gKlIJiHvWOWLUbXFGJu7sE3roZlVMdSWxAwFhS5ms3KRT3juGH18OSCcs4zyCWHESXUTLh+RTYIsQoVjr6pp9tedX1BKSMsXUj4g4+MBY2stnHycc4i2qi2agquSaGfVtbDIorDqpsccJ4sW4T6S6X6C6x3q0/1j/hCPrU+eWGT3O19ER0Ai3ngqqrmo/qXrl7g6V61XgS4QMHzhzuWQcXJwndmGJrhdRXE1rxhiyeyLwYKvR/23r7XxEg9TqeJqUXV2+WmugH04lvaZf8Qv5w3+wSX4BNEL9qsrMzlnlVoSyB1llJIwEIQuE5h+6EbCawyWc81U7orhtcZGwWgE5uUVQOi5rgomoQSCGNjYMIGvWYT0lPWfUirInIKRW9JPAX4HIGAwX7qOUQQr6SXIQAOu2JmV5xTiEFMYo4SjgG9c1cGaHT14V9UrzDaMPfvh26WtpjFyvv6duqVdMt60+wototRketkWs8EsmjqWoY/wBAYoy5xCkamaH3ZTrnWS2PqWtsTG9A5szS8IpouTdKzaZRxiEXTBUCjxvOjYNRmSrXmqXdBEyJkAhAApSlwAAbgBFvPBVVXNR/UvXL3B0r1qv4l1dvlproB9OJYcEzLiP+4X84kajp4shJAL5JAJUSYf4gny/nGySn+XJLOCd8bJad5dkc4J3xLPTNOKFRlnSUWVEMIETVA5h/oWMWLWqOPW9DOzWgQBmiBriV/FS84A/uhdBaWWURWSMRRMwlOUwYBAxd0IoO1OrLPVjA1zQKSShsKsmt5yRu40UzdQ0c4ETI+N8y2L8Yn75GGKuaPqYhBaKik5lTiAqBT9hodqRpZ/XJMuzBJTqxSYhFF0gOIF4seDSgMi2rNix4NaByLas2LDWztTJKllGtvQk5cDCcEkCAQmE3xwQr6pXmG0Ye/fDt0tbTGLlff07dUq6Zb4+jt54Kqq5qP6l65e4OletV/Eurt8tNdAPp3sc/HHtjHPxx7Yxz8ce2LnI4+FFr6NM6F+1mwSQrRZZ5YVEpJ4OXGWTEMVGYNFTUPVVIzB0HpkmJbAbABxLhTNzTl8g3klVETgomoYhg3DFHAIRR1u1e0kZJIziLlIhuy02OP2Hize1emrRZPDJmFBxSLhWkVB88v3i31fVK8w2jD374dulraYxcr7+nbqlXTLfH0dvPBVVXNR/UvXL3B0r1qv4l1dvlproB9OESAoskQdwxygMSly3Q68tLKi7OgCdIpzeeT68bVmhOVnbtJG1ZoTlZ27SRRVhNL0M/Sz63OE+pMJEOUCrGJi+deq6vaZoZGTWfp40sSZOYqQgkJ8Il5sJ3QVlZzkID8oImEoB/DKQuhJuEtqayCS6KhMOKqQDgIH+w0P8AYRZpUAnUFiCTWP8AXlDCjFUXK06gRVamXwkz8paaDEOPNOWH+m3ulnBRueW1WUmSfUUDdD5lHcEIp2oHKl3iReGxcyUzLKFMUQHdD4lH7DRSdQytV00zPcr5CTqBVDE4pviX+0byvqleYbRh798O3S1tMYuV9/Tt1Srplvj6O3ngqqrmo/qXrl7g6V61X8S6u3y010A+nEt7TL/iF/OG/wBgkvwCaIeLdZe5qS6SvEqYCTUucdwqhRHthtXSmG1uWSOUyakuiYgh8QMUL90RTsg72duLgskTXTYYiyKvOMBRLeuZp1SZs3BAw4Ql3FchLyvqleYbRh798O3S1tMYuV9/Tt1Srplvj6O3ngpqrmo/qXrl7g6V61X8S6u3y010A+nEt7TL/iF/OG/2CS/AJoh4t1l7mpLpK96wS2aRmG2SpN+nCoTcsGJJLqDgIsTiCMAJRDCBvJGGLpK0ZvTZzUc3zJFZuYVA87ibiRCeUCXrnhlVZ7NW46xRKeeXVmu3zS3lfVK8w2jD374dulraYxcr7+nbqlXTLfH0V0dVlR0k202sxuq0kdddYqop/WAsO1p9dvrdMtrnUUzMSa4ACiR8GKbFHDeuXuDpXrVfxLq7fLTXQD6cFMJDAYBwCA4QGCWx2lkIUpatnAAoYAjwzWmZXzkeGa0zK+cjwzWmZXzkeGa0zK+cioq4qqrEpZF8eV50kuYxkgVweaJrwCIDhAcAww2v2iU4iRCQqaYFEgYCpLYFigH2Y+NDtbhaa9InQmKmVTSMGAwIEIiPaUIUUUWUOoocxzmHCJjDhERiyay5ytDfEMdI6TPLHKacmR0CfMxolJOXkJSXlZZMCIoJlIQgfRKUoYoFi05xn2agqncJCZMhNoSYmSVLulGPDLablfNwsspMKqKqGExzmMYwj8RNFyvv6duqVdMt8fRW12Yu1pUgxINs/LIGlFTnOK/3gxY2qVacttvYrG1SrTltt7FYshoVxs8pVVmcJlBdUZw62OjuYDB4ltFjz5aU7tM62uEmgSVlTJHKvjxtUq05bbexWNqlWnLbb2KxtUq05bbexWNqlWnLbb2KxtUq05bbexWNqlWnLbb2KxtUq05bbexWErlesElkjnfGvFKcvHis7minnwBm2Kb/AGVOfWSxceWMaNqlWnLbb2KxtUq05bbexWKUuWEUJgi9UPQLplN7LKAJQNzjjDSztrFIIN7bJpysqiGBNJMuAAvV0wzNUUk+s0ssRNecljJEOf6AD9sbVWseXWv/ANI2qVacttvYrFjliz/ZzUc46T7lJLpLSYogVDH/AOhz/8QAFBEBAAAAAAAAAAAAAAAAAAAAgP/aAAgBAgEBPwA0/wD/xAAUEQEAAAAAAAAAAAAAAAAAAACA/9oACAEDAQE/ADT/AP/Z";
        byte[] image = Base64.decode(base64, Base64.DEFAULT);
        File file = new File(Utils.TEMP_FOLDER + File.separator + new Random().nextInt(Integer.MAX_VALUE) + ".jpg");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(file);
            out.write(image);
        } catch (IOException e) {
            throw new RuntimeException("Error al crear fotografía", e);
        }
        return file;
    }

    private VehiculoTercero getVehiculoTercero(String observaciones) {
        VehiculoTercero vehiculo = new VehiculoTercero();
        vehiculo.setVin("VSSPK46L65R069469");
        vehiculo.setTipoVehiculo("AUT");
        vehiculo.setArmadora("FI");
        vehiculo.setCarroceria("06");
        vehiculo.setVersion("02");
        vehiculo.setModelo((short) 2012);
        vehiculo.setNumeroMotor("AZL042582");
        vehiculo.setPlacas("890RMD");
        vehiculo.setPrimerImpacto(27L);
        vehiculo.setColor(1L);
        vehiculo.setDanoConsecuencia(true);
        vehiculo.setPerdidaTotalEvidente(false);
        vehiculo.setObservacion(observaciones);
        vehiculo.setDanos(getPiezas());
        return vehiculo;
    }

    private TerceroNoAuto getTerceroNoAuto(String nombre, String apellidoPaterno, String apellidoMaterno) {
        TerceroNoAuto noAuto = new TerceroNoAuto();
        noAuto.setCategoriaNoAuto(1L);
        noAuto.setTipoNoAuto(5L);
        noAuto.setAreaDanada("nada");
        noAuto.setDanoPreexistente("paso");
        noAuto.setReclamo(true);
        Afectado afectado = new Afectado();
        afectado.setNombre(nombre);
        afectado.setTipoPersona("F");
        afectado.setGenero("F");
        afectado.setApellidoPaterno(apellidoPaterno);
        afectado.setApellidoMaterno(apellidoMaterno);
        afectado.setFechaNacimiento(getDate("1995-06-23"));
        afectado.setCorreos(Arrays.asList("abraham.flores@gnp.com.mx"));
        afectado.setTelefonos(getTelefonos());
        noAuto.setAfectado(afectado);
        noAuto.setCalle("cerro de las torres");
        noAuto.setCodigoPostal("98765");
        noAuto.setColonia("0520");
        noAuto.setMunicipio("001");
        noAuto.setEstado("01");
        noAuto.setNumeroInterior("3");
        noAuto.setNumeroExterior("43");
        noAuto.setEntreCalles("ninguna");
        noAuto.setReferencias("referencias");
        noAuto.setKilometro("6473");
        return noAuto;
    }

    private RecuperacionMonetaria getRecuperacionMonetaria() {
        RecuperacionMonetaria monetaria = new RecuperacionMonetaria();
        monetaria.setEntidadFinanciera("0102");
        monetaria.setMonto(122.22);
        monetaria.setNumeroAutorizacion("1234567890");
        monetaria.setFolioComprobante("1234567890");
        monetaria.setMoneda("USD");
        monetaria.setTipoCambio(2.33);
        monetaria.setMonto(284.77);
        monetaria.setTipoRecuperacionMonetaria(4L);

        return monetaria;
    }

    private RecuperacionGarantiaPrendaria getRecuperacionGarantiaPrendaria() {
        RecuperacionGarantiaPrendaria garantiaPrendaria = new RecuperacionGarantiaPrendaria();
        garantiaPrendaria.setMismoVehiculo(true);
        garantiaPrendaria.setTipoVehiculo("ELE");
        garantiaPrendaria.setArmadoraVehiculo("DD");
        garantiaPrendaria.setCarroceriaVehiculo("01");
        garantiaPrendaria.setModeloVehiculo((short) 1998);
        garantiaPrendaria.setVersionVehiculo("00");
        garantiaPrendaria.setPlacasVehiculo("TUB8010");
        garantiaPrendaria.setVinVehiculo("1234212");
        garantiaPrendaria.setNumeroMotor("1234556");
        garantiaPrendaria.setNumeroFactura("222");
        garantiaPrendaria.setFolioGrua("3241");
        garantiaPrendaria.setDocumentoPropiedad("FAC");
        garantiaPrendaria.setRecibidoDe("la misma persona");
        garantiaPrendaria.setVencimiento(getDate("2019-07-31"));
        garantiaPrendaria.setCodigoPostal("73170");
        garantiaPrendaria.setEstado("01");
        garantiaPrendaria.setMunicipio("001");
        garantiaPrendaria.setColonia("0059");
        garantiaPrendaria.setCalle("Cerro del tezontle");
        garantiaPrendaria.setNumeroInterior("1");
        garantiaPrendaria.setNumeroExterior("2");
        garantiaPrendaria.setReferencia("referencias");
        garantiaPrendaria.setFolioDocumento("DSFSFD");
        garantiaPrendaria.setNumeroContrato("CONTRATO");
        return garantiaPrendaria;
    }

    private RecuperacionCompaniaSeguros getRecuperacionCompaniaSeguros() {
        RecuperacionCompaniaSeguros companiaSeguros = new RecuperacionCompaniaSeguros();
        companiaSeguros.setNumeroPoliza("23132");
        companiaSeguros.setAseguradora("1");
        companiaSeguros.setTipoCobertura("neuenf");
        companiaSeguros.setVigenciaPoliza(getDate("2019-07-31"));
        companiaSeguros.setTitularPoliza("ninguno");
        companiaSeguros.setNumeroSiniestro("ninguno");
        companiaSeguros.setFolioAdmision("12313124");
        companiaSeguros.setTipoVolante("OM");
        companiaSeguros.setNombreAjustador("JAvier");
        companiaSeguros.setDanoEstimado(100.00);
        companiaSeguros.setNumeroOrdenMedicas(1);
        return companiaSeguros;
    }

    private RecuperacionTurnadoAsistenciaLegal getRecuperacionTurnadoAsistenciaLegal() {
        RecuperacionTurnadoAsistenciaLegal turnadoAsistenciaLegal = new RecuperacionTurnadoAsistenciaLegal();
        turnadoAsistenciaLegal.setAseguradora("74");
        turnadoAsistenciaLegal.setObservaciones("nninguna");
        return turnadoAsistenciaLegal;
    }

    private RecuperacionMesaControversia getRecuperacionMesaControversia() {
        RecuperacionMesaControversia mesaControversia = new RecuperacionMesaControversia();
        mesaControversia.setNombreAbogado("nombre Asegurado");
        mesaControversia.setObservaciones("observaciones");
        mesaControversia.setDespachoAbogado("despacho");
        return mesaControversia;
    }

    private Responsabilidad getResponsabilidad(Tercero tercero) {
        Responsabilidad responsabilidad = new Responsabilidad();
        responsabilidad.setResponsabilidadGuia("B");
        responsabilidad.setResponsabilidadAjustador("A");
        responsabilidad.setCircunstanciaA(1L);
        responsabilidad.setCircunstanciaB(12L);
        responsabilidad.setAseguradoResponsable(true);
        responsabilidad.setTipoTercero(tercero.getTipo());
        //responsabilidad.setTercero(tercero.getId());
        responsabilidad.setTerceroResponsable(false);
        return responsabilidad;
    }

    private VolanteMedico getVolanteMedicoAsegurado() {
        VolanteMedico volanteMedico = new VolanteMedico();
        volanteMedico.setAplicaDeducible(true);
        volanteMedico.setImpresionFisica(true);
        volanteMedico.setEnvioEmail(true);
        volanteMedico.setCoberturas(Arrays.asList("0000000906"));
        return volanteMedico;
    }

    private VolanteMedico getVolanteMedicoTercero() {
        VolanteMedico volanteMedico = new VolanteMedico();
        volanteMedico.setAplicaDeducible(true);
        volanteMedico.setImpresionFisica(true);
        volanteMedico.setEnvioEmail(true);
        volanteMedico.setCoberturas(Arrays.asList("0000001273"));
        return volanteMedico;
    }

    private VolanteVehiculoAsegurado getVolanteVehiculoAsegurado() {
        VolanteVehiculoAsegurado volanteVehiculoAsegurado = new VolanteVehiculoAsegurado();
        volanteVehiculoAsegurado.setVolanteCondicionado(false);
        volanteVehiculoAsegurado.setAplicaDeducible(true);
        volanteVehiculoAsegurado.setRequiereGrua(false);
        volanteVehiculoAsegurado.setFolioSegmentacion(null);
        volanteVehiculoAsegurado.setImpresionFisica(true);
        volanteVehiculoAsegurado.setEnvioEmail(true);
        volanteVehiculoAsegurado.setCoberturas(Arrays.asList("0000001289"));
        volanteVehiculoAsegurado.setPiezasFaltantes(null);
        return volanteVehiculoAsegurado;
    }

    private VolanteVehiculoTercero getVolanteVehiculoTercero() {
        VolanteVehiculoTercero volanteVehiculoTercero = new VolanteVehiculoTercero();
        volanteVehiculoTercero.setVolanteCondicionado(false);
        volanteVehiculoTercero.setRequiereGrua(false);
        volanteVehiculoTercero.setFolioSegmentacion(null);
        volanteVehiculoTercero.setImpresionFisica(false);
        volanteVehiculoTercero.setEnvioEmail(true);
        volanteVehiculoTercero.setCoberturas(Arrays.asList("0000001273"));
        volanteVehiculoTercero.setTipoTercero("TAG");
        volanteVehiculoTercero.setTipoVolante("T");
        return volanteVehiculoTercero;
    }

    private VolanteTerceroNoAuto getVolanteTerceroNoAuto(String nombre, String observaciones) {
        VolanteTerceroNoAuto volanteTerceroNoAuto = new VolanteTerceroNoAuto();
        volanteTerceroNoAuto.setTipoVolante("T");
        volanteTerceroNoAuto.setContratista(20793L);
        volanteTerceroNoAuto.setMonto(4444.4);
        volanteTerceroNoAuto.setImpresionFisica(true);
        volanteTerceroNoAuto.setNombre(nombre);
        volanteTerceroNoAuto.setObservaciones(observaciones);
        volanteTerceroNoAuto.setTelefono("7751589378");
        volanteTerceroNoAuto.setEnvioEmail(true);
        volanteTerceroNoAuto.setCoberturas(Arrays.asList("0000001273"));
        volanteTerceroNoAuto.setCorreo("abraham.flores@gnp.com.mx");
        return volanteTerceroNoAuto;
    }

    private Comprobante getComprobante(String recibido, String observaciones) {
        Comprobante comprobante = new Comprobante();
        comprobante.setRecibimosDe(recibido);
        comprobante.setFechaPromesa(getDate("2020-07-31"));
        comprobante.setDeducible(false);
        comprobante.setMontoAcordado(50000.00);
        comprobante.setMontoRecibido(10000.00);
        comprobante.setObservaciones(observaciones);
        comprobante.setImporteTotal(1500.00);
        comprobante.setImporteTotalLetra("nada");
        comprobante.setTelefono("7751589378");
        comprobante.setEstado("01");
        comprobante.setMunicipio("001");
        comprobante.setNumeroExterior("3");
        comprobante.setColonia("0059");
        comprobante.setCodigoPostal("076578");
        comprobante.setCalle("la resolanda");
        comprobante.setEntreCalles("sol y luna");
        return comprobante;
    }
}
