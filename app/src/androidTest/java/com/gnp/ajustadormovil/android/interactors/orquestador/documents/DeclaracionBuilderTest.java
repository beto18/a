package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class DeclaracionBuilderTest {

    private static final String TAG = "DocumentBuilder";

    @Rule
    public ActivityTestRule<BaseActivity> activity
            = new ActivityTestRule<>(BaseActivity.class);

    @Test
    public void buildTest(){
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File out = new File(folder, "declaracion.pdf");

        final CountDownLatch signal = new CountDownLatch(1);

        new DeclaracionBuilder("0000000001", "C001")
                .nombre("Francisco")
                .apellidoPaterno("Montiel")
                .apellidoMaterno("Valenciana")
                .causaSiniestro("Me chocaron")
                .estatusPoliza("Vigente")
                .fechaNacimiento(DateUtils.date("25/08/1986"))
                .tipoVehiculo("Automóvil")
                .modelo((short)2016)
                .vehiculo("Mazda 3")
                .numeroSerie("ABCDEFGHO")
                .numeroPoliza("0000000001")
                .titularPoliza("Francisco Montiel")
                .vigenciaPoliza(DateUtils.date("25/08/2020"))
                .ubicacionSiniestro("Av. Cerro de las torrez 25")
                .fechaSiniestro(DateUtils.date("01/01/2019 09:30:00 am"))
                .fechaAtencion(DateUtils.date("01/01/2019 10:00:00 am"))
                .build(out)
                .subscribe(folio -> {
                    signal.countDown();
                    Log.i(TAG, "Folio: " + folio);
                }, throwable -> {
                    Log.e(TAG, "Error", throwable);
                    Assert.fail();
                    signal.countDown();
                });
        try {
            signal.await();
        } catch (Exception e){

        }
    }

}
