package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class RecuperacionBuilderTest {

    private static final String TAG=RecuperacionBuilderTest.class.getSimpleName();
    @Rule
    public ActivityTestRule<BaseActivity> activity
            = new ActivityTestRule<>(BaseActivity.class);

    @Test
    public void buildTest(){
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File out = new File(folder, "recuperacion.pdf");

        final CountDownLatch signal = new CountDownLatch(1);

        new RecuperacionBuilder("0000000001", "C001")
                .estado("CDMX")
                .municipio("Miguel Hidalgo")
                .calleNumExtInt("12345")
                .entreCalles("la que sea")
                .contratanteNombreCompleto("Javier Vite Roldan")
                .ajustadorNombreCompleto("El wey ajustador")
                .fechaCobro(new Date())
                .recibimosDe("Javier Vite Roldan")
                .importeTotalLetra("Vente mil pesos")
                .tipoRecuperacionMonetaria("Foo")
                .recuperacionEntidadFinanciera("foo")
                .telefono("1234567890")
                .tipoAfectado("foo")
                .montoRecibido(new Double(10000))
                .importeTotal(new Double(80000))
                .fechaPromesa(new Date())
                .armadora("La armadora")
                .carroceria("foo")
                .version("full")
                .modelo((short)2016)
                .numeroMotor("098120982")
                .vin("09812093812")
                .placas("DNF-FJFK")
                .observaciones("algo")
                .numeroPoliza("098098")
                .difImportes(new Double(12345))
                .build(out)
                .subscribe(folio -> {
                    signal.countDown();
                    Log.i(TAG, "Folio: " + folio);
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.e(TAG,"error",throwable);
                    signal.countDown();
                    Assert.fail();
                });

        try {
            signal.await();
        } catch (Exception e){

        }
    }

}
