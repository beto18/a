package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class VolanteMedicoBuilderTest {

    private static final String TAG= VolanteMedicoBuilderTest.class.getSimpleName();
    @Rule
    public ActivityTestRule<BaseActivity> activity
            = new ActivityTestRule<>(BaseActivity.class);

    @Test
    public void buildTest(){
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File out = new File(folder, "volanteMedico.pdf");

        final CountDownLatch signal = new CountDownLatch(1);

        new VolanteMedicoBuilder("0000000001", "C001","12")
                .tipoVolanteMedico("TERCERO")
                .numeroPoliza("098123098")
                .fechaSiniestro(new Date())
                .nombreConductor("Homero Simpson")
                .nombreLesionado("Bart Simpson")
                .fechaNacimiento(new Date())
                .posicionLesionado("D")
                .tipoVehiculo("automovil")
                .vehiculo("chicharron")
                .modelo((short)2018)
                .causaSiniestro("la cagó")
                .lstCoberturas(Arrays.asList("cob1","cob2","cob3"))
                .lstLesiones(Arrays.asList("pierna","dedo","rodilla"))
                .requiereAmbulancia(true)
                .lstObjetosPerdidos(Arrays.asList("lentes","dentadura","protesis"))
                .lugarTraslado("Hospital General")
                .direccionTraslado("Calzada Mexico Tacuba")
                .numeroLesionados(2)
                .build(out)
                .subscribe(folio -> {
                    signal.countDown();
                    Log.i(TAG, "Folio: " + folio);
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.e(TAG,"error",throwable);
                    signal.countDown();
                    Assert.fail();
                });
        try {
            signal.await();
        } catch (Exception e){

        }
    }

}
