package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class VolanteNoAutoBuilderTest {

    private static final String TAG= VolanteNoAutoBuilderTest.class.getSimpleName();

    @Rule
    public ActivityTestRule<BaseActivity> activity
            = new ActivityTestRule<>(BaseActivity.class);

    @Test
    public void buildTest(){
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File out = new File(folder, "volanteNoAuto.pdf");

        final CountDownLatch signal = new CountDownLatch(1);

        new VolanteNoAutoBuilder("0000000001", "C001","10")
                .fechaSiniestro(new Date())
                .numeroPoliza("98793871")
                .estatusPoliza("Chida")
                .tipoTercero("No auto")
                .descripcionTercero("Este es el tercero")
                .nombreContratista("Contratista Perez")
                .telefono("5509809823")
                .correoElectronico("contratista@mail.com")
                .observaciones("Aqui van las observaciones")
                .areasDañadas("areasDañadas")
                .dañoConsecuencia("dañoConsecuencia")
                .build(out)
                .subscribe(folio -> {
                    signal.countDown();
                    Log.i(TAG, "Folio: " + folio);
                }, throwable -> {
                    Assert.fail();
                    signal.countDown();
                });

        try {
            signal.await();
        } catch (Exception e){

        }
    }

}
