package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.utils.media.FileUtils;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class VolanteVehiculoAseguradoBuilderTest {

    private static final String IMG_BASE64 = "iVBORw0KGgoAAAANSUhEUgAAAMYAAADGAQMAAACzVW0eAAAABlBMVEX///8AAABVwtN+AAAACXBIWXMAAA7EAAAOxAGVKw4bAAABBElEQVRYhe2WwQ3EIAwELVEAJdE6JVEAks9rQxKFe4d94EdCmHvsYXuxyIkT34YiampFm/SMj05D8M41+Q96rmOHhGRsaCvYtoVWNmJPE05LPNtkxLPtJwrVSx3sJNElLrz+65+dZJ6rE1ljJ3GlVQTuImPNQmzPxZbUxPzPFkxE4iytg+F/rXQaEl8yhGPBQ8JR3PPi0lAe4qqh1LJt8lO7ss1ATLKLzUu29xON3h3e/KqDvSRMxc5S6zWkkJAIr0fUIP4BDYnb7OKPLtlP5hQgkWe9u4SAxNSpKMO375AQXGV3zqnInAVgNkTEsw0z9j6OtJOQMXXOOUWXeXQjOXHiq/gBV8cuz+aHWIsAAAAASUVORK5CYII=";

    private static final String TAG = VolanteVehiculoAseguradoBuilderTest.class.getSimpleName();

    private static final String TEMPLATE = "documents/volante_vehiculo_asegurado.html";

    private static final String QR_PATH = "images/gnp-qr.png";

    @Rule
    public ActivityTestRule<BaseActivity> activity
            = new ActivityTestRule<>(BaseActivity.class);

    @Test
    public void buildTest() throws IOException {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File out = new File(folder, "volanteVehiculoAsegurado.pdf");

        final CountDownLatch signal = new CountDownLatch(1);

        InputStream is = FileUtils.getFile(activity.getActivity(),QR_PATH);

        String tipoIdentificacion="LC";
        new VolanteVehiculoAseguradoBuilder("0000000001", "C001")
                .fechaSiniestro(new Date())
                .horaSiniestro(new Date())
                .fechaArribo(new Date())
                .fechaEntrega(new Date())
                .estado("estado")
                .municipio("municipio")
                .calle("calle")
                .entreCalles("entreCalles")
                .circunstancia("circunstancia")
                .numeroPoliza("numeroPoliza")
                .estatusPoliza("estatusPoliza")
                .aseguradoNombre("aseguradoNombre")
                .tipoCobertura("tipoCobertura")
                .estatusCobranza("estatusCobranza")
                .vigenciaPoliza(new Date())
                .conductorNombre("conductorNombre")
                .fechaNacimiento(new Date())
                .edad("edad")
                .tipoIdentificacion(tipoIdentificacion)
                .tipoLicencia(tipoIdentificacion.equals("LC")?"A":"","Tipo Licencia:")
                .fechaFinLicencia(tipoIdentificacion.equals("LC")?"31/05/2019":"","Válida hasta:")
                .telefono("554466778899")
                .correo("correo@gmail.com")
                .tipoVehiculo("tipoVehiculo")
                .armadora("armadora")
                .carroceria("carroceria")
                .version("version")
                .modelo("modelo")
                .placas("placas")
                .vin("vin")
                .lstDanosPreexistentes(Arrays.asList("foo","foo","foo"))
                .responsable(true)
                .aplicaDeducible("aplicaDeducible")
                .razonNoDeducible("razonNoDeducible")
                .lstRecuperaciones(Arrays.asList("foo","foo","foo"))
                .proteccionLegal(true)
                .perdidaTotalEvidente(true)
                .lstDanosConsecuencia(Arrays.asList("foo","foo","foo"))
                .vehiculoSegmentado(true)
                .folioSegmentacion("folioSegmentacion")
                .volanteCondicionado("volanteCondicionado")
                .causaCondicionamiento("causaCondicionamiento")
                .tomadorDatos(true)
                .lstPiezasFaltantes(Arrays.asList("foo","foo","foo"))
                .qrInputStream(is)
                .qrBase64(IMG_BASE64)
                .build(out)
                .subscribe(folio -> {
                    signal.countDown();
                    Log.i(TAG, "Folio: " + folio);
                }, throwable -> {
                    Log.e(TAG,"error", throwable);
                    signal.countDown();
                    Assert.fail();
                });
        try {
            signal.await();
        } catch (Exception e){

        }
    }

}
