package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.os.Environment;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class VolanteVehiculoTerceroBuilderTest {

    private static final String TAG= VolanteVehiculoTerceroBuilderTest.class.getSimpleName();
    @Rule
    public ActivityTestRule<BaseActivity> activity
            = new ActivityTestRule<>(BaseActivity.class);

    @Test
    public void buildTest(){
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File out = new File(folder, "volanteVehiculoTercero.pdf");

        final CountDownLatch signal = new CountDownLatch(1);

        new VolanteVehiculoTerceroBuilder("0000000001", "C001", "08")
                .fechaSiniestro(new Date())
                .horaSiniestro(new Date())
                .fechaArribo(new Date())
                .fechaEntrega(new Date())
                .estado("estado")
                .municipio("municipio")
                .calle("calle")
                .entreCalles("entreCalles")
                .circunstancia("circunstancia")
                .otraAseguradora("otraAseguradora")
                .numeroPoliza("numeroPoliza")
                .fechaFinPoliza(new Date())
                .conductorNombre("conductorNombre")
                .tipoVehiculo("tipoVehiculo")
                .armadora("armadora")
                .carroceria("carroceria")
                .version("version")
                .placas("placas")
                .vin("vin")
                .lstDanosPreexistentes(Arrays.asList("foo","foo","foo"))
                .lstDanosConsecuencia(Arrays.asList("foo","foo","foo"))
                .vehiculoSegmentado("vehiculoSegmentado")
                .folioSegmentacion("folioSegmentacion")
                .tomadorDatos(true)
                .lstRecuperaciones(Arrays.asList("foo","foo","foo"))
                .lstPiezasFaltantes(Arrays.asList("foo","foo","foo"))
                .build(out)
                .subscribe(folio -> {
                    signal.countDown();
                    Log.i(TAG, "Folio: " + folio);
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.e(TAG,"error",throwable);
                    signal.countDown();
                    Assert.fail();
                });

        try {
            signal.await();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
