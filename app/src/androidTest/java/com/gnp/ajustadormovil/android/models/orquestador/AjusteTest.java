package com.gnp.ajustadormovil.android.models.orquestador;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Siniestro;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ubicacion;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveInfo;

import junit.framework.Assert;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Nullable;

import io.realm.Realm;

@RunWith(AndroidJUnit4.class)
public class AjusteTest {

    private static final String TAG = AjusteTest.class.getSimpleName();

    private static final String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzYS1maXJlYmFzZS1qd3RAZ25wLWFqdXN0YWRvcm1vdmlsLXFhLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwic3ViIjoic2EtZmlyZWJhc2Utand0QGducC1hanVzdGFkb3Jtb3ZpbC1xYS5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9pZGVudGl0eXRvb2xraXQuZ29vZ2xlYXBpcy5jb21cL2dvb2dsZS5pZGVudGl0eS5pZGVudGl0eXRvb2xraXQudjEuSWRlbnRpdHlUb29sa2l0IiwiaWF0IjoxNTU4NzM1NzY1LCJleHAiOjE1OTAyNzE3NjUsInByb2plY3RpZCI6ImducC1hanVzdGFkb3Jtb3ZpbC1xYSIsInVpZCI6Ik5QQUxFU1RJIiwiY2xhaW1zIjp7InJvbCI6IlBBTEVTVElOQSBGRVJFR1JJTk8iLCJuZWdvY2lvc09wZXJhYmxlcyI6IiIsImlkcGFydGljaXBhbnRlIjoiIiwibWFpbCI6Im5lc3Rvci5wYWxlc3RpbmFAZ25wLmNvbS5teCIsImFwZW1hdGVybm8iOiJGRVJFR1JJTk8iLCJnaXZlbm5hbWUiOiJORVNUT1IgTFVCSU4iLCJhcGVwYXRlcm5vIjoiUEFMRVNUSU5BIiwiY3VlbnRhYmxvcXVlYWRhIjpmYWxzZSwidGlwb3VzdWFyaW8iOiJhanVzdGFkb3JlcyIsImNlZHVsYSI6IkM2MDkiLCJyb2xlcyI6WyJBanVzdGFkb3IiXX19.nWImBh547o-k2itSEsJ2N4UgkMmm3pTsHkpLlu036KrYkw1opVJUNCB7eiHVtw_uv27Rs8vXKuJCXwxghgiUURwXk4Crbe-osnu8Cv5Hpnyx6KeFeQP_lSsPhYZOHEK8kePHsNTZZDpTZksEKElsmOv_rHytVXrqfxcwMjy7ieWSD-MJUfoYEk8oGxTfxEF8krGBbgsMrtTorGfcTJjYlf2WPcMt82HWp4EU3SDDwqmUn6A3PQHziV7WH66q9SXOTNWOAT2HWlB_aQ3RKK_001Odxhsj8LGYoOx8J6NDE5MNDBaZ9uc7QaSbtHLqsagxj7YEmjT1UaTRL0ZvRc7UCg";

    @Test
    public void addTest() throws Exception {
        print();

        String id = "1221";

        CompletableFuture<Ajuste> future = new CompletableFuture<>();
        OrquestadorFactory.getAjusteData()
                .getAjuste(id)
                .subscribe(a -> future.complete(a),
                        throwable -> future.completeExceptionally(throwable));
        Ajuste ajuste = future.get();
        Log.i(TAG, "Ajuste Obtenido: " + ajuste);

        Realm.getDefaultInstance()
                .executeTransaction(t -> t.copyToRealmOrUpdate(ajuste));

        print();
    }

    private void print() {
        Log.i(TAG, "Ajustes: ");
        List<Ajuste> results = Realm.getDefaultInstance()
                .where(Ajuste.class)
                .findAll();
        for (Ajuste result : results) {
            Log.i(TAG, result.toString());
        }
    }

    @Test
    public void getAjustesTest() throws Exception {
        AppPreferences.setString(AppPreferences.USER_INFO_TOKEN, TOKEN);
        CompletableFuture<List<Ajuste>> future = new CompletableFuture<>();
        OrquestadorFactory.getAjusteData()
                .getAjustes()
                .subscribe(result -> future.complete(result),
                        throwable -> future.completeExceptionally(throwable));
        List<Ajuste> ajustes = future.get();
        StringBuilder sb = new StringBuilder("Ajuste Obtenidos: ");
        ajustes.forEach(ajuste -> sb.append("\n\t" + ajuste));
        Log.i(TAG, sb.toString());
    }

    @Test
    public void evaluateTest(){
        Ajuste ajuste = null;
        Object value = getValueAbraham(ajuste, "siniestro.ubicacion.estado.nombre");
        Log.i(TAG, "Valor: " + value);
        Assert.assertNull(value);

        ajuste = new Ajuste();
        value = getValueAbraham(ajuste, "siniestro.ubicacion.estado.nombre");
        Log.i(TAG, "Valor: " + value);
        Assert.assertNull(value);

        ajuste.setSiniestro(new Siniestro());
        value = getValueAbraham(ajuste, "siniestro.ubicacion.estado.nombre");
        Log.i(TAG, "Valor: " + value);
        Assert.assertNull(value);

        ajuste.getSiniestro().setUbicacion(new Ubicacion());
        value = getValueAbraham(ajuste, "siniestro.ubicacion.estado.nombre");
        Log.i(TAG, "Valor: " + value);
        Assert.assertNull(value);

        ajuste.getSiniestro().getUbicacion().setEstado(new CatalogoClaveInfo());
        value = getValueAbraham(ajuste, "siniestro.ubicacion.estado.nombre");
        Log.i(TAG, "Valor: " + value);
        Assert.assertNull(value);

        ajuste.getSiniestro().getUbicacion().getEstado().setNombre("ESTADO");
        value = getValueAbraham(ajuste, "siniestro.ubicacion.estado.nombre");
        Log.i(TAG, "Valor: " + value);
        Assert.assertEquals(value, "ESTADO");
    }

    private Object getValue(@Nullable  Object target, String property){
        String root = "root";
        Expression jexlExp = new JexlEngine().createExpression(root + "." + property);
        JexlContext jc = new MapContext();
        jc.set("root", target);
        return jexlExp.evaluate(jc);
    }

    // "siniestro.ubicacion.estado.nombre"
    private Object getValueAbraham(@Nullable  Object target, String property){
        String[] properties = property.split("\\.");
        String methodName = properties[0]; // siniestro
        methodName = "get" + Character.toString(methodName.charAt(0)).toUpperCase() + methodName.substring(1); //getSiniestro
        try {
            Object value = target.getClass().getMethod(methodName).invoke(target);
            if(value != null){
                return properties.length > 1 ? getValueAbraham(value, property.substring(properties[0].length() + 1)) : value;
            } else {
                return null;
            }
        } catch(Exception e){
            String type  = target == null ? "[null]" : target.getClass().getSimpleName();
            Log.w(TAG, "Ocurrió un error al obtener la propiedad " + type + "." + methodName);
            return null;
        }
    }

}
