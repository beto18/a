package com.gnp.ajustadormovil.android.models.orquestador;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.error.DataError;
import com.gnp.ajustadormovil.android.error.FieldError;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PosicionOcupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Afectado;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;
import io.realm.RealmList;

@RunWith(AndroidJUnit4.class)
public class AseguradoTest {

    private static final String TAG = AseguradoTest.class.getSimpleName();

    private static final String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzYS1maXJlYmFzZS1qd3RAZ25wLWFqdXN0YWRvcm1vdmlsLXFhLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwic3ViIjoic2EtZmlyZWJhc2Utand0QGducC1hanVzdGFkb3Jtb3ZpbC1xYS5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9pZGVudGl0eXRvb2xraXQuZ29vZ2xlYXBpcy5jb21cL2dvb2dsZS5pZGVudGl0eS5pZGVudGl0eXRvb2xraXQudjEuSWRlbnRpdHlUb29sa2l0IiwiaWF0IjoxNTU4NzM1NzY1LCJleHAiOjE1OTAyNzE3NjUsInByb2plY3RpZCI6ImducC1hanVzdGFkb3Jtb3ZpbC1xYSIsInVpZCI6Ik5QQUxFU1RJIiwiY2xhaW1zIjp7InJvbCI6IlBBTEVTVElOQSBGRVJFR1JJTk8iLCJuZWdvY2lvc09wZXJhYmxlcyI6IiIsImlkcGFydGljaXBhbnRlIjoiIiwibWFpbCI6Im5lc3Rvci5wYWxlc3RpbmFAZ25wLmNvbS5teCIsImFwZW1hdGVybm8iOiJGRVJFR1JJTk8iLCJnaXZlbm5hbWUiOiJORVNUT1IgTFVCSU4iLCJhcGVwYXRlcm5vIjoiUEFMRVNUSU5BIiwiY3VlbnRhYmxvcXVlYWRhIjpmYWxzZSwidGlwb3VzdWFyaW8iOiJhanVzdGFkb3JlcyIsImNlZHVsYSI6IkM2MDkiLCJyb2xlcyI6WyJBanVzdGFkb3IiXX19.nWImBh547o-k2itSEsJ2N4UgkMmm3pTsHkpLlu036KrYkw1opVJUNCB7eiHVtw_uv27Rs8vXKuJCXwxghgiUURwXk4Crbe-osnu8Cv5Hpnyx6KeFeQP_lSsPhYZOHEK8kePHsNTZZDpTZksEKElsmOv_rHytVXrqfxcwMjy7ieWSD-MJUfoYEk8oGxTfxEF8krGBbgsMrtTorGfcTJjYlf2WPcMt82HWp4EU3SDDwqmUn6A3PQHziV7WH66q9SXOTNWOAT2HWlB_aQ3RKK_001Odxhsj8LGYoOx8J6NDE5MNDBaZ9uc7QaSbtHLqsagxj7YEmjT1UaTRL0ZvRc7UCg";

    @Before
    public void setup(){
        AppPreferences.setString(AppPreferences.USER_INFO_TOKEN, TOKEN);
    }

    @Test
    public void listaEmails() throws Exception{
        RealmList<String> emails = new RealmList<>();
        emails.add("correo@correo.com");
        emails.add("lopez@gmail.com");
        emails.add("jose.madero@gmail.com");
        emails.add("mariana");
        emails.add("lorenita@asj");
        emails.add("lorenita@asj");
        emails.add("lorenita@asj");
        RealmList<Telefono> telefonos = new RealmList<Telefono>();
        Telefono telefono = new Telefono();
        telefono.setTelefono("5564738893");
        telefono.setExtension("098");
        telefono.setTipo("ofi");
        telefonos.add(telefono);
        Afectado afectado = new Afectado();
        afectado.setApellidoPaterno("asd");
        afectado.setTipoPersona("dhksa");
        afectado.setTelefonos(telefonos);
        afectado.setCorreos(emails);
        try{
            DataError dataError = ErrorUtils.validate(afectado);
            if(dataError.hasErrors()){

                Log.e("ERROR_CAMPO: ", dataError.getMessage());

            }
            boolean isEmailError = false;
            for(FieldError error: dataError.getFields()){
                /*if(error.getField().equals("correos") && !isEmailError){
                    isEmailError = true;
                    Log.e("ERROR_CAMPO: ", error.getField());
                    Log.e("ERROR_MENSAJE: ", error.getMessage());
                }
                if(isEmailError && !error.getField().equals("correos")){
                    Log.e("ERROR_CAMPO: ", error.getField());
                    Log.e("ERROR_MENSAJE: ", error.getMessage());
                }*/
                Log.e("ERROR_CAMPO: ", error.getField());
                Log.e("ERROR_MENSAJE: ", error.getMessage());

            }
        }
        catch (Exception e){
            Log.e("ERROR_CAMPO: ", e.getMessage());

        }


    }
    @Test
    public void addTest() throws Exception {
        print();

        String ajuste = "1221";

        CompletableFuture<Asegurado> future = new CompletableFuture<>();
        OrquestadorFactory.getAseguradoData()
                .get(ajuste)
                .subscribe(aseg ->future.complete(aseg),
                        throwable -> future.completeExceptionally(throwable));
        Asegurado asegurado = future.get();
        Log.i(TAG, "Asegurado: " + asegurado);

        asegurado.setAjuste(ajuste);
        Realm.getDefaultInstance()
                .executeTransaction(t -> t.copyToRealmOrUpdate(asegurado));

        print();
    }

    @Test
    public void validateTest() throws Exception{

        Persona conductor = getConductor();

        Log.i(TAG, "Objecto correcto");
        printValidation(conductor);

        Log.i(TAG, "En blanco");
        conductor.setNombre(null);
        conductor.setApellidoPaterno(null);
        printValidation(conductor);

        Log.i(TAG, "Tamaño inválido");
        conductor.setNombre("P");
        conductor.setApellidoPaterno("");
        printValidation(conductor);

        Log.i(TAG, "Caracteres especiales");
        conductor.setNombre("Conductor$");
        conductor.setApellidoPaterno("Apellido!");
        printValidation(conductor);

    }

    private void print() {
        Log.i(TAG, "Asegurados: ");
        List<Asegurado> results = Realm.getDefaultInstance()
                .where(Asegurado.class)
                .findAll();
        for (Asegurado result : results) {
            Log.i(TAG, "Asegurado Obtenidos: " + result);
        }
    }

    private void printValidation(Object obj){
        DataError error = ErrorUtils.validate(obj);
        String message = error.getMessage();
        for (FieldError field : error.getFields()){
            message += "\n\t" + field.getField() + ": " + field.getMessage();
        }
        Log.i(TAG, message);
    }


    private Asegurado getAsegurado(){
        Asegurado asegurado = new Asegurado();
        asegurado.setAjuste("1");
        asegurado.setConductor(getConductor());
        asegurado.setOcupantes(new RealmList<>());
        asegurado.getOcupantes().add(getOcupante(2L));
        asegurado.getOcupantes().add(getOcupante(3L));
        return asegurado;
    }

    private Persona getConductor(){
        Persona persona = new Persona();
        persona.setApellidoMaterno("ApellidoMaterno");
        persona.setApellidoPaterno("ApellidoPaterno");
        persona.setAutorizaTratamiento(true);
        persona.setCondicion(CondicionPersona.FALLECIDO);
        persona.setCorreos(Arrays.asList("hola@gnp.com.mx", "adios@gnp.com.mx"));
        persona.setEstadoLicencia("01");
        persona.setFechaFinLicencia(new Date());
        persona.setFechaNacimiento(new Date());
        persona.setFotografias(Arrays.asList(
                getFotografia(),
                getFotografia()));
        persona.setHospital(1L);
        persona.setId(1L);
        persona.setLesiones(Arrays.asList(1L, 2L));
        persona.setNivelLesion(1L);
        persona.setNombre("Conductor Rules");
        persona.setNumeroIdentificacion("1234567890");
        return persona;
    }

    private Ocupante getOcupante(Long id){
        Ocupante ocupante = new Ocupante();
        ocupante.setApellidoMaterno("ApellidoMaterno");
        ocupante.setApellidoPaterno("ApellidoPaterno");
        ocupante.setAutorizaTratamiento(true);
        ocupante.setCondicion(CondicionPersona.FALLECIDO);
        ocupante.setCorreos(Arrays.asList("hola@gnp.com.mx", "adios@gnp.com.mx"));
        ocupante.setEstadoLicencia("01");
        ocupante.setFechaFinLicencia(new Date());
        ocupante.setFechaNacimiento(new Date());
        ocupante.setFotografias(Arrays.asList(
                getFotografia(),
                getFotografia()));
        ocupante.setHospital(1L);
        ocupante.setId(id);
        ocupante.setLesiones(Arrays.asList(1L, 2L));
        ocupante.setNivelLesion(1L);
        ocupante.setNombre("Ocupante" + id);
        ocupante.setNumeroIdentificacion("1234567890");
        ocupante.setPosicion(PosicionOcupante.ADD);
        return ocupante;
    }

    private Fotografia getFotografia() {
        try {
            Fotografia fotografia = new Fotografia();
            fotografia.setId(1L);
            fotografia.setUrl("http://foto.jpg");
            return fotografia;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Test
    public void serializerTest(){

        Ocupante ocupante = getOcupante(1L);
        Gson gson = new Gson();
        try {
            Log.i(TAG, "Valor:" + gson.toJson(ocupante));
        }catch (Exception e){
            Log.e(TAG, "Error: ", e);
        }

    }

    @Test
    public void deserializerTest(){
        String json = "{\"vehiculo\": {\"vin\": \"HHHHHHHHHHHHH1204\",\"tipoVehiculo\": \"CA4\",\"armadora\": \"FL\",\"carroceria\": \"02\",\"version\": \"01\",\"modelo\": 2003,\"placas\": \"havsks\",\"numeroMotor\": \"jhabsysa\",\"primerImpacto\": 25,\"danoConsecuencia\": true,\"danoEstimado\": 0,\"perdidaTotalEvidente\": false,\"observacion\": \"havavVs\",\"danos\": [{\"areaAfectada\": 9,\"parteAfectada\": 197},{\"areaAfectada\": 9,\"parteAfectada\": 302}],\"danosPreexistentes\": [],\"volante\": {\"volanteCondicionado\": true,\"causaCondicionamiento\": 4,\"aplicaDeducible\": true,\"requiereGrua\": false,\"vehiculoSegmentado\": false,\"impresionFisica\": true,\"envioEmail\": true,\"coberturas\": [{\"cobertura\": \"0000001289\",\"deducible\": 15000,\"sumaAsegurada\": 300000,\"reservada\": \"0000001276\",\"costoPromedio\": 13299}],\"piezasFaltantes\": [],\"url\": \"https://storage.googleapis.com/sinaut-docs/ajustadormovil-uat/2019/05/0004950234/0004950234_a_vehiculo0004950234C80803723.pdf\",\"folio\": \"0004950234C80803723\",\"dobleDeducible\": false},\"fotografias\": [],\"valorComercial\": 300000},\"conductor\": {\"id\": 3531,\"nombre\": \"ROBERTO\",\"apellidoPaterno\": \"HERNANDEZ\",\"apellidoMaterno\": \"REYES\",\"sexo\": \"F\",\"fechaNacimiento\": \"1982-05-29\",\"tipoIdentificacion\": \"C\",\"condicion\": \"LESIONADO\",\"requiereAmbulancia\": false,\"hospital\": 100008,\"nivelLesion\": 4,\"autorizaTratamiento\": true,\"numeroIdentificacion\": \"hagshdusha\",\"telefonos\": [{\"telefono\": \"7751589378\",\"tipo\": \"C\"}],\"correos\": [\"abraham.flores@gnp.com.mx\"],\"lesiones\": [11,17],\"volante\": {\"aplicaDeducible\": true,\"impresionFisica\": true,\"envioEmail\": true,\"coberturas\": [{\"cobertura\": \"0000000906\",\"deducible\": 0,\"sumaAsegurada\": 100000,\"reservada\": \"0000001280\",\"costoPromedio\": 3000}],\"url\": \"https://storage.googleapis.com/sinaut-docs/ajustadormovil-uat/2019/05/0004950234/0004950234_a_conductor_medico0004950234C808012667.pdf\",\"folio\": \"0004950234C808012667\"}},\"ocupantes\": [{\"id\": 3533,\"nombre\": \"hVahs\",\"apellidoPaterno\": \"sbabzn\",\"apellidoMaterno\": \"kVawoxba\",\"sexo\": \"M\",\"fechaNacimiento\": \"1975-05-29\",\"tipoIdentificacion\": \"C\",\"condicion\": \"LESIONADO\",\"requiereAmbulancia\": false,\"hospital\": 100008,\"nivelLesion\": 4,\"autorizaTratamiento\": true,\"numeroIdentificacion\": \"sjagshs\",\"telefonos\": [{\"telefono\": \"7751589378\",\"tipo\": \"C\"}],\"correos\": [\"abraham.flores@gnp.com.mx\"],\"objetosPerdidos\": [1,4],\"lesiones\": [16,17],\"volante\": {\"aplicaDeducible\": true,\"impresionFisica\": true,\"envioEmail\": true,\"coberturas\": [{\"cobertura\": \"0000000906\",\"deducible\": 0,\"sumaAsegurada\": 100000,\"reservada\": \"0000001280\",\"costoPromedio\": 3000}],\"url\": \"https://storage.googleapis.com/sinaut-docs/ajustadormovil-uat/2019/05/0004950234/0004950234_a_ocupante3533_medico0004950234C808013315.pdf\",\"folio\": \"0004950234C808013315\"},\"posicion\": \"ADC\"}]}";
        try {
            Asegurado asegurado = new Gson().fromJson(json, Asegurado.class);
            Log.i(TAG, "Asegurado: " + asegurado.toString());
        } catch (Exception e){
            Log.e(TAG, "Error al parsear", e);
            Assert.fail();
        }
    }

    @Test
    public void serializeTest() throws Exception {
        String ajuste = "1221";
        CountDownLatch signal = new CountDownLatch(1);

        Gson gson = new Gson();

        OrquestadorFactory.getAseguradoData()
                .get(ajuste)
                .doAfterTerminate(() -> signal.countDown())
                .subscribe(asegurado -> {

                    try {

                        Log.i(TAG, "Asegurado Servicio: \n\t" + asegurado + "\n\t" +  gson.toJson(asegurado));

                        asegurado.setAjuste(ajuste);
                        final Asegurado ase = asegurado;
                        Realm.getDefaultInstance()
                                .executeTransaction(t -> t.copyToRealmOrUpdate(ase));

                        Log.i(TAG, "Asegurado Guardado: \n\t" + asegurado + "\n\t" +  gson.toJson(asegurado));

                        asegurado = RepositoryFactory.getAseguradoRepository().findByAjuste(ajuste);

                        Log.i(TAG, "Asegurado Realm: \n\t" + asegurado + "\n\t" +  gson.toJson(asegurado));

                    } catch (Exception ex){
                        Log.e(TAG, "Error al jugar con el asegurado", ex);
                    }

                },
                throwable -> {
                    Log.e(TAG, "Error al obtener el asegurado", throwable);
                    Assert.fail();
                });
        signal.await();
    }


}
