package com.gnp.ajustadormovil.android.models.orquestador;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Dispositivo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.TipoDispositivo;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DispositivoTest {

    private static final String TAG = DispositivoTest.class.getSimpleName();


    @Test
    public void assertNulls(){
        Dispositivo dispositivo = new Dispositivo();
        printErrors(dispositivo);
    }

    @Test
    public void assertDispositivoNulls(){
        Dispositivo dispositivo = new Dispositivo();
        dispositivo.setTipo(TipoDispositivo.OTRO);
        dispositivo.setToken("lofnfkjfkjewffoieqfwweiojdiwfqe");
        printErrors(dispositivo);
    }

    private void printErrors(Object object){
        try {
            ErrorUtils.assertValues(object);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
