package com.gnp.ajustadormovil.android.models.orquestador.Responsabilidad;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.ResponsabilidadCommand;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ResponsabilidadCommandTest {
    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "RESPONSABILIDAD_COMMAND_TEST_NOTNULL");
        ResponsabilidadCommand responsabilidadCommand = new ResponsabilidadCommand();
        printErrors(responsabilidadCommand);
    }
    @Test
    public void assertNotBlank() {
        Log.i(TAG, "RESPONSABILIDAD_COMMAND_TEST_NOTBLANK");
        ResponsabilidadCommand responsabilidadCommand = new ResponsabilidadCommand();
        responsabilidadCommand.setResponsabilidadAjustador("");
        responsabilidadCommand.setTipoTercero("");
        printErrors(responsabilidadCommand);
    }
    @Test
    public void assertNotSize() {
        Log.i(TAG, "RESPONSABILIDAD_COMMAND_TEST_SIZE");
        ResponsabilidadCommand responsabilidadCommand = new ResponsabilidadCommand();
        responsabilidadCommand.setTipoTercero("12345");
        responsabilidadCommand.setResponsabilidadAjustador("12345");
        printErrors(responsabilidadCommand);
    }
    private void printErrors(ResponsabilidadCommand responsabilidadCommand) {

        try {
            ErrorUtils.assertValues(responsabilidadCommand);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
