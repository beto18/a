package com.gnp.ajustadormovil.android.models.orquestador;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroData;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;

@RunWith(AndroidJUnit4.class)
public class TerceroTest {

    private static final String TAG = TerceroTest.class.getSimpleName();

    private TerceroData data;

    @Before
    public void setup(){
        data = OrquestadorFactory.getTerceroData();
    }

    @Test
    public void getTest() throws Exception{
        final CountDownLatch signal = new CountDownLatch(1);
        data.getAll("269431")
                .subscribe(terceros ->{
                    Log.d(TAG, "Terceros encontrados" + terceros.size());
                    Assert.assertEquals(findById(terceros, 1739L).getClass(), TerceroAuto.class);
                    Assert.assertEquals(findById(terceros, 1804L).getClass(), TerceroNoAuto.class);
                    Assert.assertEquals(findById(terceros, 1741L).getClass(), TerceroPersona.class);
                    signal.countDown();
                }, throwable -> {
                    Log.e(TAG, "Error en test", throwable);
                    Assert.fail();
                    signal.countDown();
                });
        signal.await();
    }

    private Tercero findById(List<Tercero> terceros, Long id){
        for(Tercero tercero : terceros){
            if(tercero.getId().equals(id)){
                return tercero;
            }
        }
        return null;
    }

}
