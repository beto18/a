package com.gnp.ajustadormovil.android.models.orquestador.Terceros;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Afectado;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class AfectadoTest {
    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "ASEGURADO_TEST_NOTNULL");
        Afectado afectado = new Afectado();
        printErrors(afectado);
    }
    @Test
    public void assertNotBlank() {
        Log.i(TAG, "ASEGURADO_TEST_NOTBLANK");
        List<String> correos = new ArrayList<>();
        correos.add("abraham.flores@gnp.com.mx");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        Afectado afectado = new Afectado();
        afectado.setNombre("");
        afectado.setTipoPersona("");
        afectado.setApellidoPaterno("");
        afectado.setApellidoMaterno("");
        afectado.setGenero("");
        afectado.setFechaNacimiento(new Date());
        afectado.setTelefonos(telefonos);
        afectado.setCorreos(correos);
        printErrors(afectado);
    }
    @Test
    public void assertSizeMax() {
        Log.i(TAG, "ASEGURADO_TEST_SIZE");
        List<String> correos = new ArrayList<>();
        correos.add("abraham.flores@gnp.com.mx");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        Afectado afectado = new Afectado();
        afectado.setNombre("hskjdhkasldjksalñjdksaldksañdjsalkkjdklsahdklsahdklsa");
        afectado.setTipoPersona("asd");
        afectado.setApellidoPaterno("hskjdhkasldjksalñjdksaldksañdjsalkkjdklsahdklsahdklsa");
        afectado.setApellidoMaterno("hskjdhkasldjksalñjdksaldksañdjsalkkjdklsahdklsahdklsa");
        afectado.setGenero("");
        afectado.setFechaNacimiento(new Date());
        afectado.setTelefonos(telefonos);
        afectado.setCorreos(correos);
        printErrors(afectado);
    }
    @Test
    public void assertSizeMin() {
        Log.i(TAG, "ASEGURADO_TEST_SIZE");
        Afectado afectado = new Afectado();
        afectado.setNombre("asd");
        afectado.setTipoPersona("asd");
        afectado.setApellidoPaterno("a");
        afectado.setApellidoMaterno("a");
        afectado.setGenero("a");
        afectado.setFechaNacimiento(new Date());
        printErrors(afectado);
    }
    @Test
    public void assertMatchPattern() {
        Log.i(TAG, "ASEGURADO_TEST_MATCHPATTERN");
        List<String> correos = new ArrayList<>();
        correos.add("abraham.flores@gnp.com.mx");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        Afectado afectado = new Afectado();
        afectado.setNombre("Joaquí?n");
        afectado.setTipoPersona("asd");
        afectado.setApellidoPaterno("López?");
        afectado.setApellidoMaterno("González?");
        afectado.setGenero("a");
        afectado.setFechaNacimiento(new Date());
        afectado.setTelefonos(telefonos);
        afectado.setCorreos(correos);
        printErrors(afectado);
    }
    @Test
    public void assertEmail() {
        Log.i(TAG, "ASEGURADO_TEST_EMAIL");
        List<String> correos = new ArrayList<>();
        correos.add("abraham");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        Afectado afectado = new Afectado();
        afectado.setNombre("Joaquí");
        afectado.setTipoPersona("asd");
        afectado.setApellidoPaterno("López");
        afectado.setApellidoMaterno("González");
        afectado.setGenero("a");
        afectado.setFechaNacimiento(new Date());
        afectado.setTelefonos(telefonos);
        afectado.setCorreos(correos);
        printErrors(afectado);
    }
    private void printErrors(Afectado afectado) {

        try {
            ErrorUtils.assertValues(afectado);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
