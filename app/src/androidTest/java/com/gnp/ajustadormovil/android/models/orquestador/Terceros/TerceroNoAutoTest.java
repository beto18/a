package com.gnp.ajustadormovil.android.models.orquestador.Terceros;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class TerceroNoAutoTest {
    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "TERCERO_NO_AUTO_TEST_NOTNULL");
        TerceroNoAuto terceroNoAuto = new TerceroNoAuto();
        printErrors(terceroNoAuto);
    }
    @Test
    public void assertNotBlank() {
        Log.i(TAG, "TERCERO_NO_AUTO_TEST_NOTBLANK");
        TerceroNoAuto terceroNoAuto = new TerceroNoAuto();
        terceroNoAuto.setAreaDanada("");
        terceroNoAuto.setDanoPreexistente("");
        printErrors(terceroNoAuto);
    }
    @Test
    public void assertSize() {
        Log.i(TAG, "TERCERO_NO_AUTO_TEST_SIZE");
        TerceroNoAuto terceroNoAuto = new TerceroNoAuto();
        terceroNoAuto.setAreaDanada("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        terceroNoAuto.setDanoPreexistente("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        terceroNoAuto.setCalle("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        terceroNoAuto.setNumeroInterior("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgk");
        terceroNoAuto.setNumeroExterior("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        terceroNoAuto.setEntreCalles("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        terceroNoAuto.setReferencias("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        terceroNoAuto.setKilometro("dhjsakdhjsaklfhksdjlhfsdjklhfkjsdlhfkajlhfkjsdbfgbdshjfbufdsfsdfdfdfsdfsdfsdjsdilbjfsklahfjdlhsadlknsdak");
        printErrors(terceroNoAuto);
    }

    @Test
    public void assertNoAuto(){
        VolanteTerceroNoAuto volanteTerceroNoAuto = new VolanteTerceroNoAuto();
        printErrors(volanteTerceroNoAuto);
    }

    @Test
    public void assertValuesVolanteNoAuto(){
        VolanteTerceroNoAuto volanteTerceroNoAuto = new VolanteTerceroNoAuto();
        volanteTerceroNoAuto.setEnvioEmail(true);
        volanteTerceroNoAuto.setImpresionFisica(true);
        volanteTerceroNoAuto.setMonto(2000.00);
        volanteTerceroNoAuto.setNombre("Abraham flores Bautista");
        volanteTerceroNoAuto.setTelefono("00000000000");
        volanteTerceroNoAuto.setCorreo("abraham.flores.gnp.com");
        List<String> coberturas = new ArrayList<>();
        coberturas.add("000000989");
        volanteTerceroNoAuto.setObservaciones("fwiovfvjifewoiemdlkwkfvkjekfmdlkwmlqtfyughmojwef");
        volanteTerceroNoAuto.setNacionalidad("M");
        volanteTerceroNoAuto.setCoberturas(coberturas);
        volanteTerceroNoAuto.setTipoVolante("PP");
        printErrors(volanteTerceroNoAuto);
    }

    private void printErrors(Object terceroNoAuto) {

        try {
            ErrorUtils.assertValues(terceroNoAuto);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
