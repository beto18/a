package com.gnp.ajustadormovil.android.models.orquestador;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VolanteVehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;
import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Date;

@RunWith(AndroidJUnit4.class)
public class VolantesTest {

    private static final String TAG = VolantesTest.class.getSimpleName();

    private Gson gson = new Gson();

    @Test
    public void serializeVolanteMedicoTest(){
        VolanteMedico volante = new VolanteMedico();
        volante.setAplicaDeducible(false);
        volante.setRazonNoDeducible(2L);
        volante.setCoberturas(Arrays.asList("00000001", "000000002"));
        volante.setEnvioEmail(true);
        volante.setFolio("1234567890");
        volante.setImpresionFisica(true);
        try {
            String json = gson.toJson(volante);
            Log.i(TAG, "VolanteMedico: " + json);
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deserializeVolanteMedicoTest(){
        String json = "{ \"aplicaDeducible\": false, \"razonNoDeducible\": 2, \"impresionFisica\": true, \"envioEmail\": true, \"coberturas\": [ { \"cobertura\": \"0000000904\" } ], \"url\": \"https://volante.pdf\", \"folio\": \"0034661413C5065012831\" }";
        try {
            VolanteMedico volante = gson.fromJson(json, VolanteMedico.class);
            Log.i(TAG, "VolanteMedico: " + volante.toString());
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void serializeVolanteVehiculoAseguradoTest(){
        VolanteVehiculoAsegurado volante = new VolanteVehiculoAsegurado();
        volante.setAplicaDeducible(false);
        volante.setRazonNoDeducible(2L);
        volante.setCausaCondicionamiento(2L);
        volante.setFolioSegmentacion("1234567890");
        volante.setCoberturas(Arrays.asList("00000001", "000000002"));
        volante.setEnvioEmail(true);
        volante.setImpresionFisica(true);
        try {
            String json = gson.toJson(volante);
            Log.i(TAG, "VolanteVehiculoAsegurado: " + json);
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deserializeVolanteVehiculoAseguradoTest(){
        String json = "{\"aplicaDeducible\": true,\"impresionFisica\": true,\"envioEmail\": true,\"coberturas\": [{\"cobertura\": \"0000000906\",\"deducible\": 0,\"sumaAsegurada\": 200000,\"reservada\": \"0000001280\",\"costoPromedio\": 3000}],\"url\": \"https://storage.googleapis.com/sinaut-docs/ajustadormovil/2018/11/0034782276/0034782276_a_ocupante4068_medico0034782276C808013221.pdf\",\"folio\": \"0034782276C808013221\"}";
        try {
            VolanteVehiculoAsegurado volante = gson.fromJson(json, VolanteVehiculoAsegurado.class);
            Log.i(TAG, "VolanteVehiculoAsegurado: " + volante.toString());
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void serializeVolanteVehiculoTerceroTest(){
        VolanteVehiculoTercero volante = new VolanteVehiculoTercero();
        volante.setCausaCondicionamiento(2L);
        volante.setFolioSegmentacion("1234567890");
        volante.setCoberturas(Arrays.asList("00000001", "000000002"));
        volante.setEnvioEmail(true);
        volante.setImpresionFisica(true);
        volante.setCalle("Calle");
        volante.setCentroRemate(2L);
        volante.setCodigoPostal("012012");
        volante.setColonia("Colonia");
        volante.setCorreo("hola@hola.com");
        volante.setEntreCalle("EntreCalles");
        volante.setEstado("Estado");
        volante.setFechaFinPoliza(new Date());
        volante.setFolio("123456789");
        volante.setMontoProntoPago(1000.0);
        volante.setMunicipio("Municipio");
        volante.setNacionalidad("MX");
        volante.setNombre("Nombre");
        volante.setNumeroExterior("1");
        volante.setNumeroInterior("1");
        volante.setNumeroPoliza("1234567890");
        volante.setNumeroSiniestro("23456789");
        volante.setOtraAseguradora("AXXA");
        volante.setPiezasFaltantes(Arrays.asList(new PiezaVehiculo(1L,2L)));
        volante.setReferencia("Referencia");
        volante.setRequiereGrua(true);
        volante.setRfcBeneficiario("1234567890");
        volante.setSubTipoVolante("ST");
        volante.setTelefono("1234567890");
        volante.setTipoPoliza(1L);
        volante.setTipoTercero("TA");
        volante.setTipoVolante("VOL");
        volante.setTitularPoliza("Titular");
        volante.setVolanteCondicionado(false);
        try {
            String json = gson.toJson(volante);
            Log.i(TAG, "VolanteVehiculoTercero: " + json);
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deserializeVolanteVehiculoTerceroTest(){
        String json = "{\"tipoTercero\": \"TAG\",\"tipoVolante\": \"T\",\"requiereGrua\": false,\"impresionFisica\": false,\"envioEmail\": true,\"volanteCondicionado\": false,\"coberturas\": [{\"cobertura\": \"0000001273\",\"deducible\": 0,\"sumaAsegurada\": 3000000,\"reservada\": \"0000001163\",\"costoPromedio\": 6578}],\"piezasFaltantes\": [],\"url\": \"https://storage.googleapis.com/sinaut-docs/ajustadormovil/2018/11/0034772707/0034772707_ta2438_vehiculo0034772707C60907263.pdf\",\"folio\": \"0034772707C60907263\"}";
        try {
            VolanteVehiculoTercero volante = gson.fromJson(json, VolanteVehiculoTercero.class);
            Log.i(TAG, "VolanteVehiculoTercero: " + volante.toString());
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void serializeVolanteTerceroNoAutoTest(){
        VolanteTerceroNoAuto volante = new VolanteTerceroNoAuto();
        volante.setEntreCalles("EntreCalles");
        volante.setCalle("Calle");
        volante.setCoberturas(Arrays.asList("0000001", "929472874"));
        volante.setCodigoPostal("563774");
        volante.setColonia("Colonia");
        volante.setContratista(2L);
        volante.setCorreo("hola@jhola.com");
        volante.setEnvioEmail(true);
        volante.setEstado("Estado");
        volante.setExtension("2543");
        volante.setImpresionFisica(true);
        volante.setKilometro("12");
        volante.setMonto(1200.00);
        volante.setMunicipio("Municipio");
        volante.setNacionalidad("MX");
        volante.setNombre("Nombre");
        volante.setNumeroExterior("1231");
        volante.setNumeroInterior("4234");
        volante.setObservaciones("Observaciones");
        volante.setReferencias("Referencias");
        volante.setRfcBeneficiario("yfgdyast19283712");
        volante.setTelefono("27it4124");
        volante.setTipoTelefono("T");
        volante.setTipoUbicacion("CR");
        volante.setTipoVolante("VOL");
        try {
            String json = gson.toJson(volante);
            Log.i(TAG, "VolanteTerceroNoAuto: " + json);
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void deserializeVolanteTerceroNoAutoTest(){
        String json = "{\"impresionFisica\": false,\"envioEmail\": true,\"tipoVolante\": \"PP\",\"nacionalidad\": \"M\",\"monto\": 1200,\"rfcBeneficiario\": \"HGFH299186\",\"nombre\": \"Ranma\",\"observaciones\": \"queria todo su dinero\",\"calle\": \"no\",\"codigoPostal\": \"946769\",\"colonia\": \"0478\",\"municipio\": \"002\",\"estado\": \"09\",\"numeroInterior\": \"2\",\"numeroExterior\": \"2\",\"kilometro\": \"10000\",\"telefono\": \"846797997997\",\"correo\": \"dbjJk@jajn.com\",\"coberturas\": [{\"cobertura\": \"0000001273\",\"deducible\": 0,\"sumaAsegurada\": 3000000,\"reservada\": \"0000001163\",\"costoPromedio\": 11383}],\"url\": \"https://storage.googleapis.com/sinaut-docs/ajustadormovil/2019/02/0034874693/0034874693_tna2451_volante0034874693C60910771.pdf\",\"folio\": \"0034874693C60910771\"}";
        try {
            VolanteTerceroNoAuto volante = gson.fromJson(json, VolanteTerceroNoAuto.class);
            Log.i(TAG, "VolanteTerceroNoAuto: " + volante.toString());
        } catch (Exception e){
            Log.e(TAG, "Error al serializar", e);
            Assert.fail(e.getMessage());
        }
    }

}
