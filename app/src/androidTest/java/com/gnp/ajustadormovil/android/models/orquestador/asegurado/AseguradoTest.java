package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class AseguradoTest {

    private static final String TAG = AseguradoTest.class.getSimpleName();

    @Test
    public void assertValues(){
        VehiculoAsegurado vehiculoAsegurado = new VehiculoAsegurado();
        vehiculoAsegurado.setTipoVehiculo("AU");
        vehiculoAsegurado.setArmadora("AUT");
        vehiculoAsegurado.setCarroceria("CE");
        vehiculoAsegurado.setModelo(Short.parseShort("1995"));
        vehiculoAsegurado.setVersion("01");
        vehiculoAsegurado.setPlacas("9999999883838838383");
        vehiculoAsegurado.setVin("ksksdkkkkkkkkkkdksmflksmdf");
        vehiculoAsegurado.setNumeroMotor("jkfkdfkfdkdfkfdfkfldksjfjksdfjksdfds");
        vehiculoAsegurado.setObservacion("lflkdflkfdnjndkfdfgdfgfdjkfnre");
        printVehiculoErrors(vehiculoAsegurado);
    }

    @Test
    public void assertValuesVolanteMedico(){
        VolanteMedico volanteMedico = new VolanteMedico();
        volanteMedico.setAplicaDeducible(true);
        volanteMedico.setImpresionFisica(true);
        volanteMedico.setEnvioEmail(true);
        List<String> coberturas = new ArrayList<>();
        coberturas.add("000000919");
        coberturas.add("000000929");
        volanteMedico.setCoberturas(coberturas);
        printVehiculoErrors(volanteMedico);
    }

    @Test
    public void assertValuesVolanteVehiculo() {
        VolanteVehiculoAsegurado volanteVehiculoAsegurado = new VolanteVehiculoAsegurado();
        volanteVehiculoAsegurado.setAplicaDeducible(true);
        volanteVehiculoAsegurado.setRazonNoDeducible(null);
        volanteVehiculoAsegurado.setCausaCondicionamiento(null);
        volanteVehiculoAsegurado.setEnvioEmail(true);
        volanteVehiculoAsegurado.setImpresionFisica(true);
        volanteVehiculoAsegurado.setRequiereGrua(true);
        volanteVehiculoAsegurado.setVolanteCondicionado(false);
        List<String> coberturas = new ArrayList<>();
        coberturas.add("000000919");
        coberturas.add("000000919");
        volanteVehiculoAsegurado.setCoberturas(coberturas);
        printVehiculoErrors(volanteVehiculoAsegurado);
    }

    private void printVehiculoErrors(Object vehiculo){
        try {
            ErrorUtils.assertValues(vehiculo);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }

    @Test
    public void cloneTest(){
        AseguradoRepository repository = RepositoryFactory.getAseguradoRepository();
        Asegurado asegurado = repository.findByAjuste("289315");
        Asegurado rollback = repository.findByAjuste("289315");
        Assert.assertFalse(asegurado == rollback);
    }

}
