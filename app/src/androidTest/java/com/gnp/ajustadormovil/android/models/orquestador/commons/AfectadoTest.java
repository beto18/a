package com.gnp.ajustadormovil.android.models.orquestador.commons;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Afectado;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class AfectadoTest {
    private static final String TAG = AfectadoTest.class.getSimpleName();

    @Test
    public void assertNotNullsAfectado(){
        Afectado  afectado = new Afectado();
        printErrors(afectado);
    }

    @Test
    public void assertValues(){
        Afectado afectado = new Afectado();
        afectado.setNombre("dd");
        afectado.setApellidoMaterno("ww");
        afectado.setApellidoPaterno("wq");
        afectado.setFechaNacimiento(new Date());
        afectado.setGenero("M");
        List<String> correos = new ArrayList<>();
        correos.add("abraham.flores@gnp.com.mx");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        afectado.setTelefonos(telefonos);
        afectado.setCorreos(correos);
        printErrors(afectado);
    }

    private void printErrors(Afectado afectado){
        try {
            ErrorUtils.assertValues(afectado);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }

}

