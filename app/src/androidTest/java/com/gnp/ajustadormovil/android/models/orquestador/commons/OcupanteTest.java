package com.gnp.ajustadormovil.android.models.orquestador.commons;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class OcupanteTest {

    private static final String TAG = OcupanteTest.class.getSimpleName();


    @Test
    public void assertNotNullsOcupante(){
        Ocupante ocupante = new Ocupante();
        printErrors(ocupante);
    }

    @Test
    public void assertValuesOcupante() {
        List<String> correos = new ArrayList<>();
        correos.add("abraham.flores@gnp.com.mx");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        Ocupante ocupante = new Ocupante();
        ocupante.setNombre("dkkcjkxckjcxlkcxjkcxkcxljkcdmkl");
        ocupante.setApellidoPaterno("dasnxchxcmnxcnxcjncxmnxcnmxcmnxc");
        ocupante.setApellidoMaterno("csncklcmlkdmklsdlkmlkmdmskllkmdmklsd");
        ocupante.setNumeroIdentificacion("mchcjsdnjsdjndjndhsdnjsd");
        ocupante.setTipoIdentificacion("P");
        ocupante.setTipoLicencia("e");
        ocupante.setAutorizaTratamiento(true);
        ocupante.setLesiones(null);
        ocupante.setSexo("M");
        ocupante.setPosicion(null);
        ocupante.setRequiereAmbulancia(true);
        ocupante.setTelefonos(telefonos);
        ocupante.setCorreos(correos);
        ocupante.setFechaNacimiento(new Date());
        printErrors(ocupante);
    }

    private void printErrors(Ocupante afectado){
        try {
            ErrorUtils.assertValues(afectado);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
