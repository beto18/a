package com.gnp.ajustadormovil.android.models.orquestador.commons;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class PersonaTest {

    private static final String TAG = PersonaTest.class.getSimpleName();

    @Test
    public void assertNotNullTest(){
        Persona persona = new Persona();
        printErrors(persona);
    }

    @Test
    public void assertSizeTest(){
        Persona persona = new Persona();
        persona.setNombre("WW");
        persona.setApellidoMaterno("WW");
        persona.setApellidoPaterno("WW");
        persona.setSexo("M");
        persona.setAutorizaTratamiento(true);
        persona.setFechaFinLicencia(new Date());
        persona.setFechaNacimiento(new Date());
        persona.setRequiereAmbulancia(null);
        persona.setTipoIdentificacion("A");
        persona.setTipoLicencia("");
        persona.setLesiones(null);
        persona.setRequiereAmbulancia(false);
        persona.setNumeroIdentificacion("78987678987");
        List<Telefono> telefonos = new ArrayList<>();
        telefonos.add(new Telefono("7751589378","C",null));
        persona.setEstadoLicencia("A");
        persona.setTelefonos(telefonos);
        List<String> correos = new ArrayList<>();
        correos.add("abraham.flores@gnp.com.mx");
        persona.setCorreos(correos);
        // TODO: Poner los demás.
        printErrors(persona);
    }

    private void printErrors(Persona persona){
        try {
            ErrorUtils.assertValues(persona);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }

}
