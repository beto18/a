package com.gnp.ajustadormovil.android.models.orquestador.commons;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class TelefonoTest {

    private static final String TAG = TelefonoTest.class.getSimpleName();

    @Test
    public void assertNullValues(){
        Telefono telefono = new Telefono();
        printErrors(telefono);
    }

    @Test
    public void  assetValuesTelefono(){
        Telefono telefono = new Telefono();
        telefono.setTelefono("9999999999");
        telefono.setTipo("C");
        telefono.setExtension("777");
        printErrors(telefono);
    }

    private void printErrors(Telefono telefono){
        try {
            ErrorUtils.assertValues(telefono);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }

}
