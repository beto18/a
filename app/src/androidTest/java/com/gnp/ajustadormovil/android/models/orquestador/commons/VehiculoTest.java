package com.gnp.ajustadormovil.android.models.orquestador.commons;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class VehiculoTest {

    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "VEHICULO_TEST_NOTNULL");
        VehiculoTercero vehiculoTercero = new VehiculoTercero();
        printErrors(vehiculoTercero);
    }
    @Test
    public void assertSize(){
        Log.i(TAG, "VEHICULO_TEST_SIZE");
        VehiculoTercero vehiculoTercero = new VehiculoTercero();
        vehiculoTercero.setVin("dasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdk");
        vehiculoTercero.setPlacas("dasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdk");
        vehiculoTercero.setNumeroMotor("dasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdk");
        vehiculoTercero.setObservacion("dasudhjsasadadsadklfhsdljkhflkjsdhfljksdhfkalshjfsdkdasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdkdasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdkdasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdkdasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdkdasudhjsaklfhsdljkhflkjsdhfljksdhfkalshjfsdk");
        printErrors(vehiculoTercero);
    }
    private void printErrors(VehiculoTercero vehiculoTercero) {

        try {
            ErrorUtils.assertValues(vehiculoTercero);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }

}

