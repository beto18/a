package com.gnp.ajustadormovil.android.models.orquestador.commons;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class VolanteMedicoTest {

    private static final String TAG = VolanteMedicoTest.class.getSimpleName();

    @Test
    public void assertNullValues(){
        VolanteMedico volanteMedico = new VolanteMedico();
        printErrors(volanteMedico);
    }

    @Test
    public void assertValuesVolanteMedico(){
        VolanteMedico volanteMedico = new VolanteMedico();
        volanteMedico.setAplicaDeducible(false);
        volanteMedico.setEnvioEmail(true);
        volanteMedico.setImpresionFisica(true);
        volanteMedico.setRazonNoDeducible(2L);
        printErrors(volanteMedico);
    }

    private void printErrors(VolanteMedico medico){
        try {
            ErrorUtils.assertValues(medico);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }

}
