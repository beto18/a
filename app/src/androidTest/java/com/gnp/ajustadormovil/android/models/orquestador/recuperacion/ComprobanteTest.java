package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoTest;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

@RunWith(AndroidJUnit4.class)
public class ComprobanteTest {
    private static final String TAG = AfectadoTest.class.getSimpleName();
    @Test
    public void assertNotNullsComprobante(){
        Log.i(TAG, "COMPROBANTE_TEST_NULL");
        Comprobante  comprobante = new Comprobante();
        printErrors(comprobante);
    }
    @Test
    public void assertValuesNotBlank(){
        Log.i(TAG, "COMPROBANTE_TEST_NOTBLANK");
        Comprobante comprobante = new Comprobante();
        comprobante.setRecibimosDe("");
        comprobante.setDeducible(false);
        comprobante.setMontoAcordado(0.0);
        comprobante.setMontoRecibido(0.0);
        comprobante.setFechaPromesa(new Date());
        comprobante.setObservaciones("");
        comprobante.setImporteTotal(0.0);
        comprobante.setImporteTotalLetra("");
        comprobante.setCodigoPostal("");
        comprobante.setMunicipio("");
        comprobante.setEstado("");
        comprobante.setColonia("");
        comprobante.setCalle("");
        comprobante.setNumeroInterior("");
        comprobante.setNumeroExterior("");
        comprobante.setEntreCalles("");
        comprobante.setTelefono("");
        comprobante.setUrl("");
        printErrors(comprobante);


    }
    @Test
    public void assertValuesMinMax(){
        Log.i(TAG, "COMPROBANTE_TEST_MIN_MAX");
        Comprobante comprobante = new Comprobante();
        comprobante.setRecibimosDe("asdf");
        comprobante.setDeducible(false);
        comprobante.setMontoAcordado(99999999999999.99);
        comprobante.setMontoRecibido(99999999999999.99);
        comprobante.setFechaPromesa(new Date());
        comprobante.setObservaciones("asd");
        comprobante.setImporteTotal(999999999999999.99);
        comprobante.setImporteTotalLetra("asdf");
        comprobante.setCodigoPostal("12312");
        comprobante.setMunicipio("asd");
        comprobante.setEstado("asd");
        comprobante.setColonia("asdf");
        comprobante.setCalle("asdf");
        comprobante.setNumeroInterior("1");
        comprobante.setNumeroExterior("1");
        comprobante.setEntreCalles("as");
        comprobante.setTelefono("asdaa");
        comprobante.setUrl("dsadad");
        printErrors(comprobante);


    }
    @Test
    public void assertValuesSize(){
        Log.i(TAG, "COMPROBANTE_TEST_SIZE");
        Comprobante comprobante = new Comprobante();
        comprobante.setRecibimosDe("adhsakjdjksaldjksaljdklsdfhjdfkgbhdjfbkasñldjksaljdklsdfhkjdlfhiowjrifnfjsdkfnkjdsbfjkdbkjanksljakldskjfhsdkjhfksdf");
        comprobante.setDeducible(false);
        comprobante.setMontoAcordado(0.0);
        comprobante.setMontoRecibido(0.0);
        comprobante.setFechaPromesa(new Date());
        comprobante.setObservaciones("adhsakjdjksaldjksaljdklsdfhjdfkgbhdjfbkasñldjksaljdklsdfhkjdlfhiowjrifnfjsdkfnkjdsbfjkdbkjanksljakldskjfhsdkjhfksdf");
        comprobante.setImporteTotal(9.0);
        comprobante.setImporteTotalLetra("adhsakjdjksaldjksaljdklsdfhjdfkgbhdjfbkasñldjksaljdklsdfhkjdlfhiowjrifnfjsdkfnkjdsbfjkdbkjanksljakldskjfhsdkjhfksdf");
        comprobante.setCodigoPostal("12312");
        comprobante.setMunicipio("asd");
        comprobante.setEstado("asd");
        comprobante.setColonia("asdf");
        comprobante.setCalle("adhsakjdjksaldjksaljdklsdfhjdfkgbhdjfbkasñldjksaljdklsdfhkjdlfhiowjrifnfj");
        comprobante.setNumeroInterior("12345678901234567890");
        comprobante.setNumeroExterior("12345678901234567890");
        comprobante.setEntreCalles("adhsakjdjksaldjksaljdklsdfhjdfkgbhdjfbkasñldjksaljdklsdfhkjdlfhiowjrifnfjsdkfnkjdsbfjkdbkjanksljakldskjfhsdkjhfksdf");
        comprobante.setTelefono("123456789012345678901234567890");
        comprobante.setUrl("dsadad");
        printErrors(comprobante);


    }
    private void printErrors(Comprobante comprobante){
        try {
            ErrorUtils.assertValues(comprobante);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
