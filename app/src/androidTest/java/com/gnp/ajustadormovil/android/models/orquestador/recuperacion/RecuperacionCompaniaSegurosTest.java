package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoTest;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

@RunWith(AndroidJUnit4.class)
public class RecuperacionCompaniaSegurosTest {
    private static final String TAG = AfectadoTest.class.getSimpleName();
    @Test
    public void assertNotNullsRecuperacionCompaniaSeguros(){
        Log.i(TAG, "RECUPERACION_COMPANIA_SEGUROS_TEST_NULL");
        RecuperacionCompaniaSeguros  recuperacionCompaniaSeguros = new RecuperacionCompaniaSeguros();
        printErrors(recuperacionCompaniaSeguros);
    }

    @Test
    public void assertNotBlank(){
        Log.i(TAG, "RECUPERACION_COMPANIA_SEGUROS_TEST_NOTBLANK");
        RecuperacionCompaniaSeguros companiaSeguros = new RecuperacionCompaniaSeguros();
        companiaSeguros.setAseguradora("");
        companiaSeguros.setOtraAseguradora("");
        companiaSeguros.setNumeroPoliza("");
        companiaSeguros.setTipoCobertura("");
        companiaSeguros.setVigenciaPoliza(new Date());
        companiaSeguros.setTitularPoliza("");
        companiaSeguros.setNumeroSiniestro("");
        companiaSeguros.setFolioAdmision("");
        companiaSeguros.setTipoVolante("");
        companiaSeguros.setNombreAjustador("");
        companiaSeguros.setDanoEstimado(0.0);
        printErrors(companiaSeguros);

    }
    @Test
    public void assertSize(){
        Log.i(TAG, "RECUPERACION_COMPANIA_SEGUROS_TEST_SIZE");
        RecuperacionCompaniaSeguros companiaSeguros = new RecuperacionCompaniaSeguros();
        companiaSeguros.setAseguradora("asd");
        companiaSeguros.setOtraAseguradora("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setNumeroPoliza("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setTipoCobertura("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setVigenciaPoliza(new Date());
        companiaSeguros.setTitularPoliza("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setNumeroSiniestro("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setFolioAdmision("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setTipoVolante("sf");
        companiaSeguros.setNombreAjustador("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
        companiaSeguros.setDanoEstimado(0.0);
        printErrors(companiaSeguros);

    }
    private void printErrors(RecuperacionCompaniaSeguros recuperacionCompaniaSeguros){
        try {
            ErrorUtils.assertValues(recuperacionCompaniaSeguros);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
