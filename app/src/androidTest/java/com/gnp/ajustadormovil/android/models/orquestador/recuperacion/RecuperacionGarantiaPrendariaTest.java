package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

@RunWith(AndroidJUnit4.class)
public class RecuperacionGarantiaPrendariaTest {
    private  final String TAG = this.getClass().getSimpleName();
    @Test
    public void assertNotNull(){
        Log.i(TAG, "RECUPERACION_GARANTIA_PRENDARIA_TEST_NOTNULL");
        RecuperacionGarantiaPrendaria recuperacionGarantiaPrendaria = new RecuperacionGarantiaPrendaria();
        printErrors(recuperacionGarantiaPrendaria);
    }

    @Test
    public void assertvaluesNotBlank(){
        Log.i(TAG, "RECUPERACION_GARANTIA_PRENDARIA_TEST_NOTBLANK");
        RecuperacionGarantiaPrendaria recuperacionGarantiaPrendaria = new RecuperacionGarantiaPrendaria();
        recuperacionGarantiaPrendaria.setMismoVehiculo(false);
        recuperacionGarantiaPrendaria.setTipoVehiculo("");
        recuperacionGarantiaPrendaria.setArmadoraVehiculo("");
        recuperacionGarantiaPrendaria.setCarroceriaVehiculo("");
        recuperacionGarantiaPrendaria.setVersionVehiculo("");
        recuperacionGarantiaPrendaria.setModeloVehiculo(null);
        recuperacionGarantiaPrendaria.setPlacasVehiculo("");
        recuperacionGarantiaPrendaria.setVinVehiculo("");
        recuperacionGarantiaPrendaria.setNumeroMotor("");
        recuperacionGarantiaPrendaria.setNumeroFactura("");
        recuperacionGarantiaPrendaria.setRecibidoDe("");
        recuperacionGarantiaPrendaria.setVencimiento(null);
        recuperacionGarantiaPrendaria.setNumeroContrato("");
        recuperacionGarantiaPrendaria.setFolioGrua("");
        recuperacionGarantiaPrendaria.setDocumentoPropiedad("");
        recuperacionGarantiaPrendaria.setCodigoPostal("");
        recuperacionGarantiaPrendaria.setEstado("");
        recuperacionGarantiaPrendaria.setMunicipio("");
        recuperacionGarantiaPrendaria.setColonia("");
        recuperacionGarantiaPrendaria.setCalle("");
        recuperacionGarantiaPrendaria.setNumeroExterior("");
        recuperacionGarantiaPrendaria.setNumeroInterior("");
        recuperacionGarantiaPrendaria.setReferencia("");
        recuperacionGarantiaPrendaria.setFolioDocumento("");
        printErrors(recuperacionGarantiaPrendaria);

    }
    @Test
    public void assertvaluesSize(){
        Log.i(TAG, "RECUPERACION_GARANTIA_PRENDARIA_TEST_SIZE");
        RecuperacionGarantiaPrendaria recuperacionGarantiaPrendaria = new RecuperacionGarantiaPrendaria();
        recuperacionGarantiaPrendaria.setMismoVehiculo(false);
        recuperacionGarantiaPrendaria.setTipoVehiculo("asd");
        recuperacionGarantiaPrendaria.setArmadoraVehiculo("asd");
        recuperacionGarantiaPrendaria.setCarroceriaVehiculo("asd");
        recuperacionGarantiaPrendaria.setVersionVehiculo("asd");
        recuperacionGarantiaPrendaria.setModeloVehiculo(null);
        recuperacionGarantiaPrendaria.setPlacasVehiculo("12345678901234");
        recuperacionGarantiaPrendaria.setVinVehiculo("1235678901234567890123");
        recuperacionGarantiaPrendaria.setNumeroMotor("123456789012345678901234567890");
        recuperacionGarantiaPrendaria.setNumeroFactura("123456789012345678901234567890");
        recuperacionGarantiaPrendaria.setRecibidoDe("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456787997896767");
        recuperacionGarantiaPrendaria.setVencimiento(new Date());
        recuperacionGarantiaPrendaria.setNumeroContrato("1234567890123");
        recuperacionGarantiaPrendaria.setFolioGrua("1234567890123");
        recuperacionGarantiaPrendaria.setDocumentoPropiedad("1234567");
        recuperacionGarantiaPrendaria.setCodigoPostal("892138");
        recuperacionGarantiaPrendaria.setEstado("ASD");
        recuperacionGarantiaPrendaria.setMunicipio("ASD");
        recuperacionGarantiaPrendaria.setColonia("ASD");
        recuperacionGarantiaPrendaria.setCalle("1234567890123123456789012312345678901231234567890123");
        recuperacionGarantiaPrendaria.setNumeroExterior("1234567890123");
        recuperacionGarantiaPrendaria.setNumeroInterior("1234567890123");
        recuperacionGarantiaPrendaria.setReferencia("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456787997896767");
        recuperacionGarantiaPrendaria.setFolioDocumento("1234567890123123456789012312345678901231234567890123");
        printErrors(recuperacionGarantiaPrendaria);

    }
    @Test
    public void assertvaluesMatchPattern(){
        Log.i(TAG, "RECUPERACION_GARANTIA_PRENDARIA_TEST_MATCHPATTERN");
        RecuperacionGarantiaPrendaria recuperacionGarantiaPrendaria = new RecuperacionGarantiaPrendaria();
        recuperacionGarantiaPrendaria.setMismoVehiculo(false);
        recuperacionGarantiaPrendaria.setTipoVehiculo("asd");
        recuperacionGarantiaPrendaria.setArmadoraVehiculo("asd");
        recuperacionGarantiaPrendaria.setCarroceriaVehiculo("asd");
        recuperacionGarantiaPrendaria.setVersionVehiculo("asd");
        recuperacionGarantiaPrendaria.setModeloVehiculo(null);
        recuperacionGarantiaPrendaria.setPlacasVehiculo("asd");
        recuperacionGarantiaPrendaria.setVinVehiculo("asd");
        recuperacionGarantiaPrendaria.setNumeroMotor("asd");
        recuperacionGarantiaPrendaria.setNumeroFactura("asd");
        recuperacionGarantiaPrendaria.setRecibidoDe("asd");
        recuperacionGarantiaPrendaria.setVencimiento(new Date());
        recuperacionGarantiaPrendaria.setNumeroContrato("asd");
        recuperacionGarantiaPrendaria.setFolioGrua("asd");
        recuperacionGarantiaPrendaria.setDocumentoPropiedad("asd");
        recuperacionGarantiaPrendaria.setCodigoPostal("asdfgh");
        recuperacionGarantiaPrendaria.setEstado("ASD");
        recuperacionGarantiaPrendaria.setMunicipio("ASD");
        recuperacionGarantiaPrendaria.setColonia("ASD");
        recuperacionGarantiaPrendaria.setCalle("asd");
        recuperacionGarantiaPrendaria.setNumeroExterior("asd");
        recuperacionGarantiaPrendaria.setNumeroInterior("asd");
        recuperacionGarantiaPrendaria.setReferencia("asd");
        recuperacionGarantiaPrendaria.setFolioDocumento("asd");
        printErrors(recuperacionGarantiaPrendaria);

    }
    private void printErrors(RecuperacionGarantiaPrendaria recuperacionGarantiaPrendaria){
        try {
            ErrorUtils.assertValues(recuperacionGarantiaPrendaria);
        }catch (DataException e){
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
