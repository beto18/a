package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class RecuperacionMesaControversiaTest {

    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "RECUPERACION_MESA_CONTROVERSIA_TEST_NOTNULL");
        RecuperacionMesaControversia recuperacionMesaControversia = new RecuperacionMesaControversia();
        printErrors(recuperacionMesaControversia);
    }
    @Test
    public void assertSize(){
        RecuperacionMesaControversia recuperacionMesaControversia = new RecuperacionMesaControversia();
        Log.i(TAG, "RECUPERACION_MESA_CONTROVERSIA_TEST_SIZE");
        recuperacionMesaControversia.setNombreAbogado("dsjkaldjsldfheoiñwhifncfjdsklfnmkalsfnkjñafjgkñehfioñwfhiñewfnckjsdnfjñkewhfdweñnjcsdlksadjlsakdjklsad");
        recuperacionMesaControversia.setDespachoAbogado("dsjkaldjsldfheoiñwhifncfjdsklfnmkalsfnkjñafjgkñehfioñwfhiñewfnckjsdnfjñkewhfdweñnjcsdlksadjlsakdjklsad");
        recuperacionMesaControversia.setObservaciones("dsjkaldjsldfheoiñwhifncfjdsklfnmkalsfnkjñafjgkñehfioñwfhiñewfnckjsdnfjñkewhfdweñnjcsdlksadjlsakdjklsad");
        printErrors(recuperacionMesaControversia);
    }

    private void printErrors(RecuperacionMesaControversia recuperacionMesaControversia) {
        try {
            ErrorUtils.assertValues(recuperacionMesaControversia);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
