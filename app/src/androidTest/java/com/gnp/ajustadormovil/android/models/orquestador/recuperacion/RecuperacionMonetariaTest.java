package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class RecuperacionMonetariaTest {
    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "RECUPERACION_MONETARIA_TEST_NOTNULL");
        RecuperacionMonetaria recuperacionMonetaria = new RecuperacionMonetaria();
        printErrors(recuperacionMonetaria);
    }
    @Test
    public void assertNotBlank(){
        Log.i(TAG, "RECUPERACION_MONETARIA_TEST_NOTBLANK");
        RecuperacionMonetaria recuperacionMonetaria = new RecuperacionMonetaria();
        recuperacionMonetaria.setMoneda("");
        printErrors(recuperacionMonetaria);
    }
    @Test
    public void assertSize(){
        Log.i(TAG, "RECUPERACION_MONETARIA_TEST_SIZE");
        RecuperacionMonetaria recuperacionMonetaria = new RecuperacionMonetaria();
        recuperacionMonetaria.setMoneda("asd");
        recuperacionMonetaria.setNumeroAutorizacion("asashdkjashdklsahddasdd");
        recuperacionMonetaria.setFolioComprobante("asashdkjashdklsahddasdd");
        recuperacionMonetaria.setTipoRecuperacionMonetaria(9999999999L);
        printErrors(recuperacionMonetaria);
    }
    private void printErrors(RecuperacionMonetaria recuperacionMonetaria) {
        try {
            ErrorUtils.assertValues(recuperacionMonetaria);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
