package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class RecuperacionTurnadoAsistenciaLegalTest {
    private final String TAG = this.getClass().getSimpleName();

    @Test
    public void assertNotNull() {
        Log.i(TAG, "RECUPERACION_TURNADO_ASISTENCIA_TEST_NOTNULL");
        RecuperacionTurnadoAsistenciaLegal recuperacionTurnadoAsistenciaLegal = new RecuperacionTurnadoAsistenciaLegal();
        printErrors(recuperacionTurnadoAsistenciaLegal);
    }

    @Test
    public void assertSize(){
        Log.i(TAG, "RECUPERACION_TURNADO_ASISTENCIA_TEST_SIZE");
        RecuperacionTurnadoAsistenciaLegal recuperacionTurnadoAsistenciaLegal = new RecuperacionTurnadoAsistenciaLegal();
        recuperacionTurnadoAsistenciaLegal.setObservaciones("sdklajdlsajfkdslhfnkjdlsfhksdlfjklajfdlÑAJFIDSHFOISDNFDSNKLFSDKFJKLSDJdsfadsfsasfdsfdsfsdfsdfsdfaFKLDSJFKL");
        recuperacionTurnadoAsistenciaLegal.setAseguradora("asd");
        printErrors(recuperacionTurnadoAsistenciaLegal);
    }
    private void printErrors(RecuperacionTurnadoAsistenciaLegal recuperacionTurnadoAsistenciaLegal) {

        try {
            ErrorUtils.assertValues(recuperacionTurnadoAsistenciaLegal);
        } catch (DataException e) {
            Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(e));
        }
    }
}
