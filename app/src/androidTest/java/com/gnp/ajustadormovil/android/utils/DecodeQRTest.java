package com.gnp.ajustadormovil.android.utils;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.utils.codec.DecodeEngine;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DecodeQRTest {
    private String TAG = "TEST_AMIS";
    @Test
    public void decodeEncodeModulo1Test(){
        Log.i(TAG, "MODULO 1, DATOS DEL SINIESTRO");
        String value = "AXA123456789ABC120419130956H3A|1|2XLBkszwdie11VuEmaNS4ZoJn2pQffy80BtLz0vjpDzHmaPanDvOOXCyRPh1PqOIqbYlzDNXHzzDNlYEIfmsFR7hSka1cmhx5+FwdHUlHhVFUR2DYhujbsdRztrFCoN2ytoenbn3waJLPyGsH3RAKZM0LmK0bIuTQldUipZs0PxKQ56jcmxzAWt5R1xkQ2dFK75ij8/do64K4ecYc2Y67o8q1X9j05xbA+l0q6OoqMz0mahkRtCuiIx9a5xKQRR0jTV+4DrixzC3P4qS4pybw3CVniLZWOIlvleuFmVH+swXAgY7JQXlQatI42CWLKl+atR0yR7EaNBrEp7vvCx+WdXlDxKyb3SnOpB95ceFW0TArLcWtdVglWYTD4YnYnoSujiWXK7/cz4GK82PD0jvpDhf9XrmV1NGcq+LidKMkOYLgqu5qBTN7SAoct8rxvXaQGQiU2cuxwhEPyF2suTxzaQigntlsKYl56cWHSIfhdf4uWAIKgSuf1y0k19XIQendigyWKYYz50PUmcu7qngRpfbLWSTQbxQruPv/EK9LsvkG9hhIEz4ady2lcIyaE4xWjdvbZDBFuH3KOuUdciuWS//rZXOJ7PPa5be3ny5ZmrqKdZ6o9duszUXsSTPdVCuLSCEOwFuFgaSVerZYzs5b9KdwspyWmUb1c2+VxYfkvaL5vBp7gqAIciWa9hcvuwdNYxFZNAcPSxJt/5MhuRPD0k8DOQWaq37eaI2IuF+g3xK5XEQYT73WVLg58Gp6nlFunHlbTXhdkpy9NfmkkQNPx6r0vDjHH9CqkV56XQ9+ZNL9b04w24sZbyCqUnFHSnzbGcnU4pUjRa9cKU6nocW4zOgu2B28AjK9fsnZKOFQrs+bpcvEauXylmaCnaRxgyaBL7w9YzWX6EMEAUY9jQ/ZXpf8QshYNt3zRk9Gz7rkREpzHvlsqxBcbSHRFQO4yQ2pzxpodj6u2Pfk/Xb5HeGZF/7whjdX+oedVlZpy97wTwjMmdUelyZ+zsjkbSCL+QJLCApL4hZCrMz7PbQCdyY/+7unKbDlNxVDQoYUH1kcGzPO1EpgGq9xo5KENAdlqkYmkmEqL/uroHxORa85/ReY4LyR7JEcPC6CZXC9YQY/8XmleJsxNPjDWwBMoMgMFFDwxhP/GvAMXxZHFF7izaM4w==";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 1);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo2Test(){
        Log.i(TAG, "MODULO 2, DATOS DE LA PÓLIZA");
        String value = "AXA123456789ABC120419130956H3A|2|3RDqZ8laLreJEujISnc54YlA7yuqFx2bmmDNeX0FWZjxYqO+FMxMqnkGPr4nRVXzk/mcLWuh1rta6VfxchvMSubl++Qhh58g0ULRspM5lcGZBfT2w1tg+1qAIMYfZU9d5sIzQCBCbx0N8MTB7HbRs21FlPVLd/SWtwS78XV+CgOD1CzxmtsfIZ5Co8Z7iq7UVOp6JEfKqFcDe2+aAZOwGcRFo+eNxX5aNrXdW5KGXmxq9S+0YXJxiUM+rEAftfqfaQUQeESAVMEQALGEpnsihWqVU73/Ps+tN2WRMLBVkaYcAnfhzwhasqRXiEj4a+lgUpf1DHaYqaRR+JeOTN9NsL6O9BwwYnmdUCiDtdOFt+CLkMpsMy16QTlnVLafpAYFS+IgagDkaB+NB5YBC3J52WZl/LtLB+AXJFNgV9wZyK7l6qzqP+5z7v4w8pGh29h6TxMDPQzghQNrflkUoNZGuDhwdTZ++85we96o/4GqfJb3YcZQ3+xCYR1cBGZpRcN+1SimoK8iW3ljYHFP5AkIk8ZI4Ye95DcX6u1LPAi+JMXOoti+B9QBLeKuXdd9EAb15lF6kVrC9nu9gTcX7M3tcUlJuhH2wvfEC+XQ+tMWVDChOCvN3skryNEIgoVcNODEgBDYKHoIPe8APb9iI9swI33a2/Tt4xqw1nYOjXVn1Dncn4h9O28KRfmaG+UrJWOysOUfuUvXgXIA6HOa4Z7rCYt4sP2+WZ5KB73qW3u+s0pLAmzbKZvRpkvGBtjE7F9cJpvzmeTpVQiZVT778L9d+7BETZzF5+8mkIVRzjKJr/FZdgDz64Hiy8UvDre6XAsC9ikgJH2L9K39yuTd9cH/kg==";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 2);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo3Test(){
        Log.i(TAG, "MODULO 3, DATOS DEL ASEGURADO");
        String value = "AXA123456789ABC120419130956H3A|3|fkMXi7f5DPNFHro+9L6xEx24BN8Q6yZDTPxAZ5ytHG125H+IXhzJZVqbyo7v1jnvTY5IVilkeGk0D7ed9J2baDqYJKgb4iT8MEupyMslpNJcadjmk0ea8o75cQWmVVeTFUaLNemNAsbmbUd7enhKp8QdLGf6eqHEp2c1wheR6vBrdCXevN10wR9as0RmpsA6kzoStdTWidllqjgqVaHBf87ZcEwpBOifbHHfy3qygqF/1350ixPZZiPTYNpRTehiuuL/8sHz65gLNs4pEKnrMg2TKnoEat9qq8jfLBUXGhpo8RrOCz/Cgme7cffEqEwqa7jJ47uwBW0DFOM+KuEX8gIISwVVr65BVlHjuSrgJIsU/edknW/6WblNMw65epM928zewdwe2SymrTUNApCSl+Q7CAWcFS6HW9cC/YVZTo6kr+kQKRiDO9ibXeVFC4WkiqecVGbnyAVf3D1ievevn4N4XV5yaf703dM+RNSJANs=";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 3);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo5Test(){
        Log.i(TAG, "MODULO 5, DATOS DEL VEHICULO");
        String value = "AXA123456789ABC120419130956H3A|5|7zP3ey1bozUgJJ46jdMq6Lb4xcyICSLKPpz4kvmd6cNX6nx5p63DDJLfKdshb9TS9xnFcNchXzgAm3DyCGqe5aX57fBRk2sElBnEOuw4Ap2mPZW985UDyFnGvdVuBl5gOiIHCbHxVwRdf7pTwp7YXI0QkAKSGpduc30ISQqhsyp93xI+1qr/FWWWGWdiy+Urz8T+KKPRwzv6WkCf227WoDA+x2AulqTvLwBpPI5WJBU2+rQf6bqudgj7HisoJ/d3pnRzreqDobYhE4hF/QhsZpqx62mduzF91EoDcuVYKalBsb+wjNrcTp0tFXRurIT2jiHnGySNB4Y4KRMToblG2PUz4wvQfxGxQADWtnjxAmSvfwuF4fVtSZbRKC6a/1keyW9glYxunTQARxMUJ4y8PdWO3qT5+sbGVKt0p1ksm8OhH2JSEOKnQqw1IS0CZE9y";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 5);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo6Test(){
        Log.i(TAG, "MODULO 6, DATOS GENERALES");
        String value = "AXA123456789ABC120419130956H3A|6|0zShW3zXRVCQZQliGQ8SLD7jplBncWgrtC5tuezk1b8h+MEqf10ZD9CBL/QUu87Ir0XknLurhA5NvXmrAlF/mVZrrk9jFNu7NqqrRcCblOAzUl7LxqFyz+lohQ0zHIUfZNPmKXN58zxZZfDYkM0zTuQZajc4VHXAmartYE3bt7tHI6HSypMN0p742/x7NusCGSBgtIcF03feU4OxzFDeJbCTiDOIi4LxZCD+uw8Rlj2eOgzQQNSgldGJwQkIHoq4gKSZmjDjXlFO42apnyx/oT/kpNDXQt+SUlj8fDxCQ3pieZtPlL1KO0Cs77y/Ivxy1FXotuZ2FxIttKlJlsrgEkedKANV6rVStwPhqtfL/X4qDbUGaeWskF4nkgVYGJSxBs5gu5VYaVUvKvUIQSMzGEkKy2g8iLrIByQ3SQamPfSUQkAfb9Mo00JxTr825mwJsWUjEZNs3SaUYUioyLX0o3qEEGPSivJegg+kMIqh+T69ycYGJQlVpoIp5NAyK6fQtOSKU2NPsIL4rpezvTZsVlDUr3KTigD2c/0kw1icpxxc9hcoIyEB5JJf5sCjtdLP2qLOgdfuhoA9nPIk6V2VjD3gS/BDwfhpBkmosc9hr22OpJ2OSLmCxTYCRZP99Gq3JFNANzMcxRxXCoFUg2o2PdXkshlICx8qLcbfAwyoG3o4u7yVUbqVNMI279pyVvvjKW3AWk3ZLZLEhCa3JKzJmRaxZimmPIwQKTChSNXb4lcetnyF9mx7+jahuUs3fDiROb3FrncPyDgwvCvywSK3cBhVqkc3T5g6vTuZvB+jDHmHOYUpXU7YF+JnyROyUW+eqTDmMV8+ptuFnKs9rWtrharggpPTpXElIxWID8COHBa5Ue+4T1nqvD0DbOPxZV5dK0KZJ6u7KHbTwKEkjvnuEQHx57+6yMEelL1jbckNKxuQum0Z+PNkw0x1DQFRyP7nTsEGjtLAXjJ9sFUlvjojobFLeBioPN/O5T4pCI1QmOKLewjnNNI5BvqSHN14MrUS3qasqkavjov73qOkEieaZpoIcByNKKkoWxgm7SoKHEAWQDnBYd+1CXXOIlxU7HUoXWR6pSOnJkaQxnnv6hkvQ5qrvZZs5YXSRrscEvUCZoEw8XcV8/xy/mFBs5amKV1j0VsXw1aJzBkucbfe4Tf3guaB4visCi/oLS4L62RKM9/P/aOgagjBcRYb3thE0PR2taVJBtB2497Feg7HGagQ1PXm0dPr/zpFJzZvwFvNF1Z1rKvpd3xNg2/88iJtboJG1PN+AMIyH10qVfcrm9jqsqzdwIzXANEzvDHlyS0XD1UyqoaVgqZ69qxKFq9PQcuS2/hBZoBW8WFWjmnGQ2pkYIAyTAqvWXC7uvgvvNfy+zLfE1tBsZRuWGhF/gIo0xU/6R8DtCW1Sv+7WNzrcLZpo6Uv6xfR/o0zEwFE4GRhqMkuHL0PqJhrpUsQlhCkqe8BoMqmA6faFz3YGHU3ywaMaubC/+r8uQKj/McCno7B8iXRex0bo+XT29xp5ChG49Ij8yV6X5P6BNwmNiCdmpxf01ZBrF8tRLunj+wxg/JZO49ae9LHKeg4sgvKTOvbv50IHM7dju7sOAHQW+1bPWuU6QsxgNSOSBQMD7W2lyk94UvXIDEAGJhP0L5hdDLWCzU660aLYO9IdtEbPQ4bIP80Gg==";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 6);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo7Test(){
        Log.i(TAG, "MODULO 7, CIRCUNSTANCIAS");
        String value = "AXA123456789ABC120419130956H3A|7|DvWd/43yhEa8qYHGDVqpC/3Tr/sqn4p1UuBs90fykLz214XyxCf9N8a/9t1WmVak+wv5YXV0lBKJ1dzA9mT41qq3QISgQpaLw9wjoQq0aKZ7RI9mJdUYH9UT6ENmK1j7HT5ErphdXND0xAL20cLj1fw4faTAsUpXL+TUgCnnz0gapziILt+HxLmaJK/8yfIf1ce/OfSfadBA3/t6cBL2b6iCiC1jO70i7rBIMSrKd3yTnD2iog4VZQPEc5Vc3IequzwM89Ijp87NzCRX4CNvGF3t8xwDf7L9R8vZO5nU4rRuJ8pKI7ZGajs1onnEJcmH";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 7);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo9Test(){
        Log.i(TAG, "MODULO 8, CROQUIS ACCIDENTE");
        String value = "AXA123456789ABC120419130956H3A|9|0zShW3zXRVCQZQliGQ8SLD7jplBncWgrtC5tuezk1b8h+MEqf10ZD9CBL/QUu87Ijk4OVTYD/NGCmkFl4sfSzDhyJmYXDkxVZUTg6ZiI4CkFlwFZFzt7WIdSYTOuIysvqTjSt5CnGYcNk2YNXgx4MOE0GbtwvvfVVfNTNwEiNdI=";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 9);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo19Test(){
        Log.i(TAG, "MODULO 19, DATOS GENERALES");
        String value = "AXA123456789ABC120419130956H3A|19|gWTBBXBj2uQTJJnwOGVtaEIKbnKRAuhJPO8TPosuLcr5YDo9QAWpdpvQMs4tXCDgvbwt0hjeb2OaTZZNispZC4gghxkpJ4363rjKeRfg3qIMDRb6GLVYfn8HLDOz6zWXPW6scJDwyHqa/dobSfp1zWl4AK4unfaTROnnRvMT+2inAQ2glyDOTD8v16fS6VQjus8hzpVJUoq/sDI7d4C4pgmLudUnIzD8KUH6xFUyA6unxld/pzC9AaC5JyfEQQ0Yyz20P4YNG2q436q9MxJgEUqc7nKWHBywsHndofwRBSM43ogfN8+bX2c9MVIYS9gCfGqgSpD674n7YWvYg3aspmDXeX51y/OseUXel/xqQvpTI3fT+6jYZ8hotAIKOvFGVLQW+wkZkp88p4aof/WJVqye9SrxrXsMhTO8Y+IA2d099SOuTZ1bcLkRqVQCGA6gwsZXE9scLxQCm+aoshpLk4fK3XxbDUjQBz527WRXnagmRmQ5VaogxZsyd7+UX0/BdKlRBHoTPJiA3KV9EodhshdxdBY2TPZbYUwMq/v4wSLDUbF0GmCteYmdEpKAtl1Cueh2OIArLKbR4Ib9+qrnyxyIEU0srcYroWmQvuDkoQRWRXxvqDKa4K1TLtoCelNMwRVzMQS4lLT9Nxb+vWHqzijI4lrQrd57uht+BTZ9SvzNU9zVjW4NE321iaeP487auSLqR+PswmT8HErxFPNXWfCh80/RNrmTWOE9pn7AG8rvWOX6LDVY0SeQh+gzajxz2Tv9rEbmRfj/eHion5HmBiIKAy06qcnXxbfC9Ok8UOz3d0rkLiUMfPUjE9NGlsISMEzhQNrIQVlLK6mALiDpsl54Y3cI23tFcWqbIiBXWnPnjDroIIOQDWz35dDjs04xvvCficYxj4aEsPW6nwOiWO18TK/FDsPKSqaOKZUg0P5Onu9knLMb0cC7+Ewd1yb5k6iB/DoqyApG+hUpUU4rYQ==";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 19);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo20Test(){
        Log.i(TAG, "MODULO 20, DATOS GENERALES");
        String value = "AXA123456789ABC120419130956H3A|20|DBlKh3bF6oK98DlFIKSrrZW5sG2BbOdWJPo6sLjprCHrrGvyMo7jByb3ZeTrhvRabUHhI9msGgBPV7FTudkfOwu3B6K40S5uXGYr4hFsj+nHG0Ps8R87QnEvpxA4fVwwnwtF04OqeDDAZPTcX1/ePV46ca0iQx1V+CIPcuNh3mxnPtFO05osYVD5EyTZOVzdDsmQO7KhSYcmZHsMIVz1CD3cW+vA2I2x5e//gwOnVEi6XLabVgIMJRI/Uk3F3uvzqlf/qKU79kClpcVPnoqvGE2eg/2Eq2hqsAotFqjUjNEhx4KEYoCJjnP51edpVjxCLEIO7ByIbt2Q4j1k+RT5yvpKl3XEPq7Kg04F6lG31qXZyyH1rMiIb9Xc967Ad/X64FAHw4cH+3avMci0yccmJ0zsCCKpEbg0xOSqaZCV2/xPiEsL7OkB1qfFWbmjlGzfnXZf1vYy/T68bAoJoyodmfoO1rNjufjfR8lKl5sXFLrFMcOjtRk/f9x/TylSQ+RO2v2Qy0p+bq20W38rOh9mQXy8ns7DOvSyu1ZM/wfGkr+mUzggNF0WrlEM4IpXwQS6YmN0v2ecep6Yzd3BrR1AfTvhf8E2ibsAcYJxnn/W/lXNXQb9VUSMRGjMPb9huQOBZlh+/Df0IPNIm3Mn4UhIvBuBtLtCmGqTf+28ChLjAqL3w+nHF1Zq4gOVOO9Dz05qiOxaBddl7aa5iENI4yJCf5KEw0v2nEtFZEwdKXK3sxFTvLXK/j+lv1sHlLE7aMKwH9Clj6lEaQTNzGms05PvoIMO/byxV0A9MZdJ6POO0cTBj+pabW7f4tS491PIqQDLiTQR7kOL5qkAMqnK/8vmc3TGo+tT2xT4yiqD7vBFhbc=";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 20);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo21Test(){
        Log.i(TAG, "MODULO 21, PASE MEDICO");
        String value = "AXA123456789ABC120419130956H3A|21|+yByK6frLDehWfSm+sCG9L7yMmOQefhkqU0lvk5+gGgWQmlK07Q+p8Jc5aDjHqZ40/U/SYyNnHUUYoMo84pjo7cnNg2XlsddqxsRU3Nr5fFi7Sieu7kmh58hr0c0sOODBj8O/1bXC1/orKem8V7ECVzdl3QSxii5ofeZHEk7VvqsZrHlz4VBG7Te3uyI8OUjKJ3xH4QKUIgJlibJrQIEGDySt20j0MQS4RZO5UaNxJov/2URuacchpRV9BjpB7uO38FCA73Hv9m1JYfnUm+72i3U1V1kGT+sNA8lny6ZRxnWuiMDb4SFNXw/3YAG3Y51FIlbbXUGplrcnNX2DtnNVQCSQRNYU7tDeQayT2eE1OEtJZgSBc6yeaItgMsBM4ZQMYz+p5MN4HF7RvKpjolK5dzZuWJ3mYwXCyFTTQx83NKmqY/qZXg1BEwgSSsIILuXIBxg7kVYX3XJEY+G8lDnlDx84VF+rGOL5RpGYkRAEoQ=";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 21);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }
    @Test
    public void decodeEncodeModulo22Test(){
        Log.i(TAG, "MODULO 22, NARRATIVA");
        String value = "AXA123456789ABC120419130956H3A|22|sxcrIFzqo69S460R+SFG70tGemtW94V3iqi3VhaZ7ihso28Rmhe6rovqs689spr3TVPWAc2Ef7Z4mYiUmPEY5VHzljnfW8jTmYmK/BtI5dy0swf55rjHRhOl0XbixGxgKs3UhedEIkv1lt2+dhwTeS+gKdtj7RatuUzviiB+HvkdokIhBbXUd9ZoBdFnNGPAKVK2eyWhuGnfl2Un+xWr17DORX7SzSV+FbBaf7OH0ILP0JzMIlK8fDkT+ciXECfdLa9EotZDmrL9rpVPTIlF3OmVPbsJbT0qROpiJTvsEwD2jCn+vRA8UjcRp51mgOIR/DV3boax71QKB8L/CtcuRA==";
        String [] parts = value.split("\\|");
        String folio = parts[0];
        Assert.assertEquals(folio, "AXA123456789ABC120419130956H3A");
        int modulo = Integer.parseInt(parts[1]);
        Assert.assertEquals(modulo, 22);
        String json = DecodeEngine.decode(folio, parts[2]);
        Log.i("TEST", json);
        String encode = DecodeEngine.encode(folio, json).replaceAll("\n", "");
        Assert.assertEquals(encode, parts[2]);
        Assert.assertEquals((folio + "|" + modulo + "|" + encode), value);
    }






}
