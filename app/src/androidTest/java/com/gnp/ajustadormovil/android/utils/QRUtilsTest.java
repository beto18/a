package com.gnp.ajustadormovil.android.utils;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.FirmaDigital;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.codec.DecodeEngine;
import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;

@RunWith(AndroidJUnit4.class)
public class QRUtilsTest {

    @Test
    public void generateBase64Test(){

    }
    @Test
    public void decrypTest(){
        String value = "ANA12345AN130919164807KXA|15|qAdE0mki1EEAMjdijl5zA8iP77IA0F8qAzKwCPT1ET+q/9x6K1Jcw19b8KDjHHPne7EmaCCKYX20Sv/CvGaGUT0Ss+Dg7lv+PNnm267T7ilc0SfwTRd+wWpEuDPhnHBzGpOaKz7PEaZ8IHgYwGdEweLAgqvgFf1m/HSzEEV9MxN9lZ0EcDMsN9cEIRec57r7MsdGT9Tq24FdV+XHksFhdBjQaOgJZCnt/dQGg1XzjSKSX76olJ/onQU0qf1l6K7pI/x7q80RTqkWR4uyzP5tTentDD2w/MYVIUsq6h7tRAHZYz4sgOehaM+Cypnbo8y9JdEsxOg8gj/0xf7bfYEQtXej84pO5zgpc48SOC3lJQ3sUiLuLxA4rIG9DKOZYs4OQ87yj5EhclYaA9jMtYfm0XzyVf7anjJpciI2oZhlkVvYDoUY02gfFLdMnZuu5X9ebxR+Q8GLXNivsZQR725TLnr5NDeMfb0yvRK7MAjcBUjlU5tti0KyUPcVx2rB2tcBgQIFlEkqfMKgiHq26d2IMf0k39a9gfXLcSMQlB42bJBrlxUV9sF7i0m4izONQ3lsRUz8ocZHbX2YUg8Ki+RD1eeIwP4IbVWe5chpIs11rq+XoR4iKVpnb8TxqBPPzAYmqQ5KNJaHqfZTlHizCIrBK1WKFPyfF+9XYRDr4OetadU=";
        String[] parts = value.split("\\|");
        String json = DecodeEngine.decode(parts[0], parts[2]);
        FirmaDigital firmaDigital = new Gson().fromJson(json, FirmaDigital.class);
        Log.i("idAseguradora", firmaDigital.getIdAseguradora());
        Log.i("valeDigital", firmaDigital.getValeDigital());
        Log.i("firmaDigital", firmaDigital.getFirmaDigital());
        Log.d("DESENCRIPTADO", json);

        try {
            PublicKey publicKey = DecodeEngine.getPublicKeyFromBase64(firmaDigital.getFirmaDigital());
            try {
               boolean bool =  DecodeEngine.verifySignature(publicKey, firmaDigital.getValeDigital(), firmaDigital.getFirmaDigital());
                Log.d("DESENCRIPTADO",String.valueOf(bool));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }



    }


    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();
    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();
    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();


    @Test
    public void generarFirmaDigial(){
        String ajuste = "289634";
        String folioAmis = "GNPC4345091019114743I9A";

        try {
            generarFirmaDigital(ajuste);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    private String generarValeDigital(String ajuste) {
        Ajuste a = ajusteRepository.findById(ajuste);
        Amis amis = amisRepository.findByAjuste(ajuste);
        FolioContraparteAmis contraparte = null;
        StringBuilder valeDigital = new StringBuilder();

        if (amis != null) {
            TerceroAuto terceroAuto = terceroAutoRepository.findByAjusteAndFolioAmis(ajuste,amis.getFolio().getFolio());

            for (FolioContraparteAmis c : amis.getContrapartes()) {
                if (c.getFolio().equals(terceroAuto.getFolioAmis())) {
                    contraparte = c;
                    break;
                }
            }

            if (contraparte != null) {

                valeDigital.append(amis.getFolio().getFolio());
                valeDigital.append(contraparte.getFolio().substring(contraparte.getFolio().length() - 1));
                valeDigital.append("|");
                valeDigital.append(a.getSiniestro().getIdSiniestro());
                valeDigital.append("|");
                valeDigital.append(contraparte.getSiniestro());
                valeDigital.append("|");
                valeDigital.append(Constants.AMIS_ID_GNP);
                valeDigital.append("|");
                valeDigital.append(contraparte.getCompania());
                valeDigital.append("|");
                valeDigital.append(contraparte.getFechaAtencion());
                valeDigital.append("|");
                valeDigital.append(a.getSiniestro().getPoliza().getNumero());
                valeDigital.append("|");
                valeDigital.append(contraparte.getPoliza());
                valeDigital.append("|");
                valeDigital.append("XXXXX");
                valeDigital.append("|");
                valeDigital.append("xxxxaf");
                valeDigital.append("|");
                valeDigital.append(contraparte.getFolioPaseMedico());
            }
        }

        return valeDigital.toString();
    }



    private FirmaDigital generarFirmaDigital(String ajuste) throws GeneralSecurityException, UnsupportedEncodingException {

        Ajuste a = ajusteRepository.findById(ajuste);
        FirmaDigital firmaDigitalAmis = new FirmaDigital();
        firmaDigitalAmis.setIdAseguradora("31");
        firmaDigitalAmis.setValeDigital(generarValeDigital(ajuste));

        String stringBase64 = DecodeEngine.encode(a.getSiniestro().getFolioAmis(),generarValeDigital(ajuste));
        PrivateKey privateKey = DecodeEngine.getPrivateKeyFromBase64(stringBase64);
        String firmaDigital = DecodeEngine.sign(privateKey,generarValeDigital(ajuste));
        firmaDigitalAmis.setFirmaDigital(firmaDigital);


        Log.i("Firma digital", firmaDigitalAmis.getValeDigital());
        Log.i("Firma digital", firmaDigitalAmis.getIdAseguradora());
        Log.i("Firma digital", firmaDigitalAmis.getFirmaDigital());

        return firmaDigitalAmis;
    }




}
