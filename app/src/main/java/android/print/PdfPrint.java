package android.print;

import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;

import java.io.File;
import java.io.IOException;

import io.reactivex.Completable;

public class PdfPrint {

    private final PrintAttributes printAttributes;

    public PdfPrint(PrintAttributes printAttributes) {
        this.printAttributes = printAttributes;
    }

    public Completable print(final PrintDocumentAdapter printAdapter, File output) {
        return Completable.create(e ->
                printAdapter.onLayout(null, printAttributes, null, new PrintDocumentAdapter.LayoutResultCallback() {
                    @Override
                    public void onLayoutFinished(PrintDocumentInfo info, boolean changed) {
                        printAdapter.onWrite(new PageRange[]{PageRange.ALL_PAGES}, getOutputFile(output) , new CancellationSignal(), new PrintDocumentAdapter.WriteResultCallback() {

                            @Override
                            public void onWriteFinished(PageRange[] pages) {
                                super.onWriteFinished(pages);
                                e.onComplete();
                            }

                            @Override
                            public void onWriteFailed(CharSequence error) {
                                e.tryOnError(new IOException(error.toString()));
                            }

                        });
                    }
                }, null));
    }

    private ParcelFileDescriptor getOutputFile(File file) {
        try {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            return ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_WRITE);
        }catch (IOException e){
            return null;
        }
    }

}
