package com.gnp.ajustadormovil.android;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by tixmedia on 3/14/18.
 */

public class AjustadorMovilApplication extends android.app.Application  {

    private static final String TAG = AjustadorMovilApplication.class.getCanonicalName();

    @SuppressLint("StaticFieldLeak")
    private static Application instance;

    public static Application getInstance() {
        return instance;
    }

    public static String getToken() {
        return AppPreferences.getString(AppPreferences.USER_INFO_TOKEN);
    }

    public static String getCedula() {
        return AppPreferences.getString(AppPreferences.USER_ID_CEDULA);
    }

    public static String getValue(int mString) {
        return getInstance().getString(mString);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Log.d(TAG, "Crashlytics iniciado");
        instance = this;
        Log.d(TAG, "Instancia de aplicación iniciada");
        initRealmConfig();
        Log.d(TAG, "Realm iniciado");
        initStheto();
        Configuration cfg = new Configuration();
        cfg.setLocale(Locale.forLanguageTag("es-MX"));
        this.getApplicationContext().createConfigurationContext(cfg);
    }

    private void initStheto() {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                    Stetho.newInitializerBuilder(this)
                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                            .enableWebKitInspector(
                                    RealmInspectorModulesProvider.builder(this).build()
                            )
                            .build());
        }
    }

    public void initRealmConfig() {
        Realm.init(this);
        Realm.setDefaultConfiguration(getRealmConfig());
    }

    public RealmConfiguration getRealmConfig() {
        int newVersion = 1;
        return new RealmConfiguration.Builder()
                .name("adjuster.realm")
                .schemaVersion(newVersion)
                .deleteRealmIfMigrationNeeded()
                .build();
    }



    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
