package com.gnp.ajustadormovil.android.aspects;

import android.util.Log;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AspectInvocationHandler implements InvocationHandler {

    private static final String TAG = AspectInvocationHandler.class.getSimpleName();

    private final Object instance;

    private final Map<Method, List<Interceptor<?>>> interceptors;

    public AspectInvocationHandler(Class<?> type, Object instance){
        this.instance = instance;
        this.interceptors = new HashMap<>();
        Method [] methods = type.getMethods();
        for(Method method : methods){
            List<Interceptor<?>> interceptors = getInterceptors(method);
            if(interceptors != null) {
                this.interceptors.put(method, interceptors);
            }
        }
    }

    private static List<Interceptor<?>> getInterceptors(Method method) {
        List<Interceptor<?>> interceptors = new ArrayList<>();
        Annotation[] annotations = method.getAnnotations();
        for(Annotation annotation : annotations){
            try {
                Aspect aspect = annotation.annotationType().getAnnotation(Aspect.class);
                if (aspect != null) {
                    Interceptor<?> interceptor = aspect.interceptor().newInstance();
                    interceptor.setAnnotation(annotation);
                    interceptors.add(interceptor);
                }
            } catch (Exception e){
                Log.e(TAG, String.format("Error al obtener los interceptores de método %s.%s",
                        method.getDeclaringClass().getName(), method.getName()), e);
            }
        }
        return interceptors.isEmpty() ? null : interceptors;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        List<Interceptor<?>> interceptors = this.interceptors.get(method);

        if(interceptors != null) {
            for (Interceptor<?> interceptor : interceptors) {
                try {
                    Log.v(TAG, String.format("Ejecutando interceptor %s BEFORE %s.%s",
                            interceptor.getName(), method.getDeclaringClass().getName(), method.getName()));
                    interceptor.before(instance, method, args);
                } catch (Exception e) {
                    Log.e(TAG, String.format("Error interceptor %s BEFORE %s.%s",
                            interceptor.getName(), method.getDeclaringClass().getName(), method.getName()), e);
                }
            }
        }

        Object returnValue = method.invoke(instance, args);

        if(interceptors != null) {
            for (Interceptor<?> interceptor : interceptors) {
                try {
                    Log.v(TAG, String.format("Ejecutando interceptor %s AFTER %s.%s",
                            interceptor.getName(), method.getDeclaringClass().getName(), method.getName()));
                    returnValue = interceptor.after(instance, method, returnValue);
                } catch (Exception e) {
                    Log.e(TAG, String.format("Error interceptor %s AFTER %s.%s",
                            interceptor.getName(), method.getDeclaringClass().getName(), method.getName()), e);
                }
            }
        }

        return returnValue;
    }

}
