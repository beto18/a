package com.gnp.ajustadormovil.android.aspects;

import java.lang.reflect.Proxy;

/**
 * Utileria para uso de Programación Orientada a Aspectos (AOP)
 */
public final class Aspects {

    private Aspects(){}

    @SuppressWarnings("unchecked")
    public static <T> T aspect(Class<T> type, T implementation){
        return (T) Proxy.newProxyInstance(type.getClassLoader(),
                new Class<?>[]{ type },
                new AspectInvocationHandler(type, implementation));
    }

}
