package com.gnp.ajustadormovil.android.aspects;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Interceptor de aspecto.
 */
public abstract class Interceptor<AspectAnnotation extends Annotation> {

    /**
     * Anotación.
     */
    private AspectAnnotation annotation;

    @SuppressWarnings("unchecked")
    public final void setAnnotation(Annotation annotation){
        this.annotation = (AspectAnnotation) annotation;
    }

    public final String getName(){
        return this.annotation.annotationType().getSimpleName();
    }

    /**
     * Ejecución antes del método aspectado.
     */
    public final void before(Object target, Method method, Object[] args){
        before(target, method, args, annotation);
    }

    /**
     * Ejecución despúes del método aspectado.
     * @param returnValue Valor de retorno del método aspectado.
     * @return Valor de retorno.
     */
    public final Object after(Object target, Method method,  Object returnValue){
        return after(target, method, returnValue, annotation);
    }

    /**
     * Ejecución antes del método aspectado.
     * @param annotation Anotación.
     */
    protected void before(Object target, Method method, Object[] args, AspectAnnotation annotation){}

    /**
     * Ejecución antes del retorno de valor.
     * @param annotation Anotación.
     * @param returnValue Valor de retorno del método aspectado.
     * @return Valor de retorno.
     */
    protected Object after(Object target, Method method, Object returnValue, AspectAnnotation annotation){
        return returnValue;
    }

}

