package com.gnp.ajustadormovil.android.aspects;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Aplica los schedulers en <code>subscribeOn</code> y <code>observeOn</code>
 * de un {@link io.reactivex.Observable} o un {@link io.reactivex.Completable}
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
@Aspect(interceptor = RxApplySchedulersInterceptor.class)
public @interface RxApplySchedulers {

    /**
     * Si se desea habilidar la funcionalidad. Valor por defecto <code>true</code>
     */
    boolean enabled() default true;

}
