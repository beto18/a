package com.gnp.ajustadormovil.android.aspects;

import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.lang.reflect.Method;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
* Interceptor para mostrar un {@link LoadingIndicator}
* al suscribirse a un {@link Observable} o a un {@link Completable}
* y lo oculta al terminar su ejecución.
*/
public class RxApplySchedulersInterceptor extends Interceptor<RxApplySchedulers> {

    @Override
    public Object after(Object target, Method method, Object returnValue, RxApplySchedulers annotation) {
        if(annotation.enabled()){
            if(returnValue instanceof Observable){
                return ((Observable<?>)returnValue)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if(returnValue instanceof Completable) {
                return ((Completable)returnValue)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            } else if(returnValue instanceof Single) {
                return ((Single<?>)returnValue)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        }
        return returnValue;
    }

}
