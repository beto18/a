package com.gnp.ajustadormovil.android.aspects;

import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Muestra un {@link LoadingIndicator}
 * al suscribirse a un {@link io.reactivex.Observable} o a un {@link io.reactivex.Completable}
 * y lo oculta al terminar su ejecución.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
@Aspect(interceptor = RxLoadingInterceptor.class)
public @interface RxLoading {

    /**
     * Si se desea habilidar la funcionalidad. Valor por defecto <code>true</code>
     */
    boolean enabled() default true;

    /**
     * Si se desea que se muestre un mensaje personalizado. El valor por default es el nombre de la clase junto con el método.
     */
    String message() default "";


}
