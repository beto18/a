package com.gnp.ajustadormovil.android.aspects;

import android.util.Log;

import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.lang.reflect.Method;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
* Interceptor para mostrar un {@link LoadingIndicator}
* al suscribirse a un {@link io.reactivex.Observable} o a un {@link io.reactivex.Completable}
* y lo oculta al terminar su ejecución.
*/
public class RxLoadingInterceptor extends Interceptor<RxLoading> {

    private static final String TAG = RxLoadingInterceptor.class.getSimpleName();

    @Override
    public Object after(Object proxy, Method method, Object returnValue, RxLoading annotation) {
        if(annotation.enabled()){
            String code = annotation.message();
            if("".equals(code)) {
                 code = method.getDeclaringClass().getName() + "." + method.getName();
            }
            final String message = ResourceUtils.getString(code).equals(code) ? null : ResourceUtils.getString(code);
            Consumer<? super Disposable> start = it -> {
                Log.v(TAG, "Start LoadingIndicator: " + proxy.getClass().getCanonicalName() + "." + method.getName());
                LoadingIndicator.start();
                if(message != null) {
                    LoadingIndicator.message(message);
                }
            };
            Action finish = () -> {
                Log.v(TAG, "Finish LoadingIndicator: " + proxy.getClass().getCanonicalName() + "." + method.getName());
                LoadingIndicator.finish();
            };
            if(returnValue instanceof Observable){
                return ((Observable<?>)returnValue)
                        .doOnSubscribe(start)
                        .doFinally(finish);
            } else if(returnValue instanceof Completable) {
                return ((Completable)returnValue)
                        .doOnSubscribe(start)
                        .doFinally(finish);
            } else if(returnValue instanceof Single){
                return ((Single<?>)returnValue)
                        .doOnSubscribe(start)
                        .doFinally(finish);
            }
        }
        return returnValue;
    }

}
