package com.gnp.ajustadormovil.android.data;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gnp.ajustadormovil.android.models.Servicios;
import com.gnp.ajustadormovil.android.models.services.ServicioResponse;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by tixmedia on 3/14/18.
 *
 */

public class AppPreferences {

    public static List<ServicioResponse> listServices = null;

    public static List<ServicioResponse> getListServices() {
        return listServices;
    }
    public static  List<ServicioResponse> setListServices( List<ServicioResponse> listS) {
         listServices = listS;
         return listServices;
    }



    public static final String USER_INFO_TOKEN= "USER_INFO_TOKEN";
    public static final String USER_INFO_REFRESH_TOKEN= "USER_INFO_REFRESH_TOKEN";

    public static final String USER_ID_CEDULA = "USER_ID_CEDULA";
    public static final String USER_ID_CORREO = "CORREO_ELECTRONICO";
    public static final String USER_STATUS = "USER_STATUS";
    public static final String FCM_TOKEN = "FCM_TOKEN";
    public static final String GCM_TOKEN = "GCM_TOKEN";
    public static final String FIRST_TIME_CAT = "FIRST_TIME_CAT";


    public static final String JSON_ADJUSTER = "JSON_ADJUSTER";

    public static final String LOGUEO_PRIMERA_VEZ = "LOGUEO_PRIMERA_VEZ";
    public static final String IMPRESORA_AJUSTE = "IMPRESORA";

    //hgzavala - 17042018 - Temporal, mientras se implementa realm
    public static final String JSON_AJUSTES = "JSON_AJUSTES";
    public static final String JSON_AJUSTE_DETALLE = "JSON_AJUSTE_DETALLE";
    public static final String TIPO_UBICACION = "TIPO_UBICACION";

    //hgzavala - 25042018 - Guarda las acciones sobre los ajustes, para utilizar en la cancelación de Cabina
    public static final String JSON_AJUSTES_OPERACIONES="JSON_AJUSTES_OPERACIONES";

    //hgzavala - 26042018 - Guarda el id del ajuste que se esta trabajando Actualmente. (Abierto a Mejora - Refactor)
    //Para utilizar en el Cierre y generación del Status de la atención del Ajuste
    public static final String ID_AJUSTE_ACTUAL="ID_AJUSTE_ACTIVO";

    //hgzavala - 26042018 - Variables para guardar el status de cada sección
    public static final String ESTATUS_DECLARACION="ESTATUS_DECLARACION";
    public static final String ESTATUS_ASEGURADO="ESTATUS_ASEGURADO";
    public static final String ESTATUS_TERCEROS_AUTO ="ESTATUS_TERCEROS_AUTO";
    public static final String ESTATUS_TERCEROS_NO_AUTO ="ESTATUS_TERCEROS_NO_AUTO";
    public static final String ESTATUS_TERCEROS_PEATON ="ESTATUS_TERCEROS_PEATON";
    public static final String ESTATUS_RECUPERACIONES ="ESTATUS_RECUPERACIONES";
    public static final String ESTATUS_SERVICIOS_COBERTURA="ESTATUS_SERVICIOS_COBERTURA";
    public static final String ESTATUS_RESPONSABILIDAD="ESTATUS_RESPONSABILIDAD";
    public static final String GEOCERCA = "GEOCERCA_CONFIRMACION";
    public static final String UBICACION_PETICION = "UBICACION_PETICION";

    public static final String ESTIMADO_DE_DANO_ASEGURADO =  "ESTIMADO_DE_DANO_ASEGURADO";

    public static final String PRIVATE_AVISO = "AVISO_PRIVACIDAD_ASEGURADO";
    public static final String PRIVATE_PEATON = "AVISO_PRIVACIDAD_PEATON";
    public static final String PRIVATE_AVISO_OCUPANTE = "AVISO_PRIVACIDAD_OCUPANTE";
    public static final String PRIVATE_DECLARACION = "URL_DECLARACION_FOTO";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String TIPODEUBICACION = "CARRETERA";
    public static final String DATA_AJUSTADOR = "DATOS_AJUSTADOR";

    //PERCISTENCIA DE SERVICIOS
    public static final String LISTASERVICIOS = "SERVICIOS";

    private static SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(AjustadorMovilApplication.getInstance());
    }

    public static void setString(String preference_name, String value) {
        final SharedPreferences.Editor prefEditor = getSharedPreferences().edit();
        prefEditor.putString(preference_name, value);
        prefEditor.apply();
    }


    public static void saveListaServicios(Servicios servicios){
        SharedPreferences.Editor edit = getSharedPreferences().edit();
        String json = new Gson().toJson(servicios);
        edit.putString(LISTASERVICIOS,json);
        edit.commit();
    }

    public Servicios getServicios() {
        String json = getSharedPreferences().getString(LISTASERVICIOS,null);
        if(json == null){
            return new Servicios();
        }
        Servicios servicios = new Gson().fromJson(json, Servicios.class);
        return servicios;
    }


    public static String getString(String preference_name) {
        final SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getString(preference_name, "");
    }

    public static void setBoolean(String preferences_name,Boolean value){
        final SharedPreferences.Editor prefEditor = getSharedPreferences().edit();
        prefEditor.putBoolean(preferences_name,value);
        prefEditor.apply();
    }

    public static Boolean getBoolean(String key){
        final SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getBoolean(key,false);
    }

    public static String getString(String preference_name, String default_value) {
        final SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getString(preference_name, default_value);
    }

    public static void cleanAll() {
        final SharedPreferences.Editor prefEditor = getSharedPreferences().edit();
        prefEditor.clear();
        prefEditor.apply();
    }

    public static void removeString(String jsonAjuste) {
        final SharedPreferences.Editor prefEditor = getSharedPreferences().edit();
        prefEditor.remove(jsonAjuste);
        prefEditor.apply();
        prefEditor.commit();
    }

    public static void cleanAjustes(String preferences){

        SharedPreferences.Editor spreferencesEditor = getSharedPreferences().edit();
        spreferencesEditor.remove(preferences); //we are removing prodId by key
        spreferencesEditor.apply();


    }
}
