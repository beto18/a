package com.gnp.ajustadormovil.android.data;

import java.io.Serializable;
import java.util.List;

/**
 * Clase para implementación de repositorio.
 * @param <T> Tipo de objeto del repositorio.
 * @param <K> Tipo de la llave primaria del repositorio.
 */
public interface Repository<T, K extends Serializable> {

    /**
     * Guarda un objeto en el repositorio.
     * @param entity Entidad a guardar.
     */
    void save(T entity);

    /**
     * Guarda varios objetos en el repositorio.
     * @param entities Entidades a guardar.
     */
    void save(List<T> entities);

    /**
     * Busca una entidad del repositorio por su llave primaria.
     * @param key Valor de la llave primaria de la endidad.
     * @return Entidad encontrada.
     */
    T findById(K key);

    /**
     * Elimina una entidad del repositorio por su llave primaria.
     * @param key Llave primaria de la entidad.
     *
     */
    void deleteById(K key);

    /**
     * Elimina todas las entidades.
     */
    void deleteAll();

    /**
     * Elimina una entidad.
     */
    void delete(T entity);

    /**
     * Busca todas entidades del repositorio
     * @return Entidades encontradas.
     */
    List<T> findAll();

}
