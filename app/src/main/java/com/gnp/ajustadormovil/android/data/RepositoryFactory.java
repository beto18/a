package com.gnp.ajustadormovil.android.data;

import com.gnp.ajustadormovil.android.data.orquestador.AjustadorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CertificadoCompaniaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CertificadoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CroquisRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FirmaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ServicioCoberturaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroNoAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AjustadorRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AjusteRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AseguradoRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.CertificadoCompaniaRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.CertificadoRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.CroquisRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.ErrorRepositoryMemoryImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.FirmaRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.FotografiaRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.RecuperacionRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.ResponsabilidadRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.ServiciosCoberturaRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.TerceroAutoRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.TerceroNoAutoRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.TerceroPersonaRespositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.orquestador.impl.TerceroRepositoryRealmImpl;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;

import java.io.Serializable;

import io.realm.RealmObject;

public class RepositoryFactory {

    private static AjustadorRepository ajustadorRepository;
    private static AjusteRepository ajusteRepository;
    private static AseguradoRepository aseguradoRepository;
    private static TerceroRepository terceroRepository;
    private static TerceroAutoRepository terceroAutoRepository;
    private static TerceroNoAutoRepository terceroNoAutoRepository;
    private static TerceroPersonaRepository terceroPersonaRepository;
    private static ResponsabilidadRepository responsabilidadRepository;
    private static RecuperacionRepository recuperacionRepository;
    private static ServicioCoberturaRepository servicioCoberturaRepository;
    private static ErrorRepository errorRepository;
    private static FotografiaRepository fotografiaRepository;
    private static FirmaRepository firmaRepository;
    private static CroquisRepository croquisRepository;
    private static AmisRepository amisRepository;
    private static CertificadoCompaniaRepository certificadoCompaniaRepository;
    private static CertificadoRepository certificadoRepository;

    public static AjustadorRepository getAjustadorRepository(){
        synchronized (AjustadorRepository.class){
            if (ajustadorRepository == null){
                ajustadorRepository = new AjustadorRepositoryRealmImpl();
            }
            return ajustadorRepository;
        }
    }

    public static AjusteRepository getAjusteRepository(){
        synchronized (AjusteRepository.class){
            if (ajusteRepository == null){
                ajusteRepository = new AjusteRepositoryRealmImpl();
            }
            return ajusteRepository;
        }
    }

    public static AseguradoRepository getAseguradoRepository(){
        synchronized (AseguradoRepository.class){
            if (aseguradoRepository == null){
                aseguradoRepository = new AseguradoRepositoryRealmImpl();
            }
            return aseguradoRepository;
        }
    }

    public static ServicioCoberturaRepository getServicioCoberturaRepository(){
        synchronized (ServicioCoberturaRepository.class){
            if (servicioCoberturaRepository==null){
                servicioCoberturaRepository = new ServiciosCoberturaRepositoryRealmImpl();
            }
            return servicioCoberturaRepository;
        }
    }

    public static TerceroRepository getTerceroRepository(){
        synchronized (TerceroRepository.class){
            if (terceroRepository == null){
                terceroRepository = new TerceroRepositoryRealmImpl();
            }
            return terceroRepository;
        }
    }

    public static TerceroAutoRepository getTerceroAutoRepository(){
        synchronized (TerceroAutoRepository.class){
            if (terceroAutoRepository == null){
                terceroAutoRepository = new TerceroAutoRepositoryRealmImpl();
            }
            return terceroAutoRepository;
        }
    }

    public static TerceroNoAutoRepository getTerceroNoAutoRepository(){
        synchronized (TerceroNoAutoRepository.class){
            if (terceroNoAutoRepository == null){
                terceroNoAutoRepository = new TerceroNoAutoRepositoryRealmImpl();
            }
            return terceroNoAutoRepository;
        }
    }

    public static TerceroPersonaRepository getTerceroPersonaRepository(){
        synchronized (TerceroPersonaRepository.class){
            if (terceroPersonaRepository == null){
                terceroPersonaRepository = new TerceroPersonaRespositoryRealmImpl();
            }
            return terceroPersonaRepository;
        }
    }

    public static ResponsabilidadRepository getResponsabilidadRepository(){
        synchronized (ResponsabilidadRepository.class){
            if (responsabilidadRepository == null){
                responsabilidadRepository = new ResponsabilidadRepositoryRealmImpl();
            }
            return responsabilidadRepository;
        }
    }

    public static RecuperacionRepository getRecuperacionRepository(){
        synchronized (RecuperacionRepository.class){
            if (recuperacionRepository == null){
                recuperacionRepository = new RecuperacionRepositoryRealmImpl();
            }
            return recuperacionRepository;
        }
    }

    public static ErrorRepository getErrorRepository(){
        synchronized (ErrorRepository.class){
            if (errorRepository == null){
                errorRepository = new ErrorRepositoryMemoryImpl();
            }
            return errorRepository;
        }
    }

    public static FotografiaRepository getFotodrafiaRepository(){
        synchronized (FotografiaRepository.class){
            if (fotografiaRepository == null){
                fotografiaRepository = new FotografiaRepositoryRealmImpl();
            }
            return fotografiaRepository;
        }
    }
    public static FirmaRepository getFirmaRepository(){
        synchronized (FirmaRepository.class){
            if(firmaRepository == null){
                firmaRepository = new FirmaRepositoryRealmImpl();
            }
            return firmaRepository;
        }
    }
    public static CroquisRepository getCroquisRepository(){
        synchronized (CroquisRepository.class){
            if(croquisRepository == null){
                croquisRepository = new CroquisRepositoryRealmImpl();
            }
            return croquisRepository;
        }
    }

    public static AmisRepository getAmisRepository(){
        synchronized (AmisRepository.class){
            if (amisRepository == null){
                amisRepository = new AmisRepositoryRealmImpl();
            }
            return amisRepository;
        }
    }

    public static CertificadoCompaniaRepository getCertificadoCompaniaRepository(){
        synchronized (CertificadoCompaniaRepository.class){
            if (certificadoCompaniaRepository == null){
                certificadoCompaniaRepository = new CertificadoCompaniaRepositoryRealmImpl();
            }
            return certificadoCompaniaRepository;
        }
    }

    public static CertificadoRepository getCertificadoRepository(){
        synchronized (CertificadoRepository.class){
            if (certificadoRepository == null){
                certificadoRepository = new CertificadoRepositoryRealmImpl();
            }
            return certificadoRepository;
        }
    }

    public static <T extends RealmObject, K extends Serializable> Repository<T, K> getRepository(Class<T> type, Class<K> key){
        return new RealmRepository<>(type);
    }

}
