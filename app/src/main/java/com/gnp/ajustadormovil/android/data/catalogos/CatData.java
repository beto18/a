package com.gnp.ajustadormovil.android.data.catalogos;

import com.gnp.ajustadormovil.android.models.address.AddressWS;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Colonia;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CatData {

    @Headers("Content-Type: application/json")
    @GET("cat/entidades-financieras")
    Single<List<CatalogoClave>> getEntidadesFinancieras();

    @Headers("Content-Type: application/json")
    @GET("cat/entidades-financieras/{clave}")
    Single<CatalogoClave> getEntidadFinanciera(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("cat/monedas")
    Single<List<CatalogoClaveInfo>> getMonedas();

    @Headers("Content-Type: application/json")
    @GET("cat/polizas/estatus")
    Single<List<CatalogoClaveInfo>> getEstatusPoliza();

    @Headers("Content-Type: application/json")
    @GET("cat/vehiculos/categorias")
    Single<List<CatalogoClave>> getCategoriasVehiculo();

    @Headers("Content-Type: application/json")
    @GET("cat/vias")
    Single<List<CatalogoClave>> getVias();

    @Headers("Content-Type: application/json")
    @GET("cat/otras-aseguradoras")
    Single<List<CatalogoClave>> getOtrasAseguradoras();

    @Headers("Content-Type: application/json")
    @GET("cat/otras-aseguradoras/{clave}")
    Single<CatalogoClave> getOtraAseguradora(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("cat/estados/")
    Single<List<CatalogoClaveInfo>> getEstados();

    @Headers("Content-Type: application/json")
    @GET("cat/estados/{estado}")
    Single<CatalogoClaveInfo> getEstado(@Path("estado")String estado);

    @Headers("Content-Type: application/json")
    @GET("cat/estados/{estado}/municipios")
    Single<List<Municipio>> getMunicipios(@Path("estado")String estado);

    @Headers("Content-Type: application/json")
    @GET("cat/estados/{estado}/municipios/{municipio}")
    Single<Municipio> getMunicipio(@Path("estado")String estado,
                                   @Path("municipio") String municipio);

    @Headers("Content-Type: application/json")
    @GET("cat/estados/{estado}/municipios/{municipio}/colonias")
    Single<List<ColoniaResumen>> getColonias(@Path("estado") String estado,
                                             @Path("municipio") String municipio);

    @Headers("Content-Type: application/json")
    @GET("cat/estados/{estado}/municipios/{municipio}/colonias/{clave}")
    Single<Colonia> getColonia(@Path("estado") String estado,
                                   @Path("municipio") String municipio,
                                   @Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("cat/ubicaciones")
    Single<AddressWS> getLocationAddress(@Query("lat") Double lat,
                                         @Query("lng") Double lng);

}
