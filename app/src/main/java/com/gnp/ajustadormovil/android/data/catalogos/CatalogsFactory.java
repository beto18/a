package com.gnp.ajustadormovil.android.data.catalogos;


import android.content.Context;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.catalogos.impl.CatDataSQLiteImpl;
import com.gnp.ajustadormovil.android.data.catalogos.impl.SiniestrosDataSQLiteImpl;
import com.gnp.ajustadormovil.android.data.catalogos.impl.TprDataSQLiteImpl;
import com.gnp.ajustadormovil.android.data.catalogos.impl.VersionDataSQLiteImpl;
import com.gnp.ajustadormovil.android.data.retrofit.RetrofitResources;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class CatalogsFactory {

    private static Retrofit retrofit;

    private static SiniestrosData siniestrosDataOnline;
    private static TprData tprDataOnline;
    private static CatData catDataOnline;

    private static SiniestrosData siniestrosDataOffline;
    private static TprData tprDataOffline;
    private static CatData catDataOffline;

    private static InfoData infoData;
    private static VersionData versionData;

    public static SiniestrosData getCatalogsSiniestros(DataType type) {
        if (type == DataType.ONLINE) {
            synchronized (SiniestrosData.class) {
                if (siniestrosDataOnline == null) {
                    siniestrosDataOnline = buildRetrofit(SiniestrosData.class);
                }
                return siniestrosDataOnline;
            }
        } else if (type == DataType.OFFLINE) {
            synchronized (SiniestrosDataSQLiteImpl.class) {
                if (siniestrosDataOffline == null) {
                    siniestrosDataOffline = new SiniestrosDataSQLiteImpl(getContext());

                }
                return siniestrosDataOffline;
            }

        }
        return null;
    }

    public static TprData getCatalogosTpr(DataType type) {
        if (type == DataType.ONLINE) {
            synchronized (TprData.class) {
                if (tprDataOnline == null) {
                    tprDataOnline = buildRetrofit(TprData.class);
                }
                return tprDataOnline;

            }
        } else if (type == DataType.OFFLINE) {
            synchronized (TprDataSQLiteImpl.class) {
                if (tprDataOffline == null) {
                    tprDataOffline = new TprDataSQLiteImpl(getContext());
                }

                return tprDataOffline;
            }

        }
        return null;
    }

    public static CatData getCatalogosCat(DataType type) {
        if (type == DataType.ONLINE)
            synchronized (CatData.class) {
                if (catDataOnline == null) {
                    catDataOnline = buildRetrofit(CatData.class);
                }
                return catDataOnline;

            }
        else if (type == DataType.OFFLINE) {
            synchronized (CatDataSQLiteImpl.class) {
                if (catDataOffline == null) {
                    catDataOffline = new CatDataSQLiteImpl(getContext());
                }
                return catDataOffline;
            }
        }
        return null;
    }

    public static InfoData getInfoData(){
        synchronized (InfoData.class){
            if(infoData == null){
                infoData = buildRetrofit(InfoData.class);
            }
            return infoData;
        }
    }

    public static VersionData getVersionData(){
        synchronized (VersionData.class){
            if(versionData == null){
                versionData = new VersionDataSQLiteImpl(getContext());
            }
            return versionData;
        }
    }

    private synchronized static <T> T buildRetrofit(Class<T> type){
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utils.BASE_URL_CATALOGOS)
                    .client(RetrofitResources.getUnsafeOkHttpClient(
                            RetrofitResources.getLoggingInterceptor(),
                            RetrofitResources.getErrorInterceptor()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(type);
    }

    private static Context getContext(){
        return AjustadorMovilApplication.getInstance().getBaseContext();
    }

}