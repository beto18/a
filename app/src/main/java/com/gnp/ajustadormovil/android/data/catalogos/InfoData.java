package com.gnp.ajustadormovil.android.data.catalogos;

import com.gnp.ajustadormovil.android.models.catalogos.DataVersion;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface InfoData {

    @Headers("Content-Type: application/json")
    @GET("data/{name}/info")
    Observable<DataVersion> getDataVersion(@Path("name") String dataName);

}
