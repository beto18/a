package com.gnp.ajustadormovil.android.data.catalogos;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Configuracion;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CostoParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ProductoCashflow;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Responsable;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.TipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.UsoVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SiniestrosData {

    @Headers("Content-Type: application/json")
    @GET("siniestros/abogados/tipos")
    Single<List<CatalogoClave>> getTiposAbogado();

    @Headers("Content-Type: application/json")
    @GET("siniestros/abogados/gestiones")
    Single<List<CatalogoClave>> getGestionesAbogado();

    @Headers("Content-Type: application/json")
    @GET("siniestros/abogados/comunicaciones")
    Single<List<CatalogoClave>> getComunicacionesAbogado();

    @Headers("Content-Type: application/json")
    @GET ("siniestros/ajustes/cancelaciones")
    Single<List<CatalogoClave>> getMotivosCancelacionAjuste();

    @Headers("Content-Type: application/json")
    @GET("siniestros/ajustes/rechazos")
    Single<List<CatalogoClave>> getMotivosRechazoAjuste();

    @Headers("Content-Type: application/json")
    @GET("siniestros/productos/cashflow")
    Single<ProductoCashflow> getProductoCashflow(@Query("comercial") String comercial,
                                                 @Query("tecnico") String tecnico);

    @Headers("Content-Type: application/json")
    @GET("siniestros/ajustes/cierres")
    Single<List<CatalogoClave>> getMotivosCierreAjuste();

    @Headers("Content-Type: application/json")
    @GET("siniestros/ambulancias")
    Single<List<CatalogoClave>> getTiposAmbulancia();

    @Headers("Content-Type: application/json")
    @GET ("siniestros/centro/remates")
    Single<List<CentroRemate>> getCentrosRemate();

    @Headers("Content-Type: application/json")
    @GET ("siniestros/centro/remates/{id}")
    Single<CentroRemate> getCentroRemate(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/contratistas")
    Single<List<Contratista>> getContratistas();

    @Headers("Content-Type: application/json")
    @GET("siniestros/contratistas/{id}")
    Single<Contratista> getContratista(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/documentos/propiedades")
    Single<List<CatalogoClave>> getDocumentosPropiedad();

    @Headers("Content-Type: application/json")
    @GET("siniestros/documentos/identificaciones")
    Single<List<CatalogoClave>> getTiposIdentificacion();

    @Headers("Content-Type: application/json")
    @GET("siniestros/documentos/identificaciones/{clave}")
    Single<CatalogoClave> getTipoIdentificacion(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/gruas")
    Single<List<CatalogoClave>> getTiposGrua();

    @Headers("Content-Type: application/json")
    @GET("siniestros/hospitales/{id}")
    Single<Hospital> getHospital(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/hospitales")
    Single<List<HospitalResumen>> getHospitales(@Query("supervisoria") String supervisoria);

    @Headers("Content-Type: application/json")
    @GET("siniestros/investigaciones")
    Single<List<CatalogoClave>> getMotivosInvestigacion();

    @Headers("Content-Type: application/json")
    @GET("siniestros/lesiones/niveles")
    Single<List<CatalogoId>> getNivelLesion();

    @Headers("Content-Type: application/json")
    @GET("siniestros/lesiones/areas/")
    Single<List<CatalogoId>> getAreasLesionadas();

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/Areas/{id}")
    Single<CatalogoId> getAreaAfectada(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/lesiones/areas/{area}/partes")
    Single<List<CatalogoIdAmis>> getPartesLesionadas(@Path("area") Long area);

    @Headers("Content-Type: application/json")
    @GET("siniestros/lesiones/partes/{id}")
    Single<CatalogoIdAmis> getParteLesionada(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/lesiones/partes/{claveAmis}")
    Single<CatalogoIdAmis> getParteLesionadaAmis(@Path("claveAmis") String claveAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/nacionalidades")
    Single<List<CatalogoClave>> getNacionalidades();

    @Headers("Content-Type: application/json")
    @GET("siniestros/no-deducibles")
    Single<List<CatalogoId>> getRazonesNoDeducible();

    @Headers("Content-Type: application/json")
    @GET("siniestros/no-deducibles/{id}")
    Single<CatalogoId> getRazonNoDeducible(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/objetos-perdidos/{id}")
    Single<CatalogoId> getObjetoPerdido(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/ocupantes/posiciones")
    Single<List<CatalogoClave>> getPosicionesOcupante();

    @Headers("Content-Type: application/json")
    @GET("siniestros/ocupantes/posiciones/{clave}")
    Single<CatalogoClave> getPosicionOcupante(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/patrullas")
    Single<List<CatalogoClave>> getTiposPatrulla();

    @Headers("Content-Type: application/json")
    @GET("siniestros/polizas/coberturas")
    Single<List<CatalogoId>> getTiposPoliza();

    @Headers("Content-Type: application/json")
    @GET("siniestros/polizas/condicionamientos/{id}")
    Single<CatalogoId> getCondicionamientosPoliza(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/polizas/condicionamientos")
    Single<List<CatalogoId>> getCondicionamientosPoliza();

    @Headers("Content-Type: application/json")
    @GET("siniestros/recuperaciones/volantes")
    Single<List<CatalogoClave>> getVolantesRecuperacion();

    @Headers("Content-Type: application/json")
    @GET("siniestros/recuperaciones/monetarios")
    Single<List<CatalogoId>> getTiposRecuperacionMonetaria();

    @Headers("Content-Type: application/json")
    @GET("siniestros/recuperaciones/monetarios/{id}")
    Single<CatalogoId> getTipoRecuperacionMonetaria(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/recuperaciones/monetarios/tarjeta")
    Single<List<CatalogoClave>> getTiposRecuperacionMonetariaTarjeta();

    @Headers("Content-Type: application/json")
    @GET("siniestros/servicios")
    Single<List<CatalogoClave>> getTiposServicio();

    @Headers("Content-Type: application/json")
    @GET("siniestros/tipos")
    Single<List<CatalogoClave>> getTiposSiniestro();

    @Headers("Content-Type: application/json")
    @GET("siniestros/supervisorias")
    Single<List<Supervisoria>> getSupervisorias();

    @Headers("Content-Type: application/json")
    @GET("siniestros/telefonos")
    Single<List<CatalogoClaveAmis>> getTiposTelefono();

    @Headers("Content-Type: application/json")
    @GET("siniestros/telefonos/{clave}")
    Single<CatalogoClaveAmis> getTipoTelefono(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/telefonos/{claveAmis}")
    Single<CatalogoClaveAmis> getTipoTelefonoAmis(@Path("claveAmis") String claveAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros")
    Single<List<CatalogoClave>> getTiposTercero();

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros/autos/{clave}")
    Single<CatalogoClave> getTipoTercero(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros/autos")
    Single<List<CatalogoClave>> getTiposTerceroAuto();

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros/no-autos/")
    Single<List<CatalogoId>> getCategoriasTerceroNoAuto();

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros/no-autos/{id}")
    Single<CatalogoId> getCategoriaTerceroNoAuto(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros/no-autos/{categoria}/tipos")
    Single<List<CatalogoId>> getTiposTerceroNoAuto(@Path("categoria") Long categoria);

    @Headers("Content-Type: application/json")
    @GET("siniestros/terceros/no-autos/{categoria}/tipos/{id}")
    Single<CatalogoId> getTipoTerceroNoAuto(@Path("categoria") Long categoria, @Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/volantes/no-autos")
    Single<List<CatalogoClave>> getVolantesTerceroNoAuto();

    @Headers("Content-Type: application/json")
    @GET("siniestros/ubicaciones")
    Single<List<CatalogoClaveAmis>> getTiposUbicacion();

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/areas")
    Single<List<CatalogoId>> getAreasAfectadas();

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/areas/{area}/partes")
    Single<List<ParteAfectada>> getPartesAfectadas(@Path("area") Long area);

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/areas/{area}/partes/{id}")
    Single<ParteAfectada> getParteAfectada(@Path("area") Long area, @Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/partes/{id}")
    Single<ParteAfectada> getParteAfectada(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/vehiculos/partes/{claveAmis}")
    Single<ParteAfectada> getParteAfectadaAmis(@Path("claveAmis") String claveAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/impactos")
    Single<List<ImpactoVehiculoAmis>> getImpactosVehiculo();

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/impactos/{id}")
    Single<ImpactoVehiculoAmis> getImpactoVehiculo(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/vehiculos/impactos/{claveAmis}")
    Single<ImpactoVehiculoAmis> getImpactoVehiculoAmis(@Path("claveAmis") String id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/circunstancias")
    Single<List<CatalogoId>> getCircustanciasVehiculo();

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/circunstancias/{id}")
    Single<CatalogoId> getCircustanciaVehiculo(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/volantes")
    Single<List<CatalogoClave>> getTiposVolante();

    @Headers("Content-Type: application/json")
    @GET("siniestros/volantes/categorias/{categoria}/coberturas")
    Single<List<Cobertura>> getCoberturasVolante(@Path("categoria") Integer categoria,
                                                 @Query("tipoSiniestro") String tipoSiniestro);

    @Headers("Content-Type: application/json")
    @GET("siniestros/responsables")
    Single<List<Responsable>> getResponsables(@Query("a") Long circustanciaA,
                                              @Query("b")  Long circustanciaB);

    @Headers("Content-Type: application/json")
    @GET("siniestros/responsabilidades/{clave}")
    Single<CatalogoClave> getResponsabilidad(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("/siniestros/vehiculos/costos")
    Single<List<CostoParteAfectada>> getCostoParteAfectada(@Query("categoria") String categoria,
                                                           @Query("parte") List<Long> parte);
    @Headers("Content-Type: application/json")
    @GET("/siniestros/ajustes/configuraciones/{clave}")
    Single<Configuracion> getConfiguracion(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/generos")
    Single<List<CatalogoClaveAmis>> getGeneros();

    @Headers("Content-Type: application/json")
    @GET("siniestros/generos/{clave}")
    Single<CatalogoClaveAmis> getGenero(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/generos/{claveAmis}")
    Single<CatalogoClaveAmis> getGeneroAmis(@Path("claveAmis") String claveAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/documentos/licencias")
    Single<List<CatalogoClave>> getTiposLicencia(@Query("estado") String estado);

    @Headers("Content-Type: application/json")
    @GET("siniestros/documentos/licencias/{clave}")
    Single<TipoLicencia> getTipoLicencia(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/colores")
    Single<List<CatalogoId>> getColoresVehiculo();

    @Headers("Content-Type: application/json")
    @GET("siniestros/vehiculos/colores/{id}")
    Single<CatalogoId> getColorVehiculo(@Path("id") Long id);

    @Headers("Content-Type: application/json")
    @GET("siniestros/ubicaciones/{clave}")
    Single<CatalogoClaveAmis> getTipoUbicacion(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/ubicaciones/{claveAmis}")
    Single<CatalogoClaveAmis> getTipoUbicacionAmis(@Path("claveAmi") Long claveAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/personas")
    Single<List<CatalogoClaveAmis>> getTiposPersona();

    @Headers("Content-Type: application/json")
    @GET("siniestros/personas/{clave}")
    Single<CatalogoClaveAmis> getTipoPersona(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/personas/{claveAmis}")
    Single<CatalogoClaveAmis> getTipoPersonaAmis(@Path("claveAmis") Long claveAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/vehiculos")
    Single<List<VehiculoAmis>> getVehiculosAmis(@Query("tipoVehiculo") String tipoVehiculo,
                                                @Query("armadora") String armadora,
                                                @Query("carroceria") String carroceria);
    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/vehiculos")
    Single<List<VehiculoAmis>> getVehiculosAmis(@Query("transporte") Long transporte,
                                                @Query("marca") Long marca,
                                                @Query("tipo") Long tipo);
    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/vehiculos/usos")
    Single<List<UsoVehiculo>> getUsoVehiculosAmis(@Query("usoVehiculo") String usoVehiculo);

    @Headers("Content-Type: application/json")
    @GET("siniestros/ubicaciones/estados/{estado}/municipios/{municipio}")
    Single<MunicipioAmis> getMunicipio(@Path("estado") String estado, @Path("municipio") String municipio);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/ubicaciones/estados/{estadoAmis}/municipios/{municipioAmis}")
    Single<MunicipioAmis> getMunicipioAmis(@Path("estadoAmis") Integer estadoAmis, @Path("municipioAmis") Integer municipioAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/ubicaciones/estados/{estado}")
    Single<EstadoAmis> getEstado(@Path("estado") String estado);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/ubicaciones/estados/{estadoAmis}")
    Single<EstadoAmis> getEstadoAmis(@Path("estadoAmis") Integer estadoAmis);

    @Headers("Content-Type: application/json")
    @GET("siniestros/amis/ubicaciones/estados/{estadoAmis}")
    Single<String> getOtraAseguradoraAmis(@Path("estadoAmis") Integer claveAmis);
}
