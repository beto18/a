package com.gnp.ajustadormovil.android.data.catalogos;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Armadora;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Carroceria;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface TprData {

    @Headers("Content-Type: application/json")
    @GET("tpr/coberturas")
    Single<List<CatalogoClave>> getCoberturas();

    @Headers("Content-Type: application/json")
    @GET("tpr/coberturas/{clave}")
    Single<Cobertura> getCobertura(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos")
    Single<List<CatalogoClave>> getTiposVehiculo();

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{clave}")
    Single<CatalogoClave> getTipoVehiculo(@Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras")
    Single<List<CatalogoClave>> getArmadoras(@Path("tipoVehiculo") String tipoVehiculo);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras/{clave}")
    Single<Armadora> getArmadora(@Path("tipoVehiculo") String tipo,
                                 @Path("clave") String clave);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras/{armadora}/carrocerias")
    Single<List<CatalogoClave>> getCarrocerias(@Path("tipoVehiculo") String tipoVehiculo,
                                              @Path("armadora") String armadora);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras/{armadora}/carrocerias/{carroceria}")
    Single<Carroceria> getCarroceria(@Path("tipoVehiculo") String tipoVehiculo,
                                     @Path("armadora") String armadora,
                                     @Path("carroceria") String carroceria);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras/{armadora}/carrocerias/{carroceria}/modelos")
    Single<List<Short>> getModelos(@Path("tipoVehiculo") String tipo_vehiculo,
                                   @Path("armadora") String armadora,
                                   @Path("carroceria") String carroceria);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras/{armadora}/carrocerias/{carroceria}/modelos/{modelo}/versiones")
    Single<List<VersionVehiculoResumen>> getVersiones(@Path("tipoVehiculo") String tipo_vehiculo,
                                                      @Path("armadora") String armadora,
                                                      @Path("carroceria") String carroceria,
                                                      @Path("modelo") Integer modelo);

    @Headers("Content-Type: application/json")
    @GET("tpr/vehiculos/{tipoVehiculo}/armadoras/{armadora}/carrocerias/{carroceria}/modelos/{modelo}/versiones/{version}")
    Single<VersionVehiculo> getVersion(@Path("tipoVehiculo") String tipoVehiculo,
                                       @Path("armadora") String armadora,
                                       @Path("carroceria") String carroceria,
                                       @Path("modelo") Integer modelo,
                                       @Path("version") String version);

}
