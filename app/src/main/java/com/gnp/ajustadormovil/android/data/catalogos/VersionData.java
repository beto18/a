package com.gnp.ajustadormovil.android.data.catalogos;

public interface VersionData {

    /**
     * Obtiene la versión del nombre de los datos.
     * @param dataName Nombre de los datos.
     * @return Versión de datos.
     */
    String getVersion(String dataName);

    /**
     * Guarda la versión de los datos asociado a un nombre de datos.
     * @param dataName Nombre de los datos
     * @param version Versión de datos.
     */
    void saveVersion(String dataName, String version);

}
