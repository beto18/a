package com.gnp.ajustadormovil.android.data.catalogos.impl;

import android.content.Context;

import com.gnp.ajustadormovil.android.data.catalogos.CatData;
import com.gnp.ajustadormovil.android.data.sqlite.RowMapper;
import com.gnp.ajustadormovil.android.data.sqlite.SQLiteData;
import com.gnp.ajustadormovil.android.models.address.AddressWS;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Colonia;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.utils.Utils;

import java.util.List;

import io.reactivex.Single;

public class CatDataSQLiteImpl extends SQLiteData implements CatData {

    private static final String DB_NAME = Utils.DATA_CAT_NAME;

    private static final String QUERY_ENTIDADES_FINANCIERAS = "select CVE_ENTIDAD_FINANCIERA, ENTIDAD_FINANCIERA " +
            "from CAT_ENTIDAD_FINANCIERA " +
            "order by ENTIDAD_FINANCIERA";

    private static final String QUERY_ENTIDAD_FINANCIERA = "select CVE_ENTIDAD_FINANCIERA, ENTIDAD_FINANCIERA " +
            "from CAT_ENTIDAD_FINANCIERA " +
            "where CVE_ENTIDAD_FINANCIERA = ?";

    private static final String QUERY_MONEDAS = "select CVE_MONEDA, CVE_MONEDA_INFO, MONEDA " +
            "from CAT_MONEDA " +
            "order by MONEDA";
    private static final String QUERY_ESTATUS_POLIZA = "select CVE_ESTATUS_POLIZA, CVE_ESTATUS_POLIZA_INFO, ESTATUS_POLIZA " +
            "from CAT_ESTATUS_POLIZA " +
            "order by ESTATUS_POLIZA";
    private static final String QUERY_CATEGORIAS_VEHICULO = "select CVE_CATEGORIA_VEHICULO, CATEGORIA_VEHICULO " +
            "from CAT_CATEGORIA_VEHICULO " +
            "order by CATEGORIA_VEHICULO";
    private static final String QUERY_VIAS = "select CVE_TIPO_VIA, TIPO_VIA " +
            "from CAT_TIPO_VIA " +
            "order by TIPO_VIA";
    private static final String QUERY_OTRAS_ASEGURADORAS = "select CVE_OTRA_ASEGURADORA, ASEGURADORA " +
            "from CAT_OTRAS_ASEGURADORAS " +
            "order by ASEGURADORA";
    private static final String QUERY_OTRA_ASEGURADORA = "select CVE_OTRA_ASEGURADORA, ASEGURADORA " +
            "from CAT_OTRAS_ASEGURADORAS " +
            "where CVE_OTRA_ASEGURADORA = ?";
    private static final String QUERY_ESTADOS = "select CVE_ESTADO, CVE_ESTADO_INFO, ESTADO " +
            "from CAT_ESTADO " +
            "order by ESTADO";
    private static final String QUERY_ESTADO = "select CVE_ESTADO, CVE_ESTADO_INFO, ESTADO " +
            "from CAT_ESTADO " +
            "where CVE_ESTADO = ? ";
    private static final String QUERY_MUNICIPIOS = "select " +
            "m.CVE_MUNICIPIO, m.CVE_MUNICIPIO_INFO, m.MUNICIPIO, " +
            "e.CVE_ESTADO, e.CVE_ESTADO_INFO, e.ESTADO " +
            "from CAT_MUNICIPIO m " +
            "inner join CAT_ESTADO e on " +
            "e.CVE_ESTADO = m.CVE_ESTADO " +
            "where m.CVE_ESTADO = ? " +
            "order by m.MUNICIPIO ";
    private static final String QUERY_MUNICIPIO = "select " +
            "m.CVE_MUNICIPIO, m.CVE_MUNICIPIO_INFO, m.MUNICIPIO, " +
            "e.CVE_ESTADO, e.CVE_ESTADO_INFO, e.ESTADO " +
            "from CAT_MUNICIPIO m " +
            "inner join CAT_ESTADO e on " +
            "e.CVE_ESTADO = m.CVE_ESTADO " +
            "where m.CVE_ESTADO = ? and m.CVE_MUNICIPIO = ? ";
    private static final String QUERY_COLONIAS = "select " +
            "c.CVE_COLONIA, c.CVE_COLONIA_INFO, c.COLONIA, " +
            "m.CVE_MUNICIPIO, m.CVE_MUNICIPIO_INFO, m.MUNICIPIO, " +
            "e.CVE_ESTADO, e.CVE_ESTADO_INFO, e.ESTADO " +
            "from CAT_COLONIA c " +
            "inner join CAT_MUNICIPIO m on " +
            "m.CVE_MUNICIPIO = c.CVE_MUNICIPIO and " +
            "m.CVE_ESTADO = c.CVE_ESTADO " +
            "inner join CAT_ESTADO e on " +
            "e.CVE_ESTADO = m.CVE_ESTADO " +
            "where c.CVE_ESTADO = ? " +
            "and c.CVE_MUNICIPIO = ?  " +
            "order by c.COLONIA; ";
    private static final String QUERY_COLONIA = "select " +
            "c.CVE_COLONIA, c.CVE_COLONIA_INFO, c.COLONIA, c.CODIGO_POSTAL, " +
            "m.CVE_MUNICIPIO, m.CVE_MUNICIPIO_INFO, m.MUNICIPIO, " +
            "e.CVE_ESTADO, e.CVE_ESTADO_INFO, e.ESTADO, " +
            "a.CVE_ASENTAMIENTO, a.CVE_ASENTAMIENTO_INFO, a.ASENTAMIENTO " +
            "from CAT_COLONIA c " +
            "inner join CAT_MUNICIPIO m on " +
            "m.CVE_MUNICIPIO = c.CVE_MUNICIPIO and " +
            "m.CVE_ESTADO = c.CVE_ESTADO " +
            "inner join CAT_ESTADO e on " +
            "e.CVE_ESTADO = m.CVE_ESTADO " +
            "inner join CAT_ASENTAMIENTO a on " +
            "a.CVE_ASENTAMIENTO = c.CVE_ASENTAMIENTO " +
            "where c.CVE_ESTADO = ? " +
            "and c.CVE_MUNICIPIO = ?  " +
            "and c.CVE_COLONIA = ?  " +
            "order by c.COLONIA; ";

    private static final RowMapper<CatalogoClave> MAPPER_CATALOGO_CLAVE = cursor -> {
        CatalogoClave catalogo = new CatalogoClave();
        catalogo.setClave(cursor.getString(0));
        catalogo.setNombre(cursor.getString(1));
        return catalogo;
    };
    private static final RowMapper<CatalogoClaveInfo> MAPPER_CATALOGO_CLAVE_INFO = cursor -> {
        CatalogoClaveInfo catalogo = new CatalogoClaveInfo();
        catalogo.setClave(cursor.getString(0));
        catalogo.setClaveInfo(cursor.getString(1));
        catalogo.setNombre(cursor.getString(2));
        return catalogo;
    };
    private static final RowMapper<Municipio> MAPPER_MUNICIPIO = cursor -> {
        Municipio catalogo = new Municipio();
        catalogo.setClave(cursor.getString(0));
        catalogo.setClaveInfo(cursor.getString(1));
        catalogo.setNombre(cursor.getString(2));
        catalogo.setEstado(new CatalogoClaveInfo());
        catalogo.getEstado().setClave(cursor.getString(3));
        catalogo.getEstado().setClaveInfo(cursor.getString(4));
        catalogo.getEstado().setNombre(cursor.getString(5));
        return catalogo;
    };
    private static final RowMapper<ColoniaResumen> MAPPER_COLONIA_RESUMEN = cursor -> {
        ColoniaResumen catalogo = new ColoniaResumen();
        catalogo.setClave(cursor.getString(0));
        catalogo.setClaveInfo(cursor.getString(1));
        catalogo.setNombre(cursor.getString(2));
        catalogo.setMunicipio(new Municipio());
        catalogo.getMunicipio().setClave(cursor.getString(3));
        catalogo.getMunicipio().setClaveInfo(cursor.getString(4));
        catalogo.getMunicipio().setNombre(cursor.getString(5));
        catalogo.getMunicipio().setEstado(new CatalogoClaveInfo());
        catalogo.getMunicipio().getEstado().setClave(cursor.getString(6));
        catalogo.getMunicipio().getEstado().setClaveInfo(cursor.getString(7));
        catalogo.getMunicipio().getEstado().setNombre(cursor.getString(8));
        return catalogo;
    };
    private static final RowMapper<Colonia> MAPPER_COLONIA = cursor -> {
        Colonia catalogo = new Colonia();
        catalogo.setClave(cursor.getString(0));
        catalogo.setClaveInfo(cursor.getString(1));
        catalogo.setNombre(cursor.getString(2));
        catalogo.setCodigoPostal(cursor.getString(3));
        catalogo.setMunicipio(new Municipio());
        catalogo.getMunicipio().setClave(cursor.getString(4));
        catalogo.getMunicipio().setClaveInfo(cursor.getString(5));
        catalogo.getMunicipio().setNombre(cursor.getString(6));
        catalogo.getMunicipio().setEstado(new CatalogoClaveInfo());
        catalogo.getMunicipio().getEstado().setClave(cursor.getString(7));
        catalogo.getMunicipio().getEstado().setClaveInfo(cursor.getString(8));
        catalogo.getMunicipio().getEstado().setNombre(cursor.getString(9));
        catalogo.setAsentamiento(new CatalogoClaveInfo());
        catalogo.getAsentamiento().setClave(cursor.getString(10));
        catalogo.getAsentamiento().setClaveInfo(cursor.getString(11));
        catalogo.getAsentamiento().setNombre(cursor.getString(12));
        return catalogo;
    };

    public CatDataSQLiteImpl(Context context) {
        super(context, DB_NAME);
    }

    @Override
    protected String getSqlFilename() {
        return "db/cat.sql";
    }

    public Single<List<CatalogoClave>> getEntidadesFinancieras(){
        return getSingleList(QUERY_ENTIDADES_FINANCIERAS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<CatalogoClave> getEntidadFinanciera(String clave) {
        return getSingleObject(QUERY_ENTIDAD_FINANCIERA,MAPPER_CATALOGO_CLAVE,clave);
    }

    public Single<List<CatalogoClaveInfo>> getMonedas(){
        return getSingleList(QUERY_MONEDAS, MAPPER_CATALOGO_CLAVE_INFO);
    }

    public Single<List<CatalogoClaveInfo>> getEstatusPoliza(){
        return getSingleList(QUERY_ESTATUS_POLIZA, MAPPER_CATALOGO_CLAVE_INFO);
    }

    public Single<List<CatalogoClave>> getCategoriasVehiculo(){
        return getSingleList(QUERY_CATEGORIAS_VEHICULO, MAPPER_CATALOGO_CLAVE);
    }

    public Single<List<CatalogoClave>> getVias(){
        return getSingleList(QUERY_VIAS, MAPPER_CATALOGO_CLAVE);
    }

    public Single<List<CatalogoClave>> getOtrasAseguradoras(){
        return getSingleList(QUERY_OTRAS_ASEGURADORAS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<CatalogoClave> getOtraAseguradora(String clave) {
        return getSingleObject(QUERY_OTRA_ASEGURADORA, MAPPER_CATALOGO_CLAVE, clave);
    }

    public Single<List<CatalogoClaveInfo>> getEstados(){
        return getSingleList(QUERY_ESTADOS, MAPPER_CATALOGO_CLAVE_INFO);
    }

    @Override
    public Single<CatalogoClaveInfo> getEstado(String estado){
        return getSingleObject(QUERY_ESTADO, MAPPER_CATALOGO_CLAVE_INFO, estado);
    }

    public Single<List<Municipio>> getMunicipios(String estado){
        return getSingleList(QUERY_MUNICIPIOS, MAPPER_MUNICIPIO, estado);
    }

    @Override
    public Single<Municipio> getMunicipio(String estado, String municipio) {
        return getSingleObject(QUERY_MUNICIPIO, MAPPER_MUNICIPIO, estado, municipio);
    }

    public Single<List<ColoniaResumen>> getColonias(String estado, String municipio) {
        return getSingleList(QUERY_COLONIAS, MAPPER_COLONIA_RESUMEN, estado, municipio);
    }

    public Single<Colonia> getColonia(String estado, String municipio, String clave){
        return getSingleObject(QUERY_COLONIA, MAPPER_COLONIA, estado, municipio, clave);
    }

    @Override
    public Single<AddressWS> getLocationAddress(Double lat, Double lng) {
        return Single.error(new UnsupportedOperationException("Esta función solo se encuentra disponible en modo online"));
    }

}
