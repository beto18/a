package com.gnp.ajustadormovil.android.data.catalogos.impl;

import android.content.Context;

import com.gnp.ajustadormovil.android.data.catalogos.SiniestrosData;
import com.gnp.ajustadormovil.android.data.sqlite.RowMapper;
import com.gnp.ajustadormovil.android.data.sqlite.SQLiteData;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Configuracion;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CostoParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoTipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ProductoCashflow;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Responsable;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Telefono;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.TipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.UsoVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis;
import com.gnp.ajustadormovil.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Path;

public class SiniestrosDataSQLiteImpl extends SQLiteData implements SiniestrosData {

    private static final String DB_NAME = Utils.DATA_SINAU_NAME;

    private static final String QUERY_AREAS_AFECTADAS = "select " +
            "ID_AREA_AFECTADA, AREA_AFECTADA " +
            "from SIN_CAT_AREA_AFECTADA " +
            "order by AREA_AFECTADA";

    private static final String QUERY_AREA_AFECTADA = "select " +
            "ID_AREA_AFECTADA, AREA_AFECTADA " +
            "from SIN_CAT_AREA_AFECTADA " +
            "where ID_AREA_AFECTADA = ? ";

    private static final String QUERY_RESPONSABILIDAD = "select " +
            "CVE_RESPONSABILIDAD, RESPONSABILIDAD " +
            "from SIN_CAT_RESPONSABILIDAD " +
            "where CVE_RESPONSABILIDAD = ? ";

    private static final String QUERY_AREAS_LESIONADAS = "select " +
            "ID_AREA_LESIONADA, AREA_LESIONADA " +
            "from SIN_CAT_AREA_LESIONADA " +
            "order by AREA_LESIONADA";

    private static final String QUERY_CATEGORIAS_NO_AUTO = "select " +
            "ID_CATEGORIA_NO_AUTO, CATEGORIA_NO_AUTO " +
            "from SIN_CAT_CATEGORIA_NO_AUTO " +
            "order by CATEGORIA_NO_AUTO";

    private static final String QUERY_CATEGORIA_NO_AUTO = "select " +
            "ID_CATEGORIA_NO_AUTO, CATEGORIA_NO_AUTO " +
            "from SIN_CAT_CATEGORIA_NO_AUTO " +
            "where ID_CATEGORIA_NO_AUTO = ? " +
            "order by CATEGORIA_NO_AUTO";

    private static final String QUERY_CENTRO_REMATES = "select " +
            "ID_CENTRO_REMATE, CENTRO_REMATE, DIRECCION " +
            "from SIN_CAT_CENTRO_REMATE " +
            "order by CENTRO_REMATE";

    private static final String QUERY_CENTRO_REMATE = "select " +
            "ID_CENTRO_REMATE, CENTRO_REMATE, DIRECCION " +
            "from SIN_CAT_CENTRO_REMATE " +
            "where ID_CENTRO_REMATE = ?";

    private static final String QUERY_COMUNICACION_ABOGADO = "select " +
            "CVE_COMUNICACION_ABOGADO, COMUNICACION_ABOGADO " +
            "from SIN_CAT_COMUNICACION_ABOGADO " +
            "order by COMUNICACION_ABOGADO";

    private static final String QUERY_CONTRATISTAS = "select " +
            "ID_CONTRATISTA, CVE_SUPERVISORIA, CONTRATISTA " +
            "from SIN_CAT_CONTRATISTA " +
            "order by CONTRATISTA";

    private static final String QUERY_CONTRATISTA = "select " +
            "ID_CONTRATISTA, CVE_SUPERVISORIA, CONTRATISTA " +
            "from SIN_CAT_CONTRATISTA " +
            "where ID_CONTRATISTA = ? " +
            "order by CONTRATISTA";

    private static final String QUERY_DOCUMENTO_PROPIEDAD = "select " +
            "CVE_DOCUMENTO_PROPIEDAD, DOCUMENTO_PROPIEDAD " +
            "from SIN_CAT_DOCUMENTO_PROPIEDAD " +
            "order by DOCUMENTO_PROPIEDAD";

    private static final String QUERY_ESPECIALIDAD_SERVICIO = "select " +
            "CVE_ESPECIALIDAD_SERVICIO, ESPECIALIDAD_SERVICIO " +
            "from SIN_CAT_ESPECIALIDAD_SERVICIO " +
            "where TIPO_ESPECIALIDAD_SERVICIO = ? " +
            "order by ESPECIALIDAD_SERVICIO";

    private static final String QUERY_TIPOS_LICENCIA = "select " +
            "tl.CVE_TIPO_LICENCIA, tl.TIPO_LICENCIA " +
            "from SIN_CAT_TIPO_LICENCIA tl " +
            "inner join SIN_CAT_ESTADO_TIPO_LICENCIA el on tl.CVE_TIPO_LICENCIA = el.CVE_TIPO_LICENCIA " +
            "where el.CVE_ESTADO = ? " +
            "order by tl.TIPO_LICENCIA";

    private static final String QUERY_TIPO_LICENCIA = "select " +
            "CVE_TIPO_LICENCIA, TIPO_LICENCIA " +
            "from SIN_CAT_TIPO_LICENCIA " +
            "where CVE_TIPO_LICENCIA = ? ";

    private static final String QUERY_ESTADOS_TIPO_LICENCIA = "select " +
            "CVE_ESTADO, ID_ESTADO_AMIS " +
            "from SIN_CAT_ESTADO_TIPO_LICENCIA " +
            "where CVE_TIPO_LICENCIA = ? ";


    private static final String QUERY_HOSPITALES = "select " +
            "ID_HOSPITAL, HOSPITAL, CVE_SUPERVISORIA " +
            "from SIN_CAT_HOSPITAL " +
            "where CVE_SUPERVISORIA = ? " +
            "order by HOSPITAL";

    private static final String QUERY_HOSPITAL = "select " +
            "ID_HOSPITAL, HOSPITAL, CVE_SUPERVISORIA, " +
            "DIRECCION, CORREO_ELECTRONICO " +
            "from SIN_CAT_HOSPITAL " +
            "where ID_HOSPITAL = ? ";

    private static final String QUERY_HOSPITAL_TELEFONO = "select " +
            "TELEFONO " +
            "from SIN_CAT_HOSPITAL_TELEFONO " +
            "where ID_HOSPITAL = ? ";

    private static final String QUERY_MOTIVOS_CANCELACION = "select " +
            "CVE_MOTIVO_CANCELACION, MOTIVO_CANCELACION " +
            "from SIN_CAT_MOTIVO_CANCELACION " +
            "order by MOTIVO_CANCELACION";

    private static final String QUERY_CIERRES = "select " +
            "CVE_MOTIVO_CIERRE, MOTIVO_CIERRE " +
            "from SIN_CAT_MOTIVO_CIERRE " +
            "order by MOTIVO_CIERRE";

    private static final String QUERY_MOTIVO_INVESTIGACION = "select " +
            "CVE_MOTIVO_INVESTIGACION, MOTIVO_INVESTIGACION " +
            "from SIN_CAT_MOTIVO_INVESTIGACION " +
            "order by MOTIVO_INVESTIGACION";

    private static final String QUERY_MOTIVO_PATRULLA = "select " +
            "CVE_MOTIVO_PATRULLA, MOTIVO_PATRULLA " +
            "from SIN_CAT_MOTIVO_PATRULLA " +
            "order by MOTIVO_PATRULLA";

    private static final String QUERY_MOTIVOS_RECHAZO = "select " +
            "CVE_MOTIVO_RECHAZO, MOTIVO_RECHAZO " +
            "from SIN_CAT_MOTIVO_RECHAZO " +
            "order by MOTIVO_RECHAZO";

    private static final String QUERY_NACIONALIDADES = "select " +
            "CVE_NACIONALIDAD, NACIONALIDAD " +
            "from SIN_CAT_NACIONALIDAD " +
            "order by NACIONALIDAD";

    private static final String QUERY_NIVEL_LESION = "select " +
            "ID_NIVEL_LESION, NIVEL_LESION " +
            "from SIN_CAT_NIVEL_LESION " +
            "order by NIVEL_LESION";

    private static final String QUERY_GENEROS = "select " +
            "CVE_GENERO, GENERO, CVE_GENERO_AMIS " +
            "from SIN_CAT_GENERO " +
            "order by CVE_GENERO";

    private static final String QUERY_GENERO = "select " +
            "CVE_GENERO, GENERO, CVE_GENERO_AMIS " +
            "from SIN_CAT_GENERO " +
            "where CVE_GENERO = ? " +
            "order by CVE_GENERO";

    private static final String QUERY_GENERO_AMIS = "select " +
            "CVE_GENERO, GENERO, CVE_GENERO_AMIS " +
            "from SIN_CAT_GENERO " +
            "where CVE_GENERO_AMIS = ? " +
            "order by CVE_GENERO_AMIS";

    private static final String QUERY_PARTES_AFECTADAS = "select " +
            "p.ID_PARTE_AFECTADA, p.PARTE_AFECTADA, p.CVE_PARTE_AFECTADA_AMIS, p.CVE_AREA_PARTE_AFECTADA_AMIS, " +
            "a.ID_AREA_AFECTADA, a.AREA_AFECTADA " +
            "from SIN_CAT_PARTE_AFECTADA p " +
            "inner join SIN_CAT_AREA_AFECTADA a on a.ID_AREA_AFECTADA = p.ID_AREA_AFECTADA " +
            "where a.ID_AREA_AFECTADA = ? " +
            "order by PARTE_AFECTADA";

    private static final String QUERY_PARTE_AFECTADA = "select " +
            "p.ID_PARTE_AFECTADA, p.PARTE_AFECTADA, p.CVE_PARTE_AFECTADA_AMIS, p.CVE_AREA_PARTE_AFECTADA_AMIS, " +
            "a.ID_AREA_AFECTADA, a.AREA_AFECTADA " +
            "from SIN_CAT_PARTE_AFECTADA p " +
            "inner join SIN_CAT_AREA_AFECTADA a on a.ID_AREA_AFECTADA = p.ID_AREA_AFECTADA " +
            "where a.ID_AREA_AFECTADA = ? and p.ID_PARTE_AFECTADA = ?";

    private static final String QUERY_PARTE_AFECTADA_ID = "select " +
            "p.ID_PARTE_AFECTADA, p.PARTE_AFECTADA, p.CVE_PARTE_AFECTADA_AMIS, p.CVE_AREA_PARTE_AFECTADA_AMIS, " +
            "a.ID_AREA_AFECTADA, a.AREA_AFECTADA " +
            "from SIN_CAT_PARTE_AFECTADA p " +
            "inner join SIN_CAT_AREA_AFECTADA a on a.ID_AREA_AFECTADA = p.ID_AREA_AFECTADA " +
            "where p.ID_PARTE_AFECTADA = ?";

    private static final String QUERY_PARTE_AFECTADA_AMIS = "select " +
            "p.ID_PARTE_AFECTADA, p.PARTE_AFECTADA, p.CVE_PARTE_AFECTADA_AMIS, p.CVE_AREA_PARTE_AFECTADA_AMIS, " +
            "a.ID_AREA_AFECTADA, a.AREA_AFECTADA " +
            "from SIN_CAT_PARTE_AFECTADA p " +
            "inner join SIN_CAT_AREA_AFECTADA a on a.ID_AREA_AFECTADA = p.ID_AREA_AFECTADA " +
            "where CVE_PARTE_AFECTADA_AMIS = ?";

    private static final String QUERY_PARTES_LESIONADAS = "select " +
            "ID_PARTE_LESIONADA, PARTE_LESIONADA, CVE_PARTE_LESIONADA_AMIS " +
            "from SIN_CAT_PARTE_LESIONADA " +
            "where ID_AREA_LESIONADA = ? " +
            "order by PARTE_LESIONADA";

    private static final String QUERY_PARTE_LESIONADA = "select " +
            "ID_PARTE_LESIONADA, PARTE_LESIONADA, CVE_PARTE_LESIONADA_AMIS " +
            "from SIN_CAT_PARTE_LESIONADA " +
            "where ID_PARTE_LESIONADA = ? " +
            "order by PARTE_LESIONADA";

    private static final String QUERY_PARTE_LESIONADA_AMIS = "select " +
            "ID_PARTE_LESIONADA, PARTE_LESIONADA, CVE_PARTE_LESIONADA_AMIS " +
            "from SIN_CAT_PARTE_LESIONADA " +
            "where CVE_PARTE_LESIONADA_AMIS = ? " +
            "order by PARTE_LESIONADA";

    private static final String QUERY_POSICIONES_OCUPANTE = "select " +
            "CVE_POSICION_OCUPANTE, POSICION_OCUPANTE " +
            "from SIN_CAT_POSICION_OCUPANTE " +
            "order by POSICION_OCUPANTE";

    private static final String QUERY_POSICION_OCUPANTE = "select " +
            "CVE_POSICION_OCUPANTE, POSICION_OCUPANTE " +
            "from SIN_CAT_POSICION_OCUPANTE " +
            "where CVE_POSICION_OCUPANTE = ?";

    private static final String QUERY_PUNTOS_PRIMER_IMPACTO = "select " +
            "ID_PRIMER_IMPACTO, PRIMER_IMPACTO, CVE_PRIMER_IMPACTO_AMIS, CVE_AREA_PRIMER_IMPACTO_AMIS " +
            "from SIN_CAT_PRIMER_IMPACTO " +
            "order by PRIMER_IMPACTO";


    private static final String QUERY_PUNTO_PRIMER_IMPACTO = "select " +
            "ID_PRIMER_IMPACTO, PRIMER_IMPACTO, CVE_PRIMER_IMPACTO_AMIS, CVE_AREA_PRIMER_IMPACTO_AMIS " +
            "from SIN_CAT_PRIMER_IMPACTO " +
            "where ID_PRIMER_IMPACTO = ?";


    private static final String QUERY_PUNTO_PRIMER_IMPACTO_AMIS = "select " +
            "ID_PRIMER_IMPACTO, PRIMER_IMPACTO, CVE_PRIMER_IMPACTO_AMIS, CVE_AREA_PRIMER_IMPACTO_AMIS " +
            "from SIN_CAT_PRIMER_IMPACTO " +
            "where CVE_PRIMER_IMPACTO_AMIS = ?" +
            "order by PRIMER_IMPACTO";

    private static final String QUERY_CIRCUSNTANCIAS_VEHICULO = "select " +
            "ID_CIRCUNSTANCIA_VEHICULO, CIRCUNSTANCIA_VEHICULO " +
            "from SIN_CAT_CIRCUNSTANCIA_VEHICULO " +
            "order by ID_CIRCUNSTANCIA_VEHICULO";

    private static final String QUERY_CIRCUSNTANCIA_VEHICULO = "select " +
            "ID_CIRCUNSTANCIA_VEHICULO, CIRCUNSTANCIA_VEHICULO " +
            "from SIN_CAT_CIRCUNSTANCIA_VEHICULO " +
            "where ID_CIRCUNSTANCIA_VEHICULO = ?";

    private static final String QUERY_NO_DEDUCIBLES = "select " +
            "ID_RAZON_NO_DEDUCIBLE, RAZON_NO_DEDUCIBLE " +
            "from SIN_CAT_RAZON_NO_DEDUCIBLE " +
            "order by RAZON_NO_DEDUCIBLE";

    private static final String QUERY_NO_DEDUCIBLE = "select " +
            "ID_RAZON_NO_DEDUCIBLE, RAZON_NO_DEDUCIBLE " +
            "from SIN_CAT_RAZON_NO_DEDUCIBLE " +
            "where ID_RAZON_NO_DEDUCIBLE = ?";

    private static final String QUERY_OBJETO_PERDIDO = "select " +
            "ID_OBJETO_PERDIDO, OBJETO_PERDIDO " +
            "from SIN_CAT_OBJETO_PERDIDO " +
            "where ID_OBJETO_PERDIDO = ?";

    private static final String QUERY_TIPO_GESTION = "select " +
            "CVE_TIPO_GESTION, TIPO_GESTION " +
            "from SIN_CAT_TIPO_GESTION " +
            "order by TIPO_GESTION";

    private static final String QUERY_IDENTIFICACIONES = "select " +
            "CVE_TIPO_IDENTIFICACION, TIPO_IDENTIFICACION " +
            "from SIN_CAT_TIPO_IDENTIFICACION " +
            "order by TIPO_IDENTIFICACION";

    private static final String QUERY_TIPOS_NO_AUTOS = "select " +
            "ID_TIPO_NO_AUTO, TIPO_NO_AUTO " +
            "from SIN_CAT_TIPO_NO_AUTO " +
            "where ID_CATEGORIA_NO_AUTO = ? " +
            "order by TIPO_NO_AUTO";

    private static final String QUERY_TIPO_NO_AUTO = "select " +
            "ID_TIPO_NO_AUTO, TIPO_NO_AUTO " +
            "from SIN_CAT_TIPO_NO_AUTO " +
            "where ID_CATEGORIA_NO_AUTO = ? and ID_TIPO_NO_AUTO = ? " +
            "order by TIPO_NO_AUTO";

    private static final String QUERY_TIPO_POLIZA = "select " +
            "ID_TIPO_POLIZA, TIPO_POLIZA " +
            "from SIN_CAT_TIPO_POLIZA " +
            "order by TIPO_POLIZA";

    private static final String QUERY_CONDICIONAMIENTO_POLIZA = "select " +
            "ID_CAUSA_CONDICIONAMIENTO, CAUSA_CONDICIONAMIENTO " +
            "from SIN_CAT_CAUSA_CONDICIONAMIENTO " +
            "where ID_CAUSA_CONDICIONAMIENTO = ?";

    private static final String QUERY_CONDICIONAMIENTOS_POLIZA = "select " +
            "ID_CAUSA_CONDICIONAMIENTO, CAUSA_CONDICIONAMIENTO " +
            "from SIN_CAT_CAUSA_CONDICIONAMIENTO " +
            "order by CAUSA_CONDICIONAMIENTO";

    private static final String QUERY_TIPOS_RECUPERACION_MONETARIA = "select " +
            "ID_TIPO_RECUPERACION_MON, TIPO_RECUPERACION_MON " +
            "from SIN_CAT_TIPO_RECUPERACION_MON " +
            "order by TIPO_RECUPERACION_MON";

    private static final String QUERY_TIPO_RECUPERACION_MONETARIA = "select " +
            "ID_TIPO_RECUPERACION_MON, TIPO_RECUPERACION_MON " +
            "from SIN_CAT_TIPO_RECUPERACION_MON " +
            "where ID_TIPO_RECUPERACION_MON = ?";

    private static final String QUERY_RECUPERACION_TARJETA = "select " +
            "CVE_TIPO_RECUPERACION_TAR, TIPO_RECUPERACION_TAR " +
            "from SIN_CAT_TIPO_RECUPERACION_TAR " +
            "order by TIPO_RECUPERACION_TAR";

    private static final String QUERY_SERVICIOS = "select " +
            "CVE_TIPO_SERVICIO, TIPO_SERVICIO " +
            "from SIN_CAT_TIPO_SERVICIO " +
            "order by TIPO_SERVICIO";

    private static final String QUERY_PRODUCTO_POLIZA = "select " +
            "CVE_PROD_COMERCIAL, CVE_PROD_TECNICO " +
            "from SIN_CAT_PRODUCTO_CASHFLOW " +
            "where CVE_PROD_COMERCIAL = ? and CVE_PROD_TECNICO = ?";

    private static final String QUERY_TIPOS_SINIESTROS = "select " +
            "CVE_TIPO_SINIESTRO, TIPO_SINIESTRO " +
            "from SIN_CAT_TIPO_SINIESTRO " +
            "order by TIPO_SINIESTRO";

    private static final String QUERY_TIPOS_TELEFONO = "select " +
            "CVE_TIPO_TELEFONO, TIPO_TELEFONO, CVE_TIPO_TELEFONO_AMIS " +
            "from SIN_CAT_TIPO_TELEFONO " +
            "order by TIPO_TELEFONO";

    private static final String QUERY_TIPO_TELEFONO = "select " +
            "CVE_TIPO_TELEFONO, TIPO_TELEFONO, CVE_TIPO_TELEFONO_AMIS " +
            "from SIN_CAT_TIPO_TELEFONO " +
            "where CVE_TIPO_TELEFONO = ? " +
            "order by TIPO_TELEFONO";

    private static final String QUERY_TIPO_TELEFONO_AMIS = "select " +
            "CVE_TIPO_TELEFONO, TIPO_TELEFONO, CVE_TIPO_TELEFONO_AMIS " +
            "from SIN_CAT_TIPO_TELEFONO " +
            "where CVE_TIPO_TELEFONO_AMIS = ? " +
            "order by TIPO_TELEFONO";

    private static final String QUERY_TIPOS_TERCEROS = "select " +
            "CVE_TIPO_TERCERO, TIPO_TERCERO " +
            "from SIN_CAT_TIPO_TERCERO " +
            "order by TIPO_TERCERO";

    private static final String QUERY_TIPO_TERCERO = "select " +
            "CVE_TIPO_TERCERO, TIPO_TERCERO " +
            "from SIN_CAT_TIPO_TERCERO " +
            "where CVE_TIPO_TERCERO = ? " +
            "order by TIPO_TERCERO";

    private static final String QUERY_TIPOS_AUTOS = "select " +
            "CVE_TIPO_TERCERO_AUTO, TIPO_TERCERO_AUTO " +
            "from SIN_CAT_TIPO_TERCERO_AUTO " +
            "order by TIPO_TERCERO_AUTO";

    private static final String QUERY_TIPOS_UBICACIONES = "select " +
            "CVE_TIPO_UBICACION, TIPO_UBICACION, CVE_TIPO_UBICACION_AMIS " +
            "from SIN_CAT_TIPO_UBICACION " +
            "order by TIPO_UBICACION";

    private static final String QUERY_TIPO_UBICACION = "select " +
            "CVE_TIPO_UBICACION, TIPO_UBICACION, CVE_TIPO_UBICACION_AMIS " +
            "from SIN_CAT_TIPO_UBICACION " +
            "where TIPO_UBICACION = ? " +
            "order by TIPO_UBICACION";

    private static final String QUERY_TIPO_UBICACION_AMIS = "select " +
            "CVE_TIPO_UBICACION, TIPO_UBICACION, CVE_TIPO_UBICACION_AMIS " +
            "from SIN_CAT_TIPO_UBICACION " +
            "where CVE_TIPO_UBICACION_AMIS = ? " +
            "order by TIPO_UBICACION";

    private static final String QUERY_TIPO_VOLANTES = "select " +
            "CVE_TIPO_VOLANTE, TIPO_VOLANTE " +
            "from SIN_CAT_TIPO_VOLANTE " +
            "order by TIPO_VOLANTE";

    private static final String QUERY_VOLANTES_TERCERO_NO_AUTOS = "select " +
            "CVE_TIPO_VOLANTE_NOAUT, TIPO_VOLANTE_NOAUT " +
            "from SIN_CAT_TIPO_VOLANTE_NOAUT " +
            "order by TIPO_VOLANTE_NOAUT";

    private static final String QUERY_VOLANTES_RECUPERACIONES = "select " +
            "CVE_TIPO_VOLANTE_RECUP, TIPO_VOLANTE_RECUP " +
            "from SIN_CAT_TIPO_VOLANTE_RECUP " +
            "order by TIPO_VOLANTE_RECUP";

    private static final String QUERY_MATRIZ_COBERTURA = "select " +
            "distinct CVE_COBERTURA_AFECTADA " +
            "from SIN_CAT_MATRIZ_COBERTURA " +
            "where ID_CATEGORIA_VOLANTE = ? and CVE_TIPO_SINIESTRO = ? ";

    private static final String QUERY_MATRIZ_RESPONSABILIDAD = "select " +
            "cva.ID_CIRCUNSTANCIA_VEHICULO, cva.CIRCUNSTANCIA_VEHICULO, " +
            "cvb.ID_CIRCUNSTANCIA_VEHICULO, cvb.CIRCUNSTANCIA_VEHICULO, " +
            "r.CVE_RESPONSABILIDAD, r.RESPONSABILIDAD " +
            "from SIN_CAT_MATRIZ_RESPONSABILIDAD mr " +
            "inner join SIN_CAT_CIRCUNSTANCIA_VEHICULO cva on " +
            "mr.ID_CIRCUNSTANCIA_VEHICULO_A = cva.ID_CIRCUNSTANCIA_VEHICULO " +
            "inner join SIN_CAT_CIRCUNSTANCIA_VEHICULO cvb on " +
            "mr.ID_CIRCUNSTANCIA_VEHICULO_B = cvb.ID_CIRCUNSTANCIA_VEHICULO " +
            "inner join SIN_CAT_RESPONSABILIDAD r on " +
            "r.CVE_RESPONSABILIDAD = mr.CVE_RESPONSABILIDAD " +
            "where mr.ID_CIRCUNSTANCIA_VEHICULO_A = ? and mr.ID_CIRCUNSTANCIA_VEHICULO_B = ?";

    private static final String QUERY_COSTO_PARTE_AFECTADA = "select " +
            "p.ID_PARTE_AFECTADA, p.PARTE_AFECTADA, c.COSTO " +
            "from SIN_CAT_COSTO_PARTE_AFECTADA c " +
            "inner join SIN_CAT_PARTE_AFECTADA p on p.ID_PARTE_AFECTADA = c.ID_PARTE_AFECTADA " +
            "where c.CVE_CATEGORIA_VEHICULO = ? " +
            "and p.ID_PARTE_AFECTADA = ?";

    private static final String QUERY_TIPO_SINIESTRO = "select " +
            "CVE_TIPO_SINIESTRO, TIPO_SINIESTRO, CVE_TIPO_SINIESTRO_AMIS " +
            "from SIN_CAT_TIPO_SINIESTRO " +
            "where CVE_TIPO_SINIESTRO = ?";

    private static final String QUERY_TIPO_SINIESTRO_AMIS = "select " +
            "CVE_TIPO_SINIESTRO, TIPO_SINIESTRO, CVE_TIPO_SINIESTRO_AMIS " +
            "from SIN_CAT_TIPO_SINIESTRO " +
            "where CVE_TIPO_SINIESTRO_AMIS = ?";

    private static final String QUERY_CONFIGURACION = "select " +
            "CVE_CONFIGURACION, CONFIGURACION, VALOR_CONFIGURACION " +
            "from SIN_AJUSTE_CONFIGURACION " +
            "where CVE_CONFIGURACION = ?";

    private static final String QUERY_TIPO_IDENTIFICACION = "select " +
            "CVE_TIPO_IDENTIFICACION, TIPO_IDENTIFICACION " +
            "from SIN_CAT_TIPO_IDENTIFICACION " +
            "where CVE_TIPO_IDENTIFICACION = ? ";

    private static final String QUERY_COLORES_VEHICULO = "select " +
            "ID_VEHICULO_COLOR, VEHICULO_COLOR " +
            "from SIN_CAT_VEHICULO_COLOR " +
            "order by VEHICULO_COLOR";


    private static final String QUERY_COLOR_VEHICULO = "select " +
            "ID_VEHICULO_COLOR, VEHICULO_COLOR " +
            "from SIN_CAT_VEHICULO_COLOR " +
            "where ID_VEHICULO_COLOR = ?" +
            "order by VEHICULO_COLOR";

    private static final String QUERY_TIPOS_PERSONA = "select " +
            "CVE_TIPO_PERSONA, TIPO_PERSONA, CVE_TIPO_PERSONA_AMIS " +
            "from SIN_CAT_TIPO_PERSONA " +
            "order by TIPO_PERSONA";

    private static final String QUERY_TIPO_PERSONA = "select " +
            "CVE_TIPO_PERSONA, TIPO_PERSONA, CVE_TIPO_PERSONA_AMIS " +
            "from SIN_CAT_TIPO_PERSONA " +
            "where CVE_TIPO_PERSONA = ? ";

    private static final String QUERY_TIPO_PERSONA_AMIS = "select " +
            "CVE_TIPO_PERSONA, TIPO_PERSONA, CVE_TIPO_PERSONA_AMIS " +
            "from SIN_CAT_TIPO_PERSONA " +
            "where CVE_TIPO_PERSONA_AMIS = ? ";

    private static final String QUERY_VEHICULO_AMIS = "select " +
            "ID_TRANSPORTE_OCRA, ID_MARCA_OCRA, ID_OCRA, " +
            "CVE_TIPO_VEHICULO, CVE_ARMADORA_VEHICULO, CVE_CARROCERIA_VEHICULO " +
            "from SIN_CAT_VEHICULO_AMIS " +
            "where ID_TRANSPORTE_OCRA = ? and ID_MARCA_OCRA = ? and ID_OCRA = ?";

    private static final String QUERY_VEHICULO_AMIS_GNP = "select " +
            "ID_TRANSPORTE_OCRA, ID_MARCA_OCRA, ID_OCRA, " +
            "CVE_TIPO_VEHICULO, CVE_ARMADORA_VEHICULO, CVE_CARROCERIA_VEHICULO " +
            "from SIN_CAT_VEHICULO_AMIS " +
            "where CVE_TIPO_VEHICULO = ? and CVE_ARMADORA_VEHICULO = ? and CVE_CARROCERIA_VEHICULO = ?";

    private static final String QUERY_VEHICULO_DEFAULT_AMIS = "select " +
            "ID_TRANSPORTE_OCRA, ID_MARCA_OCRA, ID_OCRA, " +
            "CVE_TIPO_VEHICULO, NULL, NULL " +
            "from SIN_CAT_VEHICULO_DEFAULT_AMIS " +
            "where CVE_TIPO_VEHICULO = ?";

    private static final String QUERY_USO_VEHICULO_AMIS = "select " +
            "CVE_USO_VEHICULO, ID_USO_VEHICULO_AMIS " +
            "from SIN_CAT_USO_VEHICULO_AMIS " +
            "where CVE_USO_VEHICULO = ?";

    private static final String QUERY_MUNICIPIO = "select " +
            "CVE_MUNICIPIO, ID_MUNICIPIO_AMIS, CVE_ESTADO, ID_ESTADO_AMIS " +
            "from SIN_CAT_MUNICIPIO_AMIS " +
            "where CVE_ESTADO = ?  and " +
            "CVE_MUNICIPIO = ?";

    private static final String QUERY_MUNICIPIO_AMIS = "select " +
            "CVE_MUNICIPIO, ID_MUNICIPIO_AMIS, CVE_ESTADO, ID_ESTADO_AMIS " +
            "from SIN_CAT_MUNICIPIO_AMIS " +
            "where ID_ESTADO_AMIS = ?  and " +
            "ID_MUNICIPIO_AMIS = ?";

    private static final String QUERY_ESTADO = "select " +
            "distinct CVE_ESTADO, ID_ESTADO_AMIS " +
            "from SIN_CAT_MUNICIPIO_AMIS " +
            "where CVE_ESTADO = ?";

    private static final String QUERY_ESTADO_AMIS = "select " +
            "distinct CVE_ESTADO, ID_ESTADO_AMIS " +
            "from SIN_CAT_MUNICIPIO_AMIS " +
            "where ID_ESTADO_AMIS = ?";


    private static final String QUERY_OTRA_ASEGURADORA_AMIS = "select " +
            "CVE_OTRA_ASEGURADORA " +
            "from SIN_CAT_ASEGURADORAS_AMIS " +
            "where CVE_ASEGURADORA_AMIS = ?";



    private static final RowMapper<CatalogoId> MAPPER_CATALOGO_ID = cursor -> {
        CatalogoId result = new CatalogoId();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        return result;
    };

    private static final RowMapper<CatalogoIdAmis> MAPPER_CATALOGO_ID_AMIS = cursor -> {
        CatalogoIdAmis result = new CatalogoIdAmis();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        result.setClaveAmis(cursor.getString(2));
        return result;
    };

    private static final RowMapper<ImpactoVehiculoAmis> MAPPER_PRIMER_IMPACTO_AMIS = cursor -> {
        ImpactoVehiculoAmis result = new ImpactoVehiculoAmis();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        result.setClaveAmis(cursor.getString(2));
        result.setAreaAmis(cursor.getLong(3));
        return result;
    };

    private static final RowMapper<CatalogoClave> MAPPER_CATALOGO_CLAVE = cursor -> {
        CatalogoClave result = new CatalogoClave();
        result.setClave(cursor.getString(0));
        result.setNombre(cursor.getString(1));
        return result;
    };

    private static final RowMapper<CatalogoClaveAmis> MAPPER_CATALOGO_CLAVE_AMIS = cursor -> {
        CatalogoClaveAmis result = new CatalogoClaveAmis();
        result.setClave(cursor.getString(0));
        result.setNombre(cursor.getString(1));
        result.setClaveAmis(cursor.getString(2));
        return result;
    };

    private static final RowMapper<CentroRemate> MAPPER_CENTRO_REMATES = cursor -> {
        CentroRemate result = new CentroRemate();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        result.setDireccion(cursor.getString(2));
        return result;
    };

    private static final RowMapper<Contratista> MAPPER_CONTRATISTAS = cursor -> {
        Contratista result = new Contratista();
        result.setId(cursor.getLong(0));
        result.setSupervisoria(cursor.getString(1));
        result.setNombre(cursor.getString(2));
        return result;
    };

    private static final RowMapper<HospitalResumen> MAPPER_HOSPITALES = cursor -> {
        HospitalResumen result = new HospitalResumen();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        result.setSupervisoria(cursor.getString(2));
        return result;
    };

    private static final RowMapper<Hospital> MAPPER_HOSPITAL = cursor -> {
        Hospital result = new Hospital();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        result.setSupervisoria(cursor.getString(2));
        result.setDireccion(cursor.getString(3));
        result.setCorreo(cursor.getString(4));
        return result;
    };

    private static final RowMapper<Telefono> MAPPER_HOSPITAL_TELEFONO = cursor -> {
        Telefono result = new Telefono();
        result.setTelefono(cursor.getString(0));
        return result;
    };

    private static final RowMapper<Cobertura> MAPPER_MATRIZ_COBERTURA = cursor -> {
        Cobertura result = new Cobertura();
        result.setClave(cursor.getString(0));
        return result;
    };

    private static final RowMapper<Responsable> MAPPER_MATRIZ_RESPONSABILIDAD = cursor -> {
        Responsable result = new Responsable();
        result.setCircunstanciaA(new CatalogoId());
        result.getCircunstanciaA().setId(cursor.getLong(0));
        result.getCircunstanciaA().setNombre(cursor.getString(1));
        result.setCircunstanciaB(new CatalogoId());
        result.getCircunstanciaB().setId(cursor.getLong(2));
        result.getCircunstanciaB().setNombre(cursor.getString(3));
        result.setResponsabilidad(new CatalogoClave());
        result.getResponsabilidad().setClave(cursor.getString(4));
        result.getResponsabilidad().setNombre(cursor.getString(5));
        return result;
    };

    private static final RowMapper<CostoParteAfectada> MAPPER_COSTO_PARTE_AFECTADA = cursor -> {
        CostoParteAfectada result = new CostoParteAfectada();
        result.setParte(new CatalogoId());
        result.getParte().setId(cursor.getLong(0));
        result.getParte().setNombre(cursor.getString(1));
        result.setCosto(cursor.getDouble(2));
        return result;
    };

    private static final RowMapper<EstadoAmis> MAPPER_ESTADO_AMIS = cursor -> {
        EstadoAmis result = new EstadoAmis();
        result.setClave(cursor.getString(0));
        result.setClaveAmis(cursor.getInt(1));
        return result;
    };

    private static final RowMapper<MunicipioAmis> MAPPER_MUNICIPIO_AMIS = cursor -> {
        MunicipioAmis result = new MunicipioAmis();
        result.setClave(cursor.getString(0));
        result.setClaveAmis(cursor.getInt(1));
        result.setEstado(new EstadoAmis());
        result.getEstado().setClave(cursor.getString(2));
        result.getEstado().setClaveAmis(cursor.getInt(3));
        return result;
    };

    private static final RowMapper<Configuracion> MAPPER_CONFIGURACION = cursor -> {
        Configuracion result = new Configuracion();
        result.setClave(cursor.getString(0));
        result.setNombre(cursor.getString(1));
        result.setValor(cursor.getString(2));
        return result;
    };

    private static final RowMapper<VehiculoAmis> MAPPER_VEHICULO_AMIS = cursor -> {
        VehiculoAmis result = new VehiculoAmis();
        result.setTransporte(cursor.getLong(0));
        result.setMarca(cursor.getLong(1));
        result.setTipo(cursor.getLong(2));
        result.setTipoVehiculo(cursor.getString(3));
        result.setArmadora(cursor.getString(4));
        result.setCarroceria(cursor.getString(5));
        return result;
    };

    private static final RowMapper<UsoVehiculo> MAPPER_USO_VEHICULO_AMIS = cursor -> {
        UsoVehiculo result = new UsoVehiculo();
        result.setUsoVehiculo(cursor.getString(0));
        result.setUsoAmis(cursor.getLong(1));
        return result;
    };

    private static final RowMapper<ParteAfectada> MAPPER_PARTE_AFECTADA = cursor -> {
        ParteAfectada result = new ParteAfectada();
        result.setId(cursor.getLong(0));
        result.setNombre(cursor.getString(1));
        result.setClaveAmis(cursor.getString(2));
        result.setAreaAmis(cursor.getString(3));
        result.setArea(new CatalogoId());
        result.getArea().setId(cursor.getLong(4));
        result.getArea().setNombre(cursor.getString(5));
        return result;
    };

    private static final RowMapper<TipoLicencia> MAPPER_TIPO_LICENCIA = cursor -> {
        TipoLicencia result = new TipoLicencia();
        result.setClave(cursor.getString(0));
        result.setNombre(cursor.getString(1));
        return result;
    };

    private static final RowMapper<EstadoTipoLicencia> MAPPER_ESTADO_TIPO_LICENCIA = cursor -> {
        EstadoTipoLicencia result = new EstadoTipoLicencia();
        result.setEstado(cursor.getString(0));
        result.setEstadoAmis(cursor.getString(1));
        return result;
    };

    private static final RowMapper<ProductoCashflow> MAPPER_PRODUCTO_POLIZA = cursor -> {
        ProductoCashflow result = new ProductoCashflow();
        result.setProductoComercial(cursor.getString(0));
        result.setProductoTecnico(cursor.getString(1));
        return result;
    };

    private static final RowMapper<String> MAPPER_ASEGURADORA_AMIS = cursor ->
            cursor.getString(0);

    public SiniestrosDataSQLiteImpl(Context context) {
        super(context, DB_NAME);
    }

    @Override
    protected String getSqlFilename() {
        return "db/sinau.sql";
    }

    @Override
    public Single<List<CatalogoClave>> getTiposAbogado() {
        return getSingleList(QUERY_ESPECIALIDAD_SERVICIO, MAPPER_CATALOGO_CLAVE, "AB");
    }

    @Override
    public Single<List<CatalogoClave>> getGestionesAbogado() {
        return getSingleList(QUERY_TIPO_GESTION, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getComunicacionesAbogado() {
        return getSingleList(QUERY_COMUNICACION_ABOGADO, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosCancelacionAjuste() {
        return getSingleList(QUERY_MOTIVOS_CANCELACION, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosRechazoAjuste() {
        return getSingleList(QUERY_MOTIVOS_RECHAZO, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosCierreAjuste() {
        return getSingleList(QUERY_CIERRES, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposAmbulancia() {
        return getSingleList(QUERY_ESPECIALIDAD_SERVICIO, MAPPER_CATALOGO_CLAVE, "AM");
    }

    @Override
    public Single<List<CentroRemate>> getCentrosRemate() {
        return getSingleList(QUERY_CENTRO_REMATES, MAPPER_CENTRO_REMATES);
    }

    @Override
    public Single<CentroRemate> getCentroRemate(Long id) {
        return getSingleObject(QUERY_CENTRO_REMATE, MAPPER_CENTRO_REMATES, id.toString());
    }

    @Override
    public Single<List<Contratista>> getContratistas() {
        return getSingleList(QUERY_CONTRATISTAS, MAPPER_CONTRATISTAS);
    }

    @Override
    public Single<Contratista> getContratista(Long idContratista) {
        return getSingleObject(QUERY_CONTRATISTA, MAPPER_CONTRATISTAS, idContratista.toString());
    }

    @Override
    public Single<List<CatalogoClave>> getDocumentosPropiedad() {
        return getSingleList(QUERY_DOCUMENTO_PROPIEDAD, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<ProductoCashflow> getProductoCashflow(String comercial, String tecnico) {
        return getSingleObject(QUERY_PRODUCTO_POLIZA, MAPPER_PRODUCTO_POLIZA, comercial, tecnico);
    }


    @Override
    public Single<List<CatalogoClave>> getTiposIdentificacion() {
        return getSingleList(QUERY_IDENTIFICACIONES, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<CatalogoClave> getTipoIdentificacion(String clave) {
        return getSingleObject(QUERY_TIPO_IDENTIFICACION, MAPPER_CATALOGO_CLAVE, clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposGrua() {
        return getSingleList(QUERY_ESPECIALIDAD_SERVICIO, MAPPER_CATALOGO_CLAVE, "GR");
    }

    @Override
    public Single<Hospital> getHospital(Long id) {
        return Single.create(e -> {
            try {
                Hospital hospital = getObject(QUERY_HOSPITAL, MAPPER_HOSPITAL, id.toString());
                if (hospital != null) {
                    hospital.setTelefonos(getList(QUERY_HOSPITAL_TELEFONO, MAPPER_HOSPITAL_TELEFONO, id.toString()));
                }
                e.onSuccess(hospital);
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<HospitalResumen>> getHospitales(String supervisoria) {
        return getSingleList(QUERY_HOSPITALES, MAPPER_HOSPITALES, supervisoria);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosInvestigacion() {
        return getSingleList(QUERY_MOTIVO_INVESTIGACION, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoId>> getNivelLesion() {
        return getSingleList(QUERY_NIVEL_LESION, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<List<CatalogoId>> getAreasLesionadas() {
        return getSingleList(QUERY_AREAS_LESIONADAS, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getAreaAfectada(Long id) {
        return getSingleObject(QUERY_AREA_AFECTADA,MAPPER_CATALOGO_ID,id.toString());
    }

    @Override
    public Single<List<CatalogoIdAmis>> getPartesLesionadas(Long area) {
        return getSingleList(QUERY_PARTES_LESIONADAS, MAPPER_CATALOGO_ID_AMIS, area.toString());
    }

    @Override
    public Single<CatalogoIdAmis> getParteLesionada(Long id) {
        return getSingleObject(QUERY_PARTE_LESIONADA, MAPPER_CATALOGO_ID_AMIS, id.toString());
    }

    @Override
    public Single<CatalogoIdAmis> getParteLesionadaAmis(String claveAmis) {
        return getSingleObject(QUERY_PARTE_LESIONADA_AMIS, MAPPER_CATALOGO_ID_AMIS, claveAmis);
    }

    @Override
    public Single<List<CatalogoClave>> getNacionalidades() {
        return getSingleList(QUERY_NACIONALIDADES, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoId>> getRazonesNoDeducible() {
        return getSingleList(QUERY_NO_DEDUCIBLES, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getRazonNoDeducible(Long id) {
        return getSingleObject(QUERY_NO_DEDUCIBLE, MAPPER_CATALOGO_ID, id.toString());
    }

    public Single<CatalogoId> getObjetoPerdido(Long id) {
        return getSingleObject(QUERY_OBJETO_PERDIDO, MAPPER_CATALOGO_ID, id.toString());
    }

    @Override
    public Single<List<CatalogoClave>> getPosicionesOcupante() {
        return getSingleList(QUERY_POSICIONES_OCUPANTE, MAPPER_CATALOGO_CLAVE);
    }

    public Single<CatalogoClave> getPosicionOcupante(String clave) {
        return getSingleObject(QUERY_POSICION_OCUPANTE, MAPPER_CATALOGO_CLAVE, clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposPatrulla() {
        return getSingleList(QUERY_MOTIVO_PATRULLA, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoId>> getTiposPoliza() {
        return getSingleList(QUERY_TIPO_POLIZA, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getCondicionamientosPoliza(Long id) {
        return getSingleObject(QUERY_CONDICIONAMIENTO_POLIZA, MAPPER_CATALOGO_ID, id.toString());
    }

    @Override
    public Single<List<CatalogoId>> getCondicionamientosPoliza() {
        return getSingleList(QUERY_CONDICIONAMIENTOS_POLIZA, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<List<CatalogoClave>> getVolantesRecuperacion() {
        return getSingleList(QUERY_VOLANTES_RECUPERACIONES, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoId>> getTiposRecuperacionMonetaria() {
        return getSingleList(QUERY_TIPOS_RECUPERACION_MONETARIA, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getTipoRecuperacionMonetaria(Long id) {
        return getSingleObject(QUERY_TIPO_RECUPERACION_MONETARIA, MAPPER_CATALOGO_ID, id.toString());
    }

    @Override
    public Single<List<CatalogoClave>> getTiposRecuperacionMonetariaTarjeta() {
        return getSingleList(QUERY_RECUPERACION_TARJETA, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposServicio() {
        return getSingleList(QUERY_SERVICIOS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposSiniestro() {
        return getSingleList(QUERY_TIPOS_SINIESTROS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<Supervisoria>> getSupervisorias() {
        return Single.error(new UnsupportedOperationException("Esta función solo se encuentra disponible en modo online"));
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposTelefono() {
        return getSingleList(QUERY_TIPOS_TELEFONO, MAPPER_CATALOGO_CLAVE_AMIS);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoTelefono(String clave) {
        return getSingleObject(QUERY_TIPO_TELEFONO, MAPPER_CATALOGO_CLAVE_AMIS, clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoTelefonoAmis(String claveAmis) {
        return getSingleObject(QUERY_TIPO_TELEFONO_AMIS, MAPPER_CATALOGO_CLAVE_AMIS, claveAmis);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposTercero() {
        return getSingleList(QUERY_TIPOS_TERCEROS, MAPPER_CATALOGO_CLAVE);
    }


    @Override
    public Single<CatalogoClave> getTipoTercero(String clave) {
        return getSingleObject(QUERY_TIPO_TERCERO, MAPPER_CATALOGO_CLAVE, clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposTerceroAuto() {
        return getSingleList(QUERY_TIPOS_AUTOS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoId>> getCategoriasTerceroNoAuto() {
        return getSingleList(QUERY_CATEGORIAS_NO_AUTO, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getCategoriaTerceroNoAuto(Long id) {
        return getSingleObject(QUERY_CATEGORIA_NO_AUTO, MAPPER_CATALOGO_ID, id.toString());
    }

    @Override
    public Single<List<CatalogoId>> getTiposTerceroNoAuto(Long categoria) {
        return getSingleList(QUERY_TIPOS_NO_AUTOS, MAPPER_CATALOGO_ID, categoria.toString());
    }

    @Override
    public Single<CatalogoId> getTipoTerceroNoAuto(Long categoria, Long id) {
        return getSingleObject(QUERY_TIPO_NO_AUTO, MAPPER_CATALOGO_ID, categoria.toString(), id.toString());
    }

    @Override
    public Single<List<CatalogoClave>> getVolantesTerceroNoAuto() {
        return getSingleList(QUERY_VOLANTES_TERCERO_NO_AUTOS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposUbicacion() {
        return getSingleList(QUERY_TIPOS_UBICACIONES, MAPPER_CATALOGO_CLAVE_AMIS);
    }

    @Override
    public Single<List<CatalogoId>> getAreasAfectadas() {
        return getSingleList(QUERY_AREAS_AFECTADAS, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<List<ParteAfectada>> getPartesAfectadas(Long area) {
        return getSingleList(QUERY_PARTES_AFECTADAS, MAPPER_PARTE_AFECTADA, area.toString());
    }

    @Override
    public Single<ParteAfectada> getParteAfectada(Long area, Long id) {
        return getSingleObject(QUERY_PARTE_AFECTADA, MAPPER_PARTE_AFECTADA, area.toString(), id.toString());
    }

    @Override
    public Single<ParteAfectada> getParteAfectada(Long id){
        return getSingleObject(QUERY_PARTE_AFECTADA_ID, MAPPER_PARTE_AFECTADA, id.toString());
    }

    @Override
    public Single<ParteAfectada> getParteAfectadaAmis(String claveAmis) {
        return getSingleObject(QUERY_PARTE_AFECTADA_AMIS, MAPPER_PARTE_AFECTADA, claveAmis);
    }

    @Override
    public Single<List<ImpactoVehiculoAmis>> getImpactosVehiculo() {
        return getSingleList(QUERY_PUNTOS_PRIMER_IMPACTO, MAPPER_PRIMER_IMPACTO_AMIS);
    }

    @Override
    public Single<ImpactoVehiculoAmis> getImpactoVehiculo(Long id) {
        return getSingleObject(QUERY_PUNTO_PRIMER_IMPACTO, MAPPER_PRIMER_IMPACTO_AMIS, id.toString());
    }

    @Override
    public Single<ImpactoVehiculoAmis> getImpactoVehiculoAmis(String claveAmis) {
        return getSingleObject(QUERY_PUNTO_PRIMER_IMPACTO_AMIS, MAPPER_PRIMER_IMPACTO_AMIS, claveAmis.toString());
    }

    @Override
    public Single<List<CatalogoId>> getCircustanciasVehiculo() {
        return getSingleList(QUERY_CIRCUSNTANCIAS_VEHICULO, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getCircustanciaVehiculo(Long id) {
        return getSingleObject(QUERY_CIRCUSNTANCIA_VEHICULO, MAPPER_CATALOGO_ID, id.toString());
    }

    @Override
    public Single<List<CatalogoClave>> getTiposVolante() {
        return getSingleList(QUERY_TIPO_VOLANTES, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<List<Cobertura>> getCoberturasVolante(Integer categoria, String tipoSiniestro) {
        return getSingleList(QUERY_MATRIZ_COBERTURA, MAPPER_MATRIZ_COBERTURA, categoria.toString(), tipoSiniestro);
    }

    @Override
    public Single<List<Responsable>> getResponsables(Long circustanciaA, Long circustanciaB) {
        return getSingleList(QUERY_MATRIZ_RESPONSABILIDAD, MAPPER_MATRIZ_RESPONSABILIDAD, circustanciaA.toString(), circustanciaB.toString());
    }

    @Override
    public Single<CatalogoClave> getResponsabilidad(String clave) {
        return getSingleObject(QUERY_RESPONSABILIDAD, MAPPER_CATALOGO_CLAVE, clave);
    }

    @Override
    public Single<List<CostoParteAfectada>> getCostoParteAfectada(String categoria, List<Long> partes) {
        return Single.create(e -> {
            List<CostoParteAfectada> result = new ArrayList<>();
            for (Long parte : partes) {
                CostoParteAfectada costo = getObject(QUERY_COSTO_PARTE_AFECTADA, MAPPER_COSTO_PARTE_AFECTADA, categoria, parte.toString());
                if (costo != null) {
                    result.add(costo);
                }
            }
            e.onSuccess(result);
        });
    }

    @Override
    public Single<Configuracion> getConfiguracion(@Path("clave") String clave) {
        return getSingleObject(QUERY_CONFIGURACION, MAPPER_CONFIGURACION, clave);
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getGeneros() {
        return getSingleList(QUERY_GENEROS, MAPPER_CATALOGO_CLAVE_AMIS);
    }

    @Override
    public Single<CatalogoClaveAmis> getGenero(String clave) {
        return getSingleObject(QUERY_GENERO, MAPPER_CATALOGO_CLAVE_AMIS, clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getGeneroAmis(String claveAmis) {
        return getSingleObject(QUERY_GENERO_AMIS, MAPPER_CATALOGO_CLAVE_AMIS, claveAmis);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposLicencia(String estado) {
        return getSingleList(QUERY_TIPOS_LICENCIA, MAPPER_CATALOGO_CLAVE, estado);
    }

    @Override
    public Single<TipoLicencia> getTipoLicencia(String clave) {
        return Single.create(e -> {
            TipoLicencia result = getObject(QUERY_TIPO_LICENCIA, MAPPER_TIPO_LICENCIA, clave);
            if(result != null) {
                result.setEstados(getList(QUERY_ESTADOS_TIPO_LICENCIA, MAPPER_ESTADO_TIPO_LICENCIA, clave));
            }
            e.onSuccess(result);
        });
    }

    @Override
    public Single<List<CatalogoId>> getColoresVehiculo() {
        return getSingleList(QUERY_COLORES_VEHICULO, MAPPER_CATALOGO_ID);
    }

    @Override
    public Single<CatalogoId> getColorVehiculo(Long id) {
        return getSingleObject(QUERY_COLOR_VEHICULO, MAPPER_CATALOGO_ID, id.toString());
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoUbicacion(String clave) {
        return getSingleObject(QUERY_TIPO_UBICACION, MAPPER_CATALOGO_CLAVE_AMIS, clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoUbicacionAmis(Long claveAmis) {
        return getSingleObject(QUERY_TIPO_UBICACION_AMIS, MAPPER_CATALOGO_CLAVE_AMIS, claveAmis.toString());
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposPersona() {
        return getSingleList(QUERY_TIPOS_PERSONA, MAPPER_CATALOGO_CLAVE_AMIS);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoPersona(String clave) {
        return getSingleObject(QUERY_TIPO_PERSONA, MAPPER_CATALOGO_CLAVE_AMIS, clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoPersonaAmis(Long claveAmis) {
        return getSingleObject(QUERY_TIPO_PERSONA_AMIS, MAPPER_CATALOGO_CLAVE_AMIS, claveAmis.toString());
    }

    @Override
    public Single<List<VehiculoAmis>> getVehiculosAmis(String tipoVehiculo, String armadora, String carroceria) {
        return Single.create(e -> {
            List<VehiculoAmis> result = getList(QUERY_VEHICULO_AMIS_GNP, MAPPER_VEHICULO_AMIS, tipoVehiculo, armadora, carroceria);
            if(result.isEmpty()) {
                result = getList(QUERY_VEHICULO_DEFAULT_AMIS, MAPPER_VEHICULO_AMIS, tipoVehiculo);
            }
            e.onSuccess(result);
        });
    }

    @Override
    public Single<List<VehiculoAmis>> getVehiculosAmis(Long transporte, Long marca, Long tipo) {
        return getSingleList(QUERY_VEHICULO_AMIS, MAPPER_VEHICULO_AMIS, transporte.toString(), marca.toString(), tipo.toString());
    }

    @Override
    public Single<List<UsoVehiculo>> getUsoVehiculosAmis(String usoVehiculo) {
        return getSingleList(QUERY_USO_VEHICULO_AMIS, MAPPER_USO_VEHICULO_AMIS, usoVehiculo);
    }

    @Override
    public Single<MunicipioAmis> getMunicipio(String estado, String municipio) {
        return getSingleObject(QUERY_MUNICIPIO, MAPPER_MUNICIPIO_AMIS, estado,municipio);
    }

    @Override
    public Single<MunicipioAmis> getMunicipioAmis(Integer estadoAmis, Integer municipioAmis) {
        return getSingleObject(QUERY_MUNICIPIO_AMIS, MAPPER_MUNICIPIO_AMIS, estadoAmis.toString(),municipioAmis.toString());
    }

    @Override
    public Single<EstadoAmis> getEstado(String estado) {
        return getSingleObject(QUERY_ESTADO, MAPPER_ESTADO_AMIS, estado);
    }

    @Override
    public Single<EstadoAmis> getEstadoAmis(Integer estadoAmis) {
        return getSingleObject(QUERY_ESTADO_AMIS, MAPPER_ESTADO_AMIS, estadoAmis.toString());
    }

    @Override
    public Single<String> getOtraAseguradoraAmis(Integer claveAmis) {
        return getSingleObject(QUERY_OTRA_ASEGURADORA_AMIS,MAPPER_ASEGURADORA_AMIS, claveAmis.toString());
    }

}
