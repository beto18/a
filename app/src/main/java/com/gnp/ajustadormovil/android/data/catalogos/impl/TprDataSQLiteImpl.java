package com.gnp.ajustadormovil.android.data.catalogos.impl;

import android.content.Context;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.catalogos.TprData;
import com.gnp.ajustadormovil.android.data.sqlite.RowMapper;
import com.gnp.ajustadormovil.android.data.sqlite.SQLiteData;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Amis;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Armadora;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Carroceria;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;

import java.util.List;

import io.reactivex.Single;

public class TprDataSQLiteImpl extends SQLiteData implements TprData {

    private static final String TAG = TprDataSQLiteImpl.class.getSimpleName();

    private static final String DB_NAME = Utils.DATA_TPR_NAME;

    private static final String QUERY_COBERTURAS = "select CVE_COBERTURA, COBERTURA " +
            "from TPR_COBERTURA " +
            "order by COBERTURA";

    private static final String QUERY_COBERTURA = "select CVE_COBERTURA, COBERTURA, " +
            "VERSION, FCH_INICIO_VIGENCIA, FCH_FIN_VIGENCIA, COBERTURA_NOMBRE, COBERTURA_SIGLAS " +
            "from TPR_COBERTURA " +
            "where CVE_COBERTURA = ?";

    private static final String QUERY_TIPO_VEHICULOS = "select CVE_TIPO_VEHICULO, TIPO_VEHICULO " +
            "from TPR_TIPO_VEHICULO " +
            "order by TIPO_VEHICULO";


    private static final String QUERY_TIPO_VEHICULO = "select CVE_TIPO_VEHICULO, TIPO_VEHICULO " +
            "from TPR_TIPO_VEHICULO " +
            "where CVE_TIPO_VEHICULO = ? " +
            "order by TIPO_VEHICULO";

    private static final String QUERY_ARMADORAS = "select CVE_ARMADORA_VEHICULO, ARMADORA_VEHICULO " +
            "from TPR_ARMADORA_VEHICULO " +
            "where CVE_TIPO_VEHICULO = ? " +
            "order by  ARMADORA_VEHICULO";

    private static final String QUERY_ARMADORA = "select a.CVE_ARMADORA_VEHICULO, a.ARMADORA_VEHICULO, " +
            "t.CVE_TIPO_VEHICULO, t.TIPO_VEHICULO " +
            "from TPR_ARMADORA_VEHICULO a" +
            "inner join TPR_TIPO_VEHICULO t on a.CVE_TIPO_VEHICULO = t.CVE_TIPO_VEHICULO " +
            "where a.CVE_TIPO_VEHICULO = ? and a.CVE_ARMADORA_VEHICULO = ? ";

    private static final String QUERY_CARROCERIAS = "select CVE_CARROCERIA_VEHICULO, CARROCERIA_VEHICULO " +
            "from TPR_CARROCERIA_VEHICULO " +
            "where CVE_TIPO_VEHICULO = ? and CVE_ARMADORA_VEHICULO = ? " +
            "order by CARROCERIA_VEHICULO";

    private static final String QUERY_CARROCERIA = "select c.CVE_CARROCERIA_VEHICULO, c.CARROCERIA_VEHICULO, " +
            "a.CVE_ARMADORA_VEHICULO, a.ARMADORA_VEHICULO, " +
            "t.CVE_TIPO_VEHICULO, t.TIPO_VEHICULO " +
            "from TPR_CARROCERIA_VEHICULO c " +
            "inner join TPR_ARMADORA_VEHICULO a on " +
                "c.CVE_TIPO_VEHICULO = a.CVE_TIPO_VEHICULO and " +
                "c.CVE_ARMADORA_VEHICULO = a.CVE_ARMADORA_VEHICULO " +
            "inner join TPR_TIPO_VEHICULO t on " +
                "c.CVE_TIPO_VEHICULO = t.CVE_TIPO_VEHICULO " +
            "where c.CVE_TIPO_VEHICULO = ? and c.CVE_ARMADORA_VEHICULO = ? and c.CVE_CARROCERIA_VEHICULO = ?";

    private static final String QUERY_MODELOS = "select distinct MODELO_VEHICULO " +
            "from TPR_MARCA_MODELO_VEHICULO " +
            "where CVE_TIPO_VEHICULO = ? and CVE_ARMADORA_VEHICULO = ? and CVE_CARROCERIA_VEHICULO = ? " +
            "order by MODELO_VEHICULO";

    private static final String QUERY_VERSIONES = "select CVE_VERSION_VEHICULO, MARCA_VEHICULO " +
            "from TPR_MARCA_MODELO_VEHICULO " +
            "where CVE_TIPO_VEHICULO = ? and CVE_ARMADORA_VEHICULO = ? and CVE_CARROCERIA_VEHICULO = ? and MODELO_VEHICULO =  ?" +
            "order by MARCA_VEHICULO";

    private static final String QUERY_VERSION_VEHICULO = "select " +
                        "v.CVE_VERSION_VEHICULO, v.MODELO_VEHICULO, v.MARCA_VEHICULO, " +
                        "v.CVE_CATEGORIA_VEHICULO, v.VALOR_VEHICULO, v.NUMERO_PASAJEROS, " +
                        "v.ID_GUIA_EBC, v.CVE_CESVI, v.BAN_VEHICULO_BLINDADO_FABRICA,  " +
                        "v.BAN_VEHICULO_ALTO_VALOR, v.BAN_VEHICULO_ALTISIMO_VALOR, v.BAN_VEHICULO_ALTO_RIESGO, " +
                        "v.CVE_VERSION_VEHICULO_AMIS, v.CVE_CARROCERIA_VEHICULO_AMIS, v.CVE_ARMADORA_VEHICULO_AMIS, " +
                        "c.CVE_CARROCERIA_VEHICULO, c.CARROCERIA_VEHICULO, " +
                        "a.CVE_ARMADORA_VEHICULO, a.ARMADORA_VEHICULO, " +
                        "t.CVE_TIPO_VEHICULO, t.TIPO_VEHICULO " +
                    "from TPR_MARCA_MODELO_VEHICULO v " +
                    "inner join TPR_CARROCERIA_VEHICULO c on  " +
                        "v.CVE_TIPO_VEHICULO = c.CVE_TIPO_VEHICULO and " +
                        "v.CVE_ARMADORA_VEHICULO = c.CVE_ARMADORA_VEHICULO and " +
                        "v.CVE_CARROCERIA_VEHICULO = c.CVE_CARROCERIA_VEHICULO " +
                    "inner join TPR_ARMADORA_VEHICULO a on " +
                        "v.CVE_TIPO_VEHICULO = a.CVE_TIPO_VEHICULO and " +
                        "v.CVE_ARMADORA_VEHICULO = a.CVE_ARMADORA_VEHICULO " +
                    "inner join TPR_TIPO_VEHICULO t on " +
                        "v.CVE_TIPO_VEHICULO = t.CVE_TIPO_VEHICULO " +
                    "where v.CVE_TIPO_VEHICULO = ? and v.CVE_ARMADORA_VEHICULO = ? and v.CVE_CARROCERIA_VEHICULO = ? and v.MODELO_VEHICULO = ? and v.CVE_VERSION_VEHICULO = ? ";

    private static final RowMapper<CatalogoClave> MAPPER_CATALOGO_CLAVE = cursor -> {
        CatalogoClave catalogo = new CatalogoClave();
        catalogo.setClave(cursor.getString(0));
        catalogo.setNombre(cursor.getString(1));
        return catalogo;
    };

    private static final RowMapper<Short> MAPPER_MODELO = cursor -> cursor.getShort(0);

    private static final RowMapper<VersionVehiculoResumen> MAPPER_VERSIONES = cursor -> {
        VersionVehiculoResumen version = new VersionVehiculoResumen();
        version.setVersion(cursor.getString(0));
        version.setNombre(cursor.getString(1));
        return version;
    };

    private static final RowMapper<Armadora> MAPPER_ARMADORA = cursor -> {
        Armadora armadora = new Armadora();
        armadora.setClave(cursor.getString(0));
        armadora.setNombre(cursor.getString(1));
        armadora.setTipoVehiculo(new CatalogoClave());
        armadora.getTipoVehiculo().setClave(cursor.getString(2));
        armadora.getTipoVehiculo().setNombre(cursor.getString(3));
        return armadora;
    };

    private static final RowMapper<Carroceria> MAPPER_CARROCERIA = cursor -> {
        Carroceria carroceria = new Carroceria();
        carroceria.setClave(cursor.getString(0));
        carroceria.setNombre(cursor.getString(1));
        carroceria.setArmadora(new Armadora());
        carroceria.getArmadora().setClave(cursor.getString(2));
        carroceria.getArmadora().setNombre(cursor.getString(3));
        carroceria.getArmadora().setTipoVehiculo(new CatalogoClave());
        carroceria.getArmadora().getTipoVehiculo().setClave(cursor.getString(4));
        carroceria.getArmadora().getTipoVehiculo().setNombre(cursor.getString(5));
        return carroceria;
    };

    private static final RowMapper<VersionVehiculo> MAPPER_VERSION_VEHICULO = cursor -> {
        VersionVehiculo version = new VersionVehiculo();
        version.setVersion(cursor.getString(0));
        version.setModelo(cursor.getShort(1));
        version.setNombre(cursor.getString(2));
        version.setCategoria(cursor.getString(3));
        version.setValor(cursor.getFloat(4));
        version.setNumeroPasajeros(cursor.getString(5));
        version.setGuiaEbc(cursor.getString(6));
        version.setCesvi(cursor.getString(7));
        if(cursor.getString(8) != null) {
            version.setBlindado(cursor.getString(8).trim().equals("1"));
        }
        if(cursor.getString(9) != null) {
            version.setAltoValor(cursor.getString(9).trim().equals("1"));
        }
        if(cursor.getString(10) != null) {
            version.setAltisimoValor(cursor.getString(10).trim().equals("1"));
        }
        if(cursor.getString(11) != null) {
            version.setAltoRiesgo(cursor.getString(11).trim().equals("1"));
        }
        version.setAmis(new Amis());
        version.getAmis().setVersion(cursor.getString(12));
        version.getAmis().setCarroceria(cursor.getString(13));
        version.getAmis().setArmadora(cursor.getString(14));
        version.setCarroceria(new Carroceria());
        version.getCarroceria().setClave(cursor.getString(15));
        version.getCarroceria().setNombre(cursor.getString(16));
        version.getCarroceria().setArmadora(new Armadora());
        version.getCarroceria().getArmadora().setClave(cursor.getString(17));
        version.getCarroceria().getArmadora().setNombre(cursor.getString(18));
        version.getCarroceria().getArmadora().setTipoVehiculo(new CatalogoClave());
        version.getCarroceria().getArmadora().getTipoVehiculo().setClave(cursor.getString(19));
        version.getCarroceria().getArmadora().getTipoVehiculo().setNombre(cursor.getString(20));
        return version;
    };

    private static final RowMapper<Cobertura> MAPPER_COBERTURA = cursor -> {
        Cobertura result = new Cobertura();
        result.setClave(cursor.getString(0));
        result.setNombre(cursor.getString(1));
        result.setVersion(cursor.getLong(2));
        try {
            result.setInicioVigencia(DateUtils.dateIso(cursor.getString(3)));
            result.setFinVigencia(DateUtils.dateIso(cursor.getString(4)));
        } catch (Exception e){
            Log.w(TAG, "Error al parsear las fechas");
        }
        result.setDescripcion(cursor.getString(5));
        result.setSiglas(cursor.getString(6));
        return result;
    };

    public TprDataSQLiteImpl(Context context) {
        super(context, DB_NAME);
    }

    @Override
    protected String getSqlFilename() {
        return "db/tpr.sql";
    }

    @Override
    public Single<List<CatalogoClave>> getCoberturas() {
        return getSingleList(QUERY_COBERTURAS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<Cobertura> getCobertura(String clave) {
        return getSingleObject(QUERY_COBERTURA, MAPPER_COBERTURA, clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposVehiculo() {
        return getSingleList(QUERY_TIPO_VEHICULOS, MAPPER_CATALOGO_CLAVE);
    }

    @Override
    public Single<CatalogoClave> getTipoVehiculo(String tipoVehiculo) {
        return getSingleObject(QUERY_TIPO_VEHICULO, MAPPER_CATALOGO_CLAVE, tipoVehiculo);
    }

    @Override
    public Single<List<CatalogoClave>> getArmadoras(String tipoVehiculo) {
        return getSingleList(QUERY_ARMADORAS, MAPPER_CATALOGO_CLAVE, tipoVehiculo);
    }

    @Override
    public Single<Armadora> getArmadora(String tipo, String armadora) {
        return getSingleObject(QUERY_ARMADORA, MAPPER_ARMADORA, armadora);
    }

    @Override
    public Single<List<CatalogoClave>> getCarrocerias(String tipoVehiculo, String armadora) {
        return getSingleList(QUERY_CARROCERIAS, MAPPER_CATALOGO_CLAVE, tipoVehiculo, armadora);
    }

    @Override
    public Single<Carroceria> getCarroceria(String tipoVehiculo, String armadora, String carroceria) {
        return getSingleObject(QUERY_CARROCERIA, MAPPER_CARROCERIA, tipoVehiculo, armadora, carroceria);
    }

    @Override
    public Single<List<Short>> getModelos(String tipoVehiculo, String armadora, String carroceria) {
        return  getSingleList(QUERY_MODELOS, MAPPER_MODELO, tipoVehiculo, armadora, carroceria);
    }

    @Override
    public Single<List<VersionVehiculoResumen>> getVersiones(String tipoVehiculo, String armadora, String carroceria, Integer modelo) {
        return getSingleList(QUERY_VERSIONES, MAPPER_VERSIONES, tipoVehiculo, armadora, carroceria, String.valueOf(modelo));
    }

    @Override
    public Single<VersionVehiculo> getVersion(String tipoVehiculo, String armadora, String carroceria, Integer modelo, String version) {
        return getSingleObject(QUERY_VERSION_VEHICULO, MAPPER_VERSION_VEHICULO, tipoVehiculo, armadora, carroceria, String.valueOf(modelo), version);
    }

}
