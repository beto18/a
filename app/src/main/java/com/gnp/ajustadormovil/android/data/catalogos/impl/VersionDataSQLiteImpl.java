package com.gnp.ajustadormovil.android.data.catalogos.impl;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gnp.ajustadormovil.android.data.catalogos.VersionData;

public class VersionDataSQLiteImpl extends SQLiteOpenHelper implements VersionData {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME =  "version.db";
    private static final String DATABASE_CREATE = "create table DATA_VERSION (ID text primary key, VERSION text);";
    private static final String DATABASE_DROP = "drop table if exists DATA_VERSION";
    private static final String QUERY_SELECT_VERSION = "select VERSION from DATA_VERSION where ID = ?;";
    private static final String QUERY_UPDATE_VERSION = "update DATA_VERSION set VERSION = ? where ID = ?;";
    private static final String QUERY_INSERT_VERSION = "insert into DATA_VERSION (ID, VERSION) values (?, ?);";

    public VersionDataSQLiteImpl(Context ctx){
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DATABASE_DROP);
        onCreate(db);
    }

    @Override
    public String getVersion(String dataName){
        SQLiteDatabase db = this.getReadableDatabase();
        String version = null;
        Cursor cursor = db.rawQuery(QUERY_SELECT_VERSION, new String[]{dataName});
        if (cursor.moveToNext()) {
            version = cursor.getString(0);
        }
        cursor.close();
        return version;
    }

    @Override
    public void saveVersion(String dataName, String version){
        SQLiteDatabase db = this.getWritableDatabase();
        if(getVersion(dataName) == null){
            db.execSQL(QUERY_INSERT_VERSION, new Object[]{dataName, version});
        } else {
            db.execSQL(QUERY_UPDATE_VERSION, new Object[]{version, dataName});
        }
    }

}
