package com.gnp.ajustadormovil.android.data.context;

import com.gnp.ajustadormovil.android.models.context.Login;
import com.gnp.ajustadormovil.android.models.context.LoginCommand;
import com.gnp.ajustadormovil.android.models.context.LoginRequest;
import com.gnp.ajustadormovil.android.models.context.RefreshTokenCommand;
import com.gnp.ajustadormovil.android.models.login.User;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface AuthenticationData {

    @Headers("Content-Type: application/json")
    @POST("login/refresh")
    Single<User> getRefreshToken(@Body User userRefresh);

    @Headers("Content-Type: application/json")
    @POST("login")
    Single<User> getUser(@Body LoginRequest login);

    @Headers("Content-Type: application/json")
    @POST("login")
    Single<Login> login(@Body LoginCommand command);

    @Headers("Content-Type: application/json")
    @POST("login/refresh")
    Single<Login> refresh(@Body RefreshTokenCommand command);

}
