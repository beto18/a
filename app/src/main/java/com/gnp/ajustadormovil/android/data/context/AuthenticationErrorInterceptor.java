package com.gnp.ajustadormovil.android.data.context;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.gnp.ajustadormovil.android.error.AuthenticationException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceError;
import com.gnp.ajustadormovil.android.error.ServerException;
import com.gnp.ajustadormovil.android.error.UserNotFoundException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthenticationErrorInterceptor implements Interceptor {

    private static final String TAG = AuthenticationErrorInterceptor.class.getCanonicalName();

    /**
     * Valida si existe algún error en la autenticación del servicio.
     * Esto ocurriría si el código de error es <code>401</code>
     * @param response La respuesta del llamado al servicio.
     * @throws AuthenticationException Si no se puede acceder al servicio por problemas de autenticación.
     */
    private void assertAuthentication(Request request, Response response) {
        if(response.code() /100 == 4){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Error en autenticación: " + error);
            throw new AuthenticationException("Usuario o password inválido");
        }
    }

    /**
     * Valida si existe algún error en la acción que se intenta realizar.
     * Esto ocurriría si el código de error es <code>403</code>
     * @param response La respuesta del llamado al servicio.
     * @throws UserNotFoundException Si los datos no son correctos.
     */
    private void assertNotFound(Request request, Response response) {
        if(response.code() == 404){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Acción prohíbida: " + error);
            throw new UserNotFoundException("El usuario no existe");
        }
    }

    /**
     * Valida si existe algún error en el servidor.
     * Esto ocurriría si el código de error es <code>5xx</code>
     * @param response La respuesta del llamado al servicio.
     * @throws ServerException Si existe un problema en el servidor.
     */
    private void assertServerError(Request request, Response response) {
        if(response.code() / 100 == 5){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Error en el servidor: " + error);
            Crashlytics.log(Log.WARN, "CUENTAS_ERROR", error.toString());
            throw new  ServerException("Ocurrió un error inesperado en el servidor de autenticación");
        }
    }



    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response;
        try {
            response = chain.proceed(request);
        } catch (IOException ex){
            throw new NetworkException("Ocurrió un problema de red.", ex);
        }
        assertNotFound(request, response);
        assertAuthentication(request, response);
        assertServerError(request, response);
        return response;

    }

}
