package com.gnp.ajustadormovil.android.data.context;

import android.content.Context;

import com.gnp.ajustadormovil.android.data.context.impl.TokenDataJwtImpl;
import com.gnp.ajustadormovil.android.data.retrofit.RetrofitResources;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ContextDataFactory {

    private static AuthenticationData authenticationData;

    private static MetadataData metadataData;

    private static TokenData tokenData;

    public static MetadataData getMetadataData(){
        synchronized (MetadataData.class){
            if (metadataData == null){
                metadataData =  getRetrofitMetadata()
                        .create(MetadataData.class);
            }
            return metadataData;
        }
    }

    public static AuthenticationData getAuthenticationData(){
        synchronized (AuthenticationData.class){
            if (authenticationData == null){
                authenticationData =  getRetrofitCuentas()
                        .create(AuthenticationData.class);

            }
            return authenticationData;
        }
    }

    public static TokenData getTokenData(){
        synchronized (TokenData.class){
            if (tokenData == null){
                tokenData =  new TokenDataJwtImpl();
            }
            return tokenData;
        }
    }

    public static Context getCurrentContext(){
        return BaseActivity.getBaseActivity().getBaseContext();
    }

    private static Retrofit getRetrofitCuentas() {
        return new Retrofit.Builder()
                .client(RetrofitResources.getUnsafeOkHttpClient(
                        RetrofitResources.getLoggingInterceptor(),
                        new AuthenticationErrorInterceptor()
                )).baseUrl(Utils.BASE_URL_LOGIN)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static Retrofit getRetrofitMetadata() {
        return new Retrofit.Builder()
                .client(RetrofitResources.getUnsafeOkHttpClient(
                        RetrofitResources.getLoggingInterceptor(),
                        RetrofitResources.getErrorInterceptor()
                )).baseUrl(Utils.BASE_URL_METADATA)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


}
