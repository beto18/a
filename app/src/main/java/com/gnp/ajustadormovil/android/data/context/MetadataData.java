package com.gnp.ajustadormovil.android.data.context;

import com.gnp.ajustadormovil.android.models.VersionResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface MetadataData {

    @Headers("Content-Type: application/json")
    @GET("{id_app}/{id_os}")
    Single<VersionResponse> getLastVersion(@Path("id_app") Integer id_app,
                                           @Path("id_os") Integer id_os);

}
