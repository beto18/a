package com.gnp.ajustadormovil.android.data.context;

import com.gnp.ajustadormovil.android.models.context.Token;
import com.gnp.ajustadormovil.android.models.context.Usuario;

public interface TokenData {

    Token parse(String token);

    Usuario getUser(String token);

    boolean isExpired(String token);

}
