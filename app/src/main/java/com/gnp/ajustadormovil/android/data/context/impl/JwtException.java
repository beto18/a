package com.gnp.ajustadormovil.android.data.context.impl;

public class JwtException extends RuntimeException {

    public JwtException(String message){
        super(message);
    }

}
