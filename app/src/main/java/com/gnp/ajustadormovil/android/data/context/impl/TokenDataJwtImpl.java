package com.gnp.ajustadormovil.android.data.context.impl;

import android.util.Base64;

import com.gnp.ajustadormovil.android.data.context.TokenData;
import com.gnp.ajustadormovil.android.models.context.Token;
import com.gnp.ajustadormovil.android.models.context.Usuario;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class TokenDataJwtImpl implements TokenData {

    private final Gson gson = new Gson();

    private static final long EXPIRE_TIME = 60 * 1000;

    private static String getBody(String token){
        if(token == null || token.isEmpty()){
            throw new JwtException("Token inválido");
        }
        String [] parts = token.split("\\.");
        if(parts.length != 3){
            throw new JwtException("Token inválido");
        }
        return new String(Base64.decode(parts[1], Base64.DEFAULT));
    }

    @Override
    public Token parse(String token) {
        try {
            return gson.fromJson(getBody(token), Token.class);
        }catch (JsonSyntaxException e){
            throw new JwtException("Formato de token inválido");
        }
    }

    @Override
    public Usuario getUser(String token) {
        return parse(token).getUsuario();
    }

    @Override
    public boolean isExpired(String token) {
        return (parse(token).getExpiration() * 1000) < (System.currentTimeMillis() - EXPIRE_TIME);
    }
}
