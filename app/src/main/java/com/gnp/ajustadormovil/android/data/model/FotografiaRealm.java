package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;

public class FotografiaRealm  extends RealmObject{
    private int id;
    private String foto;

    public FotografiaRealm(int id, String foto) {
        this.id = id;
        this.foto = foto;
    }

    public FotografiaRealm() {
    }

    public int getId() {
        return id;
    }

    public String getFoto() {
        return foto;
    }
}
