package com.gnp.ajustadormovil.android.data.model;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;

public class ImageGeneralModel {
    private String id;
    private Fotografia foto;
    private String filePhat;
    private Boolean isNewFile;

    public ImageGeneralModel() {
    }

    public ImageGeneralModel(String id, Fotografia foto, String filePhat, Boolean isNewFile) {
        this.id = id;
        this.foto = foto;
        this.filePhat = filePhat;
        this.isNewFile = isNewFile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Fotografia getFoto() {
        return foto;
    }

    public void setFoto(Fotografia foto) {
        this.foto = foto;
    }

    public String getFilePhat() {
        return filePhat;
    }

    public void setFilePhat(String filePhat) {
        this.filePhat = filePhat;
    }

    public Boolean getNewFile() {
        return isNewFile;
    }

    public void setNewFile(Boolean newFile) {
        isNewFile = newFile;
    }
}
