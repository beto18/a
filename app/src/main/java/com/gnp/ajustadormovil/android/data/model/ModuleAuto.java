package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModuleAuto  extends RealmObject{

    @PrimaryKey
    private String id;
    private String modulo;
    private String path;
    private String idAjuste;
    private long idAuto;
    private String tipoModulo;


    public ModuleAuto(String id, String modulo, String tipoModulo, String path, long idAuto, String idAjuste) {
        this.id = id;
        this.idAuto = idAuto;
        this.modulo = modulo;
        this.tipoModulo = tipoModulo;
        this.path = path;
        this.idAjuste = idAjuste;

    }

    public ModuleAuto(){

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }

    public long getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(long idAuto) {
        this.idAuto = idAuto;
    }

    public String getTipoModulo() {
        return tipoModulo;
    }

    public void setTipoModulo(String tipoModulo) {
        this.tipoModulo = tipoModulo;
    }


}
