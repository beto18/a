package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModuleGeneralPath extends RealmObject{

    @PrimaryKey
    private String id;
    private String clave;
    private String path;
    private String idAjuste;

    ModuleGeneralPath(String id, String clave, String path, String idAjuste) {
    }

    public ModuleGeneralPath(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }
}
