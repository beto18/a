package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModuleNoAuto extends RealmObject{

    @PrimaryKey
    private String id;
    private String modulo;
    private String path;
    private String tipoTerceros;
    private String idAjuste;
    private String idNoAuto;

    public ModuleNoAuto(String id, String modulo,String tipoTerceros, String path, String idAjuste, String idNoAuto){

        this.id = id;
        this.modulo = modulo;
        this.tipoTerceros = tipoTerceros;
        this.path = path;
        this.idAjuste = idAjuste;
        this.idNoAuto = idNoAuto;

    }

    public ModuleNoAuto(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }

    public String getIdNoAuto() {
        return idNoAuto;
    }

    public void setIdNoAuto(String idNoAuto) {
        this.idNoAuto = idNoAuto;
    }

    public String getTipoTerceros() {
        return tipoTerceros;
    }

    public void setTipoTerceros(String tipoTerceros) {
        this.tipoTerceros = tipoTerceros;
    }
}
