package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModuleOcupante extends RealmObject{

    @PrimaryKey
    private String id;
    private String modulo;
    private String tipoModulo;
    private String idOcupante;
    private String path;
    private String idAjuste;

    public ModuleOcupante(String id, String modulo, String tipoModulo, String idOcupante, String path, String idAjuste) {

        this.id = id;
        this.modulo = modulo;
        this.tipoModulo = tipoModulo;
        this.idOcupante = idOcupante;
        this.path = path;
        this.idAjuste = idAjuste;


    }

    public ModuleOcupante(){

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getIdOcupante() {
        return idOcupante;
    }

    public void setIdOcupante(String idOcupante) {
        this.idOcupante = idOcupante;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }

    public String getTipoModulo() {
        return tipoModulo;
    }

    public void setTipoModulo(String tipoModulo) {
        this.tipoModulo = tipoModulo;
    }
}
