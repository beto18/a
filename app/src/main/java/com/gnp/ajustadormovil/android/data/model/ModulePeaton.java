package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModulePeaton extends RealmObject{

    @PrimaryKey
    private String id;
    private String modulo;
    private String path;
    private String idPeaton;
    private String idAjuste;

    public ModulePeaton(String id, String modulo, String path, String idPeaton, String idAjuste) {
        this.id = id;
        this.modulo = modulo;
        this.path = path;
        this.idPeaton = idPeaton;
        this.idAjuste = idAjuste;

    }

    public ModulePeaton(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIdPeaton() {
        return idPeaton;
    }

    public void setIdPeaton(String idPeaton) {
        this.idPeaton = idPeaton;
    }

    public String getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }
}
