package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModuloTerceros  extends RealmObject{

    @PrimaryKey
    private String id;
    private String modulo;
    private String Tipo;
    private String Path;
    private String idAuto;
    private String idNoAuto;
    private String idPeaton;
    private String idConductor;
    private String idVehiculo;
    private String idOcupante;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public String getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(String idAuto) {
        this.idAuto = idAuto;
    }

    public String getIdNoAuto() {
        return idNoAuto;
    }

    public void setIdNoAuto(String idNoAuto) {
        this.idNoAuto = idNoAuto;
    }

    public String getIdPeaton() {
        return idPeaton;
    }

    public void setIdPeaton(String idPeaton) {
        this.idPeaton = idPeaton;
    }

    public String getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(String idConductor) {
        this.idConductor = idConductor;
    }

    public String getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(String idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getIdOcupante() {
        return idOcupante;
    }

    public void setIdOcupante(String idOcupante) {
        this.idOcupante = idOcupante;
    }
}
