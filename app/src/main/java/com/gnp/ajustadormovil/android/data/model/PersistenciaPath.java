package com.gnp.ajustadormovil.android.data.model;


import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.realm.RealmManager;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;

import java.io.File;
import java.util.List;

import io.realm.RealmList;

import static com.gnp.ajustadormovil.android.utils.Constants.ARRIBO;
import static com.gnp.ajustadormovil.android.utils.Constants.ASEGURADOS;
import static com.gnp.ajustadormovil.android.utils.Constants.AUTOS;
import static com.gnp.ajustadormovil.android.utils.Constants.CONDUCTOR;
import static com.gnp.ajustadormovil.android.utils.Constants.DECLARACION;
import static com.gnp.ajustadormovil.android.utils.Constants.ID_PRESINIESTRO;
import static com.gnp.ajustadormovil.android.utils.Constants.ID_SINIESTRO;
import static com.gnp.ajustadormovil.android.utils.Constants.MEDIA_PATH_DIR;
import static com.gnp.ajustadormovil.android.utils.Constants.NO_AUTOS;
import static com.gnp.ajustadormovil.android.utils.Constants.OCUPANTE;
import static com.gnp.ajustadormovil.android.utils.Constants.PEATON;
import static com.gnp.ajustadormovil.android.utils.Constants.RECUPERACION;
import static com.gnp.ajustadormovil.android.utils.Constants.RESPONSABILIDAD;
import static com.gnp.ajustadormovil.android.utils.Constants.TERCEROS;
import static com.gnp.ajustadormovil.android.utils.Constants.VEHICULO;

public class PersistenciaPath {
    //Modulos Cards
    public static final int ZERO = 0;
    public static final int MODULO_ASEGURADOS = 1;
    public static final int MODULO_TERCEROS = 2;
    public static final int MODULO_DECLARACION = 3;
    public static final int MODULO_ARRIBO = 4;
    public static final int MODULO_RESPONSABILIDAD = 5;
    public static final int MODULO_RECUPERACION = 6;

    //tipo de modulo al que pertenecen
    public static final int TIPO_CONDUCTOR = 1;
    public static final int TIPO_VEHICULO = 2;
    public static final int TIPO_OCUPANTE = 3;
    public static final int TIPO_AUTO = 4;
    public static final int TIPO_NO_AUTO = 5;
    public static final int TIPO_PEATON = 6;
    public static String pathFolder;


    private static void savePathDriver(String id, String modulo, String path, String idAjuste) {
        ModuleDriver moduleDriver = new ModuleDriver();
        moduleDriver.setId(id);
        moduleDriver.setModulo(modulo);
        moduleDriver.setPath(path);
        moduleDriver.setIdAjuste(idAjuste);
        RealmManager.INSTANCE.saveOrUpdateObject(moduleDriver);

    }

    private static void savePathCar(String id, String modulo, String path, String idAjuste){
        ModuleCar moduleCar =  new ModuleCar();
        moduleCar.setId(id);
        moduleCar.setModulo(modulo);
        moduleCar.setPath(path);
        moduleCar.setIdAjuste(idAjuste);

        RealmManager.INSTANCE.saveOrUpdateObject(moduleCar);

    }

    private static void savePathOcupante(String id, String modulo, String path, String idOcupante, String idAjuste) {
        ModuleOcupante moduleOcupante = new ModuleOcupante();
        moduleOcupante.setId(id);
        moduleOcupante.setModulo(modulo);
        moduleOcupante.setPath(path);
        moduleOcupante.setIdOcupante(idOcupante);
        moduleOcupante.setIdAjuste(idAjuste);

        RealmManager.INSTANCE.saveOrUpdateObject(moduleOcupante);

    }

    private static void savePathPeaton(String id, String modulo, String path, String idPeaton, String idAjuste) {

        ModulePeaton modulePeaton = new ModulePeaton();
        modulePeaton.setId(id);
        modulePeaton.setModulo(modulo);
        modulePeaton.setPath(path);
        modulePeaton.setIdPeaton(idPeaton);
        modulePeaton.setIdAjuste(idAjuste);

        RealmManager.INSTANCE.saveOrUpdateObject(modulePeaton);

    }

    private static void savePathAuto(String id, String modulo, String tipoModulo, String path, String idAjuste, String idAuto) {
        ModuleAuto moduleAuto = new ModuleAuto();
        moduleAuto.setId(id);
        moduleAuto.setModulo(modulo);
        moduleAuto.setTipoModulo(tipoModulo);
        moduleAuto.setPath(path);
        moduleAuto.setIdAuto(Long.parseLong(idAuto));
        moduleAuto.setIdAjuste(idAjuste);

        RealmManager.INSTANCE.saveOrUpdateObject(moduleAuto);

    }


    private static void savePathNoAuto(String id, String modulo, String path, String idAjuste, String idNoAuto) {
        ModuleNoAuto moduleNoAuto = new ModuleNoAuto();
        moduleNoAuto.setId(id);
        moduleNoAuto.setModulo(modulo);
        moduleNoAuto.setPath(path);
        moduleNoAuto.setIdNoAuto(idNoAuto);
        moduleNoAuto.setIdAjuste(idAjuste);

        RealmManager.INSTANCE.saveOrUpdateObject(moduleNoAuto);

    }

    public static String getImagePathDriver(ModuleDriver moduleDriver) {

        moduleDriver = RealmManager.INSTANCE.getRealmInstance().where(ModuleDriver.class)
                .equalTo("id", moduleDriver.getId())
                .equalTo("modulo", moduleDriver.getModulo())
                .equalTo("idAjuste", moduleDriver.getIdAjuste())
                .findFirst();


        return moduleDriver != null ? moduleDriver.getPath() : "";

    }


    public static String getImagePathCar(ModuleCar moduleCar) {

        moduleCar = RealmManager.INSTANCE.getRealmInstance().where(ModuleCar.class)
                .equalTo("id", moduleCar.getId())
                .equalTo("modulo", moduleCar.getModulo())
                .equalTo("idAjuste", moduleCar.getIdAjuste())
                .findFirst();


        return moduleCar != null ? moduleCar.getPath() : "";

    }

    public static String getImagePathOcupante(ModuleOcupante moduleOcupante) {

        moduleOcupante = RealmManager.INSTANCE.getRealmInstance().where(ModuleOcupante.class)
                .equalTo("id", moduleOcupante.getId())
                .equalTo("modulo", moduleOcupante.getModulo())
                .equalTo("idOcupante", moduleOcupante.getIdOcupante())
                .equalTo("idAjuste", moduleOcupante.getIdAjuste())
                .findFirst();


        return moduleOcupante != null ? moduleOcupante.getPath() : "";

    }

    public static String getImagePathAuto(ModuleAuto moduleAuto) {

        moduleAuto = RealmManager.INSTANCE.getRealmInstance().where(ModuleAuto.class)
                .equalTo("id", moduleAuto.getId())
                .equalTo("modulo", moduleAuto.getModulo())
                .equalTo("tipoModulo", moduleAuto.getTipoModulo())
                .equalTo("idAjuste", moduleAuto.getIdAjuste())
                .equalTo("idAuto", moduleAuto.getIdAuto())
                .findFirst();


        return moduleAuto != null ? moduleAuto.getPath() : "";

    }

    public static String getImagePathNoAuto(ModuleNoAuto moduleNoAuto) {

        moduleNoAuto = RealmManager.INSTANCE.getRealmInstance().where(ModuleNoAuto.class)
                .equalTo("id", moduleNoAuto.getId())
                .equalTo("modulo", moduleNoAuto.getModulo())
                .equalTo("idAjuste", moduleNoAuto.getIdAjuste())
                .equalTo("idNoAuto", moduleNoAuto.getIdNoAuto())
                .findFirst();


        return moduleNoAuto != null ? moduleNoAuto.getPath() : "";

    }

    public static String getImagePathPeaton(ModulePeaton modulePeaton) {

        modulePeaton = RealmManager.INSTANCE.getRealmInstance().where(ModulePeaton.class)
                .equalTo("id", modulePeaton.getId())
                .equalTo("modulo", modulePeaton.getModulo())
                .equalTo("idPeaton", modulePeaton.getIdPeaton())
                .equalTo("idAjuste", modulePeaton.getIdAjuste())
                .findFirst();

        return modulePeaton != null ? modulePeaton.getPath() : "";

    }


    private static String getTipoModulo(int tipo) {
        String stringTipo = null;

        switch (tipo) {
            case TIPO_CONDUCTOR:
                stringTipo = CONDUCTOR;
                break;
            case TIPO_VEHICULO:
                stringTipo = VEHICULO;
                break;
            case TIPO_OCUPANTE:
                stringTipo = OCUPANTE;
                break;
            case TIPO_AUTO:
                stringTipo = AUTOS;
                break;
            case TIPO_NO_AUTO:
                stringTipo = NO_AUTOS;
                break;
            case TIPO_PEATON:
                stringTipo = PEATON;
                break;
        }

        return stringTipo;
    }

    /**
     * devuelve todas las Imagenes de la galeria.
     *
     * @param activity parametro Activity al que pertenece
     * @param path     parametro String de la ubicacion del archivo
     * @return ArrayList with images Path
     */
    public static List<ImageGeneralModel> getAllImages(Activity activity, String path) {
        //Remove older images to avoid copying same image twice
        List<ImageGeneralModel> allImages = new RealmList<>();
        allImages.clear();
        Uri uri;
        Cursor cursor;
        int column_index_data;
        if(path != null){
            if (!path.equals("")) {
                String absolutePathOfImage;
                //El content:// URI de estilo para el volumen de almacenamiento externo "principal".
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                //Ruta al archivo en el disco.                         El nombre para mostrar del archivo
                String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME};//El nombre para mostrar del archivo
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);

                if (cursor != null) {
                    column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

                    while (cursor.moveToNext()) {
                        absolutePathOfImage = cursor.getString(column_index_data);
                        if (absolutePathOfImage.contains(path)) {
                            if (!absolutePathOfImage.contains("_1")) {
                                allImages.add(new ImageGeneralModel("", null, absolutePathOfImage, true));
                            }
                        }
                    }
                    cursor.close();
                }

                //El content:// URI de estilo para el almacenamiento interno.
                uri = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);

                if (cursor != null) {
                    column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

                    while (cursor.moveToNext()) {
                        absolutePathOfImage = cursor.getString(column_index_data);
                        if (absolutePathOfImage.contains(path)) {
                            if (!absolutePathOfImage.contains("_1")) {
                                allImages.add(new ImageGeneralModel("", null, absolutePathOfImage, true));
                            }
                        }
                    }
                    cursor.close();
                }
            }
        }

        return allImages;

    }

    private static String createDirectoryAsegurado(int tipoAsegurados, String path, String idOcupante, String idAjuste) {

        path = path + File.separator + ASEGURADOS + File.separator;

        switch (tipoAsegurados) {
            case TIPO_CONDUCTOR:
                path = path + CONDUCTOR;
                savePathDriver(idAjuste + MODULO_ASEGURADOS + tipoAsegurados, ASEGURADOS, path, idAjuste );
                break;
            case TIPO_VEHICULO:
                path = path + VEHICULO;
                savePathCar(idAjuste + MODULO_ASEGURADOS + tipoAsegurados, ASEGURADOS, path, idAjuste);
                break;
            case TIPO_OCUPANTE:
                path = path + OCUPANTE + File.separator + idOcupante;
                savePathOcupante(idOcupante, ASEGURADOS, path,idOcupante, idAjuste);
                break;

        }

        return path;
    }


    private  static String createDirectoryTerceros(int tipoTerceros,int tipoAsegurados, String path, String idAuto, String idOcupante, String idAjuste){
        path = path + File.separator + TERCEROS + File.separator;
        String id;

        switch (tipoTerceros){
            case TIPO_AUTO:
                //Creación de crapeta Terceros /pictures/idAjuste/Terceros/Auto/idAuto/
                if(getTipoModulo(tipoAsegurados).equals(OCUPANTE)) {
                    id = idOcupante;
                    path = path + AUTOS + File.separator + idAuto + File.separator + getTipoModulo(tipoAsegurados) + File.separator + idOcupante;
                }else{
                    id = idAuto + MODULO_TERCEROS + tipoAsegurados;
                    path = path + AUTOS + File.separator + idAuto + File.separator + getTipoModulo(tipoAsegurados);
                }savePathAuto(id, TERCEROS, getTipoModulo(tipoAsegurados), path, idAjuste, idAuto);
                break;
            case TIPO_NO_AUTO:
                //Creación de crapeta Terceros /pictures/idAjuste/Terceros/NoAuto/idNoAuto/
                path =  path + NO_AUTOS + File.separator + idAuto;
                savePathNoAuto(idAuto, TERCEROS, path, idAjuste,idAuto );
                break;
            case TIPO_PEATON:
                //Creación de crapeta Terceros /pictures/idAjuste/Terceros/peaton/idPeaton/
                path = path + PEATON + File.separator + idAuto;
                savePathPeaton(idAuto,TERCEROS, path,idAuto, idAjuste );
                break;
        }
        return path;
    }

    public static String createDirectory(int modulo, int tipoAsegurados, int tipoTerceros, String idAuto, String idOcupante) {

        String idSinistro =  AppPreferences.getString(ID_SINIESTRO);
        String idPresiniestro = AppPreferences.getString(ID_PRESINIESTRO);
        String idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);

        String path = MEDIA_PATH_DIR + ID_SINIESTRO +"_"+ idSinistro.replaceFirst("^0+(?!$)", "");

        switch(modulo){

            case MODULO_ASEGURADOS:
                path = createDirectoryAsegurado(tipoAsegurados,path,idOcupante, idAjuste);
                break;
            case MODULO_TERCEROS:
                path = createDirectoryTerceros(tipoTerceros,tipoAsegurados,path,idAuto,idOcupante,idAjuste);
                break;
            case MODULO_DECLARACION:
                path = createDirectoryDeclaracion(path);
                break;
            case MODULO_ARRIBO:
                path = createDirectoryArribo(path);
                break;
            case MODULO_RESPONSABILIDAD:
                path = createDirectoryResponsabilidad(path);
                break;
            case MODULO_RECUPERACION:
                path = createDirectoryRecuperacion(path);
                break;

        }
        pathFolder = path;
        return path + File.separator + System.currentTimeMillis() + ".jpeg";
    }

    private static String createDirectoryResponsabilidad(String path) {
        return path + File.separator + RESPONSABILIDAD;
    }

    private static String createDirectoryRecuperacion(String path) {
        return path + File.separator + RECUPERACION;
    }

    private static String createDirectoryDeclaracion(String path){
        return path + File.separator + DECLARACION;
    }

    private static String createDirectoryArribo(String path){
        return path + File.separator + ARRIBO;
    }


    /**
     * Método que muestra las imágenes que se encuentran en la carpeta para subir al servicio
     * @param activity parametro de la Activity que pertenece
     * @param filesPhotos parametro del ArrayList<ImageGeneralModel> para visualizar el tamaño
     */

    public static void showMessageSizeImages(Activity activity, List<ImageGeneralModel> filesPhotos){
        if(filesPhotos.size() == 1){
            ToastUtils.toastShort(activity, activity.getString(R.string.there)
                    + " " + filesPhotos.size() + " " + activity.getString(R.string.message_one_image_size_path));
        }else if(!filesPhotos.isEmpty()){
            ToastUtils.toastShort(activity,activity.getString(R.string.there)
                    + " " + filesPhotos.size() + " " + activity.getString(R.string.message_more_images_size_path));
        }

    }





}
