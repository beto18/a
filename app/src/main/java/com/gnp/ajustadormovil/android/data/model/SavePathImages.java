package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SavePathImages  extends RealmObject{

    @PrimaryKey
    private String id;
    private int modulo;
    private String path;
    private int tipoAsegurados;
    private String idOcupante;
    private int tipoTerceros;
    private String id_auto;

    public SavePathImages(){

    }
     public SavePathImages(String idAjuste, int modulo, int tipoAsegurados, String idOcupante, int tipoTerceros, String idAuto) {
        this.id = idAjuste;
        this.modulo = modulo;
        this.tipoAsegurados = tipoAsegurados;
        this.idOcupante = idOcupante;
        this.tipoTerceros = tipoTerceros;
        this.id_auto = idAuto;

    }

    public String getId() {
        return id;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getTipoAsegurados() {
        return tipoAsegurados;
    }

    public void setTipoAsegurados(int tipoAsegurados) {
        this.tipoAsegurados = tipoAsegurados;
    }

    public String getIdOcupante() {
        return idOcupante;
    }

    public void setIdOcupante(String idOcupante) {
        this.idOcupante = idOcupante;
    }

    public int getTipoTerceros() {
        return tipoTerceros;
    }

    public void setTipoTerceros(int tipoTerceros) {
        this.tipoTerceros = tipoTerceros;
    }

    public String getId_auto() {
        return id_auto;
    }

    public void setId_auto(String id_auto) {
        this.id_auto = id_auto;
    }
}
