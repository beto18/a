package com.gnp.ajustadormovil.android.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by tixmedia on 4/12/18.
 */

public class StatusChanged extends RealmObject{



    @PrimaryKey
    private int id;

    private String status;
    private String idAjuste;
    private String tipoServicio;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }
}
