package com.gnp.ajustadormovil.android.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class TercerVehiculo {

    @SerializedName("vin")
    @Expose
    private String vin;
    @SerializedName("tipoVehiculo")
    @Expose
    private String tipoVehiculo;
    @SerializedName("armadora")
    @Expose
    private String armadora;
    @SerializedName("carroceria")
    @Expose
    private String carroceria;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("modelo")
    @Expose
    private Integer modelo;
    @SerializedName("placas")
    @Expose
    private String placas;
    @SerializedName("numeroMotor")
    @Expose
    private String numeroMotor;
    @SerializedName("danoConsecuencia")
    @Expose
    private Boolean danoConsecuencia;
    @SerializedName("danoEstimado")
    @Expose
    private Double danoEstimado;
    @SerializedName("perdidaTotalEvidente")
    @Expose
    private Boolean perdidaTotalEvidente;
    @SerializedName("observacion")
    @Expose
    private String observacion;
    @SerializedName("danos")
    @Expose
    private List<Object> danos = null;
    @SerializedName("danosPreexistentes")
    @Expose
    private List<Object> danosPreexistentes = null;
    @SerializedName("fotografias")
    @Expose
    private List<Object> fotografias = null;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getArmadora() {
        return armadora;
    }

    public void setArmadora(String armadora) {
        this.armadora = armadora;
    }

    public String getCarroceria() {
        return carroceria;
    }

    public void setCarroceria(String carroceria) {
        this.carroceria = carroceria;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getModelo() {
        return modelo;
    }

    public void setModelo(Integer modelo) {
        this.modelo = modelo;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getNumeroMotor() {
        return numeroMotor;
    }

    public void setNumeroMotor(String numeroMotor) {
        this.numeroMotor = numeroMotor;
    }

    public Boolean getDanoConsecuencia() {
        return danoConsecuencia;
    }

    public void setDanoConsecuencia(Boolean danoConsecuencia) {
        this.danoConsecuencia = danoConsecuencia;
    }

    public Double getDanoEstimado() {
        return danoEstimado;
    }

    public void setDanoEstimado(Double danoEstimado) {
        this.danoEstimado = danoEstimado;
    }

    public Boolean getPerdidaTotalEvidente() {
        return perdidaTotalEvidente;
    }

    public void setPerdidaTotalEvidente(Boolean perdidaTotalEvidente) {
        this.perdidaTotalEvidente = perdidaTotalEvidente;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<Object> getDanos() {

        return danos;
    }

    public void setDanos(List<Object> danos) {
        this.danos = danos;
    }

    public List<Object> getDanosPreexistentes() {
        return danosPreexistentes;
    }

    public void setDanosPreexistentes(List<Object> danosPreexistentes) {
        this.danosPreexistentes = danosPreexistentes;
    }

    public List<Object> getFotografias() {
        return fotografias;
    }

    public void setFotografias(List<Object> fotografias) {
        this.fotografias = fotografias;
    }

}

