package com.gnp.ajustadormovil.android.data.model.bluetoothdevice;

import java.util.Objects;

public class DeviceItem {

    private String deviceName;
    private String address;

    public DeviceItem(String deviceName, String address) {
        this.deviceName = deviceName;
        this.address = address;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeviceItem)) return false;
        DeviceItem that = (DeviceItem) o;
        return Objects.equals(deviceName, that.deviceName) &&
                Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceName, address);
    }
}
