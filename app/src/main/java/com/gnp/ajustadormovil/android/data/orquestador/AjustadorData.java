package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.login.ChangePassword;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Dispositivo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.EstatusCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Horario;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.PosicionCommand;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface AjustadorData {

    @Headers("Content-Type: application/json")
    @GET("-/horario")
    Single<List<Horario>> getHorario();

    @Headers("Content-Type: application/json")
    @GET("-")
    Single<Ajustador> getAjustador();

    @Headers("Content-Type: application/json")
    @POST("-/dispositivo")
    Single<Dispositivo> addDispositivo(@Body Dispositivo dispositivo);

    @Headers("Content-Type: application/json")
    @PUT("-/posicion")
    Completable modifyPosicion(@Body PosicionCommand posicion);

    @Headers("Content-Type: application/json")
    @PUT("-/estatus")
    Completable modifyEstatus(@Body EstatusCommand estatus);

    @Headers("Content-Type: application/json")
    @POST("ajustadores/password/reset")
    Completable resetPassword(@Query("correo") String email);

    @Headers("Content-Type: application/json")
    @PUT("-/password")
    Completable changePassword(@Body ChangePassword password);

}
