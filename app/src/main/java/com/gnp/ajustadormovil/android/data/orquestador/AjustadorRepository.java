package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;

public interface AjustadorRepository {

    Ajustador findOne();

    void save(Ajustador ajustador);

    void delete();

}
