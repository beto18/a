package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.CierreCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.RechazoCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.TipoUbicacionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface AjusteData {

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("-/historial")
    Single<List<Ajuste>> getHistorial(@Query("fecha") String date);

    @Headers("Content-Type: application/json")
    @GET("-/ajustes")
    Single<List<Ajuste>> getAjustes();

    @Headers("Content-Type: application/json")
    @GET("ajustes/{id}")
    Single<Ajuste> getAjuste(@Path("id") String id);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/rechazo")
    Completable rechazar(@Path("id") String id,
                         @Body RechazoCommand data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/confirmacion")
    Completable confirmar(@Path("id") String id);

    @Multipart
    @POST("ajustes/{id}/arribo")
    Single<Archivo> arribar(@Path("id") String id,
                                @Part("archivo\"; filename=\"image.jpg\" ") RequestBody image);

    @Multipart
    @POST("ajustes/{id}/arribo/fotografia")
    Single<Archivo> addFotografiaArribo(@Path("id") String id,
                                @Part("archivo\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/termino")
    Completable terminando(@Path("id") String id);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/cierre")
    Completable cerrar(@Path("id") String id,
                       @Body CierreCommand data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/offline/cierre")
    Completable cerrarOffline(@Path("id") String id,
                       @Body CierreCommand data);

    @Streaming
    @GET("ajustes/{id}/declaracion")
    Single<Response<ResponseBody>> getDeclaracion(@Path("id") String id);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/declaracion")
    Single<Archivo> addDeclaracion(@Path("id") String id,
                                        @Body DeclaracionCommand folio);

    @Multipart
    @PUT("ajustes/{id}/declaracion/evidencia")
    Single<List<Fotografia>> addEvidenciasDeclaracion(@Path("id") String id,
                                                          @Part("archivo\"; filename=\"image.jpg\" ")RequestBody image);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{id}/ubicacion")
    Completable modifyTipoUbicacionAjustador(@Path("id") String id,
                                             @Body TipoUbicacionCommand data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id}/deducibleAdminsitrativo")
    Completable addDeducibleAdministrativo(@Path("id") String id);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{id}/deducibleAdminsitrativo")
    Completable deleteDeducibleAdministrativo(@Path("id") String id);

    @GET
    Completable getCroquis(@Url String url, Callback<Response> callback);

}
