package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;

import java.util.List;

public interface AjusteRepository {

    Ajuste findById(String id);

    void deleteById(String id);

    void save(Ajuste ajuste);

    void save(List<Ajuste> ajustes);

    List<Ajuste> findAll();

}
