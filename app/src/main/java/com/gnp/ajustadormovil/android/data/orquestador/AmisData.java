package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.amis.AjusteAmis;
import com.gnp.ajustadormovil.android.models.amis.Certificado;
import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;
import com.gnp.ajustadormovil.android.models.amis.EmisionValeDigital;
import com.gnp.ajustadormovil.android.models.amis.Folio;
import com.gnp.ajustadormovil.android.models.amis.Refoleo;
import com.gnp.ajustadormovil.android.models.amis.ValeDigital;

import java.util.Collection;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AmisData {

    /**
     * Genera un folio de AMIS.
     * @param ajuste Id de Ajuste
     * @return {@link Single} Folio de AMIS.
     */
    @GET("ajustes/{ajuste}/amis/folio")
    Single<Folio> generarFolio(@Path("ajuste") String ajuste);

    /**
     * Guarda un folio de AMIS.
     * @param ajuste Id de Ajuste.
     * @param folio Folio que se actualiza.
     * @return {@link Completable}
     */
    @POST("ajustes/{ajuste}/amis/folio")
    Completable guardarFolio(@Path("ajuste") String ajuste, @Body Folio folio);

    /**
     * Vincula las contrapartes a un folio propio.
     * Si alguna contraparte ya se vinculo, no lo vuelve a hacer.
     * @param ajuste Id de Ajuste.
     * @param folios Folios de las contrapartes.
     * @return {@link Completable}
     */
    @POST("ajustes/{ajuste}/amis/contrapartes")
    Completable vincularFolios(@Path("ajuste") String ajuste, @Body Collection<String> folios);

    /**
     * Refolea el folio actual, si lo requiere.
     * Toma en cuenta los folios vinculados para realizar el refoleo.
     * @param ajuste Id de Ajuste.
     * @return {@link Single} Folio nuevo de AMIS (refoleado).
     */
    @PUT("ajustes/{ajuste}/amis/folios")
    Single<List<Refoleo>> refolear(@Path("ajuste") String ajuste);

    /**
     * Envía la información actual del ajuste a AMIS.
     * @param ajuste Id de Ajuste.
     * @return {@link Completable}
     */
    @POST("ajustes/{ajuste}/amis/sincronizacion")
    Completable sincronizar(@Path("ajuste") String ajuste);

    /**
     * Obtiene la información de las contrapartes que se vincularon.
     * @param ajuste Id de Ajuste.
     * @return {@link Single} Información de las contrapartes.
     */
    @GET("ajustes/{ajuste}/amis/contrapartes")
    Single<List<AjusteAmis>> getContrapartes(@Path("ajuste") String ajuste);

    /**
     * Recibe una orden de vale digital.
     * @param ajuste Identificador del ajuste.
     * @param folioOrden Folio de la orden.
     * @return Vale Digital
     */
    @GET("ajustes/{ajuste}/amis/ordenes/{folioOrden}")
    Single<ValeDigital> recibirOrden(@Path("ajuste") String ajuste,
                                     @Path("folioOrden") String folioOrden);

    /**
     * Recibe una orden de vale digital.
     * @param ajuste Identificador del ajuste.
     * @param emision Emisión del Vale Digital.
     * @return {@link Completable}
     */
    @POST("ajustes/{ajuste}/amis/ordenes")
    Completable emitirOrden(@Path("ajuste") String ajuste, @Body EmisionValeDigital emision);

    @GET("amis/certificados")
    Single<List<CertificadoCompania>> getCertificados();

    @GET("amis/certificados/{compania}")
    Single<CertificadoCompania> getCertificado(@Path("compania") Long compania);

    @GET("amis/certificados/gnp/{serial}")
    Single<Certificado> getCertificadoPropio(@Path("serial") String serial);




}
