package com.gnp.ajustadormovil.android.data.orquestador;


import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VolanteVehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface AseguradoData {

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado")
    Single<Asegurado> get(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajuste}/asegurado/conductor")
    Completable modifyConductor(@Path("ajuste") String ajuste,
                                @Body Persona conductor);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/conductor")
    Single<Persona> getConductor(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/conductor/fotografias")
    Single<List<Fotografia>> getFotografiasConductor(@Path("ajuste") String ajuste);

    @Multipart
    @POST("ajustes/{ajuste}/asegurado/conductor/fotografias")
    Single<List<Fotografia>> addFotografiaConductor(@Path("ajuste") String ajuste,
                                                        @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/asegurado/conductor/fotografias/{idFotografia}")
    Completable deleteFotografiaConductor(@Path("ajuste") String ajuste,
                                          @Path("idFotografia") Long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/asegurado/conductor/volante")
    Single<Archivo> addVolanteConductor(@Path("ajuste") String ajuste,
                                            @Body VolanteMedico volante);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/vehiculo")
    Single<VehiculoAsegurado> getVehiculo(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajuste}/asegurado/vehiculo")
    Completable modifyVehiculo(@Path("ajuste") String ajuste,
                               @Body VehiculoAsegurado vehiculo);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/vehiculo/fotografias")
    Single<List<Fotografia>> getFotografiasVehiculo(@Path("ajuste") String ajuste);

    @Multipart
    @POST("ajustes/{ajuste}/asegurado/vehiculo/fotografias")
    Single<List<Fotografia>> addFotografiaVehiculo(@Path("ajuste") String ajuste,
                                                       @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/asegurado/vehiculo/fotografias/{idFotografia}")
    Completable deleteFotografiaVehiculo(@Path("ajuste") String ajuste,
                                         @Path("idFotografia") Long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/asegurado/vehiculo/volante")
    Single<Archivo> addVolanteVehiculo(@Path("ajuste") String ajuste,
                                           @Body VolanteVehiculoAsegurado volante);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/ocupantes")
    Single<List<Ocupante>> getOcupantes(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}")
    Single<Ocupante> getOcupante(@Path("ajuste") String ajuste,
                                     @Path("idOcupante") Long idOcupante);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/asegurado/ocupantes")
    Single<Ocupante> addOcupante(@Path("ajuste") String ajuste,
                                     @Body Ocupante ocupante);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}")
    Completable modifyOcupante(@Path("ajuste") String ajuste,
                               @Path("idOcupante") Long idOcupante,
                               @Body Ocupante ocupante);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}")
    Completable deleteOcupante(@Path("ajuste") String ajuste,
                               @Path("idOcupante") Long idOcupante);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}/fotografias")
    Single<List<Fotografia>> getFotografiasOcupante(@Path("ajuste") String ajuste,
                                                        @Path("idOcupante") Long idOcupante);

    @Multipart
    @POST("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}/fotografias")
    Single<List<Fotografia>> addFotografiaOcupante(@Path("ajuste") String ajuste,
                                                       @Path("idOcupante") Long idOcupante,
                                                       @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}/fotografias/{idFotografia}")
    Completable deleteFotografiaOcupante(@Path("ajuste") String ajuste,
                                         @Path("idOcupante") Long idOcupante,
                                         @Path("idFotografia") Long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/asegurado/ocupantes/{idOcupante}/volante")
    Single<Archivo> addVolanteOcupante(@Path("ajuste") String ajuste,
                                           @Path("idOcupante") Long idOcupante,
                                           @Body VolanteMedico conductor);


}
