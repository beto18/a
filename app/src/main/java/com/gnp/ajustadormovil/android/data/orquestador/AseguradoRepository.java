package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;

public interface AseguradoRepository {

    Asegurado findByAjuste(String ajuste);

    void save(Asegurado asegurado);

    void deleteByAjuste(String ajuste);

}
