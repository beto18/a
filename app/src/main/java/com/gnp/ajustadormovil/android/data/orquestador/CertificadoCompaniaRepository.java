package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;

public interface CertificadoCompaniaRepository {

    CertificadoCompania findByCompania(Long compania);

    void save(CertificadoCompania certificado);

    long count();

}
