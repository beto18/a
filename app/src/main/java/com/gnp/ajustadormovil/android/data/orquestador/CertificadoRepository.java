package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.amis.Certificado;

public interface CertificadoRepository {

    Certificado findByNumeroSerial(String numeroSerial);

    void save(Certificado certificado);

}
