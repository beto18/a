package com.gnp.ajustadormovil.android.data.orquestador;


import com.gnp.ajustadormovil.android.models.ajuste.Croquis;

public interface CroquisRepository {

    void save(Croquis croquis);

    Croquis findByAjuste(String ajuste);

    void delete(String ajuste);
}
