package com.gnp.ajustadormovil.android.data.orquestador;

public interface ErrorRepository {

    void saveByAjuste(String ajuste, Throwable throwable);

    Throwable findByAjuste(String ajuste);

    void deleteByAjuste(String ajuste);

}
