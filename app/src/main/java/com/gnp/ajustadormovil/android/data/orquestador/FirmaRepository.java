package com.gnp.ajustadormovil.android.data.orquestador;

public interface FirmaRepository {
    void saveAseguradoConductor(String ajuste, String firma);

    String getAseguradoConductor(String ajuste);

    void saveAseguradoOcupante(String ajuste, Long idOcupante, String firma);

    String getAseguradoOcupante(String ajuste, Long idOcupante);

    void saveTerceroAutoConductor(String idAjuse, Long idTercero, String firma);

    String getTerceroConductorAuto(String ajuste, Long idTercero);

    void saveTerceroAutoOcupante(String ajuste, Long idTercero, Long idOcupante, String firma);

    String getTerceroAutoOcupante(String ajuste, Long idTercero, Long idOcupante);

    void saveTerceroPersona(String ajuste, Long idTercero, String firma);

    String getTerceroPersona(String ajuste, Long idTercero);

    void saveTerceroNoAuto(String ajuste, Long idTercero, String firma);

    String getTerceroNoAuto(String ajuste, Long idTercero);

    void deleteByAjuste(String ajuste);
}
