package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;

public interface FotografiaRepository {

    void delete(Fotografia fotografia);

}
