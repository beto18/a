package com.gnp.ajustadormovil.android.data.orquestador;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.gnp.ajustadormovil.android.error.ActionError;
import com.gnp.ajustadormovil.android.error.AuthenticationException;
import com.gnp.ajustadormovil.android.error.DataError;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.ForbiddenException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceError;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.ServerDataException;
import com.gnp.ajustadormovil.android.error.ServerException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OrquestadorErrorInterceptor implements Interceptor {

    private static final String TAG = OrquestadorErrorInterceptor.class.getCanonicalName();

    private final Gson gson = new GsonBuilder().create();

    /**
     * Valida si existe algún error en los datos enviados al servicio.
     * Esto ocurriría si el código de error es <code>406</code>
     * @param response La respuesta del llamado al servicio.
     * @throws DataException Si los datos no son correctos.
     */
    private void assertData(Request request, Response response) {
        if(response.code() == 406){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Error en datos enviados: " + error);
            throw new ServerDataException(gson.fromJson(error.getResponse(), DataError.class));
        }
    }

    /**
     * Valida si existe algún error en la autenticación del servicio.
     * Esto ocurriría si el código de error es <code>401</code>
     * @param response La respuesta del llamado al servicio.
     * @throws AuthenticationException Si no se puede acceder al servicio por problemas de autenticación.
     */
    private void assertAuthentication(Request request, Response response) {
        if(response.code() == 401){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Error en autenticación: " + error);
            throw new AuthenticationException("Error de autenticación al servidor");
        }
    }

    /**
     * Valida si existe algún error en la acción que se intenta realizar.
     * Esto ocurriría si el código de error es <code>403</code>
     * @param response La respuesta del llamado al servicio.
     * @throws ForbiddenException Si los datos no son correctos.
     */
    private void assertForbidden(Request request, Response response) {
        if(response.code() == 403){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Acción prohíbida: " + error);
            throw new ForbiddenException("La operación a realizar esta prohíbida");
        }
    }

    /**
     * Valida si no existe el recurso solicitado.
     * Esto ocurriría si el código de error es <code>404</code>
     * @param response La respuesta del llamado al servicio.
     * @throws ResourceNotFoundException Si los datos no son correctos.
     */
    private void assertNotFound(Request request, Response response) throws IOException{
        if(response.code() == 404){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Recurso no encontrado: " + error);
            throw new ResourceNotFoundException();
        }
    }

    /**
     * Valida si existe algún error al consumir los servicios.
     * Esto ocurriría si el código de error es <code>4xx</code>
     * @param response La respuesta del llamado al servicio.
     * @throws LogicException Si existe un problema al consumir el servicio.
     */
    private void assertClientError(Request request, Response response) {
        if(response.code() / 100 == 4){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Error en consumir servicio: " + error);
            throw new LogicException(gson.fromJson(error.getResponse(), ActionError.class));
        }
    }

    /**
     * Valida si existe algún error en el servidor.
     * Esto ocurriría si el código de error es <code>5xx</code>
     * @param response La respuesta del llamado al servicio.
     * @throws ServerException Si existe un problema en el servidor.
     */
    private void assertServerError(Request request, Response response) {
        if(response.code() / 100 == 5){
            ResourceError error = ErrorUtils.getResourceError(request, response);
            Log.w(TAG, "Error en el servidor: " + error);
            Crashlytics.log(Log.WARN, "ORQUESTADOR_ERROR", error.toString());
            RuntimeException exception;
            try {
                exception = new UnknownException(gson.fromJson(error.getResponse(), ActionError.class));
            } catch (Exception e){
                exception = new ServerException(error.getResponse());
            }
            throw exception;
        }
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response;
        try {
            response = chain.proceed(request);
        } catch (IOException ex){
            throw new NetworkException("Ocurrió un problema de red", ex);
        }
        assertNotFound(request, response);
        assertAuthentication(request, response);
        assertForbidden(request, response);
        assertData(request, response);
        assertClientError(request, response);
        assertServerError(request, response);
        return response;
    }

}
