package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.aspects.Aspects;
import com.gnp.ajustadormovil.android.data.retrofit.RetrofitResources;
import com.gnp.ajustadormovil.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrquestadorFactory {

    private static Retrofit retrofit;

    private static List<String> noSecurityPaths;

    static {
        noSecurityPaths = new ArrayList<>();
        noSecurityPaths.add("ajustadores/password/reset");
    }

    private static AseguradoData aseguradoData;
    private static TerceroData terceroData;
    private static TerceroAutoData terceroAutoData;
    private static TerceroNoAutoData terceroNoAutoData;
    private static TercerosPersonaData tercerosPersonaData;
    private static AjusteData ajusteData;
    private static ResponsabilidadData responsabilidadesData;
    private static AjustadorData ajustadorData;
    private static ServicioCoberturaData servicioCoberturaData;
    private static RecuperacionData recuperacionData;
    private static PolizaData polizaData;
    private static AmisData amisData;

    public static AseguradoData getAseguradoData(){
        synchronized (AseguradoData.class){
            if (aseguradoData == null){
                aseguradoData = Aspects.aspect(AseguradoData.class, buildRetrofit(AseguradoData.class));
            }
            return aseguradoData;
        }
    }

    public static TerceroData getTerceroData(){
        synchronized (TerceroData.class){
            if (terceroData == null){
                terceroData = buildRetrofit(TerceroData.class);
            }
            return terceroData;
        }
    }

    public static TerceroAutoData getTerceroAutoData(){
        synchronized (TerceroAutoData.class){
            if (terceroAutoData == null){
                terceroAutoData = Aspects.aspect(TerceroAutoData.class, buildRetrofit(TerceroAutoData.class));
            }
            return terceroAutoData;
        }
    }

    public static TerceroNoAutoData getTerceroNoAutoData(){
        synchronized (TerceroNoAutoData.class){
            if (terceroNoAutoData == null){
                terceroNoAutoData = Aspects.aspect(TerceroNoAutoData.class, buildRetrofit(TerceroNoAutoData.class));
            }
            return terceroNoAutoData;
        }
    }

    public static TercerosPersonaData getTerceroPersonaData(){
        synchronized (TercerosPersonaData.class){
            if (tercerosPersonaData == null){
                tercerosPersonaData = Aspects.aspect(TercerosPersonaData.class, buildRetrofit(TercerosPersonaData.class));
            }
            return tercerosPersonaData;
        }
    }

    public static AjusteData getAjusteData(){
        synchronized (AjusteData.class){
            if (ajusteData == null){
                ajusteData = Aspects.aspect(AjusteData.class, buildRetrofit(AjusteData.class));
            }
            return ajusteData;
        }
    }

    public static ResponsabilidadData getResponsabilidadData(){
        synchronized (ResponsabilidadData.class){
            if (responsabilidadesData == null){
                responsabilidadesData = Aspects.aspect(ResponsabilidadData.class, buildRetrofit(ResponsabilidadData.class));
            }
            return responsabilidadesData;
        }
    }

    public static AjustadorData getAjustadorData(){
        synchronized (AjustadorData.class){
            if (ajustadorData == null){
                ajustadorData = buildRetrofit(AjustadorData.class);
            }
            return ajustadorData;
        }
    }

    public static ServicioCoberturaData getServicioCoberturaData(){
        synchronized (ServicioCoberturaData.class){
            if (servicioCoberturaData == null){
                servicioCoberturaData = Aspects.aspect(ServicioCoberturaData.class, buildRetrofit(ServicioCoberturaData.class));
            }
            return servicioCoberturaData;
        }
    }


    public static RecuperacionData getRecuperacionData(){
        synchronized (RecuperacionData.class){
            if (recuperacionData == null){
                recuperacionData = Aspects.aspect(RecuperacionData.class, buildRetrofit(RecuperacionData.class));
            }
            return recuperacionData;
        }
    }

    public static PolizaData getPolizaData(){
        synchronized (PolizaData.class){
            if (polizaData == null){
                polizaData = Aspects.aspect(PolizaData.class, buildRetrofit(PolizaData.class));
            }
            return polizaData;
        }
    }

    public static AmisData getAmisData(){
        synchronized (AmisData.class){
            if (amisData == null){
                amisData = Aspects.aspect(AmisData.class, buildRetrofit(AmisData.class));
            }
            return amisData;
        }
    }

    private synchronized static <T> T buildRetrofit(Class<T> type){
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utils.BASE_URL_ADD)
                    .client(RetrofitResources.getUnsafeOkHttpClient(
                            RetrofitResources.getLoggingInterceptor(),
                            new OrquestadorSecurityInterceptor(noSecurityPaths),
                            new OrquestadorErrorInterceptor()
                    )).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(type);
    }

}
