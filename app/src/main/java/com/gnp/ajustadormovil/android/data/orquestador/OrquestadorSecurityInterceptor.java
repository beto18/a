package com.gnp.ajustadormovil.android.data.orquestador;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.context.AuthenticationData;
import com.gnp.ajustadormovil.android.data.context.ContextDataFactory;
import com.gnp.ajustadormovil.android.data.context.TokenData;
import com.gnp.ajustadormovil.android.models.context.Login;
import com.gnp.ajustadormovil.android.models.context.RefreshTokenCommand;
import com.gnp.ajustadormovil.android.models.context.Token;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OrquestadorSecurityInterceptor implements Interceptor {

    private static final String TAG = OrquestadorSecurityInterceptor.class.getCanonicalName();

    private static final long EXPIRE_TIME = 5 * 60;

    private final TokenData tokenData = ContextDataFactory.getTokenData();

    private final AuthenticationData loginData = ContextDataFactory.getAuthenticationData();

    private final List<String> filters;

    public OrquestadorSecurityInterceptor(List<String> filters){
        this.filters = filters;
    }

    private boolean isSecured(Request request){
        boolean secured = true;
        for(String filter : filters){
            secured &= !request.url().uri().getPath().contains(filter);
        }
        return secured;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        if(isSecured(request) && AppPreferences.getString(AppPreferences.USER_INFO_TOKEN) != null) {
            Token token = tokenData.parse(AppPreferences.getString(AppPreferences.USER_INFO_TOKEN));
            boolean expired = (token.getExpiration() - EXPIRE_TIME) < (System.currentTimeMillis() / 1000);
            Log.d(TAG, "Token expirado: " + expired);
            if(expired){
                String refreshToken = AppPreferences.getString(AppPreferences.USER_INFO_REFRESH_TOKEN);
                try {
                    Login login = RXUtils.sync(loginData.refresh(new RefreshTokenCommand(refreshToken)));
                    Log.i(TAG, "Actualizar token");
                    AppPreferences.setString(AppPreferences.USER_INFO_TOKEN, login.getJwt());
                    AppPreferences.setString(AppPreferences.USER_INFO_REFRESH_TOKEN, login.getRefreshToken());
                } catch (IOException ex) {
                    throw ex;
                } catch (Exception ex) {
                    Log.e(TAG, "Error al intentar obtener nuevo token", ex);
                    throw new IOException(ex);
                }
            }
            request = request.newBuilder()
                .addHeader("Authorization", "Bearer " + AppPreferences.getString(AppPreferences.USER_INFO_TOKEN))
                .build();
        }
        return chain.proceed(request);
    }
}
