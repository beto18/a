package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.SerieResponse;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Costo;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PolizaData {

    @Headers("Content-Type: application/json")
    @GET("polizas/danos/calculo/asegurado")
    Single<Costo> getCostosAsegurado(@Query("categoria") String categoria,
                                         @Query("partes") List<Long> partes);

    @Headers("Content-Type: application/json")
    @GET("amis/polizas/{vin}")
    Single<List<SerieResponse>> getPolizas(@Path("vin") String vin);

    @Headers("Content-Type: application/json")
    @GET("polizas/danos/calculo/terceros")
    Single<Costo> getCostosTercero(@Query("categoria") String categoria,
                                   @Query("partes") List<Long> partes);

}
