package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Comprobante;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RecuperacionData {

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones")
    Single<List<Recuperacion>> getRecuperaciones(@Path("idAjuste") String idAjuste,
                                                 @Path("idTercero") Long idTercero);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/{idRecuperacion}")
    Single<Recuperacion> getRecuperacion(@Path("idAjuste") String idAjuste,
                                             @Path("idTercero") Long idTercero,
                                             @Path("idRecuperacion") Long idRecuperacion);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/{idRecuperacion}")
    Completable deleteRecuperacion(@Path("idAjuste") String idAjuste,
                                   @Path("idTercero") Long idTercero,
                                   @Path("idRecuperacion") Long idRecuperacion);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/monetaria")
    Single<RecuperacionMonetaria> addMonetaria(@Path("idAjuste") String idAjuste,
                                                   @Path("idTercero") Long idTercero,
                                                   @Body RecuperacionMonetaria recuperacion);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/turnado-asistencia-legal")
    Single<RecuperacionTurnadoAsistenciaLegal> addTurnadoAsistenciaLegal(@Path("idAjuste") String idAjuste,
                                                                             @Path("idTercero") Long idTercero,
                                                                             @Body RecuperacionTurnadoAsistenciaLegal recuperacion);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/turnado-asistencia-legal/{idRecuperacion}")
    Completable modifyTurnadoAsistenciaLegal(@Path("idAjuste") String idAjuste,
                                                                    @Path("idTercero") Long idTercero,
                                                                    @Path("idRecuperacion") Long idRecuperacion,
                                                                    @Body RecuperacionTurnadoAsistenciaLegal recuperacion);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/mesa-controversia")
    Single<RecuperacionMesaControversia> addMesaControversia(@Path("idAjuste") String idAjuste,
                                                                 @Path("idTercero") Long idTercero,
                                                                 @Body RecuperacionMesaControversia recuperacion);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/mesa-controversia/{idRecuperacion}")
    Completable modifyMesaControversia(@Path("idAjuste") String idAjuste,
                                        @Path("idTercero") Long idTercero,
                                        @Path("idRecuperacion") Long idRecuperacion,
                                        @Body RecuperacionMesaControversia recuperacion);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/garantia")
    Single<RecuperacionGarantiaPrendaria> addGarantiaPrendaria(@Path("idAjuste") String idAjuste,
                                                                   @Path("idTercero") Long idTercero,
                                                                   @Body RecuperacionGarantiaPrendaria recuperacion);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/compania")
    Single<RecuperacionCompaniaSeguros> addCompaniaSeguros(@Path("idAjuste") String idAjuste,
                                                               @Path("idTercero") Long idTercero,
                                                               @Body RecuperacionCompaniaSeguros recuperacion);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/{idRecuperacion}/comprobante")
    Single<Archivo> addComprobante(@Path("idAjuste") String idAjuste,
                                       @Path("idTercero") Long idTercero,
                                       @Path("idRecuperacion") Long idRecuperacion,
                                       @Body Comprobante comprobante);

    @Multipart
    @POST("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/{idRecuperacion}/fotografias")
    Single<List<Fotografia>> addFotografia(@Path("idAjuste") String idAjuste,
                                               @Path("idTercero") Long idTercero,
                                               @Path("idRecuperacion") Long idRecuperacion,
                                               @Part("archivos\"; filename=\"imagen.jpg\"") RequestBody image);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/{idTercero}/recuperaciones/{idRecuperacion}/fotografias")
    Single<List<Fotografia>> getFotografias(@Path("idAjuste") String idAjuste,
                                                @Path("idTercero") Long idTercero,
                                                @Path("idRecuperacion") Long idRecuperacion);


}
