package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;

import java.util.List;

public interface RecuperacionRepository {

    List<Recuperacion> findByAjusteAndTercero(String ajuste, Long tercero);

    Recuperacion findByAjusteAndTerceroAndId(String ajuste, Long tercero, Long id);

    void deleteByAjusteAndTerceroAndId(String ajuste, Long tercero, Long id);

    void deleteByAjusteAndTercero(String ajuste, Long tercero);

    void deleteByAjuste(String ajuste);

    void saveByAjusteAndTercero(String ajuste, Long tercero, Recuperacion recuperacion);

}
