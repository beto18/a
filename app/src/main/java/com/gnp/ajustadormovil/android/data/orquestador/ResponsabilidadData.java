package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ResponsabilidadData {

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/responsabilidades")
    Single<Responsabilidad> addResponsabilidad(@Path("ajuste") String ajuste,
                                               @Body Responsabilidad responsabilidad);

    @Multipart
    @POST("ajustes/{ajuste}/responsabilidades/fotografias")
    Single<List<Fotografia>> addFotografia(@Path("ajuste") String ajuste,
                                               @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/responsabilidades")
    Completable deleteResponsabilidad(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/responsabilidades")
    Single<Responsabilidad> getResponsabilidad(@Path("ajuste") String ajuste);

}
