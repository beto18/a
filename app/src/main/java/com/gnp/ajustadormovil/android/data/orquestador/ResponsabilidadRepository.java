package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;

public interface ResponsabilidadRepository {

    Responsabilidad findByAjuste(String ajuste);

    void save(Responsabilidad responsabilidad);

    void deleteByAjuste(String ajuste);

}
