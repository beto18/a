package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.ComentarioCancelacion;
import com.gnp.ajustadormovil.android.models.services.ArriboServicio;
import com.gnp.ajustadormovil.android.models.services.ServicioResponse;
import com.gnp.ajustadormovil.android.models.services.service.ServiceData;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ServicioCoberturaData {

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id_ajuste}/confirmacion")
    Completable sendStatus(@Path("id_ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/servicios/")
    Single<List<ServicioResponse>> getServicioCobertura(@Path("idAjuste") String idAjuste);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{idAjuste}/servicios/resumen")
    Single<List<String>> addResumen(@Path("idAjuste") String idAjuste, @Body List<String> list);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/servicios/resumen")
    Single<List<String>> getResumen(@Path("idAjuste") String idAjuste);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/{id}/cancelacion")
    Completable getServicioCoberturaDelete(@Path("idAjuste") String idAjuste,
                                            @Path("id") int idServicio,
                                            @Body ComentarioCancelacion data);

    @Headers("Content-Type: application/json")
    @POST("servicioscobertura/cambioStatus")
    Single<String> getServicioCoberturaArribo(@Body ArriboServicio arriboServicio);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/patrullas")
    Completable sendPolices(@Path("idAjuste") String id_ajuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/abogados")
    Completable sendLawyer(@Path("idAjuste") String id_ajuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{id_ajuste}/servicios/gruas")
    Completable sendCrane(@Path("id_ajuste") String id_ajuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/ambulancias")
    Completable sendAmbulance(@Path("idAjuste") String idAjuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/verificadores")
    Completable sendChecker(@Path("idAjuste") String idAjuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/recuperadores")
    Completable sendRecuperator(@Path("idAjuste") String idAjuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/funerarios")
    Completable sendFunerary(@Path("idAjuste") String idAjuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/contratistas")
    Completable sendContractor(@Path("idAjuste") String idAjuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/servicios/taxis")
    Completable sendTaxi(@Path("idAjuste") String idAjuste, @Body ServiceData data);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/servicios/{id}/arribo")
    Completable sendArribo(@Path("ajuste") String idAjuste, @Path("id") String id);

}
