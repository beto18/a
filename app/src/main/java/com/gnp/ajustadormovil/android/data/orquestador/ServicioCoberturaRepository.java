package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.servicio.ServiciosCobertura;

public interface ServicioCoberturaRepository {

    ServiciosCobertura findByAjuste(String ajuste);

    void save(ServiciosCobertura servicios);

    void deleteByAjuste(String ajuste);
}
