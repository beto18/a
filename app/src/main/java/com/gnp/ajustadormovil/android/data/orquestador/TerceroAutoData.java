package com.gnp.ajustadormovil.android.data.orquestador;


import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAutoFolio;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface TerceroAutoData {

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos")
    Single<List<TerceroAuto>> getAll(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajustes}/terceros/autos")
    Single<TerceroAuto> add(@Path("ajustes") String ajustes,
                            @Body TerceroAutoFolio folio);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}")
    Single<TerceroAuto> get(@Path("ajuste") String ajuste,
                            @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/terceros/autos/{idTercero}")
    Completable delete(@Path("ajuste") String ajuste,
                       @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajuste}/terceros/autos/{idTercero}/conductor")
    Completable modifyConductor(@Path("ajuste") String ajuste,
                       @Path("idTercero") long idTercero,
                       @Body Persona conductor);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}/conductor/fotografias")
    Single<List<Fotografia>> getFotografiasConductor(@Path("ajuste") String ajuste,
                                                         @Path("idTercero") long idTercero);

    @Multipart
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/conductor/fotografias")
    Single<List<Fotografia>> addFotografiaConductor(@Path("ajuste") String ajuste,
                                                        @Path("idTercero") long idTercero,
                                                        @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/terceros/autos/{idTercero}/conductor/fotografias/{idFotografia}")
    Completable deleteFotografiaConductor(@Path("ajuste") String ajuste,
                                          @Path("idTercero") long idTercero,
                                          @Path("idFotografia") long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/conductor/volante")
    Single<Archivo> addVolanteConductor(@Path("ajuste") String ajuste,
                                            @Path("idTercero") long idTercero,
                                            @Body VolanteMedico conductor);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}/vehiculo")
    Single<VehiculoTercero> getVehiculo(@Path("idAjuste") String ajuste,
                                            @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajustes}/terceros/autos/{idTercero}/vehiculo")
    Completable modifyVehiculo(@Path("ajustes") String ajuste,
                               @Path("idTercero") long idTercero,
                               @Body VehiculoTercero vehiculo);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}/vehiculo/fotografias")
    Single<List<Fotografia>> getFotografiaVehiculo(@Path("ajuste") String ajuste,
                                                       @Path("idTercero") long idTercero);

    @Multipart
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/vehiculo/fotografias")
    Single<List<Fotografia>> addFotografiaVehiculo(@Path("ajuste") String ajuste,
                                                       @Path("idTercero") long idTercero,
                                                       @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/terceros/autos/{idTercero}/vehiculo/fotografias/{idFotografia}")
    Completable deleteFotografiaVehiculo(@Path("ajuste") String ajuste,
                                         @Path("idTercero") long idTercero,
                                         @Path("idFotografia") long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/vehiculo/volante")
    Single<Archivo> addVolanteVehiculo(@Path("ajuste") String ajuste,
                                           @Path("idTercero") long idTercero,
                                           @Body VolanteVehiculoTercero volante);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes")
    Single<List<Ocupante>> getOcupantes(@Path("ajuste") String ajuste,
                                        @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes")
    Single<Ocupante> addOcupante(@Path("ajuste") String ajuste,
                                     @Path("idTercero") long idTercero,
                                     @Body Ocupante ocupante);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}")
    Single<Ocupante> getOcupante(@Path("ajuste") String ajuste,
                                     @Path("idTercero") long idTercero,
                                     @Path("idOcupante") long idOcupante);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}")
    Completable modifyOcupante(@Path("ajuste") String ajuste,
                               @Path("idTercero") long idTercero,
                               @Path("idOcupante") long idOcupante,
                               @Body Ocupante ocupante);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{idAjuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}")
    Completable deleteOcupante(@Path("idAjuste") String ajuste,
                               @Path("idTercero") long idTercero,
                               @Path("idOcupante") long idOcupante);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}/fotografias")
    Single<List<Fotografia>> getFotografiasOcupante(@Path("ajuste") String ajuste,
                                                        @Path("idTercero") long idTercero,
                                                        @Path("idOcupante") long idOcupante);

    @Multipart
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}/fotografias")
    Single<List<Fotografia>> addFotografiaOcupante(@Path("ajuste") String ajuste,
                                                       @Path("idTercero") long idTercero,
                                                       @Path("idOcupante") long idOcupante,
                                                       @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}/fotografias/{idFotografia}")
    Completable deleteFotografiaOcupante(@Path("ajuste") String ajuste,
                                         @Path("idTercero") long idTercero,
                                         @Path("idOcupante") long idOcupante,
                                         @Path("idFotografia") long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/terceros/autos/{idTercero}/ocupantes/{idOcupante}/volante")
    Single<Archivo> addVolanteOcupante(@Path("ajuste") String ajuste,
                                           @Path("idTercero") long idTercero,
                                           @Path("idOcupante") long idOcupante,
                                           @Body VolanteMedico volante);

}
