package com.gnp.ajustadormovil.android.data.orquestador;


import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;

import java.util.List;

public interface TerceroAutoRepository {

    List<TerceroAuto> findByAjuste(String ajuste);

    TerceroAuto findByAjusteAndId(String ajuste, Long id);

    TerceroAuto findByAjusteAndFolioAmis(String ajuste, String folioAmis);

    void deleteByAjuste(String ajuste);

    void deleteByAjusteAndId(String ajuste, Long id);

    void saveByAjuste(String ajuste, TerceroAuto tercero);

}
