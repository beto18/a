package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface TerceroData {

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros")
    Single<List<Tercero>> getAll(@Path("ajuste") String ajuste);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/{id}")
    Single<Tercero> get(@Path("ajuste") String ajuste, @Path("id") long id);

}
