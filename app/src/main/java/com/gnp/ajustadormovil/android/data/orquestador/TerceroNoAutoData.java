package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface TerceroNoAutoData {

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/no-autos")
    Single<List<TerceroNoAuto>> getAll(@Path("idAjuste") String ajuste);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/no-autos")
    Single<TerceroNoAuto> add(@Path("idAjuste") String ajuste,
                              @Body TerceroNoAuto tercero);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/no-autos/{idTercero}")
    Single<TerceroNoAuto> get(@Path("idAjuste") String ajuste,
                              @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{idAjuste}/terceros/no-autos/{idNoAuto}")
    Completable modify(@Path("idAjuste") String ajuste,
                       @Path("idNoAuto") long idTercero,
                       @Body TerceroNoAuto tercero);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{idAjuste}/terceros/no-autos/{idNoAuto}")
    Completable delete(@Path("idAjuste") String ajuste,
                       @Path("idNoAuto") long idTercero);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/no-autos/{idTercero}/fotografias")
    Single<List<Fotografia>> getFotografias(@Path("idAjuste") String ajuste,
                                                @Path("idTercero") long idTercero);

    @Multipart
    @POST("ajustes/{idAjuste}/terceros/no-autos/{idNoAuto}/fotografias")
    Single<List<Fotografia>> addFotografia(@Path("idAjuste") String ajuste,
                                               @Path("idNoAuto") long idTercero,
                                               @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{idAjuste}/terceros/no-autos/{idTercero}/fotografias/{idFotografia}")
    Completable deleteFotografia(@Path("idAjuste") String ajuste,
                                  @Path("idTercero") long idTercero,
                                  @Path("idFotografia") long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/no-autos/{idTercero}/volante")
    Single<Archivo> addVolante(@Path("idAjuste") String ajuste,
                                   @Path("idTercero") long idTercero,
                                   @Body VolanteTerceroNoAuto volante);


}
