package com.gnp.ajustadormovil.android.data.orquestador;


import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;

import java.util.List;

public interface TerceroNoAutoRepository {

    List<TerceroNoAuto> findByAjuste(String ajuste);

    TerceroNoAuto findByAjusteAndId(String ajuste, Long id);

    void deleteByAjuste(String ajuste);

    void deleteByAjusteAndId(String ajuste, Long id);

    void saveByAjuste(String ajuste, TerceroNoAuto tercero);

}
