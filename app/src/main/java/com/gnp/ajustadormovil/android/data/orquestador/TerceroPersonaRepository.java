package com.gnp.ajustadormovil.android.data.orquestador;


import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;

import java.util.List;

public interface TerceroPersonaRepository {

    List<TerceroPersona> findByAjuste(String ajuste);

    TerceroPersona findByAjusteAndId(String ajuste, Long id);

    void deleteByAjuste(String ajuste);

    void deleteByAjusteAndId(String ajuste, Long id);

    void saveByAjuste(String ajuste, TerceroPersona tercero);

}
