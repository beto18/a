package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;

import java.util.List;

public interface TerceroRepository {

    List<Tercero> findByAjuste(String ajuste);

    Tercero findByAjusteAndId(String ajuste, Long id);

    void deleteByAjuste(String ajuste);

}
