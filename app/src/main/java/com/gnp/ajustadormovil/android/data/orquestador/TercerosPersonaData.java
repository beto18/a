package com.gnp.ajustadormovil.android.data.orquestador;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface TercerosPersonaData {

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/personas")
    Single<List<TerceroPersona>> getAll(@Path("idAjuste") String ajuste);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{idAjuste}/terceros/personas")
    Single<TerceroPersona> add(@Path("idAjuste") String ajuste,
                               @Body Persona tercero);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{idAjuste}/terceros/personas/{idTercero}")
    Single<TerceroPersona> get(@Path("idAjuste") String ajuste,
                               @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/terceros/personas/{idTercero}")
    Completable delete(@Path("ajuste") String ajuste,
                       @Path("idTercero") long idTercero);

    @Headers("Content-Type: application/json")
    @PUT("ajustes/{ajuste}/terceros/personas/{idTercero}")
    Completable modify(@Path("ajuste") String ajuste,
                       @Path("idTercero") long idTercero,
                       @Body Persona tercero);

    @Headers("Content-Type: application/json")
    @GET("ajustes/{ajuste}/terceros/personas/{idTercero}/fotografias")
    Single<List<Fotografia>> getFotografias(@Path("ajuste") String ajuste,
                                            @Path("idTercero") long idTercero);

    @Multipart
    @POST("ajustes/{ajuste}/terceros/personas/{idTercero}/fotografias")
    Single<List<Fotografia>> addFotografia(@Path("ajuste") String ajuste,
                                               @Path("idTercero") long idTercero,
                                               @Part("archivos\"; filename=\"image.jpg\" ") RequestBody image);

    @Headers("Content-Type: application/json")
    @DELETE("ajustes/{ajuste}/terceros/personas/{idTercero}/fotografias/{idFotografia}")
    Completable deleteFotografia(@Path("ajuste") String ajuste,
                                 @Path("idTercero") long idTercero,
                                 @Path("idFotografia") long idFotografia);

    @Headers("Content-Type: application/json")
    @POST("ajustes/{ajuste}/terceros/personas/{idTercero}/volante")
    Single<Archivo> addVolante(@Path("ajuste") String ajuste,
                               @Path("idTercero") long idTercero,
                               @Body VolanteMedico volante);

}
