package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.context.ContextDataFactory;
import com.gnp.ajustadormovil.android.data.context.TokenData;
import com.gnp.ajustadormovil.android.data.orquestador.AjustadorRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.context.Usuario;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.util.ArrayList;

public class AjustadorRepositoryRealmImpl extends RealmRepository<Ajustador, String> implements AjustadorRepository {

    private final TokenData tokenData = ContextDataFactory.getTokenData();

    public AjustadorRepositoryRealmImpl(){
        super(Ajustador.class);
    }

    @Override
    public Ajustador findOne() {
        Ajustador ajustador = null;
        String token = AppPreferences.getString(AppPreferences.USER_INFO_TOKEN);
        if(token != null) {
            Usuario user = tokenData.getUser(token);
            ajustador = findById(user.getCorreo().toLowerCase());
            if (ajustador == null) {
                ajustador = new Ajustador();
                ajustador.setNombre(user.getNombres());
                ajustador.setApellidoPaterno(user.getApellidoPaterno());
                ajustador.setApellidoMaterno(user.getApellidoMaterno());
                ajustador.setCorreoElectronico(user.getCorreo());
                ajustador.setCedula(AjustadorMovilApplication.getCedula());
                ajustador.setHorario(new ArrayList<>());
                save(ajustador);
            }
        }
        return ajustador;
    }

    @Override
    public void delete() {
        Ajustador ajustador = findOne();
        if(ajustador != null) {
            deleteById(ajustador.getCorreoElectronico());
        }
    }

    @Override
    public void save(Ajustador entity) {
        entity.setCorreoElectronico(entity.getCorreoElectronico().toLowerCase());
        super.save(entity);
    }
}
