package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;

public class AjusteRepositoryRealmImpl extends RealmRepository<Ajuste, String> implements AjusteRepository {

    public AjusteRepositoryRealmImpl(){
        super(Ajuste.class);
    }

}
