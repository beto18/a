package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AmisData;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.amis.AjusteAmis;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.AseguradoAmis;
import com.gnp.ajustadormovil.android.models.amis.Certificado;
import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;
import com.gnp.ajustadormovil.android.models.amis.Circunstancia;
import com.gnp.ajustadormovil.android.models.amis.Danios;
import com.gnp.ajustadormovil.android.models.amis.EmisionValeDigital;
import com.gnp.ajustadormovil.android.models.amis.Folio;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.amis.Involucrado;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoData;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoDetalles;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoGeneral;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoModulo;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.models.amis.Narrativa;
import com.gnp.ajustadormovil.android.models.amis.PaseMedico;
import com.gnp.ajustadormovil.android.models.amis.PolizaAmis;
import com.gnp.ajustadormovil.android.models.amis.Refoleo;
import com.gnp.ajustadormovil.android.models.amis.SiniestroAmis;
import com.gnp.ajustadormovil.android.models.amis.ValeDigital;
import com.gnp.ajustadormovil.android.models.amis.VehiculoAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.codec.Mod1271_36;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public class AmisDataMockImpl implements AmisData {

    private static final String EDUA_DATE_PATTERN = "ddMMyyHHmmss";

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private static final List<CertificadoCompania> certificados;

    static {
        certificados = new ArrayList<>();
        CertificadoCompania certificado = new CertificadoCompania();
        certificado.setCertificado("LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlFZlRDQ0EyV2dBd0lCQWdJVUpvWkExTmRvYmNtM3FvcUZnK0ovSzVHdzZYNHdEUVlKS29aSWh2Y05BUUVMDQpCUUF3Z2MweEN6QUpCZ05WQkFZVEFrMVlNUmt3RndZRFZRUUlEQkJEU1ZWRVFVUWdSRVVnVFVWWVNVTlBNUmN3DQpGUVlEVlFRSERBNUJURlpCVWs4Z1QwSlNSVWRQVGpFcE1DY0dBMVVFQ2d3Z1IxSlZVRThnVGtGRFNVOU9RVXdnDQpVRkpQVmtsT1EwbEJUQ0JUTGtFdVFpNHhGVEFUQmdOVkJBc01ERWRPVURreU1URXlORFJRTURFak1DRUdBMVVFDQpBd3dhUVVSTlNVNUpVMVJTUVVSUFVpQkRUMDFRUVU1SlFTQkhUbEF4SXpBaEJna3Foa2lHOXcwQkNRRVdGR016DQpNUzVoWkcxcGJrQmlhV0ZoYm1rdVkyOXRNQjRYRFRFNU1EWXlNekl4TkRnMU0xb1hEVEk1TURZeU1ESXhORGcxDQpNMW93Z2MweEN6QUpCZ05WQkFZVEFrMVlNUmt3RndZRFZRUUlEQkJEU1ZWRVFVUWdSRVVnVFVWWVNVTlBNUmN3DQpGUVlEVlFRSERBNUJURlpCVWs4Z1QwSlNSVWRQVGpFcE1DY0dBMVVFQ2d3Z1IxSlZVRThnVGtGRFNVOU9RVXdnDQpVRkpQVmtsT1EwbEJUQ0JUTGtFdVFpNHhGVEFUQmdOVkJBc01ERWRPVURreU1URXlORFJRTURFak1DRUdBMVVFDQpBd3dhUVVSTlNVNUpVMVJTUVVSUFVpQkRUMDFRUVU1SlFTQkhUbEF4SXpBaEJna3Foa2lHOXcwQkNRRVdGR016DQpNUzVoWkcxcGJrQmlhV0ZoYm1rdVkyOXRNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDDQpBUUVBeWdGUmUxWjV3YndEOTNVQXVxQTlzVkwwdWZLeEdseE4vN0h1bzNjRHM5NTV1WlhObndDQmdWbFBFRUtDDQoweUU0dEZKdW05NXAveGxEc1Qvc3J3MmJ6RnN0bU1jMFlMNm1DR3RoU28rRjdHWUVuaVhnblFMRXg5K2Y0MGlqDQpBOFJjVUxVV0FUMldqbzdVVzVsRlQ2Z25jcFZqQVhZSytld3NNNml6Q2d2WC9aRDJWQWFaUVVqOStDTEZ5eUx6DQpjekNvc3Q4ZFNSc0J5RmM5Q0pvcUZtdTF1S0ltOFBieTZyaTlPajhSUjh3MGxjb1NlUHgraS9hQVArRVB4aDRiDQpxallZN1lHUHRZeXlYN0p3YlB0TmpYWEZxMHJGWVMxSkx1ek5ORHVLM2wrYlRXTEt4ODlXOXlXdndvUThubUc1DQo5ZFdSRkl6RHB2NlVySmRjeWVsVlZ3Ympxd0lEQVFBQm8xTXdVVEFkQmdOVkhRNEVGZ1FVNkRRQi9xMVJFTlFvDQpGbWplMEZHMTdJZy9FSEl3SHdZRFZSMGpCQmd3Rm9BVTZEUUIvcTFSRU5Rb0ZtamUwRkcxN0lnL0VISXdEd1lEDQpWUjBUQVFIL0JBVXdBd0VCL3pBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQWZXdS9yTlRvRnJXbkpqY0xlNno2DQozRE03a2xDUDBNUWVsMFlTb1ZRSnM5SUZLbkJldm9IcWZUaG9ESVNjM3hsaG5PdXBnU1ZCUTJGRXdUWVdobUc4DQpXZ2x4NFVGMURyY09TSVJZcTNqU0xjQUx0NTY4bzNjbHIvUVdIWnZCMG94eFh0QUVzOTlMQmtmdDZ1ajRCREtuDQpkbkVsS090NTlGMWFrVmUrRXVJbHl2WUExV0dnUzk2RGVoZ1J5d29HSjRGdVF1TElpMFppM2VXVTY3bEJvSTBpDQpyU21uVjhGd2FXdHNsazdUcjVmbzE1RGVTdmFYNmFyTXh4L1BsRk5WMGcrd2ZyNUhGdmNDcUErWDJXWUVUU2xwDQpvR0dnb3JkUzhXOWxCWGJZcnVhbWVDdTlWQmVFdklLYmNxMGtJT29XVWdJbFgyZ2VKUjFZcTVDVjRhbG9CNlA4DQprUT09DQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tDQo=");
        certificado.setCompania(Long.parseLong(Constants.AMIS_ID_GNP.toString()));
        certificado.setLlavePublica("-----BEGIN PUBLIC KEY----- MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAygFRe1Z5wbwD93UAuqA9 sVL0ufKxGlxN/7Huo3cDs955uZXNnwCBgVlPEEKC0yE4tFJum95p/xlDsT/srw2b zFstmMc0YL6mCGthSo+F7GYEniXgnQLEx9+f40ijA8RcULUWAT2Wjo7UW5lFT6gn cpVjAXYK+ewsM6izCgvX/ZD2VAaZQUj9+CLFyyLzczCost8dSRsByFc9CJoqFmu1 uKIm8Pby6ri9Oj8RR8w0lcoSePx+i/aAP+EPxh4bqjYY7YGPtYyyX7JwbPtNjXXF q0rFYS1JLuzNNDuK3l+bTWLKx89W9yWvwoQ8nmG59dWRFIzDpv6UrJdcyelVVwbj qwIDAQAB -----END PUBLIC KEY----- ");
        certificados.add(certificado);
        certificado = new CertificadoCompania();
        certificado.setCertificado("LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlFWXpDQ0EwdWdBd0lCQWdJVU9MeXNydG12RlpqK21SN0F3MW9uOVAzS1hHc3dEUVlKS29aSWh2Y05BUUVMDQpCUUF3Z2NBeEN6QUpCZ05WQkFZVEFrMVlNUmt3RndZRFZRUUlEQkJEU1ZWRVFVUWdSRVVnVFVWWVNVTlBNUmN3DQpGUVlEVlFRSERBNUJURlpCVWs4Z1QwSlNSVWRQVGpFYk1Ca0dBMVVFQ2d3U1FrSldRU0JDWVc1amIyMWxjaUJUDQpMa0V1TVJVd0V3WURWUVFMREF4Q1FrRTRNekE0TXpGTVNqSXhKREFpQmdOVkJBTU1HMEZFVFVsT1NWTlVVa0ZFDQpUMUlnUTA5TlVFRk9TVUVnUWtKV1FURWpNQ0VHQ1NxR1NJYjNEUUVKQVJZVVl6VXhMbUZrYldsdVFHSnBZV0Z1DQphUzVqYjIwd0hoY05NVGt3T0RFME1EQXlNVEk0V2hjTk1qa3dPREV4TURBeU1USTRXakNCd0RFTE1Ba0dBMVVFDQpCaE1DVFZneEdUQVhCZ05WQkFnTUVFTkpWVVJCUkNCRVJTQk5SVmhKUTA4eEZ6QVZCZ05WQkFjTURrRk1Wa0ZTDQpUeUJQUWxKRlIwOU9NUnN3R1FZRFZRUUtEQkpDUWxaQklFSmhibU52YldWeUlGTXVRUzR4RlRBVEJnTlZCQXNNDQpERUpDUVRnek1EZ3pNVXhLTWpFa01DSUdBMVVFQXd3YlFVUk5TVTVKVTFSU1FVUlBVaUJEVDAxUVFVNUpRU0JDDQpRbFpCTVNNd0lRWUpLb1pJaHZjTkFRa0JGaFJqTlRFdVlXUnRhVzVBWW1saFlXNXBMbU52YlRDQ0FTSXdEUVlKDQpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFOTXA3S2k2R3EwK1J4cW02T08yQUdYNSsyVm5lVzJsDQo4MXI0aXdQWWJjKzdDQU1zK1VyUVJzWEIyS0xZMFFwVXVEY0dtTFpvcURpVjZJYy9BL0JEdklrVXJiSmc2WWo5DQozWGFXMGxGQjgwa1RHTkprMDJZN2FrRy8rR3krb1hPcHBDS3paMkI0N1pyYjNJeHE2TUdNKyszU1REWnJIdW00DQpuQXloU0FCMjFueGx6a2kybHhlK3pyMFY5MlNwQ1QrYlVwSVBKNmNJNlk5TVhMbXRZN3pYMDdJak9xNjViUkdyDQpXNmdrTXlnQ1pMZjUrRTBNMzlScEVCQStVK21sN1JFMHk4VDVWSDVJUWRwZUtFMXVqb2RYeGlMdmFMUU9BbUN1DQpUd2hMQnRXR2tBMmFSOWpBMTdoWmgweUdwTW1JblVJaXFlNHd2S251L3kxeDA0ZFFoY2sxNlNrQ0F3RUFBYU5UDQpNRkV3SFFZRFZSME9CQllFRkl3T1N2TGNvQlBOZFhiUlk1UGt4dWhEUUhXT01COEdBMVVkSXdRWU1CYUFGSXdPDQpTdkxjb0JQTmRYYlJZNVBreHVoRFFIV09NQThHQTFVZEV3RUIvd1FGTUFNQkFmOHdEUVlKS29aSWh2Y05BUUVMDQpCUUFEZ2dFQkFHcEFEQVh5NnptcSs2U2NyRkNvSndraVk3VjFNUTBIZVlabXRpQWJPZUU0bU9HQnJJdHZJclp3DQo3MXhqOHphUVNCcmFxdFZKQ0dSZFpuRm05UVdvWWkvM3hTNVFnc0JVM0hUN0pyb1JnbStuNXRtcmRnempTNjRLDQo1TnBBNElhTlgvdjJOdytZWE4wZk84QTlSWnIvSnBNVVlUTE8yRy9KMWhnRXhBR2ZWa05TTGhJZW9VazVGdDJVDQpoS1RaZG5FeGR2MHFiUmFNWGVPYldmS0VQN1Qrai91dmF3YndtMnpheWNvd2d1c3dXUUN3eHhqSnQ2bDMxc05zDQpwWDRSeUducVJscC85QzM2Q3RPYlh2UThPc3lkS3U4Y1Z4elZkQ3NMOG44WU8zeEhSQXVoOXh4ZU1CWWxqTEdWDQp3MWQrTms3V1Q2WVhiZGRqYnR0MHo5czI4eUlJTEUwPQ0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ0K");
        certificado.setCompania(51L);
        certificado.setLlavePublica("-----BEGIN PUBLIC KEY----- MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0ynsqLoarT5HGqbo47YA Zfn7ZWd5baXzWviLA9htz7sIAyz5StBGxcHYotjRClS4NwaYtmioOJXohz8D8EO8 iRStsmDpiP3ddpbSUUHzSRMY0mTTZjtqQb/4bL6hc6mkIrNnYHjtmtvcjGrowYz7 7dJMNmse6bicDKFIAHbWfGXOSLaXF77OvRX3ZKkJP5tSkg8npwjpj0xcua1jvNfT siM6rrltEatbqCQzKAJkt/n4TQzf1GkQED5T6aXtETTLxPlUfkhB2l4oTW6Oh1fG Iu9otA4CYK5PCEsG1YaQDZpH2MDXuFmHTIakyYidQiKp7jC8qe7/LXHTh1CFyTXp KQIDAQAB -----END PUBLIC KEY----- ");
        certificados.add(certificado);

    }

    @Override
    public Single<Folio> generarFolio(String ajuste) {
        return Single.create(e -> {
            if(ConnectionProvider.getInstance().isConnected()) {
                Ajuste a = RepositoryFactory.getAjusteRepository().findById(ajuste);
                Date fechaArribo = a.getFechaArribo();
                String folio = Constants.CLAVE_COMPANIA + AjustadorMovilApplication.getCedula()+ DateUtils.toString(fechaArribo, EDUA_DATE_PATTERN);
                Folio mFolio = new Folio();
                mFolio.setFolio(new Mod1271_36().compute(folio) + "A");
                e.onSuccess(mFolio);
            } else {
                e.onError(new NetworkException("No hay internet"));
            }
        });
    }

    @Override
    public Completable guardarFolio(String ajuste, Folio folio) {
        return Completable.complete();
    }

    @Override
    public Completable vincularFolios(String ajuste, Collection<String> folios) {
        return Completable.create(e -> {
           if(ConnectionProvider.getInstance().isConnected()) {
               e.onComplete();
           } else {
               e.onError(new NetworkException("No hay internet"));
           }
        });
    }

    @Override
    public Single<List<Refoleo>> refolear(String ajuste) {
        return Single.create(e -> {
            if (ConnectionProvider.getInstance().isConnected()) {
                final String abcedario = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                Amis amis = amisRepository.findByAjuste(ajuste);
                List<Refoleo> folios = new ArrayList<>();
                folios.add(new Refoleo(amis.getFolio().getFolioInicial()));
                for (FolioContraparteAmis contraparte : amis.getContrapartes()) {
                    folios.add(new Refoleo(contraparte.getFolioInicial()));
                }
                Function<String, Date> folioToDate = folio -> {
                    String fecha = folio.substring(folio.length() - (12 + 3), folio.length() - 3);
                    return new SimpleDateFormat("ddMMyyHHmmss", Locale.forLanguageTag("es-MX")).parse(fecha);
                };

                Collections.sort(folios, (a, b) -> {
                    try {
                        Date fechaA = folioToDate.apply(a.getFolioInicial());
                        Date fechaB = folioToDate.apply(b.getFolioInicial());
                        return fechaA.compareTo(fechaB);
                    } catch (Exception ex) {
                        return 0;
                    }
                });

                String folio = folios.get(0).getFolioInicial();
                folio = folio.substring(0, folio.length() - 1);
                for (int i=0; i<folios.size(); i++) {
                    folios.get(i).setFolioRefoleo(folio + abcedario.charAt(i));
                }
                e.onSuccess(folios);
            }else {
                e.tryOnError(new NetworkException("No hay internet"));
            }
        });
    }

    @Override
    public Completable sincronizar(String ajuste) {
        return Completable.create(e -> {
            if(ConnectionProvider.getInstance().isConnected()) {
                e.onComplete();
            } else {
                e.onError(new NetworkException("No hay internet"));
            }
        });
    }

    @Override
    public Single<List<AjusteAmis>> getContrapartes(String ajuste) {
        return Single.create(e -> {
            if(ConnectionProvider.getInstance().isConnected()) {
                Amis amis = amisRepository.findByAjuste(ajuste);
                List<AjusteAmis> result = new ArrayList<>(0);
                for(FolioContraparteAmis folio : amis.getContrapartes()) {
                    result.add(getAjuste(ajuste, folio.getFolioInicial()));
                }
                e.onSuccess(result);
            } else {
                e.onError(new NetworkException("No hay internet"));
            }
        });
    }

    @Override
    public Single<ValeDigital> recibirOrden(String ajuste, String folioOrden) {
        throw new RuntimeException("Método no implementado");
    }

    @Override
    public Completable emitirOrden(String ajuste, EmisionValeDigital emision) {
        throw new RuntimeException("Método no implementado");
    }

    @Override
    public Single<List<CertificadoCompania>> getCertificados() {
        return Single.create(e -> {
            if(ConnectionProvider.getInstance().isConnected()) {
                e.onSuccess(certificados);
            } else {
                e.onError(new NetworkException("No hay internet"));
            }
        });
    }

    @Override
    public Single<CertificadoCompania> getCertificado(Long compania) {
        return Single.create(e -> {
            if(ConnectionProvider.getInstance().isConnected()) {
                CertificadoCompania certificado = null;
                for(CertificadoCompania cer : certificados) {
                    if(cer.getCompania().equals(compania)) {
                        certificado = cer;
                    }
                }
                if(certificado == null) {
                    e.onError(new ResourceNotFoundException());
                }
                else {
                    e.onSuccess(certificado);
                }
            } else {
                e.onError(new NetworkException("No hay internet"));
            }
        });
    }

    @Override
    public Single<Certificado> getCertificadoPropio(String serial) {
        return Single.create(e -> {
            if(ConnectionProvider.getInstance().isConnected()) {
                Certificado certificado = new Certificado();
                certificado.setNumeroSerial("268640d4d7686dc9b7aa8a8583e27f2b91b0e97e");
                certificado.setCertificado("LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlFZlRDQ0EyV2dBd0lCQWdJVUpvWkExTmRvYmNtM3FvcUZnK0ovSzVHdzZYNHdEUVlKS29aSWh2Y05BUUVMDQpCUUF3Z2MweEN6QUpCZ05WQkFZVEFrMVlNUmt3RndZRFZRUUlEQkJEU1ZWRVFVUWdSRVVnVFVWWVNVTlBNUmN3DQpGUVlEVlFRSERBNUJURlpCVWs4Z1QwSlNSVWRQVGpFcE1DY0dBMVVFQ2d3Z1IxSlZVRThnVGtGRFNVOU9RVXdnDQpVRkpQVmtsT1EwbEJUQ0JUTGtFdVFpNHhGVEFUQmdOVkJBc01ERWRPVURreU1URXlORFJRTURFak1DRUdBMVVFDQpBd3dhUVVSTlNVNUpVMVJTUVVSUFVpQkRUMDFRUVU1SlFTQkhUbEF4SXpBaEJna3Foa2lHOXcwQkNRRVdGR016DQpNUzVoWkcxcGJrQmlhV0ZoYm1rdVkyOXRNQjRYRFRFNU1EWXlNekl4TkRnMU0xb1hEVEk1TURZeU1ESXhORGcxDQpNMW93Z2MweEN6QUpCZ05WQkFZVEFrMVlNUmt3RndZRFZRUUlEQkJEU1ZWRVFVUWdSRVVnVFVWWVNVTlBNUmN3DQpGUVlEVlFRSERBNUJURlpCVWs4Z1QwSlNSVWRQVGpFcE1DY0dBMVVFQ2d3Z1IxSlZVRThnVGtGRFNVOU9RVXdnDQpVRkpQVmtsT1EwbEJUQ0JUTGtFdVFpNHhGVEFUQmdOVkJBc01ERWRPVURreU1URXlORFJRTURFak1DRUdBMVVFDQpBd3dhUVVSTlNVNUpVMVJTUVVSUFVpQkRUMDFRUVU1SlFTQkhUbEF4SXpBaEJna3Foa2lHOXcwQkNRRVdGR016DQpNUzVoWkcxcGJrQmlhV0ZoYm1rdVkyOXRNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDDQpBUUVBeWdGUmUxWjV3YndEOTNVQXVxQTlzVkwwdWZLeEdseE4vN0h1bzNjRHM5NTV1WlhObndDQmdWbFBFRUtDDQoweUU0dEZKdW05NXAveGxEc1Qvc3J3MmJ6RnN0bU1jMFlMNm1DR3RoU28rRjdHWUVuaVhnblFMRXg5K2Y0MGlqDQpBOFJjVUxVV0FUMldqbzdVVzVsRlQ2Z25jcFZqQVhZSytld3NNNml6Q2d2WC9aRDJWQWFaUVVqOStDTEZ5eUx6DQpjekNvc3Q4ZFNSc0J5RmM5Q0pvcUZtdTF1S0ltOFBieTZyaTlPajhSUjh3MGxjb1NlUHgraS9hQVArRVB4aDRiDQpxallZN1lHUHRZeXlYN0p3YlB0TmpYWEZxMHJGWVMxSkx1ek5ORHVLM2wrYlRXTEt4ODlXOXlXdndvUThubUc1DQo5ZFdSRkl6RHB2NlVySmRjeWVsVlZ3Ympxd0lEQVFBQm8xTXdVVEFkQmdOVkhRNEVGZ1FVNkRRQi9xMVJFTlFvDQpGbWplMEZHMTdJZy9FSEl3SHdZRFZSMGpCQmd3Rm9BVTZEUUIvcTFSRU5Rb0ZtamUwRkcxN0lnL0VISXdEd1lEDQpWUjBUQVFIL0JBVXdBd0VCL3pBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQWZXdS9yTlRvRnJXbkpqY0xlNno2DQozRE03a2xDUDBNUWVsMFlTb1ZRSnM5SUZLbkJldm9IcWZUaG9ESVNjM3hsaG5PdXBnU1ZCUTJGRXdUWVdobUc4DQpXZ2x4NFVGMURyY09TSVJZcTNqU0xjQUx0NTY4bzNjbHIvUVdIWnZCMG94eFh0QUVzOTlMQmtmdDZ1ajRCREtuDQpkbkVsS090NTlGMWFrVmUrRXVJbHl2WUExV0dnUzk2RGVoZ1J5d29HSjRGdVF1TElpMFppM2VXVTY3bEJvSTBpDQpyU21uVjhGd2FXdHNsazdUcjVmbzE1RGVTdmFYNmFyTXh4L1BsRk5WMGcrd2ZyNUhGdmNDcUErWDJXWUVUU2xwDQpvR0dnb3JkUzhXOWxCWGJZcnVhbWVDdTlWQmVFdklLYmNxMGtJT29XVWdJbFgyZ2VKUjFZcTVDVjRhbG9CNlA4DQprUT09DQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tDQo=");
                certificado.setLlavePublica("LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBeWdGUmUxWjV3YndEOTNVQXVxQTkNCnNWTDB1Zkt4R2x4Ti83SHVvM2NEczk1NXVaWE5ud0NCZ1ZsUEVFS0MweUU0dEZKdW05NXAveGxEc1Qvc3J3MmINCnpGc3RtTWMwWUw2bUNHdGhTbytGN0dZRW5pWGduUUxFeDkrZjQwaWpBOFJjVUxVV0FUMldqbzdVVzVsRlQ2Z24NCmNwVmpBWFlLK2V3c002aXpDZ3ZYL1pEMlZBYVpRVWo5K0NMRnl5THpjekNvc3Q4ZFNSc0J5RmM5Q0pvcUZtdTENCnVLSW04UGJ5NnJpOU9qOFJSOHcwbGNvU2VQeCtpL2FBUCtFUHhoNGJxallZN1lHUHRZeXlYN0p3YlB0TmpYWEYNCnEwckZZUzFKTHV6Tk5EdUszbCtiVFdMS3g4OVc5eVd2d29ROG5tRzU5ZFdSRkl6RHB2NlVySmRjeWVsVlZ3YmoNCnF3SURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
                certificado.setLlavePrivada("LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tDQpNSUlFdlFJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLY3dnZ1NqQWdFQUFvSUJBUURLQVZGN1ZubkJ2QVAzDQpkUUM2b0QyeFV2UzU4ckVhWEUzL3NlNmpkd096M25tNWxjMmZBSUdCV1U4UVFvTFRJVGkwVW02YjNtbi9HVU94DQpQK3l2RFp2TVd5Mll4elJndnFZSWEyRktqNFhzWmdTZUplQ2RBc1RIMzUvalNLTUR4RnhRdFJZQlBaYU9qdFJiDQptVVZQcUNkeWxXTUJkZ3I1N0N3enFMTUtDOWY5a1BaVUJwbEJTUDM0SXNYTEl2TnpNS2l5M3gxSkd3SElWejBJDQptaW9XYTdXNG9pYnc5dkxxdUwwNlB4Rkh6RFNWeWhKNC9INkw5b0EvNFEvR0hodXFOaGp0Z1krMWpMSmZzbkJzDQorMDJOZGNXclNzVmhMVWt1N00wME80cmVYNXROWXNySHoxYjNKYS9DaER5ZVlibjExWkVVak1PbS9wU3NsMXpKDQo2VlZYQnVPckFnTUJBQUVDZ2dFQkFJVW4yVTR6OG82Q0lSczlSUDNBSXVxQWhGRU9hUyszS0xNajF1cHB5VWNIDQpuN1YxeWRURndKaERCcHJ4UE10dldyR0NwT2FaOWNUeUltejBkay90NklVTWxVSGdEZWEzeFRwSGpocFlBUytUDQpEL2d2ZjUvLzh6Yk5jZGVnU2RSeDIyaFg5ZWxiYjZNSkEzSmQxaEpzQlpUQ1pFRFNUdW52aERPSlFIdWUwd0dSDQpIN3RLU3dVZ0pUVzFnMnRiY2FmWm5uSGdtSmVFYVVvUVEwdnVKKzVRcTUvNkRZOFVabWZmRUJ5UkJYYVg0aFVVDQpLazY3K1hSSnljQll2R244QUZacUgvWGZFbUFVaDRDZ2kybkExaFpiNkNxc0d3QnFTSC92dW5PNjBHYk5GdzVXDQpPVFFkUzZVRlpSTjQ5MjFiUTcydFpkVGhmMGdZUWU3aDJEOFM5V21BTGdFQ2dZRUErVnMwcVRucDNlRXNicjBJDQorU3duQ3dGSHk3S245aG1VQXh6dUJtQ1N4bnYxaDdsNi9Ya2ZzeVpKOSt3bUpjODdnZjNXNTBiMFlHcHRPRWlFDQpRZUVLaDRHaVRyWVRmbmRnY2RWeGNJR3F6bzBWQUY1QTZhTFpjeTd0MlJCbW91VVBQa0xvNEZWSnlkZEIrQnh2DQozT2JjTlpDb2xxcWpiN0dvYkFKZUI4elZGMzBDZ1lFQXoyTWtsQ2djZ1F6eGMwNERiZTZhZEtwa0RvRnJFNCt6DQptUEtWMWNzaE9DVlE4MXV4SGd2c2hoOGppOWxqaW9ibUxVbWR4SDZpOFIvMlUzNEdjb1NtNG1Tc0FVbVQ1VWFFDQp2bStreXJSRVZrWHBnVDFNUmVMNDFqVW5YV2pxTVNKR091ckpwQ0V3YWNEN3FTbllpcHlHbkZyalREZkNuM0JLDQpyZFd6TVg3ZzRFY0NnWUIvY1R6bDdLSjVjemM1VmNYN1M3U2NyRWZjZE8vaEdNdU1GOElUWWxKQnZXTU12aUI4DQo0ZVh3c3ZZRkR4MmtIMWtqbysrcy9wZXhtMU04KzlzVXJJYXNJUFlxVW1aK01WY0tkdk5tYys4dlorMis1VzA1DQp1N0IxYlJ2RTM5d0ZEeHNPVURZalNaUFA1QU5rWW14TnMzUUNzRzlwcWNha05HR0c2bUxTaTc2eEpRS0JnREF5DQppMnEvVTdpT1NJTVNxMnRlRzVsbmpDOXUxbkxLWCtQNDVPZTErTGR5dkxpQ0k4eW0yc1prOEQ3dE85d1M4K1dzDQppVFVFZFZ2eW9LM1F5VERPTVhiR0RNK1R5UUpzL0NiQ0RadHFmOE5KSmpaNkI0THFlQWhZZkJpU2JITVUweHVZDQpPMXFndC82cjhoRGpmOGJmRDgzQXRiaWQ4T1piRTVBSTdVZGdoR1M3QW9HQVllZWF5K2ZuaUl2QkJwUllPajlVDQpLOWlXekxzUGw0QmowY2tVVDFEMjdRZUVySEZ0UDRKSnp3eitBMHhYNTI5ZGxBOVdQQlBKdGN1WkZxbXVQbUFYDQpVNFhKNUxpN0xVS2N5SVphcGZhWGJhWERKZ3dXaFJLMkdPaTNJYXBTZWNnNE9md1JZNmR1TGlLQjY0ZXAvK1FyDQpGRzJVd2ttRUJBcVp2eG0va2xqSldsND0NCi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0NCg==");
                e.onSuccess(certificado);
            } else {
                e.onError(new NetworkException("No hay internet"));
            }
        });

    }

    private AjusteAmis getAjuste(String ajuste, String folio) throws Exception {
        AjusteAmis result = new AjusteAmis();
        result.setFolioAtencion(folio);
        result.setFolioAtencionInicial(folio);
        List<Modulo> modulos = RXUtils.sync(OrquestadorInteractorFactory.getAmisInteractor().getModulos(ajuste));
        for(Modulo modulo : modulos) {
            modulo.setFolio(folio);
            if(modulo instanceof SiniestroAmis) {
                result.setSiniestro((SiniestroAmis) modulo);
            }
            if(modulo instanceof PolizaAmis) {
                result.setPoliza((PolizaAmis) modulo);
            }
            if(modulo instanceof AseguradoAmis) {
                result.setAsegurado((AseguradoAmis) modulo);
            }
            if(modulo instanceof VehiculoAmis) {
                result.setVehiculo((VehiculoAmis) modulo);
            }
            if(modulo instanceof Danios) {
                result.setDanioGeneral((Danios) modulo);
            }
            if(modulo instanceof Circunstancia) {
                result.setCircunstancia((Circunstancia) modulo);
            }
        }
        result.setInvolucrados(getInvolucrados(modulos));
        return result;
    }

    private List<Involucrado> getInvolucrados(List<Modulo> modulos) {
        Set<String> ids = new HashSet<>();
        Collection<Modulo> involucrados = CollectionsUtils.filter(modulos, modulo -> modulo instanceof InvolucradoModulo);
        for(Modulo involucrado : involucrados) {
            ids.add(((InvolucradoModulo)involucrado).getIdInvolucrado());
        }
        List<Involucrado> result = new ArrayList<>(ids.size());
        for(String id : ids) {
            Collection<Modulo> data = CollectionsUtils.filter(modulos, modulo -> id.equals(((InvolucradoModulo)modulo).getIdInvolucrado()));
            Involucrado involucrado = getInvolucrado(data);
            involucrado.getDatosGrales().setIdInvolucrado(involucrado.getDatosGrales().getIdInvolucrado() + "-" + involucrado.getDatosGrales().getFolio());
            result.add(involucrado);
        }
        return result;
    }

    private Involucrado getInvolucrado(Collection<Modulo> modulos) {
        Involucrado involucrado = new Involucrado();
        involucrado.setDatosGrales(new InvolucradoData());
        for(Modulo modulo : modulos) {
            if (modulo instanceof InvolucradoGeneral) {
                putGeneral(involucrado.getDatosGrales(), (InvolucradoGeneral) modulo);
            }
            if (modulo instanceof InvolucradoDetalles) {
                putDetalles(involucrado.getDatosGrales(), (InvolucradoDetalles) modulo);
            }
            if (modulo instanceof PaseMedico) {
                involucrado.setPaseMedico((PaseMedico) modulo);
            }
            if (modulo instanceof Narrativa) {
                involucrado.setNarrativa((Narrativa) modulo);
            }
        }
        return involucrado;
    }

    private InvolucradoDetalles putDetalles(InvolucradoData involucrado, InvolucradoDetalles detalles) {
        involucrado.setIdInvolucrado(detalles.getIdInvolucrado());
        involucrado.setDireccion(detalles.getDireccion());
        involucrado.setFolio(detalles.getFolio());
        involucrado.setMismoAsegurado(detalles.getMismoAsegurado());
        involucrado.setPersona(detalles.getPersona());
        involucrado.setTipoInvolucrado(detalles.getTipoInvolucrado());
        return detalles;
    }

    private void putGeneral(InvolucradoData involucrado, InvolucradoGeneral general) {
        involucrado.setAvisoPrivacidad(general.getAvisoPrivacidad());
        involucrado.setCorreoElectronico(general.getCorreoElectronico());
        involucrado.setEstadoLicencia(general.getEstadoLicencia());
        involucrado.setFechaVigenciaLicencia(general.getFechaVigenciaLicencia());
        involucrado.setFolio(general.getFolio());
        involucrado.setIdInvolucrado(general.getIdInvolucrado());
        involucrado.setLicencia(general.getLicencia());
        involucrado.setTelefono(general.getTelefono());
        involucrado.setTipoLicencia(general.getTipoLicencia());
        involucrado.setTratamientoDatosPersonales(general.getTratamientoDatosPersonales());
        involucrado.setVersionAvisoPrivacidadCompania(general.getVersionAvisoPrivacidadCompania());
        involucrado.setVersionAvisoPrivacidadGeneral(general.getVersionAvisoPrivacidadGeneral());
        involucrado.setVersionCtdpCompania(general.getVersionCtdpCompania());
        involucrado.setVersionCtdpGeneral(general.getVersionCtdpCompania());
    }

}
