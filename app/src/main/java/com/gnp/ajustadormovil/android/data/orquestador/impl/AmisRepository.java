package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.models.amis.Amis;

public interface AmisRepository {

    Amis findByAjuste(String ajuste);

    void save(Amis amis);

    void deleteByAjuste(String ajsute);

}
