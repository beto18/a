package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.amis.Amis;

public class AmisRepositoryRealmImpl extends RealmRepository<Amis, String> implements AmisRepository{

    public AmisRepositoryRealmImpl() {
        super(Amis.class);
    }

    @Override
    public void save(Amis entity) {
        super.save(entity);
    }

    @Override
    public Amis findByAjuste(String ajuste) {
        return findById(ajuste);
    }

    @Override
    public void deleteByAjuste(String ajsute) {
        deleteById(ajsute);

    }
}
