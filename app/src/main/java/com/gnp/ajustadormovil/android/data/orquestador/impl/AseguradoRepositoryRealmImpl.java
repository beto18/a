package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;

public class AseguradoRepositoryRealmImpl extends RealmRepository<Asegurado, String> implements AseguradoRepository {

    public AseguradoRepositoryRealmImpl(){
        super(Asegurado.class);
    }

    @Override
    public Asegurado findByAjuste(String ajuste) {
        return findById(ajuste);
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        deleteById(ajuste);
    }

}
