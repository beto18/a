package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.CertificadoCompaniaRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;

public class CertificadoCompaniaRepositoryRealmImpl extends RealmRepository<CertificadoCompania, Long> implements CertificadoCompaniaRepository {

    public CertificadoCompaniaRepositoryRealmImpl() {
        super(CertificadoCompania.class);
    }

    @Override
    public CertificadoCompania findByCompania(Long compania) {
        return findById(compania);
    }

    @Override
    public long count() {
        return onRealm(realm -> {
            return realm.where(CertificadoCompania.class)
                    .count();
        });
    }
}
