package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.CertificadoRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.amis.Certificado;

public class CertificadoRepositoryRealmImpl extends RealmRepository<Certificado, String> implements CertificadoRepository {

    public CertificadoRepositoryRealmImpl() {
        super(Certificado.class);
    }

    @Override
    public Certificado findByNumeroSerial(String numeroSerial) {
        return findById(numeroSerial);
    }
}
