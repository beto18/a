package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.CroquisRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.ajuste.Croquis;

public class CroquisRepositoryRealmImpl extends RealmRepository<Croquis, String> implements CroquisRepository {

    public CroquisRepositoryRealmImpl(){
        super(Croquis.class);
    }
    @Override
    public void save(Croquis croquis) {
        save(croquis);
    }

    @Override
    public Croquis findByAjuste(String ajuste) {
        return findById(ajuste);
    }

    @Override
    public void delete(String ajuste) {
        deleteById(ajuste);
    }
}
