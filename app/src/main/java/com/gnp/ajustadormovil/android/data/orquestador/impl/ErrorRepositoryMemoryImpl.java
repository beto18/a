package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ErrorRepositoryMemoryImpl implements ErrorRepository {

    private static final Map<String, Throwable> ERRORS = new ConcurrentHashMap<>();

    private static final Throwable EMPTY = new Exception();

    @Override
    public void saveByAjuste(String ajuste, Throwable throwable) {
        ERRORS.put(ajuste, throwable);
    }

    @Override
    public Throwable findByAjuste(String ajuste) {
        Throwable e = ERRORS.get(ajuste);
        return e == null || e == EMPTY ? null : e;
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        ERRORS.put(ajuste, EMPTY);
    }
}
