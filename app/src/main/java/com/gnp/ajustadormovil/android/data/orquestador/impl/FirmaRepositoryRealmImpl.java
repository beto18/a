package com.gnp.ajustadormovil.android.data.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.orquestador.FirmaRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.ajuste.Firma;

import io.realm.RealmResults;

public class FirmaRepositoryRealmImpl extends RealmRepository<Firma, String> implements FirmaRepository {

    private String TAG = this.getClass().getSimpleName();

    public FirmaRepositoryRealmImpl() {
        super(Firma.class);
    }

    private synchronized Long generatedId(){
        try {
            Thread.sleep(1);
        } catch (InterruptedException ex){
            Log.e(TAG,"Se interrumpio el delay", ex);
        }
        return System.currentTimeMillis();
    }

    @Override
    public void saveAseguradoConductor(String ajuste, String data) {
        onRealm(realm -> {
            Firma firma = new Firma();
            firma.setAjuste(ajuste);
            firma.setData(data);
            firma.setOcupante(0L);
            firma.setTercero(0L);
            firma.setId(generatedId());
            realm.copyToRealmOrUpdate(firma);
        });
    }

    @Override
    public String getAseguradoConductor(String ajuste) {
        return onRealm(realm -> {
            Firma firma = realm
                    .where(Firma.class)
                    .equalTo("ajuste", ajuste)
                    .equalTo("ocupante", 0)
                    .equalTo("tercero", 0)
                    .findFirst();
            return firma == null ? null : firma.getData();
        }, true);
    }

    @Override
    public void saveAseguradoOcupante(String ajuste, Long idOcupante, String data) {
        onRealm(realm -> {
            Firma firma = new Firma();
            firma.setAjuste(ajuste);
            firma.setData(data);
            firma.setOcupante(idOcupante);
            firma.setTercero(0L);
            firma.setId(generatedId());
            realm.copyToRealmOrUpdate(firma);
        });
    }

    @Override
    public String getAseguradoOcupante(String ajuste, Long idOcupante) {
        return onRealm(realm -> {
            Firma firma = realm
                    .where(Firma.class)
                    .equalTo("ajuste", ajuste)
                    .equalTo("ocupante", idOcupante)
                    .equalTo("tercero", 0)
                    .findFirst();
            return firma == null ? null : firma.getData();
        }, true);
    }

    @Override
    public void saveTerceroAutoConductor(String ajuste, Long idTercero, String data) {
        onRealm(realm -> {
            Firma firma = new Firma();
            firma.setAjuste(ajuste);
            firma.setData(data);
            firma.setOcupante(0L);
            firma.setTercero(idTercero);
            firma.setId(generatedId());
            realm.copyToRealmOrUpdate(firma);
        });

    }

    @Override
    public String getTerceroConductorAuto(String ajuste, Long idTercero) {
        return onRealm(realm -> {
            Firma firma = realm
                    .where(Firma.class)
                    .equalTo("ajuste", ajuste)
                    .equalTo("ocupante", 0)
                    .equalTo("tercero", idTercero)
                    .findFirst();
            return firma == null ? null : firma.getData();
        }, true);
    }

    @Override
    public void saveTerceroAutoOcupante(String ajuste, Long idTercero, Long idOcupante, String data) {
        onRealm(realm -> {
            Firma firma = new Firma();
            firma.setAjuste(ajuste);
            firma.setData(data);
            firma.setOcupante(idOcupante);
            firma.setTercero(idTercero);
            firma.setId(generatedId());
            realm.copyToRealmOrUpdate(firma);
        });

    }

    @Override
    public String getTerceroAutoOcupante(String ajuste, Long idTercero, Long idOcupante) {
        return onRealm(realm -> {
            Firma firma = realm
                    .where(Firma.class)
                    .equalTo("ajuste", ajuste)
                    .equalTo("ocupante", idOcupante)
                    .equalTo("tercero", idTercero)
                    .findFirst();
            return firma == null ? null : firma.getData();
        }, true);
    }

    @Override
    public void saveTerceroPersona(String ajuste, Long idTercero, String data) {
        onRealm(realm -> {
            Firma firma = new Firma();
            firma.setAjuste(ajuste);
            firma.setData(data);
            firma.setOcupante(0L);
            firma.setTercero(idTercero);
            firma.setId(generatedId());
            realm.copyToRealmOrUpdate(firma);
        });
    }

    @Override
    public String getTerceroPersona(String ajuste, Long idTercero) {
        return onRealm(realm -> {
            Firma firma = realm
                    .where(Firma.class)
                    .equalTo("ajuste", ajuste)
                    .equalTo("ocupante", 0)
                    .equalTo("tercero", idTercero)
                    .findFirst();
            return firma == null ? null : firma.getData();
        }, true);
    }

    @Override
    public void saveTerceroNoAuto(String ajuste, Long idTercero, String data) {
        onRealm(realm -> {
            Firma firma = new Firma();
            firma.setAjuste(ajuste);
            firma.setData(data);
            firma.setOcupante(0L);
            firma.setTercero(idTercero);
            firma.setId(generatedId());
            realm.copyToRealmOrUpdate(firma);
        });

    }

    @Override
    public String getTerceroNoAuto(String ajuste, Long idTercero) {
        return onRealm(realm -> {
            Firma firma = realm
                    .where(Firma.class)
                    .equalTo("ajuste", ajuste)
                    .equalTo("ocupante", 0)
                    .equalTo("tercero", idTercero)
                    .findFirst();
            return firma == null ? null : firma.getData();
        }, true);
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        onRealm(realm -> {
            RealmResults<Firma> rows = realm
                   .where(Firma.class)
                   .equalTo("ajuste", ajuste)
                   .findAll();
            rows.deleteAllFromRealm();
        });
    }
}
