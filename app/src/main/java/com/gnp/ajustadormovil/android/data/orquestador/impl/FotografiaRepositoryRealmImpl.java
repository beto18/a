package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;

import java.io.File;

public class FotografiaRepositoryRealmImpl extends RealmRepository<Fotografia,Long> implements FotografiaRepository {

    public FotografiaRepositoryRealmImpl() {
        super(Fotografia.class);
    }

    @Override
    public void delete(Fotografia entity) {
        if (entity.getLocalUrl() != null && !entity.getLocalUrl().startsWith("http")){
            File file = new File(entity.getLocalUrl());
            if (file.exists()){
                file.delete();
            }
        }
        super.delete(entity);
    }
}
