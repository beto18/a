package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTercero;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperaciones;

import java.util.ArrayList;
import java.util.List;

public class RecuperacionRepositoryRealmImpl implements RecuperacionRepository {

    private final Repository<Recuperaciones, String> repository = RepositoryFactory.getRepository(Recuperaciones.class, String.class);

    private final Repository<RecuperacionTercero, Long> terceroRepository = RepositoryFactory.getRepository(RecuperacionTercero.class, Long.class);

    private final Repository<RecuperacionMonetaria, Long> monetariaRepository = RepositoryFactory.getRepository(RecuperacionMonetaria.class, Long.class);

    private final Repository<RecuperacionMesaControversia, Long> mesaRespository = RepositoryFactory.getRepository(RecuperacionMesaControversia.class, Long.class);

    private final Repository<RecuperacionGarantiaPrendaria, Long> garantiaRepository = RepositoryFactory.getRepository(RecuperacionGarantiaPrendaria.class, Long.class);

    private final Repository<RecuperacionCompaniaSeguros, Long> companiaRepository = RepositoryFactory.getRepository(RecuperacionCompaniaSeguros.class, Long.class);

    private final Repository<RecuperacionTurnadoAsistenciaLegal, Long> turnadoRepository = RepositoryFactory.getRepository(RecuperacionTurnadoAsistenciaLegal.class, Long.class);

    @Override
    public List<Recuperacion> findByAjusteAndTercero(String ajuste, Long tercero) {
        Recuperaciones recuperaciones = findRecuperacionesByAjuste(ajuste);
        List<Recuperacion> result = new ArrayList<>();
        for (RecuperacionTercero ter : recuperaciones.getTerceros()) {
            if (ter.getTercero().equals(tercero)) {
                result.addAll(ter.getCompaniasSeguros());
                result.addAll(ter.getGarantiasPrendarias());
                result.addAll(ter.getMesasControversias());
                result.addAll(ter.getMonetarias());
                result.addAll(ter.getTurnadoAsistenciaLegales());
            }
        }
        return result;
    }

    @Override
    public Recuperacion findByAjusteAndTerceroAndId(String ajuste, Long tercero, Long id) {
        return getById(findByAjusteAndTercero(ajuste, tercero), id);
    }

    @Override
    public void deleteByAjusteAndTerceroAndId(String ajuste, Long tercero, Long id) {
        Recuperaciones recuperaciones = findRecuperacionesByAjuste(ajuste);
        for (RecuperacionTercero ter : recuperaciones.getTerceros()) {
            if(ter.getTercero().equals(tercero)){
                removeById(ter, id);
            }
        }
        repository.save(recuperaciones);
    }

    @Override
    public void deleteByAjusteAndTercero(String ajuste, Long tercero) {
        Recuperaciones recuperaciones = findRecuperacionesByAjuste(ajuste);
        RecuperacionTercero deleted = null;
        for (RecuperacionTercero ter : recuperaciones.getTerceros()) {
            if (ter.getTercero().equals(tercero)) {
                deleted = ter;
            }
        }
        if(deleted != null){
            recuperaciones.getTerceros().remove(deleted);
            deleteFromRealm(deleted);
        }
        repository.save(recuperaciones);
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        Recuperaciones recuperaciones = findRecuperacionesByAjuste(ajuste);
        for (RecuperacionTercero ter : recuperaciones.getTerceros()) {
            deleteFromRealm(ter);
        }
        repository.deleteById(ajuste);
    }

    @Override
    public void saveByAjusteAndTercero(String ajuste, Long tercero, Recuperacion recuperacion) {
        if(recuperacion.getId() != null){
            deleteByAjusteAndTerceroAndId(ajuste, tercero, recuperacion.getId());
        } else {
            recuperacion.setId(System.currentTimeMillis());
        }
        Recuperaciones recuperaciones = findRecuperacionesByAjuste(ajuste);
        RecuperacionTercero saved = null;
        for (RecuperacionTercero ter : recuperaciones.getTerceros()) {
            if (ter.getTercero().equals(tercero)) {
                saved = ter;
            }
        }
        if(saved == null){
            saved = new RecuperacionTercero();
            saved.setTercero(tercero);
            recuperaciones.getTerceros().add(saved);
        }
        add(saved, recuperacion);
        repository.save(recuperaciones);
    }

    private Recuperaciones findRecuperacionesByAjuste(String ajuste){
        Recuperaciones recuperaciones = repository.findById(ajuste);
        if(recuperaciones == null){
            recuperaciones = new Recuperaciones();
            recuperaciones.setAjuste(ajuste);
            repository.save(recuperaciones);
        }
        return recuperaciones;
    }

    private Recuperacion getById(List<? extends Recuperacion> recuperaciones, Long id){
        for(Recuperacion rec : recuperaciones){
            if(rec.getId().equals(id)){
                return rec;
            }
        }
        return null;
    }

    private void add(RecuperacionTercero tercero, Recuperacion recuperacion){
        if(recuperacion instanceof RecuperacionCompaniaSeguros){
            tercero.getCompaniasSeguros().add((RecuperacionCompaniaSeguros)recuperacion);
        }
        if(recuperacion instanceof RecuperacionGarantiaPrendaria){
            tercero.getGarantiasPrendarias().add((RecuperacionGarantiaPrendaria)recuperacion);
        }
        if(recuperacion instanceof RecuperacionTurnadoAsistenciaLegal){
            tercero.getTurnadoAsistenciaLegales().add((RecuperacionTurnadoAsistenciaLegal)recuperacion);
        }
        if(recuperacion instanceof RecuperacionMesaControversia){
            tercero.getMesasControversias().add((RecuperacionMesaControversia)recuperacion);
        }
        if(recuperacion instanceof RecuperacionMonetaria){
            tercero.getMonetarias().add((RecuperacionMonetaria)recuperacion);
        }
    }

    private void removeById(RecuperacionTercero tercero, Long id){
        List<Recuperacion> recuperaciones = new ArrayList<>();
        recuperaciones.addAll(tercero.getCompaniasSeguros());
        recuperaciones.addAll(tercero.getGarantiasPrendarias());
        recuperaciones.addAll(tercero.getMesasControversias());
        recuperaciones.addAll(tercero.getMonetarias());
        recuperaciones.addAll(tercero.getTurnadoAsistenciaLegales());
        Recuperacion deleted = getById(recuperaciones, id);
        if(deleted != null) {
            if (deleted instanceof RecuperacionCompaniaSeguros) {
                tercero.getCompaniasSeguros().remove(deleted);
                companiaRepository.delete((RecuperacionCompaniaSeguros) deleted);
            }
            if (deleted instanceof RecuperacionGarantiaPrendaria) {
                tercero.getGarantiasPrendarias().remove(deleted);
                garantiaRepository.delete((RecuperacionGarantiaPrendaria)deleted);
            }
            if (deleted instanceof RecuperacionTurnadoAsistenciaLegal) {
                tercero.getTurnadoAsistenciaLegales().remove(deleted);
                turnadoRepository.delete((RecuperacionTurnadoAsistenciaLegal)deleted);
            }
            if (deleted instanceof RecuperacionMesaControversia) {
                tercero.getMesasControversias().remove(deleted);
                mesaRespository.delete((RecuperacionMesaControversia)deleted);
            }
            if (deleted instanceof RecuperacionMonetaria) {
                tercero.getMonetarias().remove(deleted);
                monetariaRepository.delete((RecuperacionMonetaria)deleted);
            }
            if(!hasRecuperacion(tercero)){
                terceroRepository.delete(tercero);
            }
        }
    }

    private void deleteFromRealm(RecuperacionTercero tercero){
        for(RecuperacionCompaniaSeguros recuperacion : tercero.getCompaniasSeguros()){
            companiaRepository.delete(recuperacion);
        }
        for(RecuperacionMonetaria recuperacion : tercero.getMonetarias()){
            monetariaRepository.delete(recuperacion);
        }
        for(RecuperacionGarantiaPrendaria recuperacion : tercero.getGarantiasPrendarias()){
            garantiaRepository.delete(recuperacion);
        }
        for(RecuperacionTurnadoAsistenciaLegal recuperacion : tercero.getTurnadoAsistenciaLegales()){
            turnadoRepository.delete(recuperacion);
        }
        for(RecuperacionMesaControversia recuperacion : tercero.getMesasControversias()){
            mesaRespository.delete(recuperacion);
        }
        terceroRepository.delete(tercero);
    }

    private boolean hasRecuperacion(RecuperacionTercero tercero){
        return tercero.getMesasControversias().isEmpty() &&
                tercero.getMonetarias().isEmpty() &&
                tercero.getTurnadoAsistenciaLegales().isEmpty() &&
                tercero.getGarantiasPrendarias().isEmpty() &&
                tercero.getCompaniasSeguros().isEmpty();
    }

}
