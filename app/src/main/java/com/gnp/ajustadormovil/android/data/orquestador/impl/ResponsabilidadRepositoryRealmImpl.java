package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;

public class ResponsabilidadRepositoryRealmImpl extends RealmRepository<Responsabilidad, String> implements ResponsabilidadRepository {

    public ResponsabilidadRepositoryRealmImpl(){
        super(Responsabilidad.class);
    }


    @Override
    public Responsabilidad findByAjuste(String ajuste) {
        return findById(ajuste);
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        deleteById(ajuste);
    }
}
