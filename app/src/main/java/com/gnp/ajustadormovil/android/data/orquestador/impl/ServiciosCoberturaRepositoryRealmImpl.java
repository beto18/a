package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.ServicioCoberturaRepository;
import com.gnp.ajustadormovil.android.data.realm.RealmRepository;
import com.gnp.ajustadormovil.android.models.orquestador.servicio.ServiciosCobertura;

public class ServiciosCoberturaRepositoryRealmImpl extends RealmRepository<ServiciosCobertura, String> implements ServicioCoberturaRepository {

    public ServiciosCoberturaRepositoryRealmImpl() {
        super(ServiciosCobertura.class);
    }

    @Override
    public ServiciosCobertura findByAjuste(String ajuste) {
        return findById(ajuste);
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        deleteById(ajuste);
    }
}
