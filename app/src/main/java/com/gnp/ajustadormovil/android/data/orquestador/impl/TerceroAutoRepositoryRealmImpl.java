package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Terceros;

import java.util.ArrayList;
import java.util.List;

public class TerceroAutoRepositoryRealmImpl implements TerceroAutoRepository {

    private final Repository<Terceros, String> repository = RepositoryFactory.getRepository(Terceros.class, String.class);

    private final Repository<TerceroAuto, String> terceroRepository = RepositoryFactory.getRepository(TerceroAuto.class, String.class);

    @Override
    public List<TerceroAuto> findByAjuste(String ajuste) {
        Terceros terceros = repository.findById(ajuste);
        return terceros == null ? new ArrayList<>(0) : terceros.getAutos();
    }

    @Override
    public TerceroAuto findByAjusteAndId(String ajuste, Long id) {
        List<TerceroAuto> terceros = findByAjuste(ajuste);
        for(TerceroAuto tercero : terceros){
            if(tercero.getId().equals(id)){
                return tercero;
            }
        }
        return null;
    }

    @Override
    public TerceroAuto findByAjusteAndFolioAmis(String ajuste, String folioAmis) {
        List<TerceroAuto> terceros = findByAjuste(ajuste);
        for(TerceroAuto tercero : terceros){
            if(folioAmis.equals(tercero.getFolioAmis())){
                return tercero;
            }
        }
        return null;
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        List<TerceroAuto> terceros = findByAjuste(ajuste);
        for(TerceroAuto tercero : terceros){
            terceroRepository.delete(tercero);
        }
    }

    @Override
    public void deleteByAjusteAndId(String ajuste, Long id) {
        Terceros terceros = repository.findById(ajuste);
        if(terceros != null){
            TerceroAuto deleted = null;
            for(TerceroAuto tercero : terceros.getAutos()){
                if(tercero.getId().equals(id)){
                    deleted = tercero;
                }
            }
            if(deleted != null) {
                terceros.getAutos().remove(deleted);
                terceroRepository.delete(deleted);
            }
            repository.save(terceros);
        }
    }

    @Override
    public void saveByAjuste(String ajuste, TerceroAuto tercero) {
        if(tercero.getId() != null){
            deleteByAjusteAndId(ajuste, tercero.getId());
        } else {
            tercero.setId(System.currentTimeMillis());
        }
        Terceros terceros = repository.findById(ajuste);
        if(terceros == null){
            terceros = new Terceros();
            terceros.setAjuste(ajuste);
        }
        terceros.getAutos().add(tercero);
        repository.save(terceros);
    }

}
