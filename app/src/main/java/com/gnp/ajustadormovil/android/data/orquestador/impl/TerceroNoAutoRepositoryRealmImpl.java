package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroNoAutoRepository;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Terceros;

import java.util.ArrayList;
import java.util.List;

public class TerceroNoAutoRepositoryRealmImpl implements TerceroNoAutoRepository {

    private final Repository<Terceros, String> repository = RepositoryFactory.getRepository(Terceros.class, String.class);

    private final Repository<TerceroNoAuto, Long> terceroRepository = RepositoryFactory.getRepository(TerceroNoAuto.class, Long.class);

    @Override
    public List<TerceroNoAuto> findByAjuste(String ajuste) {
        Terceros terceros = repository.findById(ajuste);
        return terceros == null ? new ArrayList<>(0) : terceros.getNoAutos();
    }

    @Override
    public TerceroNoAuto findByAjusteAndId(String ajuste, Long id) {
        List<TerceroNoAuto> terceros = findByAjuste(ajuste);
        for(TerceroNoAuto tercero : terceros){
            if(tercero.getId().equals(id)){
                return tercero;
            }
        }
        return null;
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        List<TerceroNoAuto> terceros = findByAjuste(ajuste);
        for(TerceroNoAuto tercero : terceros){
            deleteFromRealm(tercero);
        }
    }

    @Override
    public void deleteByAjusteAndId(String ajuste, Long id) {
        Terceros terceros = repository.findById(ajuste);
        if(terceros != null){
            TerceroNoAuto deleted = null;
            for(TerceroNoAuto tercero : terceros.getNoAutos()){
                if(tercero.getId().equals(id)){
                    deleted = tercero;
                }
            }
            if(deleted != null) {
                terceros.getNoAutos().remove(deleted);
                deleteFromRealm(deleted);
            }
            repository.save(terceros);
        }

    }

    @Override
    public void saveByAjuste(String ajuste, TerceroNoAuto tercero) {
        if(tercero.getId() != null){
            deleteByAjusteAndId(ajuste, tercero.getId());
        } else {
            tercero.setId(System.currentTimeMillis());
        }
        Terceros terceros = repository.findById(ajuste);
        if(terceros == null){
            terceros = new Terceros();
            terceros.setAjuste(ajuste);
        }
        terceros.getNoAutos().add(tercero);
        repository.save(terceros);
    }

    private void deleteFromRealm(TerceroNoAuto tercero){
        terceroRepository.delete(tercero);
    }

}
