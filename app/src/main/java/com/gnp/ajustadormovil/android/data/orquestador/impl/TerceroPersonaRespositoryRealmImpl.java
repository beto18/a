package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Terceros;

import java.util.ArrayList;
import java.util.List;

public class TerceroPersonaRespositoryRealmImpl implements TerceroPersonaRepository {

    private final Repository<Terceros, String> repository = RepositoryFactory.getRepository(Terceros.class, String.class);

    private final Repository<TerceroPersona, Long> terceroRepository = RepositoryFactory.getRepository(TerceroPersona.class, Long.class);

    private final Repository<Persona, Long> personaRepository = RepositoryFactory.getRepository(Persona.class, Long.class);

    @Override
    public List<TerceroPersona> findByAjuste(String ajuste) {
        Terceros terceros = repository.findById(ajuste);
        return terceros == null ? new ArrayList<>(0) : terceros.getPersonas();
    }

    @Override
    public TerceroPersona findByAjusteAndId(String ajuste, Long id) {
        List<TerceroPersona> terceros = findByAjuste(ajuste);
        for(TerceroPersona tercero : terceros){
            if(tercero.getId().equals(id)){
                return tercero;
            }
        }
        return null;
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        List<TerceroPersona> terceros = findByAjuste(ajuste);
        for(TerceroPersona tercero : terceros){
            personaRepository.delete(tercero.getPersona());
            terceroRepository.delete(tercero);
        }
    }

    @Override
    public void deleteByAjusteAndId(String ajuste, Long id) {
        Terceros terceros = repository.findById(ajuste);
        if(terceros != null){
            TerceroPersona deleted = null;
            for(TerceroPersona tercero : terceros.getPersonas()){
                if(tercero.getId().equals(id)){
                    deleted = tercero;
                }
            }
            if(deleted != null) {
                terceros.getPersonas().remove(deleted);
                personaRepository.delete(deleted.getPersona());
                terceroRepository.delete(deleted);
            }
            repository.save(terceros);
        }

    }

    @Override
    public void saveByAjuste(String ajuste, TerceroPersona tercero) {
        if(tercero.getId() != null){
            deleteByAjusteAndId(ajuste, tercero.getId());
        } else {
            tercero.setId(System.currentTimeMillis());
        }
        Terceros terceros = repository.findById(ajuste);
        if(terceros == null){
            terceros = new Terceros();
            terceros.setAjuste(ajuste);
        }
        terceros.getPersonas().add(tercero);
        repository.save(terceros);
    }
    
}
