package com.gnp.ajustadormovil.android.data.orquestador.impl;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroRepository;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Terceros;

import java.util.ArrayList;
import java.util.List;

public class TerceroRepositoryRealmImpl implements TerceroRepository {

    private final Repository<Terceros, String> repository = RepositoryFactory.getRepository(Terceros.class, String.class);

    @Override
    public List<Tercero> findByAjuste(String ajuste) {
        List<Tercero> result = new ArrayList<>();
        Terceros terceros = repository.findById(ajuste);
        if(terceros != null){
            result.addAll(terceros.getAutos());
            result.addAll(terceros.getNoAutos());
            result.addAll(terceros.getPersonas());
        }
        return result;
    }

    @Override
    public Tercero findByAjusteAndId(String ajuste, Long id) {
        List<Tercero> terceros = findByAjuste(ajuste);
        for(Tercero tercero : terceros){
            if(tercero.getId().equals(id)){
                return tercero;
            }
        }
        return null;
    }

    @Override
    public void deleteByAjuste(String ajuste) {
        repository.deleteById(ajuste);
    }

}
