package com.gnp.ajustadormovil.android.data.realm;

import java.io.Serializable;
import java.util.concurrent.CountDownLatch;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.internal.schedulers.SingleScheduler;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;

public class ConcurrentRealmRepository<T extends RealmObject, K extends Serializable> extends RealmRepository<T, K> {

    private static final Scheduler SCHEDULER = new SingleScheduler();

    public ConcurrentRealmRepository(Class<T> type) {
        super(type);
    }

    @Override
    protected <T> T onRealm(Function<Realm, T> event, boolean readOnly) {
        RealmExceptionWrapper error = new RealmExceptionWrapper();
        RealmResultWrapper<T> result = new RealmResultWrapper<>();
        CountDownLatch signal = new CountDownLatch(1);
        Disposable disposable = Completable.create(e -> {
            try(Realm realm = Realm.getDefaultInstance()) {
                if(!readOnly) {
                    realm.beginTransaction();
                }
                result.setResult(event.apply(realm));
                if(!readOnly) {
                    realm.commitTransaction();
                }
            }
        }).subscribeOn(SCHEDULER)
        .observeOn(SCHEDULER)
        .subscribe(signal::countDown, throwable -> {
            signal.countDown();
            if(throwable instanceof RealmException){
                error.setException((RealmException) throwable);
            } else {
                error.setException(new RealmException("Error en la operación de Realm", throwable));
            }
        });
        try {
            signal.await();
        } catch (Exception e){
            error.setException(new RealmException("Error al esperar término de tarea", e));
        } finally {
            disposable.dispose();
        }
        if(error.hasError()){
            throw error.getException();
        }
        return result.getResult();
    }

    private static class RealmExceptionWrapper {

        private RealmException exception;

        public RealmException getException() {
            return exception;
        }

        public void setException(RealmException exception) {
            this.exception = exception;
        }

        public boolean hasError(){
            return this.exception != null;
        }
    }

    private static class RealmResultWrapper<T> {

        private T result;

        public T getResult() {
            return result;
        }

        public void setResult(T result) {
            this.result =result;
        }

    }

}
