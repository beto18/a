package com.gnp.ajustadormovil.android.data.realm

import io.reactivex.Completable
import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject


/**
 * Created by Uraga on 16/12/2017.
 *
 */

object RealmManager {

    fun deleteAll() = with(getRealmInstance()) {
        executeTransaction { transaction -> transaction.deleteAll() }
        close()
    }

    inline fun <reified T : RealmObject> deleteObjects(field: String, value: String) {
        val realm = Realm.getDefaultInstance()
        val result = realm.where(T::class.java).equalTo(field, value).findAll()
        result.deleteAllFromRealm()
    }

    inline fun <reified T : RealmObject> deleteObjectByField(field: String, value: String): Boolean {
        val isDelete: Boolean
        val realm = Realm.getDefaultInstance()
        val result = realm.where(T::class.java).equalTo(field, value).findAll()
        realm.beginTransaction()
        isDelete = result.deleteFirstFromRealm()
        realm.commitTransaction()
        realm.close()
        return isDelete
    }

    fun <T : RealmObject> saveOrUpdateObject(realmObject: T) = with(getRealmInstance()) {
        executeTransaction { transaction -> transaction.copyToRealmOrUpdate(realmObject) }
        close()
    }

    fun <T : RealmObject> saveOrUpdateObjectList(list: RealmList<T>) = with(getRealmInstance()) {
        executeTransaction { transaction -> transaction.copyToRealmOrUpdate(list) }
        close()
    }

    fun <T : RealmObject> findAllObjects(realmObject: T): MutableList<T> {
        val realm = Realm.getDefaultInstance()
        val result = realm.copyFromRealm(realm.where(realmObject::class.java).findAll())
        realm.close()
        return result
    }

    inline fun <reified T : RealmObject> findAllObjects(): MutableList<T> {
        val realm = Realm.getDefaultInstance()
        val result = realm.copyFromRealm(realm.where(T::class.java).findAll())
        realm.close()
        return result
    }

    inline fun <reified T : RealmObject> findListByField(field: String, value: Boolean): MutableList<T> {
        val realm = Realm.getDefaultInstance()
        val list = realm.where(T::class.java).equalTo(field, value).findAll()
        val result = realm.copyFromRealm(list)
        realm.close()
        return result
    }

    inline fun <reified T : RealmObject> findListByField(field: String, value: String): MutableList<T> {
        val realm = Realm.getDefaultInstance()
        val list = realm.where(T::class.java).equalTo(field, value).findAll()
        val result = realm.copyFromRealm(list)
        realm.close()
        return result
    }

    inline fun <reified T : RealmObject> findListByTwoField(field1: String, field2: String,
                                                            value1: String, value2: String): MutableList<T> {
        val realm = Realm.getDefaultInstance()
        val list = realm.where(T::class.java).contains(field1, value1).contains(field2, value2).findAll()
        val result = realm.copyFromRealm(list)
        realm.close()
        return result
    }


    inline fun <reified T : RealmObject> findObjectByField(field: String, value: String): T {
        val realm = Realm.getDefaultInstance()
        val proxyObject = realm.where(T::class.java).equalTo(field, value).findFirst()
                ?: realm.createObject(T::class.java)
        val result = realm.copyFromRealm(proxyObject)
        realm.close()
        return result
    }

    inline fun <reified T : RealmObject> findObjectByField(field: String, value: Boolean): T {
        val realm = Realm.getDefaultInstance()
        val proxyObject = realm.where(T::class.java).equalTo(field, value).findFirst()
                ?: realm.createObject(T::class.java)
        val result = realm.copyFromRealm(proxyObject)
        realm.close()
        return result
    }

    inline fun <reified T : RealmObject> findObjectByField(field: String, value: Int): T {
        val realm = Realm.getDefaultInstance()
        val proxyObject = realm.where(T::class.java).equalTo(field, value).findFirst()
                ?: realm.createObject(T::class.java)
        val result = realm.copyFromRealm(proxyObject)
        realm.close()
        return result
    }

    /**
     * Save Object Data Realm Completable
     */
    fun <T : RealmObject> saveOrUpdateObjectCompletable(realmObject: T): Completable {
        return Completable.create { e ->
            try {
                saveOrUpdateObject(realmObject)
                e.onComplete()
            } catch (ex: Exception) {
                e.tryOnError(ex)
            }
        }
    }

    /**
     * Save object data listRealm Completable
     */
    fun <T : RealmObject> saveOrUpdateObjectListCompletable(list: RealmList<T>): Completable {
        return Completable.create { e ->
            try {
                saveOrUpdateObjectList(list)
                e.onComplete()
            } catch (ex: Exception) {
                e.tryOnError(ex)
            }
        }
    }

    /**
     * Save object data listRealm Observable
     */
    fun <T : RealmObject> saveOrUpdateObjectListObservable(list: RealmList<T>): Observable<List<T>> {
        return Observable.create { e ->
            try {
                saveOrUpdateObjectList(list)
                e.onComplete()
            } catch (ex: Exception) {
                e.tryOnError(ex)
            }
        }
    }

    fun <T : RealmObject> getObjectRealm(field: String, value: String, realmObject: Class<T>): Observable<T>? {
        return Observable.create { e ->
            try {
                val objectRealm =  Realm.getDefaultInstance()
                        .where(realmObject)
                        .equalTo(field, value)
                        .findFirst()
                e.onNext(objectRealm!!)
                e.onComplete()


            } catch (ex: Exception) {
                e.tryOnError(ex)
            }
        }

    }


    fun<T:RealmObject> getObjectListRealm( realmObject: Class<T>): Observable<List<T>>? {
        return Observable.create { e->
            try {
                val list = Realm.getDefaultInstance()
                        .where(realmObject)
                        .findAll()
                e.onNext(list!!)
                e.onComplete()

            }catch (ex:Exception){
                e.tryOnError(ex)
            }
        }

    }

    fun dbTransaction(db: Realm, transaction: Realm.Transaction): Completable {
        return Completable.create { emitter ->
            db.executeTransactionAsync(transaction, Realm.Transaction.OnSuccess { emitter.onComplete() }, Realm.Transaction.OnError { emitter.onError(it) })
        }
    }


    fun getRealmInstance() = Realm.getDefaultInstance()
}