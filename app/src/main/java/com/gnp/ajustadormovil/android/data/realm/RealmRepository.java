package com.gnp.ajustadormovil.android.data.realm;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.Repository;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.internal.schedulers.SingleScheduler;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.annotations.PrimaryKey;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

public class RealmRepository<T extends RealmObject, K extends Serializable> implements Repository<T, K> {

    private final String TAG = this.getClass().getSimpleName();

    private final Class<T> type;

    private final String key;

    private static final Scheduler SCHEDULER = new SingleScheduler();

    public RealmRepository(Class<T> type) {
        this.type = type;
        this.key = getPrimaryKey(type);
    }

    protected <T> T onRealm(Function<Realm, T> event, boolean readOnly) {
        try(Realm realm = Realm.getDefaultInstance()) {
            T result;
            if(!readOnly) {
                realm.beginTransaction();
            }
            try {
                result = event.apply(realm);
            } catch (RealmException e) {
                realm.cancelTransaction();
                throw e;
            } catch (Exception e) {
                realm.cancelTransaction();
                throw new RealmException("Error en operación Realm", e);
            }
            if(!readOnly) {
                realm.commitTransaction();
            }
            return result;
        }
    }

    protected final <T> T onRealm(Function<Realm, T> event) {
        return onRealm(event, false);
    }

    protected final void onRealm(Consumer<Realm> event, boolean readOnly) {
        onRealm(realm -> {
            event.accept(realm);
            return null;
        }, readOnly);
    }

    protected final void onRealm(Consumer<Realm> event) {
        onRealm(event, false);
    }

    @Override
    public void save(T entity) {
        onRealm(realm -> {
            log("save");
            realm.copyToRealmOrUpdate(entity);
        });
    }

    @Override
    public void save(List<T> entities) {
        onRealm(realm -> {
            log("save list");
            realm.copyToRealmOrUpdate(entities);
        });
    }

    @Override
    public T findById(K key) {
        return onRealm(realm -> {
            log("findById: " + key);
            RealmQuery<T> query = realm.where(type);
            putPrimaryKey(query, key);
            T result = query.findFirst();
            if (result != null) {
                result = realm.copyFromRealm(result);
            }
            return result;
        }, true);
    }

    @Override
    public void deleteById(K key) {
        onRealm(realm -> {
            log("deleteById: " + key);
            RealmQuery<T> query = realm.where(type);
            putPrimaryKey(query, key);
            T entity = query.findFirst();
            if (entity != null) {
                entity.deleteFromRealm();
            }
        });
    }

    @Override
    public void delete(T entity) {
        onRealm(realm -> {
            log("deleteByAjuste: ");
            if (entity != null) {
                T bd = realm.copyToRealmOrUpdate(entity);
                bd.deleteFromRealm();
            }
        });
    }

    @Override
    public void deleteAll() {
        onRealm(realm -> {
            log("deleteAll");
            realm.delete(type);
        });
    }

    @Override
    public List<T> findAll() {
        return onRealm(realm -> {
            log("findAll");
            return realm.copyFromRealm(realm
                    .where(type)
                    .findAllSorted(key));
        });

    }

    private RealmQuery<T> putPrimaryKey(RealmQuery<T> query, K key) {
        RealmQuery<T> result = query;
        if (key.getClass().equals(Byte.class)) {
            result = query.equalTo(this.key, (Byte) key);
        } else if (key.getClass().equals(Short.class)) {
            result = query.equalTo(this.key, (Short) key);
        } else if (key.getClass().equals(Integer.class)) {
            result = query.equalTo(this.key, (Integer) key);
        } else if (key.getClass().equals(Long.class)) {
            result = query.equalTo(this.key, (Long) key);
        } else if (key.getClass().equals(Float.class)) {
            result = query.equalTo(this.key, (Float) key);
        } else if (key.getClass().equals(Double.class)) {
            result = query.equalTo(this.key, (Double) key);
        } else if (key.getClass().equals(Boolean.class)) {
            result = query.equalTo(this.key, (Boolean) key);
        } else if (key.getClass().equals(String.class)) {
            result = query.equalTo(this.key, (String) key);
        }
        return result;
    }

    private void log(String message) {
        Log.d(TAG + "<" + type.getSimpleName() + ">", message);
    }

    private static String getPrimaryKey(Class<?> type) {
        for (Field field : type.getDeclaredFields()) {
            PrimaryKey key = field.getAnnotation(PrimaryKey.class);
            if (key != null) {
                if (!isCorrectKeyType(field)) {
                    throw new RealmPrimaryKeyConstraintException("Llave primaria einválida de la entidad " + type.getName());
                }
                return field.getName();
            }
        }
        throw new RealmPrimaryKeyConstraintException("No existe llave primaria para la entidad " + type.getName());
    }

    private static boolean isCorrectKeyType(Field field) {
        return Arrays.asList(
                Byte.class,
                Short.class,
                Integer.class,
                Long.class,
                Float.class,
                Double.class,
                Boolean.class,
                String.class
        ).contains(field.getType());
    }

}
