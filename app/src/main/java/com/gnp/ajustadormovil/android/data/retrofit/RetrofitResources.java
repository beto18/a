package com.gnp.ajustadormovil.android.data.retrofit;

import android.annotation.SuppressLint;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceError;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public final class RetrofitResources {

    private static final String TAG = RetrofitResources.class.getCanonicalName();

    private RetrofitResources(){}

    private static final int DEFAULT_TIMEOUT = 30;

    private static final TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {

                @SuppressLint("TrustAllX509TrustManager")
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }
            }
    };

    public static Interceptor getLoggingInterceptor(){
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG
                ? HttpLoggingInterceptor.Level.BASIC : HttpLoggingInterceptor.Level.NONE);
        loggingInterceptor.setLevel(BuildConfig.DEBUG
                ? HttpLoggingInterceptor.Level.HEADERS : HttpLoggingInterceptor.Level.NONE);
        loggingInterceptor.setLevel(BuildConfig.DEBUG
                ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }

    public static OkHttpClient getUnsafeOkHttpClient(Interceptor ... interceptors) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .hostnameVerifier((hostname, session) -> true);
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            X509TrustManager trustManager = (X509TrustManager) trustAllCerts[0];
            builder.sslSocketFactory(sslSocketFactory,trustManager);
        } catch (Exception e){
            Log.w(TAG, "Error al aprobar certificados SSL", e);
        }
        if(interceptors != null){
            for(Interceptor interceptor : interceptors){
                builder.addInterceptor(interceptor);
            }
        }
        return builder.build();
    }

    public static Interceptor getErrorInterceptor(){
        return chain -> {
            try {
                Request request = chain.request();
                Response response = chain.proceed(request);
                if(response.code() / 100 == 5){
                    ResourceError error = ErrorUtils.getResourceError(request, response);
                    Log.w(TAG, "Error en el servidor: " + error);
                    Crashlytics.log(Log.WARN, "CATALOGOS_ERROR", error.toString());
                }
                return response;
            } catch (IOException ex){
                throw new NetworkException("Ocurrió un problema de red", ex);
            }
        };
    }

}
