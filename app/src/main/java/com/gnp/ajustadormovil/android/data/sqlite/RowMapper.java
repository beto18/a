package com.gnp.ajustadormovil.android.data.sqlite;

import android.database.Cursor;

public interface RowMapper<T> {
     T map(Cursor cursor);
}
