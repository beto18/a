package com.gnp.ajustadormovil.android.data.sqlite;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public abstract class SQLiteData {

    private final Object monitor = new Object();

    private Context context;

    private volatile SqliteHelper helper;

    private final String databaseName;

    public SQLiteData(Context ctx, String databaseName){
        this.context = ctx;
        this.databaseName = databaseName;
    }

    protected SqliteHelper getHelper(){
        synchronized (monitor){
            if(helper == null){
                if(getSqlFilename() == null) {
                    helper = new SqliteHelper(context, databaseName);
                } else {
                    helper = new SqliteHelper(context, databaseName, getInitSql(getSqlFilename()));
                }
            }
            return helper;
        }
    }

    protected  <T> List<T> getList(String query, RowMapper<T> rowMapper, String... parameters) {
        return getHelper().getList(query, rowMapper, parameters);
    }

    protected  <T> T getObject(String query, RowMapper<T> rowMapper, String... parameters) {
       return getHelper().getObject(query, rowMapper, parameters);
    }

    protected <T> Observable<List<T>> getObservableList(String query, RowMapper<T> rowMapper, String... parameters){
        return getHelper().getObservableList(query, rowMapper, parameters);
    }

    protected <T> Observable<T> getObservableObject(String query, RowMapper<T> rowMapper, String... parameters){
        return getHelper().getObservableObject(query, rowMapper, parameters);
    }

    protected <T> Single<List<T>> getSingleList(String query, RowMapper<T> rowMapper, String... parameters){
        return getHelper().getSingleList(query, rowMapper, parameters);
    }

    protected <T> Single<T> getSingleObject(String query, RowMapper<T> rowMapper, String... parameters){
        return getHelper().getSingleObject(query, rowMapper, parameters);
    }

    private String getInitSql(String filename){
        String sql = null;
        try {
            InputStream in = context.getAssets().open(filename);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int count;
            while ((count = in.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            sql = new String(out.toByteArray());
            out.flush();
            out.close();
            in.close();
        } catch (Exception e){
            Log.e(this.getClass().getSimpleName(), "Error al inicializar query de creación ", e);
        }
        return sql;
    }

    protected String getSqlFilename() {
        return null;
    }

}
