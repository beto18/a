package com.gnp.ajustadormovil.android.data.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class SqliteHelper extends SQLiteOpenHelper {

    private static final String TAG = "SqliteHelper";

    private SQLiteDatabase db;

    private String sql;

    public SqliteHelper(Context context, String name) {
        super(context, name, null, 1);
    }

    public SqliteHelper(Context context, String name, String sql) {
        super(context,  name, null, 1);
        this.sql = sql;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if(sql != null) {
            Log.i(TAG, "Ejecución de query de creación de BD");
            db.execSQL(sql);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public  <T> List<T> getList(String query, RowMapper<T> rowMapper, String... parameters) {
        List<T> result = new ArrayList<>();
        Cursor resultCursor = getReadableDatabase().rawQuery(query, parameters);
        while (resultCursor.moveToNext()) {
            result.add(rowMapper.map(resultCursor));
        }
        Log.v(TAG, "Query: " + query + "; Find " + result.size() + " elements");
        resultCursor.close();
        return result;
    }

    public  <T> T getObject(String query, RowMapper<T> rowMapper, String... parameters) {
        T result = null;
        Cursor resultCursor = getReadableDatabase().rawQuery(query, parameters);
        if (resultCursor.moveToNext()) {
            result = rowMapper.map(resultCursor);
        }
        resultCursor.close();

        return result;
    }

    public <T> Observable<List<T>> getObservableList(String query, RowMapper<T> rowMapper, String... parameters){
        return Observable.create(e -> {
               try {
                   e.onNext(getList(query, rowMapper, parameters));
                   e.onComplete();
               } catch (Exception ex){
                   e.tryOnError(ex);
               }
            });
    }

    public <T> Observable<T> getObservableObject(String query, RowMapper<T> rowMapper, String... parameters) {
        return Observable.create(e -> {
            try {
                e.onNext(getObject(query, rowMapper, parameters));
                e.onComplete();
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
        });
    }

    public <T> Single<List<T>> getSingleList(String query, RowMapper<T> rowMapper, String... parameters){
        return Single.create(e -> {
            try {
                e.onSuccess(getList(query, rowMapper, parameters));
            } catch (Exception ex){
                e.tryOnError(ex);
            }
        });
    }

    public <T> Single<T> getSingleObject(String query, RowMapper<T> rowMapper, String... parameters) {
        return Single.create(e -> {
            try {
                e.onSuccess(getObject(query, rowMapper, parameters));
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
        });
    }

}
