package com.gnp.ajustadormovil.android.error;

import com.google.gson.annotations.SerializedName;

public class ActionError {

    private String error;

    @SerializedName("mensaje")
    private String message;

    public ActionError(){ }

    public ActionError(String error, String message) {
        this.error = error;
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
