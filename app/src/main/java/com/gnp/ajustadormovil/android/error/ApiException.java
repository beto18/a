package com.gnp.ajustadormovil.android.error;

public abstract class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }

    public abstract ActionError getError();

}
