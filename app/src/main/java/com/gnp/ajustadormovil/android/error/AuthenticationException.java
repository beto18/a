package com.gnp.ajustadormovil.android.error;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException(String message){
        super(message);
    }

}
