package com.gnp.ajustadormovil.android.error;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataError extends ActionError {

    @SerializedName("campos")
    private List<FieldError> fields;

    public List<FieldError> getFields() {
        return fields;
    }

    public void setFields(List<FieldError> fields) {
        this.fields = fields;
    }

    /**
     * Obtiene el primer mensaje de error que encuentre de un campo.
     * @param fieldName Nombre del campo.
     * @return Mensaje de error del campo. Puede se <code>null</code> si no contiene error.
     */
    public String getMessage(String fieldName){
        if(fields != null){
            for(FieldError field : fields){
                if(field.getField().equals(field)){
                    return field.getMessage();
                }
            }
        }
        return null;
    }

    /**
     * Verifica si un campo tiene error.
     * @param fieldName Nombre del campo.
     * @return Obtiene <code>true</code> si el campo tiene error.
     */
    public boolean hasError(String fieldName){
        for(FieldError field : fields){
            if(field.getField().equals(field)){
                return true;
            }
        }
        return false;
    }

    public boolean hasErrors(){
        return fields != null && !fields.isEmpty();
    }

}
