package com.gnp.ajustadormovil.android.error;


public class DataException extends RuntimeException{

    private DataError error;

    public DataException(DataError error){
        super(error.getMessage());
        this.error = error;
    }

    public DataError getError() {
        return this.error;
    }


}
