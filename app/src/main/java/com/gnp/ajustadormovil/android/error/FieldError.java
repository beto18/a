package com.gnp.ajustadormovil.android.error;

import com.google.gson.annotations.SerializedName;

public class FieldError {

    @SerializedName("campo")
    private String field;
    @SerializedName("mensaje")
    private String message;

    public FieldError(){}

    public FieldError(String field, String message){
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getFieldName(){
        return (field.substring(0, 1).toUpperCase() + field.substring(1))
                .replaceAll("([A-Z][a-z]+)", " $1")
                .replaceAll("([A-Z][A-Z]+)", " $1")
                .replaceAll("([^A-Za-z ]+)", " $1")
                .replaceAll("\\.", "");
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
