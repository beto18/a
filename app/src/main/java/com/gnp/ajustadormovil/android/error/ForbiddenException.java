package com.gnp.ajustadormovil.android.error;

public class ForbiddenException extends AuthenticationException {

    public ForbiddenException(String message){
        super(message);
    }

}
