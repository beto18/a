package com.gnp.ajustadormovil.android.error;

public class LogicException extends ApiException {

    private ActionError error;

    public LogicException(ActionError error){
        super(error.getMessage());
        this.error = error;
    }

    public LogicException(String code, String message){
        super(message);
        this.error = new ActionError();
        this.error.setError(code);
        this.error.setMessage(message);
    }

    public ActionError getError(){
        return this.error;
    }
}
