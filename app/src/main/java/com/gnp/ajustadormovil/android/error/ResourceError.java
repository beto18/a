package com.gnp.ajustadormovil.android.error;

public class ResourceError {

    private String url;

    private String method;

    private String request;

    private String response;

    private int code;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ResourceError {" +
                "\n\turl='" + url + '\'' +
                ", \n\tmethod='" + method + '\'' +
                ", \n\tcode='" + code + '\'' +
                ", \n\trequest='" + request + '\'' +
                ", \n\tresponse='" + response + '\'' +
                "\n}";
    }
}
