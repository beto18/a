package com.gnp.ajustadormovil.android.error;

public class ServerDataException extends DataException {

    public ServerDataException(DataError error) {
        super(error);
    }
}
