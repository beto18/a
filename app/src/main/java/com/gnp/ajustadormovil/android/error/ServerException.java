package com.gnp.ajustadormovil.android.error;

public class ServerException extends RuntimeException {

    public ServerException(String message){
        super(message);
    }

}
