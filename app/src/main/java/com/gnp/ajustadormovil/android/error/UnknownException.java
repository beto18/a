package com.gnp.ajustadormovil.android.error;

public class UnknownException extends ApiException {

    private ActionError error;

    public UnknownException(ActionError error){
        super(error.getMessage());
        this.error = error;
    }

    public ActionError getError(){
        return this.error;
    }
}
