package com.gnp.ajustadormovil.android.error;

public class UserNotFoundException extends AuthenticationException {

    public UserNotFoundException(String message){
        super(message);
    }
}
