package com.gnp.ajustadormovil.android.interactors.amis;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.interactors.orquestador.SyncInteractor;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.Certificado;
import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparte;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.models.amis.ValeDigital;

import java.util.Collection;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AmisInteractor extends SyncInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<List<Modulo>> getModulos(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Completable saveModulos(String ajuste, List<Modulo> modulos);

    @RxLoading
    @RxApplySchedulers
    Single <Amis> getFolio(String ajuste,boolean offline);

    @RxLoading
    @RxApplySchedulers
    Completable reFolear(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<List<String>> encriptarModulos(List<Modulo> modulos);

    @RxLoading
    @RxApplySchedulers
    Single<List<Modulo>> desencriptarModulos(List<String> modulos);

    @RxLoading
    @RxApplySchedulers
    Completable sincronizar(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Completable getInformacion(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Completable vincularFolios(String ajuste, Collection<String> folios);

    Single<CertificadoCompania> getCertificado(Long compania);

    Single<Certificado> getCertificadoPropio();

    @RxLoading
    @RxApplySchedulers
    Single<ValeDigital> recibirOrden(String ajuste, long tercero);

    Single<FolioContraparteAmis> getContraparte(String ajuste, long tercero);

    @RxLoading
    @RxApplySchedulers
    Single<List<FolioContraparte>> getContrapartes(String ajuste);

}
