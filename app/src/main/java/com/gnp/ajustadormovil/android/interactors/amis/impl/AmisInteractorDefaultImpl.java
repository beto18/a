package com.gnp.ajustadormovil.android.interactors.amis.impl;

import android.util.Base64;
import android.util.Log;

import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AmisData;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CertificadoCompaniaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CertificadoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CroquisRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.error.ApiException;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.SyncInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.models.amis.AjusteAmis;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.AmisClientException;
import com.gnp.ajustadormovil.android.models.amis.AmisServerException;
import com.gnp.ajustadormovil.android.models.amis.AseguradoAmis;
import com.gnp.ajustadormovil.android.models.amis.Certificado;
import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;
import com.gnp.ajustadormovil.android.models.amis.Circunstancia;
import com.gnp.ajustadormovil.android.models.amis.Danios;
import com.gnp.ajustadormovil.android.models.amis.Folio;
import com.gnp.ajustadormovil.android.models.amis.FolioAmis;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparte;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.amis.ImagenAmis;
import com.gnp.ajustadormovil.android.models.amis.Involucrado;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoData;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoDetalles;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoGeneral;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoModulo;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.models.amis.Narrativa;
import com.gnp.ajustadormovil.android.models.amis.PaseMedico;
import com.gnp.ajustadormovil.android.models.amis.PolizaAmis;
import com.gnp.ajustadormovil.android.models.amis.Refoleo;
import com.gnp.ajustadormovil.android.models.amis.SiniestroAmis;
import com.gnp.ajustadormovil.android.models.amis.ValeDigital;
import com.gnp.ajustadormovil.android.models.amis.VehiculoAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.codec.DecodeEngine;
import com.gnp.ajustadormovil.android.utils.codec.Mod1271_36;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrNotFoundException;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public class AmisInteractorDefaultImpl extends SyncInteractorOfflineImpl implements AmisInteractor {

    private final Map<Class<? extends Modulo>, ModelHandler<?>> handlers = new HashMap<>();

    private final String ABECEDARIO = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private final String TAG = AmisInteractor.class.getSimpleName();

    private final CroquisRepository croquisRepository = RepositoryFactory.getCroquisRepository();

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final AmisData amisData = OrquestadorFactory.getAmisData();

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private final CertificadoCompaniaRepository certificadoCompaniaRepository = RepositoryFactory.getCertificadoCompaniaRepository();

    private final CertificadoRepository certificadoRepository = RepositoryFactory.getCertificadoRepository();

    private final Gson parser = new Gson();

    public AmisInteractorDefaultImpl() {
        handlers.put(SiniestroAmis.class, new SiniestroSepomexModelHandler());
        handlers.put(PolizaAmis.class, new PolizaModelHandler());
        handlers.put(AseguradoAmis.class, new AseguradoModelHandler());
        handlers.put(VehiculoAmis.class, new VehiculoModelHandler());
        handlers.put(Danios.class, new DaniosModelHandler());
        handlers.put(Circunstancia.class, new CircunstanciaModelHandler());
        handlers.put(InvolucradoDetalles.class, new InvolucradoDetallesSepomexModelHandler());
        handlers.put(InvolucradoGeneral.class, new InvolucradoGeneralModelHandler());
        handlers.put(PaseMedico.class, new PaseMedicoModelHandler());
        handlers.put(Narrativa.class, new NarrativaModelHandler());
        handlers.put(ImagenAmis.class, new ImagenModelHandler());
    }

    @Override
    public Single<List<Modulo>> getModulos(String ajuste) {
        return Single.create(e -> {
            assertGenerateModulos(ajuste);
            List<Modulo> modulos = new ArrayList<>();
            for (Class<?> type : handlers.keySet()) {
                modulos.addAll(handlers.get(type).get(ajuste));
            }
            Amis amis = amisRepository.findByAjuste(ajuste);
            String folio = amis.getFolio().getFolio() != null
                    ? amis.getFolio().getFolio()
                    : amis.getFolio().getFolioInicial();
            for (Modulo modulo : modulos) {
                modulo.setFolio(folio);
            }
            sortModulos(modulos);
            e.onSuccess(modulos);
        });
    }

    private void assertGenerateModulos(String ajuste) {
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
        try {
            ErrorUtils.assertValues(asegurado.getConductor());
        } catch (DataException e) {
            throw new LogicException("conductor_incompleto", "conductor_incompleto");
        }
        try {
            ErrorUtils.assertValues(asegurado.getVehiculo());
        } catch (DataException e) {
            throw new LogicException("vehiculo_incompleto", "vehiculo_incompleto");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "responsabilidad_no_registrada");
        }
    }

    @Override
    public Completable saveModulos(String ajuste, List<Modulo> modulos) {
        return Completable.create(e -> {
            sortModulos(modulos);
            Set<String> folios = new HashSet<>();

            for (Modulo modulo : modulos) {
                folios.add(modulo.getFolio());
            }
            RXUtils.sync(vincularFolios(ajuste, folios));
            for (Modulo modulo : modulos) {
                Log.d(TAG, "modulo: " + modulo);
                handlers.get(modulo.getClass()).save(ajuste, modulo);
            }
            e.onComplete();
        });
    }

    private void sortModulos(List<Modulo> modulos) {
        Collections.sort(modulos, (a, b) -> {
            if(a instanceof InvolucradoModulo && b instanceof InvolucradoModulo) {
                Byte orderA = ((InvolucradoModelHandler)handlers.get(a.getClass())).getSeccion();
                Byte orderB = ((InvolucradoModelHandler)handlers.get(b.getClass())).getSeccion();
                return orderA.compareTo(orderB);
            } else {
                String orderA = handlers.get(a.getClass()).getIdModulo();
                String orderB = handlers.get(b.getClass()).getIdModulo();
                return orderA.compareTo(orderB);
            }
        });
    }

    @Override
    public Single<Amis> getFolio(String ajuste, boolean offline) {
        return Single.create(e -> {
            Amis amis = amisRepository.findByAjuste(ajuste);
            if (amis == null) {
                amis = new Amis();
                amis.setAjuste(ajuste);
                if (offline) {
                    getFolioOffline(ajuste, amis);
                    e.onSuccess(amis);
                } else {
                    try {
                        getFolioOnline(ajuste, amis);
                        e.onSuccess(amis);
                    } catch (ApiException ex) {
                        e.tryOnError(checkAmisError(ex));
                    } catch (Exception ex) {
                        e.tryOnError(ex);
                    }
                }
            } else {
                e.onSuccess(amis);
            }
        });
    }

    private void getFolioOnline(String ajuste, Amis amis) throws Exception {
        FolioAmis folioAmis = new FolioAmis();
        Folio folio = RXUtils.sync(amisData.generarFolio(ajuste));
        folioAmis.setFolioInicial(folio.getFolio());
        amis.setOnline(!folio.getOffline());
        amis.setFolio(folioAmis);
        amis.setSincronizado(true);
        amisRepository.save(amis);
    }

    private void getFolioOffline(String ajuste, Amis amis) {
        String folio;
        Ajuste a = ajusteRepository.findById(ajuste);
        FolioAmis folioAmis = new FolioAmis();
        Date fechaArribo = a.getFechaArribo();
        folio = Constants.CLAVE_COMPANIA + AjustadorMovilApplication.getCedula() + DateUtils.dateTimeEDUA(fechaArribo);
        folio = new Mod1271_36().compute(folio) + "A";
        amis.setOnline(false);
        amis.setSincronizado(false);
        folioAmis.setFolioInicial(folio);
        amis.setFolio(folioAmis);
        amisRepository.save(amis);
    }

    @Override
    public Completable reFolear(String ajuste) {
        return Completable.create(e -> {
            Amis amis = amisRepository.findByAjuste(ajuste);
            if (!amis.getContrapartes().isEmpty()) {
                if (amis.isOnline()) {
                    refolearOnline(amis);
                } else {

                    refolearOffline(amis);
                }
            }
            e.onComplete();
        });
    }

    private void refolearOffline(Amis amis) {
        List<String> folios = new ArrayList<>();
        folios.add(amis.getFolio().getFolioInicial());
        for (FolioContraparteAmis contraparte : amis.getContrapartes()) {
            folios.add(contraparte.getFolioInicial());
        }
        Function<String, Date> folioToDate = folio -> {
            String fecha = folio.substring(folio.length() - (12 + 3), folio.length() - 3);
            return new SimpleDateFormat("ddMMyyHHmmss", Locale.forLanguageTag("es-MX")).parse(fecha);
        };
        Collections.sort(folios, (a, b) -> {
            try {
                Date fechaA = folioToDate.apply(a);
                Date fechaB = folioToDate.apply(b);
                return fechaA.compareTo(fechaB);
            } catch (Exception ex) {
                return 0;
            }
        });
        String folio = folios.get(0);
        folio = folio.substring(0, folio.length() - 1);
        int index = folios.indexOf(amis.getFolio().getFolioInicial());
        amis.getFolio().setFolio(folio + ABECEDARIO.charAt(index));
        for (FolioContraparteAmis contraparte : amis.getContrapartes()) {
            index = folios.indexOf(contraparte.getFolioInicial());
            contraparte.setFolio(folio + ABECEDARIO.charAt(index));
        }
        amisRepository.save(amis);
    }

    private void refolearOnline(Amis amis) throws Exception {
        List<Refoleo> folios;
        try {
            folios = RXUtils.sync(amisData.refolear(amis.getAjuste()));
        } catch (ApiException ex) {
            throw checkAmisError(ex);
        }
        Function<String, String> getRefoleo = folioInicial -> {
            for (Refoleo r : folios) {
                if (r.getFolioInicial().equals(folioInicial)) {
                    return r.getFolioRefoleo();
                }
            }
            return null;
        };
        amis.getFolio().setFolio(getRefoleo.apply(amis.getFolio().getFolioInicial()));
        for (FolioContraparteAmis contraparte : amis.getContrapartes()) {
            contraparte.setFolio(getRefoleo.apply(contraparte.getFolioInicial()));
        }
        amisRepository.save(amis);
    }

    @Override
    public Completable vincularFolios(String ajuste, Collection<String> folios) {
        return Completable.create(e -> {
            Amis amis = amisRepository.findByAjuste(ajuste);
            Amis rollback = amisRepository.findByAjuste(ajuste);
            try {
                Collection<String> foliosFiltrados = CollectionsUtils.filter(folios, folio ->
                        !CollectionsUtils.anyMatch(amis.getContrapartes(),
                                contraparte -> folio.equals(contraparte.getFolioInicial()))
                );
                CollectionsUtils.forEach(foliosFiltrados, folio -> {
                    FolioContraparteAmis folioContraparte = new FolioContraparteAmis();
                    folioContraparte.setFolioInicial(folio);
                    amis.getContrapartes().add(folioContraparte);
                });
                amisRepository.save(amis);
                if (amis.isOnline()) {
                    try {
                        RXUtils.sync(amisData.vincularFolios(ajuste, folios));
                    } catch (ApiException ex) {
                        throw checkAmisError(ex);
                    }
                }
                e.onComplete();
            } catch (Exception ex) {
                amisRepository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Amis amis = amisRepository.findByAjuste(ajuste);
            Folio folio = new Folio();
            folio.setFolio(amis.getFolio().getFolioInicial());
            if (!isSynchronized(ajuste)) {
                try {
                    RXUtils.sync(amisData.guardarFolio(ajuste, folio));
                    Log.i(TAG, "El folio amis " + folio.getFolio() + " del ajuste: " + ajuste + " sincronizado");
                    amis.setSincronizado(true);
                    amisRepository.save(amis);
                    e.onComplete();
                } catch (Exception ex) {
                    Log.e(TAG, "Error al sincronizar el folio amis del ajuste: " + ajuste, ex);
                    e.tryOnError(ex);
                }
            } else {
                Log.i(TAG, "El folio amis " + folio.getFolio() + "del ajuste: " + ajuste + " ya está sincronizado");
                e.onComplete();
            }
        });
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        boolean sincronizado = true;
        Amis amis = amisRepository.findByAjuste(ajuste);
        if (amis != null) {
            sincronizado = amis.getSincronizado();
        }
        return sincronizado;
    }

    @Override
    public Completable initialize(String ajuste) {
        return Completable.create(e -> {
            Amis amis = amisRepository.findByAjuste(ajuste);
            if (amis == null) {
                amis = RXUtils.sync(getFolio(ajuste, false));
            }
            if (amis != null && amis.getFolio() != null) {
                Log.i(TAG, "Descargando datos de la contraparte del ajuste " + ajuste);
                RXUtils.sync(getInformacion(ajuste));
            }

            if (certificadoCompaniaRepository.count() == 0) {
                try {
                    List<CertificadoCompania> certificados = RXUtils.sync(amisData.getCertificados());
                    for (CertificadoCompania cer : certificados) {
                        certificadoCompaniaRepository.save(cer);
                    }
                    e.onComplete();
                } catch (ApiException ex) {
                    e.tryOnError(checkAmisError(ex));
                } catch (Exception ex) {
                    e.tryOnError(ex);
                }
            } else {
                e.onComplete();
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            croquisRepository.delete(ajuste);
            e.onComplete();
        });
    }

    @Override
    protected void cleanErrors(String ajuste) {
        croquisRepository.delete(ajuste);
    }

    @Override
    public Single<List<String>> encriptarModulos(List<Modulo> modulos) {
        return Single.create(e -> {
            handlers.get(modulos);
            List<String> listaFinal = new ArrayList<>();
            for (Modulo modulo : modulos) {
                String jsonEncriptado = DecodeEngine.encode(modulo.getFolio(), parser.toJson(modulo));
                StringBuilder qr = new StringBuilder(modulo.getFolio());
                qr.append("|");
                qr.append(handlers.get(modulo.getClass()).getIdModulo());
                qr.append("|");
                qr.append(jsonEncriptado);
                listaFinal.add(qr.toString());
            }
            e.onSuccess(listaFinal);
        });
    }

    @Override
    public Single<List<Modulo>> desencriptarModulos(List<String> modulos) {
        return Single.create(e -> {
            if (!modulos.isEmpty()) {
                List<Modulo> result = new ArrayList<>();
                for (String modulo : modulos) {
                    String[] qr = modulo.split("\\|");
                    String json = DecodeEngine.decode(qr[0], qr[2]);
                    Log.d(TAG, "MODULO: " + modulo);
                    result.add(parser.fromJson(json, getModuloType(modulo)));
                }
                e.onSuccess(result);
            } else {
                e.tryOnError(new QrNotFoundException());
            }
        });
    }

    @Override
    public Completable sincronizar(String ajuste) {
        return Completable.create(e -> {
            assertGenerateModulos(ajuste);
            Amis amis = amisRepository.findByAjuste(ajuste);
            try {
                if (amis != null && !amis.isOnline()) {
                    RXUtils.sync(amisData.guardarFolio(ajuste, new Folio(amis.getFolio().getFolioInicial())));
                }
                RXUtils.sync(amisData.sincronizar(ajuste));
                if (amis != null && !amis.isOnline()) {
                    amis.setOnline(true);
                    amisRepository.save(amis);
                }
            } catch (ApiException ex) {
                e.tryOnError(checkAmisError(ex));
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
            e.onComplete();
        });
    }

    @Override
    public Completable getInformacion(String ajuste) {
        return Completable.create(e -> {
            List<AjusteAmis> ajustes;
            try {
                ajustes = RXUtils.sync(amisData.getContrapartes(ajuste));
                List<Modulo> modulos = new ArrayList<>();
                for (AjusteAmis a : ajustes) {
                    modulos.addAll(convert(a));
                }
                RXUtils.sync(saveModulos(ajuste, modulos));
            } catch (ApiException ex) {
                e.tryOnError(checkAmisError(ex));
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
            e.onComplete();
        });
    }

    private Class<? extends Modulo> getModuloType(String modulo) {
        Set<Class<? extends Modulo>> keys = handlers.keySet();
        for (Class<? extends Modulo> type : keys) {
            if (handlers.get(type).isModuloType(modulo)) {
                return type;
            }
        }
        return null;
    }

    private List<Modulo> convert(Involucrado involucrado) {
        List<Modulo> result = new ArrayList<>(2);
        result.add(convertDetalles(involucrado.getDatosGrales()));
        result.add(convertGeneral(involucrado.getDatosGrales()));
        if (involucrado.getNarrativa() != null) {
            involucrado.getNarrativa().setIdInvolucrado(involucrado.getDatosGrales().getIdInvolucrado());
            result.add(involucrado.getNarrativa());
        }
        if (involucrado.getPaseMedico() != null) {
            involucrado.getPaseMedico().setIdInvolucrado(involucrado.getDatosGrales().getIdInvolucrado());
            result.add(involucrado.getPaseMedico());
        }
        return result;
    }

    private InvolucradoDetalles convertDetalles(InvolucradoData involucrado) {
        InvolucradoDetalles detalles = new InvolucradoDetalles();
        detalles.setIdInvolucrado(involucrado.getIdInvolucrado());
        detalles.setDireccion(involucrado.getDireccion());
        detalles.setFolio(involucrado.getFolio());
        detalles.setMismoAsegurado(involucrado.getMismoAsegurado());
        detalles.setPersona(involucrado.getPersona());
        detalles.setTipoInvolucrado(involucrado.getTipoInvolucrado());
        return detalles;
    }

    private InvolucradoGeneral convertGeneral(InvolucradoData involucrado) {
        InvolucradoGeneral general = new InvolucradoGeneral();
        general.setAvisoPrivacidad(involucrado.getAvisoPrivacidad());
        general.setCorreoElectronico(involucrado.getCorreoElectronico());
        general.setEstadoLicencia(involucrado.getEstadoLicencia());
        general.setFechaVigenciaLicencia(involucrado.getFechaVigenciaLicencia());
        general.setFolio(involucrado.getFolio());
        general.setIdInvolucrado(involucrado.getIdInvolucrado());
        general.setLicencia(involucrado.getLicencia());
        general.setTelefono(involucrado.getTelefono());
        general.setTipoLicencia(involucrado.getTipoLicencia());
        general.setTratamientoDatosPersonales(involucrado.getTratamientoDatosPersonales());
        general.setVersionAvisoPrivacidadCompania(involucrado.getVersionAvisoPrivacidadCompania());
        general.setVersionAvisoPrivacidadGeneral(involucrado.getVersionAvisoPrivacidadGeneral());
        general.setVersionCtdpCompania(involucrado.getVersionCtdpCompania());
        general.setVersionCtdpGeneral(involucrado.getVersionCtdpCompania());
        return general;
    }

    private List<Modulo> convert(AjusteAmis ajuste) {
        List<Modulo> modulos = new ArrayList<>();
        modulos.add(ajuste.getSiniestro());
        modulos.add(ajuste.getPoliza());
        modulos.add(ajuste.getAsegurado());
        for (Involucrado involucrado : ajuste.getInvolucrados()) {
            modulos.addAll(convert(involucrado));
        }
        modulos.add(ajuste.getVehiculo());
        modulos.add(ajuste.getDanioGeneral());
        modulos.add(ajuste.getCircunstancia());
        for (ImagenAmis imagen : ajuste.getImagenes()) {
            modulos.add(imagen);
        }
        CollectionsUtils.removeIf(modulos, it -> it == null);
        CollectionsUtils.forEach(modulos, it -> it.setFolio(ajuste.getFolioAtencionInicial()));
        return modulos;
    }

    @Override
    public Single<CertificadoCompania> getCertificado(Long compania) {
        return Single.create(e -> {
            try {
                CertificadoCompania certificado = RXUtils.sync(amisData.getCertificado(compania).map(cer -> {
                    cer.setLlavePublica(Base64.encodeToString(cer.getLlavePublica().getBytes(), Base64.DEFAULT));
                    return cer;
                }));
                certificadoCompaniaRepository.save(certificado);
                e.onSuccess(certificado);
            } catch (NetworkException ex) {
                CertificadoCompania certificado = certificadoCompaniaRepository.findByCompania(compania);
                if (certificado != null) {
                    e.onSuccess(certificado);
                } else {
                    e.tryOnError(new ResourceNotFoundException());
                }
            } catch (ApiException ex) {
                e.tryOnError(checkAmisError(ex));
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Certificado> getCertificadoPropio() {
        return Single.create(e -> {
            CertificadoCompania cer = RXUtils.sync(getCertificado(Constants.AMIS_ID_GNP));
            String serial = DecodeEngine.getSerialNumber(cer.getCertificado());
            Certificado certificado = certificadoRepository.findByNumeroSerial(serial);
            if (certificado == null) {
                certificado = RXUtils.sync(amisData.getCertificadoPropio(serial));
                certificadoRepository.save(certificado);
            }
            e.onSuccess(certificado);
        });
    }

    @Override
    public Single<ValeDigital> recibirOrden(String ajuste, long id) {
        return Single.create(e -> {
            String folioOrden = null;
            Amis amis = amisRepository.findByAjuste(ajuste);
            TerceroAuto tercero = terceroAutoRepository.findByAjusteAndId(ajuste, id);
            for (FolioContraparteAmis c : amis.getContrapartes()) {
                if (c.getFolioInicial().equals(tercero.getFolioAmis())) {
                    folioOrden = c.getFolio();
                    folioOrden += amis.getFolio().getFolio().charAt(amis.getFolio().getFolio().length() - 1);
                }
            }
            if (folioOrden != null) {
                ValeDigital valeDigital = RXUtils.sync(amisData.recibirOrden(ajuste, folioOrden));
                e.onSuccess(valeDigital);
            } else {
                e.tryOnError(new ResourceNotFoundException());
            }
        });
    }

    @Override
    public Single<FolioContraparteAmis> getContraparte(String ajuste, long id) {
        return Single.create(e -> {
            FolioContraparteAmis contraparte;
            Amis amis = amisRepository.findByAjuste(ajuste);
            TerceroAuto tercero = terceroAutoRepository.findByAjusteAndId(ajuste, id);
            for (FolioContraparteAmis c : amis.getContrapartes()) {
                if (c.getFolioInicial().equals(tercero.getFolioAmis())) {
                    contraparte = c;
                    e.onSuccess(contraparte);
                }
            }

            e.tryOnError(new ResourceNotFoundException());
        });
    }

    @Override
    public Single<List<FolioContraparte>> getContrapartes(String ajuste) {
        return amisData.getContrapartes(ajuste)
                .map(ajustes -> {
                    List<FolioContraparte> contrapartes = new ArrayList<>();
                   /* for(AjusteAmis a : ajustes) {
                        FolioContraparte folio = new FolioContraparte();
                        folio.setFolioInicial(a.getFolioAtencionInicial());
                        folio.setFolio(a.getFolioAtencion());
                        if(folio.getFolio() != null && folio.getFolio().isEmpty()) {
                            folio.setFolio(null);
                        }
                        contrapartes.add(folio);
                        // TODO: Verificar como saber si no esta completa la información
                    }*/
                    contrapartes.add(new FolioContraparte("GNPC4345091019114743I9A","GNPC4345091019114743I9A", true));
                    contrapartes.add(new FolioContraparte("GNPC4345091019114743I9B","GNPC4345091019114743I9B", false));
                    contrapartes.add(new FolioContraparte("GNPC4345091019114743I9C","GNPC4345091019114743I9C", true));
                    contrapartes.add(new FolioContraparte("GNPC4345091019114743I9D","GNPC4345091019114743I9D", false));
                    contrapartes.add(new FolioContraparte("GNPC4345091019114743I9E","GNPC4345091019114743I9E", true));
                    contrapartes.add(new FolioContraparte("GNPC4345091019114743I9F","GNPC4345091019114743I9F", false));
                    return contrapartes;
                });
    }


    private RuntimeException checkAmisError(RuntimeException ex) {
        RuntimeException result = ex;
        if (ex instanceof ApiException) {
            String code = ((ApiException) ex).getError().getError();
            if ("amis_conflict".equals(code)) {
                result = new AmisClientException(ex.getMessage());
            } else if ("amis_unavailable".equals(code)) {
                result = new AmisServerException(ex.getMessage());
            }
        }
        return result;
    }


}
