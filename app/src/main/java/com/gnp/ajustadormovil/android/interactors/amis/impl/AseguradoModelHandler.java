package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.AseguradoAmis;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.amis.PersonaAmis;
import com.gnp.ajustadormovil.android.models.amis.PolizaAmis;
import com.gnp.ajustadormovil.android.models.amis.TelefonoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PersonaPoliza;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AseguradoModelHandler extends ModelHandler<AseguradoAmis> {

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private static final String MODULO = "03";

    private static final String TIPO_PERSONA_FISICA = "F";

    @Override
    public List<AseguradoAmis> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos del asegurado");
        List<AseguradoAmis> result = new ArrayList<>(1);
        Ajuste a = ajusteRepository.findById(ajuste);
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        AseguradoAmis amis;
        if(a.getSiniestro().getPoliza() != null && a.getSiniestro().getPoliza().getTitular() != null) {
            amis = get(a.getSiniestro().getPoliza().getTitular());
        } else {
            amis = get(asegurado.getConductor());
        }
        amis.setTelefono(getTelefono(asegurado.getConductor()));
        result.add(amis);
        return result;
    }

    private AseguradoAmis get(Persona persona) {
        AseguradoAmis amis = new AseguradoAmis();
        PersonaAmis personaAmis = new PersonaAmis();
        String[] nombres = persona.getNombre().split(" ");
        personaAmis.setNombre(nombres[0]);
        if (nombres.length > 1) {
            personaAmis.setSegundoNombre(nombres[1]);
        } else {
            personaAmis.setSegundoNombre("");
        }
        personaAmis.setApellidoPaterno(persona.getApellidoPaterno());
        personaAmis.setApellidoMaterno(persona.getApellidoMaterno());
        CatalogoClaveAmis genero = this.getCatalogoClaveAmis(siniestrosInteractor.getGenero(persona.getSexo()));
        personaAmis.setGenero(new CatalogoAmis(genero.getClaveAmis()));
        personaAmis.setEdad(String.valueOf(DateUtils.yearsAgo(persona.getFechaNacimiento())));
        personaAmis.setId(null);
        personaAmis.setRazonSocial("");
        amis.setPersona(personaAmis);
        CatalogoClaveAmis tipoPersona = this.getCatalogoClaveAmis(siniestrosInteractor.getTipoPersona(TIPO_PERSONA_FISICA));
        amis.setTipoPersona(new CatalogoAmis(tipoPersona.getClaveAmis()));
        return amis;
    }

    private AseguradoAmis get(PersonaPoliza persona) {
        AseguradoAmis amis = new AseguradoAmis();
        PersonaAmis personaAmis = new PersonaAmis();
        if(TIPO_PERSONA_FISICA.equals(persona.getTipo())) {
            String[] nombres = persona.getNombre().split(" ");
            personaAmis.setNombre(nombres[0]);
            if (nombres.length > 1) {
                personaAmis.setSegundoNombre(nombres[1]);
            } else {
                personaAmis.setSegundoNombre("");
            }
            personaAmis.setApellidoPaterno(persona.getApellidoPaterno());
            personaAmis.setApellidoMaterno(persona.getApellidoMaterno());
            try {
                String date = persona.getRfc().substring(4, 10);
                Date fechaNacimiento = new SimpleDateFormat("yyMMdd").parse(date);
                personaAmis.setEdad(String.valueOf(DateUtils.yearsAgo(fechaNacimiento)));
            } catch (Exception e) {
                personaAmis.setEdad("0");
            }
        } else {
            personaAmis.setRazonSocial(persona.getRazonSocial());
            personaAmis.setNombre(persona.getRazonSocial());
        }
        CatalogoClaveAmis tipoPersona = this.getCatalogoClaveAmis(siniestrosInteractor.getTipoPersona(persona.getTipo()));
        amis.setTipoPersona(new CatalogoAmis(tipoPersona.getClaveAmis()));
        amis.setPersona(personaAmis);
        return amis;
    }

    @Override
    protected void saveModulo(String ajuste, AseguradoAmis modulo) {
        Amis amis = amisRepository.findByAjuste(ajuste);
        FolioContraparteAmis contraparte = null;
        for(FolioContraparteAmis c : amis.getContrapartes()) {
            if(c.getFolioInicial().equals(modulo.getFolio())) {
                contraparte = c;
                if (contraparte.getTitular() == null) {
                    putContraparte(c, modulo);
                }
            }
        }
        if(contraparte == null) {
            contraparte = new FolioContraparteAmis();
            contraparte.setFolioInicial(modulo.getFolio());
            putContraparte(contraparte,modulo);
            amis.getContrapartes().add(contraparte);
        }
        amisRepository.save(amis);
    }


    private void putContraparte(FolioContraparteAmis contraparte, AseguradoAmis modulo){
        String titular = "";
        if(modulo.getPersona() != null && modulo.getTipoPersona() != null) {
            CatalogoClaveAmis tipoPersona = this.getCatalogoClaveAmis(siniestrosInteractor.getTipoPersona(TIPO_PERSONA_FISICA));
            if (tipoPersona != null) {
                if (tipoPersona.getClaveAmis().equals(modulo.getTipoPersona().getClave())) {
                    titular = getNombreCompleto(modulo.getPersona());
                } else {
                    titular = modulo.getPersona().getRazonSocial();
                }
            }
        }
        contraparte.setTitular(titular);
    }

    private String getNombreCompleto(PersonaAmis persona) {
        if (persona.getNombre() == null && persona.getApellidoPaterno() == null) {
            return "";
        }
        return ((persona.getNombre() != null ? persona.getNombre() : "")  + " "
                + (persona.getSegundoNombre() != null ? persona.getSegundoNombre() : "") + " "
                + (persona.getApellidoPaterno() != null ? persona.getApellidoPaterno() : "") + " "
                + (persona.getApellidoMaterno() !=null ? persona.getApellidoMaterno() : "") + " ")
                .trim();
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }

    private TelefonoAmis getTelefono(Afectado afectado) {
        Telefono telefono = afectado.getTelefonos().get(0);
        TelefonoAmis result = new TelefonoAmis();
        result.setLada("");
        result.setNumero(telefono.getTelefono());
        CatalogoClaveAmis tipo = this.getCatalogoClaveAmis(siniestrosInteractor.getTipoTelefono(telefono.getTipo()));
        result.setTipoTelefono(new CatalogoAmis(tipo.getClaveAmis()));
        return result;
    }

}
