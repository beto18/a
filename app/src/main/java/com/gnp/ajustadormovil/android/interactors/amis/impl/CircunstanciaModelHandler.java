package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.Circunstancia;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.List;

public class CircunstanciaModelHandler extends ModelHandler<Circunstancia> {

    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private static final String MODULO = "07";

    private static final CatalogoAmis DEFAULT_CIRCUNSTANCIA_VEHICULO = new CatalogoAmis("9");

    @Override
    public List<Circunstancia> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos de las circunstancias");
        List<Circunstancia> result = new ArrayList<>(1);
        Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);

        if (responsabilidad != null){
            result.add(getCircusntancia(responsabilidad));
        }

        return result;
    }



    private Circunstancia getCircusntancia(Responsabilidad responsabilidad){
        Circunstancia circunstancia = new Circunstancia();
        circunstancia.setAcuerdo(false);
        circunstancia.setTipoAcuerdo(new ArrayList<>(0));

        if(responsabilidad.getCircunstanciaA() != null && responsabilidad.getTipoTercero().equals("TA")) {
            circunstancia.setOtraCircunstancia(false);
            circunstancia.setOtraCircunstanciaDescripcion("");

            CatalogoId cat = getCatalogoOrElse(siniestrosInteractor.getCircustanciaVehiculo(responsabilidad.getCircunstanciaA()));
            if (cat != null){
                circunstancia.setTipoCircunstancia(new CatalogoAmis(cat.getId().toString()));
            }
            else{
                circunstancia.setTipoCircunstancia(DEFAULT_CIRCUNSTANCIA_VEHICULO);
            }

        } else {
            circunstancia.setTipoCircunstancia(new CatalogoAmis());
            circunstancia.setOtraCircunstancia(true);
            circunstancia.setOtraCircunstanciaDescripcion(getCatalogoOrElse(siniestrosInteractor.getTipoTercero(responsabilidad.getTipoTercero())).getNombre());
        }

        return circunstancia;

    }

    @Override
    protected void saveModulo(String ajuste, Circunstancia modulo) {

    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }
}
