package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.Danio;
import com.gnp.ajustadormovil.android.models.amis.Danios;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ParteAfectada;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DaniosModelHandler extends ModelHandler<Danios> {

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private static final String MODULO = "06";

    private static final CatalogoAmis TIPO_PRIMER_IMPACTO =
            new CatalogoAmis("1");

    private static final CatalogoAmis TIPO_DANIO_CONSECUENCIA =
            new CatalogoAmis("2");

    private static final CatalogoAmis TIPO_DANIO_PREEXISTENTE =
            new CatalogoAmis("3");

    private static final CatalogoAmis CUADRANTE_SUPERIOR_IZQUIERDO =
            new CatalogoAmis("1");

    private static final CatalogoAmis CUADRANTE_SUPERIOR_DERECHO =
            new CatalogoAmis("2");

    private static final CatalogoAmis CUADRANTE_TRASERO_IZQUIERDO =
            new CatalogoAmis("3");

    private static final CatalogoAmis CUADRANTE_TRASERO_DERECHO =
            new CatalogoAmis("4");

    private static final CatalogoAmis DEFAULT_PARTE_DANIADA = new CatalogoAmis("35");

    private static final Long DEFAULT_AREA = 2L;

    private CatalogoAmis getCuadranteById(String id) {
        CatalogoAmis result = null;
        if (CUADRANTE_SUPERIOR_DERECHO.getClave().equals(id)) {
            result = CUADRANTE_SUPERIOR_DERECHO;
        } else if (CUADRANTE_SUPERIOR_IZQUIERDO.getClave().equals(id)) {
            result = CUADRANTE_SUPERIOR_IZQUIERDO;
        } else if (CUADRANTE_TRASERO_DERECHO.getClave().equals(id)) {
            result = CUADRANTE_SUPERIOR_DERECHO;
        } else if (CUADRANTE_TRASERO_IZQUIERDO.getClave().equals(id)) {
            result = CUADRANTE_TRASERO_IZQUIERDO;
        }
        return result;
    }

    @Override
    public List<Danios> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos de los daños");
        List<Danios> result = new ArrayList<>();
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        if (asegurado != null && asegurado.getVehiculo() != null &&
                (asegurado.getVehiculo().getDanoConsecuencia() && !asegurado.getVehiculo().getPerdidaTotalEvidente())) {
            Danios danios = new Danios();
            danios.setListaDanios(new ArrayList<>());
            danios.getListaDanios().add(getPrimerImpacto(asegurado.getVehiculo()));
            danios.getListaDanios().addAll(getDaniosConsecuencia(asegurado.getVehiculo()));
            danios.getListaDanios().addAll(getDaniosPreexistentes(asegurado.getVehiculo()));
            result.add(danios);
        }
        return result;
    }

    private Danio getPrimerImpacto(VehiculoAsegurado vehiculo) {
        Danio result = new Danio();
        if (vehiculo.getPrimerImpacto() != null) {
            result.setTipoDanio(TIPO_PRIMER_IMPACTO);
            ImpactoVehiculoAmis impacto = getCatalogoOrElse(siniestrosInteractor.getImpactoVehiculo(vehiculo.getPrimerImpacto()));
            if(impacto != null){
                result.setArea(getCuadranteById(impacto.getAreaAmis().toString()));
                result.setParteDaniada(new CatalogoAmis(impacto.getClaveAmis()));
            }
            else {
                result.setArea(getCuadranteById(DEFAULT_AREA.toString()));
                result.setParteDaniada(DEFAULT_PARTE_DANIADA);
            }
        }
        return result;
    }

    private Collection<Danio> getDaniosConsecuencia(VehiculoAsegurado vehiculo) {
        return getDanios(vehiculo.getDanos(), TIPO_DANIO_CONSECUENCIA);
    }

    private Collection<Danio> getDaniosPreexistentes(VehiculoAsegurado vehiculo) {
        return getDanios(vehiculo.getDanosPreexistentes(), TIPO_DANIO_PREEXISTENTE);
    }

    private Collection<Danio> getDanios(List<PiezaVehiculo> piezas, CatalogoAmis tipoDanio) {
        Set<Danio> result = new HashSet<>();
        if (piezas != null && !piezas.isEmpty()) {
            for (PiezaVehiculo pieza : piezas) {
                Danio danio = new Danio();
                ParteAfectada parte = getCatalogoOrElse(siniestrosInteractor.getParteAfectada(pieza.getAreaAfectada(), pieza.getParteAfectada()));
                if (parte != null) {
                    danio.setArea(getCuadranteById(parte.getAreaAmis()));
                    danio.setTipoDanio(tipoDanio);
                    danio.setParteDaniada(new CatalogoAmis(parte.getClaveAmis()));
                    if (!CollectionsUtils.anyMatch(result, danio::equals)) {
                        result.add(danio);
                    }
                }
            }
        }
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, Danios modulo) {
        TerceroAuto tercero = getTerceroAuto(ajuste, modulo.getFolio());
        if (tercero.getVehiculo() == null) {
            tercero.setVehiculo(new VehiculoTercero());
        }
        if (tercero.getVehiculo().getVolante() == null) {
            tercero.getVehiculo().setModificado(true);
            putPrimerImpacto(tercero.getVehiculo(), modulo.getListaDanios());
            putDaniosConsecuencia(tercero.getVehiculo(), modulo.getListaDanios());
            putDaniosPreexistentes(tercero.getVehiculo(), modulo.getListaDanios());
            terceroAutoRepository.saveByAjuste(ajuste, tercero);
        }
    }

    private void putPrimerImpacto(VehiculoTercero vehiculo, List<Danio> danios) {
        String tipo = TIPO_PRIMER_IMPACTO.getClave();
        Danio danio = CollectionsUtils.firstMatch(danios, d -> tipo.equals(d.getTipoDanio().getClave()));
        if (danio != null) {
            List<ImpactoVehiculoAmis> impactos;
            try {
                impactos = RXUtils.sync(siniestrosInteractor.getImpactosVehiculo());
            } catch (Exception ex) {
                impactos = new ArrayList<>();
            }
            ImpactoVehiculoAmis impacto = CollectionsUtils
                    .firstMatch(impactos, it -> it.getAreaAmis().toString().equals(danio.getArea().getClave()));
            if (impacto != null) {
                vehiculo.setPrimerImpacto(impacto.getId());
            }
        }
    }

    private void putDaniosConsecuencia(VehiculoTercero vehiculo, List<Danio> danios) {
        List<PiezaVehiculo> piezas = getPiezas(danios, TIPO_DANIO_CONSECUENCIA.getClave());
        vehiculo.setDanoConsecuencia(!piezas.isEmpty());
        vehiculo.setDanos(piezas);
    }

    private void putDaniosPreexistentes(VehiculoTercero vehiculo, List<Danio> danios) {
        vehiculo.setDanosPreexistentes(getPiezas(danios, TIPO_DANIO_PREEXISTENTE.getClave()));
    }

    private List<PiezaVehiculo> getPiezas(List<Danio> danios, String tipo) {
        Collection<Danio> filtered = CollectionsUtils.filter(danios, danio -> tipo.equals(danio.getTipoDanio().getClave()));
        List<PiezaVehiculo> result = new ArrayList<>(filtered.size());
        for (Danio danio : filtered) {
            PiezaVehiculo pieza = new PiezaVehiculo();
            ParteAfectada parte = getCatalogoOrElse(siniestrosInteractor.getParteAfectadaAmis(danio.getParteDaniada().getClave()));
            if (parte != null) {
                pieza.setParteAfectada(parte.getId());
                pieza.setAreaAfectada(parte.getArea().getId());
                if (!CollectionsUtils.anyMatch(result, pieza::equals)) {
                    result.add(pieza);
                }
            }
        }
        return result;
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }

}