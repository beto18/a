package com.gnp.ajustadormovil.android.interactors.amis.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.Certificado;
import com.gnp.ajustadormovil.android.models.amis.CertificadoCompania;
import com.gnp.ajustadormovil.android.models.amis.FirmaDigital;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.codec.DecodeEngine;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrNotFoundException;
import com.google.gson.Gson;

import java.security.PrivateKey;
import java.security.PublicKey;

import io.reactivex.Single;

public class FirmaDigitalHelper {

    private final String TAG = FirmaDigitalHelper.class.getSimpleName();

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private static final CatalogoAmis COMPANIA_GNP = new CatalogoAmis("31");

    private final AmisInteractor amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();

    private static final String MODULO = "15";

    private final Gson parser = new Gson();

    private static FirmaDigitalHelper instance;

    public static synchronized FirmaDigitalHelper instance(){
        if (instance == null) {
            instance = new FirmaDigitalHelper();
        }
        return instance;
    }


    public Single<FirmaDigital> get(String ajuste, long id) {
        return Single.create(e -> {
            FirmaDigital firmaDigital = new FirmaDigital();
            if(terceroAutoRepository.findByAjusteAndId(ajuste, id).getFolioAmis() != null) {
                firmaDigital.setIdAseguradora(COMPANIA_GNP.getClave());
                firmaDigital.setValeDigital(getValeDigital(ajuste, id));
                firmaDigital.setFirmaDigital(getFirmaDigital(firmaDigital.getValeDigital()));
            }
            e.onSuccess(firmaDigital);
        });

    }


    public Single<String> encriptar(FirmaDigital firmaDigital, String folio) {
        return Single.create(e -> {
            String jsonEncriptado = DecodeEngine.encode(folio, parser.toJson(firmaDigital));
            StringBuilder qr = new StringBuilder(folio);
            qr.append("|");
            qr.append(MODULO);
            qr.append("|");
            qr.append(jsonEncriptado);

            e.onSuccess(qr.toString());
        });
    }

    public Single<FirmaDigital> desencriptarModulo(String modulo) {
        return Single.create(e -> {
            if (!modulo.isEmpty()) {
                String[] qr = modulo.split("\\|");
                String json = DecodeEngine.decode(qr[0], qr[2]);
                Log.d(TAG, "MODULO: " + modulo);
                FirmaDigital firmaDigital = parser.fromJson(json,FirmaDigital.class);
                e.onSuccess(firmaDigital);
            } else {
                e.tryOnError(new QrNotFoundException());
            }
        });
    }


    private String getValeDigital(String ajuste, long id) throws Exception {
        return putData(ajuste, getContraparte(ajuste, id), id);
    }



    private FolioContraparteAmis getContraparte(String ajuste, long id) {
        TerceroAuto tercero = terceroAutoRepository.findByAjusteAndId(ajuste, id);
        Amis amis = amisRepository.findByAjuste(ajuste);
        FolioContraparteAmis contraparte = null;

        for (FolioContraparteAmis c : amis.getContrapartes()) {
            if (c.getFolioInicial().equals(tercero.getFolioAmis())) {
                contraparte = c;
            }
        }

        return contraparte;
    }

    private String putData(String ajuste, FolioContraparteAmis contraparte, long id) throws Exception {
        Ajuste a = ajusteRepository.findById(ajuste);
        Amis amis = amisRepository.findByAjuste(ajuste);
        Certificado certificadoResponsable = RXUtils.sync(amisInteractor.getCertificadoPropio());
        CertificadoCompania certificadoAfectado = RXUtils.sync(amisInteractor.getCertificado(Long.valueOf(contraparte.getCompania())));
        String folio = amis.getFolio().getFolio();
        TerceroAuto tercero = terceroAutoRepository.findByAjusteAndId(ajuste, id);
        StringBuilder valeDigital = new StringBuilder();
        valeDigital.append(folio);
        valeDigital.append(contraparte.getFolio().substring(contraparte.getFolio().length() - 1));
        valeDigital.append("|");
        valeDigital.append(a.getSiniestro().getIdSiniestro());
        valeDigital.append("|");
        valeDigital.append(contraparte.getSiniestro());
        valeDigital.append("|");
        valeDigital.append(COMPANIA_GNP.getClave());
        valeDigital.append("|");
        valeDigital.append(contraparte.getCompania());
        valeDigital.append("|");
        valeDigital.append(contraparte.getFechaAtencion());
        valeDigital.append("|");
        valeDigital.append(a.getSiniestro().getPoliza().getNumero());
        valeDigital.append("|");
        valeDigital.append(contraparte.getPoliza());
        valeDigital.append("|");
        valeDigital.append(DecodeEngine.getSerialNumber(certificadoResponsable.getCertificado()));
        valeDigital.append("|");
        valeDigital.append(DecodeEngine.getSerialNumber(certificadoAfectado.getCertificado()));
        valeDigital.append(getPasesMedicos(tercero));
        return valeDigital.toString();

    }


    private String getPasesMedicos(TerceroAuto tercero) {
        StringBuilder folioMedico = new StringBuilder();
        if (tercero.getConductor().getVolante()!= null){
            folioMedico.append("|");
            folioMedico.append(tercero.getConductor().getVolante().getFolio());
        }
        for (Ocupante ocupante : tercero.getOcupantes()){
            if (folioMedico.length() > 0) {
                folioMedico.append("|");
            }
            if (ocupante.getVolante()!= null){
                folioMedico.append(ocupante.getVolante().getFolio());
            }
        }

        return folioMedico.toString();
    }

    private String getFirmaDigital(String valeDigital) throws Exception {
        String firmaDigial = "";
        String key = RXUtils.sync(amisInteractor.getCertificadoPropio()).getLlavePrivada();
        PrivateKey privateKey = DecodeEngine.getPrivateKeyFromBase64(key);
        firmaDigial = DecodeEngine.sign(privateKey, valeDigital);
        return firmaDigial;
    }

    public boolean validarFirmaDigital(FirmaDigital firmaDigital) {
        try {
            CertificadoCompania certificado = RXUtils.sync(amisInteractor.getCertificado(Long.valueOf(firmaDigital.getIdAseguradora())));
            PublicKey publicKey = DecodeEngine.getPublicKeyFromBase64(certificado.getLlavePublica());
            return DecodeEngine.verifySignature(publicKey, firmaDigital.getValeDigital(), firmaDigital.getFirmaDigital());
        } catch (Exception ex) {
            Log.w(TAG, "Error al validar la firma digital", ex);
            return false;
        }
    }

}
