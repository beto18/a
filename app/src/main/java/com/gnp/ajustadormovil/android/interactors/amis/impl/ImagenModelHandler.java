package com.gnp.ajustadormovil.android.interactors.amis.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.models.amis.ImagenAmis;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.media.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImagenModelHandler extends ModelHandler<ImagenAmis> {

    private static final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private static final String MODULO = "99";

    private static final List<String> CATEGORIA_VEHICULO = Arrays.asList("1", "2", "4");

    @Override
    public List<ImagenAmis> get(String ajuste) {
        return Collections.EMPTY_LIST;
    }

    @Override
    protected void saveModulo(String ajuste, ImagenAmis modulo) {
        if(CATEGORIA_VEHICULO.contains(modulo.getCategoria().getClave())) {
            TerceroAuto tercero = getTerceroAuto(ajuste, modulo.getFolio());
            if (tercero.getVehiculo() != null && !CollectionsUtils.anyMatch(tercero.getVehiculo().getFotografias(), f -> modulo.getUrlFoto().equals(f.getAmisUrl()))) {
                try {
                    tercero.getVehiculo().getFotografias().add(getFotografia(modulo.getUrlFoto()));
                } catch (IOException ex) {
                    Log.e(TAG, "Error al descargar fotografía: " + modulo.getUrlFoto(), ex);
                }
            }
            terceroAutoRepository.saveByAjuste(ajuste, tercero);
        }
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }

    private Fotografia getFotografia(String url) throws IOException {
        synchronized (Fotografia.class) {
            Fotografia fotografia = new Fotografia();
            fotografia.setAgregado(true);
            fotografia.setEliminado(false);
            fotografia.setId(generateLocalId());
            fotografia.setLocalId(fotografia.getId());
            File file = new File(Utils.TEMP_FOLDER + File.separator + fotografia.getId() + ".jpg");
            if(!file.exists()) {
                file.createNewFile();
            }
            FileUtils.copy(new URL(url).openStream(), new FileOutputStream(file));
            fotografia.setUrl(file.getAbsolutePath());
            fotografia.setLocalUrl(file.getAbsolutePath());
            fotografia.setAmisUrl(url);
           return fotografia;
        }
    }

}
