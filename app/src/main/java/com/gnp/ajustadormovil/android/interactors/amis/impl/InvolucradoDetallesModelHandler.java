package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.Direccion;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoDetalles;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoGeneral;
import com.gnp.ajustadormovil.android.models.amis.PersonaAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.List;

public abstract class InvolucradoDetallesModelHandler extends InvolucradoModelHandler<InvolucradoDetalles> {

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private final TerceroPersonaRepository terceroPersonaRepository = RepositoryFactory.getTerceroPersonaRepository();

    private static final byte SECCION = 19;

    @Override
    protected Byte getSeccion() {
        return SECCION;
    }

    @Override
    public List<InvolucradoDetalles> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos del involucrado");
        List<InvolucradoDetalles> result = new ArrayList<>();
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        List<TerceroPersona> tercerosPersonas = terceroPersonaRepository.findByAjuste(ajuste);
        if (asegurado != null) {
            if (asegurado.getConductor() != null) {
                InvolucradoDetalles involucrado = getInvolucrado(asegurado.getConductor());
                involucrado.setTipoInvolucrado(TIPO_INVOLUCRADO_CONDUCTOR);
                involucrado.setMismoAsegurado(asegurado.getConductor().getCodigoAfectado() != null);
                result.add(involucrado);
            }
            for (Ocupante ocupante : asegurado.getOcupantes()) {
                InvolucradoDetalles involucrado = getInvolucrado(ocupante);
                involucrado.setTipoInvolucrado(TIPO_INVOLUCRADO_OCUPANTE);
                result.add(involucrado);
            }
        }

        for (TerceroPersona terceroPersona : tercerosPersonas) {
            InvolucradoDetalles involucrado = getInvolucrado(terceroPersona.getPersona());
            involucrado.setTipoInvolucrado(TIPO_INVOLUCRADO_PERSONA);
            result.add(involucrado);
        }

        putIdSeccion(result);
        return result;
    }

    private InvolucradoDetalles getInvolucrado(Afectado afectado) {
        InvolucradoDetalles involucrado = new InvolucradoDetalles();
        involucrado.setIdInvolucrado(afectado.getInvolucradoId());
        involucrado.setMismoAsegurado(false);
        involucrado.setPersona(getPersona(afectado));
        involucrado.setDireccion(getDireccionDefault());
        return involucrado;
    }

    private PersonaAmis getPersona(Afectado afectado) {
        PersonaAmis result = new PersonaAmis();
        String[] nombres = afectado.getNombre().split(" ");
        result.setNombre(nombres[0]);
        if (nombres.length > 1) {
            result.setSegundoNombre(nombres[1]);
        }
        result.setApellidoPaterno(afectado.getApellidoPaterno());
        result.setApellidoMaterno(afectado.getApellidoMaterno());
        CatalogoClaveAmis genero = this.getCatalogoClaveAmis(siniestrosInteractor.getGenero(afectado.getSexo()));
        result.setGenero(new CatalogoAmis(genero.getClaveAmis()));
        result.setEdad(String.valueOf(DateUtils.yearsAgo(afectado.getFechaNacimiento())));
        result.setId(null);
        result.setRazonSocial(null);
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, InvolucradoDetalles modulo) {
        TerceroAuto tercero = getTerceroAuto(ajuste, modulo.getFolio());
        if (TIPO_INVOLUCRADO_CONDUCTOR.getClave().equals(modulo.getTipoInvolucrado().getClave())) {
            putConductor(tercero, modulo);
        } else if (TIPO_INVOLUCRADO_OCUPANTE.getClave().equals(modulo.getTipoInvolucrado().getClave())) {
            putOcupante(tercero, modulo);
        } else if (TIPO_INVOLUCRADO_PERSONA.getClave().equals(modulo.getTipoInvolucrado().getClave())) {
            TerceroPersona persona = getTerceroPersona(ajuste, modulo.getIdInvolucrado());
            persona.setDescripcion(modulo.getPersona().getNombre() + " " + modulo.getPersona().getApellidoPaterno() + " " + modulo.getPersona().getApellidoMaterno());
            putPeaton(persona, modulo);
            terceroPersonaRepository.saveByAjuste(ajuste, persona);
        }

        terceroAutoRepository.saveByAjuste(ajuste, tercero);
    }

    private void putConductor(TerceroAuto tercero, InvolucradoDetalles modulo) {
        if (tercero.getConductor() == null) {
            tercero.setConductor(new Persona());
            tercero.getConductor().setId(generateLocalId());
        }
        if (tercero.getConductor().getVolante() == null) {
            tercero.getConductor().setModificado(true);
            putAfectado(tercero.getConductor(), modulo);
        }
    }

    private void putOcupante(TerceroAuto tercero, InvolucradoDetalles modulo) {
        Ocupante ocupante = findOcupante(tercero, modulo.getIdInvolucrado());
        if (ocupante == null) {
            ocupante = new Ocupante();
            ocupante.setId(generateLocalId());
            ocupante.setLocalId(ocupante.getId());
            ocupante.setAgregado(true);
            ocupante.setEliminado(false);
            ocupante.setModificado(false);
            tercero.getOcupantes().add(ocupante);
        }
        if (ocupante.getVolante() == null) {
            ocupante.setModificado(!ocupante.isAgregado());
            putAfectado(ocupante, modulo);
        }
    }

    private void putPeaton(TerceroPersona peaton, InvolucradoDetalles modulo) {
        if (peaton.getPersona().getVolante() == null) {
            peaton.getPersona().setModificado(!peaton.isAgregado());
            putAfectado(peaton.getPersona(), modulo);
        }
    }

    private void putAfectado(Afectado afectado, InvolucradoDetalles modulo) {
        afectado.setInvolucradoId(modulo.getIdInvolucrado());
        afectado.getCorreos().clear();
        putPersona(afectado, modulo.getPersona());
    }

    private void putPersona(Afectado afectado, PersonaAmis persona) {
        if (!"".equals(persona.getNombre())) {
            afectado.setNombre(persona.getNombre());
        }
        if (persona.getSegundoNombre() != null && !"".equals(persona.getSegundoNombre())) {
            afectado.setNombre(afectado.getNombre() + " " + persona.getSegundoNombre());
        }
        if (!"".equals(persona.getApellidoPaterno())) {
            afectado.setApellidoPaterno(persona.getApellidoPaterno());
        }
        if (!"".equals(persona.getApellidoMaterno())) {
            afectado.setApellidoMaterno(persona.getApellidoMaterno());
        }
        CatalogoClaveAmis genero = getCatalogoClaveAmis(siniestrosInteractor.getGeneroAmis(persona.getGenero().getClave()));
        if (!"".equals(genero.getClave())) {
            afectado.setSexo(genero.getClave());
        }
    }

    protected abstract Direccion getDireccionDefault();

}
