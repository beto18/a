package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.Direccion;

public class InvolucradoDetallesSipacModelHandler extends InvolucradoDetallesModelHandler {

    private static final CatalogoAmis COLONIA_DEFAULT = new CatalogoAmis("9999999999");

    private static final CatalogoAmis MUNICIPIO_DEFAULT = new CatalogoAmis("9999");

    private static final CatalogoAmis ESTADO_DEFAULT = new CatalogoAmis("99");

    private static final CatalogoAmis TIPO_VIALIDAD_DEFAULT = new CatalogoAmis("1");

    private static final String CALLE_DEFAULT = "NA";

    private static final String CP_DEFAULT = "00000";

    private static final String ENTRE_CALLES_DEFAULT = "DESCONOCIDO";

    private static final String NUM_EXTERIOR_DEFAULT = "0";

    @Override
    protected Direccion getDireccionDefault(){
        Direccion direccion = new Direccion();
        direccion.setCalle(CALLE_DEFAULT);
        direccion.setCodigoPostal(CP_DEFAULT);
        direccion.setColonia(COLONIA_DEFAULT);
        direccion.setDelegacionMunicipio(MUNICIPIO_DEFAULT);
        direccion.setEntreCalles1(ENTRE_CALLES_DEFAULT);
        direccion.setEntreCalles2(ENTRE_CALLES_DEFAULT);
        direccion.setEstado(ESTADO_DEFAULT);
        direccion.setNumeroExterior(NUM_EXTERIOR_DEFAULT);
        direccion.setTipoVialidad(TIPO_VIALIDAD_DEFAULT);
        return direccion;
    }
}
