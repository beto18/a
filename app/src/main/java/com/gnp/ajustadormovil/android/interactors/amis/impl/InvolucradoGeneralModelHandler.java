package com.gnp.ajustadormovil.android.interactors.amis.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoGeneral;
import com.gnp.ajustadormovil.android.models.amis.TelefonoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoTipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.TipoLicencia;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.time.chrono.ThaiBuddhistEra;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmObjectSchema;

public class InvolucradoGeneralModelHandler extends InvolucradoModelHandler<InvolucradoGeneral> {

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private final TerceroPersonaRepository terceroPersonaRepository = RepositoryFactory.getTerceroPersonaRepository();

    private static final byte SECCION = 20;

    private static final int VERSION_DEFAULT = 1;

    private static final String IDENTIFICACION_LICENCIA = "LC";

    private static final CatalogoAmis DEFAULT_ESTADO_LICENCIA = new CatalogoAmis("35");

    private static final CatalogoAmis DEFAULT_TIPO_LICENCIA = new CatalogoAmis("67");

    private static final String DEFAULT_NUMERO_LICENCIA = "NA";


    @Override
    protected Byte getSeccion() {
        return SECCION;
    }

    @Override
    public List<InvolucradoGeneral> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos del involucrado");
        List<InvolucradoGeneral> result = new ArrayList<>();
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        List<TerceroPersona> tercerosPersonas = terceroPersonaRepository.findByAjuste(ajuste);
        if (asegurado != null) {
            if (asegurado.getConductor() != null) {
                InvolucradoGeneral involucrado = getInvolucrado(asegurado.getConductor());
                result.add(involucrado);
            }
            for (Ocupante ocupante : asegurado.getOcupantes()) {
                InvolucradoGeneral involucrado = getInvolucrado(ocupante);
                result.add(involucrado);
            }
        }

            for (TerceroPersona terceroPersona : tercerosPersonas) {
                InvolucradoGeneral involucrado = getInvolucrado(terceroPersona.getPersona());
                result.add(involucrado);
            }


        putIdSeccion(result);
        return result;
    }

    private InvolucradoGeneral getInvolucrado(Afectado afectado) {
        InvolucradoGeneral involucrado = new InvolucradoGeneral();
        involucrado.setIdInvolucrado(afectado.getInvolucradoId());
        involucrado.setVersionCtdpCompania(VERSION_DEFAULT);
        involucrado.setVersionCtdpGeneral(VERSION_DEFAULT);
        if (afectado.getAutorizaTratamiento()) {
            involucrado.setVersionAvisoPrivacidadCompania(VERSION_DEFAULT);
            involucrado.setVersionAvisoPrivacidadGeneral(VERSION_DEFAULT);
        }
        involucrado.setAvisoPrivacidad(afectado.getAutorizaTratamiento());
        involucrado.setTratamientoDatosPersonales(afectado.getAutorizaTratamiento());
        involucrado.setCorreoElectronico(afectado.getCorreos().get(0));
        putLicencia(afectado, involucrado);
        involucrado.setTelefono(getTelefono(afectado));
        return involucrado;
    }

    private TelefonoAmis getTelefono(Afectado afectado) {
        Telefono telefono = afectado.getTelefonos().get(0);
        TelefonoAmis result = new TelefonoAmis();
        result.setLada("");
        result.setNumero(telefono.getTelefono());
        CatalogoClaveAmis tipo = this.getCatalogoClaveAmis(siniestrosInteractor.getTipoTelefono(telefono.getTipo()));
        if (tipo != null){
            result.setTipoTelefono(new CatalogoAmis(tipo.getClaveAmis()));
        }
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, InvolucradoGeneral modulo) {
        TerceroAuto tercero = getTerceroAuto(ajuste, modulo.getFolio());
        List<TerceroPersona> terceroPersonas = terceroPersonaRepository.findByAjuste(ajuste);

        if (tercero.getConductor() != null && modulo.getIdInvolucrado().equals(tercero.getConductor().getInvolucradoId()) && tercero.getConductor().getVolante() == null) {
            putAfectado(tercero.getConductor(), modulo);
            tercero.getConductor().setModificado(true);
        }

        Ocupante ocupante = findOcupante(tercero, modulo.getIdInvolucrado());
        if (ocupante != null && ocupante.getVolante() == null) {
            putAfectado(ocupante, modulo);
            ocupante.setModificado(!ocupante.isAgregado());
        }

        TerceroPersona terceroPersona = findPersona(terceroPersonas,modulo.getIdInvolucrado());

        if (terceroPersona != null && terceroPersona.getPersona().getVolante() == null) {
            putAfectado(terceroPersona.getPersona(), modulo);
            terceroPersona.setModificado(!terceroPersona.isAgregado());
            terceroPersonaRepository.saveByAjuste(ajuste, terceroPersona);
        }

        Log.d(TAG,"modulo" + modulo);
        terceroAutoRepository.saveByAjuste(ajuste, tercero);
    }

    private void putAfectado(Afectado afectado, InvolucradoGeneral modulo) {
        afectado.setInvolucradoId(modulo.getIdInvolucrado());
        afectado.setAutorizaTratamiento(modulo.getTratamientoDatosPersonales());
        afectado.getCorreos().clear();
        if (modulo.getCorreoElectronico() != null && !modulo.getCorreoElectronico().isEmpty()) {
            afectado.getCorreos().add(modulo.getCorreoElectronico());
        }
        putLicenciaAfectado(afectado, modulo);
        putTelefono(afectado, modulo.getTelefono());
    }

    private void putLicencia(Afectado afectado, InvolucradoGeneral involucrado) {
        if (IDENTIFICACION_LICENCIA.equals(afectado.getTipoIdentificacion())) {
            involucrado.setFechaVigenciaLicencia(DateUtils.dateIso(afectado.getFechaFinLicencia()));
            involucrado.setLicencia(afectado.getNumeroIdentificacion());
            TipoLicencia tipoLicencia = getCatalogoOrElse(siniestrosInteractor.getTipoLicencia(afectado.getTipoLicencia()));
            if (tipoLicencia != null) {
                involucrado.setTipoLicencia(new CatalogoAmis(tipoLicencia.getClave()));
                if (tipoLicencia.getEstados() != null) {
                    for (EstadoTipoLicencia estado : tipoLicencia.getEstados()) {
                        if (estado.getEstado().equals(afectado.getEstadoLicencia())) {
                            involucrado.setEstadoLicencia(new CatalogoAmis(estado.getEstadoAmis()));
                        }
                    }
                }
            }
        } else {
            involucrado.setTipoLicencia(DEFAULT_TIPO_LICENCIA);
            involucrado.setEstadoLicencia(DEFAULT_ESTADO_LICENCIA);
            involucrado.setFechaVigenciaLicencia(DateUtils.dateIso(new Date()));
            involucrado.setLicencia(DEFAULT_NUMERO_LICENCIA);
        }
    }

    private void putLicenciaAfectado(Afectado afectado, InvolucradoGeneral modulo) {
        if (modulo.getLicencia() != null && !"".equals(modulo.getLicencia())) {
            afectado.setTipoIdentificacion(IDENTIFICACION_LICENCIA);
            if (modulo.getFechaVigenciaLicencia() != null) {
                afectado.setFechaFinLicencia(DateUtils.dateIso(modulo.getFechaVigenciaLicencia()));
            }
            afectado.setNumeroIdentificacion(modulo.getLicencia());
            if (modulo.getTipoLicencia() != null) {
                TipoLicencia tipoLicencia = getCatalogoOrElse(siniestrosInteractor.getTipoLicencia(modulo.getTipoLicencia().getClave()));
                if (tipoLicencia != null) {
                    afectado.setTipoLicencia(tipoLicencia.getClave());
                    if(tipoLicencia.getEstados() != null) {
                        for (EstadoTipoLicencia estado : tipoLicencia.getEstados()) {
                            if (estado.getEstadoAmis().equals(modulo.getEstadoLicencia().getClave())) {
                                afectado.setEstadoLicencia(estado.getEstado());
                            }
                        }
                    }
                }
            }
        }
    }

    private void putTelefono(Afectado afectado, TelefonoAmis telefono) {
        afectado.getTelefonos().clear();
        if (telefono != null) {
            Telefono tel = new Telefono();
            tel.setTelefono(telefono.getNumero());
            CatalogoClaveAmis tipo = getCatalogoClaveAmis(siniestrosInteractor.getTipoTelefonoAmis(telefono.getTipoTelefono().getClave()));
            if (!"".equals(tipo.getClave())) {
                tel.setTipo(tipo.getClave());
            }
            afectado.getTelefonos().add(tel);
        }
    }

}
