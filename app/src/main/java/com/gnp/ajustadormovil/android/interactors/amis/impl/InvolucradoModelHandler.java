package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.InvolucradoModulo;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.codec.DecodeEngine;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.Gson;

import java.util.List;

public abstract class InvolucradoModelHandler<T extends Modulo> extends ModelHandler<T> {

    private static final String MODULO = "04";

    private Gson gson = new Gson();

    protected static final CatalogoAmis TIPO_INVOLUCRADO_CONDUCTOR =
            new CatalogoAmis("1");

    protected static final CatalogoAmis TIPO_INVOLUCRADO_OCUPANTE =
            new CatalogoAmis("2");

    protected static final CatalogoAmis TIPO_INVOLUCRADO_PERSONA =
            new CatalogoAmis("3");

    protected Ocupante findOcupante(TerceroAuto tercero, String idInvolucrado) {
        return CollectionsUtils.firstMatch(tercero.getOcupantes(), o -> idInvolucrado.equals(o.getInvolucradoId()));
    }

    protected TerceroPersona findPersona(List<TerceroPersona> personas, String idInvolucrado) {
        return CollectionsUtils.firstMatch(personas, p -> idInvolucrado.equals(p.getPersona().getInvolucradoId()));
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }

    protected abstract Byte getSeccion();

    @Override
    public boolean isModuloType(String value) {
        if(super.isModuloType(value)) {
            String [] qr =  value.split("\\|");
            String json = DecodeEngine.decode(qr[0], qr[2]);
            InvolucradoModulo modulo = gson.fromJson(json, InvolucradoModulo.class);
            return getSeccion().equals(modulo.getIdSeccion());
        }
        return false;
    }

    protected void putIdSeccion(List<? extends InvolucradoModulo> modulos) {
        if(modulos != null) {
            for (InvolucradoModulo modulo : modulos) {
                modulo.setIdSeccion(getSeccion());
            }
        }
    }

}
