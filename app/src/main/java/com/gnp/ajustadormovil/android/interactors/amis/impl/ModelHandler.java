package com.gnp.ajustadormovil.android.interactors.amis.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.models.amis.Modulo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.util.List;

import io.reactivex.Single;

public abstract class ModelHandler<T extends Modulo> {

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final TerceroPersonaRepository terceroPersonaRepository = RepositoryFactory.getTerceroPersonaRepository();

    private static final CatalogoClaveAmis CATALOGO_CLAVE_AMIS_EMPTY;

    private static final CatalogoIdAmis CATALOGO_ID_AMIS_EMPTY;

    static {
        CATALOGO_CLAVE_AMIS_EMPTY = new CatalogoClaveAmis();
        CATALOGO_CLAVE_AMIS_EMPTY.setClave("");
        CATALOGO_CLAVE_AMIS_EMPTY.setClaveAmis("0");
        CATALOGO_CLAVE_AMIS_EMPTY.setNombre("");
        CATALOGO_ID_AMIS_EMPTY = new CatalogoIdAmis();
        CATALOGO_ID_AMIS_EMPTY.setId(0L);
        CATALOGO_ID_AMIS_EMPTY.setClaveAmis("0");
        CATALOGO_ID_AMIS_EMPTY.setNombre("");
    }

    protected final String TAG = this.getClass().getSimpleName();

    public abstract List<T> get(String ajuste);

    public void save(String ajuste, Modulo modulo) {
        saveModulo(ajuste, (T) modulo);
    }

    protected abstract void saveModulo(String ajuste, T modulo);

    public abstract String getIdModulo();

    protected final CatalogoClaveAmis getCatalogoClaveAmis(Single<CatalogoClaveAmis> single){
       return getCatalogoOrElse(single, CATALOGO_CLAVE_AMIS_EMPTY);
    }

    protected final CatalogoIdAmis getCatalogoIdAmis(Single<CatalogoIdAmis> single){
        return getCatalogoOrElse(single, CATALOGO_ID_AMIS_EMPTY);
    }

    protected final <T> T getCatalogoOrElse(Single<T> single, T defaultValue) {
        T result;
        try {
           result = RXUtils.sync(single);
        } catch (Exception ex) {
           Log.w(TAG, "Error al obtener el catálogo", ex);
           result = defaultValue;
        }
        return result;
    }

    protected final <T> T getCatalogoOrElse(Single<T> single) {
        return getCatalogoOrElse(single, null);
    }

    protected TerceroAuto getTerceroAuto(String ajuste, String folioAmis) {
        List<TerceroAuto> terceros = terceroAutoRepository.findByAjuste(ajuste);
        TerceroAuto tercero = null;
        for(TerceroAuto t : terceros) {
            if(folioAmis.equals(t.getFolioAmis())) {
                tercero = t;
            }
        }
        if(tercero == null) {
            tercero = new TerceroAuto();
            tercero.setFolioAmis(folioAmis);
            tercero.setId(generateLocalId());
            tercero.setLocalId(tercero.getId());
            tercero.setDescripcion("Sin información del tercero");
            tercero.setAgregado(true);
            tercero.setEliminado(false);
        }
        return tercero;
    }


    protected TerceroPersona getTerceroPersona(String ajuste, String idInvolucrado) {
        List<TerceroPersona> terceros = terceroPersonaRepository.findByAjuste(ajuste);
        TerceroPersona tercero = null;

        for(TerceroPersona t : terceros) {
            if(idInvolucrado.equals(t.getPersona().getInvolucradoId())) {
                tercero = t;
            }
        }
        if(tercero == null) {
            tercero = new TerceroPersona();
            tercero.setPersona(new Persona());
            tercero.setId(generateLocalId());
            tercero.setLocalId(tercero.getId());
            tercero.setDescripcion("Sin información del tercero");
            tercero.setAgregado(true);
            tercero.setEliminado(false);
        }
        return tercero;
    }

    protected synchronized long generateLocalId() {
        try {
            Thread.sleep(1L);
        } catch (InterruptedException e) {
            Log.w(TAG, "Delay interrumpido", e);
        }
        return -System.currentTimeMillis();
    }

    public boolean isModuloType(String value) {
        String [] values = value.split("\\|");
        if(values.length > 1) {
            return Byte.parseByte(values[1]) == Byte.parseByte(getIdModulo());
        }
        return false;
    }

}
