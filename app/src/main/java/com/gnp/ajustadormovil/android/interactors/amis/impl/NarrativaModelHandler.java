package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.models.amis.Narrativa;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.List;

public class NarrativaModelHandler extends InvolucradoModelHandler<Narrativa> {

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private static final byte SECCION = 22;

    @Override
    protected Byte getSeccion() {
        return SECCION;
    }

    @Override
    public List<Narrativa> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos de la narrativa");
        List<Narrativa> result = new ArrayList<>(1);
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        Ajuste a = ajusteRepository.findById(ajuste);
        if (asegurado != null && asegurado.getConductor() != null) {
            Narrativa narrativa = new Narrativa();
            narrativa.setIdInvolucrado(asegurado.getConductor().getInvolucradoId());
            narrativa.setDeclaracionBajoProtesta(true);
            narrativa.setDeclaracionBajoProtestaVersionGeneral(1);
            narrativa.setNarracion(a.getNarrativa());
            result.add(narrativa);
        }
        putIdSeccion(result);
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, Narrativa modulo) {
    }

}
