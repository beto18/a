package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.PaseMedico;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.List;

public class PaseMedicoModelHandler extends InvolucradoModelHandler<PaseMedico> {

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final TerceroPersonaRepository terceroPersonaRepository = RepositoryFactory.getTerceroPersonaRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private static final byte SECCION = 21;

    @Override
    protected Byte getSeccion() {
        return SECCION;
    }

    @Override
    public List<PaseMedico> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos del pase médico");
        List<PaseMedico> result = new ArrayList<>();
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        List<TerceroPersona> terceroPersonas = terceroPersonaRepository.findByAjuste(ajuste);
        if (asegurado != null) {
            PaseMedico paseMedico = getPaseMedico(asegurado.getConductor());
            if (paseMedico != null) {
                result.add(paseMedico);
            }
            for (Ocupante ocupante : asegurado.getOcupantes()) {
                PaseMedico paseMedicoOcupante = getPaseMedico(ocupante);
                if (paseMedicoOcupante != null) {
                    result.add(paseMedicoOcupante);
                }
            }

            for (TerceroPersona tercero : terceroPersonas) {
                PaseMedico paseMedicoPersona = getPaseMedico(tercero.getPersona());
                if (paseMedicoPersona != null) {
                    result.add(paseMedicoPersona);
                }
            }

        }
        putIdSeccion(result);
        return result;
    }

    private PaseMedico getPaseMedico(Afectado afectado) {
        PaseMedico result = null;
        if (CondicionPersona.LESIONADO.equals(afectado.getCondicion())) {
            result = new PaseMedico();
            if (afectado.getVolante() != null) {
                result.setFolioPaseMedico(afectado.getVolante().getFolio());
            } else {
                result.setFolioPaseMedico("");
            }
            result.setIdInvolucrado(afectado.getInvolucradoId());
            result.setMedicalPassUpdated(false);
            result.setPaseMedico(afectado.getVolante() != null);
            result.setLesiones(getLesionesAmis(afectado.getLesiones()));
        }
        return result;
    }

    private List<CatalogoAmis> getLesionesAmis(List<Long> lesiones) {
        List<CatalogoAmis> result = new ArrayList<>(lesiones.size());

        for (Long id : lesiones) {
            CatalogoIdAmis lesion = getCatalogoIdAmis(siniestrosInteractor.getParteLesionada(id));
            if (!CollectionsUtils.anyMatch(result, new CatalogoAmis(lesion.getClaveAmis())::equals)) {
                result.add(new CatalogoAmis(lesion.getClaveAmis()));
            }
        }
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, PaseMedico modulo) {
        TerceroAuto tercero = getTerceroAuto(ajuste, modulo.getFolio());
        if (tercero.getConductor() != null && modulo.getIdInvolucrado().equals(tercero.getConductor().getInvolucradoId()) && tercero.getConductor().getVolante() == null) {
            putAfectado(tercero.getConductor(), modulo);
            tercero.getConductor().setModificado(true);
        }

        Ocupante ocupante = findOcupante(tercero, modulo.getIdInvolucrado());
        if (ocupante != null && ocupante.getVolante() == null) {
            putAfectado(ocupante, modulo);
            ocupante.setModificado(!ocupante.isAgregado());
        }

        terceroAutoRepository.saveByAjuste(ajuste, tercero);
    }

    private void putAfectado(Afectado afectado, PaseMedico modulo) {
        List<Long> lesiones = new ArrayList<>();
        if (modulo.getLesiones() != null) {
            for (CatalogoAmis lesion : modulo.getLesiones()) {
                CatalogoIdAmis value = getCatalogoIdAmis(siniestrosInteractor.getParteLesionadaAmis(lesion.getClave()));
                if (!value.getId().equals(0L)) {
                    if (!CollectionsUtils.anyMatch(lesiones, value.getId()::equals)) {
                        lesiones.add(value.getId());
                    }
                }
            }

        }
        afectado.getLesiones().clear();
        afectado.getLesiones().addAll(lesiones);
        if (!afectado.getLesiones().isEmpty()) {
            afectado.setCondicion(CondicionPersona.LESIONADO);
        } else {
            afectado.setCondicion(CondicionPersona.ILESO);
        }
    }


}
