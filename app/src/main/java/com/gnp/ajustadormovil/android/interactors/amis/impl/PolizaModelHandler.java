package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.amis.PersonaAmis;
import com.gnp.ajustadormovil.android.models.amis.PolizaAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Poliza;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PolizaModelHandler extends ModelHandler<PolizaAmis> {

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private static final String MODULO = "02";

    private static final String INCISO_DEFAULT = "1";

    private static final String TIPO_COLISION = "DM";

    private static final CatalogoAmis COMPANIA_GNP = new CatalogoAmis(Constants.AMIS_ID_GNP.toString());

    private static final CatalogoAmis SIN_POLIZA = new CatalogoAmis("2");

    private static final CatalogoAmis CON_POLIZA = new CatalogoAmis("1");

    private enum TipoCobertura {

        RC, GM, RT, DM;

    }

    private static final Map<TipoCobertura, List<String>> COBERTURAS;

    static {
        COBERTURAS = new HashMap<>();
        COBERTURAS.put(TipoCobertura.RC, Arrays.asList(
                "0000001056",
                "0000001458",
                "0000001055",
                "0000000911",
                "0000000912",
                "0000001453",
                "0000000914",
                "0000001273",
                "0000000915"
        ));
        COBERTURAS.put(TipoCobertura.GM, Arrays.asList(
                "0000000907",
                "0000000906",
                "0000001055",
                "0000000911",
                "0000000912",
                "0000001453",
                "0000000914",
                "0000001273",
                "0000000915"
        ));
        COBERTURAS.put(TipoCobertura.RT, Arrays.asList(
                "0000000916",
                "0000001071"
        ));
        COBERTURAS.put(TipoCobertura.DM, Arrays.asList(
                "0000000892",
                "0000001095",
                "0000001289",
                "0000001288",
                "0000000900",
                "0000001065",
                "0000001070"
        ));
    }

    @Override
    public List<PolizaAmis> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos de la póliza");
        List<PolizaAmis> result = new ArrayList<>(1);
        Ajuste a = ajusteRepository.findById(ajuste);
        PolizaAmis poliza = new PolizaAmis();
        poliza.setCompania(COMPANIA_GNP);
        poliza.setInciso(INCISO_DEFAULT);
        putSiniestroData(poliza, a);
        putPolizaData(poliza, a.getSiniestro().getPoliza());
        result.add(poliza);
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, PolizaAmis modulo) {
        Amis amis = amisRepository.findByAjuste(ajuste);
        FolioContraparteAmis contraparte = null;
        for (FolioContraparteAmis c : amis.getContrapartes()) {
            if (c.getFolioInicial().equals(modulo.getFolio())) {
                contraparte = c;
            }
        }
        if (contraparte == null) {
            contraparte = new FolioContraparteAmis();
            contraparte.setFolioInicial(modulo.getFolio());
            amis.getContrapartes().add(contraparte);
        }
        putContraparte(contraparte, modulo);
        amisRepository.save(amis);
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }


    private void putContraparte(FolioContraparteAmis contraparte, PolizaAmis modulo) {
        contraparte.setPoliza(modulo.getNumeroPoliza());
        contraparte.setSiniestro(modulo.getNumeroSiniestro());
        contraparte.setCompania(modulo.getCompania().getClave());
        contraparte.setTipoCobertura(modulo.getTipoCobertura().getClave());
        contraparte.setVigenciaHasta(DateUtils.dateIsoToDate(modulo.getVigenciaHasta()));

    }



    private void putSiniestroData(PolizaAmis polizaAmis, Ajuste ajuste) {
        if (ajuste.getSiniestro().getTipoSiniestro() != null) {
            polizaAmis.setColision(TIPO_COLISION.equals(ajuste.getSiniestro().getTipoSiniestro().getClave()));
            polizaAmis.setTipoCausa(new ArrayList<>());
            polizaAmis.getTipoCausa().add(new CatalogoAmis(ajuste.getSiniestro().getTipoSiniestro().getClaveAmis()));
        }
        polizaAmis.setNumeroReporte(ajuste.getId());
        polizaAmis.setNumeroSiniestro(ajuste.getSiniestro().getId());
    }

    private void putPolizaData(PolizaAmis polizaAmis, Poliza poliza) {
        polizaAmis.setTipoPoliza(poliza != null ? CON_POLIZA : SIN_POLIZA);
        if (poliza != null) {
            polizaAmis.setNumeroPoliza(poliza.getNumero());
            polizaAmis.setVigenciaDel(DateUtils.dateIso(poliza.getFechaVigenciaInicial()));
            polizaAmis.setVigenciaHasta(DateUtils.dateIso(poliza.getFechaVigenciaFinal()));
            polizaAmis.setTipoCobertura(getTipoCobertura(poliza.getCoberturas()));
            if (poliza.getEstatusCobranza() != null) {
                polizaAmis.setTipoCobranza(new CatalogoAmis(poliza.getEstatusCobranza().getClaveAmis()));
            }
            if (poliza.getVehiculo() != null) {
                polizaAmis.setVinEmision(poliza.getVehiculo().getVin());
            }
            if (poliza.getContratante() != null) {
                polizaAmis.setContratante(new PersonaAmis());
                if ("F".equals(poliza.getContratante().getTipo())) {
                    String[] nombres = poliza.getContratante().getNombre().split(" ");
                    polizaAmis.getContratante().setNombre(nombres[0]);
                    if (nombres.length > 1) {
                        polizaAmis.getContratante().setSegundoNombre(nombres[1]);
                    }
                    polizaAmis.getContratante().setApellidoPaterno(poliza.getContratante().getApellidoPaterno());
                    polizaAmis.getContratante().setApellidoMaterno(poliza.getContratante().getApellidoMaterno());
                } else if ("M".equals(poliza.getContratante().getTipo())) {
                    polizaAmis.getContratante().setRazonSocial(poliza.getContratante().getRazonSocial());
                }
            }
        }
    }

    private CatalogoAmis getTipoCobertura(List<PolizaCobertura> coberturas) {
        int clave = 0;
        for (TipoCobertura tipo : TipoCobertura.values()) {
            if (CollectionsUtils.anyMatch(coberturas, cobertura -> COBERTURAS.get(tipo).contains(cobertura.getClave()))) {
                clave ++;
            }
        }
        if (clave == 0) {
            clave = 5;
        }
        return new CatalogoAmis(Integer.toString(clave));
    }


}
