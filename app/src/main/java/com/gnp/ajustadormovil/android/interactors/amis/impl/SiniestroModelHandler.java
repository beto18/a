package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.Carretera;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.Coordenadas;
import com.gnp.ajustadormovil.android.models.amis.Direccion;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.amis.SiniestroAmis;
import com.gnp.ajustadormovil.android.models.amis.UbicacionSiniestroAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ubicacion;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.List;

public abstract class SiniestroModelHandler extends ModelHandler<SiniestroAmis> {

    private final AjusteRepository repository = RepositoryFactory.getAjusteRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private static final String MODULO = "01";

    private static final String CLAVE_CARRETERA = "CRR";

    private static final CatalogoAmis TIPO_VIALIDAD = new CatalogoAmis("1");

    private static final CatalogoAmis TIPO_CARRETERA = new CatalogoAmis("0");

    private static final CatalogoAmis COLONIA_DEFAULT = new CatalogoAmis("9999999999");

    private static final CatalogoAmis MUNICIPIO_DEFAULT = new CatalogoAmis("9999");

    private static final CatalogoAmis ESTADO_DEFAULT = new CatalogoAmis("99");

    @Override
    public List<SiniestroAmis> get(String ajuste) {
        LoadingIndicator.message("Obteniendo datos del siniestro");
        List<SiniestroAmis> result = new ArrayList<>(1);
        Ajuste a = repository.findById(ajuste);
        SiniestroAmis siniestro = new SiniestroAmis();
        siniestro.setHoraOcurrido(DateUtils.timeIso(a.getSiniestro().getFechaReporte()));
        siniestro.setHoraAtencion(DateUtils.timeIso(a.getFechaArribo()));
        siniestro.setFechaAtencion(DateUtils.dateIso(a.getFechaArribo()));
        siniestro.setFechaOcurrido(DateUtils.dateIso(a.getSiniestro().getFechaReporte()));
        siniestro.setUbicacionSiniestro(getUbicacion(a.getSiniestro().getUbicacion()));
        siniestro.setCitaProgramada(a.getCita() == null ? false : a.getCita());
        result.add(siniestro);
        return result;
    }

    @Override
    protected void saveModulo(String ajuste, SiniestroAmis modulo) {
        Amis amis = amisRepository.findByAjuste(ajuste);
        FolioContraparteAmis contraparte = null;
        if (amis != null) {
            for (FolioContraparteAmis c : amis.getContrapartes()) {
                if (modulo.getFolio().equals(c.getFolioInicial())) {
                    contraparte = c;
                    putContraparte(c, modulo);
                    break;
                }
            }
            if (contraparte == null) {
                contraparte = new FolioContraparteAmis();
                contraparte.setFolioInicial(modulo.getFolio());
                putContraparte(contraparte, modulo);
                amis.getContrapartes().add(contraparte);
            }
        }
        amisRepository.save(amis);
    }


    private void putContraparte(FolioContraparteAmis contraparte, SiniestroAmis modulo) {
        contraparte.setFechaAtencion(DateUtils.dateIsoToDate(modulo.getFechaAtencion()));
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }

    private UbicacionSiniestroAmis getUbicacion(Ubicacion ubicacion) {
        UbicacionSiniestroAmis result = new UbicacionSiniestroAmis();
        result.setCiudad(getDireccion(ubicacion));
        result.setCarretera(getCarretera(ubicacion));
        if (ubicacion != null) {
            result.setCoordenadas(new Coordenadas(ubicacion.getLatitud(), ubicacion.getLongitud()));
            if (ubicacion.getTipoUbicacion() != null) {
                result.setLugarSiniestro(new CatalogoAmis(ubicacion.getTipoUbicacion().getClaveAmis()));
            }
        }
        return result;
    }

    private Direccion getDireccion(Ubicacion ubicacion) {
        Direccion result = null;
        if (ubicacion != null && !CLAVE_CARRETERA.equals(ubicacion.getTipoUbicacion().getClave())) {
            result = new Direccion();
            result.setEstado(getEstado(ubicacion));
            result.setDelegacionMunicipio(getMunicipio(ubicacion));
            result.setColonia(getColonia(ubicacion));
            result.setCodigoPostal(ubicacion.getCodigoPostal());
            result.setNumeroExterior(ubicacion.getNumeroExterior());
            result.setNumeroInterior(ubicacion.getNumeroInterior());
            result.setEntreCalles1(ubicacion.getEntreCalles());
            result.setEntreCalles2("");
            result.setCalle(ubicacion.getVia());
            result.setTipoVialidad(TIPO_VIALIDAD);
        }
        return result;
    }

    private Carretera getCarretera(Ubicacion ubicacion) {
        Carretera result = null;
        if (ubicacion != null && CLAVE_CARRETERA.equals(ubicacion.getTipoUbicacion().getClave())) {
            result = new Carretera();
            result.setNombre(ubicacion.getVia());
            result.setKilometro(ubicacion.getNumeroExterior() != null ? ubicacion.getNumeroExterior() : ubicacion.getNumeroInterior());
            result.setObservaciones(ubicacion.getReferencias());
            result.setTipoCarretera(TIPO_CARRETERA);
        }
        return result;
    }

    protected abstract CatalogoAmis getEstado(Ubicacion ubicacion);

    protected abstract CatalogoAmis getMunicipio(Ubicacion ubicacion);

    protected abstract CatalogoAmis getColonia(Ubicacion ubicacion);

}
