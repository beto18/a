package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ubicacion;

public class SiniestroSepomexModelHandler extends SiniestroModelHandler {

    private static final CatalogoAmis COLONIA_DEFAULT = new CatalogoAmis("090030586");

    private static final CatalogoAmis MUNICIPIO_DEFAULT = new CatalogoAmis("09003");

    private static final CatalogoAmis ESTADO_DEFAULT = new CatalogoAmis("09");

    @Override
    protected CatalogoAmis getEstado(Ubicacion ubicacion) {
        if (ubicacion.getEstado() != null) {
            return new CatalogoAmis(ubicacion.getEstado().getClave());
        }
        return ESTADO_DEFAULT;
    }

    @Override
    protected CatalogoAmis getMunicipio(Ubicacion ubicacion) {
        if (ubicacion.getEstado() != null && ubicacion.getMunicipio() != null) {
            return new CatalogoAmis(ubicacion.getEstado().getClave() + ubicacion.getMunicipio().getClave());
        }
        return MUNICIPIO_DEFAULT;
    }

    @Override
    protected CatalogoAmis getColonia(Ubicacion ubicacion) {
        if (ubicacion.getEstado() != null && ubicacion.getMunicipio() != null && ubicacion.getColonia() != null) {
            return new CatalogoAmis(ubicacion.getEstado().getClave() + ubicacion.getMunicipio().getClave() + ubicacion.getColonia().getClave());
        }
        return COLONIA_DEFAULT;
    }

}
