package com.gnp.ajustadormovil.android.interactors.amis.impl;

import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ubicacion;

public class SiniestroSipacModelHandler extends SiniestroModelHandler {

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private static final CatalogoAmis COLONIA_DEFAULT = new CatalogoAmis("9999999999");

    private static final CatalogoAmis MUNICIPIO_DEFAULT = new CatalogoAmis("9999");

    private static final CatalogoAmis ESTADO_DEFAULT = new CatalogoAmis("99");

    @Override
    protected CatalogoAmis getEstado(Ubicacion ubicacion) {
        CatalogoAmis result = ESTADO_DEFAULT;
        if (ubicacion.getEstado() != null) {
            EstadoAmis estadoAmis = getCatalogoOrElse(siniestrosInteractor.getEstado(ubicacion.getEstado().getClave()));
            if (estadoAmis != null) {
                result = new CatalogoAmis(estadoAmis.getClaveAmis().toString());
            }
        }
        return result;
    }

    @Override
    protected CatalogoAmis getMunicipio(Ubicacion ubicacion) {
        CatalogoAmis result = MUNICIPIO_DEFAULT;
        if (ubicacion.getEstado() != null && ubicacion.getMunicipio() != null) {
            MunicipioAmis municipioAmis = getCatalogoOrElse(siniestrosInteractor.getMunicipio(ubicacion.getEstado().getClave()
                    , ubicacion.getMunicipio().getClave()));
            if (municipioAmis != null) {
                result = new CatalogoAmis(municipioAmis.getClaveAmis().toString());
            }
        }
        return result;
    }

    @Override
    protected CatalogoAmis getColonia(Ubicacion ubicacion) {
        CatalogoAmis result = COLONIA_DEFAULT;
        if (ubicacion.getEstado() != null && ubicacion.getMunicipio() != null && ubicacion.getColonia() != null) {
            MunicipioAmis municipioAmis = getCatalogoOrElse(siniestrosInteractor.getMunicipio(ubicacion.getEstado().getClave(), ubicacion.getMunicipio().getClave()));
            if (municipioAmis != null) {
                result = new CatalogoAmis(municipioAmis.getEstado().getClaveAmis().toString() + municipioAmis.getClaveAmis().toString() + ubicacion.getColonia().getClave());
            }
        }
        return result;
    }

}
