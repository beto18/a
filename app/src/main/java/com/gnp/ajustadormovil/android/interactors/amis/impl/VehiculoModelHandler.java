package com.gnp.ajustadormovil.android.interactors.amis.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.amis.CatalogoAmis;
import com.gnp.ajustadormovil.android.models.amis.VehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.UsoVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.util.ArrayList;
import java.util.List;

public class VehiculoModelHandler extends ModelHandler<VehiculoAmis> {

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private static final String MODULO = "05";

    private static final CatalogoAmis DEFAULT_CLASE_AMIS = new CatalogoAmis("21");

    private static final CatalogoAmis DEFAULT_TIPO_AMIS = new CatalogoAmis("2123");

    private static final CatalogoAmis DEFAULT_MARCA_AMIS = new CatalogoAmis("393");

    private static final CatalogoAmis DEFAULT_TRANSPORTE_AMIS = new CatalogoAmis("1");

    private static final CatalogoAmis DEFAULT_USO_AMIS = new CatalogoAmis("99");

    private static final CatalogoAmis DEFAULT_COLOR_AMIS = new CatalogoAmis("1");

    @Override
    public List<VehiculoAmis> get(String ajuste) {
        LoadingIndicator.message("Cargando módulo de vehículo.");
        List<VehiculoAmis> result = new ArrayList<>(1);
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        Ajuste a = ajusteRepository.findById(ajuste);
        if(asegurado != null && asegurado.getVehiculo() != null) {
            VehiculoAmis vehiculo = new VehiculoAmis();
            CatalogoId color = getCatalogoOrElse(siniestrosInteractor.getColorVehiculo(asegurado.getVehiculo().getColor()));
            if(color != null) {
                vehiculo.setColor(new CatalogoAmis(color.getId().toString()));
            } else {
                vehiculo.setColor(DEFAULT_COLOR_AMIS);
            }
            vehiculo.setModelo(asegurado.getVehiculo().getModelo().toString());
            vehiculo.setPermiso(asegurado.getVehiculo().getPlacas());
            vehiculo.setVin(asegurado.getVehiculo().getVin());
            vehiculo.setClase(DEFAULT_CLASE_AMIS);
            putUso(vehiculo, a);
            putMarcaTipo(vehiculo, asegurado.getVehiculo());
            result.add(vehiculo);
        }
        return result;
    }

    private void putUso(VehiculoAmis vehiculo, Ajuste ajuste) {
        try {
            UsoVehiculo uso = RXUtils.sync(siniestrosInteractor.getUsoVehiculoAmis(ajuste.getSiniestro().getPoliza().getVehiculo().getUso().getClave()));
            vehiculo.setUso(new CatalogoAmis(uso.getUsoAmis().toString()));
        } catch (Exception e) {
            vehiculo.setUso(DEFAULT_USO_AMIS);
        }
    }

    private void putMarcaTipo(VehiculoAmis vehiculo, VehiculoAsegurado asegurado) {
        try {
            com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis amis =
                    RXUtils.sync(siniestrosInteractor.getVehiculoAmis(
                            asegurado.getTipoVehiculo(),
                            asegurado.getArmadora(),
                            asegurado.getCarroceria()));
            vehiculo.setTipoVehiculo(new CatalogoAmis(amis.getTransporte().toString()));
            vehiculo.setTipo(new CatalogoAmis(amis.getTipo().toString()));
            vehiculo.setMarca(new CatalogoAmis(amis.getMarca().toString()));
        } catch (Exception e) {
            Log.e(TAG, "Error al obtener los datos del vehículo", e);
            vehiculo.setTipoVehiculo(DEFAULT_TRANSPORTE_AMIS);
            vehiculo.setTipo(DEFAULT_TIPO_AMIS);
            vehiculo.setMarca(DEFAULT_MARCA_AMIS);
        }
    }

    @Override
    protected void saveModulo(String ajuste, VehiculoAmis modulo) {
        TerceroAuto tercero = getTerceroAuto(ajuste, modulo.getFolio());
        if(tercero.getVehiculo() == null) {
            tercero.setVehiculo(new VehiculoTercero());
        }
        if (tercero.getVehiculo().getVolante() == null) {
            tercero.getVehiculo().setModificado(true);
            try {
                com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis amis =
                        RXUtils.sync(siniestrosInteractor.getVehiculoAmis(
                                Long.parseLong(modulo.getTipoVehiculo().getClave()),
                                Long.parseLong(modulo.getMarca().getClave()),
                                Long.parseLong(modulo.getTipo().getClave())));
                tercero.getVehiculo().setTipoVehiculo(amis.getTipoVehiculo());
                tercero.getVehiculo().setArmadora(amis.getArmadora());
                tercero.getVehiculo().setCarroceria(amis.getCarroceria());
            } catch (Exception e) {
                tercero.getVehiculo().setTipoVehiculo(null);
                tercero.getVehiculo().setArmadora(null);
                tercero.getVehiculo().setCarroceria(null);
            }
            tercero.getVehiculo().setVersion(null);
            tercero.getVehiculo().setModelo(Short.parseShort(modulo.getModelo()));
            tercero.getVehiculo().setVin(modulo.getVin());
            tercero.getVehiculo().setPlacas(modulo.getPermiso());
            tercero.setDescripcion(tercero.getDescripcion().equals("Sin información del tercero") ? modulo.getFolio() : tercero.getDescripcion());
            tercero.getVehiculo().setColor(Long.parseLong(modulo.getColor().getClave()));
            terceroAutoRepository.saveByAjuste(ajuste, tercero);
        }
    }

    @Override
    public String getIdModulo() {
        return MODULO;
    }
}
