package com.gnp.ajustadormovil.android.interactors.catalogos;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.address.AddressWS;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Colonia;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;

import java.util.List;

import io.reactivex.Single;

public interface CatInteractor extends SyncInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getEntidadesFinancieras();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getEntidadFinanciera(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveInfo>> getMonedas();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveInfo>> getEstatusPoliza();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getCategoriasVehiculo();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getVias();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getOtrasAseguradoras();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getOtraAseguradora(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveInfo>> getEstados();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveInfo> getEstado(String estado);

    @RxLoading
    @RxApplySchedulers
    Single<List<Municipio>> getMunicipios(String estado);

    @RxLoading
    @RxApplySchedulers
    Single<Municipio> getMunicipio(String estado,
                                       String municipio);

    @RxLoading
    @RxApplySchedulers
    Single<List<ColoniaResumen>> getColonias(String estado,
                                                 String municipio);

    @RxLoading
    @RxApplySchedulers
    Single<Colonia> getColonia(String estado,
                                   String municipio,
                                   String clave);

    @RxLoading
    @RxApplySchedulers
    Single<AddressWS> getLocationAddress(Double lat,
                                             Double lng);

}
