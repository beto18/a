package com.gnp.ajustadormovil.android.interactors.catalogos;

import com.gnp.ajustadormovil.android.aspects.Aspects;
import com.gnp.ajustadormovil.android.interactors.catalogos.impl.CatInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.catalogos.impl.SiniestrosInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.catalogos.impl.TprInteractorOfflineImpl;

public final class CatalogoInteractorsFactory {

    private static CatInteractor catInteractor;

    private static TprInteractor tprService;

    private static SiniestrosInteractor siniestrosInteractor;

    public static CatInteractor getCatInteractor(){
        synchronized (CatInteractor.class){
            if(catInteractor == null){
                catInteractor = Aspects.aspect(CatInteractor.class, new CatInteractorOfflineImpl());
            }
            return catInteractor;
        }
    }

    public static TprInteractor getTprService(){
        synchronized (TprInteractor.class){
            if(tprService == null){
                tprService = Aspects.aspect(TprInteractor.class, new TprInteractorOfflineImpl());
            }
            return tprService;
        }
    }

    public static SiniestrosInteractor getSiniestrosInteractor(){
        synchronized (SiniestrosInteractor.class){
            if(siniestrosInteractor == null){
                siniestrosInteractor = Aspects.aspect(SiniestrosInteractor.class, new SiniestrosInteractorOfflineImpl());
            }
            return siniestrosInteractor;
        }
    }

}
