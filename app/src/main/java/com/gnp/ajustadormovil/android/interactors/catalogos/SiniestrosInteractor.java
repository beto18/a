package com.gnp.ajustadormovil.android.interactors.catalogos;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.SerieResponse;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Configuracion;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CostoParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Responsable;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.TipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.UsoVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis;

import java.util.List;

import io.reactivex.Single;

public interface SiniestrosInteractor extends SyncInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposAbogado();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getAreaAfectada(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getGestionesAbogado();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getComunicacionesAbogado();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getObjetoPerdido(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getMotivosCancelacionAjuste();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getMotivosRechazoAjuste();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getMotivosCierreAjuste();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposAmbulancia();

    @RxLoading
    @RxApplySchedulers
    Single<List<CentroRemate>> getCentrosRemate();

    @RxLoading
    @RxApplySchedulers
    Single<List<Contratista>> getContratistas();

    @RxLoading
    @RxApplySchedulers
    Single<Contratista> getContratista(Long idContratista);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getDocumentosPropiedad();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposIdentificacion();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getTipoIdentificacion(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposGrua();

    @RxLoading
    @RxApplySchedulers
    Single<Hospital> getHospital(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<HospitalResumen>> getHospitales(String supervisoria);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getMotivosInvestigacion();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getNivelLesion();

    @RxLoading
    @RxApplySchedulers
    Single<List<Area>> getAreasPartesLesionadas();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getAreasLesionadas();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoIdAmis>> getPartesLesionadas(Long area);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getNacionalidades();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getRazonesNoDeducible();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getRazonNoDeducible(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getPosicionesOcupante();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getPosicionOcupante(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposPatrulla();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getTiposPoliza();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getCondicionamientosPoliza(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getCondicionamientosPoliza();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getVolantesRecuperacion();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getTiposRecuperacionMonetaria();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getTipoRecuperacionMonetaria(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposRecuperacionMonetariaTarjeta();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposServicio();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposSiniestro();

    @RxLoading
    @RxApplySchedulers
    Single<List<Supervisoria>> getSupervisorias();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveAmis>> getTiposTelefono();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getTipoTelefono(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getTipoTelefonoAmis(String claveAmis);


    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposTercero();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getTipoTercero(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposTerceroAuto();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getCategoriaTerceroNoAuto();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getTiposTerceroNoAuto(Long categoria);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getVolantesTerceroNoAuto();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveAmis>> getTiposUbicacion();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getTipoUbicacion(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getTipoUbicacionAmis(Long claveAmis);

    @RxLoading
    @RxApplySchedulers
    Single<List<Area>> getAreasPartesAfectadas();

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getAreasAfectadas();

    @RxLoading
    @RxApplySchedulers
    Single<List<ParteAfectada>> getPartesAfectadas(Long area);

    @RxLoading
    @RxApplySchedulers
    Single<ParteAfectada> getParteAfectada(Long area, Long id);

    @RxLoading
    @RxApplySchedulers
    Single<ParteAfectada> getParteAfectada(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<ParteAfectada> getParteAfectadaAmis(String claveAmis);

    @RxLoading
    @RxApplySchedulers
    Single<List<ImpactoVehiculoAmis>> getImpactosVehiculo();

    @RxLoading
    @RxApplySchedulers
    Single<ImpactoVehiculoAmis> getImpactoVehiculo(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<ImpactoVehiculoAmis> getImpactoVehiculoAmis(String id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getCircustanciasVehiculo();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getCircustanciaVehiculo(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposVolante();

    @RxLoading
    @RxApplySchedulers
    Single<List<Cobertura>> getCoberturasVolante(Integer categoria, String tipoSiniestro);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoIdAmis> getParteLesionada(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoIdAmis> getParteLesionadaAmis(String claveAmis);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getCategoriaTerceroNoAuto(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getTipoTerceroNoAuto(Long categoria, Long id);

    @RxLoading
    @RxApplySchedulers
    Single<Responsable> getResponsable(Long circustanciaA, Long circustanciaB);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getResponsabilidades(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveAmis>> getGeneros();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getGenero(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getGeneroAmis(String claveAmis);

    Single<List<CostoParteAfectada>> getCostoParteAfectada(String categoria, List<Long> parte);

    Single<Configuracion> getConfiguracion(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposLicencia(String estado);

    @RxLoading
    @RxApplySchedulers
    Single<TipoLicencia> getTipoLicencia(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoId>> getColoresVehiculo();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoId> getColorVehiculo(Long id);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClaveAmis>> getTiposPersona();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getTipoPersona(String clave);

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClaveAmis> getTipoPersonaAmis(Long claveAmis);

    @RxLoading
    @RxApplySchedulers
    Single<MunicipioAmis> getMunicipio(String estado, String municipio);

    @RxLoading
    @RxApplySchedulers
    Single<MunicipioAmis> getMunicipioAmis(Integer estadoAmis, Integer municipioAmis);

    @RxLoading
    @RxApplySchedulers
    Single<EstadoAmis> getEstado(String estado);

    @RxLoading
    @RxApplySchedulers
    Single<EstadoAmis> getEstadoAmis(Integer estadoAmis);


    @RxLoading
    @RxApplySchedulers
    Single<VehiculoAmis> getVehiculoAmis(String tipoVehiculo,
                                         String armadora,
                                         String carroceria);

    @RxLoading
    @RxApplySchedulers
    Single<VehiculoAmis> getVehiculoAmis(Long transporte,
                                         Long marca,
                                         Long tipo);

    @RxLoading
    @RxApplySchedulers
    Single<UsoVehiculo> getUsoVehiculoAmis(String usoVehiculo);

    @RxLoading
    @RxApplySchedulers
    Single<Boolean> getProductoCashflow(String comercial, String tecnico);


    Single<String> getOtraAseguradoraAmis(Integer claveAmis);






}
