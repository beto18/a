package com.gnp.ajustadormovil.android.interactors.catalogos;

import io.reactivex.Completable;

public interface SyncInteractor {

    /**
     * Sincroniza la información de los datos.
     */
    Completable synchronizeData();

    /**
     * Obtiene si los datos están sincronizados.
     * @return Si esta sincronizado.
     */
    boolean isSynchronized();


}
