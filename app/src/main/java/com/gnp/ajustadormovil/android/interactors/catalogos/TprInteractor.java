package com.gnp.ajustadormovil.android.interactors.catalogos;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Armadora;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Carroceria;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;

import java.util.List;

import io.reactivex.Single;

public interface TprInteractor extends SyncInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getTiposVehiculo();

    @RxLoading
    @RxApplySchedulers
    Single<CatalogoClave> getTipoVehiculo(String tipoVehiculo);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getArmadoras(String tipoVehiculo);

    @RxLoading
    @RxApplySchedulers
    Single<Armadora> getArmadora(String tipoVehiculo, String armadora);

    @RxLoading
    @RxApplySchedulers
    Single<List<CatalogoClave>> getCarrocerias(String tipoVehiculo, String tipoArmadora);

    @RxLoading
    @RxApplySchedulers
    Single<Carroceria> getCarroceria(String tipoVehiculo, String armadora, String carroceria);

    @RxLoading
    @RxApplySchedulers
    Single<List<Short>> getModelos(String tipoVehiculo, String tipoArmadora, String tipoCarroceria);

    @RxLoading
    @RxApplySchedulers
    Single<List<VersionVehiculoResumen>> getVersiones(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int year);

    @RxLoading
    @RxApplySchedulers
    Single<Cobertura> getCobertura(String clave);

    @RxLoading
    @RxApplySchedulers
    Single <VersionVehiculo> getVersion(String tipoVehiculo,
                                           String armadora,
                                           String carroceria,
                                           Integer modelo,
                                           String version);

}