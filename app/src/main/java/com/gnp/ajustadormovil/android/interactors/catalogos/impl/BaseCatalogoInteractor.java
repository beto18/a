package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.data.catalogos.InfoData;
import com.gnp.ajustadormovil.android.data.catalogos.VersionData;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.interactors.catalogos.SyncInteractor;
import com.gnp.ajustadormovil.android.utils.core.ConnectivityUtils;
import com.gnp.ajustadormovil.android.utils.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseCatalogoInteractor<T> implements SyncInteractor {

    protected final String TAG = this.getClass().getSimpleName();

    private final Object monitor = new Object();

    private volatile boolean sync = false;

    private final InfoData infoData = CatalogsFactory.getInfoData();

    private final VersionData versionData = CatalogsFactory.getVersionData();

    private final T onlineData;

    private final T offlineData;

    public BaseCatalogoInteractor(T onlineData, T offlineData) {
        this.onlineData = onlineData;
        this.offlineData = offlineData;
    }

    /**
     * Obtiene el objeto de acceso a datos dependiendo de:
     * <ul>
     *     <li>Si se encuentra en un modo de trabajo offline, obtiene la implementación offline</li>
     *     <li>Si se encuentra en un modo de trabajo online:
     *         <ul>
     *             <li>Si los catálogos están sincronizados, obtiene la implementación offline</li>
     *             <li>Si los catálogos no están sincronizados, obtiene la implementación online</li>
     *         </ul>
     *     </li>
     * </ul>
     */
    protected T getData() {
        return getOfflineData();
    }

    /**
     * Obtiene el archvivo de datos de los catálogos.
     * @return Archivo de datos {@link File}.
     */
    protected abstract File getDataFile();

    /**
     * Obtiene el objeto de acceso a datos principal del servicio en su implementación online.
     * @return Objedo de acceso a datos.
     */
    protected T getOnlineData() {
        return onlineData;
    }

    /**
     * Obtiene el objeto de acceso a datos principal del servicio en su implementación offline.
     * @return Objedo de acceso a datos.
     */

    protected T getOfflineData() {
        return offlineData;
    }

    /**
     * Comprueba si la aplicación esta en modo online.
     * @return Si se encuentra online.
     */
    protected static boolean isOnline(){
        return ConnectivityUtils.isConnectedFast();
    }

    /**
     * Obtiene el nombre de los datos de los catálogos.
     * @return Nombre de los datos.
     */
    protected abstract String getDataName();

    @Override
    public boolean isSynchronized(){
        synchronized (monitor){
            return sync;
        }
    }

    private void setSynchronized(boolean sync){
        synchronized (monitor){
            this.sync = sync;
        }
    }

    @Override
    public Completable synchronizeData(){
        return Completable.create(source -> {
            source.setDisposable(infoData
                    .getDataVersion(getDataName())
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(version -> {
                        if (!isCurrentVersion(version.getVersion())) {
                            putDataFile(version.getUrl());
                            saveVersion(version.getVersion());
                        }
                        setSynchronized(true);
                        source.onComplete();
                    }, throwable -> {
                        setSynchronized(getDataFile().exists());
                        if(isSynchronized()) {
                            source.onComplete();
                        } else {
                            source.tryOnError(throwable);
                        }
                    })
            );
        });
    }

    /**
     * Descarga el archivo de datos, lo descomprime y lo coloca en el archivo de la fuente de datos.
     * @param url URL del archivo de datos de internet.
     * @throws IOException Si existe algún problema de acceso a archivos o red.
     */
    private void putDataFile(String url) throws IOException {
        Log.d(TAG, getDataName() + " sincronizando información");
        File tempZipFile = new File(Utils.TEMP_FOLDER + File.separator + getDataName() + ".zip");
        File tempDataFile = new File(Utils.TEMP_FOLDER + File.separator + getDataName() + ".db");
        File dataFile = getDataFile();
        Log.d(TAG, getDataName() + " descargando información");
        download(new URL(url), tempZipFile);
        Log.d(TAG, getDataName() + " descomprimiendo información");
        unzip(tempZipFile, tempDataFile.getParentFile());
        Log.d(TAG, getDataName() + " guardando información");
        move(tempDataFile, dataFile);
        tempZipFile.delete();
        tempDataFile.delete();
        Log.d(TAG, getDataName() + " información sincronizada");
    }

    /**
     * Descarga un archivo de la red.
     * @param url URL del archivo.
     * @param target {@link File} donde se almacenará el archivo.
     * @throws IOException Si existe un error de acceso a archivos.
     */
    private void download(URL url, File target) throws IOException {
        try {
            if (target.exists()) {
                target.delete();
            }
            target.getParentFile().mkdirs();
            target.createNewFile();
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream out = new FileOutputStream(target);
            byte[] buffer = new byte[1024];
            int count;
            while ((count = input.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            out.flush();
            out.close();
            input.close();
        } catch (SocketTimeoutException | UnknownHostException | ConnectException ex ) {
            throw new NetworkException(ex.getMessage(), ex);
        }
    }

    /**
     * Descomprime un archivo zip en la carpeta que se indica.
     * @param zip Archivo ZIP para descomprimir
     * @param folder Directorio en donde se descomprimira el ZIP
     * @throws IOException Si existe un error de acceso a archivos.
     */
    private void unzip(File zip, File folder) throws IOException {
        if(!folder.exists()){
            folder.mkdirs();
        }
        String filename;
        ZipInputStream in = new ZipInputStream(new BufferedInputStream(new FileInputStream(zip)));
        ZipEntry ze;
        byte[] buffer = new byte[1024];
        int count;
        while ((ze = in.getNextEntry()) != null) {
            filename = ze.getName();
            if (ze.isDirectory()) {
                File fmd = new File(folder.getAbsolutePath() + File.separator + filename);
                fmd.mkdirs();
                continue;
            }
            FileOutputStream out = new FileOutputStream(folder.getAbsolutePath() + File.separator + filename);
            while ((count = in.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            out.flush();
            out.close();
            in.closeEntry();
        }
        in.close();
    }

    /**
     * Mueve un archivo de una dirección a otra. Si ya existe un archivo previo lo reemplaza.
     * @param source Archivo origen.
     * @param target Archivo destino.
     */
    private void move(File source, File target) throws IOException {
        if(!source.exists()){
            throw new IOException("No existe el archivo: " + source.getAbsolutePath());
        }
        if(!target.getParentFile().exists()){
            target.getParentFile().mkdirs();
        }
        if(target.exists()){
            if(!target.delete()){
                throw new IOException("Error al borrar el archivo: " + target.getAbsolutePath());
            }
            target.createNewFile();
        }
        InputStream in = new FileInputStream(source);
        OutputStream out = new FileOutputStream(target);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
    }

    /**
     * Valida si es la versión actual de los datos de catálogos.
     * @param version Versión que se desea comparar con la instalada actualmente.
     * @return Si es la misma versión.
     */
    private boolean isCurrentVersion(String version){
        String currentVersion = versionData.getVersion(getDataName());
        Log.d(TAG, getDataName() + " versión instalada: " + currentVersion + " versión actual: " + version);
        return currentVersion != null && version.equals(currentVersion);
    }

    /**
     * Actualiza la versión actual de los datos.
     * @param version Versión de datos de catálogos.
     */
    private void saveVersion(String version){
        versionData.saveVersion(getDataName(), version);
    }

}
