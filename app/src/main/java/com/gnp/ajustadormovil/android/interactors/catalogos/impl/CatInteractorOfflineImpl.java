package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.catalogos.CatData;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.models.address.AddressWS;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Colonia;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.io.File;
import java.util.List;

import io.reactivex.Single;

public class CatInteractorOfflineImpl extends BaseCatalogoInteractor<CatData> implements CatInteractor {

    private static final String DATA_NAME = "cat";

    private static final File DATA_FILE = AjustadorMovilApplication.getInstance().getDatabasePath(Utils.DATA_CAT_NAME);

    public CatInteractorOfflineImpl() {
        super(CatalogsFactory.getCatalogosCat(DataType.ONLINE), CatalogsFactory.getCatalogosCat(DataType.OFFLINE));
    }

    @Override
    protected File getDataFile() {
        return DATA_FILE;
    }

    @Override
    protected String getDataName() {
        return DATA_NAME;
    }

    @Override
    public Single<List<CatalogoClave>> getEntidadesFinancieras() {
        return getData().getEntidadesFinancieras();
    }

    @Override
    public Single<CatalogoClave> getEntidadFinanciera(String clave) {
        return getData().getEntidadFinanciera(clave);
    }

    @Override
    public Single<List<CatalogoClaveInfo>> getMonedas() {
        return getData().getMonedas();
    }

    @Override
    public Single<List<CatalogoClaveInfo>> getEstatusPoliza() {
        return getData().getEstatusPoliza();
    }

    @Override
    public Single<List<CatalogoClave>> getCategoriasVehiculo() {
        return getData().getCategoriasVehiculo();
    }

    @Override
    public Single<List<CatalogoClave>> getVias() {
        return getData().getVias();
    }

    @Override
    public Single<List<CatalogoClave>> getOtrasAseguradoras() {
        return getData().getOtrasAseguradoras();
    }

    @Override
    public Single<CatalogoClave> getOtraAseguradora(String clave) {
        return getData().getOtraAseguradora(clave);
    }

    @Override
    public Single<List<CatalogoClaveInfo>> getEstados() {
        return getData().getEstados();
    }

    @Override
    public Single<CatalogoClaveInfo> getEstado(String estado) {
        return getData().getEstado(estado);
    }

    @Override
    public Single<List<Municipio>> getMunicipios(String estado) {
        return getData().getMunicipios(estado);
    }

    @Override
    public Single<Municipio> getMunicipio(String estado, String municipio) {
        return getData().getMunicipio(estado, municipio);
    }

    @Override
    public Single<List<ColoniaResumen>> getColonias(String estado, String municipio) {
        return getData().getColonias(estado, municipio);
    }

    @Override
    public Single<Colonia> getColonia(String estado, String municipio, String clave) {
        return getData().getColonia(estado, municipio, clave);
    }

    @Override
    public Single<AddressWS> getLocationAddress(Double lat, Double lon){
        return getOnlineData().getLocationAddress(lat,lon);
    }

}
