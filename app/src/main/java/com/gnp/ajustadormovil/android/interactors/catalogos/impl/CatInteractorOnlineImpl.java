package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.catalogos.CatData;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.models.address.AddressWS;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Colonia;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;

import java.util.List;

import io.reactivex.Single;

public class CatInteractorOnlineImpl extends SyncInteractorMockImpl implements CatInteractor {
    
    private final CatData data = CatalogsFactory.getCatalogosCat(DataType.ONLINE);

    @Override
    public Single<List<CatalogoClave>> getEntidadesFinancieras() {
        return data.getEntidadesFinancieras();
    }

    @Override
    public Single<CatalogoClave> getEntidadFinanciera(String clave) {
        return data.getEntidadFinanciera(clave);
    }

    @Override
    public Single<List<CatalogoClaveInfo>> getMonedas() {
        return data.getMonedas();
    }

    @Override
    public Single<List<CatalogoClaveInfo>> getEstatusPoliza() {
        return data.getEstatusPoliza();
    }

    @Override
    public Single<List<CatalogoClave>> getCategoriasVehiculo() {
        return data.getCategoriasVehiculo();
    }

    @Override
    public Single<List<CatalogoClave>> getVias() {
        return data.getVias();
    }

    @Override
    public Single<List<CatalogoClave>> getOtrasAseguradoras() {
        return data.getOtrasAseguradoras();
    }

    @Override
    public Single<CatalogoClave> getOtraAseguradora(String clave) {
        return data.getOtraAseguradora(clave);
    }

    @Override
    public Single<List<CatalogoClaveInfo>> getEstados() {
        return data.getEstados();
    }

    @Override
    public Single<CatalogoClaveInfo> getEstado(String estado) {
        return data.getEstado(estado);
    }

    @Override
    public Single<List<Municipio>> getMunicipios(String estado) {
        return data.getMunicipios(estado);
    }

    @Override
    public Single<Municipio> getMunicipio(String estado, String municipio) {
        return data.getMunicipio(estado, municipio);
    }

    @Override
    public Single<List<ColoniaResumen>> getColonias(String estado, String municipio) {
        return data.getColonias(estado, municipio);
    }

    @Override
    public Single<Colonia> getColonia(String estado, String municipio, String clave) {
        return data.getColonia(estado, municipio, clave);
    }

    @Override
    public Single<AddressWS> getLocationAddress(Double lat, Double lng) {
        return data.getLocationAddress(lat, lng);
    }

}
