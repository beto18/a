package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.data.catalogos.SiniestrosData;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Configuracion;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CostoParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ProductoCashflow;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Responsable;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.TipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.UsoVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class SiniestrosInteractorOfflineImpl extends BaseCatalogoInteractor<SiniestrosData> implements SiniestrosInteractor {

    private static final String DATA_NAME = "sinau";

    private static final String RESPONSABLE_IMAGE_FOLDER =  "file:///android_asset/images/responsabilidad/";

    private static final File DATA_FILE = AjustadorMovilApplication.getInstance().getDatabasePath(Utils.DATA_SINAU_NAME);

    private final Repository<Supervisoria, String> supervisoriaRepository = RepositoryFactory.getRepository(Supervisoria.class, String.class);

    public SiniestrosInteractorOfflineImpl() {
        super(CatalogsFactory.getCatalogsSiniestros(DataType.ONLINE), CatalogsFactory.getCatalogsSiniestros(DataType.OFFLINE));
    }

    @Override
    protected File getDataFile() {
        return DATA_FILE;
    }

    @Override
    protected String getDataName() {
        return DATA_NAME;
    }

    @Override
    public Single<List<CatalogoClave>> getTiposAbogado(){
        return getData().getTiposAbogado();
    }

    @Override
    public Single<CatalogoId> getAreaAfectada(Long id) {
        return getData().getAreaAfectada(id);
    }

    @Override
    public Single<List<CatalogoClave>> getGestionesAbogado(){
        return getData().getGestionesAbogado();
    }

    @Override
    public Single<CatalogoId> getObjetoPerdido(Long id) {
        return getData().getObjetoPerdido(id);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosCancelacionAjuste(){
        return getData().getMotivosCancelacionAjuste();
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosRechazoAjuste(){
        return getData().getMotivosRechazoAjuste();

    }

    @Override
    public Single<List<CatalogoClave>> getMotivosCierreAjuste(){
        return getData().getMotivosCierreAjuste();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposAmbulancia(){
        return getData().getTiposAmbulancia();
    }

    @Override
    public Single<List<CentroRemate>> getCentrosRemate(){
        return getData().getCentrosRemate();
    }

    @Override
    public Single<List<Contratista>> getContratistas(){
        return getData().getContratistas();
    }

    @Override
    public Single<Contratista> getContratista(Long idContratista) {
        return getData().getContratista(idContratista);
    }

    @Override
    public Single<List<CatalogoClave>> getDocumentosPropiedad(){
        return getData().getDocumentosPropiedad();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposIdentificacion(){
        return getData().getTiposIdentificacion();
    }

    @Override
    public Single<CatalogoClave> getTipoIdentificacion(String clave) {
        return getData().getTipoIdentificacion(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposGrua(){
        return getData().getTiposGrua();
    }

    @Override
    public Single<Hospital> getHospital(Long id){
        return getData().getHospital(id);
    }

    @Override
    public Single<List<HospitalResumen>> getHospitales(String supervisoria){
        return getData().getHospitales(supervisoria);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosInvestigacion(){
        return getData().getMotivosInvestigacion();
    }

    @Override
    public Single<Boolean> getProductoCashflow(String comercial, String tecnico) {
        return getData().getProductoCashflow(comercial, tecnico)
                .onErrorReturnItem(new ProductoCashflow())
                .map(result -> result.getProductoComercial() != null);
    }

    @Override
    public Single<List<CatalogoId>> getNivelLesion(){
        return getData().getNivelLesion();
    }

    @Override
    public Single<List<Area>> getAreasPartesLesionadas(){
        return getData().getAreasLesionadas()
                .flatMapObservable(Observable::fromIterable)
                .flatMapSingle(area -> Single.zip(
                        Single.just(new Area(area.getId(), area.getNombre())),
                        getData().getPartesLesionadas(area.getId()),
                        (areaLesionada, partes) -> {
                            Area a = new Area(areaLesionada.getId(), areaLesionada.getNombre());
                            a.setPartes(partes);
                            return a;
                        })
                ).toList();
    }

    @Override
    public Single<List<CatalogoId>> getAreasLesionadas(){
        return getData().getAreasLesionadas();
    }

    @Override
    public Single<List<CatalogoIdAmis>> getPartesLesionadas(Long area){
        return getData().getPartesLesionadas(area);
    }

    @Override
    public Single<List<CatalogoClave>> getNacionalidades(){
        return getData().getNacionalidades();
    }

    @Override
    public Single<List<CatalogoId>> getRazonesNoDeducible(){
        return getData().getRazonesNoDeducible();
    }

    @Override
    public Single<CatalogoId> getRazonNoDeducible(Long id) {
        return getData().getRazonNoDeducible(id);
    }

    @Override
    public Single<List<CatalogoClave>> getPosicionesOcupante(){
        return getData().getPosicionesOcupante();
    }

    @Override
    public Single<CatalogoClave> getPosicionOcupante(String clave){
        return getData().getPosicionOcupante(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposPatrulla(){
        return getData().getTiposPatrulla();
    }

    @Override
    public Single<List<CatalogoId>> getTiposPoliza(){
        return getData().getTiposPoliza();
    }

    @Override
    public Single<CatalogoId> getCondicionamientosPoliza(Long id){
        return getData().getCondicionamientosPoliza(id);
    }

    @Override
    public Single<List<CatalogoId>> getCondicionamientosPoliza() {
        return getData().getCondicionamientosPoliza();
    }

    @Override
    public Single<List<CatalogoClave>> getVolantesRecuperacion(){
        return getData().getVolantesRecuperacion();
    }

    @Override
    public Single<List<CatalogoId>> getTiposRecuperacionMonetaria(){
        return getData().getTiposRecuperacionMonetaria();
    }

    @Override
    public Single<CatalogoId> getTipoRecuperacionMonetaria(Long id){
        return getData().getTipoRecuperacionMonetaria(id);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposRecuperacionMonetariaTarjeta(){
        return getData().getTiposRecuperacionMonetariaTarjeta();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposServicio(){
        return getData().getTiposServicio();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposSiniestro(){
        return getData().getTiposSiniestro();
    }

    @Override
    public Single<List<Supervisoria>> getSupervisorias(){
        return Single.create(e -> {
                e.setDisposable(getOnlineData()
                        .getSupervisorias()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .subscribe(supervisorias -> {
                            Log.i(TAG, "Guardando supervisorías en local");
                            supervisoriaRepository.deleteAll();
                            supervisoriaRepository.save(supervisorias);
                            e.onSuccess(supervisorias);
                        }, throwable -> {
                            List<Supervisoria> supervisorias = supervisoriaRepository.findAll();
                            if (supervisorias.isEmpty()) {
                                e.tryOnError(throwable);
                            }else {
                                e.onSuccess(supervisorias);
                            }
                        }));

        });
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposTelefono(){
        return getData().getTiposTelefono();
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoTelefono(String clave) {
        return getData().getTipoTelefono(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoTelefonoAmis(String claveAmis) {
        return getData().getTipoTelefonoAmis(claveAmis);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposTercero(){
        return getData().getTiposTercero();
    }

    @Override
    public Single<CatalogoClave> getTipoTercero(String clave){
        return getData().getTipoTercero(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposTerceroAuto(){
        return getData().getTiposTerceroAuto();
    }

    @Override
    public Single<List<CatalogoId>> getCategoriaTerceroNoAuto(){
        return getData().getCategoriasTerceroNoAuto();
    }

    @Override
    public Single<List<CatalogoId>> getTiposTerceroNoAuto(Long categoria){
        return getData().getTiposTerceroNoAuto(categoria);
    }

    @Override
    public Single<List<CatalogoClave>> getVolantesTerceroNoAuto(){
        return getData().getVolantesTerceroNoAuto();
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposUbicacion(){
        return getData().getTiposUbicacion();
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoUbicacion(String clave) {
        return getData().getTipoUbicacion(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoUbicacionAmis(Long claveAmis) {
        return getData().getTipoUbicacionAmis(claveAmis);
    }

    @Override
    public Single<List<Area>> getAreasPartesAfectadas(){
        return getData().getAreasAfectadas()
                .flatMapObservable(Observable::fromIterable)
                .flatMapSingle(area -> Single.zip(
                        Single.just(new Area(area.getId(), area.getNombre())),
                        getData().getPartesAfectadas(area.getId()),
                        (areaAfectada, partes) -> {
                            areaAfectada.setPartes(partes);
                            return areaAfectada;
                        })
                ).toList();
    }

    @Override
    public Single<List<CatalogoId>> getAreasAfectadas(){
        return getData().getAreasAfectadas();
    }

    @Override
    public Single<List<ParteAfectada>> getPartesAfectadas(Long area){
        return getData().getPartesAfectadas(area);
    }

    @Override
    public Single<ParteAfectada> getParteAfectada(Long area, Long id) {
        return getData().getParteAfectada(area,id);
    }

    @Override
    public Single<ParteAfectada> getParteAfectada(Long id){
        return getData().getParteAfectada(id);
    }

    @Override
    public Single<ParteAfectada> getParteAfectadaAmis(String claveAmis) {
        return getData().getParteAfectadaAmis(claveAmis);
    }

    @Override
    public Single<List<ImpactoVehiculoAmis>> getImpactosVehiculo(){
        return getData().getImpactosVehiculo();
    }

    @Override
    public Single<ImpactoVehiculoAmis> getImpactoVehiculo(Long id) {
        return getData().getImpactoVehiculo(id);
    }

    @Override
    public Single<ImpactoVehiculoAmis> getImpactoVehiculoAmis(String id) {
        return getData().getImpactoVehiculoAmis(id);
    }

    @Override
    public Single<List<CatalogoId>> getCircustanciasVehiculo(){
        return getData().getCircustanciasVehiculo();
    }

    @Override
    public Single<CatalogoId> getCircustanciaVehiculo(Long id){
        return getData().getCircustanciaVehiculo(id);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposVolante(){
        return getData().getTiposVolante();
    }

    @Override
    public Single<List<Cobertura>> getCoberturasVolante(Integer categoria, String tipoSiniestro){
        return getData().getCoberturasVolante(categoria, tipoSiniestro);
    }

    @Override
    public Single<List<CatalogoClave>> getComunicacionesAbogado() {
        return getData().getComunicacionesAbogado();
    }

    @Override
    public Single<CatalogoIdAmis> getParteLesionada(Long id) {
        return getData().getParteLesionada(id);
    }

    @Override
    public Single<CatalogoIdAmis> getParteLesionadaAmis(String claveAmis) {
        return getData().getParteLesionadaAmis(claveAmis);
    }

    @Override
    public Single<CatalogoId> getCategoriaTerceroNoAuto(Long id) {
        return getData().getCategoriaTerceroNoAuto(id);
    }

    @Override
    public Single<CatalogoId> getTipoTerceroNoAuto(Long categoria, Long id) {
        return getData().getTipoTerceroNoAuto(categoria, id);
    }

    @Override
    public Single<Responsable> getResponsable(Long circustanciaA, Long circustanciaB){
        return getData().getResponsables(circustanciaA, circustanciaB)
                .map(results -> {
                    Responsable result = results.get(0);
                    if(result.getUrlImagen() == null){
                        result.setUrlImagen(RESPONSABLE_IMAGE_FOLDER + "a" + circustanciaA + "b" + circustanciaB + ".png");
                    }
                    return result;
                });
    }

    @Override
    public Single<CatalogoClave> getResponsabilidades(String clave) {
        return getData().getResponsabilidad(clave);
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getGeneros() {
        return getData().getGeneros();
    }

    @Override
    public Single<CatalogoClaveAmis> getGenero(String clave) {
        return getData().getGenero(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getGeneroAmis(String claveAmis) {
        return getData().getGeneroAmis(claveAmis);
    }

    @Override
    public Single<List<CostoParteAfectada>> getCostoParteAfectada(String categoria, List<Long> parte) {
        return getData().getCostoParteAfectada(categoria, parte);
    }

    @Override
    public Single<Configuracion> getConfiguracion(String clave) {
        return getData().getConfiguracion(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposLicencia(String estado) {
        return getData().getTiposLicencia(estado);
    }

    @Override
    public Single<TipoLicencia> getTipoLicencia(String clave) {
        return getData().getTipoLicencia(clave);
    }

    @Override
    public Single<List<CatalogoId>> getColoresVehiculo() {
        return getData().getColoresVehiculo();
    }

    @Override
    public Single<CatalogoId> getColorVehiculo(Long id) {
        return getData().getColorVehiculo(id);
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposPersona() {
        return getData().getTiposPersona();
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoPersona(String clave) {
        return getData().getTipoPersona(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoPersonaAmis(Long claveAmis) {
        return getData().getTipoPersonaAmis(claveAmis);
    }

    @Override
    public Single<MunicipioAmis> getMunicipio(String estado, String municipio) {
        return getData().getMunicipio(estado,municipio);
    }

    @Override
    public Single<MunicipioAmis> getMunicipioAmis(Integer estadoAmis, Integer municipioAmis) {
        return getData().getMunicipioAmis(estadoAmis,municipioAmis);
    }

    @Override
    public Single<EstadoAmis> getEstado(String estado) {
        return getData().getEstado(estado);
    }

    @Override
    public Single<EstadoAmis> getEstadoAmis(Integer estadoAmis) {
        return getData().getEstadoAmis(estadoAmis);
    }

    @Override
    public Single<VehiculoAmis> getVehiculoAmis(String tipoVehiculo, String armadora, String carroceria) {
        return getData().getVehiculosAmis(tipoVehiculo, armadora, carroceria)
                .map(vehiculos -> vehiculos.isEmpty() ? null : vehiculos.get(0));
    }

    @Override
    public Single<VehiculoAmis> getVehiculoAmis(Long transporte, Long marca, Long tipo) {
        return getData().getVehiculosAmis(transporte, marca, tipo)
                .map(vehiculos -> vehiculos.isEmpty() ? null : vehiculos.get(0));
    }

    @Override
    public Single<UsoVehiculo> getUsoVehiculoAmis(String usoVehiculo) {
        return getData().getUsoVehiculosAmis(usoVehiculo)
                .map(usos -> usos.isEmpty() ? null : usos.get(0));
    }

    @Override
    public Single<String> getOtraAseguradoraAmis(Integer claveAmis) {
        return getData().getOtraAseguradoraAmis(claveAmis);
    }

    @Override
    public Completable synchronizeData() {
        return Completable.mergeArrayDelayError(
                Completable.fromSingle(getSupervisorias()),
                super.synchronizeData()
        );
    }

}
