package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.data.catalogos.SiniestrosData;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoIdAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Configuracion;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CostoParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.EstadoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.MunicipioAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ParteAfectada;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ProductoCashflow;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Responsable;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.TipoLicencia;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.UsoVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.VehiculoAmis;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class SiniestrosInteractorOnlineImpl extends SyncInteractorMockImpl implements SiniestrosInteractor {

    private final SiniestrosData data = CatalogsFactory.getCatalogsSiniestros(DataType.ONLINE);

    @Override
    public Single<List<CatalogoClave>> getTiposAbogado(){
        return data.getTiposAbogado();
    }

    @Override
    public Single<CatalogoId> getAreaAfectada(Long id) {
        return data.getAreaAfectada(id);
    }

    @Override
    public Single<List<CatalogoClave>> getGestionesAbogado(){
        return data.getGestionesAbogado();
    }

    @Override
    public Single<CatalogoId> getObjetoPerdido(Long id) {
        return data.getObjetoPerdido(id);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosCancelacionAjuste(){
        return data.getMotivosCancelacionAjuste();
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosRechazoAjuste(){
        return data.getMotivosRechazoAjuste();
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosCierreAjuste(){
        return data.getMotivosCierreAjuste();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposAmbulancia(){
        return data.getTiposAmbulancia();
    }

    @Override
    public Single<List<CentroRemate>> getCentrosRemate(){
        return data.getCentrosRemate();
    }

    @Override
    public Single<List<Contratista>> getContratistas(){
        return data.getContratistas();
    }

    @Override
    public Single<Contratista> getContratista(Long idContratista) {
        return data.getContratista(idContratista);
    }

    @Override
    public Single<Boolean> getProductoCashflow(String comercial, String tecnico) {
        return data.getProductoCashflow(comercial, tecnico)
                .onErrorReturnItem(new ProductoCashflow())
                .map(result -> result.getProductoComercial() != null);
    }

    @Override
    public Single<List<CatalogoClave>> getDocumentosPropiedad(){
        return data.getDocumentosPropiedad();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposIdentificacion(){
        return data.getTiposIdentificacion();
    }

    @Override
    public Single<CatalogoClave> getTipoIdentificacion(String clave) {
        return data.getTipoIdentificacion(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposGrua(){
        return data.getTiposGrua();
    }

    @Override
    public Single<Hospital> getHospital(Long id){
        return data.getHospital(id);
    }

    @Override
    public Single<List<HospitalResumen>> getHospitales(String supervisoria){
        return data.getHospitales(supervisoria);
    }

    @Override
    public Single<List<CatalogoClave>> getMotivosInvestigacion(){
        return data.getMotivosInvestigacion();
    }

    @Override
    public Single<List<CatalogoId>> getNivelLesion(){
        return data.getNivelLesion();
    }

    @Override
    public Single<List<Area>> getAreasPartesLesionadas(){
        return data.getAreasLesionadas()
                .flatMapObservable(Observable::fromIterable)
                .flatMapSingle(area -> Single.zip(
                        Single.just(area),
                        data.getPartesLesionadas(area.getId()),
                        (areaLesionada, partes) -> {
                            Area a = new Area(areaLesionada.getId(), areaLesionada.getNombre());
                            a.setPartes(partes);
                            return a;
                        })
                ).toList();
    }

    @Override
    public Single<List<CatalogoId>> getAreasLesionadas(){
        return data.getAreasLesionadas();
    }

    @Override
    public Single<List<CatalogoIdAmis>> getPartesLesionadas(Long area){
        return data.getPartesLesionadas(area);
    }

    @Override
    public Single<List<CatalogoClave>> getNacionalidades(){
        return data.getNacionalidades();
    }

    @Override
    public Single<List<CatalogoId>> getRazonesNoDeducible(){
        return data.getRazonesNoDeducible();
    }

    @Override
    public Single<CatalogoId> getRazonNoDeducible(Long id) {
        return data.getRazonNoDeducible(id);
    }

    @Override
    public Single<List<CatalogoClave>> getPosicionesOcupante(){
        return data.getPosicionesOcupante();
    }

    @Override
    public Single<CatalogoClave> getPosicionOcupante(String clave){
        return data.getPosicionOcupante(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposPatrulla(){
        return data.getTiposPatrulla();
    }

    @Override
    public Single<List<CatalogoId>> getTiposPoliza(){
        return data.getTiposPoliza();
    }

    @Override
    public Single<CatalogoId> getCondicionamientosPoliza(Long id){
        return data.getCondicionamientosPoliza(id);
    }

    @Override
    public Single<List<CatalogoId>> getCondicionamientosPoliza() {
        return data.getCondicionamientosPoliza();
    }

    @Override
    public Single<List<CatalogoClave>> getVolantesRecuperacion(){
        return data.getVolantesRecuperacion();
    }

    @Override
    public Single<List<CatalogoId>> getTiposRecuperacionMonetaria(){
        return data.getTiposRecuperacionMonetaria();
    }

    @Override
    public Single<CatalogoId> getTipoRecuperacionMonetaria(Long id){
        return data.getTipoRecuperacionMonetaria(id);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposRecuperacionMonetariaTarjeta(){
        return data.getTiposRecuperacionMonetariaTarjeta();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposServicio(){
        return data.getTiposServicio();
    }

    @Override
    public Single<List<CatalogoClave>> getTiposSiniestro(){
        return data.getTiposSiniestro();
    }

    @Override
    public Single<List<Supervisoria>> getSupervisorias(){
        return data.getSupervisorias();
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposTelefono(){
        return data.getTiposTelefono();
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoTelefono(String clave) {
        return data.getTipoTelefono(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoTelefonoAmis(String claveAmis) {
        return data.getTipoTelefonoAmis(claveAmis);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposTercero(){
        return data.getTiposTercero();
    }


    @Override
    public Single<CatalogoClave> getTipoTercero(String clave){
        return data.getTipoTercero(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposTerceroAuto(){
        return data.getTiposTerceroAuto();
    }

    @Override
    public Single<List<CatalogoId>> getCategoriaTerceroNoAuto(){
        return data.getCategoriasTerceroNoAuto();
    }

    @Override
    public Single<List<CatalogoId>> getTiposTerceroNoAuto(Long categoria){
        return data.getTiposTerceroNoAuto(categoria);
    }

    @Override
    public Single<List<CatalogoClave>> getVolantesTerceroNoAuto(){
        return data.getVolantesTerceroNoAuto();
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposUbicacion(){
        return data.getTiposUbicacion();
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoUbicacion(String clave) {
        return data.getTipoUbicacion(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoUbicacionAmis(Long claveAmis) {
        return data.getTipoUbicacionAmis(claveAmis);
    }

    @Override
    public Single<List<Area>> getAreasPartesAfectadas(){
        return data.getAreasAfectadas()
                .flatMapObservable(Observable::fromIterable)
                .flatMapSingle(area -> Single.zip(
                        Single.just(new Area(area.getId(), area.getNombre())),
                        data.getPartesAfectadas(area.getId()),
                        (areaAfectada, partes) -> {
                            areaAfectada.setPartes(partes);
                            return areaAfectada;
                        })
                ).toList();
    }

    @Override
    public Single<List<CatalogoId>> getAreasAfectadas(){
        return data.getAreasAfectadas();
    }

    @Override
    public Single<List<ParteAfectada>> getPartesAfectadas(Long area){
        return data.getPartesAfectadas(area);
    }

    @Override
    public Single<ParteAfectada> getParteAfectada(Long area, Long id) {
        return data.getParteAfectada(area,id);
    }

    @Override
    public Single<ParteAfectada> getParteAfectada(Long id){
        return data.getParteAfectada(id);
    }

    @Override
    public Single<ParteAfectada> getParteAfectadaAmis(String claveAmis) {
        return data.getParteAfectadaAmis(claveAmis);
    }

    @Override
    public Single<List<ImpactoVehiculoAmis>> getImpactosVehiculo(){
        return data.getImpactosVehiculo();
    }

    @Override
    public Single<ImpactoVehiculoAmis> getImpactoVehiculo(Long id) {
        return data.getImpactoVehiculo(id);
    }

    @Override
    public Single<ImpactoVehiculoAmis> getImpactoVehiculoAmis(String claveAmis) {
        return data.getImpactoVehiculoAmis(claveAmis);
    }

    @Override
    public Single<List<CatalogoId>> getCircustanciasVehiculo(){
        return data.getCircustanciasVehiculo();
    }

    @Override
    public Single<CatalogoId> getCircustanciaVehiculo(Long id){
        return data.getCircustanciaVehiculo(id);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposVolante(){
        return data.getTiposVolante();
    }

    @Override
    public Single<List<Cobertura>> getCoberturasVolante(Integer categoria, String tipoSiniestro){
        return data.getCoberturasVolante(categoria, tipoSiniestro);
    }

    @Override
    public Single<List<CatalogoClave>> getComunicacionesAbogado() {
      return data.getComunicacionesAbogado();
    }

    @Override
    public Single<CatalogoIdAmis> getParteLesionada(Long id) {
        return data.getParteLesionada(id);
    }

    @Override
    public Single<CatalogoIdAmis> getParteLesionadaAmis(String claveAmis) {
        return data.getParteLesionadaAmis(claveAmis);
    }

    @Override
    public Single<CatalogoId> getCategoriaTerceroNoAuto(Long id) {
        return data.getCategoriaTerceroNoAuto(id);
    }

    @Override
    public Single<CatalogoId> getTipoTerceroNoAuto(Long categoria, Long id) {
        return data.getTipoTerceroNoAuto(categoria, id);
    }

    @Override
    public Single<Responsable> getResponsable(Long circustanciaA, Long circustanciaB){
        return data.getResponsables(circustanciaA, circustanciaB)
                .map(results -> results.get(0));
    }

    @Override
    public Single<CatalogoClave> getResponsabilidades(String clave) {
        return data.getResponsabilidad(clave);
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getGeneros() {
        return data.getGeneros();
    }

    @Override
    public Single<CatalogoClaveAmis> getGenero(String clave) {
        return data.getGenero(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getGeneroAmis(String claveAmis) {
        return data.getGeneroAmis(claveAmis);
    }

    @Override
    public Single<List<CostoParteAfectada>> getCostoParteAfectada(String categoria, List<Long> parte) {
        return data.getCostoParteAfectada(categoria, parte);
    }

    @Override
    public Single<Configuracion> getConfiguracion(String clave) {
        return data.getConfiguracion(clave);
    }

    @Override
    public Single<List<CatalogoClave>> getTiposLicencia(String estado) {
        return data.getTiposLicencia(estado);
    }

    @Override
    public Single<TipoLicencia> getTipoLicencia(String clave) {
        return data.getTipoLicencia(clave);
    }

    @Override
    public Single<List<CatalogoId>> getColoresVehiculo() {
        return data.getColoresVehiculo();
    }

    @Override
    public Single<CatalogoId> getColorVehiculo(Long id) {
        return data.getColorVehiculo(id);
    }

    @Override
    public Single<List<CatalogoClaveAmis>> getTiposPersona() {
        return data.getTiposPersona();
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoPersona(String clave) {
        return data.getTipoPersona(clave);
    }

    @Override
    public Single<CatalogoClaveAmis> getTipoPersonaAmis(Long claveAmis) {
        return data.getTipoPersonaAmis(claveAmis);
    }

    @Override
    public Single<MunicipioAmis> getMunicipio(String estado, String municipio) {
        return data.getMunicipio(estado,municipio);
    }

    @Override
    public Single<MunicipioAmis> getMunicipioAmis(Integer estadoAmis, Integer municipioAmis) {
        return data.getMunicipioAmis(estadoAmis,municipioAmis);
    }

    @Override
    public Single<EstadoAmis> getEstado(String estado) {
        return data.getEstado(estado);
    }

    @Override
    public Single<EstadoAmis> getEstadoAmis(Integer estadoAmis) {
        return data.getEstadoAmis(estadoAmis);
    }

    @Override
    public Single<VehiculoAmis> getVehiculoAmis(String tipoVehiculo, String armadora, String carroceria) {
        return data.getVehiculosAmis(tipoVehiculo, armadora, carroceria)
                .map(vehiculos -> vehiculos.isEmpty() ? null : vehiculos.get(0));
    }

    @Override
    public Single<VehiculoAmis> getVehiculoAmis(Long transporte, Long marca, Long tipo) {
        return data.getVehiculosAmis(transporte, marca, tipo)
                .map(vehiculos -> vehiculos.isEmpty() ? null : vehiculos.get(0));
    }

    @Override
    public Single<UsoVehiculo> getUsoVehiculoAmis(String usoVehiculo) {
        return data.getUsoVehiculosAmis(usoVehiculo)
                .map(usos -> usos.isEmpty() ? null : usos.get(0));
    }

    @Override
    public Single<String> getOtraAseguradoraAmis(Integer claveAmis) {
        return data.getOtraAseguradoraAmis(claveAmis);
    }

}
