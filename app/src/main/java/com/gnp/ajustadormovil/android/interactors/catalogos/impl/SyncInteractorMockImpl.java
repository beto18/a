package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.interactors.catalogos.SyncInteractor;

import io.reactivex.Completable;

public class SyncInteractorMockImpl implements SyncInteractor {

    private String TAG = this.getClass().getSimpleName();

    @Override
    public Completable synchronizeData() {
        Log.i(TAG, "Emulando sincronización.");
        return Completable.complete();
    }

    @Override
    public boolean isSynchronized() {
        return false;
    }

}
