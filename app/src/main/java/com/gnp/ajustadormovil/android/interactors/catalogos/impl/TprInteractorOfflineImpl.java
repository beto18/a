package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.data.catalogos.TprData;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Armadora;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Carroceria;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.io.File;
import java.util.List;

import io.reactivex.Single;

public class TprInteractorOfflineImpl extends BaseCatalogoInteractor<TprData> implements TprInteractor {

    private static final String DATA_NAME = "tpr";

    private static final File DATA_FILE = AjustadorMovilApplication.getInstance().getDatabasePath(Utils.DATA_TPR_NAME);

    public TprInteractorOfflineImpl() {
        super(CatalogsFactory.getCatalogosTpr(DataType.ONLINE), CatalogsFactory.getCatalogosTpr(DataType.OFFLINE));
    }

    @Override
    protected File getDataFile() {
        return DATA_FILE;
    }

    @Override
    protected String getDataName() {
        return DATA_NAME;
    }

    @Override
    public Single<List<CatalogoClave>> getTiposVehiculo(){
        return getData().getTiposVehiculo();
    }

    @Override
    public Single<CatalogoClave> getTipoVehiculo(String tipoVehiculo) {
        return getData().getTipoVehiculo(tipoVehiculo);
    }

    @Override
    public Single<List<CatalogoClave>> getArmadoras(String parameters){
        return getData().getArmadoras(parameters);
    }

    @Override
    public Single<Armadora> getArmadora(String tipoVehiculo, String armadora) {
        return getData().getArmadora(tipoVehiculo,armadora);
    }

    @Override
    public Single<List<CatalogoClave>> getCarrocerias(String tipoVehiculo, String tipoArmadora){
        return getData().getCarrocerias(tipoVehiculo,tipoArmadora);
    }

    @Override
    public Single<Carroceria> getCarroceria(String tipoVehiculo, String armadora, String carroceria) {
        return getData().getCarroceria(tipoVehiculo,armadora,carroceria);
    }

    @Override
    public Single<List<Short>>getModelos(String tipoVehiculo, String tipoArmadora, String tipoCarroceria){
        return getData().getModelos(tipoVehiculo,tipoArmadora,tipoCarroceria);
    }

    @Override
    public Single<List<VersionVehiculoResumen>>getVersiones(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int year){
        return getData().getVersiones(tipoVehiculo,tipoArmadora,tipoCarroceria,year);
    }

    @Override
    public Single<Cobertura> getCobertura(String clave) {
        return getData().getCobertura(clave);
    }

    @Override
    public Single<VersionVehiculo> getVersion(String tipoVehiculo, String armadora, String carroceria, Integer modelo, String version) {
        return getData().getVersion(tipoVehiculo,armadora,carroceria,modelo,version);
    }

}
