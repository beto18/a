package com.gnp.ajustadormovil.android.interactors.catalogos.impl;

import com.gnp.ajustadormovil.android.data.DataType;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.data.catalogos.TprData;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Armadora;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.Carroceria;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;

import java.util.List;

import io.reactivex.Single;

public class TprInteractorOnlineImpl extends SyncInteractorMockImpl implements TprInteractor {

    private final TprData data = CatalogsFactory.getCatalogosTpr(DataType.ONLINE);

    @Override
    public Single<List<CatalogoClave>> getTiposVehiculo(){
        return data.getTiposVehiculo();
    }

    @Override
    public Single<CatalogoClave> getTipoVehiculo(String tipoVehiculo) {
        return data.getTipoVehiculo(tipoVehiculo);
    }

    @Override
    public Single<List<CatalogoClave>> getArmadoras(String parameters){
        return data.getArmadoras(parameters);
    }

    @Override
    public Single<Armadora> getArmadora(String tipoVehiculo, String armadora) {
        return data.getArmadora(tipoVehiculo,armadora);
    }

    @Override
    public Single<List<CatalogoClave>> getCarrocerias(String tipoVehiculo, String tipoArmadora){
        return data.getCarrocerias(tipoVehiculo,tipoArmadora);
    }

    @Override
    public Single<Carroceria> getCarroceria(String tipoVehiculo, String armadora, String carroceria) {
        return data.getCarroceria(tipoVehiculo,armadora,carroceria);
    }

    @Override
    public Single<List<Short>>getModelos(String tipoVehiculo, String tipoArmadora, String tipoCarroceria){
        return data.getModelos(tipoVehiculo,tipoArmadora,tipoCarroceria);
    }

    @Override
    public Single<List<VersionVehiculoResumen>>getVersiones(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int year){
        return data.getVersiones(tipoVehiculo,tipoArmadora,tipoCarroceria,year);
    }

    @Override
    public Single<Cobertura> getCobertura(String clave) {
        return data.getCobertura(clave);
    }

    @Override
    public Single<VersionVehiculo> getVersion(String tipoVehiculo, String armadora, String carroceria, Integer modelo, String version) {
        return data.getVersion(tipoVehiculo,armadora,carroceria,modelo,version);
    }

}