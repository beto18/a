package com.gnp.ajustadormovil.android.interactors.context;

import com.gnp.ajustadormovil.android.data.context.AuthenticationData;
import com.gnp.ajustadormovil.android.data.context.ContextDataFactory;
import com.gnp.ajustadormovil.android.data.context.MetadataData;
import com.gnp.ajustadormovil.android.models.VersionResponse;
import com.gnp.ajustadormovil.android.models.context.Login;
import com.gnp.ajustadormovil.android.models.context.LoginCommand;
import com.gnp.ajustadormovil.android.models.login.User;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ContextInteractor {

    private final AuthenticationData loginData = ContextDataFactory.getAuthenticationData();

    private final MetadataData metadataData = ContextDataFactory.getMetadataData();

    private static ContextInteractor INSTANCE;

    private ContextInteractor(){ }

    public static ContextInteractor getInstance(){
        synchronized(ContextInteractor.class){
            if(INSTANCE == null){
                INSTANCE = new ContextInteractor();
            }
            return INSTANCE;
        }
    }

    public Single<Login> login(LoginCommand command){
        return loginData
                .login(command)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<User> refreshLogin(User user){
        return loginData
                .getRefreshToken(user);
    }

    public Single<VersionResponse> getLastVersion(Integer app, Integer os){
        return metadataData
                .getLastVersion(app, os)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
