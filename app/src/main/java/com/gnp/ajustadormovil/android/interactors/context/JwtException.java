package com.gnp.ajustadormovil.android.interactors.context;

public class JwtException extends RuntimeException {

    public JwtException(String message){
        super(message);
    }

}
