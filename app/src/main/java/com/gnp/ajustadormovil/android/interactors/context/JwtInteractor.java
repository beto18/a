package com.gnp.ajustadormovil.android.interactors.context;

import android.util.Base64;

import com.gnp.ajustadormovil.android.models.context.Token;
import com.gnp.ajustadormovil.android.models.context.Usuario;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public final class JwtInteractor {

    private static final Gson gson = new Gson();

    private static final long EXPIRE_TIME = 60 * 1000;

    private static String getBody(String token) {
        if(token == null || token.isEmpty()){
            throw new JwtException("Token inválido");
        }
        String [] parts = token.split("\\.");
        if(parts.length != 3){
            throw new JwtException("Token inválido");
        }
        return new String(Base64.decode(parts[1], Base64.DEFAULT));
    }

    public static Token parse(String token) {
        try {
            return gson.fromJson(getBody(token), Token.class);
        }catch (JsonSyntaxException e){
            throw new JwtException("Formato de token inválido");
        }
    }

    public static Usuario getUser(String token) {
        return parse(token).getUsuario();
    }

    public static boolean isExpired(String token) {
        return (parse(token).getExpiration() * 1000) < (System.currentTimeMillis() - EXPIRE_TIME);
    }
}
