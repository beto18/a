package com.gnp.ajustadormovil.android.interactors.orquestador;


import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.models.login.ChangePassword;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Dispositivo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.EstatusCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Horario;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.PosicionCommand;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AjustadorInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<List<Horario>> getHorario();

    @RxLoading
    @RxApplySchedulers
    Single<Ajustador> getAjustador();

    @RxLoading
    @RxApplySchedulers
    Single<Dispositivo> addDispositivo(Dispositivo dispositivo);

    @RxApplySchedulers
    Completable modifyUbicacion(PosicionCommand posicion);

    @RxLoading
    @RxApplySchedulers
    Completable modifyStatus(EstatusCommand estatus);

    @RxLoading
    @RxApplySchedulers
    default Completable init(){
        return Completable.complete();
    }

    @RxLoading
    @RxApplySchedulers
    Completable resetPassword(String email);

    @RxLoading
    @RxApplySchedulers
    Completable changePassword(ChangePassword changePassword);



    @RxLoading
    @RxApplySchedulers
    default Completable cerrarSesion(){
        return Completable.create(e -> {
            AppPreferences.cleanAll();
        });
    }

}
