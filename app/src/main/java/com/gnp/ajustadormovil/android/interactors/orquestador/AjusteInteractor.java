package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.InfoEstatus;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AjusteInteractor extends SyncInteractor, EstatusInteractor<DeclaracionResumen> {

    @RxLoading
    @RxApplySchedulers
    Single<List<Ajuste>> getHistorial(String date);

    @RxLoading
    @RxApplySchedulers
    Single<List<Ajuste>> getAjustes();

    @RxLoading
    @RxApplySchedulers
    Single<Ajuste> getAjuste(String id);

    @RxLoading
    @RxApplySchedulers
    Completable rechazar(String id, String motivo);

    @RxLoading
    @RxApplySchedulers
    Completable confirmar(String id);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> arribar(String id, File image);

    @RxLoading
    @RxApplySchedulers
    Completable terminando(String id);

    @RxApplySchedulers
    Completable cerrar(String id, String motivo, String conclusion, boolean backgroud);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> getDeclaracion(String id, DeclaracionCommand declaracion);

    @RxApplySchedulers
    Single<Fotografia> addEvidenciasDeclaracion(String id,File images);

    @RxLoading
    @RxApplySchedulers
    Completable modifyTipoUbicacion(String id, String tipoUbicacion);

    @RxLoading
    @RxApplySchedulers
    Completable addDeducibleAdministrativo(String id);

    @RxLoading
    @RxApplySchedulers
    Completable deleteDeducibleAdministrativo(String id);

    boolean canCerrar(InfoEstatus estatus);

    @RxApplySchedulers
    Completable evolucionar(String ajuste);

}
