package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface AseguradoInteractor extends SyncInteractor, EstatusInteractor<AseguradoResumen> {

    @RxLoading
    @RxApplySchedulers
    Single<Asegurado> get(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Completable modifyConductor(String ajuste, Persona conductor);

    @RxLoading
    @RxApplySchedulers
    Single<Persona> getConductor(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<List<Fotografia>> getFotografiasConductor(String ajuste);

    @RxApplySchedulers
    Single<Fotografia> addFotografiaConductor(String ajuste, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografiaConductor(String ajuste, Long idFotografia);

    @RxLoading(message = "Obteniendo datos del vehículo")
    @RxApplySchedulers
    Single<VehiculoAsegurado> getVehiculo(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Completable modifyVehiculo(String ajuste, VehiculoAsegurado vehiculo);

    @RxLoading
    @RxApplySchedulers
    Observable<List<Fotografia>> getFotografiasVehiculo(String ajuste);

    @RxApplySchedulers
    Observable<Fotografia> addFotografiaVehiculo(String ajuste, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografiaVehiculo(String ajuste, Long idFotografia);

    @RxLoading
    @RxApplySchedulers
    Single<List<Ocupante>> getOcupantes(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<Ocupante> getOcupante(String ajuste, Long idOcupante);

    @RxLoading
    @RxApplySchedulers
    Single<Ocupante> addOcupante(String ajuste, Ocupante ocupante);

    @RxLoading
    @RxApplySchedulers
    Completable modifyOcupante(String ajuste, Long idOcupante, Ocupante ocupante);

    @RxLoading
    @RxApplySchedulers
    Completable deleteOcupante(String ajuste, Long idOcupante);

    @RxLoading
    @RxApplySchedulers
    Single<List<Fotografia>> getFotografiasOcupante(String ajuste, Long idOcupante);

    @RxApplySchedulers
    Single<Fotografia> addFotografiaOcupante(String ajuste, Long idOcupante, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografiaOcupante(String ajuste, Long idOcupante, Long idFotografia);

}
