package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.AjusteOptions;
import com.gnp.ajustadormovil.android.utils.core.ConnectivityUtils;

public final class ConnectionProvider {

    private static ConnectionProvider INSTANCE;

    private final Repository<AjusteOptions, String> repository = RepositoryFactory.getRepository(AjusteOptions.class, String.class);

    public static ConnectionProvider getInstance(){
        synchronized (ConnectionProvider.class){
            if(INSTANCE == null){
                INSTANCE = new ConnectionProvider();
            }
            return INSTANCE;
        }
    }

    public boolean isConnected(){
        return ConnectivityUtils.isConnected();
    }

    public boolean isConnected(String ajuste){
        synchronized (AjusteOptions.class) {
            AjusteOptions options = getOptions(ajuste);
            if(options.getConectado() == null){
                options.setConectado(ConnectivityUtils.isConnected());
                repository.save(options);
            }
            return options.getConectado();
        }
    }

    public void notConnected(String ajuste){
        synchronized (AjusteOptions.class) {
            AjusteOptions options = getOptions(ajuste);
            options.setConectado(false);
            repository.save(options);
        }
    }

    public void connected(String ajuste){
        synchronized (AjusteOptions.class) {
            AjusteOptions options = getOptions(ajuste);
            options.setConectado(true);
            repository.save(options);
        }
    }

    public void clean(String ajuste){
        synchronized (AjusteOptions.class) {
            repository.deleteById(ajuste);
        }
    }

    private AjusteOptions getOptions(String ajuste){
        AjusteOptions options = repository.findById(ajuste);
        if (options == null) {
            options = new AjusteOptions();
            options.setAjuste(ajuste);
            repository.save(options);
        }
        return options;
    }

}
