package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;

import io.reactivex.Observable;


public interface EstatusInteractor<T> {

    @RxApplySchedulers
    default Observable<T> getEstatus(String ajuste){
        return Observable.empty();
    }

}
