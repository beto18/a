package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.Aspects;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.amis.impl.AmisInteractorDefaultImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.AjustadorInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.AjusteInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.AseguradoInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.CompositeSyncAjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.PolizaInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.RecuperacionInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.ResponsabilidadInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.ServiciosCoberturasOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.TerceroAutoInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.TerceroInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.TerceroNoAutoInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.TerceroPersonaInteractorOfflineImpl;
import com.gnp.ajustadormovil.android.interactors.orquestador.impl.VolanteInteractorOfflineImpl;

public final class OrquestadorInteractorFactory {

    private volatile static AjusteInteractor ajusteInteractor;

    private volatile static AjustadorInteractor ajustadorInteractor;

    private volatile static AseguradoInteractor aseguradoInteractor;

    private volatile static TerceroInteractor terceroService;

    private volatile static TerceroAutoInteractor terceroAutoService;

    private volatile static TerceroNoAutoInteractor tercerosNoAutoService;

    private volatile static TerceroPersonaInteractor tercerosPersonaService;

    private volatile static ResponsabilidadInteractor responsabilidadesServices;

    private volatile static ServiciosCoberturaInteractor serviciosCoberturaInteractor;

    private volatile static RecuperacionInteractor recuperacionInteractor;

    private volatile static PolizaInteractor polizaInteractor;

    private volatile static VolanteInteractor volanteService;

    private volatile static SyncInteractor syncAjusteService;

    private volatile static AmisInteractor amisInteractor;

    public static AjustadorInteractor getAjustadorInteractor(){
        synchronized (AjustadorInteractor.class){
            if(ajustadorInteractor == null){
                ajustadorInteractor = Aspects
                        .aspect(AjustadorInteractor.class, new AjustadorInteractorOfflineImpl());
            }
            return ajustadorInteractor;
        }
    }

    public static RecuperacionInteractor getRecuperacionInteractor(){
        synchronized ((AjustadorInteractor.class)){
            if (recuperacionInteractor == null){
                recuperacionInteractor = Aspects
                        .aspect(RecuperacionInteractor.class, new RecuperacionInteractorOfflineImpl());
            }
            return recuperacionInteractor;
        }
    }

    public static AjusteInteractor getAjusteInteractor(){
        synchronized (AjusteInteractor.class){
            if(ajusteInteractor == null){
                ajusteInteractor = Aspects
                        .aspect(AjusteInteractor.class, new AjusteInteractorOfflineImpl());
            }
            return ajusteInteractor;
        }
    }

    public static ServiciosCoberturaInteractor getServiciosCoberturaInteractor(){
        synchronized (AjusteInteractor.class){
            if(serviciosCoberturaInteractor == null){
                serviciosCoberturaInteractor = Aspects
                        .aspect(ServiciosCoberturaInteractor.class, new ServiciosCoberturasOfflineImpl());
            }
            return serviciosCoberturaInteractor;
        }
    }

    public static AseguradoInteractor getAseguradoInteractor(){
        synchronized (AseguradoInteractor.class){
            if(aseguradoInteractor == null){
                aseguradoInteractor = Aspects
                        .aspect(AseguradoInteractor.class, new AseguradoInteractorOfflineImpl());
            }
            return aseguradoInteractor;
        }
    }

    public static TerceroInteractor getTerceroService(){
        synchronized (TerceroInteractor.class){
            if(terceroService == null){
                terceroService = Aspects
                        .aspect(TerceroInteractor.class, new TerceroInteractorOfflineImpl());
            }
            return terceroService;
        }
    }

    public static TerceroAutoInteractor getTerceroAutoService(){
        synchronized (TerceroAutoInteractor.class){
            if(terceroAutoService == null){
                terceroAutoService = Aspects
                        .aspect(TerceroAutoInteractor.class, new TerceroAutoInteractorOfflineImpl());
            }
            return terceroAutoService;
        }
    }

    public static TerceroNoAutoInteractor getTerceroNoAutoService(){
        synchronized (TerceroNoAutoInteractor.class){
            if(tercerosNoAutoService == null){
                tercerosNoAutoService = Aspects
                        .aspect(TerceroNoAutoInteractor.class, new TerceroNoAutoInteractorOfflineImpl());
            }
            return tercerosNoAutoService;
        }
    }

    public static TerceroPersonaInteractor getTerceroPersonaService(){
        synchronized (TerceroPersonaInteractor.class){
            if(tercerosPersonaService == null){
                tercerosPersonaService = Aspects
                        .aspect(TerceroPersonaInteractor.class, new TerceroPersonaInteractorOfflineImpl());
            }
            return tercerosPersonaService;
        }
    }

    public static ResponsabilidadInteractor getResponsabilidadesService(){
        synchronized (ResponsabilidadInteractor.class){
            if(responsabilidadesServices == null){
                responsabilidadesServices = Aspects
                        .aspect(ResponsabilidadInteractor.class, new ResponsabilidadInteractorOfflineImpl());
            }
            return responsabilidadesServices;
        }
    }

    public static PolizaInteractor getPolizaInteractor(){
        synchronized (PolizaInteractor.class){
            if(polizaInteractor == null){
                polizaInteractor = Aspects
                        .aspect(PolizaInteractor.class, new PolizaInteractorOfflineImpl());
            }
            return polizaInteractor;
        }
    }

    public static VolanteInteractor getVolanteService(){
        synchronized (VolanteInteractor.class){
            if(volanteService == null){
                volanteService = Aspects
                        .aspect(VolanteInteractor.class, new VolanteInteractorOfflineImpl());
            }
            return volanteService;
        }
    }
    public static AmisInteractor getAmisInteractor(){
        synchronized (AmisInteractor.class){
            if(amisInteractor == null){
                amisInteractor = Aspects
                        .aspect(AmisInteractor.class, new AmisInteractorDefaultImpl());
            }
            return amisInteractor;
        }
    }

    public static SyncInteractor getSyncService(){
        synchronized (SyncInteractor.class){
            if(syncAjusteService == null){
                CompositeSyncAjusteInteractor service = new CompositeSyncAjusteInteractor();
                service.addService(getAjusteInteractor(), CompositeSyncAjusteInteractor.MAX_PRIORITY);
                service.addService(getAseguradoInteractor(), CompositeSyncAjusteInteractor.HIGH_PRIORITY);
                service.addService(getTerceroAutoService(), CompositeSyncAjusteInteractor.HIGH_PRIORITY);
                service.addService(getTerceroPersonaService(), CompositeSyncAjusteInteractor.HIGH_PRIORITY);
                service.addService(getTerceroNoAutoService(), CompositeSyncAjusteInteractor.HIGH_PRIORITY);
                service.addService(getTerceroService(), CompositeSyncAjusteInteractor.MEDIUM_PRIORITY);
                service.addService(getResponsabilidadesService(), CompositeSyncAjusteInteractor.MEDIUM_PRIORITY);
                service.addService(getRecuperacionInteractor(), CompositeSyncAjusteInteractor.LOW_PRIORITY);
                service.addService(getServiciosCoberturaInteractor(), CompositeSyncAjusteInteractor.MEDIUM_PRIORITY);
                service.addService(getVolanteService(), CompositeSyncAjusteInteractor.LOW_PRIORITY);
                service.addService(getAmisInteractor(), CompositeSyncAjusteInteractor.LOW_PRIORITY);
                syncAjusteService = Aspects.aspect(SyncInteractor.class, service);
            }
            return syncAjusteService;
        }
    }

}
