package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.SerieResponse;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Costo;

import java.util.List;

import io.reactivex.Single;

public interface PolizaInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<Costo> getCostosAsegurado(String categoria, List<Long> partes);

    @RxLoading
    @RxApplySchedulers
    Single<List<SerieResponse>> getPolizas(String vin);

    @RxLoading
    @RxApplySchedulers
    Single<Costo> getCostosTercero(String categoria, List<Long> partes);

}
