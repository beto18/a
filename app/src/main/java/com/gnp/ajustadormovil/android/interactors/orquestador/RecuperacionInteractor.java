package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Comprobante;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface RecuperacionInteractor extends SyncInteractor, EstatusInteractor<Resumen> {

    @RxLoading
    @RxApplySchedulers
    Single<List<Recuperacion>> getRecuperaciones(String idAjuste,
                                                 Long idTercero);

    @RxLoading
    @RxApplySchedulers
    Single<Recuperacion> getRecuperacion(String idAjuste,
                                             Long idTercero,
                                             Long idRecuperacion);

    @RxLoading
    @RxApplySchedulers
    Completable deleteRecuperacion(String idAjuste,
                                   Long idTercero,
                                   Long idRecuperacion);

    @RxLoading
    @RxApplySchedulers
    Single<RecuperacionMonetaria> addMonetaria(String idAjuste,
                                                   Long idTercero,
                                                   RecuperacionMonetaria recuperacion);

    @RxLoading
    @RxApplySchedulers
    Single<RecuperacionTurnadoAsistenciaLegal> addTurnadoAsistenciaLegal(String idAjuste,
                                                                             Long idTercero,
                                                                             RecuperacionTurnadoAsistenciaLegal recuperacion);

    @RxLoading
    @RxApplySchedulers
    Completable modifyTurnadoAsistenciaLegal(String idAjuste,
                                                                             Long idTercero,
                                                                             Long idRecuperacion,
                                                                             RecuperacionTurnadoAsistenciaLegal recuperacion);

    @RxLoading
    @RxApplySchedulers
    Single<RecuperacionMesaControversia> addMesaControversia(String idAjuste,
                                                                 Long idTercero,
                                                                 RecuperacionMesaControversia recuperacion);
    @RxLoading
    @RxApplySchedulers
    Completable modifyMesaControversia(String idAjuste,
                                                                 Long idTercero,
                                                                 Long idRecuperacion,
                                                                 RecuperacionMesaControversia recuperacion);

    @RxLoading
    @RxApplySchedulers
    Single<RecuperacionGarantiaPrendaria> addGarantiaPrendaria(String idAjuste,
                                                                   Long idTercero,
                                                                   RecuperacionGarantiaPrendaria recuperacion);

    @RxLoading
    @RxApplySchedulers
    Single<RecuperacionCompaniaSeguros> addCompaniaSeguros(String idAjuste,
                                                               Long idTercero,
                                                               RecuperacionCompaniaSeguros compania);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> addComprobante(String idAjuste,
                                       Long idTercero,
                                       Long idRecuperacion,
                                       Comprobante comprobante);

    @RxApplySchedulers
    Single<Fotografia> addFotografia(String idAjuste,
                                               Long idTercero,
                                               Long idRecuperacion,
                                               File image);

    @RxLoading
    @RxApplySchedulers
    Single<List<Fotografia>> getFotografias(String idAjuste,
                                                Long idTercero,
                                                Long idRecuperacion);

}
