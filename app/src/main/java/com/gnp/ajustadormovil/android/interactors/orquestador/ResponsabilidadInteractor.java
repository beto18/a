package com.gnp.ajustadormovil.android.interactors.orquestador;


import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;

import java.io.File;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface ResponsabilidadInteractor extends SyncInteractor, EstatusInteractor<Resumen> {

    @RxLoading
    @RxApplySchedulers
    Single<Responsabilidad> addResponsabilidad(String idAjuste, Responsabilidad responsabilidad);

    @RxLoading
    @RxApplySchedulers
    Single<Fotografia> addFotografia(String ajuste, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteResponsabilidad(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<Responsabilidad> getResponsabilidad(String ajuste);
}
