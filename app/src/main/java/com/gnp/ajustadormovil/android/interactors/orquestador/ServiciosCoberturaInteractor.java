package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.ComentarioCancelacion;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.services.ArriboServicio;
import com.gnp.ajustadormovil.android.models.services.ServicioResponse;
import com.gnp.ajustadormovil.android.models.services.service.ServiceData;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface ServiciosCoberturaInteractor extends SyncInteractor, EstatusInteractor<Resumen> {

    @RxLoading
    @RxApplySchedulers
    Completable sendStatus(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<List<ServicioResponse>> getServicioCobertura(String servicioAjuste);

    @RxLoading
    @RxApplySchedulers
    Completable getServicioCoberturaDelete(String servicioAjuste, int idServicio,ComentarioCancelacion data);

    @RxLoading
    @RxApplySchedulers
    Single<String> getServicioCoberturaArribo(ArriboServicio arriboServicio);

    @RxLoading
    @RxApplySchedulers
    Completable sendPolices( String id_ajuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendLawyer( String id_ajuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendCrane( String id_ajuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendAmbulance( String idAjuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendChecker( String idAjuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendRecuperator( String idAjuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendFunerary( String idAjuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendContractor( String idAjuste, ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendTaxi( String idAjuste,  ServiceData data);

    @RxLoading
    @RxApplySchedulers
    Completable sendArribo( String idAjuste, String id);


    @RxLoading
    @RxApplySchedulers
    Completable addResumen(String ajuste, List<String> servicios);

    @RxLoading
    @RxApplySchedulers
    Single<List<String>> getResumen(String idAjuste);
}
