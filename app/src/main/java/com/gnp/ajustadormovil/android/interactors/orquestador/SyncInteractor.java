package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;

import io.reactivex.Completable;

public interface SyncInteractor {

    /**
     * Sincroniza la información de los datos.
     */
    @RxApplySchedulers
    default Completable synchronizeData(String ajuste){
        return Completable.complete();
    }

    /**
     * Obtiene si los datos están sincronizados.
     * @return Si esta sincronizado.
     */
    default boolean isSynchronized(String ajuste){
        return true;
    }

    /**
     * Inicializa los datos del servidor.
     * @param ajuste Ajuste para inicializar.
     * @return Cuando esta completo.
     */
    @RxApplySchedulers
    default Completable initialize(String ajuste){
        return Completable.complete();
    }

    /**
     * Finaliza los datos del servidor.
     * @param ajuste Ajuste para terminar.
     * @return Cuando esta completo.
     */
    @RxApplySchedulers
    default Completable terminate(String ajuste){
        return Completable.complete();
    }

}
