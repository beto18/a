package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface TerceroAutoInteractor extends SyncInteractor, EstatusInteractor<AfectadoResumen> {

    @RxLoading
    @RxApplySchedulers
    Single<List<TerceroAuto>> getAll(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<TerceroAuto> add(String ajustes);

    @RxLoading
    @RxApplySchedulers
    Single<TerceroAuto> get(String ajuste, long idTercero);

    @RxLoading
    @RxApplySchedulers
    Single<TerceroAuto> get(String ajuste, String folioAmis);

    @RxLoading
    @RxApplySchedulers
    Completable delete(String ajuste, long idTercero);

    @RxLoading
    @RxApplySchedulers
    Completable modifyConductor(String ajuste, long idTercero, Persona conductor);

    @RxLoading
    @RxApplySchedulers
    Single<List<Fotografia>> getFotografiasConductor(String ajuste, long idTercero);

    @RxApplySchedulers
    Single<Fotografia> addFotografiaConductor(String ajuste, long idTercero, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografiaConductor(String ajuste, long idTercero, long idFotografia);

    @RxLoading
    @RxApplySchedulers
    Single<VehiculoTercero> getVehiculo(String ajuste, long idTercero);

    @RxLoading
    @RxApplySchedulers
    Completable modifyVehiculo(String ajuste, long idTercero, VehiculoTercero vehiculo);

    @RxLoading
    @RxApplySchedulers
    Observable<List<Fotografia>> getFotografiaVehiculo(String ajuste, long idTercero);

    @RxApplySchedulers
    Observable<Fotografia> addFotografiaVehiculo(String ajuste, long idTercero, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografiaVehiculo(String ajuste, long idTercero, long idFotografia);

    @RxLoading
    @RxApplySchedulers
    Single<List<Ocupante>> getOcupantes(String ajuste, long idTercero);

    @RxLoading
    @RxApplySchedulers
    Single<Ocupante> addOcupante(String ajuste, long idTercero, Ocupante ocupante);

    @RxLoading
    @RxApplySchedulers
    Single<Ocupante> getOcupante(String ajuste, long idTercero, long idOcupante);

    @RxLoading
    @RxApplySchedulers
    Completable modifyOcupante(String ajuste, long idTercero, long idOcupante, Ocupante ocupante);

    @RxLoading
    @RxApplySchedulers
    Completable deleteOcupante(String ajuste, long idTercero, long idOcupante);

    @RxLoading
    @RxApplySchedulers
    Single<List<Fotografia>> getFotografiasOcupante(String ajuste, long idTercero, long idOcupante);

    @RxApplySchedulers
    Single<Fotografia> addFotografiaOcupante(String ajuste, long idTercero, long idOcupante, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografiaOcupante(String ajuste, long idTercero, long idOcupante, long idFotografia);

}
