package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;

import java.util.List;

import io.reactivex.Single;

public interface TerceroInteractor extends SyncInteractor {

    @RxLoading
    @RxApplySchedulers
    Single<List<Tercero>> getAll(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<Tercero> get(String ajuste, long id);

}
