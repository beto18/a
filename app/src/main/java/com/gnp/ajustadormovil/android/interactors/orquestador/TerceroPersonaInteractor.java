package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface TerceroPersonaInteractor extends SyncInteractor, EstatusInteractor<AfectadoResumen> {

    @RxLoading
    @RxApplySchedulers
    Single<List<TerceroPersona>> getAll(String ajuste);

    @RxLoading
    @RxApplySchedulers
    Single<TerceroPersona> add(String ajuste, Persona tercero);

    @RxLoading
    @RxApplySchedulers
    Single<TerceroPersona> get(String ajuste, long idTercero);

    @RxLoading
    @RxApplySchedulers
    Completable delete(String ajuste, long idTercero);

    @RxLoading
    @RxApplySchedulers
    Completable modify(String ajuste, long idTercero, Persona tercero);

    @RxLoading
    @RxApplySchedulers
    Single<List<Fotografia>> getFotografias(String ajuste, long idTercero);

    @RxApplySchedulers
    Single<Fotografia> addFotografia(String ajuste, long idTercero, File image);

    @RxLoading
    @RxApplySchedulers
    Completable deleteFotografia(String ajuste, long idTercero, long idFotografia);

}
