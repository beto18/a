package com.gnp.ajustadormovil.android.interactors.orquestador;

import com.gnp.ajustadormovil.android.aspects.RxApplySchedulers;
import com.gnp.ajustadormovil.android.aspects.RxLoading;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VolanteVehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.ArchivoEnviado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;

import io.reactivex.Single;

public interface VolanteInteractor extends SyncInteractor, EstatusInteractor<Resumen> {

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateAseguradoConductor(String ajuste, VolanteMedico volante);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateAseguradoVehiculo(String ajuste, VolanteVehiculoAsegurado volante);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateAseguradoOcupante(String ajuste, Long idOcupante, VolanteMedico conductor);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateTerceroAutoConductor(String ajuste, long idTercero, VolanteMedico conductor);

    @RxLoading
    @RxApplySchedulers
    Single<ArchivoEnviado> generateTerceroAutoVehiculo(String ajuste, long idTercero, VolanteVehiculoTercero volante);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateTerceroAutoOcupante(String ajuste, long idTercero, long idOcupante, VolanteMedico volante);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateTerceroNoAuto(String ajuste, long idTercero, VolanteTerceroNoAuto volante);

    @RxLoading
    @RxApplySchedulers
    Single<Archivo> generateTerceroPersona(String ajuste, long idTercero, VolanteMedico volante);

}
