package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import com.gnp.ajustadormovil.android.utils.commons.DateUtils;

import java.util.Date;

public class DeclaracionBuilder extends DocumentBuilder {

    public static final String TEMPLATE = "documents/declaracion.html";

    public static final String TIPO_DOCUMENTO = "14";

    public static final String [] PARAMS = {
            "fechaSiniestro",
            "fechaAtencion",
            "titularPoliza",
            "numeroPoliza",
            "estatusPoliza",
            "vigenciaPoliza",
            "vehiculo",
            "modelo",
            "tipoVehiculo",
            "numeroSerie",
            "nombre",
            "apellidoPaterno",
            "apellidoMaterno",
            "fechaNacimiento",
            "ubicacionSiniestro",
            "causaSiniestro",
            "narrativa",
            "firma",
            "cliente"
    };

    public DeclaracionBuilder(String siniestro, String cedula){
        super(TEMPLATE, TIPO_DOCUMENTO, siniestro, cedula);
    }

    @Override
    protected String[] getParams() {
        return PARAMS;
    }


    public DeclaracionBuilder fechaSiniestro(Date fechaSiniestro){
        if (fechaSiniestro != null)
            putParam("fechaSiniestro", DateUtils.datetime(fechaSiniestro));
        return this;
    }

    public DeclaracionBuilder fechaAtencion(Date fechaAtencion){
        if (fechaAtencion != null)
            putParam("fechaAtencion", DateUtils.date(fechaAtencion));
        return this;
    }

    public DeclaracionBuilder titularPoliza(String titularPoliza){
        putParam("titularPoliza", titularPoliza);
        return this;
    }

    public DeclaracionBuilder numeroPoliza(String numeroPoliza){
        putParam("numeroPoliza", numeroPoliza);
        return this;
    }

    public DeclaracionBuilder estatusPoliza(String estatusPoliza){
        putParam("estatusPoliza", estatusPoliza);
        return this;
    }

    public DeclaracionBuilder vigenciaPoliza(Date vigenciaPoliza){
        if(vigenciaPoliza != null) {
            putParam("vigenciaPoliza", DateUtils.date(vigenciaPoliza));
        }
        return this;
    }

    public DeclaracionBuilder vehiculo(String vehiculo){
        putParam("vehiculo", vehiculo);
        return this;
    }

    public DeclaracionBuilder modelo(Short modelo){
        if(modelo != null) {
            putParam("modelo", modelo.toString());
        }
        return this;
    }

    public DeclaracionBuilder tipoVehiculo(String tipoVehiculo){
        putParam("tipoVehiculo", tipoVehiculo);
        return this;
    }

    public DeclaracionBuilder numeroSerie(String numeroSerie){
        putParam("numeroSerie", numeroSerie);
        return this;
    }

    public DeclaracionBuilder nombre(String nombre){
        putParam("nombre", nombre);
        return this;
    }

    public DeclaracionBuilder apellidoPaterno(String apellidoPaterno){
        putParam("apellidoPaterno", apellidoPaterno);
        return this;
    }

    public DeclaracionBuilder apellidoMaterno(String apellidoMaterno){
        putParam("apellidoMaterno", apellidoMaterno);
        return this;
    }

    public DeclaracionBuilder fechaNacimiento(Date fechaNacimiento){
        if (fechaNacimiento != null)
            putParam("fechaNacimiento", DateUtils.date(fechaNacimiento));
        return this;
    }

    public DeclaracionBuilder ubicacionSiniestro(String ubicacionSiniestro){
        putParam("ubicacionSiniestro", ubicacionSiniestro);
        return this;
    }

    public DeclaracionBuilder causaSiniestro(String causaSiniestro){
        putParam("causaSiniestro", causaSiniestro);
        return this;
    }
    public DeclaracionBuilder narrativa(String narrativa){
        putParam("narrativa", narrativa);
        return this;
    }
    public DeclaracionBuilder firma(String firma){
        putImageParam("firma", firma);
        return this;
    }

    public DeclaracionBuilder cliente(String cliente) {
        putParam("cliente", cliente);
        return this;
    }

}
