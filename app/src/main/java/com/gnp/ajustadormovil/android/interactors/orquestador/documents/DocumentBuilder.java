package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import android.app.Activity;
import android.print.PdfPrint;
import android.print.PrintAttributes;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.media.FileUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;

public abstract class DocumentBuilder {

    private static final String TAG = DocumentBuilder.class.getSimpleName();

    private final Map<String, String> params;

    private String asset;

    private static final String DEFAULT_VALUE = "";

    private static final String DEFAULT_BOOLEAN_VALUE = "No";

    private static final int DEFAULT_WIDTH_IN = 3;

    private static final int DEFAULT_HEIGTH_IN = 40;

    private Activity context;

    public DocumentBuilder(String asset, String tipo, String siniestro, String cedula){
        this.context = BaseActivity.getBaseActivity();
        this.asset = asset;
        this.params = new HashMap<>();
        this.params.put("siniestro", siniestro);
        this.params.put("cedula", cedula);
        this.params.put("folio", String.format("%s%s%s%03d", siniestro, cedula, tipo, new Random().nextInt(1000)));
        initParams();
    }

    protected String getValue(String param) {
        return params.get(param);
    }

    protected void setAsset(String asset) {
        this.asset = asset;
    }

    protected void putParam(String param, String value){
        params.put(param, value == null ? DEFAULT_VALUE : value);
    }

    protected void putParam(String param, Boolean value){
        params.put(param, value == null || !value ? DEFAULT_BOOLEAN_VALUE : "Sí");
    }

    protected void putDateParam(String param, Date value){
        params.put(param, value == null ? DEFAULT_VALUE : DateUtils.date(value));
    }

    protected void putDatetimeParam(String param, Date value){
        params.put(param, value == null ? DEFAULT_VALUE : DateUtils.datetime(value));
    }

    protected void putTimeParam(String param, Date value){
        params.put(param, value == null ? DEFAULT_VALUE : DateUtils.time(value));
    }

    protected void putCurrencyParam(String param, Double value){
        params.put(param, value == null ? DEFAULT_VALUE : NumberFormat.getCurrencyInstance().format(value));
    }

    protected void putParam(String param, Integer value){
        params.put(param, value == null ? DEFAULT_VALUE : value.toString());
    }

    protected void putListParam(String param, List<String> values){
        params.put(param, values == null || values.isEmpty()? DEFAULT_VALUE : listValueConverter(values));
    }


    protected void putMapParam(String param, String value, String label){
        params.put(param, value == null ? DEFAULT_VALUE : rowMapGen(value,label));
    }

    protected void putImageParam(String param, String value){
        params.put(param, value == null ? DEFAULT_VALUE : rowImageGen(value));
    }

    protected void putImageParam(String param, InputStream value){
        String base64 = Base64.encodeToString(FileUtils.getBytes(value),0);
        params.put(param, value == null ? DEFAULT_VALUE : rowImageGen(base64));
    }

    private String rowMapGen(String value, String label){
        if(value==null||value.trim().isEmpty()){
            return "";
        }
        return String.format("<tr><td>%s</td><td>%s</td>",label,value);
    }

    private String rowImageGen(String value){
        if(value==null||value.trim().isEmpty()){
            return "";
        }
        return String.format("<img src=\"data:image/*;base64,%s\"/>",value);
    }

    private String listValueConverter(List<String> values){
        StringBuilder sb = new StringBuilder();
        for (String s:values) {
            sb.append(s).append(",");
        }
        return sb.toString().replaceAll(",$", "");
    }

    private void initParams(){
        String[] params = getParams();
        for(String param : params){
            this.params.put(param, DEFAULT_VALUE);
        }
    }

    protected abstract String[] getParams();

    private String getHtml() throws IOException {
        Log.d(TAG, "Creando HTML");
        InputStream template = FileUtils.getFile(context,asset);
        String html = FileUtils.getContentFile(template);
        html = replaceParams(html, params);
        return html;
    }

    /**
     * Sustituye los parámetos en el template. Los parámetros deben estar en notación <code>${parametro}</code>
     * @param template Cadena de texto con los parámetros
     * @param params Clave y valor de los parámetros
     * @return Cadena de texto con la sustitución de los parámetros.
     */
    private static String replaceParams(String template, Map<String, String> params){
        String result = template;
        for(String param : params.keySet()){
            Log.d(TAG,"<<< Reemplazando " + param + " con " + params.get(param));
            result = result.replaceAll("\\$\\{" + param + "\\}", Matcher.quoteReplacement(params.get(param)));
            Log.d(TAG,">>> Reemplazando "+ param);
        }
        return result;
    }

    private int getHeigth(String html){
        Pattern pattern = Pattern.compile("size: " + DEFAULT_WIDTH_IN + "in (\\d*)in");
        Matcher matcher = pattern.matcher(html);
        int heigth = DEFAULT_HEIGTH_IN * 1000;
        if(matcher.find()){
            try{
                heigth = Integer.parseInt(matcher.group(1)) * 1000;
            } catch (NumberFormatException ex){
                Log.w(TAG, "No se pudo obtener el tamaño del papel");
            }
        }
        return heigth;
    }


    public Observable<String> build(File out) {
        return Observable.create(e ->
            context.runOnUiThread(() -> {
                Log.d(TAG, "Iniciando creación de archivo");
                WebView view = new WebView(context);
                view.getSettings().setAllowContentAccess(true);
                view.getSettings().setJavaScriptEnabled(false);
                view.getSettings().setSupportZoom(false);
                view.setWebViewClient(new WebViewClient(){
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        int heigth = getHeigth(url);
                        int width = DEFAULT_WIDTH_IN * 1000;
                        DisplayMetrics metrics = view.getResources().getDisplayMetrics();
                        PrintAttributes printAttributes = new PrintAttributes.Builder()
                                .setMediaSize(new PrintAttributes.MediaSize("id", "label", width, heigth))
                                .setMinMargins(PrintAttributes.Margins.NO_MARGINS)
                                .setColorMode(PrintAttributes.COLOR_MODE_COLOR)
                                .setResolution(new PrintAttributes.Resolution("pdf", "pdf", metrics.densityDpi, metrics.densityDpi))
                                .build();
                        e.setDisposable(new PdfPrint(printAttributes)
                                .print(view.createPrintDocumentAdapter("pdf"), out)
                                .subscribe(() -> {
                                    e.onNext(params.get("folio"));
                                    e.onComplete();
                                    Log.d(TAG, "PDF Creado");
                                    view.destroy();
                                }, e::tryOnError));
                    }
                });
                try {
                    FileUtils.createFile(out);
                    view.loadData(getHtml(), "text/html; charset=utf-8", "UTF-8");
                } catch (Exception ex){
                    e.tryOnError(ex);
                }
            }));
    }
}
