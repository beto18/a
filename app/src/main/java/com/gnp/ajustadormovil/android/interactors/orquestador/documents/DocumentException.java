package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

public class DocumentException extends RuntimeException {

    public DocumentException(String message, Throwable e){
        super(message, e);
    }

    public DocumentException(Throwable e){
        super(e);
    }

}
