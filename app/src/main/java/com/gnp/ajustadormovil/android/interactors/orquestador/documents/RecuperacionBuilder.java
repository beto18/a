package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import java.util.Date;

public class RecuperacionBuilder extends DocumentBuilder {

    public static final String TEMPLATE = "documents/recuperacion.html";

    public static final String TIPO_DOCUMENTO = "14";

    public static final String [] PARAMS = {
            "estado",
            "municipio",
            "numeroPoliza",
            "calleNumExtInt",
            "entreCalles",
            "contratanteNombreCompleto",
            "ajustadorNombreCompleto",
            "fechaCobro",
            "recibimosDe",
            "importeTotal",
            "importeTotalLetra",
            "tipoRecuperacionMonetaria",
            "recuperacionEntidadFinanciera",
            "telefono",
            "tipoAfectado",
            "montoRecibido",
            "fechaPromesa",
            "armadora",
            "carroceria",
            "version",
            "modelo",
            "numeroMotor",
            "vin",
            "placas",
            "observaciones",
            "difImportes"
    };

    public RecuperacionBuilder(String siniestro, String cedula){
        super(TEMPLATE, TIPO_DOCUMENTO, siniestro, cedula);
    }

    @Override
    protected String[] getParams() {
        return PARAMS;
    }

    public RecuperacionBuilder estado(String estado){
        putParam("estado",estado);
        return this;
    }

    public RecuperacionBuilder municipio(String municipio){
        putParam("municipio",municipio);
        return this;
    }

    public RecuperacionBuilder calleNumExtInt(String calleNumExtInt){
        putParam("calleNumExtInt", calleNumExtInt);
        return this;
    }

    public RecuperacionBuilder entreCalles(String entreCalles){
        putParam("entreCalles",entreCalles);
        return this;
    }

    public RecuperacionBuilder contratanteNombreCompleto(String contratanteNombreCompleto){
        putParam("contratanteNombreCompleto",contratanteNombreCompleto);
        return this;
    }

    public RecuperacionBuilder ajustadorNombreCompleto(String ajustadorNombreCompleto){
        putParam("ajustadorNombreCompleto",ajustadorNombreCompleto);
        return this;
    }

    public RecuperacionBuilder fechaCobro(Date fechaCobro){
        putDateParam("fechaCobro",fechaCobro);
        return this;
    }

    public RecuperacionBuilder recibimosDe(String recibimosDe){
        putParam("recibimosDe",recibimosDe);
        return this;
    }

    public RecuperacionBuilder importeTotal(Double importeTotal){
        putCurrencyParam("importeTotal",importeTotal);
        return this;
    }

    public RecuperacionBuilder importeTotalLetra(String importeTotalLetra){
        putParam("importeTotalLetra",importeTotalLetra);
        return this;
    }

    public RecuperacionBuilder tipoRecuperacionMonetaria(String tipoRecuperacionMonetaria){
        putParam("tipoRecuperacionMonetaria",tipoRecuperacionMonetaria);
        return this;
    }

    public RecuperacionBuilder recuperacionEntidadFinanciera(String recuperacionEntidadFinanciera){
        putParam("recuperacionEntidadFinanciera",recuperacionEntidadFinanciera);
        return this;
    }

    public RecuperacionBuilder telefono(String telefono){
        putParam("telefono",telefono);
        return this;
    }

    public RecuperacionBuilder tipoAfectado(String tipoAfectado){
        putParam("tipoAfectado",tipoAfectado);
        return this;
    }

    public RecuperacionBuilder montoRecibido(Double montoRecibido){
        putCurrencyParam("montoRecibido",montoRecibido);
        return this;
    }

    public RecuperacionBuilder fechaPromesa(Date fechaPromesa){
        putDateParam("fechaPromesa",fechaPromesa);
        return this;
    }

    public RecuperacionBuilder armadora(String armadora){
        putParam("armadora",armadora);
        return this;
    }

    public RecuperacionBuilder carroceria(String carroceria){
        putParam("carroceria",carroceria);
        return this;
    }

    public RecuperacionBuilder version(String version){
        putParam("version",version);
        return this;
    }

    public RecuperacionBuilder modelo(Short modelo){
        if(modelo != null) {
            putParam("modelo", modelo.toString());
        }
        return this;
    }

    public RecuperacionBuilder numeroMotor(String numeroMotor){
        putParam("numeroMotor",numeroMotor);
        return this;
    }

    public RecuperacionBuilder vin(String vin){
        putParam("vin",vin);
        return this;
    }

    public RecuperacionBuilder placas(String placas){
        putParam("placas",placas);
        return this;
    }

    public RecuperacionBuilder observaciones(String observaciones){
        putParam("observaciones",observaciones);
        return this;
    }

    public RecuperacionBuilder folio(String folio){
        putParam("folio",folio);
        return this;
    }

    public RecuperacionBuilder numeroPoliza(String numeroPoliza){
        putParam("numeroPoliza",numeroPoliza);
        return this;
    }

    public RecuperacionBuilder difImportes(Double difImportes){
        putCurrencyParam("difImportes",difImportes);
        return this;
    }


}
