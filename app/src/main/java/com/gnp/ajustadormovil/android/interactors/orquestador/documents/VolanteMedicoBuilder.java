package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import java.util.Date;
import java.util.List;

public class VolanteMedicoBuilder extends DocumentBuilder {

    public static final String TEMPLATE = "documents/volante_medico.html";


    public static final String [] PARAMS = {
            "tipoVolanteMedico",
            "numeroPoliza",
            "fechaSiniestro",
            "nombreConductor",
            "nombreLesionado",
            "fechaNacimiento",
            "posicionLesionado",
            "tipoVehiculo",
            "vehiculo",
            "modelo",
            "causaSiniestro",
            "lstCoberturas",
            "lstLesiones",
            "requiereAmbulancia",
            "lstObjetosPerdidos",
            "lugarTraslado",
            "direccionTraslado",
            "numeroLesionados",
            "cliente"
    };

    public VolanteMedicoBuilder(String siniestro, String cedula, String tipoDocumento){
        super(TEMPLATE, tipoDocumento, siniestro, cedula);
    }

    @Override
    protected String[] getParams() {
        return PARAMS;
    }

    public VolanteMedicoBuilder tipoVolanteMedico(String tipoVolanteMedico){
        putParam("tipoVolanteMedico",tipoVolanteMedico);
        return this;
    }
    public VolanteMedicoBuilder numeroPoliza(String numeroPoliza){
        putParam("numeroPoliza",numeroPoliza);
        return this;
    }
    public VolanteMedicoBuilder fechaSiniestro(Date fechaSiniestro){
        putDatetimeParam("fechaSiniestro",fechaSiniestro);
        return this;
    }
    public VolanteMedicoBuilder nombreConductor(String nombreConductor){
        putParam("nombreConductor",nombreConductor);
        return this;
    }
    public VolanteMedicoBuilder nombreLesionado(String nombreLesionado){
        putParam("nombreLesionado",nombreLesionado);
        return this;
    }
    public VolanteMedicoBuilder fechaNacimiento(Date fechaNacimiento){
        putDateParam("fechaNacimiento",fechaNacimiento);
        return this;
    }
    public VolanteMedicoBuilder posicionLesionado(String posicionLesionado){
        putParam("posicionLesionado",posicionLesionado);
        return this;
    }
    public VolanteMedicoBuilder tipoVehiculo(String tipoVehiculo){
        putParam("tipoVehiculo",tipoVehiculo);
        return this;
    }
    public VolanteMedicoBuilder vehiculo(String vehiculo){
        putParam("vehiculo",vehiculo);
        return this;
    }
    public VolanteMedicoBuilder modelo(Short modelo){
        if(modelo != null) {
            putParam("modelo", modelo.toString());
        }
        return this;
    }
    public VolanteMedicoBuilder causaSiniestro(String causaSiniestro){
        putParam("causaSiniestro",causaSiniestro);
        return this;
    }
    public VolanteMedicoBuilder lstCoberturas(List<String> lstCoberturas){
        putListParam("lstCoberturas",lstCoberturas);
        return this;
    }
    public VolanteMedicoBuilder lstLesiones(List<String> lstLesiones){
        putListParam("lstLesiones",lstLesiones);
        return this;
    }
    public VolanteMedicoBuilder requiereAmbulancia(Boolean requiereAmbulancia){
        putParam("requiereAmbulancia",requiereAmbulancia);
        return this;
    }
    public VolanteMedicoBuilder lstObjetosPerdidos(List<String> lstObjetosPerdidos){
        putListParam("lstObjetosPerdidos",lstObjetosPerdidos);
        return this;
    }
    public VolanteMedicoBuilder lugarTraslado(String lugarTraslado){
        putParam("lugarTraslado",lugarTraslado);
        return this;
    }
    public VolanteMedicoBuilder direccionTraslado(String direccionTraslado){
        putParam("direccionTraslado",direccionTraslado);
        return this;
    }
    public VolanteMedicoBuilder numeroLesionados(Integer numeroLesionados){
        putParam("numeroLesionados",numeroLesionados);
        return this;
    }
    public VolanteMedicoBuilder cliente(String cliente) {
        putParam("cliente", cliente);
        return this;
    }
}
