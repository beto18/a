package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import java.util.Date;

public class VolanteNoAutoBuilder extends DocumentBuilder {

    public static final String TEMPLATE = "documents/volante_no_auto.html";

    public static final String TIPO_DOCUMENTO = "10";

    public static final String [] PARAMS = {
            "fechaSiniestro",
            "numeroPoliza",
            "estatusPoliza",
            "tipoTercero",
            "descripcionTercero",
            "nombreContratista",
            "telefono",
            "correoElectronico",
            "observaciones",
            "areasDañadas",
            "dañoConsecuencia"
    };

    public VolanteNoAutoBuilder(String siniestro, String cedula, String tipoDocumento){
        super(TEMPLATE, TIPO_DOCUMENTO, siniestro, cedula);
    }

    @Override
    protected String[] getParams() {
        return PARAMS;
    }


    public VolanteNoAutoBuilder fechaSiniestro(Date fechaSiniestro){
        putDateParam("fechaSiniestro",fechaSiniestro);
        return this;
    }
    public VolanteNoAutoBuilder numeroPoliza(String numeroPoliza){
        putParam("numeroPoliza",numeroPoliza);
        return this;
    }
    public VolanteNoAutoBuilder estatusPoliza(String estatusPoliza){
        putParam("estatusPoliza",estatusPoliza);
        return this;
    }
    public VolanteNoAutoBuilder tipoTercero(String tipoTercero){
        putParam("tipoTercero",tipoTercero);
        return this;
    }
    public VolanteNoAutoBuilder descripcionTercero(String descripcionTercero){
        putParam("descripcionTercero",descripcionTercero);
        return this;
    }
    public VolanteNoAutoBuilder nombreContratista(String nombreContratista){
        putParam("nombreContratista",nombreContratista);
        return this;
    }
    public VolanteNoAutoBuilder telefono(String telefono){
        putParam("telefono",telefono);
        return this;
    }
    public VolanteNoAutoBuilder correoElectronico(String correoElectronico){
        putParam("correoElectronico",correoElectronico);
        return this;
    }
    public VolanteNoAutoBuilder observaciones(String observaciones){
        putParam("observaciones",observaciones);
        return this;
    }
    public VolanteNoAutoBuilder areasDañadas(String areasDañadas){
        putParam("areasDañadas",areasDañadas);
        return this;
    }
    public VolanteNoAutoBuilder dañoConsecuencia(String dañoConsecuencia){
        putParam("dañoConsecuencia",dañoConsecuencia);
        return this;
    }
}
