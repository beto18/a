package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

public class VolanteVehiculoAseguradoBuilder extends DocumentBuilder {

    public static final String TEMPLATE = "documents/volante_vehiculo_asegurado.html";

    public static final String TEMPLATE_TALLER = "documents/volante_vehiculo_asegurado_taller.html";

    public static final String TIPO_DOCUMENTO = "03";

    public static final String [] PARAMS = {
            "fechaSiniestro",
            "horaSiniestro",
            "fechaArribo",
            "fechaEntrega",
            "estado",
            "municipio",
            "calle",
            "entreCalles",
            "circunstancia",
            "numeroPoliza",
            "estatusPoliza",
            "aseguradoNombre",
            "tipoCobertura",
            "estatusCobranza",
            "vigenciaPoliza",
            "conductorNombre",
            "fechaNacimiento",
            "edad",
            "tipoIdentificacion",
            "tipoLicencia",
            "fechaFinLicencia",
            "lstTelefonos",
            "lstCorreos",
            "tipoVehiculo",
            "armadora",
            "carroceria",
            "version",
            "modelo",
            "placas",
            "vin",
            "lstDanosPreexistentes",
            "responsabilidadAjustador",
            "aplicaDeducible",
            "razonNoDeducible",
            "lstRecuperaciones",
            "proteccionLegal",
            "perdidaTotalEvidente",
            "lstDanosConsecuencia",
            "vehiculoSegmentado",
            "folioSegmentacion",
            "volanteCondicionado",
            "causaCondicionamiento",
            "tomadorDatos",
            "lstPiezasFaltantes",
            "qrTaller",
            "cliente"
    };

    public VolanteVehiculoAseguradoBuilder(String siniestro, String cedula){
        super(TEMPLATE, TIPO_DOCUMENTO, siniestro, cedula);
    }

    @Override
    protected String[] getParams() {
        return PARAMS;
    }


    public VolanteVehiculoAseguradoBuilder fechaSiniestro(Date fechaSiniestro){
        putDateParam("fechaSiniestro",fechaSiniestro);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder horaSiniestro(Date horaSiniestro){
        putTimeParam("horaSiniestro",horaSiniestro);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder fechaArribo(Date fechaArribo){
        putDatetimeParam("fechaArribo",fechaArribo);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder fechaEntrega(Date fechaEntrega){
        putDatetimeParam("fechaEntrega",fechaEntrega);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder estado(String estado){
        putParam("estado",estado);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder municipio(String municipio){
        putParam("municipio",municipio);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder calle(String calle){
        putParam("calle",calle);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder entreCalles(String entreCalles){
        putParam("entreCalles",entreCalles);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder circunstancia(String circunstancia){
        putParam("circunstancia",circunstancia);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder numeroPoliza(String numeroPoliza){
        putParam("numeroPoliza",numeroPoliza);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder estatusPoliza(String estatusPoliza){
        putParam("estatusPoliza",estatusPoliza);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder aseguradoNombre(String aseguradoNombre){
        putParam("aseguradoNombre",aseguradoNombre);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder tipoCobertura(String tipoCobertura){
        putParam("tipoCobertura",tipoCobertura);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder estatusCobranza(String estatusCobranza){
        putParam("estatusCobranza",estatusCobranza);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder vigenciaPoliza(Date vigenciaPoliza){
        putDateParam("vigenciaPoliza",vigenciaPoliza);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder conductorNombre(String conductorNombre){
        putParam("conductorNombre",conductorNombre);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder fechaNacimiento(Date fechaNacimiento){
        putDateParam("fechaNacimiento",fechaNacimiento);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder edad(String edad){
        putParam("edad",edad);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder tipoIdentificacion(String tipoIdentificacion){
        putParam("tipoIdentificacion",tipoIdentificacion);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder tipoLicencia(String tipoLicencia,String label){
        putMapParam("tipoLicencia",tipoLicencia,label);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder fechaFinLicencia(String fechaFinLicencia,String label){
        putMapParam("fechaFinLicencia",fechaFinLicencia,label);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder telefono(String telefono){
        putParam("telefono", telefono);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder correo(String correo){
        putParam("correo", correo);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder tipoVehiculo(String tipoVehiculo){
        putParam("tipoVehiculo",tipoVehiculo);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder armadora(String armadora){
        putParam("armadora",armadora);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder carroceria(String carroceria){
        putParam("carroceria",carroceria);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder version(String version){
        putParam("version",version);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder modelo(String modelo){
        putParam("modelo",modelo);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder placas(String placas){
        putParam("placas",placas);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder vin(String vin){
        putParam("vin",vin);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder lstDanosPreexistentes(List<String> lstDanosPreexistentes){
        putListParam("lstDanosPreexistentes",lstDanosPreexistentes);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder responsable(Boolean responsable){
        putParam("responsable", responsable);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder aplicaDeducible(String aplicaDeducible){
        putParam("aplicaDeducible",aplicaDeducible);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder razonNoDeducible(String razonNoDeducible){
        putParam("razonNoDeducible",razonNoDeducible);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder lstRecuperaciones(List<String> lstRecuperaciones){
        putListParam("lstRecuperaciones",lstRecuperaciones);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder proteccionLegal(Boolean proteccionLegal){
        putParam("proteccionLegal",proteccionLegal);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder perdidaTotalEvidente(Boolean perdidaTotalEvidente){
        putParam("perdidaTotalEvidente",perdidaTotalEvidente);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder lstDanosConsecuencia(List<String> lstDanosConsecuencia){
        putListParam("lstDanosConsecuencia",lstDanosConsecuencia);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder vehiculoSegmentado(Boolean vehiculoSegmentado){
        putParam("vehiculoSegmentado",vehiculoSegmentado);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder folioSegmentacion(String folioSegmentacion){
        putParam("folioSegmentacion",folioSegmentacion);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder volanteCondicionado(String volanteCondicionado){
        putParam("volanteCondicionado",volanteCondicionado);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder causaCondicionamiento(String causaCondicionamiento){
        putParam("causaCondicionamiento",causaCondicionamiento);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder tomadorDatos(Boolean tomadorDatos){
        putParam("tomadorDatos",tomadorDatos);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder lstPiezasFaltantes(List<String> lstPiezasFaltantes){
        putListParam("lstPiezasFaltantes",lstPiezasFaltantes);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder qrBase64(String qr){
        putImageParam("qrTaller", qr);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder qrInputStream(InputStream qr){
        putImageParam("qrInputStream",qr);
        return this;
    }

    public VolanteVehiculoAseguradoBuilder cliente(String cliente) {
        putParam("cliente", cliente);
        return this;
    }

    @Override
    public Observable<String> build(File out) {
        if(!getValue("qrTaller").isEmpty()) {
            setAsset(TEMPLATE_TALLER);
        }
        return super.build(out);
    }
}
