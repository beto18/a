package com.gnp.ajustadormovil.android.interactors.orquestador.documents;

import java.util.Date;
import java.util.List;

public class VolanteVehiculoTerceroBuilder extends DocumentBuilder {

    public static final String TEMPLATE = "documents/volante_vehiculo_tercero.html";

    public static final String [] PARAMS = {
            "fechaSiniestro",
            "horaSiniestro",
            "fechaArribo",
            "fechaEntrega",
            "estado",
            "municipio",
            "calle",
            "entreCalles",
            "circunstancia",
            "otraAseguradora",
            "numeroSiniestro",
            "numeroPoliza",
            "fechaFinPoliza",
            "conductorNombre",
            "telefono",
            "edad",
            "tipoIdentificacion",
            "tipoVehiculo",
            "armadora",
            "carroceria",
            "version",
            "modelo",
            "placas",
            "vin",
            "lstDanosPreexistentes",
            "responsabilidad",
            "proteccionLegal",
            "perdidaTotalEvidente",
            "lstDanosConsecuencia",
            "vehiculoSegmentado",
            "folioSegmentacion",
            "causaCondicionamiento",
            "volanteCondicionado",
            "tomadorDatos",
            "lstRecuperaciones",
            "lstPiezasFaltantes"
    };

    public VolanteVehiculoTerceroBuilder(String siniestro, String cedula, String tipoDocumento){
        super(TEMPLATE, tipoDocumento, siniestro, cedula);
    }

    @Override
    protected String[] getParams() {
        return PARAMS;
    }

    public VolanteVehiculoTerceroBuilder fechaSiniestro(Date fechaSiniestro){
        putDateParam("fechaSiniestro",fechaSiniestro);
        return this;
    }

    public VolanteVehiculoTerceroBuilder horaSiniestro(Date horaSiniestro){
        putTimeParam("horaSiniestro",horaSiniestro);
        return this;
    }

    public VolanteVehiculoTerceroBuilder fechaArribo(Date fechaArribo){
        putDateParam("fechaArribo",fechaArribo);
        return this;
    }

    public VolanteVehiculoTerceroBuilder fechaEntrega(Date fechaEntrega){
        putDateParam("fechaEntrega",fechaEntrega);
        return this;
    }

    public VolanteVehiculoTerceroBuilder causaCondicionamiento( String causaCondicionamiento){
        putParam("causaCondicionamiento",causaCondicionamiento);
        return this;
    }

    public VolanteVehiculoTerceroBuilder estado(String estado){
        putParam("estado",estado);
        return this;
    }

    public VolanteVehiculoTerceroBuilder municipio(String municipio){
        putParam("municipio",municipio);
        return this;
    }

    public VolanteVehiculoTerceroBuilder calle(String calle){
        putParam("calle",calle);
        return this;
    }

    public VolanteVehiculoTerceroBuilder entreCalles(String entreCalles){
        putParam("entreCalles",entreCalles);
        return this;
    }

    public VolanteVehiculoTerceroBuilder circunstancia(String circunstancia){
        putParam("circunstancia",circunstancia);
        return this;
    }

    public VolanteVehiculoTerceroBuilder otraAseguradora(String otraAseguradora){
        putParam("otraAseguradora",otraAseguradora);
        return this;
    }

    public VolanteVehiculoTerceroBuilder numeroSiniestro(String numeroSiniestro){
        putParam("numeroSiniestro", numeroSiniestro);
        return this;
    }

    public VolanteVehiculoTerceroBuilder numeroPoliza(String numeroPoliza){
        putParam("numeroPoliza",numeroPoliza);
        return this;
    }

    public VolanteVehiculoTerceroBuilder fechaFinPoliza(Date fechaFinPoliza){
        putDateParam("fechaFinPoliza",fechaFinPoliza);
        return this;
    }

    public VolanteVehiculoTerceroBuilder conductorNombre(String conductorNombre){
        putParam("conductorNombre",conductorNombre);
        return this;
    }

    public VolanteVehiculoTerceroBuilder telefono(String telefono){
        putParam("telefono",telefono);
        return this;
    }

    public VolanteVehiculoTerceroBuilder edad(Integer edad){
        putParam("edad", edad);
        return this;
    }

    public VolanteVehiculoTerceroBuilder tipoIdentificacion(String identificacion){
        putParam("tipoIdentificacion", identificacion);
        return this;
    }

    public VolanteVehiculoTerceroBuilder tipoVehiculo(String tipoVehiculo){
        putParam("tipoVehiculo",tipoVehiculo);
        return this;
    }

    public VolanteVehiculoTerceroBuilder armadora(String armadora){
        putParam("armadora",armadora);
        return this;
    }

    public VolanteVehiculoTerceroBuilder carroceria(String carroceria){
        putParam("carroceria",carroceria);
        return this;
    }

    public VolanteVehiculoTerceroBuilder version(String version){
        putParam("version",version);
        return this;
    }

    public VolanteVehiculoTerceroBuilder modelo(Short modelo){
        if(modelo != null) {
            putParam("modelo", modelo.intValue());
        }
        return this;
    }

    public VolanteVehiculoTerceroBuilder placas(String placas){
        putParam("placas",placas);
        return this;
    }

    public VolanteVehiculoTerceroBuilder vin(String vin){
        putParam("vin",vin);
        return this;
    }

    public VolanteVehiculoTerceroBuilder lstDanosPreexistentes(List<String> lstDanosPreexistentes){
        putListParam("lstDanosPreexistentes",lstDanosPreexistentes);
        return this;
    }

    public VolanteVehiculoTerceroBuilder responsabilidad(Boolean responsabilidad){
        putParam("responsabilidad", responsabilidad);
        return this;
    }

    public VolanteVehiculoTerceroBuilder proteccionLegal(Boolean proteccionLegal){
        putParam("proteccionLegal",proteccionLegal);
        return this;
    }

    public VolanteVehiculoTerceroBuilder perdidaTotalEvidente(Boolean perdidaTotalEvidente){
        putParam("perdidaTotalEvidente", perdidaTotalEvidente);
        return this;
    }

    public VolanteVehiculoTerceroBuilder lstDanosConsecuencia(List<String> lstDanosConsecuencia){
        putListParam("lstDanosConsecuencia",lstDanosConsecuencia);
        return this;
    }

    public VolanteVehiculoTerceroBuilder vehiculoSegmentado(String vehiculoSegmentado){
        putParam("vehiculoSegmentado",vehiculoSegmentado);
        return this;
    }

    public VolanteVehiculoTerceroBuilder folioSegmentacion(String folioSegmentacion){
        putParam("folioSegmentacion",folioSegmentacion);
        return this;
    }

    public VolanteVehiculoTerceroBuilder volanteCondicionado(Boolean volanteCondicionado){
        putParam("volanteCondicionado",volanteCondicionado);
        return this;
    }

    public VolanteVehiculoTerceroBuilder tomadorDatos(Boolean tomadorDatos){
        putParam("tomadorDatos",tomadorDatos);
        return this;
    }

    public VolanteVehiculoTerceroBuilder lstRecuperaciones(List<String> lstRecuperaciones){
        putListParam("lstRecuperaciones",lstRecuperaciones);
        return this;
    }

    public VolanteVehiculoTerceroBuilder lstPiezasFaltantes(List<String> lstPiezasFaltantes){
        putListParam("lstPiezasFaltantes",lstPiezasFaltantes);
        return this;
    }
}
