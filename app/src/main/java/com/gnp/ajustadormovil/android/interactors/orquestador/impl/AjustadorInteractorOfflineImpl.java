package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjustadorData;
import com.gnp.ajustadormovil.android.data.orquestador.AjustadorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjustadorInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.models.login.ChangePassword;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Dispositivo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.EstatusCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Horario;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.PosicionCommand;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class AjustadorInteractorOfflineImpl implements AjustadorInteractor {

    private static final String TAG = AjustadorInteractorOfflineImpl.class.getSimpleName();

    private final AjustadorData data = OrquestadorFactory.getAjustadorData();

    private final AjustadorRepository repository = RepositoryFactory.getAjustadorRepository();

    private final ConnectionProvider connection = ConnectionProvider.getInstance();

    @Override
    public Single<List<Horario>> getHorario() {
        return Single.create(e -> {
            if (connection.isConnected()) {
                e.setDisposable(data.getHorario()
                        .subscribe(horario -> {
                            Ajustador ajustador = repository.findOne();
                            ajustador.setHorario(horario);
                            repository.save(ajustador);
                            e.onSuccess(horario);
                        }, throwable -> {
                            Log.e(TAG, "Error al obtener horario", throwable);
                            e.tryOnError(throwable);
                        }));
            } else {
                Ajustador ajustador = repository.findOne();
                e.onSuccess(ajustador.getHorario());
            }
        });
    }

    @Override
    public Single<Ajustador> getAjustador() {
        return Single.create(e -> {
            if (connection.isConnected()) {
                e.setDisposable(data.getAjustador()
                        .subscribe(aj -> {
                            try {
                                Ajustador ajustador = repository.findOne();
                                aj.setHorario(ajustador.getHorario());
                            } catch (Exception ex) {
                                Log.e(TAG, "Error al obtener el ajustador", ex);
                            }
                            repository.save(aj);
                            e.onSuccess(aj);
                        }, throwable -> {
                            if(throwable instanceof NetworkException || throwable instanceof UnknownException){
                                try {
                                    Ajustador ajustador = repository.findOne();
                                    e.onSuccess(ajustador);
                                } catch (Exception ex) {
                                    Log.e(TAG, "Error al obtener el ajustador", ex);
                                    e.tryOnError(ex);
                                }
                            } else {
                                Log.e(TAG, "Error al obtener ajustador", throwable);
                                e.tryOnError(throwable);
                            }
                        }));
            } else {
                Ajustador ajustador = repository.findOne();
                e.onSuccess(ajustador);
            }
        });
    }

    @Override
    public Single<Dispositivo> addDispositivo(Dispositivo dispositivo) {
        return data.addDispositivo(dispositivo);
    }

    @Override
    public Completable modifyUbicacion(PosicionCommand posicion) {
        return data.modifyPosicion(posicion);
    }

    @Override
    public Completable modifyStatus(EstatusCommand estatus) {
        return data.modifyEstatus(estatus);
    }


    @Override
    public Completable init() {
        return Completable.create(e -> {
            Ajustador ajustador = repository.findOne();
            if (ajustador == null || ajustador.getCedula() == null) {
                e.setDisposable(data.getAjustador()
                        .observeOn(Schedulers.io())
                        .subscribeOn(Schedulers.io())
                        .subscribe(aj->{
                            repository.save(aj);
                            e.onComplete();
                        }, throwable -> {
                            Log.e(TAG, "Error al inicializar los datos el ajustador", throwable);
                            e.tryOnError(throwable);
                        }));

            } else {
                e.onComplete();
            }

        });
    }

    @Override
    public Completable resetPassword(String email) {
        return data.resetPassword(email);
    }

    @Override
    public Completable changePassword(ChangePassword changePassword) {
        return data.changePassword(changePassword);
    }

    @Override
    public Completable cerrarSesion(){
        return Completable.create(e -> {
            Realm.getDefaultInstance().executeTransaction(t -> {
                t.deleteAll();
                AppPreferences.cleanAll();
            });
            e.onComplete();
        });
    }
}
