package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjustadorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteData;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FirmaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.SyncInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.documents.DeclaracionBuilder;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.AjusteArriboException;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.AjusteException;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.CierreCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionResumen;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.EstatusAjuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.RechazoCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.TipoUbicacionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.InfoEstatus;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AjusteInteractorOfflineImpl extends SyncInteractorOfflineImpl implements AjusteInteractor {

    private final AjusteData data = OrquestadorFactory.getAjusteData();

    private final AjusteRepository repository = RepositoryFactory.getAjusteRepository();

    private final AjustadorRepository ajustadorRepository = RepositoryFactory.getAjustadorRepository();

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final ConnectionProvider connection = ConnectionProvider.getInstance();

    private final TprInteractor tprService = CatalogoInteractorsFactory.getTprService();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();

    private final FirmaRepository firmaRepository = RepositoryFactory.getFirmaRepository();



    @Override
    public Single<List<Ajuste>> getHistorial(String date) {
        return data.getHistorial(date);
    }

    @Override
    public Single<List<Ajuste>> getAjustes() {
        return Single.create(e -> {
            if (connection.isConnected()) {
                List<Ajuste> ajustes = RXUtils.sync(data.getAjustes());
                checkTerminate(ajustes);
                for (Ajuste ajuste : ajustes) {
                    Ajuste local = repository.findById(ajuste.getId());
                    Ajuste cloud = RXUtils.sync(data.getAjuste(ajuste.getId()));
                    cloud.setEstatus(ajuste.getEstatus());
                    if (local == null) {
                        for (Fotografia fotografia : cloud.getFotografiasDeclaracion()) {
                            fotografia.setLocalId(fotografia.getId());
                            fotografia.setLocalUrl(fotografia.getUrl());
                        }
                        repository.save(cloud);
                        try {
                            RXUtils.sync(getSyncService().initialize(ajuste.getId()));
                            Log.i(TAG, "Ajuste inicializado: " + ajuste.getId());
                        } catch (Exception ex) {
                            Log.e(TAG, "Error al inicializar la información del ajuste " + ajuste.getId(), ex);
                        }
                    } else {
                        checkEstatus(local, cloud);
                    }
                }
            }
            e.onSuccess(repository.findAll());
        });
    }

    @Override
    public Completable evolucionar(String ajuste) {
        return Completable.create(e -> {
            Ajuste local = repository.findById(ajuste);
            if (local != null) {
                Ajuste cloud = RXUtils.sync(data.getAjuste(ajuste));
                local.setSiniestro(cloud.getSiniestro());
                repository.save(local);
            }
            e.onComplete();
        });
    }

    @Override
    public Single<Ajuste> getAjuste(String id) {
        return Single.create(e -> {
            Ajuste ajuste = repository.findById(id);
            if (ajuste == null) {
                if (connection.isConnected()) {
                    Ajuste result = RXUtils.sync(data.getAjuste(ajuste.getId()));
                    e.onSuccess(result);
                } else {
                    e.tryOnError(new ResourceNotFoundException());
                }
            } else {
                e.onSuccess(ajuste);
            }
        });
    }

    @Override
    public Completable rechazar(String id, String motivo) {
        return Completable.create(e -> {
            RXUtils.sync(data.rechazar(id, new RechazoCommand(motivo)));
            Ajuste ajuste = RXUtils.sync(getAjuste(id));
            ajuste.setEstatus(EstatusAjuste.RECHAZANDO);
            repository.save(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Completable confirmar(String id) {
        return Completable.create(e -> {
            RXUtils.sync(data.confirmar(id));
            Ajuste ajuste = repository.findById(id);
            ajuste.setEstatus(EstatusAjuste.CONFIRMANDO);
            repository.save(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Single<Archivo> arribar(String id, File image) {
        return Single.create(e -> {
            Ajuste ajuste = repository.findById(id);
            ajuste.setFotografiaArribo(image.getAbsolutePath());
            ajuste.setFechaArribo(new Date());
            repository.save(ajuste);
            try {
                Archivo result = RXUtils.sync(data.arribar(id, RequestBody.create(MediaType.parse("image/jpg"), image)));
                Ajuste a = repository.findById(id);
                a.setEstatus(EstatusAjuste.ARRIBANDO);
                a.setFotografiaArribo(result.getUrl());
                a.setFechaArribo(new Date());
                repository.save(a);
                e.onSuccess(result);
            } catch (NetworkException | UnknownException ex) {
                Ajuste a = repository.findById(id);
                connection.notConnected(id);
                a.setEstatus(EstatusAjuste.LLEGADO);
                a.setFechaArribo(new Date());
                repository.save(a);
                e.tryOnError(new AjusteArriboException(a.getId()));
            } catch (Exception ex) {
                Ajuste a = repository.findById(id);
                a.setFotografiaArribo(null);
                a.setFechaArribo(null);
                repository.save(a);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminando(String id) {
        if (connection.isConnected(id)) {
            return data.terminando(id);
        } else {
            return Completable.complete();
        }
    }

    @Override
    public Completable cerrar(String id, String motivo, String conclusion, boolean background) {
        return Completable.create(e -> {
            Ajuste ajuste = repository.findById(id);
            ajuste.setMotivoCierre(motivo);
            ajuste.setConclusionCierre(conclusion);
            ajuste.setFechaTerminado(new Date());
            ajuste.setEstatus(EstatusAjuste.TERMINANDO);
            repository.save(ajuste);
            if (!getSyncService().isSynchronized(id) && connection.isConnected()) {
                try {
                    super.setBackgroud(id, background);
                    RXUtils.sync(getSyncService().synchronizeData(id));
                } catch (Exception ex) {
                    e.tryOnError(ex);
                } finally {
                    super.setBackgroud(id, true);
                }
            }
            if (connection.isConnected() && getSyncService().isSynchronized(id)) {
                if (ajuste.isOffline()) {
                    cerrar(id, data.cerrarOffline(id, new CierreCommand(motivo, conclusion)));
                } else {
                    cerrar(id, data.cerrar(id, new CierreCommand(motivo, conclusion)));
                }
            } else {
                ajuste.setEstatus(EstatusAjuste.SINCRONIZANDO);
                ajuste.setOffline(true);
                repository.save(ajuste);
            }
            e.onComplete();
        });
    }

    private void cerrar(String id, Completable cierre) throws Exception {
        try {
            RXUtils.sync(cierre);
            Ajuste a = repository.findById(id);
            a.setFechaTerminado(new Date());
            a.setEstatus(EstatusAjuste.TERMINANDO);
            repository.save(a);
        } catch (NetworkException | UnknownException ex) {
            Ajuste a = repository.findById(id);
            a.setEstatus(EstatusAjuste.SINCRONIZANDO);
            a.setFechaTerminado(null);
            a.setOffline(true);
            repository.save(a);
        } catch (Exception ex) {
            List<Ajuste> ajustes = RXUtils.sync(data.getAjustes());
            Ajuste cloud = null;
            for (Ajuste a : ajustes) {
                if (a.getId().equals(id)) {
                    cloud = a;
                }
            }
            Ajuste a = repository.findById(id);
            if (cloud == null) {
                a.setEstatus(EstatusAjuste.TERMINANDO);
                repository.save(a);
            } else {
                a.setEstatus(EstatusAjuste.LLEGADO);
                a.setFechaTerminado(null);
                repository.save(a);
                throw ex;
            }
        }
    }

    @Override
    public Single<Archivo> getDeclaracion(String id, DeclaracionCommand declaracionCommand) {
        return Single.create(e -> {
            CompositeDisposable disposable = new CompositeDisposable();
            e.setDisposable(disposable);
            Ajustador ajustador = ajustadorRepository.findOne();
            Ajuste ajuste = repository.findById(id);
            AjusteRepository repository = RepositoryFactory.getAjusteRepository();
            ajuste.setNarrativa(declaracionCommand.getNarrativa());
            ajuste.setFirma(declaracionCommand.getFirma());
            repository.save(ajuste);
            ajuste.setFirma(declaracionCommand.getFirma());
            Ajuste rollback = repository.findById(id);
            Asegurado asegurado = aseguradoRepository.findByAjuste(id);
            VersionVehiculo vehiculo = getVehiculo(asegurado);
            File declaracion = new File(Utils.TEMP_FOLDER + File.separator + "declaracion_" + ajuste.getSiniestro().getId() + ".pdf");
            disposable.add(new DeclaracionBuilder(ajuste.getSiniestro().getId(), ajustador.getCedula())
                    .nombre(getStringValue(asegurado, "conductor.nombre"))
                    .apellidoPaterno(getStringValue(asegurado, "conductor.apellidoPaterno"))
                    .apellidoMaterno(getStringValue(asegurado, "conductor.apellidoMaterno"))
                    .causaSiniestro(getStringValue(ajuste, "siniestro.tipoSiniestro.nombre"))
                    .narrativa(ajuste.getNarrativa())
                    .estatusPoliza(getStringValue(ajuste, "siniestro.poliza.estatus.nombre"))
                    .fechaNacimiento((Date) getValue(asegurado, "conductor.fechaNacimiento"))
                    .tipoVehiculo(getStringValue(vehiculo, "carroceria.armadora.tipoVehiculo.nombre"))
                    .modelo(vehiculo.getModelo())
                    .vehiculo(vehiculo.getNombre())
                    .numeroSerie(getStringValue(asegurado, "vehiculo.vin"))
                    .numeroPoliza(getStringValue(ajuste, "siniestro.poliza.numero"))
                    .titularPoliza(getStringValue(ajuste, "siniestro.poliza.titular.nombreCompleto"))
                    .vigenciaPoliza((Date) getValue(ajuste, "siniestro.poliza.fechaVigenciaFinal"))
                    .ubicacionSiniestro(getStringValue(ajuste, "siniestro.ubicacion.ubicationCompleta"))
                    .fechaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .fechaAtencion(ajuste.getFechaArribo())
                    .firma(declaracionCommand.getFirma())
                    .cliente(ajuste.getSiniestro().getPoliza() != null ? ajuste.getSiniestro().getPoliza().getCliente() : null)
                    .build(declaracion)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(folio -> {
                        ajuste.setUrlDeclaracion(declaracion.getAbsolutePath());
                        ajuste.setFolioDeclaracion(folio);
                        repository.save(ajuste);
                        firmaRepository.saveAseguradoConductor(id, declaracionCommand.getFirma());

                        try {
                            synchronize(ajuste.getId());
                            Ajuste stored = repository.findById(id);
                            e.onSuccess(new Archivo(stored.getUrlDeclaracion()));
                        }catch (Exception ex){
                            repository.save(rollback);
                            e.tryOnError(ex);
                        }
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al sincronizar declaracion");
                    }));
        });
    }

    @Override
    public Single<Fotografia> addEvidenciasDeclaracion(String id, File images) {
        return Single.create(e -> {
            Ajuste ajuste = repository.findById(id);
            Ajuste rollback = repository.findById(id);
            Fotografia fotografia = createFotografia(images);
            ajuste.getFotografiasDeclaracion().add(fotografia);
            repository.save(ajuste);
            try {
                synchronize(id);
                ajuste = repository.findById(id);
                fotografia = findByIdOrLocalId(ajuste.getFotografiasDeclaracion(), fotografia);
                e.onSuccess(fotografia);
            } catch (Throwable ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable modifyTipoUbicacion(String id, String tipoUbicacion) {
        return Completable.create(e -> {
            Ajuste rollback = repository.findById(id);
            assertModifyTipoUbicacion(tipoUbicacion);
            Ajuste ajuste = repository.findById(id);
            ajuste.setTipoUbicacionAjustador(new CatalogoClave());
            ajuste.getTipoUbicacionAjustador().setClave(tipoUbicacion);
            repository.save(ajuste);
            try {
                synchronize(id);
                e.onComplete();
            } catch (Throwable throwable) {
                repository.save(rollback);
                e.tryOnError(throwable);
            }
        });
    }

    @Override
    public Completable addDeducibleAdministrativo(String id) {
        return data.addDeducibleAdministrativo(id);
    }

    @Override
    public Completable deleteDeducibleAdministrativo(String id) {
        return data.deleteDeducibleAdministrativo(id);
    }

    private void assertModifyTipoUbicacion(String tipoUbicacion) {
        if (!tipoUbicacion.equals("CCR") && !tipoUbicacion.equals("CRR")) {
            throw new LogicException("tipo_ubicacion_not_valid", "El tipo de ubicación es CCR o CRR");

        }
    }

    @Override
    public boolean isSynchronized(String id) {
        Ajuste ajuste = repository.findById(id);
        if (ajuste == null) {
            return true;
        }
        if (ajuste.getFotografiaArribo() != null && !ajuste.getFotografiaArribo().startsWith("http")) {
            return false;
        }
        if (ajuste.getTipoUbicacionAjustador() != null && ajuste.getTipoUbicacionAjustador().getNombre() == null) {
            return false;
        }
        if (ajuste.getUrlDeclaracion() != null && !ajuste.getUrlDeclaracion().startsWith("http")) {
            return false;
        }
        if (ajuste.getFotografiasDeclaracion() != null) {
            for (Fotografia fotografia : ajuste.getFotografiasDeclaracion()) {
                if (fotografia.isEliminado() || fotografia.isAgregado()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Completable synchronizeData(String id) {
        return syncCompletable(id, e -> {
            Log.i(TAG, "Sincronizar datos del ajuste: " + id);
            if (!isSynchronized(id)) {
                cleanErrors(id);
                super.log(id, "Sincronzando datos del ajuste");
                try {
                    Ajuste ajuste = repository.findById(id);
                    synchronizeDataFotografiaArribo(ajuste);
                    synchronizeDataUbicacion(ajuste);
                    synchronizeDataDeclaracion(ajuste);
                    synchcronizeDataFotografiasEvidencia(ajuste);
                    Log.i(TAG, "Datos del ajuste: " + id + " sincronizados");
                    e.onComplete();
                } catch (Throwable ex) {
                    Log.e(TAG, "Error al sincronizar los datos del ajuste: " + id, ex);
                    if (ex instanceof NetworkException) {
                        e.tryOnError(ex);
                    } else if (ex instanceof UnknownException) {
                        e.tryOnError(ex);
                    } else {
                        e.tryOnError(new AjusteException(ex));
                    }
                }
            } else {
                Log.i(TAG, "Los datos del ajuste: " + id + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private void synchcronizeDataFotografiasEvidencia(Ajuste ajuste) throws Throwable {
        if (ajuste.getFotografiasDeclaracion() != null && !ajuste.getFotografiasDeclaracion().isEmpty()) {
            int size = ajuste.getFotografiasDeclaracion().size();
            syncAddFotografias(ajuste.getFotografiasDeclaracion(),
                    (req, index) -> {
                        log(ajuste.getId(), "synchronize_ajuste_evidencias", index + 1, size);
                        return data.addEvidenciasDeclaracion(ajuste.getId(), req);
                    }, () -> repository.save(ajuste));
        }
    }

    private void synchronizeDataDeclaracion(Ajuste ajuste) throws Throwable {
        if (ajuste.getUrlDeclaracion() != null && !ajuste.getUrlDeclaracion().startsWith("http")) {
            DeclaracionCommand declaracion = new DeclaracionCommand();
            declaracion.setFolio(ajuste.getFolioDeclaracion());
            declaracion.setFirma(firmaRepository.getAseguradoConductor(ajuste.getId()));
            declaracion.setNarrativa(ajuste.getNarrativa());
            log(ajuste.getId(), "synchronize_ajuste_declaracion");
            String url = RXUtils.sync(data.addDeclaracion(ajuste.getId(), declaracion)).getUrl();
            new File(ajuste.getUrlDeclaracion()).delete();
            ajuste.setUrlDeclaracion(url);
            repository.save(ajuste);
        }
    }

    private void synchronizeDataUbicacion(Ajuste ajuste) throws Throwable {
        if (ajuste.getTipoUbicacionAjustador() != null && ajuste.getTipoUbicacionAjustador().getNombre() == null) {
            log(ajuste.getId(), "synchronize_ajuste_ubicacion");
            RXUtils.sync(data.modifyTipoUbicacionAjustador(ajuste.getId(), new TipoUbicacionCommand(ajuste.getTipoUbicacionAjustador().getClave())));
            ajuste.getTipoUbicacionAjustador().setNombre(ajuste.getTipoUbicacionAjustador().getClave());
            repository.save(ajuste);
        }
    }

    private void synchronizeDataFotografiaArribo(Ajuste ajuste) throws Throwable {
        if (ajuste.getFotografiaArribo() != null && !ajuste.getFotografiaArribo().startsWith("http")) {
            log(ajuste.getId(), "synchronize_ajuste_arribo");
            File file = new File(ajuste.getFotografiaArribo());
            RequestBody request = RequestBody.create(MediaType.parse("image/jpg"), file);
            try {
                String url = RXUtils.sync(data.addFotografiaArribo(ajuste.getId(), request)).getUrl();
                new File(ajuste.getFotografiaArribo()).delete();
                ajuste.setFotografiaArribo(url);
                repository.save(ajuste);
            } catch (LogicException ex){
                if("ajuste_con_fotografia_arribo".equals(ex.getError().getError())){
                    Ajuste a = RXUtils.sync(data.getAjuste(ajuste.getId()));
                    ajuste.setFotografiaArribo(a.getFotografiaArribo());
                    repository.save(ajuste);
                } else {
                    throw ex;
                }
            }
        }
    }

    /**
     * Obtiene el valor de la propiedad. Si en el arbol de llamados un objeto es null, la respuesta será null.
     *
     * @param target   Objeto para obtener su propiedad
     * @param property Nombre de la propiedad.
     * @return Valor de la expresión.
     */
    private Object getValue(@Nullable Object target, String property) {
        String root = "root";
        Expression jexlExp = new JexlEngine().createExpression(root + "." + property);
        JexlContext jc = new MapContext();
        jc.set("root", target);
        return jexlExp.evaluate(jc);
    }

    private String getStringValue(@Nullable Object target, String property) {
        Object result = getValue(target, property);
        return result != null ? result.toString() : "";
    }

    private VersionVehiculo getVehiculo(Asegurado asegurado) {
        VersionVehiculo vehiculo = new VersionVehiculo();
        if (asegurado.getVehiculo() != null) {
            try {
                vehiculo = RXUtils.sync(tprService.getVersion(
                        asegurado.getVehiculo().getTipoVehiculo(),
                        asegurado.getVehiculo().getArmadora(),
                        asegurado.getVehiculo().getCarroceria(),
                        asegurado.getVehiculo().getModelo().intValue(),
                        asegurado.getVehiculo().getVersion()));
            } catch (Throwable ex) {
                Log.e(TAG, "Error al obtener el vehículo", ex);
            }
        }
        return vehiculo;
    }

    private SyncInteractor getSyncService() {
        return OrquestadorInteractorFactory.getSyncService();
    }

    private void checkTerminate(List<Ajuste> ajustes) throws Exception {
        List<Ajuste> locals = repository.findAll();
        for (Ajuste local : locals) {
            boolean exists = false;
            for (Ajuste ajuste : ajustes) {
                if (ajuste.getId().equals(local.getId())) {
                    exists = true;
                }
            }
            if (!exists) {
                RXUtils.sync(getSyncService().terminate(local.getId()));
            }
        }
    }

    private void checkEstatus(Ajuste local, Ajuste cloud) {
        if (local.getEstatus().equals(EstatusAjuste.ERROR)) {
            local.setEstatus(EstatusAjuste.SINCRONIZANDO);
        }
        boolean change = true;
        change &= !(cloud.getEstatus().equals(EstatusAjuste.ASIGNADO) && local.getEstatus().equals(EstatusAjuste.CONFIRMANDO));
        change &= !(cloud.getEstatus().equals(EstatusAjuste.CONFIRMADO) && local.getEstatus().equals(EstatusAjuste.ARRIBANDO));
        change &= !(cloud.getEstatus().equals(EstatusAjuste.CONFIRMADO) && local.getEstatus().equals(EstatusAjuste.SINCRONIZANDO));
        change &= !(cloud.getEstatus().equals(EstatusAjuste.CONFIRMADO) && local.getEstatus().equals(EstatusAjuste.LLEGADO));
        change &= !(cloud.getEstatus().equals(EstatusAjuste.LLEGADO) && local.getEstatus().equals(EstatusAjuste.TERMINANDO));
        change &= !(cloud.getEstatus().equals(EstatusAjuste.LLEGADO) && local.getEstatus().equals(EstatusAjuste.SINCRONIZANDO));
        if (change) {
            local.setEstatus(cloud.getEstatus());
        }
        if (cloud.getFechaArribo() != null) {
            local.setFechaArribo(cloud.getFechaArribo());
        }
        if (local.isOffline()) {
            connection.notConnected(local.getId());
        }
        if (local.getSiniestro().isPresiniestro() && !cloud.getSiniestro().isPresiniestro()) {
            local.setSiniestro(cloud.getSiniestro());
        }
        Throwable error = errorRepository.findByAjuste(local.getId());
        if (error != null) {
            local.setEstatus(EstatusAjuste.ERROR);
        }
        repository.save(local);
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            Ajuste data = repository.findById(ajuste);
            for (Fotografia fotografia : data.getFotografiasDeclaracion()) {
                fotografiaRepository.delete(fotografia);
            }
            repository.deleteById(ajuste);
            firmaRepository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Observable<DeclaracionResumen> getEstatus(String id) {
        return Observable.create(e -> {
            Ajuste ajuste = repository.findById(id);
            DeclaracionResumen resumen = new DeclaracionResumen();
            resumen.setDocumento(ajuste.getUrlDeclaracion() != null);
            resumen.setEvidencias(!filter(ajuste.getFotografiasDeclaracion()).isEmpty());
            resumen.setEstatus(isSynchronized(id) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            if (!resumen.isDocumento()) {
                resumen.getErrores().add(ResourceUtils.getString("ajuste_estatus_documento"));
                resumen.setEstatus(EstatusResumen.ERROR);
            }
            if (!resumen.isEvidencias()) {
                resumen.getErrores().add(ResourceUtils.getString("ajuste_estatus_fotografia"));
                resumen.setEstatus(EstatusResumen.ERROR);
            }
            Throwable error = errorRepository.findByAjuste(id);
            if (error instanceof AjusteException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    @Override
    public boolean canCerrar(InfoEstatus estatus) {
        if (!estatus.isComplete()) {
            return false;
        }
        if (estatus.getDeclaracion().getEstatus().equals(EstatusResumen.ERROR)) {
            return false;
        }
        if (estatus.getResponsabilidad().getEstatus().equals(EstatusResumen.SIN_ESTATUS)) {
            return true;
        }
        EstatusResumen merged = EstatusResumen.merge(
                estatus.getDeclaracion().getEstatus(),
                estatus.getAsegurado().getEstatus(),
                estatus.getTercerosAuto().getEstatus(),
                estatus.getTercerosNoAuto().getEstatus(),
                estatus.getTercerosPersona().getEstatus(),
                estatus.getResponsabilidad().getEstatus(),
                estatus.getRecuperacion().getEstatus(),
                estatus.getVolantes().getEstatus(),
                estatus.getServicios().getEstatus());
        return merged.equals(EstatusResumen.OK) || merged.equals(EstatusResumen.PENDIENTE);
    }

    @Override
    protected void cleanErrors(String id) {
        Throwable error = errorRepository.findByAjuste(id);
        if (error instanceof AjusteException) {
            errorRepository.deleteByAjuste(id);
        }
    }
}

