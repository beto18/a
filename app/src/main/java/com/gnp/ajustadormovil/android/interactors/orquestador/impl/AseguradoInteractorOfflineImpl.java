package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoData;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AfectadoIncompletoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoConductorException;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoException;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoOcupanteException;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoVehiculoException;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class AseguradoInteractorOfflineImpl extends SyncInteractorOfflineImpl implements AseguradoInteractor {

    private final AseguradoData data = OrquestadorFactory.getAseguradoData();

    private final AseguradoRepository repository = RepositoryFactory.getAseguradoRepository();

    private final ConnectionProvider connection = ConnectionProvider.getInstance();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();

    private final Repository<Persona, Long> personaRepository = RepositoryFactory.getRepository(Persona.class, Long.class);

    private final Repository<Ocupante, Long> ocupanteRepository = RepositoryFactory.getRepository(Ocupante.class, Long.class);

    private Asegurado getAsegurado(String ajuste) {
        Asegurado asegurado = repository.findByAjuste(ajuste);
        if (asegurado == null) {
            asegurado = new Asegurado();
            asegurado.setAjuste(ajuste);
            repository.save(asegurado);
        }
        return asegurado;
    }

    @Override
    public Single<Asegurado> get(String ajuste) {
        return Single.create(e -> {
            Asegurado asegurado = getAsegurado(ajuste);
            if (asegurado.getVehiculo() == null || asegurado.getConductor() == null) {
                try {
                    Asegurado result = RXUtils.sync(data.get(ajuste));
                    result.setAjuste(ajuste);
                    repository.save(result);
                    e.onSuccess(result);
                } catch (NetworkException ex) {
                    connection.notConnected(ajuste);
                    e.onSuccess(getAsegurado(ajuste));
                }
            } else {
                e.onSuccess(asegurado);
            }
        });
    }

    @Override
    public Completable modifyConductor(String ajuste, Persona conductor) {
        return Completable.create(e -> {
            assertValues(conductor);
            Asegurado asegurado = getAsegurado(ajuste);
            if (asegurado.getConductor() != null) {
                conductor.setFotografias(asegurado.getConductor().getFotografias());
                conductor.setVolante(asegurado.getConductor().getVolante());
                conductor.setId(asegurado.getConductor().getId());
                conductor.setInvolucradoId(asegurado.getConductor().getInvolucradoId());
            }
            if(conductor.getInvolucradoId() == null) {
                conductor.setInvolucradoId(UUID.randomUUID().toString());
            }
            assertConductor(conductor);
            asegurado.setConductor(conductor);
            asegurado.getConductor().setModificado(true);
            if (asegurado.getConductor().getId() == null) {
                asegurado.getConductor().setId(generateLocalId());
            }
            Asegurado rollback = getAsegurado(ajuste);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    private void assertValues(Afectado afectado) {
        ErrorUtils.assertValues(afectado);
        for (Telefono telefono : afectado.getTelefonos()) {
            ErrorUtils.assertValues(telefono);
        }
    }

    @Override
    public Single<Persona> getConductor(String ajuste) {
        return Single.create(e -> {
            Asegurado asegurado = getAsegurado(ajuste);
            if (asegurado.getConductor() == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(asegurado.getConductor());
            }
        });
    }

    @Override
    public Single<List<Fotografia>> getFotografiasConductor(String ajuste) {
        return Single.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            if (asegurado.getConductor() != null) {
                e.onSuccess(filter(asegurado.getConductor().getFotografias()));
            } else {
                e.onSuccess(new ArrayList<>(0));
            }
        });
    }

    @Override
    public Single<Fotografia> addFotografiaConductor(String ajuste, File image) {
        return Single.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Asegurado rollback = repository.findByAjuste(ajuste);
            Fotografia fotografia = createFotografia(image);
            asegurado.getConductor().getFotografias().add(fotografia);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                asegurado = repository.findByAjuste(ajuste);
                fotografia = findByIdOrLocalId(asegurado.getConductor().getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografiaConductor(String ajuste, Long idFotografia) {
        return Completable.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Asegurado rollback = repository.findByAjuste(ajuste);
            Fotografia deleted = null;
            for (Fotografia fotografia : asegurado.getConductor().getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            if (deleted.isAgregado()) {
                asegurado.getConductor().getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);
            }
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<VehiculoAsegurado> getVehiculo(String ajuste) {
        return Single.create(e -> {
            Asegurado asegurado = getAsegurado(ajuste);
            if (asegurado.getVehiculo() == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(asegurado.getVehiculo());
            }
        });
    }

    @Override
    public Completable modifyVehiculo(String ajuste, VehiculoAsegurado vehiculo) {
        return Completable.create(e -> {
            ErrorUtils.assertValues(vehiculo);
            Asegurado asegurado = getAsegurado(ajuste);
            if (asegurado.getVehiculo() != null) {
                vehiculo.setFotografias(asegurado.getVehiculo().getFotografias());
                vehiculo.setVolante(asegurado.getVehiculo().getVolante());
            }
            assertVehiculo(vehiculo);
            asegurado.setVehiculo(vehiculo);
            asegurado.getVehiculo().setModificado(true);
            Asegurado rollback = getAsegurado(ajuste);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Observable<List<Fotografia>> getFotografiasVehiculo(String ajuste) {
        return Observable.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            if (asegurado.getVehiculo() != null) {
                e.onNext(filter(asegurado.getVehiculo().getFotografias()));
            } else {
                e.onNext(new ArrayList<>(0));
            }
            e.onComplete();
        });
    }

    @Override
    public Observable<Fotografia> addFotografiaVehiculo(String ajuste, File image) {
        return Observable.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Asegurado rollback = repository.findByAjuste(ajuste);
            Fotografia fotografia = createFotografia(image);
            asegurado.getVehiculo().getFotografias().add(fotografia);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                asegurado = repository.findByAjuste(ajuste);
                fotografia = findByIdOrLocalId(asegurado.getVehiculo().getFotografias(), fotografia);
                e.onNext(fotografia);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografiaVehiculo(String ajuste, Long idFotografia) {
        return Completable.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Asegurado rollback = repository.findByAjuste(ajuste);
            Fotografia deleted = null;
            for (Fotografia fotografia : asegurado.getVehiculo().getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            if (deleted.isAgregado()) {
                asegurado.getVehiculo().getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);
            }
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<Ocupante>> getOcupantes(String ajuste) {
        return Single.create(e -> {
            try {
                Asegurado asegurado = getAsegurado(ajuste);
                e.onSuccess(this.filter(asegurado.getOcupantes()));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Ocupante> getOcupante(String ajuste, Long idOcupante) {
        return Single.create(e -> {
            try {
                Asegurado asegurado = getAsegurado(ajuste);
                Ocupante result = findByIdOrLocalId(asegurado.getOcupantes(), idOcupante);
                e.onSuccess(result);
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Ocupante> addOcupante(String ajuste, Ocupante ocupante) {
        return Single.create(e -> {
            assertValues(ocupante);
            final Asegurado asegurado = getAsegurado(ajuste);
            Asegurado rollback = getAsegurado(ajuste);
            ocupante.setId(generateLocalId());
            ocupante.setLocalId(ocupante.getId());
            ocupante.setInvolucradoId(UUID.randomUUID().toString());
            ocupante.setAgregado(true);
            ocupante.setEliminado(false);
            ocupante.setModificado(false);
            asegurado.getOcupantes().add(ocupante);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                e.onSuccess(findByIdOrLocalId(repository.findByAjuste(ajuste).getOcupantes(), ocupante));
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable modifyOcupante(String ajuste, Long idOcupante, Ocupante ocupante) {
        return Completable.create(e -> {
            assertValues(ocupante);
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Asegurado rollback = repository.findByAjuste(ajuste);
            Ocupante old = findByIdOrLocalId(asegurado.getOcupantes(), idOcupante);
            ocupante.setAgregado(old.isAgregado());
            ocupante.setEliminado(old.isEliminado());
            ocupante.setFotografias(old.getFotografias());
            ocupante.setVolante(old.getVolante());
            ocupante.setId(old.getId());
            ocupante.setLocalId(old.getLocalId());
            ocupante.setInvolucradoId(old.getInvolucradoId());
            ocupante.setModificado(!ocupante.isAgregado());
            assertOcupante(ocupante);
            asegurado.getOcupantes().remove(old);
            asegurado.getOcupantes().add(ocupante);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteOcupante(String ajuste, Long idOcupante) {
        return Completable.create(e -> {
            try {
                Asegurado asegurado = repository.findByAjuste(ajuste);
                Asegurado rollback = repository.findByAjuste(ajuste);
                Ocupante old = findByIdOrLocalId(asegurado.getOcupantes(), idOcupante);
                deleteAssertOcupante(old);
                if (old.isAgregado()) {
                    asegurado.getOcupantes().remove(old);
                } else {
                    old.setModificado(false);
                    old.setEliminado(true);
                }
                repository.save(asegurado);
                try {
                    synchronize(ajuste);
                    e.onComplete();
                } catch (Exception ex) {
                    repository.save(rollback);
                    e.tryOnError(ex);
                }
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<Fotografia>> getFotografiasOcupante(String ajuste, Long idOcupante) {
        return Single.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Ocupante ocupante = findByIdOrLocalId(asegurado.getOcupantes(), idOcupante);
            e.onSuccess(filter(ocupante.getFotografias()));
        });
    }

    @Override
    public Single<Fotografia> addFotografiaOcupante(String ajuste, Long idOcupante, File image) {
        return Single.create(e -> {
            Asegurado asegurado = repository.findByAjuste(ajuste);
            Asegurado rollback = repository.findByAjuste(ajuste);
            Ocupante ocupante = findByIdOrLocalId(asegurado.getOcupantes(), idOcupante);
            Fotografia fotografia = createFotografia(image);
            ocupante.getFotografias().add(fotografia);
            repository.save(asegurado);
            try {
                synchronize(ajuste);
                asegurado = repository.findByAjuste(ajuste);
                ocupante = findByIdOrLocalId(asegurado.getOcupantes(), ocupante);
                fotografia = findByIdOrLocalId(ocupante.getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografiaOcupante(String ajuste, Long idOcupante, Long idFotografia) {
        return Completable.create(e -> {
            try {
                Asegurado asegurado = repository.findByAjuste(ajuste);
                Asegurado rollback = repository.findByAjuste(ajuste);
                Ocupante ocupante = findByIdOrLocalId(asegurado.getOcupantes(), idOcupante);
                Fotografia deleted = null;
                for (Fotografia fotografia : ocupante.getFotografias()) {
                    if (fotografia.getId().equals(idFotografia)) {
                        deleted = fotografia;
                    }
                }
                if (deleted.isAgregado()) {
                    ocupante.getFotografias().remove(deleted);
                } else {
                    deleted.setEliminado(true);
                }
                repository.save(asegurado);
                try {
                    synchronize(ajuste);
                    e.onComplete();
                } catch (Exception ex) {
                    repository.save(rollback);
                    e.tryOnError(ex);
                }
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    private void assertConductor(Persona conductor) {
        if (conductor.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (conductor.getCondicion().equals(CondicionPersona.LESIONADO) && conductor.getLesiones().isEmpty()) {
            throw new LogicException("lesiones_required", ResourceUtils.getString("lesiones_required"));
        }
    }

    private void assertVehiculo(VehiculoAsegurado vehiculo) {
        if (vehiculo.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (vehiculo.getDanoConsecuencia() && vehiculo.getDanos().isEmpty()) {
            throw new LogicException("not_Acceptable", ResourceUtils.getString("not_aceptable"));
        }
        if (vehiculo.getDanoConsecuencia() && vehiculo.getPrimerImpacto() == null) {
            throw new LogicException("property_required", ResourceUtils.getString("property_required"));
        }

    }

    private void assertOcupante(Ocupante ocupante) {
        if (ocupante.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (ocupante.getCondicion().equals(CondicionPersona.LESIONADO) && ocupante.getLesiones().isEmpty()) {
            throw new LogicException("lesiones_required", ResourceUtils.getString("lesiones_ocupante_required"));
        }
        if (ocupante.getTelefonos().size() > 2) {
            throw new LogicException("max_telefonos", ResourceUtils.getString("max_telefonos"));
        }
        if (ocupante.getCorreos().size() > 2) {
            throw new LogicException("max_correos", ResourceUtils.getString("max_correos"));
        }
    }

    private void deleteAssertOcupante(Ocupante ocupante) {
        if (ocupante.getVolante() != null) {
            throw new LogicException("volante_impreso_ocupante", ResourceUtils.getString("volante_impreso_ocupante"));
        }
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Sincronizar datos del asegurado del ajuste: " + ajuste);
            cleanErrors(ajuste);
            if (!isSynchronized(ajuste)) {
                log(ajuste, "synchronize_asegurado");
                try {
                    Asegurado asegurado = getAsegurado(ajuste);
                    if (asegurado.getConductor() != null)
                        synchronizeDataConductor(asegurado);
                    if (asegurado.getVehiculo() != null)
                        synchronizeDataVehiculo(asegurado);
                    if (!asegurado.getOcupantes().isEmpty())
                        synchronizeDataOcupantes(asegurado);
                    Log.i(TAG, "Datos del asegurado del ajuste: " + ajuste + " sincronizados");
                    e.onComplete();
                } catch (Throwable ex) {
                    Log.e(TAG, "Error al sincronizar datos del asegurado del ajuste: " + ajuste, ex);
                    e.tryOnError(ex);
                }
            } else {
                Log.i(TAG, "Los datos del asegurado del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private synchronized void synchronizeDataConductor(Asegurado asegurado) throws NetworkException {
        if (asegurado.getConductor() != null) {
            try {
                log(asegurado.getAjuste(), "synchronize_asegurado_conductor");
                syncModificable(asegurado.getConductor(),
                        conductor -> data.modifyConductor(asegurado.getAjuste(), conductor),
                        () -> repository.save(asegurado));
                syncRemoveFotografias(asegurado.getConductor(),
                        id -> data.deleteFotografiaConductor(asegurado.getAjuste(), id),
                        () -> repository.save(asegurado));
                int size = asegurado.getConductor().getFotografias().size();
                syncAddFotografias(asegurado.getConductor(),
                        (req, index) -> {
                            log(asegurado.getAjuste(), "synchronize_asegurado_conductor_fotografias", index + 1, size);
                            return data.addFotografiaConductor(asegurado.getAjuste(), req);
                        }, () -> repository.save(asegurado));
            } catch (Throwable ex) {
                Log.e(TAG, "Error al sincronizar los datos del conductor del asegurado del ajuste: " + asegurado.getAjuste(), ex);
                if (ex instanceof NetworkException) {
                    throw (NetworkException) ex;
                } else if (ex instanceof UnknownException) {
                    throw (UnknownException) ex;
                } else {
                    throw new AseguradoConductorException(ex);

                }
            }
        }
    }

    private synchronized void synchronizeDataVehiculo(Asegurado asegurado) throws NetworkException {
        if (asegurado.getVehiculo() != null) {
            try {
                log(asegurado.getAjuste(), "synchronize_asegurado_vehiculo");
                syncModificable(asegurado.getVehiculo(),
                        vehiculo -> data.modifyVehiculo(asegurado.getAjuste(), vehiculo),
                        () -> repository.save(asegurado));
                syncRemoveFotografias(asegurado.getVehiculo(),
                        id -> data.deleteFotografiaVehiculo(asegurado.getAjuste(), id),
                        () -> repository.save(asegurado));
                int size = asegurado.getVehiculo().getFotografias().size();
                syncAddFotografias(asegurado.getVehiculo(),
                        (req, index) -> {
                            log(asegurado.getAjuste(), "synchronize_asegurado_vehiculo_fotografias", index + 1, size);
                            return data.addFotografiaVehiculo(asegurado.getAjuste(), req);
                        }, () -> repository.save(asegurado));

            } catch (Throwable ex) {
                Log.e(TAG, "Error al sincronizar los datos del vehiculo del asegurado del ajuste: " + asegurado.getAjuste(), ex);
                if (ex instanceof NetworkException) {
                    throw (NetworkException) ex;
                } else if (ex instanceof UnknownException) {
                    throw (UnknownException) ex;
                } else {
                    throw new AseguradoConductorException(ex);

                }
            }
        }
    }

    private synchronized void synchronizeDataOcupantes(Asegurado asegurado) throws NetworkException {
        ValueObject<Ocupante> error = new ValueObject<>();
        log(asegurado.getAjuste(), "synchronize_asegurado_ocupantes");
        try {
            syncEliminable(asegurado.getOcupantes(),
                    ocupante -> data.deleteOcupante(asegurado.getAjuste(), ocupante.getId())
                            .doOnError(throwable -> error.setValue(ocupante)),
                    ocupante -> {
                        ocupanteRepository.delete(ocupante);
                        repository.save(asegurado);
                    });
        } catch (Throwable t) {
            Log.e(TAG, "Error al sincronizar los datos del ocupante del asegurado del ajuste: " + asegurado.getAjuste(), t);
            String nombre = error.getValue() == null ? "Desconocido" : error.getValue().getNombreCompleto();
            if (t instanceof NetworkException) {
                throw (NetworkException) t;
            } else if (t instanceof UnknownException) {
                throw (UnknownException) t;
            } else {
                throw new AseguradoOcupanteException(nombre, t);
            }


        }
        for (Ocupante ocupante : asegurado.getOcupantes()) {
            synchronizeDataOcupante(asegurado, ocupante);
        }
    }

    protected void synchronizeDataOcupante(Asegurado asegurado, Ocupante ocupante) throws NetworkException {
        try {
            log(asegurado.getAjuste(), "synchronize_asegurado_ocupante", ocupante.getNombreCompleto());
            syncAgregable(ocupante,
                    o -> data.addOcupante(asegurado.getAjuste(), o),
                    result -> {
                        ocupanteRepository.deleteById(ocupante.getId());
                        ocupante.setId(result.getId());
                        repository.save(asegurado);
                    });
            syncModificable(ocupante,
                    o -> data.modifyOcupante(asegurado.getAjuste(), o.getId(), o),
                    () -> repository.save(asegurado));
            syncRemoveFotografias(ocupante,
                    id -> data.deleteFotografiaOcupante(asegurado.getAjuste(), ocupante.getId(), id),
                    () -> repository.save(asegurado));
            int size = ocupante.getFotografias().size();
            syncAddFotografias(ocupante,
                    (req, index) -> {
                        log(asegurado.getAjuste(), "synchronize_asegurado_ocupante_fotografias",
                                ocupante.getNombreCompleto(), index + 1, size);
                        return data.addFotografiaOcupante(asegurado.getAjuste(), ocupante.getId(), req);
                    },
                    () -> repository.save(asegurado));

        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new AseguradoOcupanteException(ocupante.getNombreCompleto(), ex);
            }
        }
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        Asegurado asegurado = getAsegurado(ajuste);
        boolean sincronizado = true;
        sincronizado &= isSynchronized(asegurado.getConductor());
        sincronizado &= isSynchronized(asegurado.getVehiculo());
        for (Ocupante ocupante : asegurado.getOcupantes()) {
            sincronizado &= isSynchronized(ocupante);
        }
        return sincronizado;
    }

    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Iniciando datos del asegurado del ajuste: " + ajuste);
            try {
                Asegurado asegurado = RXUtils.sync(data.get(ajuste));
                repository.deleteByAjuste(ajuste);
                asegurado.setAjuste(ajuste);
                if (asegurado.getConductor() != null) {
                    asegurado.getConductor().setId(generateLocalId());
                    putFotografiasLocalId(asegurado.getConductor());
                    if(asegurado.getConductor().getInvolucradoId() == null) {
                        asegurado.getConductor().setInvolucradoId(UUID.randomUUID().toString());
                    }
                }
                if (asegurado.getVehiculo() != null) {
                    putFotografiasLocalId(asegurado.getVehiculo());
                }
                for (Ocupante ocupante : asegurado.getOcupantes()) {
                    putLocalId(ocupante);
                }
                repository.save(asegurado);
                Log.i(TAG, "Datos del asegurado del ajuste: " + ajuste + " obtenidos");
                e.onComplete();
            } catch (ResourceNotFoundException ex) {
                Log.w(TAG, "No se encontraron datos del asegurado del ajuste: " + ajuste);
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar los datos del asegurado del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            Asegurado asegurado = getAsegurado(ajuste);
            if (asegurado.getConductor() != null) {
                for (Fotografia fotografia : asegurado.getConductor().getFotografias()) {
                    fotografiaRepository.delete(fotografia);
                }
                personaRepository.delete(asegurado.getConductor());
            }
            if (asegurado.getVehiculo() != null) {
                for (Fotografia fotografia : asegurado.getVehiculo().getFotografias()) {
                    fotografiaRepository.delete(fotografia);
                }
            }
            for (Ocupante ocupante : asegurado.getOcupantes()) {
                for (Fotografia fotografia : ocupante.getFotografias()) {
                    fotografiaRepository.delete(fotografia);
                }
                ocupanteRepository.delete(ocupante);
            }
            repository.deleteByAjuste(ajuste);

            e.onComplete();
        });
    }

    @Override
    public Observable<AseguradoResumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            AseguradoResumen resumen = new AseguradoResumen();
            Asegurado asegurado = getAsegurado(ajuste);
            resumen.setConductor(getEstatusConductor(asegurado));
            resumen.setVehiculo(getEstatusVehiculo(asegurado));
            resumen.setOcupantes(getEstatusOcupantes(asegurado));
            e.onNext(resumen);
            e.onComplete();
        });
    }

    private AfectadoIncompletoResumen getEstatusConductor(Asegurado asegurado) {
        AfectadoIncompletoResumen resumen = new AfectadoIncompletoResumen();

        if (asegurado.getConductor() != null) {
            resumen.setEstatus(isSynchronized(asegurado.getConductor()) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            try {
                assertValues(asegurado.getConductor());
                resumen.setDatos(true);
            } catch (DataException e) {
                resumen.getErrores().add(ResourceUtils.getString("conductor_incompleto"));
                resumen.setDatos(false);
            }

            resumen.setFotografias(!filter(asegurado.getConductor().getFotografias()).isEmpty());
            resumen.setVolante(asegurado.getConductor().getVolante() != null);
            if (!resumen.isDatos()) {
                resumen.setEstatus(EstatusResumen.ERROR);
            }
            if (!resumen.isFotografias()) {
                resumen.getErrores().add(ResourceUtils.getString("conductor_fotos_error"));
                resumen.setEstatus(EstatusResumen.ERROR);
            }
        } else {
            resumen.getErrores().add(ResourceUtils.getString("conductor_incompleto"));
            resumen.setEstatus(EstatusResumen.ERROR);
        }
        Throwable error = errorRepository.findByAjuste(asegurado.getAjuste());
        if (error instanceof AseguradoConductorException) {
            resumen.setEstatus(EstatusResumen.ERROR);
            resumen.setError(ErrorUtils.getErrorMessage(error));
        }
        return resumen;
    }

    private AfectadoIncompletoResumen getEstatusVehiculo(Asegurado asegurado) {
        AfectadoIncompletoResumen resumen = new AfectadoIncompletoResumen();
        if (asegurado.getVehiculo() != null) {
            resumen.setEstatus(isSynchronized(asegurado.getVehiculo()) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            try {
                ErrorUtils.assertValues(asegurado.getVehiculo());
                resumen.setDatos(true);
            } catch (DataException e) {
                resumen.getErrores().add(ResourceUtils.getString("vehiculo_incompleto"));
                resumen.setDatos(false);
            }
            resumen.setFotografias(!filter(asegurado.getVehiculo().getFotografias()).isEmpty());
            resumen.setVolante(asegurado.getVehiculo().getVolante() != null);

            if (!resumen.isDatos()) {
                resumen.setEstatus(EstatusResumen.ERROR);
            }
            if (!resumen.isFotografias()) {
                resumen.getErrores().add(ResourceUtils.getString("vehiculo_fotos_error"));
                resumen.setEstatus(EstatusResumen.ERROR);
            }
        } else {
            resumen.getErrores().add(ResourceUtils.getString("vehiculo_incompleto"));
            resumen.setEstatus(EstatusResumen.ERROR);
        }
        Throwable error = errorRepository.findByAjuste(asegurado.getAjuste());
        if (error instanceof AseguradoVehiculoException) {
            resumen.setEstatus(EstatusResumen.ERROR);
            resumen.setError(ErrorUtils.getErrorMessage(error));
        }
        return resumen;
    }

    private AfectadoIncompletoResumen getEstatusOcupantes(Asegurado asegurado) {
        AfectadoIncompletoResumen resumen = new AfectadoIncompletoResumen();
        if (filter(asegurado.getOcupantes()).isEmpty()) {
            resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
        } else {
            boolean sincronizado = true;
            boolean volante = true;
            boolean fotografias = true;
            for (Ocupante ocupante : filter(asegurado.getOcupantes())) {
                try {
                    assertValues(ocupante);
                    resumen.setDatos(true);
                } catch (DataException e) {
                    resumen.getErrores().add(ResourceUtils.getString("ocupante_incompleto"));
                    resumen.setDatos(false);
                }

                fotografias &= !ocupante.getFotografias().isEmpty();
                volante &= ocupante.getVolante() != null;
                sincronizado &= isSynchronized(ocupante);
            }
            resumen.setFotografias(fotografias);
            resumen.setVolante(volante);
            resumen.setEstatus(sincronizado ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            if (!resumen.isFotografias()) {
                resumen.getErrores().add(ResourceUtils.getString("ocupante_fotos_error"));
                resumen.setEstatus(EstatusResumen.ERROR);
            }
            if (!resumen.isDatos()) {
                resumen.setEstatus(EstatusResumen.ERROR);
            }
        }
        Throwable error = errorRepository.findByAjuste(asegurado.getAjuste());
        if (error instanceof AseguradoOcupanteException) {
            resumen.setEstatus(EstatusResumen.ERROR);
            resumen.setError(ErrorUtils.getErrorMessage(error));
        }
        return resumen;
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof AseguradoException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }


}