package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.CroquisRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.SyncInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.EstatusAjuste;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.Completable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CompositeSyncAjusteInteractor extends ConcurrentAjusteSync implements SyncInteractor {

    public static final int MAX_PRIORITY = 0;

    public static final int HIGH_PRIORITY = 1;

    public static final int MEDIUM_PRIORITY = 2;

    public static final int LOW_PRIORITY = 3;

    public static final Object MONITOR = new Object();

    private static final String TAG = CompositeSyncAjusteInteractor.class.getSimpleName();

    private final List<OrderedSyncService> services;

    private static final Map<String, Boolean> INITIALIZED = new ConcurrentHashMap<>();

    private final ConnectionProvider connection = ConnectionProvider.getInstance();

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    public CompositeSyncAjusteInteractor(){
        services = Collections.synchronizedList(new ArrayList<>());
    }

    private boolean isInitialized(String ajuste){
        if(INITIALIZED.get(ajuste) == null){
            INITIALIZED.put(ajuste, false);
        }
        return INITIALIZED.get(ajuste);
    }

    @Override
    public Completable initialize(String ajuste) {
        synchronized (MONITOR) {
            return Completable.create(e -> {
                Log.i(TAG, "Inicializando ajuste: " + ajuste);
                if (!isInitialized(ajuste)) {
                    Log.i(TAG, "Inicializando modulos del ajuste: " + ajuste);
                    e.setDisposable(runServices(service -> service.initialize(ajuste))
                            .subscribe(() -> {
                                INITIALIZED.put(ajuste, true);
                                Log.i(TAG, "El ajuste " + ajuste + " iniciado");
                                e.onComplete();
                            }, e::tryOnError));
                } else {
                    Log.i(TAG, "El ajuste " + ajuste + " ya se está inicializando");
                    INITIALIZED.put(ajuste, true);
                    e.onComplete();
                }
            });
        }
    }

    @Override
    public Completable terminate(String ajuste) {
        synchronized (MONITOR) {
            return Completable.create(e -> {
                Log.i(TAG, "Limpiando información del ajuste: " + ajuste);
                e.setDisposable(runServices(service -> service.terminate(ajuste))
                        .subscribe(() -> {
                            INITIALIZED.put(ajuste, false);
                            connection.clean(ajuste);
                            Log.i(TAG, "Información del ajuste: " + ajuste + " eliminada");
                            e.onComplete();
                        }, e::tryOnError));
            });
        }
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        synchronized (MONITOR) {
            boolean sincronizado = true;
            for (OrderedSyncService service : services) {
                sincronizado &= service.getService().isSynchronized(ajuste);
            }
            return sincronizado;
        }
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        synchronized (MONITOR) {
            return Completable.create(e -> {
                Log.i(TAG, "Sincronizando ajuste: " + ajuste);
                if (!isSynchronized(ajuste)) {
                    Log.i(TAG, "Sincronizando módulos del ajuste: " + ajuste);
                    e.setDisposable(runServices(service -> service.synchronizeData(ajuste))
                            .subscribe(() -> {
                                errorRepository.deleteByAjuste(ajuste);
                                Log.i(TAG, "Módulos del ajuste: " + ajuste + " sincronizados");
                                if (isSynchronized(ajuste)) {
                                    connection.connected(ajuste);
                                }
                                e.onComplete();
                            }, throwable -> {
                                if (throwable instanceof UnknownException || throwable instanceof NetworkException) {
                                    connection.notConnected(ajuste);
                                    e.tryOnError(throwable);
                                } else {
                                    Ajuste a = ajusteRepository.findById(ajuste);
                                    if(a != null) {
                                        a.setEstatus(EstatusAjuste.ERROR);
                                        ajusteRepository.save(a);
                                        errorRepository.saveByAjuste(ajuste, throwable);
                                        e.tryOnError(throwable);
                                    } else {
                                        Log.w(TAG, "El ajuste: " + ajuste + " ya no existe");
                                        e.onComplete();
                                    }

                                }
                            }));
                } else {
                    Log.i(TAG, "El ajuste " + ajuste + " ya está sincronizado");
                    errorRepository.deleteByAjuste(ajuste);
                    e.onComplete();
                }
            });
        }
    }

    public void addService(SyncInteractor service, int order) {
        services.add(new OrderedSyncService(service, order));
    }

    private Completable runServices(Function<SyncInteractor, Completable> func) throws Exception{
        return Completable.concat(getServices(MAX_PRIORITY, func))
                .andThen(Completable.concat(getServices(HIGH_PRIORITY, func)))
                .andThen(Completable.concat(getServices(MEDIUM_PRIORITY, func)))
                .andThen(Completable.concat(getServices(LOW_PRIORITY, func)))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io());
    }

    private Iterable<Completable> getServices(int order, Function<SyncInteractor, Completable> func) throws Exception {
        List<Completable> services = new ArrayList<>();
        for(OrderedSyncService service : this.services){
            if(service.getOrder() == order){
                services.add(func.apply(service.getService()));
            }
        }
        return services;
    }

    static class OrderedSyncService {

        private int order;

        private SyncInteractor service;

        public OrderedSyncService(SyncInteractor service, int order){
            this.service = service;
            this.order = order;
        }

        public int getOrder() {
            return order;
        }

        public SyncInteractor getService() {
            return service;
        }
    }

}
