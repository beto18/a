package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.Completable;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;

import static com.gnp.ajustadormovil.android.utils.core.ResourceUtils.getString;

public abstract class ConcurrentAjusteSync {

    private static final Map<String, Object> MONITOR = new ConcurrentHashMap<>();

    private final String TAG = this.getClass().getSimpleName();

    private static final Map<String, Boolean> BACKGROUD = new ConcurrentHashMap<>();

    private static final Object BACKGROUD_MONITOR = new Object();

    private Object getMonitor(String ajuste){
        synchronized (this) {
            if (MONITOR.get(ajuste) == null) {
                MONITOR.put(ajuste, new Object());
            }
            return MONITOR.get(ajuste);
        }
    }

    private boolean isBackgroud(String ajuste){
        synchronized (BACKGROUD_MONITOR) {
            if (BACKGROUD.get(ajuste) == null) {
                BACKGROUD.put(ajuste, true);
            }
            return BACKGROUD.get(ajuste);
        }
    }

    protected void setBackgroud(String ajuste, boolean backgroud){
        synchronized (BACKGROUD_MONITOR) {
            BACKGROUD.put(ajuste, backgroud);
        }
    }

    protected final Completable syncCompletable(String ajuste, CompletableOnSubscribe source) {
        return Completable.create(e -> {
            synchronized (getMonitor(ajuste)) {
                source.subscribe(e);
            }
        });
    }

    protected final <T> Single<T> syncSingle(String ajuste, SingleOnSubscribe<T> source) {
        return Single.create(e -> {
            synchronized (getMonitor(ajuste)) {
                source.subscribe(e);
            }
        });
    }

    protected final <T> Observable<T> syncObservable(String ajuste, ObservableOnSubscribe<T> source) {
        return Observable.create(e -> {
            synchronized (getMonitor(ajuste)) {
                source.subscribe(e);
            }
        });
    }

    protected void log(String ajuste, String message, Object ... params){
        String msg = ResourceUtils.getString(message);
        msg = MessageFormat.format(getString(msg), params);
        Log.i(TAG, "Ajuste " + ajuste + ": " + msg);
        if(!isBackgroud(ajuste)) {
            LoadingIndicator.message(msg);
        }
    }

}
