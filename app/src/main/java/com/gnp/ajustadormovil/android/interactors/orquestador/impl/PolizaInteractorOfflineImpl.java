package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.PolizaData;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.PolizaInteractor;
import com.gnp.ajustadormovil.android.models.SerieResponse;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Configuracion;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CostoParteAfectada;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Costo;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.util.List;

import io.reactivex.Single;

public class PolizaInteractorOfflineImpl implements PolizaInteractor {

    private static final String CLAVE_FACTOR_AJUSTE_TERCEROS = "FAT";

    private PolizaData data = OrquestadorFactory.getPolizaData();

    private SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    @Override
    public Single<Costo> getCostosAsegurado(String categoria, List<Long> partes) {
        return Single.create(e -> {
            e.onSuccess(getCosto(categoria, partes));
        });
    }

    @Override
    public Single<List<SerieResponse>> getPolizas(String vin) {
        return data.getPolizas(vin);
    }

    @Override
    public Single<Costo> getCostosTercero(String categoria, List<Long> partes) {
        return Single.create(e -> {
            Costo costo = getCosto(categoria, partes);
            Configuracion configuracion = RXUtils.sync(siniestrosInteractor.getConfiguracion(CLAVE_FACTOR_AJUSTE_TERCEROS));
            costo.setCosto(costo.getCosto() * Double.parseDouble(configuracion.getValor()));
            e.onSuccess(costo);
        });
    }

    private Costo getCosto(String categoria, List<Long> partes) throws Exception {
        List<CostoParteAfectada> costos = RXUtils.sync(siniestrosInteractor.getCostoParteAfectada(categoria, partes));
        Costo result = new Costo();
        result.setCosto(0.0);
        for(CostoParteAfectada costo : costos){
            result.setCosto(result.getCosto() + costo.getCosto());
        }
        return result;
    }

}
