package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjustadorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionData;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroData;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.amis.impl.FirmaDigitalHelper;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.documents.RecuperacionBuilder;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Comprobante;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionException;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.joda.time.LocalDate;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class RecuperacionInteractorOfflineImpl extends SyncInteractorOfflineImpl implements RecuperacionInteractor {

    private final RecuperacionData data = OrquestadorFactory.getRecuperacionData();

    private final RecuperacionRepository repository = RepositoryFactory.getRecuperacionRepository();

    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();

    private final AjustadorRepository ajustadorRepository = RepositoryFactory.getAjustadorRepository();

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();

    private final TerceroRepository terceroRepository = RepositoryFactory.getTerceroRepository();

    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();

    private final TerceroData terceroData = OrquestadorFactory.getTerceroData();

    private final CatInteractor catInteractor = CatalogoInteractorsFactory.getCatInteractor();

    private final TprInteractor tprService = CatalogoInteractorsFactory.getTprService();

    private final SiniestrosInteractor siniestroService = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();

    private final FirmaDigitalHelper builder = FirmaDigitalHelper.instance();

    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private final AmisInteractor amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();

    private Recuperacion findByAjusteAndTerceroAndId(String ajuste, Long tercero, Long id) {
        return findByIdOrLocalId(repository.findByAjusteAndTercero(ajuste, tercero), id);
    }

    @Override
    public Single<List<Recuperacion>> getRecuperaciones(String idAjuste, Long idTercero) {
        return Single.create(e -> {
            List<Recuperacion> noAutos = repository.findByAjusteAndTercero(idAjuste, idTercero);
            e.onSuccess(this.filter(noAutos));
        });
    }

    @Override
    public Single<Recuperacion> getRecuperacion(String idAjuste, Long idTercero, Long idRecuperacion) {
        return Single.create(e -> {
            Recuperacion tercero = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            if (tercero == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(tercero);
            }
        });
    }

    @Override
    public Completable deleteRecuperacion(String idAjuste, Long idTercero, Long idRecuperacion) {
        return Completable.create(e -> {
            Recuperacion recuperacion = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            Recuperacion rollback = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            if (recuperacion.isAgregado()) {
                repository.deleteByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
                e.onComplete();
            } else {
                recuperacion.setEliminado(true);
                repository.saveByAjusteAndTercero(idAjuste, idTercero, recuperacion);
                try {
                    synchronize(idAjuste);
                    e.onComplete();
                } catch (Exception ex) {
                    repository.saveByAjusteAndTercero(idAjuste, idTercero, rollback);
                    e.tryOnError(ex);
                }
            }
        });
    }

    @Override
    public Single<RecuperacionMonetaria> addMonetaria(String idAjuste, Long idTercero, RecuperacionMonetaria recuperacion) {
        return Single.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                assertRecuperacionMonetaria(recuperacion);
                if (recuperacion.getMoneda().equals("MXN")) {
                    recuperacion.setTipoCambio(1.0);
                }
                recuperacion.setMontoCalculado(recuperacion.getMonto() * recuperacion.getTipoCambio());
                initDescripcion(recuperacion);
                e.onSuccess(add(idAjuste, idTercero, recuperacion));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<RecuperacionTurnadoAsistenciaLegal> addTurnadoAsistenciaLegal(String idAjuste, Long idTercero, RecuperacionTurnadoAsistenciaLegal recuperacion) {
        return Single.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                initDescripcion(recuperacion);
                e.onSuccess(add(idAjuste, idTercero, recuperacion));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable modifyTurnadoAsistenciaLegal(String idAjuste, Long idTercero, Long idRecuperacion, RecuperacionTurnadoAsistenciaLegal recuperacion) {
        return Completable.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                initDescripcion(recuperacion);
                modify(idAjuste, idTercero, idRecuperacion, recuperacion);
                e.onComplete();
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<RecuperacionMesaControversia> addMesaControversia(String idAjuste, Long idTercero, RecuperacionMesaControversia recuperacion) {
        return Single.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                e.onSuccess(add(idAjuste, idTercero, recuperacion));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable modifyMesaControversia(String idAjuste, Long idTercero, Long idRecuperacion, RecuperacionMesaControversia recuperacion) {
        return Completable.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                modify(idAjuste, idTercero, idRecuperacion, recuperacion);
                e.onComplete();
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<RecuperacionGarantiaPrendaria> addGarantiaPrendaria(String idAjuste, Long idTercero, RecuperacionGarantiaPrendaria recuperacion) {
        return Single.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                assertRecuperacionGarantiaPrendaria(recuperacion);
                e.onSuccess(add(idAjuste, idTercero, recuperacion));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<RecuperacionCompaniaSeguros> addCompaniaSeguros(String idAjuste, Long idTercero, RecuperacionCompaniaSeguros recuperacion) {
        return Single.create(e -> {
            try {
                ErrorUtils.assertValues(recuperacion);
                initDescripcion(recuperacion);
                e.onSuccess(add(idAjuste, idTercero, recuperacion));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Archivo> addComprobante(String idAjuste, Long idTercero, Long idRecuperacion, Comprobante comprobante) {
        return Single.create(e -> {
            try {
                ErrorUtils.assertValues(comprobante);
                Recuperacion recuperacion = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
                RecuperacionMonetaria rollback = (RecuperacionMonetaria) repository.findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
                assertVolanteRecuperacion(recuperacion, comprobante);
                String entidadFinanciera = null;
                RecuperacionMonetaria monetaria = (RecuperacionMonetaria) recuperacion;
                Ajustador ajustador = ajustadorRepository.findOne();
                Ajuste ajuste = ajusteRepository.findById(idAjuste);
                Asegurado asegurado = aseguradoRepository.findByAjuste(idAjuste);
                VersionVehiculo vehiculo = getVehiculo(asegurado);
                String estado = RXUtils.sync(catInteractor.getEstado(comprobante.getEstado())).getNombre();
                String municipio = RXUtils.sync(catInteractor.getMunicipio(comprobante.getEstado(), comprobante.getMunicipio())).getNombre();
                String tipoRecuperacion = RXUtils.sync(siniestroService.getTipoRecuperacionMonetaria(monetaria.getTipoRecuperacionMonetaria())).getNombre();
                String contratante = getStringValue(ajuste, "siniestro.poliza.contratante.nombreCompleto");
                if (contratante == null) {
                    contratante = getStringValue(asegurado, "conductor.nombreCompleto");
                }
                if (((RecuperacionMonetaria) recuperacion).getTipoRecuperacionMonetaria() == 1) {
                    entidadFinanciera = RXUtils.sync(catInteractor.getEntidadFinanciera(((RecuperacionMonetaria) recuperacion).getEntidadFinanciera())).getNombre();
                }
                File documento = new File(Utils.TEMP_FOLDER + File.separator + "comprobante_" + ajuste.getSiniestro().getId() + "_" + idTercero + "_" + idRecuperacion + ".pdf");
                e.setDisposable(new RecuperacionBuilder(ajuste.getSiniestro().getId(), ajustador.getCedula())
                        .estado(estado)
                        .municipio(municipio)
                        .calleNumExtInt(comprobante.getDireccion())
                        .entreCalles(comprobante.getEntreCalles())
                        .contratanteNombreCompleto(contratante)
                        .ajustadorNombreCompleto(ajustador.getNombreCompleto())
                        .fechaCobro(new Date())
                        .recibimosDe(comprobante.getRecibimosDe())
                        .importeTotalLetra(comprobante.getImporteTotalLetra())
                        .tipoRecuperacionMonetaria(tipoRecuperacion)
                        .recuperacionEntidadFinanciera(entidadFinanciera)
                        .telefono(comprobante.getTelefono())
                        .tipoAfectado("ASEGURADO")
                        .montoRecibido(comprobante.getMontoRecibido())
                        .importeTotal(comprobante.getMontoAcordado())
                        .fechaPromesa(comprobante.getFechaPromesa())
                        .armadora(getStringValue(vehiculo, "carroceria.armadora.nombre"))
                        .carroceria(getStringValue(vehiculo, "carroceria.carroceria.nombre"))
                        .version(vehiculo.getNombre())
                        .modelo(vehiculo.getModelo())
                        .numeroMotor(asegurado.getVehiculo().getNumeroMotor())
                        .vin(asegurado.getVehiculo().getVin())
                        .placas(asegurado.getVehiculo().getPlacas())
                        .observaciones(comprobante.getObservaciones())
                        .numeroPoliza(getStringValue(ajuste, "siniestro.poliza.numero"))
                        .difImportes(comprobante.getMontoAcordado() - comprobante.getMontoRecibido())
                        .build(documento)
                        .observeOn(Schedulers.io())
                        .subscribeOn(Schedulers.io())
                        .subscribe(folio -> {
                            comprobante.setUrl(documento.getAbsolutePath());
                            comprobante.setFolio(folio);
                            monetaria.setComprobante(comprobante);
                            repository.saveByAjusteAndTercero(idAjuste, idTercero, monetaria);
                            try {
                                synchronize(idAjuste);
                                RecuperacionMonetaria stored = (RecuperacionMonetaria) repository.findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
                                e.onSuccess(new Archivo(stored.getComprobante().getUrl()));
                            } catch (Exception ex) {
                                repository.saveByAjusteAndTercero(idAjuste, idTercero, rollback);
                                e.tryOnError(ex);
                            }
                        }, e::tryOnError));
            } catch (Throwable ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Fotografia> addFotografia(String idAjuste, Long idTercero, Long idRecuperacion, File image) {
        return Single.create(e -> {
            Recuperacion recuperacion = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            Recuperacion rollback = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            Fotografia fotografia = createFotografia(image);
            recuperacion.getFotografias().add(fotografia);
            repository.saveByAjusteAndTercero(idAjuste, idTercero, recuperacion);
            try {
                synchronize(idAjuste);
                recuperacion = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
                fotografia = findByIdOrLocalId(recuperacion.getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.saveByAjusteAndTercero(idAjuste, idTercero, rollback);
                e.tryOnError(ex);

            }

        });
    }

    @Override
    public Single<List<Fotografia>> getFotografias(String idAjuste, Long idTercero, Long idRecuperacion) {
        return Single.create(e -> {
            Recuperacion recuperacion = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            e.onSuccess(filter(recuperacion.getFotografias()));
        });
    }

    @SuppressWarnings("unchecked")
    private <T extends Recuperacion> T add(String idAjuste, Long idTercero, T recuperacion) throws Exception {
        recuperacion.setId(generateLocalId());
        recuperacion.setLocalId(recuperacion.getId());
        recuperacion.setAgregado(true);
        recuperacion.setEliminado(false);
        repository.saveByAjusteAndTercero(idAjuste, idTercero, recuperacion);
        Recuperacion rollback = repository.findByAjusteAndTerceroAndId(idAjuste, idTercero, recuperacion.getId());
        try {
            synchronize(idAjuste);
            List<Recuperacion> recuperaciones = repository.findByAjusteAndTercero(idAjuste, idTercero);
            return (T) findByIdOrLocalId(recuperaciones, recuperacion);
        } catch (Exception ex) {
            repository.saveByAjusteAndTercero(idAjuste, idTercero, rollback);
            throw ex;
        }
    }

    private void modify(String idAjuste, Long idTercero, Long idRecuperacion, Recuperacion recuperacion) throws Exception {
        if (recuperacion instanceof Modificable) {
            Recuperacion old = findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            Recuperacion rollback = repository.findByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            recuperacion.setAgregado(old.isAgregado());
            recuperacion.setEliminado(old.isEliminado());
            recuperacion.setFotografias(old.getFotografias());
            recuperacion.setId(old.getId());
            recuperacion.setLocalId(old.getLocalId());
            ((Modificable) recuperacion).setModificado(!recuperacion.isAgregado());
            repository.deleteByAjusteAndTerceroAndId(idAjuste, idTercero, idRecuperacion);
            repository.saveByAjusteAndTercero(idAjuste, idTercero, recuperacion);

            try {
                synchronize(idAjuste);
            } catch (Exception ex) {
                repository.saveByAjusteAndTercero(idAjuste, idTercero, rollback);
                throw ex;
            }
        }

    }

    private void assertRecuperacionMonetaria(RecuperacionMonetaria recuperacion) {
        if (recuperacion.getTipoRecuperacionMonetariaTarjeta() != null && recuperacion.getEntidadFinanciera() == null) {
            throw new LogicException("entidad_financiera_required", ResourceUtils.getString("entidad_financiera_required"));
        }
        if (!(recuperacion.getTipoRecuperacionMonetaria() == 3) && (recuperacion.getNumeroAutorizacion() == null || recuperacion.getNumeroAutorizacion().equals(""))) {
            throw new LogicException("numero_autorizacion_required", ResourceUtils.getString("numero_autorizacion_required"));
        }
    }

    private void assertRecuperacionGarantiaPrendaria(RecuperacionGarantiaPrendaria recuperacion) {
        if (recuperacion.getDocumentoPropiedad() != null && recuperacion.getDocumentoPropiedad().equals("")) {
            throw new LogicException("otro_documento_propiedad_not_valid", ResourceUtils.getString("otro_documento_propiedad_not_valid"));
        }
    }

    private void assertVolanteRecuperacion(Recuperacion recuperacion, Comprobante comprobante) {
        if (!(recuperacion instanceof RecuperacionMonetaria)) {
            throw new ResourceNotFoundException();
        }
        if (comprobante.getMontoRecibido() > comprobante.getMontoAcordado()) {
            throw new LogicException("monto_recibido_not_valid", ResourceUtils.getString("monto_recibido_not_valid"));
        }
        if (LocalDate.fromDateFields(comprobante.getFechaPromesa()).isBefore(LocalDate.now())) {
            throw new LogicException("fecha_promesa_not_valid", ResourceUtils.getString("fecha_promesa_not_valid"));
        }
    }

    private void initDescripcion(Recuperacion rec) throws Exception {
        if (rec instanceof RecuperacionMonetaria) {
            RecuperacionMonetaria recuperacion = (RecuperacionMonetaria) rec;
            CatalogoId tipo = RXUtils.sync(siniestroService.getTipoRecuperacionMonetaria(recuperacion.getTipoRecuperacionMonetaria()));
            recuperacion.setNombreTipoRecuperacionMonetaria(tipo.getNombre());
        }
        if (rec instanceof RecuperacionTurnadoAsistenciaLegal) {
            RecuperacionTurnadoAsistenciaLegal recuperacion = (RecuperacionTurnadoAsistenciaLegal) rec;
            recuperacion.setNombreAseguradora(RXUtils.sync(catInteractor.getOtraAseguradora(recuperacion.getAseguradora())).getNombre());
        }
        if (rec instanceof RecuperacionCompaniaSeguros) {
            RecuperacionCompaniaSeguros recuperacion = (RecuperacionCompaniaSeguros) rec;
            recuperacion.setNombreAseguradora(RXUtils.sync(catInteractor.getOtraAseguradora(recuperacion.getAseguradora())).getNombre());
        }
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            cleanErrors(ajuste);
            Log.i(TAG, "Sincronizar datos de las recuperaciones del ajuste: " + ajuste);
            if (!isSynchronized(ajuste)) {
                try {
                    log(ajuste, "synchronize_recuperaciones");
                    List<Tercero> terceros = terceroRepository.findByAjuste(ajuste);
                    for (Tercero tercero : terceros) {
                        List<Recuperacion> recuperaciones = repository.findByAjusteAndTercero(ajuste, tercero.getId());
                        for (Recuperacion recuperacion : recuperaciones) {
                            synchronizeData(ajuste, tercero.getId(), recuperacion);
                        }
                    }
                    Log.i(TAG, "Datos de las recuperaciones del ajuste: " + ajuste + " soncronizados");
                    e.onComplete();
                } catch (Throwable ex) {
                    Log.e(TAG, "Error al sincronizar datos de las recuperaciones del ajuste: " + ajuste, ex);
                    e.tryOnError(ex);
                }
            } else {
                Log.i(TAG, "Los datos de las recuperaciones del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private void synchronizeData(String ajuste, Long tercero, Recuperacion recuperacion) throws NetworkException {
        try {
            log(ajuste, "synchronize_recuperacion", recuperacion.getDescripcion());
            syncAgregableModificableEliminable(ajuste, tercero, recuperacion);
            if (recuperacion instanceof RecuperacionMonetaria) {
                RecuperacionMonetaria monetaria = (RecuperacionMonetaria) recuperacion;
                if (monetaria.getComprobante() != null) {
                    syncComprobante(ajuste, tercero, monetaria);
                }
            }
            int size = recuperacion.getFotografias().size();
            syncAddFotografias(recuperacion,
                    (req, index) -> {
                        log(ajuste, "synchronize_recuperaciones_fotografias", recuperacion.getDescripcion(), index + 1, size);
                        return data.addFotografia(ajuste, tercero, recuperacion.getId(), req);
                    }, () -> repository.saveByAjusteAndTercero(ajuste, tercero, recuperacion));
        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new RecuperacionException(recuperacion.getDescripcion(), ex);
            }
        }
    }

    private void syncComprobante(String ajuste, Long tercero, RecuperacionMonetaria recuperacion) throws Exception {
        if (recuperacion.getComprobante() != null && !recuperacion.getComprobante().getUrl().startsWith("http")) {
            Archivo archivo = RXUtils.sync(data.addComprobante(ajuste, tercero, recuperacion.getId(), recuperacion.getComprobante()));
            recuperacion.getComprobante().setUrl(archivo.getUrl());
            repository.saveByAjusteAndTercero(ajuste, tercero, recuperacion);
        }
    }

    private void syncAgregableModificableEliminable(String ajuste, Long tercero, Recuperacion recuperacion) throws Throwable {
        Action modify = () -> repository.saveByAjusteAndTercero(ajuste, tercero, recuperacion);
        Consumer<Recuperacion> add = r -> {
            repository.deleteByAjusteAndTerceroAndId(ajuste, tercero, recuperacion.getLocalId());
            recuperacion.setId(r.getId());
            repository.saveByAjusteAndTercero(ajuste, tercero, recuperacion);
        };
        if (recuperacion instanceof RecuperacionMonetaria) {
            syncAgregable((RecuperacionMonetaria) recuperacion,
                    rec -> data.addMonetaria(ajuste, tercero, rec),
                    add::accept);
        }
        if (recuperacion instanceof RecuperacionMesaControversia) {
            syncAgregable((RecuperacionMesaControversia) recuperacion,
                    rec -> data.addMesaControversia(ajuste, tercero, rec),
                    add::accept);
            syncModificable((RecuperacionMesaControversia) recuperacion,
                    rec -> data.modifyMesaControversia(ajuste, tercero, recuperacion.getId(), rec),
                    modify);
        }
        if (recuperacion instanceof RecuperacionTurnadoAsistenciaLegal) {
            syncAgregable((RecuperacionTurnadoAsistenciaLegal) recuperacion,
                    rec -> data.addTurnadoAsistenciaLegal(ajuste, tercero, rec),
                    add::accept);
            syncModificable((RecuperacionTurnadoAsistenciaLegal) recuperacion,
                    rec -> data.modifyTurnadoAsistenciaLegal(ajuste, tercero, recuperacion.getId(), rec),
                    modify);
        }
        if (recuperacion instanceof RecuperacionGarantiaPrendaria) {
            syncAgregable((RecuperacionGarantiaPrendaria) recuperacion,
                    rec -> data.addGarantiaPrendaria(ajuste, tercero, rec),
                    add::accept);
        }
        if (recuperacion instanceof RecuperacionCompaniaSeguros) {
            syncAgregable((RecuperacionCompaniaSeguros) recuperacion,
                    rec -> data.addCompaniaSeguros(ajuste, tercero, rec),
                    add::accept);
        }
        syncEliminable(recuperacion, rec -> data.deleteRecuperacion(ajuste, tercero, rec.getId()),
                () -> repository.deleteByAjusteAndTerceroAndId(ajuste, tercero, recuperacion.getId()));
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        boolean sincronizado = true;
        List<Tercero> terceros = terceroRepository.findByAjuste(ajuste);
        for (Tercero tercero : terceros) {
            List<Recuperacion> recuperaciones = repository.findByAjusteAndTercero(ajuste, tercero.getId());
            for (Recuperacion recuperacion : recuperaciones) {
                sincronizado &= isSynchronized(recuperacion);
                if (recuperacion instanceof RecuperacionMonetaria) {
                    if (((RecuperacionMonetaria) recuperacion).getComprobante() != null
                            && !((RecuperacionMonetaria) recuperacion).getComprobante().getUrl().startsWith("http"))
                        sincronizado = false;
                }
            }
        }
        return sincronizado;
    }

    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Iniciando datos de las recuperaciones del ajuste: " + ajuste);
            try {
                List<Tercero> terceros = RXUtils.sync(terceroData.getAll(ajuste));
                for (Tercero tercero : terceros) {
                    List<Recuperacion> recuperaciones = RXUtils.sync(data.getRecuperaciones(ajuste, tercero.getId()));
                    for (Recuperacion recuperacion : recuperaciones) {
                        repository.deleteByAjusteAndTerceroAndId(ajuste, tercero.getId(), recuperacion.getId());
                        putLocalId(recuperacion);
                        initDescripcion(recuperacion);
                        repository.saveByAjusteAndTercero(ajuste, tercero.getId(), recuperacion);
                    }
                }
                Log.i(TAG, "Datos de las recuperaciones del ajuste: " + ajuste + " obtenidos");
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar las recuperaciones por obtener los terceros del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    /**
     * Obtiene el valor de la propiedad. Si en el arbol de llamados un objeto es null, la respuesta será null.
     *
     * @param target   Objeto para obtener su propiedad
     * @param property Nombre de la propiedad.
     * @return Valor de la expresión.
     */
    private Object getValue(@Nullable Object target, String property) {
        String root = "root";
        Expression jexlExp = new JexlEngine().createExpression(root + "." + property);
        JexlContext jc = new MapContext();
        jc.set("root", target);
        return jexlExp.evaluate(jc);
    }

    private String getStringValue(@Nullable Object target, String property) {
        Object result = getValue(target, property);
        return result != null ? result.toString() : "";
    }

    private VersionVehiculo getVehiculo(Asegurado asegurado) {
        VersionVehiculo vehiculo = new VersionVehiculo();
        if (asegurado.getVehiculo() != null) {
            try {
                vehiculo = RXUtils.sync(tprService.getVersion(
                        asegurado.getVehiculo().getTipoVehiculo(),
                        asegurado.getVehiculo().getArmadora(),
                        asegurado.getVehiculo().getCarroceria(),
                        asegurado.getVehiculo().getModelo().intValue(),
                        asegurado.getVehiculo().getVersion()));
            } catch (Throwable ex) {
                Log.e(TAG, "Error al obtener el vehículo", ex);
            }
        }
        return vehiculo;
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            List<Tercero> terceros = terceroRepository.findByAjuste(ajuste);
            for (Tercero tercero : terceros) {
                List<Recuperacion> recuperaciones = repository.findByAjusteAndTercero(ajuste, tercero.getId());
                for (Recuperacion recuperacion : recuperaciones) {
                    cleanData(ajuste, tercero.getId(), recuperacion);
                }
            }
            repository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Observable<Resumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            Resumen resumen = new Resumen();
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            List<Tercero> terceros = filter(terceroRepository.findByAjuste(ajuste));
            if (terceros.isEmpty() || responsabilidad.getAseguradoResponsable()) {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            }
            for (Tercero tercero : terceros) {
                List<Recuperacion> recuperaciones = filter(repository.findByAjusteAndTercero(ajuste, tercero.getId()));
                if (responsabilidad != null && responsabilidad.getTerceroResponsable() && recuperaciones.isEmpty()
                        && responsabilidad.getResponsabilidadAjustador().equals("B")) {
                    resumen.getErrores().add(ResourceUtils.getString("recuperacion_responsabilidad_tercero_required"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                for (Recuperacion recuperacion : recuperaciones) {
                    if (recuperacion instanceof RecuperacionMonetaria) {
                        if (((RecuperacionMonetaria) recuperacion).getComprobante() == null) {
                            resumen.getErrores().add(ResourceUtils.getString("comprobante_not_empty"));
                            resumen.setEstatus(EstatusResumen.ERROR);
                        }
                    }
                    if (recuperacion instanceof RecuperacionCompaniaSeguros) {
                        if (filter(recuperacion.getFotografias()).isEmpty()) {
                            resumen.getErrores().add(ResourceUtils.getString("compania_foto_not_empty"));
                            resumen.setEstatus(EstatusResumen.ERROR);
                        }
                    }
                }
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof RecuperacionException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof RecuperacionException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }

    private void cleanData(String ajuste, Long tercero, Recuperacion recuperacion) {
        for (Fotografia fotografia : recuperacion.getFotografias()) {
            fotografiaRepository.delete(fotografia);
        }
        repository.deleteByAjusteAndTerceroAndId(ajuste, tercero, recuperacion.getId());
    }

}
