package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadData;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroRepository;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.ResponsabilidadException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class ResponsabilidadInteractorOfflineImpl extends SyncInteractorOfflineImpl implements ResponsabilidadInteractor {

    private final ResponsabilidadRepository repository = RepositoryFactory.getResponsabilidadRepository();

    private final ResponsabilidadData data = OrquestadorFactory.getResponsabilidadData();

    private final TerceroRepository terceroRepository = RepositoryFactory.getTerceroRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();


    private Tercero findByAjusteAndId(String ajuste, Long id) {
        return findByIdOrLocalId(terceroRepository.findByAjuste(ajuste), id);
    }

    @Override
    public Single<Responsabilidad> addResponsabilidad(String idAjuste, Responsabilidad responsabilidad) {
        return Single.create(e -> {
            Responsabilidad actual = repository.findByAjuste(idAjuste);
            Responsabilidad rollback = repository.findByAjuste(idAjuste);
            if(actual != null && actual.getFotografias().isEmpty()) {
                RXUtils.sync(deleteResponsabilidad(idAjuste));
            }
            ErrorUtils.assertValues(responsabilidad);
            responsabilidad.setAjuste(idAjuste);
            assertResponsabilidad(responsabilidad);
            responsabilidad.setAgregado(true);
            responsabilidad.setEliminado(false);
            repository.save(responsabilidad);
            try {
                synchronize(idAjuste);
                e.onSuccess(repository.findByAjuste(idAjuste));
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Fotografia> addFotografia(String ajuste, File image) {
        return Single.create(e -> {
            Responsabilidad responsabilidad = repository.findByAjuste(ajuste);
            Responsabilidad rollback = repository.findByAjuste(ajuste);
            Fotografia fotografia = createFotografia(image);
            responsabilidad.getFotografias().add(fotografia);
            repository.save(responsabilidad);
            try {
                synchronize(ajuste);
                responsabilidad = repository.findByAjuste(ajuste);
                fotografia = findByIdOrLocalId(responsabilidad.getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteResponsabilidad(String ajuste) {
        return Completable.create(e -> {
            Responsabilidad responsabilidad = repository.findByAjuste(ajuste);
            Responsabilidad rollback = repository.findByAjuste(ajuste);
            assertResponsabilidad(responsabilidad);
            if (responsabilidad.isAgregado()) {
                repository.deleteByAjuste(ajuste);
            } else {
                responsabilidad.setEliminado(true);
                repository.save(responsabilidad);
            }
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Responsabilidad> getResponsabilidad(String ajuste) {
        return Single.create(e -> {
            Responsabilidad responsabilidad = repository.findByAjuste(ajuste);
            if (responsabilidad == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(responsabilidad);
            }
        });
    }

    private void assertResponsabilidad(Responsabilidad responsabilidad) {
        List<Tercero> terceros = terceroRepository.findByAjuste(responsabilidad.getAjuste());

        if (repository.findByAjuste(responsabilidad.getAjuste()) != null) {
            throw new LogicException("unique_value_exception", ResourceUtils.getString("unique_value_exception"));
        }

        if (!terceros.isEmpty() && responsabilidad.getTipoTercero().equals("ST")) {
            throw new LogicException("tercero_not_valid", "No se puede asignar la responsabilidad 'sin terceros' debido a que hay terceros registrados");
        }
        if (!responsabilidad.getResponsabilidadAjustador().equals("C") && responsabilidad.getAseguradoResponsable() && responsabilidad.getTerceroResponsable()) {
            throw new LogicException("asegurado_responsable", ResourceUtils.getString("asegurado_tercero_responsable"));
        }

        if (terceros.isEmpty() && responsabilidad.getTerceroResponsable() && responsabilidad.getTipoTercero().equals("ST")) {
            throw new LogicException("tercero_not_valid", "El asegurado es responsable si no se registró ningún tercero");
        }


    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            cleanErrors(ajuste);
            Log.i(TAG, "Sincronizar datos de la responsabilidad del ajuste: " + ajuste);
            if (!isSynchronized(ajuste)) {
                try {
                    log(ajuste, "synchronize_responsabilidad");
                    Responsabilidad responsabilidad = repository.findByAjuste(ajuste);
                    try {
                        syncAgregable(responsabilidad, r -> data.addResponsabilidad(ajuste, r), r -> {
                            responsabilidad.setAgregado(false);
                            repository.save(responsabilidad);
                        });
                    } catch (LogicException ex) {
                        if ("unique_value".equals(ex.getError().getError())) {
                            responsabilidad.setAgregado(false);
                            repository.save(responsabilidad);
                        } else {
                            e.tryOnError(ex);
                        }
                    }
                    syncEliminable(responsabilidad,
                            r -> data.deleteResponsabilidad(ajuste),
                            () -> repository.deleteByAjuste(ajuste));
                    syncAddFotografias(responsabilidad.getFotografias(),
                            (req, index) -> {
                                log(ajuste, "synchronize_responsabilidad_fotografias");
                                return data.addFotografia(ajuste, req);
                            }, () -> repository.save(responsabilidad));
                    Log.i(TAG, "Datos de la responsabilidad del ajuste: " + ajuste + " sincronizados");
                    e.onComplete();
                } catch (NetworkException | UnknownException ex) {
                    e.tryOnError(ex);
                } catch (Exception ex) {
                    e.tryOnError(new ResponsabilidadException(ex));
                }
            } else {
                Log.i(TAG, "Los datos de la responsabilidad del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }

        });
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        Responsabilidad responsabilidad = repository.findByAjuste(ajuste);
        if (responsabilidad != null) {
            if (responsabilidad.isAgregado() || responsabilidad.isEliminado()) {
                return false;
            }
            for (Fotografia fotografia : responsabilidad.getFotografias()) {
                if (fotografia.isEliminado() || fotografia.isAgregado()) {
                    return false;
                }

            }
        }
        return true;
    }

    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Iniciando datos de la responsabilidad del ajuste: " + ajuste);
            try {
                Responsabilidad responsabilidad = RXUtils.sync(data.getResponsabilidad(ajuste));
                responsabilidad.setAjuste(ajuste);
                putFotografiasLocalId(responsabilidad);
                repository.save(responsabilidad);
                Log.i(TAG, "Datos de la responsabilidad del ajuste: " + ajuste + " obtenidos");
                e.onComplete();
            } catch (ResourceNotFoundException ex) {
                Log.w(TAG, "No se encontraron datos de la responsabilidad del ajuste: " + ajuste);
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar los datos de la responsabilidad del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            cleanData(repository.findByAjuste(ajuste));
            e.onComplete();
        });
    }

    @Override
    public Observable<Resumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            Resumen resumen = new Resumen();
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            Responsabilidad responsabilidad = repository.findByAjuste(ajuste);
            if (responsabilidad != null) {
                if (responsabilidad.getFotografias().isEmpty()) {
                    resumen.setEstatus(EstatusResumen.ERROR);
                    resumen.getErrores().add(ResourceUtils.getString("responsabilidad_fotos"));
                }
                validateTerceroResponsable(responsabilidad, resumen);
            } else if (!filter(terceroRepository.findByAjuste(ajuste)).isEmpty()) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.getErrores().add(ResourceUtils.getString("responsabilidad_empty"));
            } else {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof ResponsabilidadException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    private void validateTerceroResponsable(Responsabilidad responsabilidad, Resumen resumen) {
        if(!"TFR".equals(responsabilidad.getTipoTercero()) && !"ST".equals(responsabilidad.getTipoTercero())) {
            List<Tercero> terceros = terceroRepository.findByAjuste(responsabilidad.getAjuste());
            if (!CollectionsUtils.anyMatch(terceros, tercero -> tercero.getTipo().equals(responsabilidad.getTipoTercero()))) {
                resumen.getErrores().add(ResourceUtils.getString("responsabilidad_sin_" + responsabilidad.getTipoTercero().toLowerCase()));
                resumen.setEstatus(EstatusResumen.ERROR);
            }
        }
    }


    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof ResponsabilidadException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }

    private void cleanData(Responsabilidad responsabilidad) {
        if (responsabilidad != null) {
            for (Fotografia fotografia : responsabilidad.getFotografias()) {
                fotografiaRepository.delete(fotografia);
            }
            repository.deleteByAjuste(responsabilidad.getAjuste());
        }
    }

}
