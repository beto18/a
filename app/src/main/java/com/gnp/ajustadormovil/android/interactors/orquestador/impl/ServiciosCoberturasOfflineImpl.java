package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ServicioCoberturaData;
import com.gnp.ajustadormovil.android.data.orquestador.ServicioCoberturaRepository;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.ServiciosCoberturaInteractor;
import com.gnp.ajustadormovil.android.models.ComentarioCancelacion;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.servicio.ServicioCoberturaException;
import com.gnp.ajustadormovil.android.models.orquestador.servicio.ServiciosCobertura;
import com.gnp.ajustadormovil.android.models.services.ArriboServicio;
import com.gnp.ajustadormovil.android.models.services.ServicioResponse;
import com.gnp.ajustadormovil.android.models.services.service.ServiceData;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class ServiciosCoberturasOfflineImpl extends SyncInteractorOfflineImpl implements ServiciosCoberturaInteractor {

    private final ServicioCoberturaData data = OrquestadorFactory.getServicioCoberturaData();

    private final ServicioCoberturaRepository repository = RepositoryFactory.getServicioCoberturaRepository();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    @Override
    public Completable sendStatus(String ajuste) {
        return data.sendStatus(ajuste);
    }

    @Override
    public Single<List<ServicioResponse>> getServicioCobertura(String servicioAjuste) {
        return Single.create(e -> {
            List<ServicioResponse> result = RXUtils.sync(data.getServicioCobertura(servicioAjuste));
            e.onSuccess(result);
        });
    }

    @Override
    public Completable getServicioCoberturaDelete(String ajuste, int idServicio, ComentarioCancelacion comentarioCancelacion) {
        return data.getServicioCoberturaDelete(ajuste, idServicio, comentarioCancelacion)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Single<String> getServicioCoberturaArribo(ArriboServicio arriboServicio) {
        return data.getServicioCoberturaArribo(arriboServicio);
    }

    @Override
    public Completable sendPolices(String ajuste, ServiceData serviceData) {
        return data.sendPolices(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendLawyer(String ajuste, ServiceData serviceData) {
        return data.sendLawyer(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendCrane(String ajuste, ServiceData serviceData) {
        return data.sendCrane(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendAmbulance(String ajuste, ServiceData serviceData) {
        return data.sendAmbulance(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendChecker(String ajuste, ServiceData serviceData) {
        return data.sendChecker(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendRecuperator(String ajuste, ServiceData serviceData) {
        return data.sendRecuperator(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendFunerary(String ajuste, ServiceData serviceData) {
        return data.sendFunerary(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendContractor(String ajuste, ServiceData serviceData) {
        return data.sendContractor(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendTaxi(String ajuste, ServiceData serviceData) {
        return data.sendTaxi(ajuste, serviceData)
                .andThen(updateResumen(ajuste));
    }

    @Override
    public Completable sendArribo(String ajuste, String id) {
        return data.sendArribo(ajuste, id);
    }

    @Override
    public Completable addResumen(String ajuste, List<String> servicios) {
        return Completable.create(e -> {
            ServiciosCobertura cobertura = getServicios(ajuste);
            ServiciosCobertura rollback = getServicios(ajuste);
            cobertura.setServicios(removeRepetidos(servicios));
            cobertura.setModificado(true);
            repository.save(cobertura);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "getResumen: ", ex);
                repository.save(rollback);
                e.tryOnError(ex);
            }
        });
    }

    private Completable updateResumen(String ajuste) {
        return Completable.create(e -> {
            try {
                List<ServicioResponse> servicios = RXUtils.sync(data.getServicioCobertura(ajuste));
                ServiciosCobertura resumen = getServicios(ajuste);
                resumen.getServicios().clear();
                for (ServicioResponse servicio : servicios) {
                    if (!"CANCELADO".equals(servicio.getStatus())) {
                        resumen.getServicios().add(servicio.getTipoServicio());
                    }
                }
                resumen.setServicios(removeRepetidos(resumen.getServicios()));
                repository.save(resumen);
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "getResumen: ", ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<String>> getResumen(String ajuste) {
        return Single.create(e -> {
            try {
                e.onSuccess(getServicios(ajuste).getServicios());
            } catch (Exception ex) {
                Log.e(TAG, "getResumen: ", ex);
                e.tryOnError(ex);
            }
        });
    }

    private ServiciosCobertura getServicios(String ajuste) {
        ServiciosCobertura resumen = repository.findByAjuste(ajuste);
        if (resumen == null) {
            resumen = new ServiciosCobertura();
            resumen.setAjuste(ajuste);
            repository.save(resumen);
        }
        return resumen;
    }

    private List<String> removeRepetidos(List<String> servicios) {
        Set<String> items = new HashSet<>(servicios);
        return new ArrayList<>(items);
    }

    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Iniciando datos de los servicios del ajuste: " + ajuste);
            try {
                List<String> servicios = RXUtils.sync(data.getResumen(ajuste));
                ServiciosCobertura resumen = getServicios(ajuste);
                resumen.setServicios(servicios);
                repository.save(resumen);
                e.onComplete();
                Log.i(TAG, "Datos de los servicios del ajuste: " + ajuste + " obtenidos");
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar los datos de los servicios del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            repository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Sincronizar datos de los servicios del ajuste: " + ajuste);
            if (!isSynchronized(ajuste)) {
                try {
                    log(ajuste, "synchronize_serviciosDeCobertura");
                    ServiciosCobertura resumen = getServicios(ajuste);
                    syncModificable(resumen,
                            c -> Completable.fromSingle(data.addResumen(ajuste, c.getServicios())),
                            () -> repository.save(resumen));
                    Log.i(TAG, "Datos de los servicios del ajuste: " + ajuste + " sincronizados");
                    e.onComplete();
                } catch (Throwable ex) {
                    Log.e(TAG, "Error al sincronizar datos de los servicios del ajuste: " + ajuste, ex);
                    if (ex instanceof NetworkException) {
                        e.tryOnError(ex);
                    } else if (ex instanceof UnknownException) {
                        e.tryOnError(ex);
                    } else {
                        e.tryOnError(new ServicioCoberturaException(ex));
                    }
                }
            } else {
                Log.i(TAG, "Los datos de los servicios del ajuste: " + ajuste + " ya fueron sincronizados");
                e.onComplete();
            }
        });
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        ServiciosCobertura resumen = getServicios(ajuste);
        return resumen.getServicios().isEmpty() || !resumen.isModificado();
    }

    @Override
    public Observable<Resumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            Resumen resumen = new Resumen();
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            ServiciosCobertura servicios = getServicios(ajuste);
            if (servicios.getServicios().isEmpty()) {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof ServicioCoberturaException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof ServicioCoberturaException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }
}