package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.SyncInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Agregable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografiable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.LocalEntity;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.utils.media.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public abstract class SyncInteractorOfflineImpl extends ConcurrentAjusteSync implements SyncInteractor {

    protected final String TAG = this.getClass().getSimpleName();

    private final ConnectionProvider connection = ConnectionProvider.getInstance();

    private final Repository<Fotografia, Long> fotografiaRepository = RepositoryFactory.getRepository(Fotografia.class, Long.class);

    protected void synchronize(String ajuste) throws Exception {
        cleanErrors(ajuste);
        if (connection.isConnected(ajuste)) {
            Log.i(TAG, "Synchronize ajuste: " + ajuste);
            try {
                RXUtils.sync(synchronizeData(ajuste));
            } catch (NetworkException | UnknownException e) {
                Log.w(TAG, "El ajuste '" + ajuste + "' no se ha podido sincronizar debido a falta de conectividad o a un error inesperado en el servidor, se atenderá offline");
                connection.notConnected(ajuste);
            }
        } else {
            Log.w(TAG, "No se puede sincronizar el ajuste '" + ajuste + "' debido a que se encuentra en modo de atención offline");
        }
    }

    protected <T extends Eliminable> List<T> filter(List<T> source) {
        List<T> result = new ArrayList<>();
        if (source != null) {
            for (T item : source) {
                if (!item.isEliminado()) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    protected <T extends Agregable> void syncAgregable(T agregable, Function<T, Single<T>> func, Consumer<T> doOnFinish) throws Exception {
        if (agregable != null && agregable.isAgregado()) {
            Log.d(TAG, "Start syncAgregable: " + agregable.getClass().getSimpleName());
            try {
                T result = RXUtils.sync(func.apply(agregable));
                agregable.setAgregado(false);
                if (doOnFinish != null) {
                    doOnFinish.accept(result);
                }
                Log.d(TAG, "Finish syncAgregable: " + agregable.getClass().getSimpleName());
            } catch (Exception ex) {
                Log.w(TAG, "Error syncAgregable: " + agregable.getClass().getSimpleName(), ex);
                throw ex;
            }
        }
    }

    protected <T extends Modificable> void syncModificable(T modificable, Function<T, Completable> func, Action doOnFinish) throws Exception {
        if (modificable != null && modificable.isModificado()) {
            Log.d(TAG, "Start syncModificable: " + modificable.getClass().getSimpleName());
            try {
                RXUtils.sync(func.apply(modificable));
                modificable.setModificado(false);
                if (doOnFinish != null) {
                    doOnFinish.run();
                }
                Log.d(TAG, "Finish syncModificable: " + modificable.getClass().getSimpleName());
            } catch (Exception ex) {
                Log.w(TAG, "Error syncModificable: " + modificable.getClass().getSimpleName(), ex);
                throw ex;
            }
        }
    }

    protected <T extends Eliminable> void syncEliminable(List<T> eliminables, Function<T, Completable> func, Consumer<T> doOnFinish) throws Exception {
        if (eliminables != null) {
            List<T> temp = new ArrayList<>(eliminables);
            for (T eliminable : temp) {
                if (eliminable.isEliminado()) {
                    Log.d(TAG, "Start syncEliminable: " + eliminable.getClass().getSimpleName());
                    try {
                        eliminable.setEliminado(false);
                        RXUtils.sync(func.apply(eliminable));
                        eliminables.remove(eliminable);
                        if (doOnFinish != null) {
                            doOnFinish.accept(eliminable);
                        }
                        Log.d(TAG, "Finish syncModificable: " + eliminable.getClass().getSimpleName());
                    } catch (Exception ex) {
                        Log.w(TAG, "Error syncEliminable: " + eliminable.getClass().getSimpleName(), ex);
                        throw ex;
                    }
                }
            }
        }
    }

    protected <T extends Eliminable> void syncEliminable(T eliminable, Function<T, Completable> func, Action doOnFinish) throws Exception {
        if (eliminable.isEliminado()) {
            Log.d(TAG, "Start syncEliminable: " + eliminable.getClass().getSimpleName());
            try {
                RXUtils.sync(func.apply(eliminable));
                eliminable.setEliminado(false);
                if (doOnFinish != null) {
                    doOnFinish.run();
                }
                Log.d(TAG, "Finish syncModificable: " + eliminable.getClass().getSimpleName());
            } catch (Exception ex) {
                Log.w(TAG, "Error syncEliminable: " + eliminable.getClass().getSimpleName(), ex);
                throw ex;
            }
        }
    }

    protected void syncAddFotografias(List<Fotografia> fotografias, BiFunction<RequestBody, Integer, Single<List<Fotografia>>> func, Action doOnFinish) throws Exception {
        if (fotografias != null && !fotografias.isEmpty()) {
            int i = 0;
            for (Fotografia fotografia : fotografias) {
                File file = new File(fotografia.getUrl());
                if (fotografia.isAgregado() && file.exists()) {
                    Log.d(TAG, "Start syncAddFotografia: " + fotografia.getUrl());
                    RequestBody request = RequestBody.create(MediaType.parse("image/jpg"), file);
                    try {
                        Fotografia foto = RXUtils.sync(func.apply(request, i)).get(0);
                        fotografiaRepository.deleteById(fotografia.getId());
                        fotografia.setUrl(foto.getUrl());
                        fotografia.setId(foto.getId());
                        fotografia.setAgregado(false);
                        fotografiaRepository.save(fotografia);
                        if (doOnFinish != null) {
                            doOnFinish.run();
                        }
                        Log.d(TAG, "Finish syncAddFotografia: " + fotografia.getUrl());
                    } catch (Exception ex) {
                        Log.w(TAG, "Error syncAddFotografia: " + fotografia.getUrl(), ex);
                        throw ex;
                    }
                }
                i++;
            }
        }
    }

    protected void syncAddFotografias(Fotografiable fotografiable, BiFunction<RequestBody, Integer, Single<List<Fotografia>>> func, Action doOnFinish) throws Throwable {
        if (fotografiable != null) {
            syncAddFotografias(fotografiable.getFotografias(), func, doOnFinish);
        }
    }


    protected void syncRemoveFotografias(List<Fotografia> fotografias, Function<Long, Completable> func, Action doOnFinish) throws Throwable {
        if (fotografias != null && !fotografias.isEmpty()) {
            List<Fotografia> fotos = new ArrayList<>(fotografias);
            for (Fotografia fotografia : fotos) {
                if (fotografia.isEliminado()) {
                    if (fotografia.getUrl().startsWith("http")) {
                        Log.d(TAG, "Start syncRemoveFotografia: " + fotografia.getUrl());
                        try {
                            RXUtils.sync(func.apply(fotografia.getId()));
                        } catch (Exception ex) {
                            Log.w(TAG, "Error syncRemoveFotografia: " + fotografia.getUrl(), ex);
                            throw ex;
                        }
                    } else {
                        fotografia.setAgregado(false);
                        File file = new File(fotografia.getUrl());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                    fotografia.setEliminado(false);
                    fotografiaRepository.deleteById(fotografia.getId());
                    fotografias.remove(fotografia);
                    if (doOnFinish != null) {
                        doOnFinish.run();
                    }
                    Log.d(TAG, "Finish syncRemoveFotografia: " + fotografia.getUrl());
                }
            }
        }
    }

    protected void syncRemoveFotografias(Fotografiable fotografiable, Function<Long, Completable> func, Action doOnFinish) throws Throwable {
        if (fotografiable != null) {
            syncRemoveFotografias(fotografiable.getFotografias(), func, doOnFinish);
        }
    }

    protected boolean isSynchronized(Object obj) {
        boolean sincronizado = true;
        if (obj != null) {
            if (obj instanceof Agregable) {
                sincronizado &= !((Agregable) obj).isAgregado();
            }
            if (obj instanceof Modificable) {
                sincronizado &= !((Modificable) obj).isModificado();
            }
            if (obj instanceof Eliminable) {
                sincronizado &= !((Eliminable) obj).isEliminado();
            }
            if (obj instanceof Fotografiable) {
                for (Fotografia fotografia : ((Fotografiable) obj).getFotografias()) {
                    sincronizado &= !fotografia.isAgregado() && !fotografia.isEliminado();
                }
            }

        }
        return sincronizado;
    }

    protected Fotografia createFotografia(File file) throws IOException {
        synchronized (Fotografia.class) {
            Fotografia fotografia = new Fotografia();
            fotografia.setAgregado(true);
            fotografia.setEliminado(false);
            fotografia.setId(generateLocalId());
            fotografia.setLocalId(fotografia.getId());
            String url = Utils.TEMP_FOLDER + File.separator + fotografia.getId() + ".jpg";
            FileUtils.copy(file, url);
            Log.d(TAG, "Existe: " + new File(url).exists());
            fotografia.setUrl(url);
            fotografia.setLocalUrl(url);
            return fotografia;
        }
    }

    protected void putLocalId(LocalEntity entity) {
        if (entity != null) {
            entity.setLocalId(entity.getId());
            if (entity instanceof Fotografiable) {
                putFotografiasLocalId((Fotografiable) entity);
            }
        }
    }

    protected void putFotografiasLocalId(Fotografiable fotografiable) {
        if (fotografiable != null) {
            for (Fotografia fotografia : fotografiable.getFotografias()) {
                fotografia.setLocalId(fotografia.getId());
                fotografia.setLocalUrl(fotografia.getUrl());
            }
        }
    }

    private void delay(long milis) {
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e) {
            Log.w(TAG, "Delay interrumpido", e);
        }
    }

    protected synchronized long generateLocalId() {
        delay(1L);
        return -System.currentTimeMillis();
    }

    protected <T extends LocalEntity> T findByIdOrLocalId(Collection<T> elements, Long id) {
        for (T element : elements) {
            if (element.getId().equals(id) || (element.getLocalId() != null && element.getLocalId().equals(id))) {
                return element;
            }
        }
        throw new ResourceNotFoundException();
    }

    protected <T extends LocalEntity> T findByIdOrLocalId(Collection<T> elements, T entity) {
        return findByIdOrLocalId(elements, entity.getLocalId());
    }

    protected abstract void cleanErrors(String ajuste);

}
