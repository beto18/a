package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoData;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAutoConductorException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAutoException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAutoFolio;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAutoOcupanteException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAutoVehiculoException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

public class TerceroAutoInteractorOfflineImpl extends SyncInteractorOfflineImpl implements TerceroAutoInteractor {

    private final TerceroAutoData data = OrquestadorFactory.getTerceroAutoData();

    private final TerceroAutoRepository repository = RepositoryFactory.getTerceroAutoRepository();

    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();

    private final RecuperacionRepository recuperacionRepository = RepositoryFactory.getRecuperacionRepository();

    private final TprInteractor tprService = CatalogoInteractorsFactory.getTprService();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();

    private final Repository<Persona, Long> personaRepository = RepositoryFactory.getRepository(Persona.class, Long.class);

    private final Repository<Ocupante, Long> ocupanteRepository = RepositoryFactory.getRepository(Ocupante.class, Long.class);

    private TerceroAuto findByAjusteAndId(String ajuste, Long id){
        return findByIdOrLocalId(repository.findByAjuste(ajuste), id);
    }


    @Override
    public Single<List<TerceroAuto>> getAll(String ajuste) {
        return Single.create(e -> {
            try {
                List<TerceroAuto> tercerosAutos = repository.findByAjuste(ajuste);
                e.onSuccess(this.filter(tercerosAutos));
            } catch (Exception ex) {
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<TerceroAuto> add(String ajuste) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = new TerceroAuto();
            terceroAuto.setId(generateLocalId());
            terceroAuto.setLocalId(terceroAuto.getId());
            terceroAuto.setDescripcion("Sin información del tercero");
            terceroAuto.setAgregado(true);
            terceroAuto.setEliminado(false);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                terceroAuto = findByIdOrLocalId(repository.findByAjuste(ajuste), terceroAuto);
                e.onSuccess(terceroAuto);
            } catch (Exception ex){
                repository.deleteByAjusteAndId(ajuste, terceroAuto.getId());
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<TerceroAuto> get(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            if (terceroAuto == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(terceroAuto);
            }
        });
    }

    @Override
    public Single<TerceroAuto> get(String ajuste, String folioAmis) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = repository.findByAjusteAndFolioAmis(ajuste, folioAmis);
            if (terceroAuto == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(terceroAuto);
            }
        });
    }

    @Override
    public Completable delete(String ajuste, long idTercero) {
        return Completable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
            assertDeleteTercerosAutos(terceroAuto, responsabilidad);
            if (terceroAuto.isAgregado()) {
                repository.deleteByAjusteAndId(ajuste, idTercero);
            } else {
                terceroAuto.setEliminado(true);
                repository.saveByAjuste(ajuste, terceroAuto);
            }
            List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(ajuste, idTercero);
            recuperacionRepository.deleteByAjusteAndTercero(ajuste, idTercero);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                for (Recuperacion recuperacion : recuperaciones) {
                    recuperacionRepository.saveByAjusteAndTercero(ajuste, idTercero, recuperacion);
                }
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable modifyConductor(String ajuste, long idTercero, Persona conductor) {
        return Completable.create(e -> {
            assertValues(conductor);
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            if (terceroAuto!= null && terceroAuto.getConductor() != null) {
                conductor.setFotografias(terceroAuto.getConductor().getFotografias());
                conductor.setVolante(terceroAuto.getConductor().getVolante());
                conductor.setId(terceroAuto.getConductor().getId());
                conductor.setInvolucradoId(terceroAuto.getConductor().getInvolucradoId());
            }
            assertTerceroAutoConductor(conductor);
            terceroAuto.setConductor(conductor);
            terceroAuto.getConductor().setModificado(true);
            if (terceroAuto.getConductor().getId() == null) {
                terceroAuto.getConductor().setId(generateLocalId());
            }

            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    private void assertValues(Afectado afectado) {
        ErrorUtils.assertValues(afectado);
        for (Telefono telefono : afectado.getTelefonos()) {
            ErrorUtils.assertValues(telefono);
        }
    }

    @Override
    public Single<List<Fotografia>> getFotografiasConductor(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            if (terceroAuto.getConductor() != null) {
                e.onSuccess(filter(terceroAuto.getConductor().getFotografias()));

            } else {
                e.onSuccess(new ArrayList<>(0));
            }
        });
    }

    @Override
    public Single<Fotografia> addFotografiaConductor(String ajuste, long idTercero, File image) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia fotografia = createFotografia(image);
            terceroAuto.getConductor().getFotografias().add(fotografia);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                terceroAuto = findByAjusteAndId(ajuste, idTercero);
                fotografia = findByIdOrLocalId(terceroAuto.getConductor().getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografiaConductor(String ajuste, long idTercero, long idFotografia) {
        return Completable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia deleted = null;
            for (Fotografia fotografia : terceroAuto.getConductor().getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            if (deleted.isAgregado()) {
                terceroAuto.getConductor().getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);
            }
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<VehiculoTercero> getVehiculo(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            if (terceroAuto.getVehiculo() == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(terceroAuto.getVehiculo());
            }
        });
    }

    @Override
    public Completable modifyVehiculo(String ajuste, long idTercero, VehiculoTercero vehiculo) {
        return Completable.create(e -> {
            ErrorUtils.assertValues(vehiculo);
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);

            if (terceroAuto.getVehiculo() != null) {
                vehiculo.setFotografias(terceroAuto.getVehiculo().getFotografias());
                vehiculo.setVolante(terceroAuto.getVehiculo().getVolante());
            }
            assertTerceroAutoVehiculo(vehiculo);
            VersionVehiculo versionVehiculo = RXUtils.sync(tprService.getVersion(vehiculo.getTipoVehiculo(),
                    vehiculo.getArmadora(), vehiculo.getCarroceria(), vehiculo.getModelo().intValue(), vehiculo.getVersion()));
            terceroAuto.setDescripcion(versionVehiculo.getNombre());
            terceroAuto.setVehiculo(vehiculo);
            terceroAuto.getVehiculo().setModificado(true);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Observable<List<Fotografia>> getFotografiaVehiculo(String ajuste, long idTercero) {
        return Observable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            if (terceroAuto.getVehiculo() != null) {
                e.onNext(filter(terceroAuto.getVehiculo().getFotografias()));
            } else {
                e.onNext(new ArrayList<>(0));
            }
            e.onComplete();
        });
    }

    @Override
    public Observable<Fotografia> addFotografiaVehiculo(String ajuste, long idTercero, File image) {
        return Observable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia fotografia = createFotografia(image);
            terceroAuto.getVehiculo().getFotografias().add(fotografia);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                terceroAuto = findByAjusteAndId(ajuste, idTercero);
                fotografia = findByIdOrLocalId(terceroAuto.getVehiculo().getFotografias(), fotografia);
                e.onNext(fotografia);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografiaVehiculo(String ajuste, long idTercero, long idFotografia) {
        return Completable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia deleted = null;
            for (Fotografia fotografia : terceroAuto.getVehiculo().getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            if (deleted.isAgregado()) {
                terceroAuto.getVehiculo().getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);
            }
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<Ocupante>> getOcupantes(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            if (terceroAuto == null || terceroAuto.getOcupantes().isEmpty()) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(this.filter(terceroAuto.getOcupantes()));
            }

        });
    }

    @Override
    public Single<Ocupante> addOcupante(String ajuste, long idTercero, Ocupante ocupante) {
        return Single.create(e -> {
            assertValues(ocupante);
            assertTerceroAutoOcupante(ocupante);
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            ocupante.setId(generateLocalId());
            ocupante.setLocalId(ocupante.getId());
            ocupante.setAgregado(true);
            ocupante.setEliminado(false);
            ocupante.setModificado(false);
            terceroAuto.getOcupantes().add(ocupante);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                terceroAuto = findByAjusteAndId(ajuste, idTercero);
                e.onSuccess(findByIdOrLocalId(terceroAuto.getOcupantes(), ocupante));
            } catch (Exception ex){
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<Ocupante> getOcupante(String ajuste, long idTercero, long idOcupante) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            Ocupante ocupante = findByIdOrLocalId(terceroAuto.getOcupantes(), idOcupante);
            e.onSuccess(ocupante);
        });
    }

    @Override
    public Completable modifyOcupante(String ajuste, long idTercero, long idOcupante, Ocupante ocupante) {
        return Completable.create(e -> {
            assertValues(ocupante);
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Ocupante old = findByIdOrLocalId(terceroAuto.getOcupantes(), idOcupante);
            ocupante.setAgregado(old.isAgregado());
            ocupante.setEliminado(old.isEliminado());
            ocupante.setFotografias(old.getFotografias());
            ocupante.setVolante(old.getVolante());
            ocupante.setId(old.getId());
            ocupante.setLocalId(old.getLocalId());
            ocupante.setInvolucradoId(old.getInvolucradoId());
            ocupante.setModificado(!ocupante.isAgregado());
            assertTerceroAutoOcupante(ocupante);
            terceroAuto.getOcupantes().remove(old);
            terceroAuto.getOcupantes().add(ocupante);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteOcupante(String ajuste, long idTercero, long idOcupante) {
        return Completable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            Ocupante old = findByIdOrLocalId(terceroAuto.getOcupantes(), idOcupante);
            assertDeleteOcupante(old);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            if (old.isAgregado()) {
                terceroAuto.getOcupantes().remove(old);
            } else {
                old.setModificado(false);
                old.setEliminado(true);
            }
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<Fotografia>> getFotografiasOcupante(String ajuste, long idTercero, long idOcupante) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            Ocupante ocupante = findByIdOrLocalId(terceroAuto.getOcupantes(), idOcupante);
            e.onSuccess(filter(ocupante.getFotografias()));
        });
    }

    @Override
    public Single<Fotografia> addFotografiaOcupante(String ajuste, long idTercero, long idOcupante, File image) {
        return Single.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Ocupante ocupante = findByIdOrLocalId(terceroAuto.getOcupantes(), idOcupante);
            Fotografia fotografia = createFotografia(image);
            ocupante.getFotografias().add(fotografia);
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                terceroAuto = findByAjusteAndId(ajuste, idTercero);
                ocupante = findByIdOrLocalId(terceroAuto.getOcupantes(), ocupante);
                fotografia = findByIdOrLocalId(ocupante.getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografiaOcupante(String ajuste, long idTercero, long idOcupante, long idFotografia) {
        return Completable.create(e -> {
            TerceroAuto terceroAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Ocupante ocupante = findByIdOrLocalId(terceroAuto.getOcupantes(), idOcupante);
            Fotografia deleted = null;
            for (Fotografia fotografia : ocupante.getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            if (deleted.isAgregado()) {
                ocupante.getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);

            }
            repository.saveByAjuste(ajuste, terceroAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }
    private void assertTerceroAutoConductor(Persona conductor) {
        if (conductor.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (conductor.getCondicion().equals(CondicionPersona.LESIONADO) && conductor.getLesiones().isEmpty()) {
            throw new LogicException("lesiones_required", ResourceUtils.getString("lesiones_required"));
        }

    }

    private void assertTerceroAutoVehiculo(VehiculoTercero vehiculo) {
        if (vehiculo.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (vehiculo.getDanoConsecuencia() && vehiculo.getPrimerImpacto() == null) {
            throw new LogicException("property_required", ResourceUtils.getString("property_required"));
        }
        if (vehiculo.getDanoConsecuencia() && vehiculo.getDanos().isEmpty()) {
            throw new LogicException("daños_required", ResourceUtils.getString("danos_required"));
        }

    }

    private void assertTerceroAutoOcupante(Ocupante ocupante) {
        if (ocupante.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (ocupante.getCondicion().equals(CondicionPersona.LESIONADO) && ocupante.getLesiones().isEmpty()) {
            throw new LogicException("lesiones_ocupante_required", ResourceUtils.getString("lesiones_ocupante_required"));
        }
        if (ocupante.getCorreos().size() > 2) {
            throw new LogicException("max_correos", ResourceUtils.getString("max_correos"));

        }
        if (ocupante.getTelefonos().size() > 2) {
            throw new LogicException("max_telefonos", ResourceUtils.getString("max_telefonos"));
        }

    }

    private void assertDeleteTercerosAutos(TerceroAuto terceroAuto, Responsabilidad responsabilidad) {

        if (terceroAuto.getConductor() != null && terceroAuto.getConductor().getVolante() != null) {
            throw new LogicException("tercero_volante", ResourceUtils.getString("tercero_volante"));
        }
        if (terceroAuto.getVehiculo() != null && terceroAuto.getVehiculo().getVolante() != null) {
            throw new LogicException("tercero_volante", ResourceUtils.getString("tercero_volante"));
        }
        if (!terceroAuto.getOcupantes().isEmpty()) {
            for (Ocupante ocupante : terceroAuto.getOcupantes()) {
                if (ocupante != null && ocupante.getVolante() != null) {
                    throw new LogicException("tercero_volante", ResourceUtils.getString("tercero_volante"));
                }
            }
        }
    }

    private void assertDeleteOcupante(Ocupante ocupante) {
        if (ocupante != null && ocupante.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            cleanErrors(ajuste);
            Log.i(TAG, "Sincronizar datos de los terceros autos del ajuste: " + ajuste);
            if (!isSynchronized(ajuste)) {
                try {
                    log(ajuste, "synchronize_tercerosAutos");
                    List<TerceroAuto> terceros = repository.findByAjuste(ajuste);
                    for (TerceroAuto tercero : terceros) {
                        if (!isSynchronize(tercero)) {
                            log(ajuste, "synchronize_terceroAuto", tercero.getDescripcion());
                            synchronizeDataTercero(ajuste, tercero);
                            if (tercero.getConductor() != null && isComplete(tercero.getConductor()))
                                synchronizeDataConductor(ajuste, tercero);
                            if (tercero.getVehiculo() != null && isComplete(tercero.getVehiculo()))
                                synchronizeDataVehiculo(ajuste, tercero);
                            if (!tercero.getOcupantes().isEmpty())
                                synchronizeDataOcupantes(ajuste, tercero);
                            Log.i(TAG, "Datos del tercero auto " + tercero.getDescripcion() + " del ajuste: " + ajuste + " sincronizados");
                        }
                    }
                    e.onComplete();
                    Log.i(TAG, "Datos de los terceros autos del ajuste: " + ajuste + " sincronizados");
                } catch (Throwable ex) {
                    Log.e(TAG, "Error al sincronizar datos de los terceros autos del ajuste: " + ajuste, ex);
                    e.tryOnError(ex);
                }
            } else {
                Log.i(TAG, "Los datos de los terceros autos del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private Boolean isComplete(Object object){
        try{
            ErrorUtils.assertValues(object);
            return true;
        }
        catch (DataException ex){
            return false;
        }
    }

    private synchronized void synchronizeDataTercero(String ajuste, TerceroAuto tercero) throws NetworkException {
        try {
            log(ajuste, "synchronize_terceroAuto", tercero.getDescripcion());

            syncEliminable(tercero,
                    t -> data.delete(ajuste, t.getId()),
                    () -> repository.deleteByAjusteAndId(ajuste, tercero.getId()));

            syncAgregable(tercero, t -> data.add(ajuste, new TerceroAutoFolio(t.getFolioAmis())), t -> {
                repository.deleteByAjusteAndId(ajuste, tercero.getId());
                tercero.setId(t.getId());
                repository.saveByAjuste(ajuste, tercero);

                List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(ajuste, tercero.getLocalId());
                for (Recuperacion recuperacion : recuperaciones) {
                    recuperacionRepository.deleteByAjusteAndTerceroAndId(ajuste, tercero.getLocalId(), recuperacion.getId());
                    recuperacionRepository.saveByAjusteAndTercero(ajuste, tercero.getId(), recuperacion);
                }
                Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
                if (responsabilidad != null ) {
                    responsabilidadRepository.save(responsabilidad);
                }
            });

        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new TerceroAutoException(tercero.getDescripcion(), ex);
            }
        }
    }


    private synchronized void synchronizeDataConductor(String ajuste, TerceroAuto tercero) throws NetworkException {
        try {
            log(ajuste, "synchronize_terceroAuto_conductor", tercero.getDescripcion());
            Action modify = () -> repository.saveByAjuste(ajuste, tercero);
            syncModificable(tercero.getConductor(), conductor ->
                    data.modifyConductor(ajuste, tercero.getId(), conductor), modify);

            syncRemoveFotografias(tercero.getConductor(),
                    id -> data.deleteFotografiaConductor(ajuste, tercero.getId(), id), modify);

            int size = tercero.getConductor().getFotografias().size();
            syncAddFotografias(tercero.getConductor(), (req, index) -> {
                log(ajuste, "synchronize_terceroAuto_conductor_fotografias", tercero.getDescripcion(), index + 1, size);
                return data.addFotografiaConductor(ajuste, tercero.getId(), req);
            }, modify);


        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new TerceroAutoConductorException(tercero.getDescripcion(), ex);
            }
        }
    }


    private synchronized void synchronizeDataVehiculo(String ajuste, TerceroAuto tercero) throws NetworkException {
        try {

            log(ajuste, "synchronize_terceroAuto_vehiculo", tercero.getDescripcion());
            Action modify = () -> repository.saveByAjuste(ajuste, tercero);

            syncModificable(tercero.getVehiculo(),
                    vehiculo -> data.modifyVehiculo(ajuste, tercero.getId(), vehiculo), modify);

            syncRemoveFotografias(tercero.getVehiculo(),
                    id -> data.deleteFotografiaVehiculo(ajuste, tercero.getId(), id), modify);

            int size = tercero.getVehiculo().getFotografias().size();
            syncAddFotografias(tercero.getVehiculo(), (req, index) -> {
                log(ajuste, "synchronize_terceroAuto_vehiculo_fotografias", tercero.getDescripcion(), index + 1, size);
                return data.addFotografiaVehiculo(ajuste, tercero.getId(), req);
            }, modify);


        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new TerceroAutoVehiculoException(tercero.getDescripcion(), ex);
            }
        }
    }


    private synchronized void synchronizeDataOcupantes(String ajuste, TerceroAuto tercero) throws NetworkException {
        ValueObject<Ocupante> errorOcupante = new ValueObject<>();
        Action modify = () -> repository.saveByAjuste(ajuste, tercero);
        try {
            log(ajuste, "synchronize_terceroAuto_ocupantes");
            log(ajuste, "");
            syncEliminable(tercero.getOcupantes(),
                    ocupante -> data.deleteOcupante(ajuste, tercero.getId(), ocupante.getId())
                            .doOnError(throwable -> errorOcupante.setValue(ocupante)),
                    ocupante -> {
                        ocupanteRepository.delete(ocupante);
                        modify.run();
                    });
        } catch (Throwable ex) {
            String nombre = errorOcupante.getValue() == null ? "Desconocido" : errorOcupante.getValue().getNombreCompleto();
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new TerceroAutoOcupanteException(tercero.getDescripcion(), nombre, ex);
            }

        }

        for (Ocupante ocupante : tercero.getOcupantes()) {
            if (isComplete(ocupante)) {
                try {
                    log(ajuste, "synchronize_terceroAuto_ocupante", tercero.getDescripcion(), ocupante.getNombreCompleto());
                    syncAgregable(ocupante, o -> data.addOcupante(ajuste, tercero.getId(), o),
                            result -> {
                                ocupanteRepository.deleteById(ocupante.getId());
                                ocupante.setId(result.getId());
                                modify.run();
                            });
                    syncModificable(ocupante, o ->
                            data.modifyOcupante(ajuste, tercero.getId(), ocupante.getId(), o), modify);

                    syncRemoveFotografias(ocupante, id ->
                            data.deleteFotografiaOcupante(ajuste, tercero.getId(), ocupante.getId(), id), modify);

                    int size = ocupante.getFotografias().size();
                    syncAddFotografias(ocupante, (req, index) -> {
                        log(ajuste, "synchronize_terceroAuto_ocupante_fotografias", tercero.getDescripcion(), ocupante.getNombreCompleto(), index + 1, size);
                        return data.addFotografiaOcupante(ajuste, tercero.getId(), ocupante.getId(), req);
                    }, modify);


                } catch (Throwable ex) {
                    if (ex instanceof NetworkException) {
                        throw (NetworkException) ex;
                    } else if (ex instanceof UnknownException) {
                        throw (UnknownException) ex;
                    } else {
                        throw new TerceroAutoOcupanteException(tercero.getDescripcion(), ocupante.getNombreCompleto(), ex);
                    }
                }
            }
        }
    }


    @Override
    public boolean isSynchronized(String ajuste) {
        boolean sincronizado = true;
        List<TerceroAuto> terceros = repository.findByAjuste(ajuste);
        for (TerceroAuto tercero : terceros) {
            sincronizado = isSynchronize(tercero);
        }
        return sincronizado;
    }

    private boolean isSynchronize(TerceroAuto tercero) {
        boolean sincronizado = true;
        sincronizado &= isSynchronized(tercero);
        sincronizado &= isSynchronized(tercero.getConductor());
        sincronizado &= isSynchronized(tercero.getVehiculo());
        for (Ocupante ocupante : tercero.getOcupantes()) {
            sincronizado &= isSynchronized(ocupante);
        }
        return sincronizado;
    }

    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            try {
                Log.i(TAG, "Iniciando datos de los terceros autos del ajuste: " + ajuste);
                List<TerceroAuto> terceros = RXUtils.sync(data.getAll(ajuste));
                for (TerceroAuto tercero : terceros) {
                    repository.deleteByAjusteAndId(ajuste, tercero.getId());
                    putLocalId(tercero);
                    if (tercero.getConductor() != null) {
                        tercero.getConductor().setId(generateLocalId());
                        putFotografiasLocalId(tercero.getConductor());
                    }
                    if (tercero.getVehiculo() != null) {
                        VersionVehiculo versionVehiculo = RXUtils.sync(
                                tprService.getVersion(tercero.getVehiculo().getTipoVehiculo(),
                                    tercero.getVehiculo().getArmadora(),
                                    tercero.getVehiculo().getCarroceria(),
                                    tercero.getVehiculo().getModelo().intValue(),
                                    tercero.getVehiculo().getVersion()));
                        tercero.setDescripcion(versionVehiculo.getNombre());
                        putFotografiasLocalId(tercero.getVehiculo());
                    }
                    for (Ocupante ocupante : tercero.getOcupantes()) {
                        putLocalId(ocupante);
                    }
                    repository.saveByAjuste(ajuste, tercero);
                }
                Log.i(TAG, "Datos de los terceros autos del ajuste: " + ajuste + " obtenidos");
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar los datos de los terceros autos del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            List<TerceroAuto> terceros = repository.findByAjuste(ajuste);
            for (TerceroAuto tercero : terceros) {
                cleanData(tercero);
            }
            repository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Observable<AfectadoResumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            AfectadoResumen resumen = new AfectadoResumen();
            List<TerceroAuto> terceroAutos = filter(repository.findByAjuste(ajuste));
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            if (terceroAutos.isEmpty()) {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            } else {
                boolean conductor = true;
                boolean vehiculo = true;
                boolean datosConductor = true;
                boolean datosVehiculo = true;
                boolean datosOcupantes = true;
                boolean fotografiasConductor = true;
                boolean fotografiasVehiculo = true;
                boolean fotografiasOcupantes = true;
                boolean volante = true;
                for (TerceroAuto terceroAuto : terceroAutos) {
                    conductor &= terceroAuto.getConductor() != null;
                    if (terceroAuto.getConductor() != null) {
                        try {
                            assertValues(terceroAuto.getConductor());
                        }catch (DataException ex){
                            resumen.getErrores().add(ResourceUtils.getString("conductor_incompleto"));
                            datosConductor = false;
                        }
                        fotografiasConductor &= !filter(terceroAuto.getConductor().getFotografias()).isEmpty();
                        volante &= terceroAuto.getConductor().getVolante() != null;

                    }else {
                        fotografiasConductor = false;
                        volante = false;
                    }
                    vehiculo &= terceroAuto.getVehiculo() != null;
                    if (terceroAuto.getVehiculo() != null) {
                        try {
                            ErrorUtils.assertValues(terceroAuto.getVehiculo());
                        }catch (DataException ex){
                            resumen.getErrores().add(ResourceUtils.getString("vehiculo_incompleto"));
                            datosVehiculo = false;
                        }
                        fotografiasVehiculo &= !filter(terceroAuto.getVehiculo().getFotografias()).isEmpty();
                        volante &= terceroAuto.getVehiculo().getVolante() != null;

                    }else {
                        fotografiasVehiculo = false;
                        volante = false;
                    }
                    for (Ocupante ocupante : filter(terceroAuto.getOcupantes())) {
                        try {
                            assertValues(ocupante);
                        }catch (DataException ex){
                            resumen.getErrores().add(ResourceUtils.getString("ocupante_incompleto"));
                            datosOcupantes = false;
                        }
                        fotografiasOcupantes &= !filter(ocupante.getFotografias()).isEmpty();
                        volante &= ocupante.getVolante() != null;
                    }
                }
                resumen.setFotografias(fotografiasConductor && fotografiasOcupantes && fotografiasVehiculo);
                resumen.setVolante(volante);
                if (!conductor) {
                    resumen.getErrores().add(ResourceUtils.getString("conductor_empty"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!vehiculo) {
                    resumen.getErrores().add(ResourceUtils.getString("vehiculo_empty"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!fotografiasConductor) {
                    resumen.getErrores().add(ResourceUtils.getString("conductor_fotos_error"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!fotografiasVehiculo) {
                    resumen.getErrores().add(ResourceUtils.getString("vehiculo_fotos_error"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!fotografiasOcupantes) {
                    resumen.getErrores().add(ResourceUtils.getString("ocupante_fotos_error"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!datosConductor){
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!datosVehiculo){
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!datosOcupantes){
                    resumen.setEstatus(EstatusResumen.ERROR);

                }
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof TerceroAutoException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof TerceroAutoException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }

    private void cleanData(TerceroAuto tercero) {
        if (tercero.getConductor() != null) {
            for (Fotografia fotografia : tercero.getConductor().getFotografias()) {
                fotografiaRepository.delete(fotografia);
            }
            personaRepository.delete(tercero.getConductor());
        }
        if (tercero.getVehiculo() != null) {
            for (Fotografia fotografia : tercero.getVehiculo().getFotografias()) {
                fotografiaRepository.delete(fotografia);
            }
        }
        for (Ocupante ocupante : tercero.getOcupantes()) {
            for (Fotografia fotografia : ocupante.getFotografias()) {
                fotografiaRepository.delete(fotografia);
            }
            ocupanteRepository.delete(ocupante);
        }
    }

}
