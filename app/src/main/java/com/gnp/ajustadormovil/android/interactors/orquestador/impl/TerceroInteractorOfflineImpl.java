package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroRepository;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class TerceroInteractorOfflineImpl implements TerceroInteractor {

    private final TerceroRepository repository = RepositoryFactory.getTerceroRepository();


    @Override
    public Single<List<Tercero>> getAll(String ajuste){
        return Single.create(e -> {
            e.onSuccess(filter(repository.findByAjuste(ajuste)));
        });
    }

    @Override
    public Single<Tercero> get(String ajuste, long id){
        return Single.create(e -> {
            e.onSuccess(repository.findByAjusteAndId(ajuste, id));
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            repository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    private List<Tercero> filter(List<Tercero> source){
        List<Tercero> result = new ArrayList<>();
        if(source != null) {
            for (Tercero item : source) {
                if(!item.isEliminado()) {
                    result.add(item);
                }
            }
        }
        return result;
    }

}
