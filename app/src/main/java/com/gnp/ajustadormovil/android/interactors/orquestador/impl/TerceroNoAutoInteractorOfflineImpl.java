package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroNoAutoData;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroNoAutoRepository;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAutoException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;

import java.io.File;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

public class TerceroNoAutoInteractorOfflineImpl extends SyncInteractorOfflineImpl implements TerceroNoAutoInteractor {

    private final TerceroNoAutoData data = OrquestadorFactory.getTerceroNoAutoData();

    private final TerceroNoAutoRepository repository = RepositoryFactory.getTerceroNoAutoRepository();

    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();

    private final RecuperacionRepository recuperacionRepository = RepositoryFactory.getRecuperacionRepository();

    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();


    private TerceroNoAuto findByAjusteAndId(String ajuste, Long id){
        return  findByIdOrLocalId(repository.findByAjuste(ajuste), id);
    }

    @Override
    public Single<List<TerceroNoAuto>> getAll(String ajuste) {
        return Single.create(e -> {
            List<TerceroNoAuto> noAutos = repository.findByAjuste(ajuste);
            e.onSuccess(this.filter(noAutos));
        });
    }

    @Override
    public Single<TerceroNoAuto> add(String ajuste, TerceroNoAuto tercero) {
        return Single.create(e -> {

            assertValues(tercero);
            assertTerceroNoAuto(tercero);
            TerceroNoAuto noAuto = tercero;
            noAuto.setId(generateLocalId());
            noAuto.setLocalId(noAuto.getId());
            noAuto.setAgregado(true);
            noAuto.setEliminado(false);
            noAuto.setModificado(false);
            CatalogoId categoria = RXUtils.sync(siniestrosInteractor.getCategoriaTerceroNoAuto(tercero.getCategoriaNoAuto()));
            CatalogoId tipoTercero = RXUtils.sync(siniestrosInteractor.getTipoTerceroNoAuto(categoria.getId(), tercero.getTipoNoAuto()));
            tercero.setDescripcion(categoria.getNombre() + ", " + tipoTercero.getNombre());
            repository.saveByAjuste(ajuste, noAuto);
            try {
                synchronize(ajuste);
                noAuto = findByIdOrLocalId(repository.findByAjuste(ajuste), noAuto);
                e.onSuccess(noAuto);
            } catch (Exception ex){
                repository.deleteByAjusteAndId(ajuste, noAuto.getId());
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<TerceroNoAuto> get(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroNoAuto tercero = findByAjusteAndId(ajuste, idTercero);
            if (tercero == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(tercero);
            }
        });
    }

    @Override
    public Completable modify(String ajuste, long idTercero, TerceroNoAuto tercero) {
        return Completable.create(e -> {
            assertValues(tercero);
            TerceroNoAuto old = findByAjusteAndId(ajuste, idTercero);
            TerceroNoAuto rollback = findByAjusteAndId(ajuste, idTercero);
            tercero.setAgregado(old.isAgregado());
            tercero.setEliminado(old.isEliminado());
            tercero.setFotografias(old.getFotografias());
            tercero.setVolante(old.getVolante());
            tercero.setId(old.getId());
            tercero.setLocalId(old.getLocalId());
            tercero.setModificado(!tercero.isAgregado());
            assertTerceroNoAuto(tercero);
            CatalogoId categoria = RXUtils.sync(siniestrosInteractor.getCategoriaTerceroNoAuto(tercero.getCategoriaNoAuto()));
            CatalogoId tipoTercero = RXUtils.sync(siniestrosInteractor.getTipoTerceroNoAuto(categoria.getId(), tercero.getTipoNoAuto()));
            tercero.setDescripcion(categoria.getNombre() + tipoTercero.getNombre());
            repository.deleteByAjusteAndId(ajuste, idTercero);
            repository.saveByAjuste(ajuste, tercero);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    private void assertValues(TerceroNoAuto tercero) {
        ErrorUtils.assertValues(tercero);
        if (tercero.getAfectado() != null) {
            ErrorUtils.assertValues(tercero.getAfectado());
            for (Telefono telefono : tercero.getAfectado().getTelefonos()) {
                ErrorUtils.assertValues(telefono);
            }
        }
    }

    @Override
    public Completable delete(String ajuste, long idTercero) {
        return Completable.create(e -> {
            TerceroNoAuto terceroNoAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroNoAuto rollback = findByAjusteAndId(ajuste, idTercero);
            if (terceroNoAuto.isAgregado()) {
                repository.deleteByAjusteAndId(ajuste, idTercero);
            } else {
                terceroNoAuto.setEliminado(true);
                repository.saveByAjuste(ajuste, terceroNoAuto);
            }
            List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(ajuste, idTercero);
            recuperacionRepository.deleteByAjusteAndTercero(ajuste, idTercero);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                for (Recuperacion recuperacion : recuperaciones) {
                    recuperacionRepository.saveByAjusteAndTercero(ajuste, idTercero, recuperacion);
                }
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<Fotografia>> getFotografias(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroNoAuto tercero = findByAjusteAndId(ajuste, idTercero);
            e.onSuccess(filter(tercero.getFotografias()));
        });
    }

    @Override
    public Single<Fotografia> addFotografia(String ajuste, long idTercero, File image) {
        return Single.create(e -> {
            TerceroNoAuto tercero = findByAjusteAndId(ajuste, idTercero);
            TerceroNoAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia fotografia = createFotografia(image);
            tercero.getFotografias().add(fotografia);
            repository.saveByAjuste(ajuste, tercero);
            try {
                synchronize(ajuste);
                tercero = findByAjusteAndId(ajuste, idTercero);
                fotografia = findByIdOrLocalId(tercero.getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografia(String ajuste, long idTercero, long idFotografia) {
        return Completable.create(e -> {
            TerceroNoAuto noAuto = findByAjusteAndId(ajuste, idTercero);
            TerceroNoAuto rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia deleted = null;
            for (Fotografia fotografia : noAuto.getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            if (deleted.isAgregado()) {
                noAuto.getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);
            }
            repository.saveByAjuste(ajuste, noAuto);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }


    private void assertTerceroNoAuto(TerceroNoAuto terceroNoAuto) {
        if (terceroNoAuto != null && terceroNoAuto.getReclamo() && terceroNoAuto.getAfectado() == null) {
            throw new LogicException("afectado_required", ResourceUtils.getString("afectado_required"));
        }
        if (terceroNoAuto != null && terceroNoAuto.getVolante() != null) {
            throw new LogicException("tercero_no_auto_no_editable", ResourceUtils.getString("tercero_no_auto_no_editable"));
        }
    }



    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Sincronizar datos de los terceros no autos del ajuste: " + ajuste);
            cleanErrors(ajuste);
            if (!isSynchronized(ajuste)) {
                Log.i(TAG, "Sincronizando datos de los terceros no autos del ajuste: " + ajuste);
                LoadingIndicator.message("Sincronizando datos de los terceros no autos");
                try {
                    List<TerceroNoAuto> terceros = repository.findByAjuste(ajuste);
                    for (TerceroNoAuto tercero : terceros) {
                        if (!isSynchronized(tercero)) {
                            Log.i(TAG, "Sincronizando datos del tercero no autos " + tercero.getDescripcion() + " del ajuste: " + ajuste);
                            synchronizeDataTercero(ajuste, tercero);
                        }
                    }
                    Log.i(TAG, "Datos de los terceros no autos del ajuste: " + ajuste + " sincronizados");
                    e.onComplete();
                } catch (Exception ex) {
                    Log.e(TAG, "Error al sincronizar datos de los terceros no autos del ajuste: " + ajuste, ex);
                    e.tryOnError(ex);
                }
            } else {
                Log.i(TAG, "Los datos de los terceros no autos del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private synchronized void synchronizeDataTercero(String ajuste, TerceroNoAuto tercero) throws NetworkException {
        Action save = () -> repository.saveByAjuste(ajuste, tercero);
        try {
            log(ajuste, "synchronize_terceroNoAuto", tercero.getDescripcion());
            syncEliminable(tercero, t ->
                    data.delete(ajuste, t.getId()), () -> repository.deleteByAjusteAndId(ajuste, tercero.getId()));

            syncAgregable(tercero, t -> data.add(ajuste, t), t -> {
                repository.deleteByAjusteAndId(ajuste, tercero.getId());
                tercero.setId(t.getId());
                repository.saveByAjuste(ajuste, tercero);
                List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(ajuste, tercero.getLocalId());
                for (Recuperacion recuperacion : recuperaciones) {
                    recuperacionRepository.deleteByAjusteAndTerceroAndId(ajuste, tercero.getLocalId(), recuperacion.getId());
                    recuperacionRepository.saveByAjusteAndTercero(ajuste, tercero.getId(), recuperacion);
                }
                Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
                if (responsabilidad != null) {
                    responsabilidadRepository.save(responsabilidad);
                }
            });
            syncModificable(tercero, t ->
                    data.modify(ajuste, t.getId(), t), save);


            syncRemoveFotografias(tercero, id ->
                    data.deleteFotografia(ajuste, tercero.getId(), id), save);

            int size = tercero.getFotografias().size();
            syncAddFotografias(tercero, (req, index) -> {
                log(ajuste, "synchronize_terceroNoAuto_fotografias", tercero.getDescripcion(), index + 1, size);
                return data.addFotografia(ajuste, tercero.getId(), req);
            }, save);

            Log.i(TAG, "Datos sincronizados del tercero no autos " + tercero.getDescripcion() + " del ajuste: " + ajuste);
        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new TerceroNoAutoException(tercero.getNombreCompleto(), ex);
            }
        }
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        boolean sincronizado = true;
        List<TerceroNoAuto> terceros = repository.findByAjuste(ajuste);
        for (TerceroNoAuto tercero : terceros) {
            sincronizado &= isSynchronized(tercero);
        }
        return sincronizado;
    }

    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            try {
                Log.i(TAG, "Iniciando datos de los terceros no autos del ajuste: " + ajuste);
                List<TerceroNoAuto> terceros = RXUtils.sync(data.getAll(ajuste));
                for (TerceroNoAuto tercero : terceros) {
                    repository.deleteByAjusteAndId(ajuste, tercero.getId());
                    putLocalId(tercero);
                    CatalogoId categoria = RXUtils.sync(siniestrosInteractor.getCategoriaTerceroNoAuto(tercero.getCategoriaNoAuto()));
                    CatalogoId tipoTercero = RXUtils.sync(siniestrosInteractor.getTipoTerceroNoAuto(categoria.getId(), tercero.getTipoNoAuto()));
                    tercero.setDescripcion(categoria.getNombre() + ", " + tipoTercero.getNombre());
                    putFotografiasLocalId(tercero);
                    repository.saveByAjuste(ajuste, tercero);
                }
                Log.i(TAG, "Datos de los terceros no autos del ajuste: " + ajuste + " obtenidos");
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar los datos de los terceros no autos del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            List<TerceroNoAuto> terceros = repository.findByAjuste(ajuste);
            for (TerceroNoAuto tercero : terceros) {
                cleanData(tercero);
            }
            repository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Observable<AfectadoResumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            AfectadoResumen resumen = new AfectadoResumen();
            List<TerceroNoAuto> terceros = filter(repository.findByAjuste(ajuste));
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            if (terceros.isEmpty()) {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            } else {
                boolean fotografias = true;
                boolean volantes = true;
                boolean datosAfectado = true;
                for (TerceroNoAuto tercero : terceros) {
                    try {
                        assertValues(tercero);
                    }catch (DataException ex){
                        resumen.getErrores().add(ResourceUtils.getString("no_auto_incompleto"));
                        datosAfectado = false;

                    }
                    fotografias &= !filter(tercero.getFotografias()).isEmpty();
                    volantes &= tercero.getVolante() != null;
                }
                resumen.setVolante(volantes);
                resumen.setFotografias(fotografias);
                if (!fotografias) {
                    resumen.getErrores().add(ResourceUtils.getString("no_auto_fotografias_error"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!datosAfectado){
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof TerceroNoAutoException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof TerceroNoAutoException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }

    private void cleanData(TerceroNoAuto tercero) {
        for (Fotografia fotografia : tercero.getFotografias()) {
            fotografiaRepository.delete(fotografia);
        }
    }

}
