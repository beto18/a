package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.Repository;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.FotografiaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TercerosPersonaData;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAutoException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersonaException;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;

import java.io.File;
import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

public class TerceroPersonaInteractorOfflineImpl extends SyncInteractorOfflineImpl implements TerceroPersonaInteractor {

    private final TercerosPersonaData data = OrquestadorFactory.getTerceroPersonaData();

    private final TerceroPersonaRepository repository = RepositoryFactory.getTerceroPersonaRepository();

    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();

    private final RecuperacionRepository recuperacionRepository = RepositoryFactory.getRecuperacionRepository();

    private final FotografiaRepository fotografiaRepository = RepositoryFactory.getFotodrafiaRepository();

    private final Repository<Persona, Long> personaRepository = RepositoryFactory.getRepository(Persona.class, Long.class);

    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();

    private TerceroPersona findByAjusteAndId(String ajuste, Long id){
        return findByIdOrLocalId(repository.findByAjuste(ajuste), id);
    }

    @Override
    public Single<List<TerceroPersona>> getAll(String ajuste) {
        return Single.create(e -> {
            List<TerceroPersona> personas = repository.findByAjuste(ajuste);
            e.onSuccess(this.filter(personas));
        });
    }

    @Override
    public Single<TerceroPersona> add(String ajuste, Persona tercero) {
        return Single.create(e -> {
            assertValues(tercero);
            assertPersona(tercero);
            tercero.setId(generateLocalId());
            TerceroPersona terceroPersona = new TerceroPersona();
            terceroPersona.setPersona(tercero);
            terceroPersona.getPersona().setInvolucradoId(UUID.randomUUID().toString());
            terceroPersona.setId(generateLocalId());
            terceroPersona.setLocalId(terceroPersona.getId());
            terceroPersona.setAgregado(true);
            terceroPersona.setEliminado(false);
            terceroPersona.setModificado(false);
            terceroPersona.setDescripcion(tercero.getNombreCompleto());
            repository.saveByAjuste(ajuste, terceroPersona);
            try {
                synchronize(ajuste);
                terceroPersona = findByIdOrLocalId(repository.findByAjuste(ajuste), terceroPersona);
                e.onSuccess(terceroPersona);
            } catch (Exception ex) {
                repository.deleteByAjusteAndId(ajuste, terceroPersona.getId());
                e.tryOnError(ex);
            }
        });
    }

    private void assertValues(Afectado afectado) {
        ErrorUtils.assertValues(afectado);
        for (Telefono telefono : afectado.getTelefonos()) {
            ErrorUtils.assertValues(telefono);
        }
    }

    @Override
    public Single<TerceroPersona> get(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroPersona persona = findByAjusteAndId(ajuste, idTercero);
            if (persona == null) {
                e.tryOnError(new ResourceNotFoundException());
            } else {
                e.onSuccess(persona);
            }
        });
    }

    @Override
    public Completable delete(String ajuste, long idTercero) {
        return Completable.create(e -> {
            TerceroPersona persona = findByAjusteAndId(ajuste, idTercero);
            TerceroPersona rollback = findByAjusteAndId(ajuste, idTercero);
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
            assertDeleteTercerosPersonas(persona, responsabilidad);
            if (persona.isAgregado()) {
                repository.deleteByAjusteAndId(ajuste, idTercero);
            } else {
                persona.setEliminado(true);
                repository.saveByAjuste(ajuste, persona);
            }
            List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(ajuste, idTercero);
            recuperacionRepository.deleteByAjusteAndTercero(ajuste, idTercero);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                for (Recuperacion recuperacion : recuperaciones) {
                    recuperacionRepository.saveByAjusteAndTercero(ajuste, idTercero, recuperacion);
                }
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable modify(String ajuste, long idTercero, Persona tercero) {
        return Completable.create(e -> {
            assertValues(tercero);
            TerceroPersona persona = new TerceroPersona();
            persona.setPersona(tercero);
            TerceroPersona old = findByAjusteAndId(ajuste, idTercero);
            TerceroPersona rollback = findByAjusteAndId(ajuste, idTercero);
            persona.setAgregado(old.isAgregado());
            persona.setEliminado(old.isEliminado());
            persona.getPersona().setFotografias(old.getPersona().getFotografias());
            persona.getPersona().setVolante(old.getPersona().getVolante());
            persona.setId(old.getId());
            persona.setLocalId(old.getLocalId());
            persona.getPersona().setId(old.getPersona().getId());
            persona.getPersona().setInvolucradoId(old.getPersona().getInvolucradoId());
            persona.setDescripcion(persona.getPersona().getNombreCompleto());
            persona.setModificado(!persona.isAgregado());
            assertPersona(tercero);
            repository.deleteByAjusteAndId(ajuste, idTercero);
            repository.saveByAjuste(ajuste, persona);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Single<List<Fotografia>> getFotografias(String ajuste, long idTercero) {
        return Single.create(e -> {
            TerceroPersona tercero = findByAjusteAndId(ajuste, idTercero);
            e.onSuccess(filter(tercero.getPersona().getFotografias()));
        });
    }

    @Override
    public Single<Fotografia> addFotografia(String ajuste, long idTercero, File image) {
        return Single.create(e -> {
            TerceroPersona tercero = findByAjusteAndId(ajuste, idTercero);
            TerceroPersona rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia fotografia = createFotografia(image);
            tercero.getPersona().getFotografias().add(fotografia);
            repository.saveByAjuste(ajuste, tercero);
            try {
                synchronize(ajuste);
                tercero = repository.findByAjusteAndId(ajuste, idTercero);
                fotografia = findByIdOrLocalId(tercero.getPersona().getFotografias(), fotografia);
                e.onSuccess(fotografia);
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable deleteFotografia(String ajuste, long idTercero, long idFotografia) {
        return Completable.create(e -> {
            TerceroPersona persona = findByAjusteAndId(ajuste, idTercero);
            TerceroPersona rollback = findByAjusteAndId(ajuste, idTercero);
            Fotografia deleted = null;
            for (Fotografia fotografia : persona.getPersona().getFotografias()) {
                if (fotografia.getId().equals(idFotografia)) {
                    deleted = fotografia;
                }
            }
            assert deleted != null;
            if (deleted.isAgregado()) {
                persona.getPersona().getFotografias().remove(deleted);
            } else {
                deleted.setEliminado(true);
            }
            repository.saveByAjuste(ajuste, persona);
            try {
                synchronize(ajuste);
                e.onComplete();
            } catch (Exception ex) {
                repository.saveByAjuste(ajuste, rollback);
                e.tryOnError(ex);
            }
        });
    }

    private void assertPersona(Persona persona) {
        if (persona.getVolante() != null) {
            throw new LogicException("volante_impreso", ResourceUtils.getString("volante_impreso"));
        }
        if (persona.getCondicion().equals(CondicionPersona.LESIONADO) && persona.getLesiones().isEmpty()) {
            throw new LogicException("lesiones_required", ResourceUtils.getString("peaton_lesion_required"));
        }
    }

    private void assertDeleteTercerosPersonas(TerceroPersona terceroPersona, Responsabilidad responsabilidad) {
        if (terceroPersona.getPersona() != null && terceroPersona.getPersona().getVolante() != null) {
            throw new LogicException("tercero_volante", ResourceUtils.getString("volante_impreso"));
        }
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Sincronizar datos de los terceros personas del ajuste: " + ajuste);
            cleanErrors(ajuste);
            if (!isSynchronized(ajuste)) {
                try {
                    log(ajuste, "synchronize_tercerosPersonas");
                    List<TerceroPersona> terceros = repository.findByAjuste(ajuste);
                    for (TerceroPersona tercero : terceros) {
                        if (!isSynchronized(tercero)) {
                            Log.i(TAG, "Sincronizando datos del tercero persona " + tercero.getDescripcion() + " del ajuste: " + ajuste);
                            synchronizeDataTercero(ajuste, tercero);
                            Log.i(TAG, "Datos sincronizados del tercero persona " + tercero.getDescripcion() + " del ajuste: " + ajuste);
                        }
                    }
                    Log.i(TAG, "Datos de los terceros personas del ajuste: " + ajuste + " sincronizados");
                    e.onComplete();
                } catch (Throwable ex) {
                    Log.i(TAG, "Error al sincronizar datos de los terceros personas del ajuste: " + ajuste, ex);
                    e.tryOnError(ex);
                }
            } else {
                Log.i(TAG, "Los datos de los terceros personas del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private synchronized void synchronizeDataTercero(String ajuste, TerceroPersona tercero) throws NetworkException {
        Action save = () -> repository.saveByAjuste(ajuste, tercero);
        try {
            log(ajuste, "synchronize_terceroPersona", tercero.getPersona().getNombreCompleto());
            syncAgregable(tercero, t -> data.add(ajuste, t.getPersona()), t -> {
                repository.deleteByAjusteAndId(ajuste, tercero.getId());
                tercero.setId(t.getId());
                repository.saveByAjuste(ajuste, tercero);
                List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(ajuste, tercero.getLocalId());
                for (Recuperacion recuperacion : recuperaciones) {
                    recuperacionRepository.deleteByAjusteAndTerceroAndId(ajuste, tercero.getLocalId(), recuperacion.getId());
                    recuperacionRepository.saveByAjusteAndTercero(ajuste, tercero.getId(), recuperacion);
                }
                Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(ajuste);
                if (responsabilidad != null) {
                    responsabilidadRepository.save(responsabilidad);
                }
            });
            syncModificable(tercero, t ->
                    data.modify(ajuste, t.getId(), t.getPersona()), save);

            syncEliminable(tercero, t ->
                    data.delete(ajuste, t.getId()), () -> repository.deleteByAjusteAndId(ajuste, tercero.getId()));

            syncRemoveFotografias(tercero.getPersona(), id ->
                    data.deleteFotografia(ajuste, tercero.getId(), id), save);

            int size = tercero.getPersona().getFotografias().size();
            syncAddFotografias(tercero.getPersona(), (req, index) -> {
                log(ajuste, "synchronize_terceroPersona_fotografias", tercero.getPersona().getNombreCompleto(), index + 1, size);
                return data.addFotografia(ajuste, tercero.getId(), req);
            }, save);

        } catch (Throwable ex) {
            if (ex instanceof NetworkException) {
                throw (NetworkException) ex;
            } else if (ex instanceof UnknownException) {
                throw (UnknownException) ex;
            } else {
                throw new TerceroPersonaException(tercero.getPersona().getNombreCompleto(), ex);
            }
        }
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        boolean sincronizado = true;
        List<TerceroPersona> terceros = repository.findByAjuste(ajuste);
        for (TerceroPersona tercero : terceros) {
            sincronizado = isSynchronized(tercero);
        }
        return sincronizado;
    }

    private boolean isSynchronized(TerceroPersona tercero) {
        boolean sincronizado = true;
        sincronizado &= isSynchronized((Object) tercero);
        sincronizado &= isSynchronized(tercero.getPersona());
        return sincronizado;
    }


    @Override
    public Completable initialize(String ajuste) {
        return syncCompletable(ajuste, e -> {
            try {
                Log.i(TAG, "Iniciando datos de los terceros personas del ajuste: " + ajuste);
                List<TerceroPersona> terceros = RXUtils.sync(data.getAll(ajuste));
                for (TerceroPersona tercero : terceros) {
                    repository.deleteByAjusteAndId(ajuste, tercero.getId());
                    tercero.getPersona().setId(tercero.getId());
                    putLocalId(tercero);
                    tercero.setDescripcion(tercero.getPersona().getNombreCompleto());
                    putFotografiasLocalId(tercero.getPersona());
                    repository.saveByAjuste(ajuste, tercero);
                }
                Log.i(TAG, "Datos de los terceros personas del ajuste: " + ajuste + " obtenidos");
                e.onComplete();
            } catch (Exception ex) {
                Log.e(TAG, "Error al inicializar los datos de los terceros personas del ajuste: " + ajuste, ex);
                e.tryOnError(ex);
            }
        });
    }

    @Override
    public Completable terminate(String ajuste) {
        return Completable.create(e -> {
            Log.i(TAG, "Limpiando datos del ajuste: " + ajuste);
            List<TerceroPersona> terceros = repository.findByAjuste(ajuste);
            for (TerceroPersona tercero : terceros) {
                cleanData(tercero);
            }
            repository.deleteByAjuste(ajuste);
            e.onComplete();
        });
    }

    @Override
    public Observable<AfectadoResumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            AfectadoResumen resumen = new AfectadoResumen();
            List<TerceroPersona> terceros = filter(repository.findByAjuste(ajuste));
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            if (terceros.isEmpty()) {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            } else {
                boolean fotografias = true;
                boolean volantes = true;
                boolean datosPersona = true;
                for (TerceroPersona tercero : terceros) {
                    try {
                        assertValues(tercero.getPersona());
                    }catch (DataException ex){
                        resumen.getErrores().add(ResourceUtils.getString("peaton_incompleto"));
                        datosPersona = false;
                    }
                    fotografias &= !filter(tercero.getPersona().getFotografias()).isEmpty();
                    volantes &= tercero.getPersona().getVolante() != null;
                }
                resumen.setVolante(volantes);
                resumen.setFotografias(fotografias);
                if (!fotografias) {
                    resumen.getErrores().add(ResourceUtils.getString("peaton_fotos_error"));
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
                if (!datosPersona){
                    resumen.setEstatus(EstatusResumen.ERROR);
                }
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof TerceroPersonaException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof TerceroNoAutoException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }

    private void cleanData(TerceroPersona tercero) {
        if (tercero.getPersona() != null) {
            for (Fotografia fotografia : tercero.getPersona().getFotografias()) {
                fotografiaRepository.delete(fotografia);
            }
            personaRepository.delete(tercero.getPersona());
        }
    }

}
