package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TipoDocumentoProvider {

    private static final Map<CategoriaVolante, String> CATEGORIAS;

    private static final String CVE_SEGURO_PP = "09";

    private static final String CVE_SEGURO_DIRECTA = "08";

    private static final String CVE_SEGURO_TRADICIONAL = "07";

    private static final String CVE_SEGURO_SIPAC = "06";

    private static final String CVE_SIN_SEGURO_PP = "05";

    private static final String CVE_SIN_SEGURO_DIRECTO = "04";

    private static final String CVE_VOLANTE_TRADICIONAL = "T";

    private static final String CVE_VOLANTE_SIPAC = "SIPAC";

    private static final String CVE_VOLANTE_PRONTO_PAGO = "PP";

    private static final String CVE_VOLANTE_DIRECTA = "D";

    private static final String CVE_TERCERO_ASEGURADO = "TAO";

    private static final String CVE_TERCERO_ASEGURADO_GNP = "TAG";

    private static final String CVE_TERCERO_SIN_SEGURO = "TSS";

    static {
        CATEGORIAS = new HashMap<>();
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_SIN_SEGURO, CVE_VOLANTE_DIRECTA), CVE_SIN_SEGURO_DIRECTO);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_SIN_SEGURO, CVE_VOLANTE_PRONTO_PAGO), CVE_SIN_SEGURO_PP);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO_GNP, CVE_VOLANTE_SIPAC), CVE_SEGURO_SIPAC);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO_GNP, CVE_VOLANTE_TRADICIONAL), CVE_SEGURO_TRADICIONAL);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO_GNP, CVE_VOLANTE_DIRECTA), CVE_SEGURO_DIRECTA);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO_GNP, CVE_VOLANTE_PRONTO_PAGO), CVE_SEGURO_PP);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO, CVE_VOLANTE_SIPAC), CVE_SEGURO_SIPAC);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO, CVE_VOLANTE_TRADICIONAL), CVE_SEGURO_TRADICIONAL);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO, CVE_VOLANTE_DIRECTA), CVE_SEGURO_DIRECTA);
        CATEGORIAS.put(CategoriaVolante.of(CVE_TERCERO_ASEGURADO, CVE_VOLANTE_PRONTO_PAGO), CVE_SEGURO_PP);
    }

    private static class CategoriaVolante {

        private String seguro;

        private String tipo;

        private CategoriaVolante(String seguro, String tipo){
            this.seguro = seguro;
            this.tipo = tipo;
        }

        public static CategoriaVolante of(String seguro, String tipo){
            return new CategoriaVolante(seguro, tipo);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof CategoriaVolante)) return false;
            CategoriaVolante that = (CategoriaVolante) o;
            return Objects.equals(seguro, that.seguro) &&
                    Objects.equals(tipo, that.tipo);
        }

        @Override
        public int hashCode() {
            return Objects.hash(seguro, tipo);
        }
    }


    public static String getTipoDocumento(VolanteVehiculoTercero volante){
        String tipoVolante = volante.getTipoVolante();
        if (CVE_VOLANTE_DIRECTA.equals(tipoVolante) && CVE_VOLANTE_PRONTO_PAGO.equals(volante.getSubTipoVolante())) {
            tipoVolante = volante.getSubTipoVolante();
        }
        CategoriaVolante categoria = CategoriaVolante.of(volante.getTipoTercero(), tipoVolante);
        String categoriaVolante = CATEGORIAS.get(categoria);
        if(categoriaVolante == null) {
            throw new LogicException("volante_no_imprimible", "Combinación de tipo volante y tipo tercero no encontrada");
        }
        return categoriaVolante;
    }

}
