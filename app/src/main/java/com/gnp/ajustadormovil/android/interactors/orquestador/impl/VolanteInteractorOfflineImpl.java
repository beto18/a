package com.gnp.ajustadormovil.android.interactors.orquestador.impl;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjustadorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.AmisData;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoData;
import com.gnp.ajustadormovil.android.data.orquestador.AseguradoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ErrorRepository;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.data.orquestador.RecuperacionRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ResponsabilidadRepository;
import com.gnp.ajustadormovil.android.data.orquestador.ServicioCoberturaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoData;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroNoAutoData;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroNoAutoRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TerceroPersonaRepository;
import com.gnp.ajustadormovil.android.data.orquestador.TercerosPersonaData;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.documents.VolanteMedicoBuilder;
import com.gnp.ajustadormovil.android.interactors.orquestador.documents.VolanteNoAutoBuilder;
import com.gnp.ajustadormovil.android.interactors.orquestador.documents.VolanteVehiculoAseguradoBuilder;
import com.gnp.ajustadormovil.android.interactors.orquestador.documents.VolanteVehiculoTerceroBuilder;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.EmisionValeDigital;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VolanteVehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Archivo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.ArchivoEnviado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VehiculoAfectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteException;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volanteable;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.servicio.ServiciosCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrUtils;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class VolanteInteractorOfflineImpl extends SyncInteractorOfflineImpl implements VolanteInteractor {

    private static final String TAG = VolanteInteractorOfflineImpl.class.getSimpleName();

    private static final String TIPO_ASEGURADO_CONDUCTOR = "12";

    private static final String TIPO_ASEGURADO_OCUPANTE = "13";

    private static final String TIPO_TERCERO_CONDUCTOR = "15";

    private static final String TIPO_TERCERO_OCUPANTE = "16";

    private static final String TIPO_TERCERO_PEATON = "11";

    private static final String TIPO_TERCERO_NO_AUTO = "10";

    public static final String TIPO_ASEGURADO_VEHICULO = "03";

    public static final String TIPO_TERCERO_OTRA_ASEGURADORA = "TAO";

    public static final String TIPO_VOLANTE_TRADICIONAL = "T";

    public static final String TIPO_ASEGURADO_CON_GNP = "TAG";

    private static final String CVE_VOLANTE_SIPAC = "SIPAC";

    public static final String TIPO_SIN_SEGURO = "TSS";


    public static final List<String> TIPOS_ASEGURADO = Arrays.asList(TIPO_ASEGURADO_CONDUCTOR, TIPO_ASEGURADO_OCUPANTE, TIPO_ASEGURADO_VEHICULO);

    private final AseguradoData aseguradoData = OrquestadorFactory.getAseguradoData();
    private final TerceroAutoData terceroAutoData = OrquestadorFactory.getTerceroAutoData();
    private final TerceroNoAutoData terceroNoAutoData = OrquestadorFactory.getTerceroNoAutoData();
    private final TercerosPersonaData tercerosPersonaData = OrquestadorFactory.getTerceroPersonaData();
    private final AmisData amisData = OrquestadorFactory.getAmisData();

    private final AseguradoRepository aseguradoRepository = RepositoryFactory.getAseguradoRepository();
    private final TerceroAutoRepository terceroAutoRepository = RepositoryFactory.getTerceroAutoRepository();
    private final TerceroNoAutoRepository terceroNoAutoRepository = RepositoryFactory.getTerceroNoAutoRepository();
    private final TerceroPersonaRepository terceroPersonaRepository = RepositoryFactory.getTerceroPersonaRepository();
    private final ResponsabilidadRepository responsabilidadRepository = RepositoryFactory.getResponsabilidadRepository();
    private final AjustadorRepository ajustadorRepository = RepositoryFactory.getAjustadorRepository();
    private final AjusteRepository ajusteRepository = RepositoryFactory.getAjusteRepository();
    private final RecuperacionRepository recuperacionRepository = RepositoryFactory.getRecuperacionRepository();
    private final SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
    private final TprInteractor tprService = CatalogoInteractorsFactory.getTprService();
    private final CatInteractor catInteractor = CatalogoInteractorsFactory.getCatInteractor();
    private final ServicioCoberturaRepository servicioCoberturaRepository = RepositoryFactory.getServicioCoberturaRepository();
    private final ErrorRepository errorRepository = RepositoryFactory.getErrorRepository();
    private final AmisRepository amisRepository = RepositoryFactory.getAmisRepository();

    private TerceroAuto findByAjusteAndTAId(String ajuste, long id) {
        return findByIdOrLocalId(terceroAutoRepository.findByAjuste(ajuste), id);
    }

    private TerceroNoAuto findByAjusteAndTNAId(String ajuste, long id) {
        return findByIdOrLocalId(terceroNoAutoRepository.findByAjuste(ajuste), id);
    }

    private TerceroPersona findByAjusteAndTPId(String ajuste, long id) {
        return findByIdOrLocalId(terceroPersonaRepository.findByAjuste(ajuste), id);
    }

    @Override
    public Single<Archivo> generateAseguradoConductor(String idAjuste, VolanteMedico volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            Asegurado asegurado = aseguradoRepository.findByAjuste(idAjuste);
            Asegurado rollback = aseguradoRepository.findByAjuste(idAjuste);
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            assertVolanteMedicoAsegurado(ajusteRepository.findById(idAjuste), volante, asegurado, responsabilidad);
            int lesionados = 1;
            for (Ocupante ocp : asegurado.getOcupantes()) {
                if (ocp.getCondicion().equals(CondicionPersona.LESIONADO)) {
                    lesionados++;
                }
            }
            asegurado.getConductor().setVolante(volante);
            VersionVehiculo vehiculo = getVehiculo(asegurado.getVehiculo());
            e.setDisposable(
                    generateVolanteMedico(idAjuste,
                            asegurado.getConductor(),
                            asegurado.getConductor(),
                            vehiculo,
                            TIPO_ASEGURADO_CONDUCTOR,
                            lesionados,
                            "ADI")
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .subscribe(() -> {
                                aseguradoRepository.save(asegurado);
                                try {
                                    synchronize(idAjuste);
                                    Asegurado stored = aseguradoRepository.findByAjuste(idAjuste);
                                    e.onSuccess(new Archivo(stored.getConductor().getVolante().getUrl()));

                                } catch (Exception ex) {
                                    aseguradoRepository.save(rollback);
                                    e.tryOnError(ex);

                                }

                            }, e::tryOnError));
        });
    }

    @Override
    public Single<Archivo> generateAseguradoVehiculo(String idAjuste, VolanteVehiculoAsegurado volante) {
        return Single.create(ex -> {
            ErrorUtils.assertValues(volante);
            String fechaFinLicencia = "";
            String circunstancia = null;
            String codigoQr = null;
            Ajuste ajuste = ajusteRepository.findById(idAjuste);
            Asegurado asegurado = aseguradoRepository.findByAjuste(idAjuste);
            Asegurado rollback = aseguradoRepository.findByAjuste(idAjuste);
            try {
                ErrorUtils.assertValues(asegurado.getConductor());
            } catch (DataException e) {
                throw new LogicException("conductor_not_found", "No hay datos del conductor");
            }
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            assertVolanteAdmisionAseguradoVehiculo(ajuste, volante, asegurado, responsabilidad);
            File documento = new File(Utils.TEMP_FOLDER + File.separator + idAjuste + "_asegurado_vehiculo.pdf");
            CatalogoId razonNoDeducible = new CatalogoId();
            if (!volante.getAplicaDeducible()) {
                razonNoDeducible = RXUtils.sync(siniestrosInteractor.getRazonNoDeducible(volante.getRazonNoDeducible()));
            }
            VersionVehiculo versionVehiculo = RXUtils.sync(tprService.getVersion(asegurado.getVehiculo().getTipoVehiculo(),
                    asegurado.getVehiculo().getArmadora(),
                    asegurado.getVehiculo().getCarroceria(),
                    asegurado.getVehiculo().getModelo().intValue(),
                    asegurado.getVehiculo().getVersion()));
            if (responsabilidad.getCircunstanciaA() != null) {
                circunstancia = RXUtils.sync(siniestrosInteractor.getCircustanciaVehiculo(responsabilidad.getCircunstanciaA())).getNombre();
            }
            CatalogoId causaCondicionamiento = new CatalogoId();
            if (volante.getVolanteCondicionado()) {
                causaCondicionamiento = RXUtils.sync(siniestrosInteractor.getCondicionamientosPoliza(volante.getCausaCondicionamiento()));
            }
            if (asegurado.getConductor().getTipoIdentificacion().equals("LC") && !asegurado.getConductor().getLicenciaPermanente()) {
                fechaFinLicencia = DateUtils.date(asegurado.getConductor().getFechaFinLicencia());
            }
            if (volante.getCodigoTaller() != null && !volante.getCodigoTaller().isEmpty()) {
                codigoQr = RXUtils.sync(QrUtils.generateBase64(volante.getCodigoTaller(), 200, 200));

            }
            String tipoIdentificacion = RXUtils.sync(siniestrosInteractor.getTipoIdentificacion(asegurado.getConductor().getTipoIdentificacion())).getNombre();
            Ajustador ajustador = ajustadorRepository.findOne();
            ex.setDisposable(new VolanteVehiculoAseguradoBuilder(ajuste.getSiniestro().getId(), ajustador.getCedula())
                    .fechaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .horaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .fechaArribo(ajuste.getFechaArribo())
                    .fechaEntrega(new Date())
                    .estado((String) getValue(ajuste, "siniestro.ubicacion.estado.nombre"))
                    .municipio((String) getValue(ajuste, "siniestro.ubicacion.municipio.nombre"))
                    .calle((String) getValue(ajuste, "siniestro.ubicacion.colonia.nombre"))
                    .entreCalles((String) getValue(ajuste, "siniestro.ubicacion.entreCalles"))
                    .circunstancia(circunstancia)
                    .numeroPoliza((String) getValue(ajuste, "siniestro.poliza.numero"))
                    .estatusPoliza((String) getValue(ajuste, "siniestro.poliza.estatus.nombre"))
                    .aseguradoNombre(asegurado.getConductor().getNombreCompleto())
                    .tipoCobertura((String) getValue(ajuste, "siniestro.poliza.nombrePaquete"))
                    .estatusCobranza((String) getValue(ajuste, "siniestro.poliza.estatusCobranza.nombre"))
                    .vigenciaPoliza((Date) getValue(ajuste, "siniestro.poliza.fechaVigenciaFinal"))
                    .conductorNombre(asegurado.getConductor().getNombreCompleto())
                    .fechaNacimiento(asegurado.getConductor().getFechaNacimiento())
                    .edad(calculateAge(asegurado.getConductor().getFechaNacimiento()).toString())
                    .tipoIdentificacion(tipoIdentificacion)
                    .tipoLicencia(asegurado.getConductor().getTipoIdentificacion().equals("LC") ? asegurado.getConductor().getTipoLicencia() : "", "Tipo Licencia:")
                    .fechaFinLicencia(fechaFinLicencia, "Válida hasta:")
                    .telefono(asegurado.getConductor().getTelefonos().get(0).getTelefono())
                    .correo(asegurado.getConductor().getCorreos().get(0))
                    .tipoVehiculo(versionVehiculo.getCarroceria().getArmadora().getTipoVehiculo().getNombre())
                    .armadora(versionVehiculo.getCarroceria().getArmadora().getNombre())
                    .carroceria(versionVehiculo.getCarroceria().getNombre())
                    .version(versionVehiculo.getVersion())
                    .modelo(versionVehiculo.getModelo().toString())
                    .placas(asegurado.getVehiculo().getPlacas())
                    .vin(asegurado.getVehiculo().getVin())
                    .causaCondicionamiento(causaCondicionamiento.getNombre())
                    .lstDanosPreexistentes(getPartes(asegurado.getVehiculo().getDanosPreexistentes()))
                    .responsable("A".equals(responsabilidad.getResponsabilidadAjustador()))
                    .aplicaDeducible(volante.getAplicaDeducible() ? "SI" : "NO")
                    .razonNoDeducible(razonNoDeducible.getNombre())
                    .perdidaTotalEvidente(asegurado.getVehiculo().getPerdidaTotalEvidente())
                    .lstDanosConsecuencia(getPartes(asegurado.getVehiculo().getDanos()))
                    .vehiculoSegmentado(volante.getFolioSegmentacion() != null)
                    .folioSegmentacion(volante.getFolioSegmentacion())
                    .volanteCondicionado(volante.getVolanteCondicionado() ? "SI" : "NO")
                    .proteccionLegal(existServicio(idAjuste, "CO"))
                    .tomadorDatos(existServicio(idAjuste, "AB"))
                    .lstPiezasFaltantes(getPartes(volante.getPiezasFaltantes()))
                    .qrBase64(codigoQr)
                    .cliente(ajuste.getSiniestro().getPoliza() != null ? ajuste.getSiniestro().getPoliza().getCliente() : null)
                    .build(documento).subscribe(folio -> {
                        volante.setFolio(folio);
                        volante.setUrl(documento.getAbsolutePath());
                        volante.setOnline(true);
                        asegurado.getVehiculo().setVolante(volante);
                        aseguradoRepository.save(asegurado);
                        try {
                            synchronize(idAjuste);
                            Asegurado stored = aseguradoRepository.findByAjuste(idAjuste);
                            if(!isSynchronized(asegurado.getVehiculo())) {
                                stored.getVehiculo().getVolante().setOnline(false);
                                aseguradoRepository.save(stored);
                            }
                            ex.onSuccess(new Archivo(stored.getVehiculo().getVolante().getUrl()));
                        } catch (Exception e) {
                            aseguradoRepository.save(rollback);
                            ex.tryOnError(e);

                        }

                    }, ex::onError));
        });
    }

    private boolean existServicio(String ajuste, String clave) throws Exception {
        ServiciosCobertura servicios = servicioCoberturaRepository.findByAjuste(ajuste);
        return servicios != null && servicios.getServicios().contains(clave);
    }

    @Override
    public Single<Archivo> generateAseguradoOcupante(String idAjuste, Long idOcupante, VolanteMedico volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            Asegurado asegurado = aseguradoRepository.findByAjuste(idAjuste);
            Asegurado rollback = aseguradoRepository.findByAjuste(idAjuste);

            Ocupante ocupante = getOcupante(asegurado.getOcupantes(), idOcupante);
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            assertVolanteMedicoAseguradoOcupante(ajusteRepository.findById(idAjuste), volante, ocupante, responsabilidad);

            int lesionados = 0;
            if (asegurado.getConductor().getCondicion().equals(CondicionPersona.LESIONADO)) {
                lesionados++;
            }
            for (Ocupante ocp : asegurado.getOcupantes()) {
                if (ocp.getCondicion().equals(CondicionPersona.LESIONADO)) {
                    lesionados++;
                }
            }
            ocupante.setVolante(volante);
            VersionVehiculo vehiculo = getVehiculo(asegurado.getVehiculo());
            e.setDisposable(
                    generateVolanteMedico(idAjuste,
                            ocupante,
                            asegurado.getConductor(),
                            vehiculo,
                            TIPO_ASEGURADO_OCUPANTE,
                            lesionados,
                            ocupante.getPosicion().name())
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .subscribe(() -> {
                                aseguradoRepository.save(asegurado);
                                try {
                                    synchronize(idAjuste);
                                    Ocupante stored = findByIdOrLocalId(aseguradoRepository.findByAjuste(idAjuste).getOcupantes(), idOcupante);
                                    e.onSuccess(new Archivo(stored.getVolante().getUrl()));

                                } catch (Exception ex) {
                                    aseguradoRepository.save(rollback);
                                    e.tryOnError(ex);
                                }


                            }, e::tryOnError));
        });
    }


    @Override
    public Single<Archivo> generateTerceroAutoConductor(String idAjuste, long idTercero, VolanteMedico volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            TerceroAuto tercero = findByAjusteAndTAId(idAjuste, idTercero);
            TerceroAuto rollback = findByAjusteAndTAId(idAjuste, idTercero);

            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            assertVolanteMedicoTerceroAuto(ajusteRepository.findById(idAjuste), volante, tercero, responsabilidad);
            int lesionados = 1;
            for (Ocupante ocp : tercero.getOcupantes()) {
                if (ocp.getCondicion().equals(CondicionPersona.LESIONADO)) {
                    lesionados++;
                }
            }
            tercero.getConductor().setVolante(volante);
            VersionVehiculo vehiculo = getVehiculo(tercero.getVehiculo());
            e.setDisposable(
                    generateVolanteMedico(idAjuste,
                            tercero.getConductor(),
                            tercero.getConductor(),
                            vehiculo,
                            TIPO_TERCERO_CONDUCTOR,
                            lesionados,
                            "ADI")
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .subscribe(() -> {
                                terceroAutoRepository.saveByAjuste(idAjuste, tercero);

                                try {
                                    synchronize(idAjuste);
                                    TerceroAuto stored = terceroAutoRepository.findByAjusteAndId(idAjuste, idTercero);
                                    e.onSuccess(new Archivo(stored.getConductor().getVolante().getUrl()));

                                } catch (Exception ex) {
                                    terceroAutoRepository.saveByAjuste(idAjuste, rollback);
                                    e.tryOnError(ex);
                                }

                            }, e::tryOnError));
        });
    }

    @Override
    public Single<ArchivoEnviado> generateTerceroAutoVehiculo(String idAjuste, long idTercero, VolanteVehiculoTercero volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            TerceroAuto tercero = findByAjusteAndTAId(idAjuste, idTercero);
            TerceroAuto rollback = findByAjusteAndTAId(idAjuste, idTercero);
            FolioContraparteAmis contraparte;
            try {
                ErrorUtils.assertValues(tercero.getVehiculo());
            } catch (DataException ex) {
                throw new LogicException("tercero_auto_sin_vehículo", "tercero_auto_sin_vehículo");
            }

            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            Ajuste ajuste = ajusteRepository.findById(idAjuste);
            assertVolanteAdmisionTerceroAutoVehiculo(ajuste, volante, tercero, responsabilidad);

            List<String> piezasFaltantes = null;
            CatalogoClave otraAseguradora = new CatalogoClave();
            CatalogoId causaCondicionamiento = new CatalogoId();
            String numeroPoliza = null;
            Date fechaFinPoliza = null;
            String numeroSiniestro = null;
            String estado = (String) getValue(ajuste, "siniestro.ubicacion.estado.nombre");
            String municipio = (String) getValue(ajuste, "siniestro.ubicacion.municipio.nombre");
            String calle = (String) getValue(ajuste, "siniestro.ubicacion.colonia.nombre");
            String entreCalles = (String) getValue(ajuste, "siniestro.ubicacion.entreCalles");

            VersionVehiculo vehiculo = getVehiculo(tercero.getVehiculo());
            Ajustador ajustador = ajustadorRepository.findOne();
            if (volante.getVolanteCondicionado()) {
                causaCondicionamiento = RXUtils.sync(siniestrosInteractor.getCondicionamientosPoliza(volante.getCausaCondicionamiento()));
            }
            if (TIPO_TERCERO_OTRA_ASEGURADORA.equals(volante.getTipoTercero())) {
                otraAseguradora = RXUtils.sync(catInteractor.getOtraAseguradora(volante.getOtraAseguradora()));
                numeroPoliza = (String) getValue(volante, "numeroPoliza");
                fechaFinPoliza = (Date) getValue(volante, "fechaFinPoliza");
                numeroSiniestro = (String) getValue(volante, "numeroSiniestro");

            }
            if (TIPO_SIN_SEGURO.equals(volante.getTipoTercero())) {
                estado = RXUtils.sync(catInteractor.getEstado(volante.getEstado())).getNombre();
                municipio = RXUtils.sync(catInteractor.getMunicipio(volante.getEstado(), volante.getMunicipio())).getNombre();
                calle = volante.getCalle();
                entreCalles = volante.getEntreCalle();
            }

            if (volante.getRequiereGrua()) {
                piezasFaltantes = getPartes(volante.getPiezasFaltantes());
            }
            tercero.getVehiculo().setVolante(volante);
            String circunstancia = null;
            if (responsabilidad.getCircunstanciaB() != null) {
                circunstancia = RXUtils.sync(siniestrosInteractor.getCircustanciaVehiculo(responsabilidad.getCircunstanciaB())).getNombre();
            }

            if (TIPO_TERCERO_OTRA_ASEGURADORA.equals(volante.getTipoTercero()) && CVE_VOLANTE_SIPAC.equals(volante.getTipoVolante()) && tercero.getFolioAmis() != null) {
                contraparte = getContraparte(idAjuste, tercero);
                if (contraparte != null) {
                    ErrorUtils.assertValues(contraparte);
                }
            }


            String tipoIdentificacion = RXUtils.sync(siniestrosInteractor.getTipoIdentificacion(tercero.getConductor().getTipoIdentificacion())).getNombre();
            File documento = new File(Utils.TEMP_FOLDER + File.separator + "tercero_auto_vehiculo_" + System.currentTimeMillis());
            e.setDisposable(new VolanteVehiculoTerceroBuilder(ajuste.getSiniestro().getId(), ajustador.getCedula(), TipoDocumentoProvider.getTipoDocumento(volante))
                    .fechaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .horaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .fechaArribo((Date) getValue(ajuste, "fechaArribo"))
                    .fechaEntrega(new Date())
                    .estado(estado)
                    .municipio(municipio)
                    .calle(calle)
                    .entreCalles(entreCalles)
                    .numeroPoliza(numeroPoliza)
                    .fechaFinPoliza(fechaFinPoliza)
                    .circunstancia(circunstancia)
                    .otraAseguradora(otraAseguradora.getNombre())
                    .numeroSiniestro(numeroSiniestro)
                    .conductorNombre(tercero.getConductor().getNombreCompleto())
                    .telefono(tercero.getConductor().getTelefonos().get(0).getTelefono())
                    .edad(calculateAge(tercero.getConductor().getFechaNacimiento()))
                    .tipoIdentificacion(tipoIdentificacion)
                    .tipoVehiculo(vehiculo.getCarroceria().getArmadora().getTipoVehiculo().getNombre())
                    .armadora(vehiculo.getCarroceria().getArmadora().getNombre())
                    .carroceria(vehiculo.getCarroceria().getNombre())
                    .version(vehiculo.getNombre())
                    .modelo(vehiculo.getModelo())
                    .placas(tercero.getVehiculo().getPlacas())
                    .vin(tercero.getVehiculo().getVin())
                    .causaCondicionamiento(causaCondicionamiento.getNombre())
                    .lstDanosPreexistentes(getPartes(tercero.getVehiculo().getDanosPreexistentes()))
                    .responsabilidad(responsabilidad.getTerceroResponsable())
                    .proteccionLegal(existServicio(idAjuste, "CO"))
                    .perdidaTotalEvidente(tercero.getVehiculo().getPerdidaTotalEvidente())
                    .lstDanosConsecuencia(getPartes(tercero.getVehiculo().getDanos()))
                    .folioSegmentacion(volante.getFolioSegmentacion())
                    .volanteCondicionado(volante.getVolanteCondicionado())
                    .tomadorDatos(existServicio(idAjuste, "AB"))
                    .lstRecuperaciones(getRecuperaciones(idAjuste, tercero))
                    .lstPiezasFaltantes(piezasFaltantes)
                    .build(documento)
                    .subscribe(folio -> {
                        volante.setFolio(folio);
                        if (TIPO_TERCERO_OTRA_ASEGURADORA.equals(volante.getTipoTercero()) && CVE_VOLANTE_SIPAC.equals(volante.getTipoVolante())) {
                            volante.setUrl(Constants.FIRMA_DIGITAL);
                        } else {
                            volante.setUrl(documento.getAbsolutePath());
                        }
                        tercero.getVehiculo().setVolante(volante);
                        terceroAutoRepository.saveByAjuste(idAjuste, tercero);
                        try {
                            synchronize(idAjuste);
                            TerceroAuto stored = terceroAutoRepository.findByAjusteAndId(idAjuste, idTercero);
                            ArchivoEnviado archivo = new ArchivoEnviado(stored.getVehiculo().getVolante().getUrl());
                            if(volante.getValeDigital() != null && volante.getFirmaDigital() != null) {
                                try {
                                    RXUtils.sync(amisData.emitirOrden(ajuste.getId(), new EmisionValeDigital(volante.getValeDigital(), volante.getFirmaDigital())));
                                    archivo.setEnviado(true);
                                } catch (Exception ex) {
                                    Log.w(TAG,"No se pudo enviar el volante a AMIS", ex);
                                    archivo.setEnviado(false);
                                }
                            }
                            e.onSuccess(archivo);
                        } catch (Exception ex) {
                            terceroAutoRepository.saveByAjuste(idAjuste, rollback);
                            e.tryOnError(ex);
                        }

                    }, e::tryOnError));
        });
    }


    @Override
    public Single<Archivo> generateTerceroAutoOcupante(String idAjuste, long idTercero, long idOcupante, VolanteMedico volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            TerceroAuto tercero = findByAjusteAndTAId(idAjuste, idTercero);
            TerceroAuto rollback = findByAjusteAndTAId(idAjuste, idTercero);
            Ocupante ocupante = getOcupante(tercero.getOcupantes(), idOcupante);
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            assertVolanteMedicoTerceroAutoOcupante(ajusteRepository.findById(idAjuste), volante, ocupante, responsabilidad);
            int lesionados = 0;
            if (tercero.getConductor().getCondicion().equals(CondicionPersona.LESIONADO)) {
                lesionados++;
            }
            for (Ocupante ocp : tercero.getOcupantes()) {
                if (ocp.getCondicion().equals(CondicionPersona.LESIONADO)) {
                    lesionados++;
                }
            }
            ocupante.setVolante(volante);
            VersionVehiculo vehiculo = getVehiculo(tercero.getVehiculo());
            e.setDisposable(
                    generateVolanteMedico(idAjuste,
                            ocupante,
                            tercero.getConductor(),
                            vehiculo,
                            TIPO_TERCERO_OCUPANTE,
                            lesionados,
                            ocupante.getPosicion().name())
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .subscribe(() -> {
                                terceroAutoRepository.saveByAjuste(idAjuste, tercero);
                                try {
                                    synchronize(idAjuste);
                                    Ocupante stored = findByIdOrLocalId(terceroAutoRepository.findByAjusteAndId(idAjuste, idTercero).getOcupantes(), idOcupante);
                                    e.onSuccess(new Archivo(stored.getVolante().getUrl()));
                                } catch (Exception ex) {
                                    terceroAutoRepository.saveByAjuste(idAjuste, rollback);
                                    e.tryOnError(ex);
                                }

                            }, e::tryOnError));
        });
    }


    @Override
    public Single<Archivo> generateTerceroNoAuto(String idAjuste, long idTercero, VolanteTerceroNoAuto volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            File documento = new File(Utils.TEMP_FOLDER + File.separator + "tercero_no_auto" + System.currentTimeMillis());
            Contratista contratista = new Contratista();
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            TerceroNoAuto noAuto = findByAjusteAndTNAId(idAjuste, idTercero);
            TerceroNoAuto rollback = findByAjusteAndTNAId(idAjuste, idTercero);

            Ajuste ajuste = ajusteRepository.findById(idAjuste);
            assertVolanteTerceroNoAuto(ajuste, volante, noAuto, responsabilidad);
            noAuto.setVolante(volante);
            Ajustador ajustador = ajustadorRepository.findOne();
            CatalogoId tipoNoAuto = RXUtils.sync(siniestrosInteractor.getCategoriaTerceroNoAuto(noAuto.getCategoriaNoAuto()));
            CatalogoId subTipoNoAuto = RXUtils.sync(siniestrosInteractor.getTipoTerceroNoAuto(noAuto.getCategoriaNoAuto(), noAuto.getTipoNoAuto()));
            if (TIPO_VOLANTE_TRADICIONAL.equals(volante.getTipoVolante())) {
                contratista = RXUtils.sync(siniestrosInteractor.getContratista(volante.getContratista()));
            }

            e.setDisposable(new VolanteNoAutoBuilder(ajuste.getSiniestro().getIdSiniestro() != null ? ajuste.getSiniestro().getIdSiniestro() : ajuste.getSiniestro().getIdPresiniestro(), ajustador.getCedula(), TIPO_TERCERO_NO_AUTO)
                    .fechaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .numeroPoliza((String) getValue(ajuste, "siniestro.poliza.numero"))
                    .estatusPoliza((String) getValue(ajuste, "siniestro.poliza.estatus.nombre"))
                    .tipoTercero(tipoNoAuto.getNombre())
                    .descripcionTercero(subTipoNoAuto.getNombre())
                    .nombreContratista(contratista.getNombre())
                    .telefono(volante.getTelefono())
                    .correoElectronico(volante.getCorreo())
                    .observaciones(volante.getObservaciones())
                    .areasDañadas(noAuto.getAreaDanada())
                    .dañoConsecuencia(noAuto.getDanoPreexistente())
                    .build(documento)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe(folio -> {
                        volante.setUrl(documento.getAbsolutePath());
                        volante.setFolio(folio);
                        noAuto.setVolante(volante);
                        terceroNoAutoRepository.saveByAjuste(idAjuste, noAuto);
                        try {
                            synchronize(idAjuste);
                            TerceroNoAuto stored = terceroNoAutoRepository.findByAjusteAndId(idAjuste, idTercero);
                            e.onSuccess(new Archivo(stored.getVolante().getUrl()));
                        } catch (Exception ex) {
                            terceroNoAutoRepository.saveByAjuste(idAjuste, rollback);
                            e.tryOnError(ex);

                        }

                    }, e::tryOnError));
        });
    }

    @Override
    public Single<Archivo> generateTerceroPersona(String idAjuste, long idTercero, VolanteMedico volante) {
        return Single.create(e -> {
            ErrorUtils.assertValues(volante);
            TerceroPersona tercero = findByAjusteAndTPId(idAjuste, idTercero);
            TerceroPersona rollback = findByAjusteAndTPId(idAjuste, idTercero);
            Asegurado asegurado = aseguradoRepository.findByAjuste(idAjuste);
            Responsabilidad responsabilidad = responsabilidadRepository.findByAjuste(idAjuste);
            assertVolanteTerceroPersona(asegurado, volante, tercero, responsabilidad);
            tercero.getPersona().setVolante(volante);
            VersionVehiculo vehiculo = getVehiculo(asegurado.getVehiculo());
            e.setDisposable(
                    generateVolanteMedico(idAjuste,
                            tercero.getPersona(),
                            asegurado.getConductor(),
                            vehiculo,
                            TIPO_TERCERO_PEATON,
                            1,
                            null)
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .subscribe(() -> {
                                terceroPersonaRepository.saveByAjuste(idAjuste, tercero);
                                try {
                                    synchronize(idAjuste);
                                    TerceroPersona stored = terceroPersonaRepository.findByAjusteAndId(idAjuste, idTercero);
                                    e.onSuccess(new Archivo(stored.getPersona().getVolante().getUrl()));
                                } catch (Exception ex) {
                                    terceroPersonaRepository.saveByAjuste(idAjuste, rollback);
                                    e.tryOnError(ex);

                                }

                            }, e::tryOnError));
        });
    }

    private Completable generateVolanteMedico(String idAjuste, Afectado persona, Afectado conductor, VersionVehiculo vehiculo, String tipo, int lesionados, String posicion) {
        return Completable.create(e -> {
            Ajuste ajuste = ajusteRepository.findById(idAjuste);
            Ajustador ajustador = ajustadorRepository.findOne();
            String lugar;
            String direccion;
            if (persona.getHospital() != null) {
                Hospital lugarTraslado = RXUtils.sync(siniestrosInteractor.getHospital(persona.getHospital()));
                lugar = lugarTraslado.getNombre();
                direccion = lugarTraslado.getDireccion();
            } else {
                lugar = persona.getOtroHospital();
                direccion = "N/A";
            }
            CatalogoClave pos = new CatalogoClave();
            pos.setNombre("N/A");
            if (posicion != null) {
                pos = RXUtils.sync(siniestrosInteractor.getPosicionOcupante(posicion));
            }
            File documento = new File(Utils.TEMP_FOLDER + File.separator + "volante_medico_" + tipo + "_" + getValue(persona, "id") + ".pdf");
            e.setDisposable(new VolanteMedicoBuilder(ajuste.getSiniestro().getId(), ajustador.getCedula(), tipo)
                    .tipoVolanteMedico(TIPOS_ASEGURADO.contains(tipo) ? "ASEGURADO" : "TERCERO")
                    .numeroPoliza((String) getValue(ajuste, "siniestro.poliza.numero"))
                    .fechaSiniestro((Date) getValue(ajuste, "siniestro.fechaReporte"))
                    .nombreConductor(conductor.getNombreCompleto())
                    .nombreLesionado(persona.getNombreCompleto())
                    .fechaNacimiento(persona.getFechaNacimiento())
                    .tipoVehiculo((String) getValue(vehiculo, "carroceria.armadora.tipoVehiculo.nombre"))
                    .vehiculo(vehiculo.getNombre())
                    .modelo(vehiculo.getModelo())
                    .posicionLesionado(pos.getNombre())
                    .causaSiniestro((String) getValue(ajuste, "siniestro.tipoSiniestro.nombre"))
                    .lstCoberturas(getCoberturas(persona.getVolante().getCoberturas()))
                    .numeroLesionados(lesionados)
                    .requiereAmbulancia(persona.getRequiereAmbulancia())
                    .lugarTraslado(lugar)
                    .direccionTraslado(direccion)
                    .lstLesiones(getLesiones(persona.getLesiones()))
                    .lstObjetosPerdidos(getObjetosPerdidos(persona.getObjetosPerdidos()))
                    .cliente(TIPOS_ASEGURADO.contains(tipo) ? (ajuste.getSiniestro().getPoliza() != null ? ajuste.getSiniestro().getPoliza().getCliente() : null) : null)
                    .build(documento)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe(folio -> {
                        persona.getVolante().setUrl(documento.getAbsolutePath());
                        persona.getVolante().setFolio(folio);
                        e.onComplete();
                    }, e::tryOnError));
        });
    }


    private FolioContraparteAmis getContraparte(String ajuste, TerceroAuto tercero) {
        Amis amis = amisRepository.findByAjuste(ajuste);
        FolioContraparteAmis contraparte = null;
        for (FolioContraparteAmis c : amis.getContrapartes()) {
            if (c.getFolioInicial().equals(tercero.getFolioAmis())) {
                contraparte = c;
                break;
            }
        }
        return contraparte;
    }

    private List<String> getLesiones(List<Long> lesiones) throws Exception {
        return RXUtils.sync(Observable.fromIterable(lesiones)
                .flatMapSingle(siniestrosInteractor::getParteLesionada)
                .map(CatalogoId::getNombre)
                .toList());
    }

    private List<String> getPartes(List<PiezaVehiculo> piezas) throws Exception {
        return RXUtils.sync(Observable.fromIterable(piezas)
                .flatMapSingle(pieza -> siniestrosInteractor.getParteAfectada(pieza.getAreaAfectada(), pieza.getParteAfectada()))
                .map(CatalogoId::getNombre)
                .toList());
    }

    private List<String> getObjetosPerdidos(List<Long> objetosPerdidos) throws Exception {
        return RXUtils.sync(Observable.fromIterable(objetosPerdidos).flatMapSingle(siniestrosInteractor::getObjetoPerdido)
                .map(CatalogoId::getNombre)
                .toList());
    }

    private List<String> getCoberturas(List<String> coberturas) throws Exception {
        return RXUtils.sync(Observable.fromIterable(coberturas)
                .flatMapSingle(tprService::getCobertura)
                .map(Cobertura::getNombre)
                .toList());
    }

    private Ocupante getOcupante(List<Ocupante> ocupantes, long idOcupante) {
        Ocupante ocupante = null;
        for (Ocupante ocp : ocupantes) {
            if (ocp.getId().equals(idOcupante)) {
                ocupante = ocp;
            }
        }
        return ocupante;
    }

    private Integer calculateAge(Date dob) {
        return Years
                .yearsBetween(LocalDate.fromDateFields(dob), LocalDate.now())
                .getYears();
    }

    private List<String> getRecuperaciones(String idAjuste, Tercero tercero) {
        List<Recuperacion> recuperaciones = recuperacionRepository.findByAjusteAndTercero(idAjuste, tercero.getId());
        List<String> result = new ArrayList<>(recuperaciones.size());
        for (Recuperacion recuperacion : recuperaciones) {
            result.add(recuperacion.getTipoRecuperacion());
        }
        return result;
    }

    private VersionVehiculo getVehiculo(VehiculoAfectado vehiculo) throws Exception {
        return RXUtils.sync(tprService.getVersion(
                vehiculo.getTipoVehiculo(),
                vehiculo.getArmadora(),
                vehiculo.getCarroceria(),
                vehiculo.getModelo().intValue(),
                vehiculo.getVersion()));
    }

    private void assertVolanteMedicoAsegurado(Ajuste ajuste, VolanteMedico medico, Asegurado asegurado, Responsabilidad responsabilidad) {
        Persona conductor = asegurado.getConductor();
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (!medico.getAplicaDeducible() && medico.getRazonNoDeducible() == null) {
            throw new LogicException("not_acceptable", "Si no aplica deducible y razón de no deducible no se informa");
        }
        if (conductor.getCondicion() != CondicionPersona.LESIONADO) {
            throw new LogicException("volante_no_imprimible", "La persona no esta lesionada");
        }
        if (conductor.getCorreos().isEmpty() && medico.getEnvioEmail()) {
            throw new LogicException("correo_no_registrado", "No tiene correo la persona y se marco envió por email");
        }
        if (asegurado.getVehiculo() == null) {
            throw new LogicException("vehiculo_not_found", "Vehículo inválido");
        }

    }

    private void assertVolanteAdmisionAseguradoVehiculo(Ajuste ajuste, VolanteVehiculoAsegurado volanteVehiculoAsegurado, Asegurado asegurado, Responsabilidad responsabilidad) {
        if (!volanteVehiculoAsegurado.getAplicaDeducible() && volanteVehiculoAsegurado.getRazonNoDeducible() == null) {
            throw new LogicException("not_acceptable", "Si no aplica deducible y razón de no deducible no se informa");
        }
        if (!asegurado.getVehiculo().getDanoConsecuencia() && !asegurado.getVehiculo().getPerdidaTotalEvidente()) {
            throw new LogicException("vehiculo_no_imprimible", "Si no es perdida total evidente y no tiene daños a consecuencia");
        }
        if (!asegurado.getVehiculo().getPerdidaTotalEvidente() && asegurado.getVehiculo().getDanos().isEmpty()) {
            throw new LogicException("vehiculo_no_imprimible", "Si no es perdida total evidente y no tiene partes dañadas reportadas");
        }
        if (volanteVehiculoAsegurado.getVolanteCondicionado() && volanteVehiculoAsegurado.getCausaCondicionamiento() == null) {
            throw new LogicException("causa_condicionamiento_required", "Si el volante esta condicionado y no se informo la causa de condicionamiento");
        }
        if (!volanteVehiculoAsegurado.getVolanteCondicionado() && volanteVehiculoAsegurado.getCausaCondicionamiento() != null) {
            throw new LogicException("causa_condicionamiento_not_required", "Si el volante esta condicionado y no se informo la causa de condicionamiento");
        }
        if (ajuste.getSiniestro().getIdPresiniestro() == null && volanteVehiculoAsegurado.getCoberturas().isEmpty()) {
            throw new LogicException("cobertura_required", "Si no es presiniestro debe tener coberturas");
        }
        if (!volanteVehiculoAsegurado.getRequiereGrua() && !volanteVehiculoAsegurado.getPiezasFaltantes().isEmpty()) {
            throw new LogicException("pieza_faltante_not_valid", "Si no requiere grúa no se permite colocar piezas faltantes");
        }
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (asegurado.getConductor() == null) {
            throw new LogicException("conductor_not_found", "No hay datos del conductor");
        }
        if (asegurado.getVehiculo() == null) {
            throw new LogicException("vehiculo_not_found", "Vehículo inválido");
        }
        if (asegurado.getVehiculo().getPerdidaTotalEvidente() && volanteVehiculoAsegurado.getCentroRemate() == null) {
            throw new LogicException("centros_remate_not_found", "Si es perdida total y el centro de remate no es válido");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "No se ha registrado la responsabilidad del siniestro");
        }
    }

    private void assertVolanteMedicoAseguradoOcupante(Ajuste ajuste, VolanteMedico medico, Ocupante ocupante, Responsabilidad responsabilidad) {
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (!ocupante.getCondicion().equals(CondicionPersona.LESIONADO)) {
            throw new LogicException("volante_no_imprimible", "La persona no esta lesionada");
        }
        if (!medico.getAplicaDeducible() && medico.getRazonNoDeducible() == null) {
            throw new LogicException("not_acceptable", "Si no aplica deducible y razón de no deducible no se informa");
        }
        if (ocupante.getCorreos().isEmpty() && medico.getEnvioEmail()) {
            throw new LogicException("correo_no_registrado", "No tiene correo la persona y se marco envió por email");
        }

    }


    private void assertVolanteMedicoTerceroAuto(Ajuste ajuste, VolanteMedico volante, TerceroAuto terceroAuto, Responsabilidad responsabilidad) {
        if (!volante.getAplicaDeducible() && volante.getRazonNoDeducible() == null) {
            throw new LogicException("not_acceptable", "Si no aplica deducible y razón de no deducible no se informa");
        }
        if (!terceroAuto.getConductor().getCondicion().equals(CondicionPersona.LESIONADO)) {
            throw new LogicException("volante_no_imprimible", "La persona no esta lesionada");
        }
        if (terceroAuto.getConductor().getCondicion().equals(CondicionPersona.LESIONADO) && terceroAuto.getConductor().getLesiones().isEmpty()) {
            throw new LogicException("volante_no_imprimible", "la persona no registró lesiones");
        }
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (terceroAuto.getConductor().getCorreos().isEmpty() && volante.getEnvioEmail()) {
            throw new LogicException("correo_no_registrado", "No tiene correo la persona y se marco envió por email");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "No se ha registrado la responsabilidad del siniestro");
        }
    }

    private void assertVolanteAdmisionTerceroAutoVehiculo(Ajuste ajuste, VolanteVehiculoTercero volanteVehiculoAsegurado, TerceroAuto terceroAuto, Responsabilidad responsabilidad) {
        if (terceroAuto == null || terceroAuto.getConductor() == null) {
            throw new LogicException("conductor_not_found", "No hay datos del conductor");
        }
        if (terceroAuto == null || terceroAuto.getVehiculo() == null) {
            throw new LogicException("vehiculo_not_found", "No hay datos del vehículo");
        }
        if (!terceroAuto.getVehiculo().getDanoConsecuencia() && !terceroAuto.getVehiculo().getPerdidaTotalEvidente()) {
            throw new LogicException("volante_not_imprimible", "No hay daños a consecuencia o pérdida total evidente");
        }
        if (!terceroAuto.getVehiculo().getPerdidaTotalEvidente() && terceroAuto.getVehiculo().getDanos().isEmpty()) {
            throw new LogicException("volante_not_imprimible", "Si no es perdida total evidente y no tiene partes dañadas reportadas");
        }
        if (volanteVehiculoAsegurado.getVolanteCondicionado() && volanteVehiculoAsegurado.getCausaCondicionamiento() == null) {
            throw new LogicException("causa_condicionamiento_required", "Si el volante esta condicionado y no se informo la causa de condicionamiento");
        }
        if (!volanteVehiculoAsegurado.getVolanteCondicionado() && volanteVehiculoAsegurado.getCausaCondicionamiento() != null) {
            throw new LogicException("causa_condicionamiento_not_required", "Si el volante no esta condicionado y se informo la causa de condicionamiento");
        }
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (terceroAuto.getVehiculo().getPerdidaTotalEvidente() && volanteVehiculoAsegurado.getCentroRemate() == null) {
            throw new LogicException("centros_remate_not_found", "Si es perdida total y el centro de remate no es válido");
        }
        if (!volanteVehiculoAsegurado.getRequiereGrua() && !volanteVehiculoAsegurado.getPiezasFaltantes().isEmpty()) {
            throw new LogicException("pieza_faltante_not_valid", "Si no requiere grúa no se permite colocar piezas faltantes");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "No se ha registrado la responsabilidad del siniestro");
        }
    }

    private void assertVolanteMedicoTerceroAutoOcupante(Ajuste ajuste, VolanteMedico volante, Ocupante ocupante, Responsabilidad responsabilidad) {
        if (!volante.getAplicaDeducible() && volante.getRazonNoDeducible() == null) {
            throw new LogicException("not_acceptable", "Si no aplica deducible y razón de no deducible no se informa");
        }
        if (!ocupante.getCondicion().equals(CondicionPersona.LESIONADO)) {
            throw new LogicException("volante_no_imprimible", "la persona no esta lesionada");
        }
        if (ocupante.getCondicion().equals(CondicionPersona.LESIONADO) && ocupante.getLesiones().isEmpty()) {
            throw new LogicException("volante_no_imprimible", "la persona no registró lesiones");
        }
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (ocupante.getCorreos().isEmpty() && volante.getEnvioEmail()) {
            throw new LogicException("correo_no_registrado", "No tiene correo la persona y se marco envió por email");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "No se ha registrado la responsabilidad del siniestro");
        }
    }

    private void assertVolanteTerceroNoAuto(Ajuste ajuste, VolanteTerceroNoAuto volante, TerceroNoAuto noAuto, Responsabilidad responsabilidad) {
        if (ajuste == null) {
            throw new LogicException("resource_not_found", "No tiene correo la persona afectada y se marco envió por email");
        }
        if (noAuto.getAfectado().getCorreos().isEmpty() && volante.getEnvioEmail()) {
            throw new LogicException("correo_no_registrado", "No tiene correo la persona afectada y se marco envió por email");
        }
        if (noAuto.getAfectado() == null) {
            throw new LogicException("no_auto_afectado_no_registrado", "No tiene al afectado registrado");
        }
        if (getValue(ajuste, "siniestro.poliza.coberturas") == null && !ajuste.getSiniestro().isPresiniestro()) {
            throw new LogicException("poliza_sin_coberturas", "Póliza sin coberturas");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "No se ha registrado la responsabilidad del siniestro");
        }
    }

    private void assertVolanteTerceroPersona(Asegurado asegurado, VolanteMedico volante, TerceroPersona persona, Responsabilidad responsabilidad) {
        if (persona.getPersona().getCondicion() != CondicionPersona.LESIONADO) {
            throw new LogicException("volante_not_imprimible", "El tercero persona no esta lesionado");
        }
        if (persona.getPersona().getCondicion() == CondicionPersona.LESIONADO && persona.getPersona().getLesiones().isEmpty()) {
            throw new LogicException("volante_not_imprimible", "El tercero persona esta lesionado y no tiene partes afectadas");
        }
        if (persona.getPersona().getCorreos().isEmpty() && volante.getEnvioEmail()) {
            throw new LogicException("correo_no_registrado", "No tiene correo la persona y se marco envió por email");
        }
        if (responsabilidad == null) {
            throw new LogicException("responsabilidad_no_registrada", "No se ha registrado la responsabilidad del siniestro");
        }
        if (asegurado.getVehiculo() == null) {
            throw new LogicException("vehiculo_not_found", "Si el VIN del vehículo del asegurado no se encuentra");
        }
        if (asegurado.getVehiculo().getVin() == null) {
            throw new LogicException("vehiculo_asegurado_not_found", "Si el VIN del vehículo del asegurado no se encuentra");
        }
    }

    /**
     * Obtiene el valor de la propiedad. Si en el arbol de llamados un objeto es null, la respuesta será null.
     *
     * @param target   Objeto para obtener su propiedad
     * @param property Nombre de la propiedad.
     * @return Valor de la expresión.
     */
    private Object getValue(@Nullable Object target, String property) {
        String root = "root";
        Expression jexlExp = new JexlEngine().createExpression(root + "." + property);
        JexlContext jc = new MapContext();
        jc.set("root", target);
        return jexlExp.evaluate(jc);
    }

    @Override
    public boolean isSynchronized(String ajuste) {
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        boolean sincronizado = true;
        if(asegurado != null) {
            sincronizado &= isSynchronized(asegurado.getConductor());
            sincronizado &= isSynchronized(asegurado.getVehiculo());
            for (Ocupante ocupante : asegurado.getOcupantes()) {
                sincronizado &= isSynchronized(ocupante);
            }
        }
        List<TerceroAuto> autos = terceroAutoRepository.findByAjuste(ajuste);
        for (TerceroAuto tercero : autos) {
            sincronizado &= isSynchronized(tercero.getConductor());
            sincronizado &= isSynchronized(tercero.getVehiculo());
            for (Ocupante ocupante : tercero.getOcupantes()) {
                sincronizado &= isSynchronized(ocupante);
            }
        }
        List<TerceroPersona> personas = terceroPersonaRepository.findByAjuste(ajuste);
        for (TerceroPersona tercero : personas) {
            sincronizado &= isSynchronized(tercero.getPersona());
        }
        List<TerceroNoAuto> noAutos = terceroNoAutoRepository.findByAjuste(ajuste);
        for (TerceroNoAuto tercero : noAutos) {
            sincronizado &= isSynchronized(tercero);
        }
        return sincronizado;
    }

    private boolean isSynchronized(Volanteable volanteable) {
        return volanteable == null || volanteable.getVolante() == null
                || (volanteable.getVolante().getUrl() != null && volanteable.getVolante().getUrl().startsWith("http") )
                || volanteable.getVolante().getUrl() == null ;
    }

    @Override
    public Completable synchronizeData(String ajuste) {
        return syncCompletable(ajuste, e -> {
            Log.i(TAG, "Sincronizar datos de los volantes del ajuste: " + ajuste);
            cleanErrors(ajuste);
            if (!isSynchronized(ajuste)) {
                try {
                    log(ajuste, "synchronize_volantes");
                    synchronizeAsegurado(ajuste);
                    synchronizeTercerosAutos(ajuste);
                    synchronizeTercerosNoAutos(ajuste);
                    synchronizeTercerosPersona(ajuste);
                    Log.i(TAG, "Datos de los volantes del ajuste: " + ajuste + " sincronizados");
                    e.onComplete();
                } catch (VolanteException ex) {
                    if (ex.getCause() instanceof NetworkException) {
                        e.tryOnError(ex.getCause());
                    } else if (ex.getCause() instanceof UnknownException) {
                        e.tryOnError(ex.getCause());
                    } else {
                        Log.e(TAG, "Error al sincronizar datos de los volantes del ajuste: " + ajuste, ex);
                        e.tryOnError(ex);
                    }
                }
            } else {
                Log.i(TAG, "Los datos de los volantes del ajuste: " + ajuste + " ya están sincronizados");
                e.onComplete();
            }
        });
    }

    private void synchronizeAsegurado(String ajuste) {
        log(ajuste, "synchronize_volantes_asegurado");
        Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
        if (!isSynchronized(asegurado.getConductor()) && isSynchronized((Object) asegurado.getConductor())) {
            try {
                log(ajuste, "synchronize_volante_asegurado_conductor");
                Archivo archivo = RXUtils.sync(aseguradoData.addVolanteConductor(ajuste, asegurado.getConductor().getVolante()));
                asegurado.getConductor().getVolante().setUrl(archivo.getUrl());
                aseguradoRepository.save(asegurado);
            } catch (Exception ex) {
                throw new VolanteException("conductor asegurado", ex);
            }
        }

        if (!isSynchronized(asegurado.getVehiculo()) && isSynchronized((Object) asegurado.getVehiculo())) {
            try {
                log(ajuste, "synchronize_volante_asegurado_vehiculo");
                Archivo archivo = RXUtils.sync(aseguradoData.addVolanteVehiculo(ajuste, asegurado.getVehiculo().getVolante()));
                asegurado.getVehiculo().getVolante().setUrl(archivo.getUrl());
                aseguradoRepository.save(asegurado);
            } catch (Exception ex) {
                throw new VolanteException("vehículo asegurado", ex);
            }


        }
        for (Ocupante ocupante : asegurado.getOcupantes()) {
            if (!isSynchronized(ocupante) && isSynchronized((Object) ocupante)) {
                try {
                    log(ajuste, "synchronize_volante_asegurado_ocupante", ocupante.getNombreCompleto());
                    Archivo archivo = RXUtils.sync(aseguradoData.addVolanteOcupante(ajuste, ocupante.getId(), ocupante.getVolante()));
                    ocupante.getVolante().setUrl(archivo.getUrl());
                    aseguradoRepository.save(asegurado);
                } catch (Exception ex) {
                    throw new VolanteException("ocupante " + ocupante.getNombreCompleto() + "  del asegurado", ex);
                }

            }
        }
    }

    private void synchronizeTercerosAutos(String ajuste) {
        log(ajuste, "synchronize_volantes_terceroAuto");
        List<TerceroAuto> terceros = terceroAutoRepository.findByAjuste(ajuste);
        for (TerceroAuto tercero : terceros) {
            if (!isSynchronized(tercero.getConductor()) && isSynchronized((Object)tercero.getConductor())) {
                try {
                    log(ajuste, "synchronize_volante_terceroAuto_conductor", tercero.getDescripcion());
                    Archivo archivo = RXUtils.sync(terceroAutoData.addVolanteConductor(ajuste, tercero.getId(), tercero.getConductor().getVolante()));
                    tercero.getConductor().getVolante().setUrl(archivo.getUrl());
                    terceroAutoRepository.saveByAjuste(ajuste, tercero);
                } catch (Exception ex) {
                    throw new VolanteException("conductor del tercero auto " + tercero.getDescripcion(), ex);
                }

            }
            if (!isSynchronized(tercero.getVehiculo()) && isSynchronized((Object) tercero.getVehiculo())) {
                try {
                    log(ajuste, "synchronize_volante_terceroAuto_vehiculo", tercero.getDescripcion());
                    Archivo archivo = RXUtils.sync(terceroAutoData.addVolanteVehiculo(ajuste, tercero.getId(), tercero.getVehiculo().getVolante()));
                    tercero.getVehiculo().getVolante().setUrl(archivo.getUrl());
                    terceroAutoRepository.saveByAjuste(ajuste, tercero);
                } catch (Exception ex) {
                    throw new VolanteException("vehículo del tercero auto " + tercero.getDescripcion(), ex);
                }

            }
            for (Ocupante ocupante : tercero.getOcupantes()) {
                if (!isSynchronized(ocupante) && isSynchronized((Object) ocupante)) {
                    try {
                        log(ajuste, "synchronize_volante_terceroAuto_ocupante", tercero.getDescripcion(), ocupante.getNombreCompleto());
                        Archivo archivo = RXUtils.sync(terceroAutoData.addVolanteOcupante(ajuste, tercero.getId(), ocupante.getId(), ocupante.getVolante()));
                        ocupante.getVolante().setUrl(archivo.getUrl());
                        terceroAutoRepository.saveByAjuste(ajuste, tercero);
                    } catch (Exception ex) {
                        throw new VolanteException("ocupante " + ocupante.getNombreCompleto() + "  del tercero auto " + tercero.getDescripcion(), ex);
                    }

                }
            }
        }
    }

    private void synchronizeTercerosNoAutos(String ajuste) {
        log(ajuste, "synchronize_volantes_terceroNoAuto");
        List<TerceroNoAuto> terceros = terceroNoAutoRepository.findByAjuste(ajuste);
        for (TerceroNoAuto tercero : terceros) {
            if (!isSynchronized(tercero) && isSynchronized((Object) tercero)) {
                try {
                    log(ajuste, "synchronize_volante_terceroNoAuto", tercero.getDescripcion());
                    Archivo archivo = RXUtils.sync(terceroNoAutoData.addVolante(ajuste, tercero.getId(), tercero.getVolante()));
                    tercero.getVolante().setUrl(archivo.getUrl());
                    terceroNoAutoRepository.saveByAjuste(ajuste, tercero);
                } catch (Exception ex) {
                    throw new VolanteException("tercero no auto " + tercero.getDescripcion(), ex);
                }

            }
        }
    }

    private void synchronizeTercerosPersona(String ajuste) {
        log(ajuste, "synchronize_volantes_terceroPersona");
        List<TerceroPersona> terceros = terceroPersonaRepository.findByAjuste(ajuste);
        for (TerceroPersona tercero : terceros) {
            if (!isSynchronized(tercero.getPersona()) && isSynchronized((Object) tercero.getPersona())) {
                try {
                    log(ajuste, "synchronize_volante_terceroPersona", tercero.getPersona().getNombreCompleto());
                    Archivo archivo = RXUtils.sync(tercerosPersonaData.addVolante(ajuste, tercero.getId(), tercero.getPersona().getVolante()));
                    tercero.getPersona().getVolante().setUrl(archivo.getUrl());
                    terceroPersonaRepository.saveByAjuste(ajuste, tercero);
                } catch (Exception ex) {
                    throw new VolanteException("tercero persona " + tercero.getDescripcion(), ex);
                }
            }
        }
    }

    @Override
    protected void cleanErrors(String ajuste) {
        Throwable error = errorRepository.findByAjuste(ajuste);
        if (error instanceof VolanteException) {
            errorRepository.deleteByAjuste(ajuste);
        }
    }

    @Override
    public Observable<Resumen> getEstatus(String ajuste) {
        return Observable.create(e -> {
            Resumen resumen = new Resumen();
            resumen.setEstatus(isSynchronized(ajuste) ? EstatusResumen.OK : EstatusResumen.PENDIENTE);
            boolean hasVolante = false;
            Asegurado asegurado = aseguradoRepository.findByAjuste(ajuste);
            List<TerceroAuto> autos = terceroAutoRepository.findByAjuste(ajuste);
            List<TerceroNoAuto> noAutos = terceroNoAutoRepository.findByAjuste(ajuste);
            List<TerceroPersona> personas = terceroPersonaRepository.findByAjuste(ajuste);
            if (asegurado != null) {
                hasVolante |= asegurado.getConductor() != null && asegurado.getConductor().getVolante() != null;
                hasVolante |= asegurado.getVehiculo() != null && asegurado.getVehiculo().getVolante() != null;
                for (Ocupante ocupante : asegurado.getOcupantes()) {
                    hasVolante |= ocupante.getVolante() != null;
                }
            }
            for (TerceroAuto tercero : autos) {
                hasVolante |= tercero.getConductor() != null && tercero.getConductor().getVolante() != null;
                hasVolante |= tercero.getVehiculo() != null && tercero.getVehiculo().getVolante() != null;
                for (Ocupante ocupante : tercero.getOcupantes()) {
                    hasVolante |= ocupante.getVolante() != null;
                }
            }
            for (TerceroPersona tercero : personas) {
                hasVolante |= tercero.getPersona() != null && tercero.getPersona().getVolante() != null;
            }
            for (TerceroNoAuto tercero : noAutos) {
                hasVolante |= tercero.getVolante() != null;
            }
            if (!hasVolante) {
                resumen.setEstatus(EstatusResumen.SIN_ESTATUS);
            }
            Throwable error = errorRepository.findByAjuste(ajuste);
            if (error instanceof VolanteException) {
                resumen.setEstatus(EstatusResumen.ERROR);
                resumen.setError(ErrorUtils.getErrorMessage(error));
            }
            e.onNext(resumen);
            e.onComplete();
        });
    }

}
