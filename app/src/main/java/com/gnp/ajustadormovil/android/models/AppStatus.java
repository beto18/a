package com.gnp.ajustadormovil.android.models;

/**
 * Created by oscar on 17/11/17.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.gnp.ajustadormovil.android.AjustadorMovilApplication;


public class AppStatus {
    public static Context context;
    private static boolean connected = false;

    public static boolean isOnline() {
        context = AjustadorMovilApplication.getInstance().getApplicationContext();
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) AjustadorMovilApplication.getInstance()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;


        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
}