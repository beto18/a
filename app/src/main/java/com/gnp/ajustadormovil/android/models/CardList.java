package com.gnp.ajustadormovil.android.models;

/**
 * Almacena la información de cada Card: imagén, título, leyendas, iconos y mensaje de error
 * Created by tixmedia on 4/17/18.
 */

public class CardList {


    private int imageCard;
    private String tittleCard;
    private String subModule1;
    private int icoSm1_status;
    private int icoSm1_foto;
    private int icoSm1_volante;
    private String subModule2;
    private int icoSm2_status;
    private int icoSm2_foto;
    private int icoSm2_volante;
    private String subModule3;
    private int icoSm3_status;
    private int icoSm3_foto;
    private int icoSm3_volante;
    private String subModule4;
    private int icoSm4_status;
    private int icoSm4_foto;
    private int icoSm4_volante;


    private String msgSm1;
    private String msgSm2;
    private String msgSm3;
    private String msgSm4;

    public String getMsgSm1() {
        return msgSm1;
    }

    public void setMsgSm1(String msgSm1) {
        this.msgSm1 = msgSm1;
    }

    public String getMsgSm2() {
        return msgSm2;
    }

    public void setMsgSm2(String msgSm2) {
        this.msgSm2 = msgSm2;
    }

    public String getMsgSm3() {
        return msgSm3;
    }

    public void setMsgSm3(String msgSm3) {
        this.msgSm3 = msgSm3;
    }

    public String getMsgSm4() {
        return msgSm4;
    }

    public void setMsgSm4(String msgSm4) {
        this.msgSm4 = msgSm4;
    }

    public int getIcoSm2_status() {

        return icoSm2_status;
    }

    public void setIcoSm2_status(int icoSm2_status) {
        this.icoSm2_status = icoSm2_status;
    }

    public int getIcoSm2_foto() {
        return icoSm2_foto;
    }

    public void setIcoSm2_foto(int icoSm2_foto) {
        this.icoSm2_foto = icoSm2_foto;
    }

    public int getIcoSm2_volante() {
        return icoSm2_volante;
    }

    public void setIcoSm2_volante(int icoSm2_volante) {
        this.icoSm2_volante = icoSm2_volante;
    }

    public int getIcoSm3_status() {
        return icoSm3_status;
    }

    public void setIcoSm3_status(int icoSm3_status) {
        this.icoSm3_status = icoSm3_status;
    }

    public int getIcoSm3_foto() {
        return icoSm3_foto;
    }

    public void setIcoSm3_foto(int icoSm3_foto) {
        this.icoSm3_foto = icoSm3_foto;
    }

    public int getIcoSm3_volante() {
        return icoSm3_volante;
    }

    public void setIcoSm3_volante(int icoSm3_volante) {
        this.icoSm3_volante = icoSm3_volante;
    }

    public int getIcoSm4_status() {
        return icoSm4_status;
    }

    public void setIcoSm4_status(int icoSm4_status) {
        this.icoSm4_status = icoSm4_status;
    }

    public int getIcoSm4_foto() {
        return icoSm4_foto;
    }

    public void setIcoSm4_foto(int icoSm4_foto) {
        this.icoSm4_foto = icoSm4_foto;
    }

    public int getIcoSm4_volante() {
        return icoSm4_volante;
    }

    public void setIcoSm4_volante(int icoSm4_volante) {
        this.icoSm4_volante = icoSm4_volante;
    }

    public int getImageCard() {
        return imageCard;
    }

    public void setImageCard(int imageCard) {
        this.imageCard = imageCard;
    }

    public String getTittleCard() {
        return tittleCard;
    }

    public void setTittleCard(String tittleCard) {
        this.tittleCard = tittleCard;
    }

    public String getSubModule1() {
        return subModule1;
    }

    public void setSubModule1(String subModule1) {
        this.subModule1 = subModule1;
    }

    public String getSubModule2() {
        return subModule2;
    }

    public void setSubModule2(String subModule2) {
        this.subModule2 = subModule2;
    }

    public String getSubModule3() {
        return subModule3;
    }

    public void setSubModule3(String subModule3) {
        this.subModule3 = subModule3;
    }

    public String getSubModule4() {
        return subModule4;
    }

    public void setSubModule4(String subModule4) {
        this.subModule4 = subModule4;
    }

    public int getIcoSm1_status() {
        return icoSm1_status;
    }

    public void setIcoSm1_status(int icoSm1_status) {
        this.icoSm1_status = icoSm1_status;
    }

    public int getIcoSm1_foto() {
        return icoSm1_foto;
    }

    public void setIcoSm1_foto(int icoSm1_foto) {
        this.icoSm1_foto = icoSm1_foto;
    }

    public int getIcoSm1_volante() {
        return icoSm1_volante;
    }

    public void setIcoSm1_volante(int icoSm1_volante) {
        this.icoSm1_volante = icoSm1_volante;
    }

}

