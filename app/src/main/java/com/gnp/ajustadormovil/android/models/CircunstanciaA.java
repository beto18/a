package com.gnp.ajustadormovil.android.models;

/**
 * Created by
 * oscar on 14/02/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CircunstanciaA {

    @SerializedName("id")
    @Expose
    private Integer id = null;
    @SerializedName("nombre")
    @Expose
    private String nombre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        if (id != null){
            this.id = id;
        }
        else {
            this.id = null;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
