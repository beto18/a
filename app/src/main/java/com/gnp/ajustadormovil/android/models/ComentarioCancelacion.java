package com.gnp.ajustadormovil.android.models;

import com.google.gson.annotations.SerializedName;

public class ComentarioCancelacion {

    @SerializedName("comentario")
    private String comentario;
    @SerializedName("motivo")
    private String cancelacion;

    public ComentarioCancelacion() {
    }

    public ComentarioCancelacion(String comentario, String cancelacion) {
        this.comentario = comentario;
        this.cancelacion = cancelacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCancelacion() {
        return cancelacion;
    }

    public void setCancelacion(String cancelacion) {
        this.cancelacion = cancelacion;
    }
}
