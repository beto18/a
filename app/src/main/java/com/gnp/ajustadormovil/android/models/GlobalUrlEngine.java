package com.gnp.ajustadormovil.android.models;

import com.gnp.ajustadormovil.android.BuildConfig;

/**
 * Created by oscarmichael on 2/28/18.
 * Modified by muraga on 04/04/2018
 */

public class GlobalUrlEngine {
    public static String GlobalUrlEngine_Orquestador = BuildConfig.ORQUESTADOR;
    public static String GlobalUrlEngine_Catalogos = BuildConfig.CATALOGOS;
    public static String GlobalUrlEngine_LogIn = BuildConfig.LOGIN;
    public static String GlobalUrlEngine_LogIn_Aut =  BuildConfig.LOGIN_AUT;
    public static String GlobalUrlEngine_LogIn_Aut_VAR =  BuildConfig.LOGIN_AUT_VAR;
    public static String GlobalUrlEngine_Version =  BuildConfig.VERSION;
    public static final String GlobalUrlEngine_Segmentador = "http://serviciosmoviles.gnp.com.mx/Segmentacion/inicio.html#esPersiana664";
    public static final String GlobalUrlEngine_SolicitudGrua= "https://docs.google.com/forms/d/e/1FAIpQLSf_QtXEfiXJOT9vUs3X1WCsdmcNHVndv5_rk82rHIcPVWTU1w/viewform?c=0&w=1";
}