package com.gnp.ajustadormovil.android.models;

/**
 *
 * Created by Abraham on 25/11/2017.
 */

public class PolizaDetalle {

    private String coche;
    private String placa;
    private int numeroSiniestro;
    private int modelo;

    private PolizaDetalle(PolizaDetalleBuilder builder){
        this.coche = builder.coche;
        this.placa = builder.placa;
        this.numeroSiniestro = builder.numeroSiniestro;
        this.modelo = builder.modelo;
    }

    public String getCoche () {
        return coche;
    }

    public String getPlaca () {
        return placa;
    }

    public int getNumeroSiniestro () {
        return numeroSiniestro;
    }

    public int getModelo () {
        return modelo;
    }

    public static class PolizaDetalleBuilder {

        private String coche;
        private String placa;
        private int numeroSiniestro;
        private int modelo;

        public PolizaDetalleBuilder coche (String coche) {
            this.coche = coche;
            return this;
        }

        public PolizaDetalleBuilder placa (String placa){
            this.placa = placa;
            return this;
        }

        public PolizaDetalleBuilder numeroSiniestro(int numeroSiniestro){
            this.numeroSiniestro = numeroSiniestro;
            return this;
        }

        public PolizaDetalleBuilder modelo(int modelo){
            this.modelo = modelo;
            return this;
        }

        public PolizaDetalle builder(){
            return new PolizaDetalle(this);
        }
    }
}
