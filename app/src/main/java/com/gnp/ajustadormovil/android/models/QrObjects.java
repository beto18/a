package com.gnp.ajustadormovil.android.models;

import android.os.Parcel;
import android.os.Parcelable;

public class QrObjects implements Parcelable {
    private String titulo;
    private String qr;

    public QrObjects(){
        titulo = "";
        qr = "";
    }
    public QrObjects(Parcel in){
        titulo = in.readString();
        qr = in.readString();
    }
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(titulo);
        parcel.writeString(qr);
    }
    public static final Creator<QrObjects> CREATOR = new Creator<QrObjects>()
    {
        public QrObjects createFromParcel(Parcel in)
        {
            return new QrObjects(in);
        }
        public QrObjects[] newArray(int size)
        {
            return new QrObjects[size];
        }
    };
}

