package com.gnp.ajustadormovil.android.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abraham on 20/02/2018.
 */

public class SerieResponse {

    @SerializedName ("poliza")
    private String poliza;
    @SerializedName ("inicioPoliza")
    private String inicioPoliza;
    @SerializedName ("finPoliza")
    private String finPoliza;
    @SerializedName ("marca")
    private String marca;
    @SerializedName ("modelo")
    private Integer modelo;
    @SerializedName ("vin")
    private String vin;
    @SerializedName ("inciso")
    private String inciso;
    @SerializedName ("submarca")
    private String submarca;

    public SerieResponse () {
    }

    public SerieResponse (String poliza, String inicioPoliza, String finPoliza, String marca, Integer modelo, String vin, String inciso, String submarca) {
        this.poliza = poliza;
        this.inicioPoliza = inicioPoliza;
        this.finPoliza = finPoliza;
        this.marca = marca;
        this.modelo = modelo;
        this.vin = vin;
        this.inciso = inciso;
        this.submarca = submarca;
    }

    public String getPoliza () {
        return poliza;
    }

    public String getInicioPoliza () {
        return inicioPoliza;
    }

    public String getFinPoliza () {
        return finPoliza;
    }

    public String getMarca () {
        return marca;
    }

    public Integer getModelo () {
        return modelo;
    }

    public String getVin () {
        return vin;
    }

    public String getInciso () {
        return inciso;
    }

    public String getSubmarca () {
        return submarca;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public void setInicioPoliza(String inicioPoliza) {
        this.inicioPoliza = inicioPoliza;
    }

    public void setFinPoliza(String finPoliza) {
        this.finPoliza = finPoliza;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModelo(Integer modelo) {
        this.modelo = modelo;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public void setInciso(String inciso) {
        this.inciso = inciso;
    }

    public void setSubmarca(String submarca) {
        this.submarca = submarca;
    }
}