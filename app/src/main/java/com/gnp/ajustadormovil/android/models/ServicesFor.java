package com.gnp.ajustadormovil.android.models;

public class ServicesFor {
    private Long id;
    private String conductor;
    private String vehiculo;
    private String servicioPara;


    public ServicesFor(Long id, String conductor, String vehiculo, String servicioPara) {
        this.id = id;
        this.conductor = conductor;
        this.vehiculo = vehiculo;
        this.servicioPara = servicioPara;
    }

    public ServicesFor() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public String getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(String vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getServicioPara() {
        return servicioPara;
    }

    public void setServicioPara(String servicioPara) {
        this.servicioPara = servicioPara;
    }
}
