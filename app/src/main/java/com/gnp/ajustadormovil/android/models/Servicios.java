package com.gnp.ajustadormovil.android.models;

/**
 * Created by OscarMichael on 2/2/18.
 */

public class Servicios {
    private String title;
    private String id;
    private String dato2;

    public Servicios (ServiciosBuilder builder) {
        this.title = builder.title;
        this.id = builder.id;
        this.dato2 = builder.dato2;
    }
    public Servicios () {
    }

    public String getTitle () {
        return title;
    }

    public String getDato1 () {
        return id;
    }

    public String getDato2 () {
        return dato2;
    }

    public static class ServiciosBuilder{

        private String title;
        private String id;
        private String dato2;

        public ServiciosBuilder title(String title){
            this.title = title;
            return this;
        }

        public  ServiciosBuilder dato1(String dato1){
            this.id = id;
            return this;
        }

        public ServiciosBuilder dato2(String dato2){
            this.dato2 = dato2;
            return this;
        }

        public Servicios build(){
            return new Servicios (this);
        }
    }

}
