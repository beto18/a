package com.gnp.ajustadormovil.android.models;

public class ValueObject<T> {

    private T value;

    private final Object monitor = new Object();

    public ValueObject(){}

    public ValueObject(T value){
        this.value = value;
    }

    public void setValue(T value){
        synchronized (monitor) {
            this.value = value;
        }
    }

    public T getValue(){
        synchronized (monitor) {
            return this.value;
        }
    }

}
