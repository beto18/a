package com.gnp.ajustadormovil.android.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hgzavala on 12/06/18.
 */

public class VersionResponse {
    @SerializedName("id_app")
    private Integer id_app;

    @SerializedName("version")
    private String version;

    @SerializedName("id_OS")
    private Integer id_OS;

    @SerializedName("fecha_publicacion")
    private String fecha_publicacion;

    @SerializedName("requerida")
    private Boolean requerida;

    public Integer getId_app() {
        return id_app;
    }

    public void setId_app(Integer id_app) {
        this.id_app = id_app;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getId_OS() {
        return id_OS;
    }

    public void setId_OS(Integer id_OS) {
        this.id_OS = id_OS;
    }

    public String getFecha_publicacion() {
        return fecha_publicacion;
    }

    public void setFecha_publicacion(String fecha_publicacion) {
        this.fecha_publicacion = fecha_publicacion;
    }

    public Boolean isRequerida() {
        return requerida;
    }

    @Override
    public String toString() {
        return "VersionResponse{" +
                "id_app=" + id_app +
                ", version='" + version + '\'' +
                ", id_OS=" + id_OS +
                ", fecha_publicacion=" + fecha_publicacion +
                ", requerida=" + requerida +
                '}';
    }

    public void setRequerida(Boolean requerida) {
        this.requerida = requerida;
    }
}
