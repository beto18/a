package com.gnp.ajustadormovil.android.models.address;

import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressWS {

    @SerializedName("latitud")
    @Expose
    private Double latitud;
    @SerializedName("longitud")
    @Expose
    private Double longitud;
    @SerializedName("codigoPostal")
    @Expose
    private String codigoPostal;
    @SerializedName("estado")
    @Expose
    private CatalogoClaveInfo estado;
    @SerializedName("municipio")
    @Expose
    private Municipio municipio;
    @SerializedName("colonia")
    @Expose
    private ColoniaResumen colonia;
    @SerializedName("via")
    @Expose
    private String via;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("direccion")
    @Expose
    private String direccion;

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public CatalogoClaveInfo getEstado() {
        return estado;
    }

    public void setEstado(CatalogoClaveInfo estado) {
        this.estado = estado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public ColoniaResumen getColonia() {
        return colonia;
    }

    public void setColonia(ColoniaResumen colonia) {
        this.colonia = colonia;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}