package com.gnp.ajustadormovil.android.models.ajuste;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hgzavala on 25/4/18.
 */

public class AjusteOperacion {
    @SerializedName("idAjuste")
    private String idAjuste;

    @SerializedName("operacion")
    private String operacion;

    public AjusteOperacion(String idAjuste, String operacion) {
        this.idAjuste = idAjuste;
        this.operacion = operacion;
    }

    @Override
    public String toString() {
        return "AjusteOperacion{" +
                "idAjuste=" + idAjuste +
                "operación=" + operacion +
                '}';
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getIdAjuste() {

        return idAjuste;
    }

    public void setIdAjuste(String idAjuste) {
        this.idAjuste = idAjuste;
    }


}
