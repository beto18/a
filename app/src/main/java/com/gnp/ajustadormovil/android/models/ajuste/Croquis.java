package com.gnp.ajustadormovil.android.models.ajuste;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Croquis extends RealmObject {
    @PrimaryKey
    private String ajuste;

    private String croquis;

    private Double latitude;

    private Double longitude;

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public String getCroquis() {
        return croquis;
    }

    public void setCroquis(String croquis) {
        this.croquis = croquis;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
