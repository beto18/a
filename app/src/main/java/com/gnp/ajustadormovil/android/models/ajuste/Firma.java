package com.gnp.ajustadormovil.android.models.ajuste;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Firma extends RealmObject {
    @PrimaryKey
    private Long id;

    /**
     * Firma en Base64.
     */
    private String data;

    private String ajuste;

    private Long ocupante;

    private Long tercero;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public Long getOcupante() {
        return ocupante;
    }

    public void setOcupante(Long ocupante) {
        this.ocupante = ocupante;
    }

    public Long getTercero() {
        return tercero;
    }

    public void setTercero(Long tercero) {
        this.tercero = tercero;
    }
}
