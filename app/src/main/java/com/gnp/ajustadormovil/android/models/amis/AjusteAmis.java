package com.gnp.ajustadormovil.android.models.amis;

import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.util.List;

import io.realm.RealmList;

public class AjusteAmis {

    private String folioAtencion;

    private String folioAtencionInicial;

    private SiniestroAmis siniestro;

    private CatalogoAmis estatus;

    private CatalogoAmis subestatus;

    private VehiculoAmis vehiculo;

    private Danios danioGeneral;

    private Circunstancia circunstancia;

    private List<ImagenAmis> imagenes;

    private List<Involucrado> involucrados;

    private AseguradoAmis asegurado;

    private PolizaAmis poliza;

    public List<ImagenAmis> getImagenes() {
        if (imagenes == null){
            imagenes = new RealmList<>();
        }
        return imagenes;
    }

    public void setImagenes(List<ImagenAmis> imagenes) {
        this.imagenes = CollectionsUtils.toRealmList(imagenes);
    }

    public String getFolioAtencion() {
        return folioAtencion;
    }

    public void setFolioAtencion(String folioAtencion) {
        this.folioAtencion = folioAtencion;
    }

    public String getFolioAtencionInicial() {
        return folioAtencionInicial;
    }

    public void setFolioAtencionInicial(String folioAtencionInicial) {
        this.folioAtencionInicial = folioAtencionInicial;
    }

    public SiniestroAmis getSiniestro() {
        return siniestro;
    }

    public void setSiniestro(SiniestroAmis siniestro) {
        this.siniestro = siniestro;
    }

    public CatalogoAmis getEstatus() {
        return estatus;
    }

    public void setEstatus(CatalogoAmis estatus) {
        this.estatus = estatus;
    }

    public CatalogoAmis getSubestatus() {
        return subestatus;
    }

    public void setSubestatus(CatalogoAmis subestatus) {
        this.subestatus = subestatus;
    }

    public VehiculoAmis getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(VehiculoAmis vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Danios getDanioGeneral() {
        return danioGeneral;
    }

    public void setDanioGeneral(Danios danioGeneral) {
        this.danioGeneral = danioGeneral;
    }

    public Circunstancia getCircunstancia() {
        return circunstancia;
    }

    public void setCircunstancia(Circunstancia circunstancia) {
        this.circunstancia = circunstancia;
    }

    public List<Involucrado> getInvolucrados() {
        if (involucrados == null){
            involucrados = new RealmList<>();
        }
        return involucrados;
    }

    public void setInvolucrados(List<Involucrado> involucrados) {
        this.involucrados = involucrados;
    }

    public AseguradoAmis getAsegurado() {
        return asegurado;
    }

    public void setAsegurado(AseguradoAmis asegurado) {
        this.asegurado = asegurado;
    }

    public PolizaAmis getPoliza() {
        return poliza;
    }

    public void setPoliza(PolizaAmis poliza) {
        this.poliza = poliza;
    }
}
