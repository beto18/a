package com.gnp.ajustadormovil.android.models.amis;

import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Amis extends RealmObject {

    /**
     * Identificador de ajuste
     */
    @PrimaryKey
    private String ajuste;

    /**
     * Identificador de folio
     */
    private FolioAmis folio;

    /**
     * Folios de amis vinculados
     */
    private RealmList<FolioContraparteAmis> contrapartes;

    /**
     * Si el folio amis se genero offline u online
     */
    private boolean online = true;

    /***
     * Bandera de folio sincronizado
     */
    private Boolean sincronizado;


    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public FolioAmis getFolio() {
        return folio;
    }

    public void setFolio(FolioAmis folio) {
        this.folio = folio;
    }

    public List<FolioContraparteAmis> getContrapartes() {
        if (contrapartes == null){
            contrapartes = new RealmList<>();
        }
        return contrapartes;
    }

    public void setContrapartes(List<FolioContraparteAmis> contrapartes) {
        this.contrapartes = CollectionsUtils.toRealmList(contrapartes);
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public void setContrapartes(RealmList<FolioContraparteAmis> contrapartes) {
        this.contrapartes = contrapartes;
    }

    public Boolean getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(Boolean sincronizado) {
        this.sincronizado = sincronizado;
    }
}
