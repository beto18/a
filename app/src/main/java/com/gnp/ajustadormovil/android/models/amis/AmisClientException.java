package com.gnp.ajustadormovil.android.models.amis;

public class AmisClientException extends AmisException {

    public AmisClientException(String message) {
        super(message);
    }

}
