package com.gnp.ajustadormovil.android.models.amis;

public class AmisException extends RuntimeException {

    public AmisException(String message) {
        super(message);
    }

}
