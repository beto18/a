package com.gnp.ajustadormovil.android.models.amis;

public class AmisServerException extends AmisException {

    public AmisServerException(String message) {
        super(message);
    }
}
