package com.gnp.ajustadormovil.android.models.amis;

public class AseguradoAmis implements Modulo {

    public static final int MODULO = 3;

    private String folio;

    private PersonaAmis persona;

    private TelefonoAmis telefono;

    private CatalogoAmis tipoPersona;

    public PersonaAmis getPersona() {
        return persona;
    }

    public void setPersona(PersonaAmis persona) {
        this.persona = persona;
    }

    public TelefonoAmis getTelefono() {
        return telefono;
    }

    public void setTelefono(TelefonoAmis telefono) {
        this.telefono = telefono;
    }

    public CatalogoAmis getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(CatalogoAmis tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }

}
