package com.gnp.ajustadormovil.android.models.amis;

public class Carretera {

    private String nombre;

    private CatalogoAmis tipoCarretera;

    private String kilometro;

    private String observaciones;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CatalogoAmis getTipoCarretera() {
        return tipoCarretera;
    }

    public void setTipoCarretera(CatalogoAmis tipoCarretera) {
        this.tipoCarretera = tipoCarretera;
    }

    public String getKilometro() {
        return kilometro;
    }

    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
