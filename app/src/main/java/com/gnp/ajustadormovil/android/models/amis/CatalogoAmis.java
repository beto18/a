package com.gnp.ajustadormovil.android.models.amis;

import java.util.Objects;

public class CatalogoAmis {

    private String clave;

    private String valor;

    public CatalogoAmis() { }

    public CatalogoAmis(String clave) {
        this.clave = clave;
    }

    public CatalogoAmis(String clave, String valor) {
        this.clave = clave;
        this.valor = valor;
    }


    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatalogoAmis that = (CatalogoAmis) o;
        return Objects.equals(clave, that.clave) &&
                Objects.equals(valor, that.valor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clave, valor);
    }
}
