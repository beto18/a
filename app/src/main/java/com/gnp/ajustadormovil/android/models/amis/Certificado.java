package com.gnp.ajustadormovil.android.models.amis;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Certificado extends RealmObject {

    @PrimaryKey
    private String numeroSerial;

    private String certificado;

    private String llavePublica;

    private String llavePrivada;

    public String getNumeroSerial() {
        return numeroSerial;
    }

    public void setNumeroSerial(String numeroSerial) {
        this.numeroSerial = numeroSerial;
    }

    public String getCertificado() {
        return certificado;
    }

    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }

    public String getLlavePublica() {
        return llavePublica;
    }

    public void setLlavePublica(String llavePublica) {
        this.llavePublica = llavePublica;
    }

    public String getLlavePrivada() {
        return llavePrivada;
    }

    public void setLlavePrivada(String llavePrivada) {
        this.llavePrivada = llavePrivada;
    }
}
