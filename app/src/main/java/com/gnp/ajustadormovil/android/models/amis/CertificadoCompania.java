package com.gnp.ajustadormovil.android.models.amis;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CertificadoCompania extends RealmObject {

    @PrimaryKey
    private Long compania;

    private String certificado;

    private String llavePublica;

    public Long getCompania() {
        return compania;
    }

    public void setCompania(Long compania) {
        this.compania = compania;
    }

    public String getCertificado() {
        return certificado;
    }

    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }

    public String getLlavePublica() {
        return llavePublica;
    }

    public void setLlavePublica(String llavePublica) {
        this.llavePublica = llavePublica;
    }
}
