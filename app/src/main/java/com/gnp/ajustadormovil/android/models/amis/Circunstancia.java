package com.gnp.ajustadormovil.android.models.amis;

import java.util.List;

public class Circunstancia implements Modulo {

    private String folio;

    private Boolean acuerdo;

    private List<CatalogoAmis> tipoAcuerdo;

    private CatalogoAmis tipoCircunstancia;

    private Boolean otraCircunstancia;

    private String otraCircunstanciaDescripcion;

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Boolean getAcuerdo() {
        return acuerdo;
    }

    public void setAcuerdo(Boolean acuerdo) {
        this.acuerdo = acuerdo;
    }

    public List<CatalogoAmis> getTipoAcuerdo() {
        return tipoAcuerdo;
    }

    public void setTipoAcuerdo(List<CatalogoAmis> tipoAcuerdo) {
        this.tipoAcuerdo = tipoAcuerdo;
    }

    public CatalogoAmis getTipoCircunstancia() {
        return tipoCircunstancia;
    }

    public void setTipoCircunstancia(CatalogoAmis tipoCircunstancia) {
        this.tipoCircunstancia = tipoCircunstancia;
    }

    public Boolean getOtraCircunstancia() {
        return otraCircunstancia;
    }

    public void setOtraCircunstancia(Boolean otraCircunstancia) {
        this.otraCircunstancia = otraCircunstancia;
    }

    public String getOtraCircunstanciaDescripcion() {
        return otraCircunstanciaDescripcion;
    }

    public void setOtraCircunstanciaDescripcion(String otraCircunstanciaDescripcion) {
        this.otraCircunstanciaDescripcion = otraCircunstanciaDescripcion;
    }
}
