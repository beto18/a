package com.gnp.ajustadormovil.android.models.amis;

import java.util.Objects;

public class Danio {

    private CatalogoAmis area;

    private CatalogoAmis tipoDanio;

    private CatalogoAmis parteDaniada;

    public CatalogoAmis getArea() {
        return area;
    }

    public void setArea(CatalogoAmis area) {
        this.area = area;
    }

    public CatalogoAmis getTipoDanio() {
        return tipoDanio;
    }

    public void setTipoDanio(CatalogoAmis tipoDanio) {
        this.tipoDanio = tipoDanio;
    }

    public CatalogoAmis getParteDaniada() {
        return parteDaniada;
    }

    public void setParteDaniada(CatalogoAmis parteDaniada) {
        this.parteDaniada = parteDaniada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Danio)) return false;
        Danio danio = (Danio) o;
        return Objects.equals(area, danio.area) &&
                Objects.equals(tipoDanio, danio.tipoDanio) &&
                Objects.equals(parteDaniada, danio.parteDaniada);
    }

    @Override
    public int hashCode() {
        return Objects.hash(area, tipoDanio, parteDaniada);
    }
}
