package com.gnp.ajustadormovil.android.models.amis;

import java.util.List;

public class Danios implements Modulo {

    private String folio;

    private List<Danio> listaDanios;

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public List<Danio> getListaDanios() {
        return listaDanios;
    }

    public void setListaDanios(List<Danio> listaDanios) {
        this.listaDanios = listaDanios;
    }
}
