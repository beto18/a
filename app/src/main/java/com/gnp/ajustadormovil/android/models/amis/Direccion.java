package com.gnp.ajustadormovil.android.models.amis;


public class Direccion {

    private CatalogoAmis estado;

    private CatalogoAmis delegacionMunicipio;

    private CatalogoAmis colonia;

    private String codigoPostal;

    private CatalogoAmis tipoVialidad;

    private String calle;

    private String entreCalles1;

    private String entreCalles2;

    private String numeroExterior;

    private String numeroInterior;

    public CatalogoAmis getEstado() {
        return estado;
    }

    public void setEstado(CatalogoAmis estado) {
        this.estado = estado;
    }

    public CatalogoAmis getDelegacionMunicipio() {
        return delegacionMunicipio;
    }

    public void setDelegacionMunicipio(CatalogoAmis delegacionMunicipio) {
        this.delegacionMunicipio = delegacionMunicipio;
    }

    public CatalogoAmis getColonia() {
        return colonia;
    }

    public void setColonia(CatalogoAmis colonia) {
        this.colonia = colonia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public CatalogoAmis getTipoVialidad() {
        return tipoVialidad;
    }

    public void setTipoVialidad(CatalogoAmis tipoVialidad) {
        this.tipoVialidad = tipoVialidad;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getEntreCalles1() {
        return entreCalles1;
    }

    public void setEntreCalles1(String entreCalles1) {
        this.entreCalles1 = entreCalles1;
    }

    public String getEntreCalles2() {
        return entreCalles2;
    }

    public void setEntreCalles2(String entreCalles2) {
        this.entreCalles2 = entreCalles2;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }
}
