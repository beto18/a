package com.gnp.ajustadormovil.android.models.amis;

public class EmisionValeDigital {

    private String valeDigital;

    private String firmaDigital;

    public EmisionValeDigital() {}

    public EmisionValeDigital(String valeDigital, String firmaDigital) {
        this.valeDigital = valeDigital;
        this.firmaDigital = firmaDigital;
    }

    public String getValeDigital() {
        return valeDigital;
    }

    public void setValeDigital(String valeDigital) {
        this.valeDigital = valeDigital;
    }

    public String getFirmaDigital() {
        return firmaDigital;
    }

    public void setFirmaDigital(String firmaDigital) {
        this.firmaDigital = firmaDigital;
    }
}
