package com.gnp.ajustadormovil.android.models.amis;

public class FirmaDigital {

    /**
     * identificador de la aseguradora
     */
    private String idAseguradora;

    /**
     * Información del valeDigital digital
     */
    private String valeDigital;
    /**
     * Información de  la firmaDigital generada
     */
    private String firmaDigital;

    public String getIdAseguradora() {
        return idAseguradora;
    }

    public void setIdAseguradora(String idAseguradora) {
        this.idAseguradora = idAseguradora;
    }

    public String getValeDigital() {
        return valeDigital;
    }

    public void setValeDigital(String valeDigital) {
        this.valeDigital = valeDigital;
    }

    public String getFirmaDigital() {
        return firmaDigital;
    }

    public void setFirmaDigital(String firmaDigital) {
        this.firmaDigital = firmaDigital;
    }


}
