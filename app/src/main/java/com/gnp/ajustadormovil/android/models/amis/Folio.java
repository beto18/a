package com.gnp.ajustadormovil.android.models.amis;

public class Folio {

    private String folio;

    private Boolean offline;

    public Folio(String folio) {
        this.folio = folio;
    }

    public Folio() {
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Boolean getOffline() {
        return offline;
    }

    public void setOffline(Boolean offline) {
        this.offline = offline;
    }
}
