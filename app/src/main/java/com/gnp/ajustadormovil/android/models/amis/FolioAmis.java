package com.gnp.ajustadormovil.android.models.amis;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FolioAmis extends RealmObject {

    @PrimaryKey
    private String folioInicial;

    private String folio;

    public String getFolioInicial() {
        return folioInicial;
    }

    public void setFolioInicial(String folioInicial) {
        this.folioInicial = folioInicial;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

}
