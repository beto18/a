package com.gnp.ajustadormovil.android.models.amis;

import java.io.Serializable;

public class FolioContraparte implements Serializable {

    private String folioInicial;

    private String folio;

    private boolean completo = false;

    public FolioContraparte(String folioInicial, String folio, boolean completo) {
        this.folioInicial = folioInicial;
        this.folio = folio;
        this.completo = completo;
    }

    public FolioContraparte() {
    }

    public String getFolioInicial() {
        return folioInicial;
    }

    public void setFolioInicial(String folioInicial) {
        this.folioInicial = folioInicial;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public boolean isCompleto() {
        return completo;
    }

    public void setCompleto(boolean completo) {
        this.completo = completo;
    }
}
