package com.gnp.ajustadormovil.android.models.amis;


import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import net.sf.oval.constraint.NotNull;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FolioContraparteAmis extends RealmObject {

    /**
     * Identificador de folio inicial
     */
    @PrimaryKey
    @NotNull(message = "folioContraparteAmis.folioInicial.NotNull")
    private String folioInicial;

    /**
     * Identificador de folio
     */
    @NotNull(message = "folioContraparteAmis.folio.NotNull")
    private String folio;

    /**
     * Número de siniestro  de otra compañia
     */
    @NotNull(message = "folioContraparteAmis.siniestro.NotNull")
    private String siniestro;

    /**
     * Clave de la otra compañia
     */
    @NotNull(message = "folioContraparteAmis.compania.NotNull")
    private String compania;

    /**
     * Número de póliza
     */
    @NotNull(message = "folioContraparteAmis.poliza.NotNull")
    private String poliza;

    /**
     * Nombre del titular
     */
    @NotNull(message = "folioContraparteAmis.titular.NotNull" )
    private String titular;

    /**
     *
     * Fecha vigencia fin
     */
    @NotNull(message = "folioContraparteAmis.vigenciaHasta.NotNull")
    private String vigenciaHasta;

    /**
     * fecha de atencion del siniestro
     */
    @NotNull(message = "folioContraparteAmis.fechaAtencion.NotNull" )
    private String fechaAtencion;

    /**
     * Tipo de cobertura
     */
    @NotNull(message = "folioContraparteAmis.tipoCobertura.NotNull")
    private String tipoCobertura;

    /**
     * Folio de pase médico afectado
     */
    private RealmList<String> folioPaseMedico;


    public String getFolioInicial() {
        return folioInicial;
    }


    public void setFolioInicial(String folioInicial) {
        this.folioInicial = folioInicial;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getSiniestro() {
        return siniestro;
    }

    public void setSiniestro(String siniestro) {
        this.siniestro = siniestro;
    }

    public String getCompania() {
        return compania;
    }

    public void setCompania(String compania) {
        this.compania = compania;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(String tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public String getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public List<String> getFolioPaseMedico() {
        if (folioPaseMedico == null) {
            folioPaseMedico = new RealmList<>();
        }
        return folioPaseMedico;
    }

    public void setFolioPaseMedico(List<String> folioPaseMedico) {
        this.folioPaseMedico = CollectionsUtils.toRealmList(folioPaseMedico);
    }


    @Override
    public String toString() {
        return "FolioContraparteAmis{" +
                "folioInicial='" + folioInicial + '\'' +
                ", folio='" + folio + '\'' +
                ", siniestro='" + siniestro + '\'' +
                ", compania='" + compania + '\'' +
                ", poliza='" + poliza + '\'' +
                ", titular='" + titular + '\'' +
                ", vigenciaHasta='" + vigenciaHasta + '\'' +
                ", fechaAtencion='" + fechaAtencion + '\'' +
                ", tipoCobertura='" + tipoCobertura + '\'' +
                ", folioPaseMedico=" + folioPaseMedico +
                '}';
    }
}
