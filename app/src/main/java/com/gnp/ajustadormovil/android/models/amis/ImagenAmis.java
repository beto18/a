package com.gnp.ajustadormovil.android.models.amis;

public class ImagenAmis implements Modulo {

    private String folio;

    private Long id;

    private CatalogoAmis categoria;

    private String urlFoto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CatalogoAmis getCategoria() {
        return categoria;
    }

    public void setCategoria(CatalogoAmis categoria) {
        this.categoria = categoria;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }
}
