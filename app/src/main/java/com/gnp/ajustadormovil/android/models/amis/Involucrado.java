package com.gnp.ajustadormovil.android.models.amis;

public class Involucrado {

    private InvolucradoData datosGrales;

    private PaseMedico paseMedico;

    private Narrativa narrativa;

    public InvolucradoData getDatosGrales() {
        return datosGrales;
    }

    public void setDatosGrales(InvolucradoData datosGrales) {
        this.datosGrales = datosGrales;
    }

    public PaseMedico getPaseMedico() {
        return paseMedico;
    }

    public void setPaseMedico(PaseMedico paseMedico) {
        this.paseMedico = paseMedico;
    }

    public Narrativa getNarrativa() {
        return narrativa;
    }

    public void setNarrativa(Narrativa narrativa) {
        this.narrativa = narrativa;
    }
}
