package com.gnp.ajustadormovil.android.models.amis;

public class InvolucradoDetalles extends InvolucradoModulo {

    private CatalogoAmis tipoInvolucrado;

    private PersonaAmis persona;

    private Direccion direccion;

    private Boolean mismoAsegurado;

    public CatalogoAmis getTipoInvolucrado() {
        return tipoInvolucrado;
    }

    public void setTipoInvolucrado(CatalogoAmis tipoInvolucrado) {
        this.tipoInvolucrado = tipoInvolucrado;
    }

    public PersonaAmis getPersona() {
        return persona;
    }

    public void setPersona(PersonaAmis persona) {
        this.persona = persona;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Boolean getMismoAsegurado() {
        return mismoAsegurado;
    }

    public void setMismoAsegurado(Boolean mismoAsegurado) {
        this.mismoAsegurado = mismoAsegurado;
    }
}
