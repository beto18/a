package com.gnp.ajustadormovil.android.models.amis;

public class InvolucradoGeneral extends InvolucradoModulo {

    private Boolean tratamientoDatosPersonales;

    private Integer versionCtdpCompania;

    private Integer versionCtdpGeneral;

    private String correoElectronico;

    private String fechaVigenciaLicencia;

    private String licencia;

    private CatalogoAmis estadoLicencia;

    private CatalogoAmis tipoLicencia;

    private Boolean avisoPrivacidad;

    private Integer versionAvisoPrivacidadCompania;

    private Integer versionAvisoPrivacidadGeneral;

    private TelefonoAmis telefono;

    public Boolean getTratamientoDatosPersonales() {
        return tratamientoDatosPersonales;
    }

    public void setTratamientoDatosPersonales(Boolean tratamientoDatosPersonales) {
        this.tratamientoDatosPersonales = tratamientoDatosPersonales;
    }

    public Integer getVersionCtdpCompania() {
        return versionCtdpCompania;
    }

    public void setVersionCtdpCompania(Integer versionCtdpCompania) {
        this.versionCtdpCompania = versionCtdpCompania;
    }

    public Integer getVersionCtdpGeneral() {
        return versionCtdpGeneral;
    }

    public void setVersionCtdpGeneral(Integer versionCtdpGeneral) {
        this.versionCtdpGeneral = versionCtdpGeneral;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getFechaVigenciaLicencia() {
        return fechaVigenciaLicencia;
    }

    public void setFechaVigenciaLicencia(String fechaVigenciaLicencia) {
        this.fechaVigenciaLicencia = fechaVigenciaLicencia;
    }

    public String getLicencia() {
        return licencia;
    }

    public void setLicencia(String licencia) {
        this.licencia = licencia;
    }

    public CatalogoAmis getEstadoLicencia() {
        return estadoLicencia;
    }

    public void setEstadoLicencia(CatalogoAmis estadoLicencia) {
        this.estadoLicencia = estadoLicencia;
    }

    public CatalogoAmis getTipoLicencia() {
        return tipoLicencia;
    }

    public void setTipoLicencia(CatalogoAmis tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }

    public Boolean getAvisoPrivacidad() {
        return avisoPrivacidad;
    }

    public void setAvisoPrivacidad(Boolean avisoPrivacidad) {
        this.avisoPrivacidad = avisoPrivacidad;
    }

    public Integer getVersionAvisoPrivacidadCompania() {
        return versionAvisoPrivacidadCompania;
    }

    public void setVersionAvisoPrivacidadCompania(Integer versionAvisoPrivacidadCompania) {
        this.versionAvisoPrivacidadCompania = versionAvisoPrivacidadCompania;
    }

    public Integer getVersionAvisoPrivacidadGeneral() {
        return versionAvisoPrivacidadGeneral;
    }

    public void setVersionAvisoPrivacidadGeneral(Integer versionAvisoPrivacidadGeneral) {
        this.versionAvisoPrivacidadGeneral = versionAvisoPrivacidadGeneral;
    }

    public TelefonoAmis getTelefono() {
        return telefono;
    }

    public void setTelefono(TelefonoAmis telefono) {
        this.telefono = telefono;
    }
}
