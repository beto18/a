package com.gnp.ajustadormovil.android.models.amis;

public class InvolucradoModulo implements Modulo {

    private String folio;

    private String idInvolucrado;

    private Byte idSeccion;

    public String getIdInvolucrado() {
        return idInvolucrado;
    }

    public void setIdInvolucrado(String idInvolucrado) {
        this.idInvolucrado = idInvolucrado;
    }

    public Byte getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Byte idSeccion) {
        this.idSeccion = idSeccion;
    }

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }
}
