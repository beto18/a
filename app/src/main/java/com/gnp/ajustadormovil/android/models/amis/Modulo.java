package com.gnp.ajustadormovil.android.models.amis;

public interface Modulo {

    String getFolio();

    void setFolio(String folio);

}
