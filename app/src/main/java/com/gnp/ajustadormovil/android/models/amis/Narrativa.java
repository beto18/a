package com.gnp.ajustadormovil.android.models.amis;

public class Narrativa extends InvolucradoModulo {

    private String narracion;

    private Boolean declaracionBajoProtesta;

    private Integer declaracionBajoProtestaVersion;

    public String getNarracion() {
        return narracion;
    }

    public void setNarracion(String narracion) {
        this.narracion = narracion;
    }

    public Boolean getDeclaracionBajoProtesta() {
        return declaracionBajoProtesta;
    }

    public void setDeclaracionBajoProtesta(Boolean declaracionBajoProtesta) {
        this.declaracionBajoProtesta = declaracionBajoProtesta;
    }

    public Integer getDeclaracionBajoProtestaVersionGeneral() {
        return declaracionBajoProtestaVersion;
    }

    public void setDeclaracionBajoProtestaVersionGeneral(Integer declaracionBajoProtestaVersionGeneral) {
        this.declaracionBajoProtestaVersion = declaracionBajoProtestaVersionGeneral;
    }

}
