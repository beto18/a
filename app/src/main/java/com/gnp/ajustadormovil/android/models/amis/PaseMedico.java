package com.gnp.ajustadormovil.android.models.amis;

import java.util.List;

public class PaseMedico extends InvolucradoModulo {

    private Boolean paseMedico;

    private String folioPaseMedico;

    private Boolean medicalPassUpdated;

    private List<CatalogoAmis> lesiones;

    public Boolean getPaseMedico() {
        return paseMedico;
    }

    public void setPaseMedico(Boolean paseMedico) {
        this.paseMedico = paseMedico;
    }

    public String getFolioPaseMedico() {
        return folioPaseMedico;
    }

    public void setFolioPaseMedico(String folioPaseMedico) {
        this.folioPaseMedico = folioPaseMedico;
    }

    public Boolean getMedicalPassUpdated() {
        return medicalPassUpdated;
    }

    public void setMedicalPassUpdated(Boolean medicalPassUpdated) {
        this.medicalPassUpdated = medicalPassUpdated;
    }

    public List<CatalogoAmis> getLesiones() {
        return lesiones;
    }

    public void setLesiones(List<CatalogoAmis> lesiones) {
        this.lesiones = lesiones;
    }

}
