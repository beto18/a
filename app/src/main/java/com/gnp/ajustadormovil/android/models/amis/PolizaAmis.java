package com.gnp.ajustadormovil.android.models.amis;

import java.util.List;

public class PolizaAmis implements Modulo {

    private String folio;

    private List<CatalogoAmis> tipoCausa;

    private String numeroSiniestro;

    private Boolean colision;

    private CatalogoAmis compania;

    private PersonaAmis contratante;

    private CatalogoAmis tipoCobranza;

    private CatalogoAmis tipoCobertura;

    private String vigenciaDel;

    private String vigenciaHasta;

    private String numeroPoliza;

    private String inciso;

    private CatalogoAmis tipoPoliza;

    private String numeroReporte;

    private String vinEmision;

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public List<CatalogoAmis> getTipoCausa() {
        return tipoCausa;
    }

    public void setTipoCausa(List<CatalogoAmis> tipoCausa) {
        this.tipoCausa = tipoCausa;
    }

    public String getNumeroSiniestro() {
        return numeroSiniestro;
    }

    public void setNumeroSiniestro(String numeroSiniestro) {
        this.numeroSiniestro = numeroSiniestro;
    }

    public Boolean getColision() {
        return colision;
    }

    public void setColision(Boolean colision) {
        this.colision = colision;
    }

    public CatalogoAmis getCompania() {
        return compania;
    }

    public void setCompania(CatalogoAmis compania) {
        this.compania = compania;
    }

    public PersonaAmis getContratante() {
        return contratante;
    }

    public void setContratante(PersonaAmis contratante) {
        this.contratante = contratante;
    }

    public CatalogoAmis getTipoCobranza() {
        return tipoCobranza;
    }

    public void setTipoCobranza(CatalogoAmis tipoCobranza) {
        this.tipoCobranza = tipoCobranza;
    }

    public CatalogoAmis getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(CatalogoAmis tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public String getVigenciaDel() {
        return vigenciaDel;
    }

    public void setVigenciaDel(String vigenciaDel) {
        this.vigenciaDel = vigenciaDel;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getInciso() {
        return inciso;
    }

    public void setInciso(String inciso) {
        this.inciso = inciso;
    }

    public CatalogoAmis getTipoPoliza() {
        return tipoPoliza;
    }

    public void setTipoPoliza(CatalogoAmis tipoPoliza) {
        this.tipoPoliza = tipoPoliza;
    }

    public String getNumeroReporte() {
        return numeroReporte;
    }

    public void setNumeroReporte(String numeroReporte) {
        this.numeroReporte = numeroReporte;
    }

    public String getVinEmision() {
        return vinEmision;
    }

    public void setVinEmision(String vinEmision) {
        this.vinEmision = vinEmision;
    }
}
