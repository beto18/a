package com.gnp.ajustadormovil.android.models.amis;

public class Refoleo {

    private String folioInicial;

    private String folioRefoleo;

    public Refoleo(){}

    public Refoleo(String folioInicial){
        this.folioInicial = folioInicial;
    }

    public String getFolioInicial() {
        return folioInicial;
    }

    public void setFolioInicial(String folioInicial) {
        this.folioInicial = folioInicial;
    }

    public String getFolioRefoleo() {
        return folioRefoleo;
    }

    public void setFolioRefoleo(String folioRefoleo) {
        this.folioRefoleo = folioRefoleo;
    }
}
