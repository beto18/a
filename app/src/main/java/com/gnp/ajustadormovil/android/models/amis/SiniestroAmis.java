package com.gnp.ajustadormovil.android.models.amis;

public class SiniestroAmis implements Modulo {

    private String folio;

    private String fechaAtencion;

    private String fechaOcurrido;

    private Boolean citaProgramada;

    private UbicacionSiniestroAmis ubicacionSiniestro;

    private String horaAtencion;

    private String horaOcurrido;

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public String getFechaOcurrido() {
        return fechaOcurrido;
    }

    public void setFechaOcurrido(String fechaOcurrido) {
        this.fechaOcurrido = fechaOcurrido;
    }

    public Boolean getCitaProgramada() {
        return citaProgramada;
    }

    public void setCitaProgramada(Boolean citaProgramada) {
        this.citaProgramada = citaProgramada;
    }

    public UbicacionSiniestroAmis getUbicacionSiniestro() {
        return ubicacionSiniestro;
    }

    public void setUbicacionSiniestro(UbicacionSiniestroAmis ubicacionSiniestro) {
        this.ubicacionSiniestro = ubicacionSiniestro;
    }

    public String getHoraAtencion() {
        return horaAtencion;
    }

    public void setHoraAtencion(String horaAtencion) {
        this.horaAtencion = horaAtencion;
    }

    public String getHoraOcurrido() {
        return horaOcurrido;
    }

    public void setHoraOcurrido(String horaOcurrido) {
        this.horaOcurrido = horaOcurrido;
    }
}
