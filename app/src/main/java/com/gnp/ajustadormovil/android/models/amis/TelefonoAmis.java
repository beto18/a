package com.gnp.ajustadormovil.android.models.amis;

public class TelefonoAmis {

    private String lada;

    private String numero;

    public TelefonoAmis(){

    }

    public TelefonoAmis(String lada, String numero){
        this.lada = lada;
        this.numero = numero;
    }

    private CatalogoAmis tipoTelefono;

    public String getLada() {
        return lada;
    }

    public void setLada(String lada) {
        this.lada = lada;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public CatalogoAmis getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(CatalogoAmis tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }
}
