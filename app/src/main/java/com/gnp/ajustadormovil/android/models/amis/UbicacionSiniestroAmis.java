package com.gnp.ajustadormovil.android.models.amis;

public class UbicacionSiniestroAmis {

    private Direccion ciudad;

    private Carretera carretera;

    private Coordenadas coordenadas;

    private CatalogoAmis lugarSiniestro;

    public Direccion getCiudad() {
        return ciudad;
    }

    public void setCiudad(Direccion ciudad) {
        this.ciudad = ciudad;
    }

    public Carretera getCarretera() {
        return carretera;
    }

    public void setCarretera(Carretera carretera) {
        this.carretera = carretera;
    }

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Coordenadas coordenadas) {
        this.coordenadas = coordenadas;
    }

    public CatalogoAmis getLugarSiniestro() {
        return lugarSiniestro;
    }

    public void setLugarSiniestro(CatalogoAmis lugarSiniestro) {
        this.lugarSiniestro = lugarSiniestro;
    }
}
