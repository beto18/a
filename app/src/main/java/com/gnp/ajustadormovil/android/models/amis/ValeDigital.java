package com.gnp.ajustadormovil.android.models.amis;

public class ValeDigital {

    private String valeDigital;

    public String getValeDigital() {
        return valeDigital;
    }

    public void setValeDigital(String valeDigital) {
        this.valeDigital = valeDigital;
    }
}
