package com.gnp.ajustadormovil.android.models.amis;

public class VehiculoAmis implements Modulo {

    private String folio;

    private CatalogoAmis marca;

    private CatalogoAmis clase;

    private CatalogoAmis color;

    private String modelo;

    private String permiso;

    private CatalogoAmis tipo;

    private CatalogoAmis uso;

    private CatalogoAmis tipoVehiculo;

    private String vin;

    @Override
    public String getFolio() {
        return folio;
    }

    @Override
    public void setFolio(String folio) {
        this.folio = folio;
    }

    public CatalogoAmis getMarca() {
        return marca;
    }

    public void setMarca(CatalogoAmis marca) {
        this.marca = marca;
    }

    public CatalogoAmis getClase() {
        return clase;
    }

    public void setClase(CatalogoAmis clase) {
        this.clase = clase;
    }

    public CatalogoAmis getColor() {
        return color;
    }

    public void setColor(CatalogoAmis color) {
        this.color = color;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPermiso() {
        return permiso;
    }

    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }

    public CatalogoAmis getTipo() {
        return tipo;
    }

    public void setTipo(CatalogoAmis tipo) {
        this.tipo = tipo;
    }

    public CatalogoAmis getUso() {
        return uso;
    }

    public void setUso(CatalogoAmis uso) {
        this.uso = uso;
    }

    public CatalogoAmis getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(CatalogoAmis tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
