package com.gnp.ajustadormovil.android.models.catalogos;

import java.io.Serializable;
import java.util.Objects;

public class CatalogoClave implements Serializable, Keyable {

    /**
     * Clave.
     */
    private String clave;

    /**
     * Nombre.
     */
    private String nombre;

    public CatalogoClave(){}

    public CatalogoClave(String clave, String nombre){
        this.clave = clave;
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "CatalogoClave{" +
                "clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    @Override
    public Serializable getKey() {
        return clave;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatalogoClave that = (CatalogoClave) o;
        return clave.equals(that.clave);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clave);
    }
}
