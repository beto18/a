package com.gnp.ajustadormovil.android.models.catalogos;

import java.io.Serializable;

public interface Keyable {

    Serializable getKey();

}
