package com.gnp.ajustadormovil.android.models.catalogos.cat;

import com.gnp.ajustadormovil.android.models.catalogos.Keyable;

import java.io.Serializable;

public class CatalogoClaveInfo implements Serializable, Keyable {

    private String clave;

    private String claveInfo;

    private String nombre;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClaveInfo() {
        return claveInfo;
    }

    public void setClaveInfo(String claveInfo) {
        this.claveInfo = claveInfo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "CatalogoClaveInfo{" +

                ", claveInfo='" + claveInfo + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    @Override
    public Serializable getKey() {
        return clave;
    }
}
