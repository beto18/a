package com.gnp.ajustadormovil.android.models.catalogos.cat;

import java.io.Serializable;

public class Colonia extends ColoniaResumen implements Serializable {

    private String codigoPostal;

    private CatalogoClaveInfo asentamiento;

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public CatalogoClaveInfo getAsentamiento() {
        return asentamiento;
    }

    public void setAsentamiento(CatalogoClaveInfo asentamiento) {
        this.asentamiento = asentamiento;
    }

    @Override
    public String toString() {
        return "Colonia{" +
                "clave='" + getClave() + '\'' +
                ", claveInfo='" + getClaveInfo() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", municipio=" + getMunicipio() +
                ", codigoPostal=" + codigoPostal +
                ", asentamiento=" + asentamiento +
                '}';
    }
}
