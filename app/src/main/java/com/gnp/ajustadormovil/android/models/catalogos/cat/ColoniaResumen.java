package com.gnp.ajustadormovil.android.models.catalogos.cat;


public class ColoniaResumen extends CatalogoClaveInfo {

    private Municipio municipio;

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Override
    public String toString() {
        return "ColoniaResumen{" +
                "clave='" + getClave() + '\'' +
                ", claveInfo='" + getClaveInfo() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", municipio=" + municipio +
                '}';
    }
}
