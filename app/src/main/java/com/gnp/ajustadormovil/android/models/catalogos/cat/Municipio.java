package com.gnp.ajustadormovil.android.models.catalogos.cat;


public class Municipio extends CatalogoClaveInfo {

    private CatalogoClaveInfo estado;

    public CatalogoClaveInfo getEstado() {
        return estado;
    }

    public void setEstado(CatalogoClaveInfo estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Municipio{" +
                "clave='" + getClave() + '\'' +
                ", claveInfo='" + getClaveInfo() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", estado=" + estado +
                '}';
    }
}
