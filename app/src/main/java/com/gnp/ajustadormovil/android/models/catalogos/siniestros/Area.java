package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.util.List;

import io.realm.RealmList;

public class Area extends CatalogoId {

    private List<? extends CatalogoId> partes;

    public Area(){ }

    public Area(Long id, String nombre) {
        super(id, nombre);
    }

    public List<? extends CatalogoId> getPartes() {
        if (partes == null){
            partes = new RealmList<>();
        }
        return partes;
    }

    public void setPartes(List<? extends CatalogoId> partes) {
        this.partes = partes;
    }

    @Override
    public String toString() {
        return "AreaLesionada{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", partes='" + partes + '\'' +
                '}';
    }

}
