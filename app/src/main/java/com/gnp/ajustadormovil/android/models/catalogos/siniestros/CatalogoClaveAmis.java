package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

import java.io.Serializable;

public class CatalogoClaveAmis extends CatalogoClave implements CatalogoAmis, Serializable {

    private String claveAmis;

    @Override
    public void setClaveAmis(String claveAmis) {
        this.claveAmis = claveAmis;
    }

    @Override
    public String getClaveAmis() {
        return claveAmis;
    }


}
