package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import com.gnp.ajustadormovil.android.models.catalogos.Keyable;

import java.io.Serializable;

public class CatalogoId implements Serializable, Keyable {

    /**
     * Identificador.
     */
    private Long id;

    /**
     * Nombre.
     */
    private String nombre;

    public CatalogoId(){}

    public CatalogoId(Long id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "CatalogoId{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    @Override
    public Serializable getKey() {
        return id;
    }
}
