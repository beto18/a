package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class CatalogoIdAmis extends CatalogoId implements CatalogoAmis {

    private String claveAmis;

    @Override
    public void setClaveAmis(String claveAmis) {
        this.claveAmis = claveAmis;
    }

    @Override
    public String getClaveAmis() {
        return claveAmis;
    }

    @Override
    public String toString() {
        return "CatalogoId{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", claveAmis='" + claveAmis + '\'' +
                '}';
    }
}
