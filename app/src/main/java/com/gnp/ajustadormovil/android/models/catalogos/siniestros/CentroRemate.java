package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class CentroRemate extends CatalogoId {

    private String direccion;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "CentroRemate{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }

}
