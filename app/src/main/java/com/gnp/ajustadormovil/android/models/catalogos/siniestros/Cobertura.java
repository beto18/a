package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

import java.util.Date;

public class Cobertura extends CatalogoClave {

    private Long version;

    @JsonAdapter(DateTypeAdapter.class)
    private Date inicioVigencia;

    @JsonAdapter(DateTypeAdapter.class)
    private Date finVigencia;

    private String descripcion;

    private String siglas;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(Date inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public Date getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(Date finVigencia) {
        this.finVigencia = finVigencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    @Override
    public String toString() {
        return "Cobertura{" +
                "clave=" + getClave() +
                ", nombre=" + getNombre() +
                ", version=" + version +
                ", inicioVigencia=" + inicioVigencia +
                ", finVigencia=" + finVigencia +
                ", descripcion='" + descripcion + '\'' +
                ", siglas='" + siglas + '\'' +
                '}';
    }
}
