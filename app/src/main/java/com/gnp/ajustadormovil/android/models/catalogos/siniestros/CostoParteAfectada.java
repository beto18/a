package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class CostoParteAfectada {

    private Double costo;

    private CatalogoId parte;

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public CatalogoId getParte() {
        return parte;
    }

    public void setParte(CatalogoId parte) {
        this.parte = parte;
    }
}
