package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.io.Serializable;

public class EstadoAmis implements Serializable {

    private String clave;

    private Integer claveAmis;

    public EstadoAmis() {
    }

    public String getClave() {
        return clave;
    }

    public Integer getClaveAmis() {
        return claveAmis;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setClaveAmis(Integer claveAmis) {
        this.claveAmis = claveAmis;
    }
}
