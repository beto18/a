package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.io.Serializable;

public class EstadoTipoLicencia implements Serializable {

    private String estado;

    private String estadoAmis;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoAmis() {
        return estadoAmis;
    }

    public void setEstadoAmis(String estadoAmis) {
        this.estadoAmis = estadoAmis;
    }
}
