package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.util.List;

import io.realm.RealmList;

public class Hospital extends HospitalResumen {

    private String correo;

    private String direccion;

    private List<Telefono> telefonos;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Telefono> getTelefonos() {
        if (telefonos == null){
            telefonos = new RealmList<>();
        }
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos = telefonos;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", supervisoria='" + getSupervisoria() + '\'' +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                ", telefonos=" + telefonos +
                '}';
    }
}
