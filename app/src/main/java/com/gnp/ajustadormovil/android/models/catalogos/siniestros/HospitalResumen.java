package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class HospitalResumen extends CatalogoId {

    private String supervisoria;

    public String getSupervisoria() {
        return supervisoria;
    }

    public void setSupervisoria(String supervisoria) {
        this.supervisoria = supervisoria;
    }

    @Override
    public String toString() {
        return "HospitalResumen{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", supervisoria='" + supervisoria + '\'' +
                '}';
    }
}
