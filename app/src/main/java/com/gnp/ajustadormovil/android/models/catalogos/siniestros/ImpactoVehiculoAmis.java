package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class ImpactoVehiculoAmis extends CatalogoIdAmis {

    private Long areaAmis;

    public Long getAreaAmis() {
        return areaAmis;
    }

    public void setAreaAmis(Long areaAmis) {
        this.areaAmis = areaAmis;
    }

    @Override
    public String toString() {
        return "CatalogoId{" +
                "id='" + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", areaAmis='" + areaAmis + '\'' +
                '}';
    }

}
