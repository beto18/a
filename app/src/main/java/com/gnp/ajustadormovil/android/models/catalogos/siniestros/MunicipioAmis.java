package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.io.Serializable;

public class MunicipioAmis implements Serializable {

    private String clave;

    private Integer claveAmis;

    private EstadoAmis estado;

    public MunicipioAmis() {
    }

    public String getClave() {
        return clave;
    }

    public Integer getClaveAmis() {
        return claveAmis;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setClaveAmis(Integer claveAmis) {
        this.claveAmis = claveAmis;
    }

    public EstadoAmis getEstado() {
        return estado;
    }

    public void setEstado(EstadoAmis estado) {
        this.estado = estado;
    }
}
