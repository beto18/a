package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class ParteAfectada extends CatalogoIdAmis {

    private CatalogoId area;

    private String areaAmis;

    public CatalogoId getArea() {
        return area;
    }

    public void setArea(CatalogoId area) {
        this.area = area;
    }

    public String getAreaAmis() {
        return areaAmis;
    }

    public void setAreaAmis(String areaAmis) {
        this.areaAmis = areaAmis;
    }
}
