package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.io.Serializable;

public class ProductoCashflow implements Serializable {

    private String productoTecnico;

    private String productoComercial;

    public String getProductoTecnico() {
        return productoTecnico;
    }

    public void setProductoTecnico(String productoTecnico) {
        this.productoTecnico = productoTecnico;
    }

    public String getProductoComercial() {
        return productoComercial;
    }

    public void setProductoComercial(String productoComercial) {
        this.productoComercial = productoComercial;
    }
}
