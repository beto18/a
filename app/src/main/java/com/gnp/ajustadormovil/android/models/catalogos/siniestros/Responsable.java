package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

import java.io.Serializable;

public class Responsable implements Serializable {

    private CatalogoId circunstanciaA;

    private CatalogoId circunstanciaB;

    private CatalogoClave responsabilidad;

    private String urlImagen;

    public CatalogoId getCircunstanciaA() {
        return circunstanciaA;
    }

    public void setCircunstanciaA(CatalogoId circunstanciaA) {
        this.circunstanciaA = circunstanciaA;
    }

    public CatalogoId getCircunstanciaB() {
        return circunstanciaB;
    }

    public void setCircunstanciaB(CatalogoId circunstanciaB) {
        this.circunstanciaB = circunstanciaB;
    }

    public CatalogoClave getResponsabilidad() {
        return responsabilidad;
    }

    public void setResponsabilidad(CatalogoClave responsabilidad) {
        this.responsabilidad = responsabilidad;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }
}
