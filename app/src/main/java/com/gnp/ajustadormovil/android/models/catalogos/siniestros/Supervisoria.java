package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Supervisoria extends RealmObject implements Serializable {

    /**
     * Clave.
     */
    @PrimaryKey
    private String clave;

    /**
     * Nombre.
     */
    private String nombre;

    private Double latitud;

    private Double longitud;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Supervisoria{" +
                "clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                ", latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                '}';
    }

}
