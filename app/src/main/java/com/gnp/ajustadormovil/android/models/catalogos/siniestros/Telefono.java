package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import java.io.Serializable;

public class Telefono implements Serializable {

    private String telefono;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Telefono{" +
                "telefono='" + telefono + '\'' +
                '}';
    }
}
