package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

import java.util.List;

public class TipoLicencia extends CatalogoClave {

    private List<EstadoTipoLicencia> estados;

    public List<EstadoTipoLicencia> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadoTipoLicencia> estados) {
        this.estados = estados;
    }
}
