package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class UsoVehiculo {

    private String usoVehiculo;

    private Long claveAmis;

    public String getUsoVehiculo() {
        return usoVehiculo;
    }

    public void setUsoVehiculo(String usoVehiculo) {
        this.usoVehiculo = usoVehiculo;
    }

    public Long getUsoAmis() {
        return claveAmis;
    }

    public void setUsoAmis(Long usoAmis) {
        this.claveAmis = usoAmis;
    }
}
