package com.gnp.ajustadormovil.android.models.catalogos.siniestros;

public class VehiculoAmis {

    private String tipoVehiculo;

    private String armadora;

    private String carroceria;

    private Long transporte;

    private Long marca;

    private Long tipo;

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getArmadora() {
        return armadora;
    }

    public void setArmadora(String armadora) {
        this.armadora = armadora;
    }

    public String getCarroceria() {
        return carroceria;
    }

    public void setCarroceria(String carroceria) {
        this.carroceria = carroceria;
    }

    public Long getTransporte() {
        return transporte;
    }

    public void setTransporte(Long transporte) {
        this.transporte = transporte;
    }

    public Long getMarca() {
        return marca;
    }

    public void setMarca(Long marca) {
        this.marca = marca;
    }

    public Long getTipo() {
        return tipo;
    }

    public void setTipo(Long tipo) {
        this.tipo = tipo;
    }
}
