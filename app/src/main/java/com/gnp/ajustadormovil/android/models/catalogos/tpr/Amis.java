package com.gnp.ajustadormovil.android.models.catalogos.tpr;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Amis implements Serializable {

    @SerializedName("versionAMIS")
    private String version;

    @SerializedName("carroceriaAMIS")
    private String carroceria;

    @SerializedName("armadoraAMIS")
    private String armadora;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCarroceria() {
        return carroceria;
    }

    public void setCarroceria(String carroceria) {
        this.carroceria = carroceria;
    }

    public String getArmadora() {
        return armadora;
    }

    public void setArmadora(String armadora) {
        this.armadora = armadora;
    }

    @Override
    public String toString() {
        return "Amis{" +
                "version='" + version + '\'' +
                ", carroceria='" + carroceria + '\'' +
                ", armadora='" + armadora + '\'' +
                '}';
    }
}
