package com.gnp.ajustadormovil.android.models.catalogos.tpr;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

public class Armadora extends CatalogoClave {

    private CatalogoClave tipoVehiculo;

    public CatalogoClave getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(CatalogoClave tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    @Override
    public String toString() {
        return "Armadora{" +
                "clave='" + getClave() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", tipoVehiculo='" + tipoVehiculo + '\'' +
                '}';
    }

}
