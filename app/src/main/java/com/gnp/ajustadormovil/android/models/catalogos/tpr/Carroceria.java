package com.gnp.ajustadormovil.android.models.catalogos.tpr;

import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

public class Carroceria extends CatalogoClave {

    private Armadora armadora;

    public Armadora getArmadora() {
        return armadora;
    }

    public void setArmadora(Armadora armadora) {
        this.armadora = armadora;
    }

    @Override
    public String toString() {
        return "Carroceria{" +
                "clave='" + getClave() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", armadora='" + armadora + '\'' +
                '}';
    }
    
}
