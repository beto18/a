package com.gnp.ajustadormovil.android.models.catalogos.tpr;

import java.io.Serializable;

public class VersionVehiculo implements Serializable {

    /**
     * Carrocería del vehículo.
     */
    private Carroceria carroceria;

    /**
     * Versión.
     */
    private String version;

    /**
     * Modelo.
     */
    private Short modelo;

    /**
     * Marca.
     */
    private String nombre;

    /**
     * Categoría
     */
    private String categoria;

    /**
     * Valor.
     */
    private Float valor;

    /**
     * Número pasajeros.
     */
    private String numeroPasajeros;

    /**
     * Guía EBC.
     */
    private String guiaEbc;

    /**
     * CESVI.
     */
    private String cesvi;

    /**
     * AMIS.
     */
    private Amis amis;

    /**
     * Blindado.
     */
    private Boolean blindado;

    /**
     * Alto valor.
     */
    private Boolean altoValor;

    /**
     * Altísimo valor.
     */
    private Boolean altisimoValor;

    /**
     * Alto riesgo.
     */
    private Boolean altoRiesgo;

    public Carroceria getCarroceria() {
        return carroceria;
    }

    public void setCarroceria(Carroceria carroceria) {
        this.carroceria = carroceria;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Short getModelo() {
        return modelo;
    }

    public void setModelo(Short modelo) {
        this.modelo = modelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public String getNumeroPasajeros() {
        return numeroPasajeros;
    }

    public void setNumeroPasajeros(String numeroPasajeros) {
        this.numeroPasajeros = numeroPasajeros;
    }

    public String getGuiaEbc() {
        return guiaEbc;
    }

    public void setGuiaEbc(String guiaEbc) {
        this.guiaEbc = guiaEbc;
    }

    public String getCesvi() {
        return cesvi;
    }

    public void setCesvi(String cesvi) {
        this.cesvi = cesvi;
    }

    public Amis getAmis() {
        return amis;
    }

    public void setAmis(Amis amis) {
        this.amis = amis;
    }

    public Boolean getBlindado() {
        return blindado;
    }

    public void setBlindado(Boolean blindado) {
        this.blindado = blindado;
    }

    public Boolean getAltoValor() {
        return altoValor;
    }

    public void setAltoValor(Boolean altoValor) {
        this.altoValor = altoValor;
    }

    public Boolean getAltisimoValor() {
        return altisimoValor;
    }

    public void setAltisimoValor(Boolean altisimoValor) {
        this.altisimoValor = altisimoValor;
    }

    public Boolean getAltoRiesgo() {
        return altoRiesgo;
    }

    public void setAltoRiesgo(Boolean altoRiesgo) {
        this.altoRiesgo = altoRiesgo;
    }

    @Override
    public String toString() {
        return "VersionVehiculo{" +
                "carroceria=" + carroceria +
                ", version='" + version + '\'' +
                ", modelo=" + modelo +
                ", nombre='" + nombre + '\'' +
                ", categoria='" + categoria + '\'' +
                ", valor=" + valor +
                ", numeroPasajeros='" + numeroPasajeros + '\'' +
                ", guiaEbc='" + guiaEbc + '\'' +
                ", cesvi='" + cesvi + '\'' +
                ", amis=" + amis +
                ", blindado=" + blindado +
                ", altoValor=" + altoValor +
                ", altisimoValor=" + altisimoValor +
                ", altoRiesgo=" + altoRiesgo +
                '}';
    }
}
