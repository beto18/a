package com.gnp.ajustadormovil.android.models.catalogos.tpr;

import com.gnp.ajustadormovil.android.models.catalogos.Keyable;

import java.io.Serializable;

public class VersionVehiculoResumen implements Serializable, Keyable {

    private String version;

    private String nombre;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "VersionVehiculoResumen{" +
                "version='" + version + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }


    @Override
    public Serializable getKey() {
        return version;
    }
}
