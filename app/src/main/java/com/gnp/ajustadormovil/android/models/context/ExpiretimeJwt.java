package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tixmedia on 3/23/18.
 * Modified by hgzavala on 19/06/18.
 */

public class ExpiretimeJwt {

    @SerializedName("days")
    private double days;

    public ExpiretimeJwt(double days) {
        super();
        this.days = days;
    }

    public double getDays() {
        return days;
    }

    public void setDays(double days) {
        this.days = days;
    }

}
