package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.SerializedName;

public class Login {

    private String jwt;

    @SerializedName("refreshtoken")
    private String refreshToken;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
