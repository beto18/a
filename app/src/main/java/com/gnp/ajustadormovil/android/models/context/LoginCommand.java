package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.SerializedName;

public class LoginCommand {

    private String mail;

    private String password;

    @SerializedName("projectid")
    private String projectId;

    @SerializedName("tipousuario")
    private String tipo;

    @SerializedName("expiretime_jwt")
    private ExpireCommand expire;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ExpireCommand getExpire() {
        return expire;
    }

    public void setExpire(ExpireCommand expire) {
        this.expire = expire;
    }
}
