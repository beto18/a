package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tixmedia on 3/23/18.
 */

public class LoginRequest {

    @SerializedName("mail")
    @Expose
    private String mail;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("projectid")
    @Expose
    private String projectid;
    @SerializedName("tipousuario")
    @Expose
    private String tipousuario;
    @SerializedName("expiretime_jwt")
    @Expose
    private ExpiretimeJwt expiretimeJwt;

    public LoginRequest(String mail, String password, String projectid, String tipousuario, ExpiretimeJwt expiretimeJwt) {
        super();
        this.mail = mail;
        this.password = password;
        this.projectid = projectid;
        this.tipousuario = tipousuario;
        this.expiretimeJwt = expiretimeJwt;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProjectid() {
        return projectid;
    }

    public void setProjectid(String projectid) {
        this.projectid = projectid;
    }

    public String getTipousuario() {
        return tipousuario;
    }

    public void setTipousuario(String tipousuario) {
        this.tipousuario = tipousuario;
    }

    public ExpiretimeJwt getExpiretimeJwt() {
        return expiretimeJwt;
    }

    public void setExpiretimeJwt(ExpiretimeJwt expiretimeJwt) {
        this.expiretimeJwt = expiretimeJwt;
    }

}
