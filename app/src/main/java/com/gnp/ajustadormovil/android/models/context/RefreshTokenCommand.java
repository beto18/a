package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.SerializedName;

public class RefreshTokenCommand {

    public  RefreshTokenCommand(){}

    public RefreshTokenCommand(String refreshToken){
        this.refreshToken = refreshToken;
    }

    @SerializedName("refreshtoken")
    public String refreshToken;

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
