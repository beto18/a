package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.SerializedName;

public class Token {

    /**
     * Identificador o nombre del servidor o sistema que emite el token.
     */
    @SerializedName("iss")
    private String issuer;

    /**
     * Identificador o nombre del usuario que el token representa.
     */
    @SerializedName("sub")
    private String subject;

    /**
     * Destinatario previsto de este token.
     */
    @SerializedName("aud")
    private String audience;

    /**
     * Hora y fecha cuando se emitió el token.
     */
    @SerializedName("iat")
    private Long issuedAt;

    /**
     * Hora y fecha cuando expira el token.
     */
    @SerializedName("exp")
    private Long expiration;

    /**
     * Identificador del usuario.
     */
    @SerializedName("uid")
    private String userId;

    /**
     * Datos del usuario.
     */
    @SerializedName("claims")
    private Usuario usuario;

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public Long getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Long issuedAt) {
        this.issuedAt = issuedAt;
    }

    public Long getExpiration() {
        return expiration;
    }

    public void setExpiration(Long expiration) {
        this.expiration = expiration;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "JwtBody{" +
                "issuer='" + issuer + '\'' +
                ", subject='" + subject + '\'' +
                ", audience='" + audience + '\'' +
                ", issuedAt=" + issuedAt +
                ", expiration=" + expiration +
                ", userId='" + userId + '\'' +
                ", usuario=" + usuario +
                '}';
    }
}
