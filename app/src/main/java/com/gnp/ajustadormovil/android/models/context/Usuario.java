package com.gnp.ajustadormovil.android.models.context;

import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class Usuario {

    /**
     * Rol del usuario.
     */
    private String rol;

    /**
     * Negocios operables del usuario.
     */
    private String negociosOperables;

    /**
     * Id del usuario.
     */
    @SerializedName("idparticipante")
    private String idParticipante;

    /**
     * Correo del usuario.
     */
    @SerializedName("mail")
    private String correo;

    /**
     * Apellido materno del usuario.
     */
    @SerializedName("apematerno")
    private String apellidoMaterno;

    /**
     * Apellido paterno del usuario.
     */
    @SerializedName("apepaterno")
    private String apellidoPaterno;

    /**
     * Nombre del usuario.
     */
    @SerializedName("givenname")
    private String nombres;

    /**
     * Si la cuenta del usuario esta bloqueada.
     */
    @SerializedName("cuentabloqueada")
    private Boolean bloqueado;

    /**
     * Tipo de usuario.
     */
    @SerializedName("tipousuario")
    private String tipo;

    /**
     * Roles del usuario.
     */
    private Set<String> roles;

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNegociosOperables() {
        return negociosOperables;
    }

    public void setNegociosOperables(String negociosOperables) {
        this.negociosOperables = negociosOperables;
    }

    public String getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(String idParticipante) {
        this.idParticipante = idParticipante;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Boolean getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(Boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "rol='" + rol + '\'' +
                ", negociosOperables='" + negociosOperables + '\'' +
                ", idParticipante='" + idParticipante + '\'' +
                ", correo='" + correo + '\'' +
                ", apellidoMaterno='" + apellidoMaterno + '\'' +
                ", apellidoPaterno='" + apellidoPaterno + '\'' +
                ", nombres='" + nombres + '\'' +
                ", bloqueado=" + bloqueado +
                ", tipo='" + tipo + '\'' +
                ", roles=" + roles +
                '}';
    }
}
