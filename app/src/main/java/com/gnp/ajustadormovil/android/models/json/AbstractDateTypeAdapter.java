package com.gnp.ajustadormovil.android.models.json;

import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public abstract class AbstractDateTypeAdapter extends TypeAdapter<Date> {

    protected abstract SimpleDateFormat getFormat();

    @Override
    public void write(JsonWriter out, Date value) throws IOException {
        if (value == null){
            out.nullValue();
        } else {
            Calendar.getInstance();
            try {
                out.value(getFormat().format(value));
            } catch (ArrayIndexOutOfBoundsException e) {
                out.nullValue();
            }
        }
    }

    @Override
    public Date read(JsonReader in) throws IOException {
        JsonToken token = in.peek();
        if(token == JsonToken.NULL){
            return null;
        } else {
            try {
                Calendar.getInstance();
                return getFormat().parse(in.nextString());
            } catch (ParseException e){
                throw new JsonParseException(e);
            }
        }
    }


}
