package com.gnp.ajustadormovil.android.models.json;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.SimpleTimeZone;

public class DateTimeTypeAdapter extends AbstractDateTypeAdapter {

    private static final SimpleDateFormat UTC_FORMAT;

    static {
        UTC_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.forLanguageTag("es-MX"));
        UTC_FORMAT.setTimeZone(SimpleTimeZone.getTimeZone("CST6CDT"));
    }

    @Override
    protected SimpleDateFormat getFormat() {
        return UTC_FORMAT;
    }

}
