package com.gnp.ajustadormovil.android.models.json;

import java.text.SimpleDateFormat;

public class DateTypeAdapter extends AbstractDateTypeAdapter {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected SimpleDateFormat getFormat() {
        return FORMAT;
    }
}
