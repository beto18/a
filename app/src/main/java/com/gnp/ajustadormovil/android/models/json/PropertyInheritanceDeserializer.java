package com.gnp.ajustadormovil.android.models.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Type;
import java.util.Map;

public abstract class PropertyInheritanceDeserializer<T> implements JsonDeserializer<T> {

    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json.isJsonNull()){
            return null;
        }
        JsonObject jsonObject = json.getAsJsonObject();
        JsonPrimitive type = (JsonPrimitive) jsonObject.get(getProperty());
        Class<?> clazz = getTypeMappings().get(type.getAsString());
        if(clazz == null){
            throw new JsonParseException("No se puede deserializar el objeto de tipo " + typeOfT.toString());
        }
        return context.deserialize(jsonObject, clazz);
    }

    /**
     * La propiedad que sirve para decidir el tipo de implementación.
     * @return Nombre de la propiedad de tipo de objeto.
     */
    protected abstract String getProperty();

    /**
     * La propiedad que sirve para decidir el tipo de implementación.
     * @return Nombre de la propiedad de tipo de objeto.
     */
    protected abstract Map<String, Class<? extends T>> getTypeMappings();

}
