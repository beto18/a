package com.gnp.ajustadormovil.android.models.json;

import java.text.SimpleDateFormat;
import java.util.SimpleTimeZone;

public class TimeTypeAdapter extends AbstractDateTypeAdapter {

    private static final SimpleDateFormat FORMAT;

    static {
        FORMAT = new SimpleDateFormat("hh:mm:ss");
        FORMAT.setTimeZone(SimpleTimeZone.getTimeZone("CST6CDT"));
    }

    @Override
    protected SimpleDateFormat getFormat() {
        return FORMAT;
    }

}
