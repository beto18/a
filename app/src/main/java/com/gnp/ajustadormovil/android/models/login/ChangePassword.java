package com.gnp.ajustadormovil.android.models.login;

public class ChangePassword {

    private String actual;
    private String nueva;

    public ChangePassword(String actual, String nuevo) {
        this.actual = actual;
        this.nueva = nuevo;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getNuevo() {
        return nueva;
    }

    public void setNuevo(String nuevo) {
        this.nueva= nuevo;
    }
}
