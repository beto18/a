package com.gnp.ajustadormovil.android.models.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tixmedia on 3/20/18.
 */

public class User {
    @SerializedName("jwt")
    public String jwt;
    @SerializedName("cedula")
    public String cedula;
    @SerializedName("mail")
    String mMail;
    @SerializedName("password")
    String mPassword;
    @SerializedName("projectid")
    String mProjectid;
    @SerializedName("tipousuario")
    String mTipousuario;
    @SerializedName("lastlogin")
    String lastlogin;
    @SerializedName("refreshtoken")
    String refreshtoken;


    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getMail() {
        return mMail;
    }

    public void setMail(String mail) {
        mMail = mail;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getProjectid() {
        return mProjectid;
    }

    public void setProjectid(String projectid) {
        mProjectid = projectid;
    }

    public String getTipousuario() {
        return mTipousuario;
    }

    public void setTipousuario(String tipousuario) {
        mTipousuario = tipousuario;
    }

    public String getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }

    public String getRefreshtoken() {
        return refreshtoken;
    }

    public void setRefreshtoken(String refreshtoken) {
        this.refreshtoken = refreshtoken;
    }

    public User(String mail, String password, String projectid, String tipousuario) {
        this.mMail = mail;
        this.mPassword = password;
        this.mProjectid = projectid;
        this.mTipousuario = tipousuario;

    }

    public User(String refreshtoken) {
        this.refreshtoken = refreshtoken;


    }

}
