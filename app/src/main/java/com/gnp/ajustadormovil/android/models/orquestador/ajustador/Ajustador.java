package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClave;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Ajustador extends RealmObject implements Serializable {

    /**
     * Correo Electronico.
     */
    @PrimaryKey
    private String correoElectronico;

    /**
     * Cédula del ajustador
     */
    private String cedula;

    /**
     * Identificador de ajustador
     */
    private String idParticipante;

    /**
     * Estatus del ajustador.
     */
    private String estatus;

    /**
     * Nombre.
     */
    private String nombre;

    /**
     * Apellido Paterno.
     */
    private String apellidoPaterno;

    /**
     * Apellido Materno.
     */
    private String apellidoMaterno;

    /**
     * Fotografía.
     */
    private String fotografia;

    /**
     * Teléfono.
     */
    private String telefono;

    /**
     * Tipo contrato.
     */
    private CatalogoClave tipoContrato;

    /**
     * Contrato.
     */
    private String contrato;

    /**
     * Clave de supervisoría.
     */
    private String supervisoria;

    /**
     * Horario del ajustador.
     */
    private RealmList<Horario> horario;

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(String idParticipante) {
        this.idParticipante = idParticipante;
    }

    public EstatusAjustador getEstatus() {
        return EstatusAjustador.valueOf(estatus);
    }

    public void setEstatus(EstatusAjustador estatus) {
        this.estatus = estatus.name();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getFotografia() {
        return fotografia;
    }

    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public CatalogoClave getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(CatalogoClave tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getSupervisoria() {
        return supervisoria;
    }

    public void setSupervisoria(String supervisoria) {
        this.supervisoria = supervisoria;
    }

    public List<Horario> getHorario() {
        if (horario == null){
            horario = new RealmList<>();
        }
        return horario;
    }

    public void setHorario(List<Horario> horario) {
        this.horario = CollectionsUtils.toRealmList(horario);
    }

    public String getNombreCompleto() {
        if (nombre == null && apellidoPaterno == null && apellidoMaterno == null) {
            return null;
        }
        return ((nombre != null ? nombre : "")  + " "
                + (apellidoPaterno !=null ? apellidoPaterno : "") + " "
                + (apellidoMaterno !=null ? apellidoMaterno : "") + " ")
                .trim();
    }

    @Override
    public String toString() {
        return "Ajustador{" +
                "correoElectronico='" + correoElectronico + '\'' +
                ", cedula='" + cedula + '\'' +
                ", idParticipante='" + idParticipante + '\'' +
                ", estatus=" + estatus +
                ", nombre='" + nombre + '\'' +
                ", apellidoPaterno='" + apellidoPaterno + '\'' +
                ", apellidoMaterno='" + apellidoMaterno + '\'' +
                ", fotografia='" + fotografia + '\'' +
                ", telefono='" + telefono + '\'' +
                ", tipoContrato=" + tipoContrato +
                ", contrato='" + contrato + '\'' +
                ", supervisoria='" + supervisoria + '\'' +
                '}';
    }
}
