package com.gnp.ajustadormovil.android.models.orquestador.ajustador;


public enum Dia {
	
	/**
     * Instancia singleton del día de la semana Lunes {@code 1}.
     */
	LUNES,
	
	/**
     * Instancia singleton del día de la semana Martes {@code 2}.
     */
	MARTES,
	
	/**
     * Instancia singleton del día de la semana Miercoles {@code 3}.
     */
	MIERCOLES,
	
	/**
     * Instancia singleton del día de la semana Jueves {@code 4}.
     */
	JUEVES,
	
	/**
     * Instancia singleton del día de la semana Viernes {@code 5}.
     */
	VIERNES,
	
	/**
     * Instancia singleton del día de la semana Sabado {@code 6}.
     */
	SABADO,
	
	/**
     * Instancia singleton del día de la semana Domingo {@code 7}.
     */
	DOMINGO

}
