package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

import net.sf.oval.constraint.NotNull;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Modelo de Dispositivo.
 */
public class Dispositivo extends RealmObject implements Serializable {

	/**
	 * Token.
	 */
	@NotNull(message = "dispositivo.token.NotNull")
	private String token;

	/**
	 * Tipo de dispositivo.
	 */
	@NotNull(message = "dispositivo.tipoDispositivo.NotNull")
	private String tipo;


	public Dispositivo(){ }

	public Dispositivo(String token){
		this.token = token;
		this.tipo = TipoDispositivo.ANDROID.name();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public TipoDispositivo getTipo() {
		return tipo == null ? null : TipoDispositivo.valueOf(tipo);
	}

	public void setTipo(TipoDispositivo tipo) {
		this.tipo = tipo == null ? null : tipo.name();
	}

	@Override
	public String toString() {
		return "Dispositivo{" +
				"token='" + token + '\'' +
				", tipo=" + tipo +
				'}';
	}
}
