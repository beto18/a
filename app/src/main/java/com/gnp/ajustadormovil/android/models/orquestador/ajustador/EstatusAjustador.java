package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

/**
 * Estatus de {@link Ajustador}.
 */
public enum EstatusAjustador {
	
	ATENDIENDO,
	DISPONIBLE,
	EN_CAMINO,
	EN_RECESO,
	EN_ESPERA_SERVICIO_COBERTURA,
	NO_DISPONIBLE,
	POR_CONFIRMAR,
	POR_SALIR,
	POR_TERMINAR,
	NO_PERMITIDO,
	RETIRADO,
	OTRO

}
