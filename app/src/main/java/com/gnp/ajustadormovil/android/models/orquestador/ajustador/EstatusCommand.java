package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

import net.sf.oval.constraint.NotNull;

import java.io.Serializable;

public class EstatusCommand implements Serializable {

    @NotNull(message = "estatusCommand_estatus_NotNull")
    private EstatusAjustador estatus;

    public EstatusCommand(EstatusAjustador estatus){
        this.estatus = estatus;
    }

    public EstatusAjustador getEstatus() {
        return estatus;
    }

    public void setEstatus(EstatusAjustador estatus) {
        this.estatus = estatus;
    }
}
