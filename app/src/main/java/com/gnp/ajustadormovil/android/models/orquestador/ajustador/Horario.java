package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

import com.gnp.ajustadormovil.android.models.json.TimeTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;


public class Horario extends RealmObject implements Serializable {
	
	private String diaEntrada;
	
	private String diaSalida;

	@JsonAdapter(TimeTypeAdapter.class)
	private Date hrEntrada;

	@JsonAdapter(TimeTypeAdapter.class)
	private Date hrSalida;

	public Dia getDiaEntrada() {
		return diaEntrada == null ? null : Dia.valueOf(diaEntrada);
	}

	public void setDiaEntrada(Dia diaEntrada) {
		this.diaEntrada = diaEntrada == null ? null : diaEntrada.name();
	}

	public Dia getDiaSalida() {
		return diaSalida == null ? null : Dia.valueOf(diaSalida);
	}

	public void setDiaSalida(Dia diaSalida) {
		this.diaSalida = diaSalida == null ? null : diaSalida.name();
	}

	public Date getHrEntrada() {
		return hrEntrada;
	}

	public void setHrEntrada(Date hrEntrada) {
		this.hrEntrada = hrEntrada;
	}

	public Date getHrSalida() {
		return hrSalida;
	}

	public void setHrSalida(Date hrSalida) {
		this.hrSalida = hrSalida;
	}
}
