package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

import net.sf.oval.constraint.NotNull;

import java.io.Serializable;

public class PosicionCommand implements Serializable {

    @NotNull(message = "posicionCommand_latitude_NotNull")
    private Double latitud;

    @NotNull(message = "posicionCommand_longitude_NotNull")
    private Double longitud;

    public PosicionCommand(double latitud, double longitud){
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }
}
