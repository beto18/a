package com.gnp.ajustadormovil.android.models.orquestador.ajustador;

/**
 * Tipos de dispositivos.
 *
 */
public enum TipoDispositivo {

    IOS,
	ANDROID,
	OTRO

}
