package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.json.DateTimeTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo de Ajuste.
 */
public class Ajuste extends RealmObject implements Serializable {

	/**
	 * Identificador de ajuste.
	 */
	@PrimaryKey
	private String id;

	/**
	 * Identificador de siniestro.
	 */
	private Siniestro siniestro;

	/**
	 * Indentificador de Ajustador.
	 */
	private String ajustador;

	/**
	 * Estatus.
	 */
	private String estatus;

	/**
	 * Fecha asignación.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaAsignacion;

	/**
	 * Fecha confirmacion.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaConfirmacion;

	/**
	 * Fecha arribo.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaArribo;

	/**
	 * Fecha terminado.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaTerminado;

	/**
	 * Fecha cancelado.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaCancelado;

	/**
	 * Fecha rechazado.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaRechazado;

	/**
	 * Fotografía para arribo.
	 */
	private String fotografiaArribo;

	/**
	 * Fotografía declaración.
	 */
	private RealmList<Fotografia> fotografiasDeclaracion;

	/**
	 * Identificador del motivo de cancelación.
	 */
	private String motivoCancelacion;

	/**
	 * Identificador del motivo rechazo.
	 */
	private String motivoRechazo;

	/**
	 * Identificador de motivo cierre.
	 */
	private String motivoCierre;

	/**
	 * Conclusión cierre.
	 */
	private String conclusionCierre;

	/**
	 * Ubicación.
	 */
	private Ubicacion ubicacion;

	/**
	 * URL del formato de la declaración.
	 */
	private String urlDeclaracion;

	/**
	 * Folio de la declaración.
	 */
	private String folioDeclaracion;

	/**
	 * Generó costo.
	 */
	private Boolean costo;

	/**
	 * Bandera para aplicar deducible administrativo
	 */
	private Boolean deducibleAdministrativo;

	/**
	 * Cierre desde cabina.
	 */
	private Boolean cierreCabina;

    /**
     * Tipo de ubicación del ajustador.
     */
    private CatalogoClave tipoUbicacionAjustador;

	/**
	 * Narrativa de la declaración
	 */
    private String narrativa;

	/**
	 * Si el ajuste es por cita.
	 */
	private Boolean cita;

	/**
	 * firma de la declaración
	 */
	private String firma;


    private Boolean isImpreso = false;

    private boolean offline = false;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Siniestro getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(Siniestro siniestro) {
		this.siniestro = siniestro;
	}

	public String getAjustador() {
		return ajustador;
	}

	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}

	public EstatusAjuste getEstatus() {
		return estatus == null ? null : EstatusAjuste.valueOf(estatus);
	}

	public void setEstatus(EstatusAjuste estatus) {
		this.estatus = estatus == null ? null : estatus.name();
	}

	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public Date getFechaConfirmacion() {
		return fechaConfirmacion;
	}

	public void setFechaConfirmacion(Date fechaConfirmacion) {
		this.fechaConfirmacion = fechaConfirmacion;
	}

	public Date getFechaArribo() {
		return fechaArribo;
	}

	public void setFechaArribo(Date fechaArribo) {
		this.fechaArribo = fechaArribo;
	}

	public Date getFechaTerminado() {
		return fechaTerminado;
	}

	public void setFechaTerminado(Date fechaTerminado) {
		this.fechaTerminado = fechaTerminado;
	}

	public Date getFechaCancelado() {
		return fechaCancelado;
	}

	public void setFechaCancelado(Date fechaCancelado) {
		this.fechaCancelado = fechaCancelado;
	}

	public Date getFechaRechazado() {
		return fechaRechazado;
	}

	public void setFechaRechazado(Date fechaRechazado) {
		this.fechaRechazado = fechaRechazado;
	}

	public String getFotografiaArribo() {
		return fotografiaArribo;
	}

	public void setFotografiaArribo(String fotografiaArribo) {
		this.fotografiaArribo = fotografiaArribo;
	}

	public List<Fotografia> getFotografiasDeclaracion() {
		if(fotografiasDeclaracion == null){
			fotografiasDeclaracion = new RealmList<>();
		}
		return fotografiasDeclaracion;
	}



	public void setFotografiasDeclaracion(List<Fotografia> fotografiasDeclaracion) {
		this.fotografiasDeclaracion = CollectionsUtils.toRealmList(fotografiasDeclaracion);
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	public String getMotivoCierre() {
		return motivoCierre;
	}

	public void setMotivoCierre(String motivoCierre) {
		this.motivoCierre = motivoCierre;
	}

	public String getConclusionCierre() {
		return conclusionCierre;
	}

	public void setConclusionCierre(String conclusionCierre) {
		this.conclusionCierre = conclusionCierre;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getUrlDeclaracion() {
		return urlDeclaracion;
	}

	public void setUrlDeclaracion(String urlDeclaracion) {
		this.urlDeclaracion = urlDeclaracion;
	}

	public String getFolioDeclaracion() {
		return folioDeclaracion;
	}

	public void setFolioDeclaracion(String folioDeclaracion) {
		this.folioDeclaracion = folioDeclaracion;
	}

	public Boolean getCosto() {
		return costo;
	}

	public void setCosto(Boolean costo) {
		this.costo = costo;
	}

	public Boolean getDeducibleAdministrativo() {
		return deducibleAdministrativo;
	}

	public void setDeducibleAdministrativo(Boolean deducibleAdministrativo) {
		this.deducibleAdministrativo = deducibleAdministrativo;
	}

	public Boolean getCierreCabina() {
		return cierreCabina;
	}

	public void setCierreCabina(Boolean cierreCabina) {
		this.cierreCabina = cierreCabina;
	}

	public CatalogoClave getTipoUbicacionAjustador() {
		return tipoUbicacionAjustador;
	}

	public void setTipoUbicacionAjustador(CatalogoClave tipoUbicacionAjustador) {
		this.tipoUbicacionAjustador = tipoUbicacionAjustador;
	}


	public boolean isOffline() {
		return offline;
	}

	public void setOffline(boolean offline) {
		this.offline = offline;
	}

	public boolean hasArriboOffline(){
		return getFotografiaArribo() != null && !getFotografiaArribo().startsWith("http");
	}

	public String getNarrativa() {
		return narrativa;
	}

	public void setNarrativa(String narrativa) {
		this.narrativa = narrativa;
	}

	public Boolean getImpreso() {
		return isImpreso;
	}

	public void setImpreso(Boolean impreso) {
		isImpreso = impreso;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public Boolean getCita() {
		return cita;
	}

	public void setCita(Boolean cita) {
		this.cita = cita;
	}




	@Override
	public String toString() {
		return "Ajuste{" +
				"id='" + id + '\'' +
				", siniestro=" + siniestro +
				", ajustador='" + ajustador + '\'' +
				", estatus='" + estatus + '\'' +
				", fechaAsignacion=" + fechaAsignacion +
				", fechaConfirmacion=" + fechaConfirmacion +
				", fechaArribo=" + fechaArribo +
				", fechaTerminado=" + fechaTerminado +
				", fechaCancelado=" + fechaCancelado +
				", fechaRechazado=" + fechaRechazado +
				", fotografiaArribo='" + fotografiaArribo + '\'' +
				", fotografiasDeclaracion=" + fotografiasDeclaracion +
				", motivoCancelacion='" + motivoCancelacion + '\'' +
				", motivoRechazo='" + motivoRechazo + '\'' +
				", motivoCierre='" + motivoCierre + '\'' +
				", conclusionCierre='" + conclusionCierre + '\'' +
				", ubicacion=" + ubicacion +
				", urlDeclaracion='" + urlDeclaracion + '\'' +
				", folioDeclaracion='" + folioDeclaracion + '\'' +
				", costo=" + costo +
				", deducibleAdministrativo=" + deducibleAdministrativo +
				", cierreCabina=" + cierreCabina +
				", tipoUbicacionAjustador=" + tipoUbicacionAjustador +
				", narrativa=" + narrativa +
				", isImpreso=" + isImpreso +
				", firma=" + firma +
				'}';
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Ajuste ajuste = (Ajuste) o;
		return Objects.equals(id, ajuste.id) &&
				Objects.equals(siniestro, ajuste.siniestro) &&
				Objects.equals(ajustador, ajuste.ajustador) &&
				Objects.equals(estatus, ajuste.estatus) &&
				Objects.equals(fechaAsignacion, ajuste.fechaAsignacion) &&
				Objects.equals(fechaConfirmacion, ajuste.fechaConfirmacion) &&
				Objects.equals(fechaArribo, ajuste.fechaArribo) &&
				Objects.equals(fechaTerminado, ajuste.fechaTerminado) &&
				Objects.equals(fechaCancelado, ajuste.fechaCancelado) &&
				Objects.equals(fechaRechazado, ajuste.fechaRechazado) &&
				Objects.equals(fotografiaArribo, ajuste.fotografiaArribo) &&
				Objects.equals(fotografiasDeclaracion, ajuste.fotografiasDeclaracion) &&
				Objects.equals(motivoCancelacion, ajuste.motivoCancelacion) &&
				Objects.equals(motivoRechazo, ajuste.motivoRechazo) &&
				Objects.equals(motivoCierre, ajuste.motivoCierre) &&
				Objects.equals(conclusionCierre, ajuste.conclusionCierre) &&
				Objects.equals(ubicacion, ajuste.ubicacion) &&
				Objects.equals(urlDeclaracion, ajuste.urlDeclaracion) &&
				Objects.equals(folioDeclaracion, ajuste.folioDeclaracion) &&
				Objects.equals(costo, ajuste.costo) &&
				Objects.equals(deducibleAdministrativo, ajuste.deducibleAdministrativo) &&
				Objects.equals(cierreCabina, ajuste.cierreCabina) &&
				Objects.equals(tipoUbicacionAjustador, ajuste.tipoUbicacionAjustador) &&
				Objects.equals(narrativa, ajuste.narrativa) &&
				Objects.equals(firma, ajuste.firma) &&
				Objects.equals(isImpreso, ajuste.isImpreso);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, siniestro, ajustador, estatus, fechaAsignacion, fechaConfirmacion, fechaArribo, fechaTerminado, fechaCancelado, fechaRechazado, fotografiaArribo, fotografiasDeclaracion, motivoCancelacion, motivoRechazo, motivoCierre, conclusionCierre, ubicacion, urlDeclaracion, folioDeclaracion, costo, deducibleAdministrativo, cierreCabina, tipoUbicacionAjustador, narrativa);
	}
}
