package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

public class AjusteArriboException extends RuntimeException {

    public AjusteArriboException(String ajuste){
        super("No se puede arribar el ajuste " + ajuste + " por falta de conectividad");
    }

}
