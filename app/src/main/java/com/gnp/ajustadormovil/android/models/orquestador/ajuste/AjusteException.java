package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class AjusteException extends ModuloException {

    public AjusteException(Throwable e){
        super(e);
    }

}
