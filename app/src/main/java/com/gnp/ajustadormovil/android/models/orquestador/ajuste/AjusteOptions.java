package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AjusteOptions extends RealmObject {

    @PrimaryKey
    private String ajuste;

    private Boolean conectado;

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public Boolean getConectado() {
        return conectado;
    }

    public void setConectado(Boolean conectado) {
        this.conectado = conectado;
    }
}
