package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;
import net.sf.oval.constraint.Size;

public class CierreCommand {

    @NotNull
    @NotBlank
    private String motivo;

    @Size(max = 255)
    private String conclusion;

    public CierreCommand(){}

    public CierreCommand(String motivo, String conclusion){
        this.motivo = motivo;
        this.conclusion = conclusion;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    @Override
    public String toString() {
        return "CierreCommand{" +
                "motivo='" + motivo + '\'' +
                ", conclusion='" + conclusion + '\'' +
                '}';
    }

}
