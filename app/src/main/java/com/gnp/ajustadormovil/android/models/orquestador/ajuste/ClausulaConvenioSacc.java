package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Modelo de cláusula de convenio.
 *
 */
public class ClausulaConvenioSacc extends RealmObject implements Serializable {

	/**
	 * Código.
	 */
	private String codigo;

	/**
	 * Descripción.
	 */
	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "ClausulaConvenioSacc{" +
				"codigo='" + codigo + '\'' +
				", descripcion='" + descripcion + '\'' +
				'}';
	}
}
