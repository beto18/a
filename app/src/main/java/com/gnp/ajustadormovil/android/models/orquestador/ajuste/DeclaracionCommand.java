package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import net.sf.oval.constraint.NotNull;
import net.sf.oval.constraint.Size;

public class DeclaracionCommand {

    private String folio;

    @NotNull(message = "declaracionCommand_narrativa_NotNull")
    @Size(max = 500, message = "declaracionCommand_narrativa_MaxSize")
    private String narrativa;

    @NotNull(message = "declaracionCommand_firma_NotNull")
    private String firma;

    public DeclaracionCommand(String folio, String narrativa, String firma){
        this.folio = folio;
    }

    public DeclaracionCommand(){

    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio){
        this.folio = folio;
    }

    public String getNarrativa() {
        return narrativa;
    }

    public void setNarrativa(String narrativa) {
        this.narrativa = narrativa;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }
}
