package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;

public class DeclaracionResumen extends Resumen {

    private boolean documento = false;

    private boolean evidencias = false;

    public boolean isDocumento() {
        return documento;
    }

    public void setDocumento(boolean documento) {
        this.documento = documento;
    }

    public boolean isEvidencias() {
        return evidencias;
    }

    public void setEvidencias(boolean evidencias) {
        this.evidencias = evidencias;
    }
}
