package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

/**
 * Estatus del {@link Ajuste}.
 */
public enum EstatusAjuste {
	
	ASIGNADO,
	CONFIRMADO,
	CONFIRMANDO,
	LLEGADO,
	ARRIBANDO,
	TERMINADO,
	TERMINANDO,
	CANCELADO,
	PENDIENTE_ASIGNACION,
	RECHAZADO,
	RECHAZANDO,
	SINCRONIZANDO,
	ERROR,
	OTRO
	
}
