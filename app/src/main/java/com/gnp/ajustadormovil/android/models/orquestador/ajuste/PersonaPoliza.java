package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Modelo de persona de póliza.
 *
 */
public class PersonaPoliza extends RealmObject implements Serializable {

	/**
	 * Id de la persona.
	 */
	private String codigoFiliacion;

	/**
	 * RFC.
	 */
	private String rfc;

	/**
	 * Tipo de persona.
	 */
	private String tipo;

	/**
	 * Nombres.
	 */
	private String nombre;

	/**
	 * Apellido paterno.
	 */
	private String apellidoPaterno;

	/**
	 * Apellido materno.
	 */
	private String apellidoMaterno;

	/**
	 * Razón social.
	 */
	private String razonSocial;

	public String getNombreCompleto() {
		if("F".equals(tipo)) {
			if (nombre != null || apellidoPaterno != null || apellidoMaterno != null) {
				return (nombre != null ? nombre : "") + " "
						+ (apellidoPaterno != null ? apellidoPaterno : "") + " "
						+ (apellidoMaterno != null ? apellidoMaterno : "") + " ";
			}
		} else if("M".equals(tipo)){
			return razonSocial;
		}
		return "";
	}

	public String getCodigoFiliacion() {
		return codigoFiliacion;
	}

	public void setCodigoFiliacion(String codigoFiliacion) {
		this.codigoFiliacion = codigoFiliacion;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
}
