package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Modelo persona de siniestro.
 */
public class PersonaSiniestro extends RealmObject implements Serializable {

	/**
	 * Nombre de la persona que reporta.
	 */
	private String nombre;

	/**
	 * Apellido paterno.
	 */
	private String apellidoPaterno;

	/***
	 * Apellido Materno.
	 */
	private String apellidoMaterno;

	/**
	 * Telefono.
	 */
	private String telefono;

	/**
	 * Correo Eléctronico.
	 */
	private String correoElectronico;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNombreCompleto(){
		if (nombre != null || apellidoPaterno != null || apellidoMaterno != null) {
			return (nombre != null ? nombre : "") + " "
					+ (apellidoPaterno != null ? apellidoPaterno : "") + " "
					+ (apellidoMaterno != null ? apellidoMaterno : "") + " ";
		}
		return "";
	}

	@Override
	public String toString() {
		return "PersonaSiniestro{" +
				"nombre='" + nombre + '\'' +
				", apellidoPaterno='" + apellidoPaterno + '\'' +
				", apellidoMaterno='" + apellidoMaterno + '\'' +
				", telefono='" + telefono + '\'' +
				", correoElectronico='" + correoElectronico + '\'' +
				'}';
	}
}
