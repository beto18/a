package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveInfoAmis;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Modelo de póliza.
 */
public class Poliza extends RealmObject implements Serializable {

	/**
	 * Número de la poliza.
	 */
	private String numero;

	/**
	 * Clave del sistema origen.
	 */
	private Integer version;

	/**
	 * Producto Técnico
	 */
	private String productoTecnico;

	/**
	 * Producto Comercial
	 */
	private String productoComercial;

	/**
	 * Estatus.
	 */
	private CatalogoClaveInfo estatus;

	/**
	 * Estatus Cobranza.
	 */
	private CatalogoClaveInfoAmis estatusCobranza;

	/**
	 * Forma de indeminización.
	 */
	private CatalogoClave formaIndeminizacion;

	/**
	 * Valor de forma de indeminización.
	 */
	private Double valorFormaIndeminizacion;

	/**
	 * Fecha vigencia inicial.
	 */
	@JsonAdapter(DateTypeAdapter.class)
	private Date fechaVigenciaInicial;

	/**
	 * Fecha Vigencia Final.
	 */
	@JsonAdapter(DateTypeAdapter.class)
	private Date fechaVigenciaFinal;

	/**
	 * Periodo de gracia.
	 */
	private String periodoGracia;

	/**
	 * Alcance.
	 */
	@JsonAdapter(DateTypeAdapter.class)
	private Date alcance;

	/**
	 * Segmento Cliente.
	 */
	private CatalogoClave segmentoCliente;

	/**
	 * Paquete.
	 */
	private String paquete;

	/**
	 * Nombre del paquete
	 */
	private String nombrePaquete;

	/**
	 * Tipo de cobertura.
	 */
	private String tipoCobertura;

	/**
	 * Vehículo de la póliza.
	 */
	private Vehiculo vehiculo;

	/**
	 * Conductor habitual.
	 */
	private PersonaPoliza conductorHabitual;

	/**
	 * Contratante.
	 */
	private PersonaPoliza contratante;

	/**
	 * Titular.
	 */
	private PersonaPoliza titular;

	/**
	 * Coberturas.
	 */
	private RealmList<PolizaCobertura> coberturas;

	/**
	 * Cláusulas.
	 */
	private RealmList<ClausulaConvenioSacc> clausulas;

	/**
	 * Cliente.
	 */
	private String cliente;

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getProductoTecnico() {
		return productoTecnico;
	}

	public void setProductoTecnico(String productoTecnico) {
		this.productoTecnico = productoTecnico;
	}

	public String getProductoComercial() {
		return productoComercial;
	}

	public void setProductoComercial(String productoComercial) {
		this.productoComercial = productoComercial;
	}

	public CatalogoClaveInfo getEstatus() {
		return estatus;
	}

	public void setEstatus(CatalogoClaveInfo estatus) {
		this.estatus = estatus;
	}

	public CatalogoClaveInfoAmis getEstatusCobranza() {
		return estatusCobranza;
	}

	public void setEstatusCobranza(CatalogoClaveInfoAmis estatusCobranza) {
		this.estatusCobranza = estatusCobranza;
	}

	public CatalogoClave getFormaIndeminizacion() {
		return formaIndeminizacion;
	}

	public void setFormaIndeminizacion(CatalogoClave formaIndeminizacion) {
		this.formaIndeminizacion = formaIndeminizacion;
	}

	public Double getValorFormaIndeminizacion() {
		return valorFormaIndeminizacion;
	}

	public void setValorFormaIndeminizacion(Double valorFormaIndeminizacion) {
		this.valorFormaIndeminizacion = valorFormaIndeminizacion;
	}

	public Date getFechaVigenciaInicial() {
		return fechaVigenciaInicial;
	}

	public void setFechaVigenciaInicial(Date fechaVigenciaInicial) {
		this.fechaVigenciaInicial = fechaVigenciaInicial;
	}

	public Date getFechaVigenciaFinal() {
		return fechaVigenciaFinal;
	}

	public void setFechaVigenciaFinal(Date fechaVigenciaFinal) {
		this.fechaVigenciaFinal = fechaVigenciaFinal;
	}

	public String getPeriodoGracia() {
		return periodoGracia;
	}

	public void setPeriodoGracia(String periodoGracia) {
		this.periodoGracia = periodoGracia;
	}

	public Date getAlcance() {
		return alcance;
	}

	public void setAlcance(Date alcance) {
		this.alcance = alcance;
	}

	public CatalogoClave getSegmentoCliente() {
		return segmentoCliente;
	}

	public void setSegmentoCliente(CatalogoClave segmentoCliente) {
		this.segmentoCliente = segmentoCliente;
	}

	public String getPaquete() {
		return paquete;
	}

	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}

	public String getNombrePaquete() {
		return nombrePaquete;
	}

	public void setNombrePaquete(String nombrePaquete) {
		this.nombrePaquete = nombrePaquete;
	}

	public String getTipoCobertura() {
		return tipoCobertura;
	}

	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public PersonaPoliza getConductorHabitual() {
		return conductorHabitual;
	}

	public void setConductorHabitual(PersonaPoliza conductorHabitual) {
		this.conductorHabitual = conductorHabitual;
	}

	public PersonaPoliza getContratante() {
		return contratante;
	}

	public void setContratante(PersonaPoliza contratante) {
		this.contratante = contratante;
	}

	public PersonaPoliza getTitular() {
		return titular;
	}

	public void setTitular(PersonaPoliza titular) {
		this.titular = titular;
	}

	public List<PolizaCobertura> getCoberturas() {
		if (coberturas == null){
			coberturas = new RealmList<>();
		}
		return coberturas;
	}

	public void setCoberturas(List<PolizaCobertura> coberturas) {
		this.coberturas = CollectionsUtils.toRealmList(coberturas);
	}

	public List<ClausulaConvenioSacc> getClausulas() {
		if (clausulas == null){
			clausulas = new RealmList<>();
		}
		return clausulas;
	}

	public void setClausulas(List<ClausulaConvenioSacc> clausulas) {
		this.clausulas = CollectionsUtils.toRealmList(clausulas);
	}
}
