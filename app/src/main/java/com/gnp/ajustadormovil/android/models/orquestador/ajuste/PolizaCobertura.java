package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Poliza Cobertura.
 */
public class PolizaCobertura extends RealmObject implements Serializable {

    /**
     * Clave.
     */
    private String clave;

    /**
     * Nombre
     */
    private String nombre;

    /**
     * Deducible.
     */
    private Double deducible;

    /**
     * Suma Aseguradora.
     */
    private Double sumaAsegurada;

    /**
     * Unidad deducible.
     */
    private String unidadMedidaDeducible;

    /**
     * Unidad de la suma asegurada.
     */
    private String unidadMedidaSumaAsegurada;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getDeducible() {
        if (deducible == null) {
            deducible = 0D;
        }
        return deducible;
    }

    public void setDeducible(Double deducible) {
        this.deducible = deducible;
    }

    public Double getSumaAsegurada() {
        if (sumaAsegurada == null){
            sumaAsegurada = 0D;
        }
        return sumaAsegurada;
    }

    public void setSumaAsegurada(Double sumaAsegurada) {
        this.sumaAsegurada = sumaAsegurada;
    }

    public UnidadMedida getUnidadMedidaDeducible() {
        return unidadMedidaDeducible == null ? null : UnidadMedida.valueOf(unidadMedidaDeducible);
    }

    public void setUnidadMedidaDeducible(UnidadMedida unidadMedidaDeducible) {
        this.unidadMedidaDeducible = unidadMedidaDeducible == null ? null : unidadMedidaDeducible.name();
    }

    public UnidadMedida getUnidadMedidaSumaAsegurada() {
        return unidadMedidaSumaAsegurada == null ? null : UnidadMedida.valueOf(unidadMedidaSumaAsegurada);
    }

    public void setUnidadMedidaSumaAsegurada(UnidadMedida unidadMedidaSumaAsegurada) {
        this.unidadMedidaSumaAsegurada = unidadMedidaSumaAsegurada == null ? null : unidadMedidaSumaAsegurada.name();
    }

    @Override
    public String toString() {
        return "PolizaCobertura{" +
                "clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                ", deducible=" + deducible +
                ", sumaAsegurada=" + sumaAsegurada +
                ", unidadMedidaDeducible='" + unidadMedidaDeducible + '\'' +
                ", unidadMedidaSumaAsegurada='" + unidadMedidaSumaAsegurada + '\'' +
                '}';
    }
}
