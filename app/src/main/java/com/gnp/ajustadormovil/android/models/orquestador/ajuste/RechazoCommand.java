package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

public class RechazoCommand {

    @NotNull
    @NotBlank
    private String motivo;

    public RechazoCommand(){}

    public RechazoCommand(String motivo){
        this.motivo = motivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @Override
    public String toString() {
        return "RechazoCommand{" +
                "motivo='" + motivo + '\'' +
                '}';
    }
}
