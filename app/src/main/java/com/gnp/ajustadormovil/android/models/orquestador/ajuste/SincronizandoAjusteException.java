package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

public class SincronizandoAjusteException extends RuntimeException {

    public SincronizandoAjusteException(String idAjuste){
        super("El ajuste " + idAjuste + " ya se esta sincronizando");
    }

}
