package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.json.DateTimeTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveAmis;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;

/**
 * Modelo de siniestro.
 */
public class Siniestro extends RealmObject implements Serializable {

	/**
	 * Identificador del siniestro.
	 */
	private String id;

	/**
	 * UUID.
	 */
	private String uuid;

	/**
	 * Id de siniestro.
	 */
	private String idSiniestro;

	/**
	 * Id de pre-siniestro.
	 */
	private String idPresiniestro;

	/**
	 * Tipo Siniestro.
	 */
	private CatalogoClaveAmis tipoSiniestro;

	/**
	 * Fecha del reporte.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaReporte;

	/**
	 * Fecha del reporte.
	 */
	@JsonAdapter(DateTimeTypeAdapter.class)
	private Date fechaAsignacion;

	/**
	 * Observaciones.
	 */
	private String observaciones;

	/**
	 * Segmento.
	 */
	private String segmento;

	/**
	 * Quien reporta.
	 */
	private PersonaSiniestro reporta;

	/**
	 * Conductor del siniestro.
	 */
	private PersonaSiniestro conductor;

	/**
	 * Ubicación.
	 */
	private Ubicacion ubicacion;

	/**
	 * Póliza.
	 */
	private Poliza poliza;

	/**
	 * Vehiculo.
	 */
	private Vehiculo vehiculo;

	private String folioAmis;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isPresiniestro() {
		return (idPresiniestro != null && !idPresiniestro.isEmpty()) && (idSiniestro == null || idSiniestro.isEmpty());
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getIdSiniestro() {
		return idSiniestro;
	}

	public void setIdSiniestro(String idSiniestro) {
		this.idSiniestro = idSiniestro;
	}

	public String getIdPresiniestro() {
		return idPresiniestro;
	}

	public void setIdPresiniestro(String idPresiniestro) {
		this.idPresiniestro = idPresiniestro;
	}

	public CatalogoClaveAmis getTipoSiniestro() {
		return tipoSiniestro;
	}

	public void setTipoSiniestro(CatalogoClaveAmis tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	public Date getFechaReporte() {
		return fechaReporte;
	}

	public void setFechaReporte(Date fechaReporte) {
		this.fechaReporte = fechaReporte;
	}

	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public PersonaSiniestro getReporta() {
		return reporta;
	}

	public void setReporta(PersonaSiniestro reporta) {
		this.reporta = reporta;
	}

	public PersonaSiniestro getConductor() {
		return conductor;
	}

	public void setConductor(PersonaSiniestro conductor) {
		this.conductor = conductor;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Poliza getPoliza() {
		return poliza;
	}

	public void setPoliza(Poliza poliza) {
		this.poliza = poliza;
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public String getFolioAmis() {
		return folioAmis;
	}

	public void setFolioAmis(String folioAmis) {
		this.folioAmis = folioAmis;
	}

	@Override
	public String toString() {
		return "Siniestro{" +
				"id='" + id + '\'' +
				", uuid='" + uuid + '\'' +
				", idSiniestro='" + idSiniestro + '\'' +
				", idPresiniestro='" + idPresiniestro + '\'' +
				", tipoSiniestro=" + tipoSiniestro +
				", fechaReporte=" + fechaReporte +
				", fechaAsignacion=" + fechaAsignacion +
				", observaciones='" + observaciones + '\'' +
				", segmento='" + segmento + '\'' +
				", reporta=" + reporta +
				", conductor=" + conductor +
				", ubicacion=" + ubicacion +
				", poliza=" + poliza +
				", vehiculo=" + vehiculo +
				", folioAmis=" + folioAmis +
				'}';
	}
}
