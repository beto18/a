package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

public class TipoUbicacionCommand {

    @NotNull
    @NotBlank
    private String tipoUbicacion;

    public TipoUbicacionCommand(){}

    public TipoUbicacionCommand(String tipoUbicacion){
        this.tipoUbicacion = tipoUbicacion;
    }

    public String getTipoUbicacion() {
        return tipoUbicacion;
    }

    public void setTipoUbicacion(String tipoUbicacion) {
        this.tipoUbicacion = tipoUbicacion;
    }

    @Override
    public String toString() {
        return "TipoUbicacionCommand{" +
                "tipoUbicacion='" + tipoUbicacion + '\'' +
                '}';
    }
}
