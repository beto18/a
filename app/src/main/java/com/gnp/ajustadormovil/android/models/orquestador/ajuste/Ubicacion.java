package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveInfo;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Ubicacion de Ajuste.
 */
public class Ubicacion extends RealmObject implements Serializable {

	/**
	 * Latitud.
	 */
	private Double latitud;

	/**
	 * Longitud.
	 */
	private Double longitud;

	/**
	 * Código Postal.
	 */
	private String codigoPostal;

	/**
	 * Estado.
	 */
	private CatalogoClaveInfo estado;

	/**
	 * Municipio.
	 */
	private CatalogoClaveInfo municipio;

	/**
	 * Colonia.
	 */
	private CatalogoClaveInfo colonia;

	/**
	 * Localidad.
	 */
	private String localidad;

	/**
	 * Vía.
	 */
	private String via;

	/**
	 * Número Interior.
	 */
	private String numeroInterior;

	/**
	 * Númeero Exterior.
	 */
	private String numeroExterior;

	/**
	 * Entre Calles.
	 */
	private String entreCalles;

	/**
	 * Referencias.
	 */
	private String referencias;

	/**
	 * Tipo de ubicación.
	 */
	private CatalogoClaveAmis tipoUbicacion;

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public CatalogoClaveInfo getEstado() {
		return estado;
	}

	public void setEstado(CatalogoClaveInfo estado) {
		this.estado = estado;
	}

	public CatalogoClaveInfo getMunicipio() {
		return municipio;
	}

	public void setMunicipio(CatalogoClaveInfo municipio) {
		this.municipio = municipio;
	}

	public CatalogoClaveInfo getColonia() {
		return colonia;
	}

	public void setColonia(CatalogoClaveInfo colonia) {
		this.colonia = colonia;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getNumeroInterior() {
		return numeroInterior;
	}

	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}

	public String getNumeroExterior() {
		return numeroExterior;
	}

	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}

	public String getEntreCalles() {
		return entreCalles;
	}

	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}

	public String getReferencias() {
		return referencias;
	}

	public void setReferencias(String referencias) {
		this.referencias = referencias;
	}

	public CatalogoClaveAmis getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(CatalogoClaveAmis tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public String getUbicationCompleta(){
		return (via !=null ?via + " " : "")
				+(numeroExterior != null ? numeroExterior+" ":"")
				+(entreCalles!=null?entreCalles+" ":" ")
				+(estado != null?estado.getNombre()+" ":"")
				+(municipio != null?municipio.getNombre()+" ":"")
				+(colonia != null?colonia.getNombre()+" ":"")
				+(localidad!=null?localidad+" ":"")
				+(codigoPostal!=null?codigoPostal+" ":"");
	}

	@Override
	public String toString() {
		return "Ubicacion{" +
				"latitud=" + latitud +
				", longitud=" + longitud +
				", codigoPostal='" + codigoPostal + '\'' +
				", estado=" + estado +
				", municipio=" + municipio +
				", colonia=" + colonia +
				", localidad='" + localidad + '\'' +
				", via='" + via + '\'' +
				", numeroInterior='" + numeroInterior + '\'' +
				", numeroExterior='" + numeroExterior + '\'' +
				", entreCalles='" + entreCalles + '\'' +
				", referencias='" + referencias + '\'' +
				", tipoUbicacion=" + tipoUbicacion +
				'}';
	}
}
