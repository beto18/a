package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import java.text.NumberFormat;
import java.util.Locale;

public enum UnidadMedida {

    ANS(cantidad -> cantidad + " Años"),
    DIAS(cantidad -> cantidad + " Días"),
    DSMG(cantidad -> cantidad + " DSMG"),
    ESOP(cantidad -> cantidad + " puntos"),
    HRS(cantidad -> cantidad + " Horas"),
    MIN(cantidad -> cantidad + " Minutos"),
    PORC(cantidad -> cantidad + " %"),
    SMGMA(cantidad -> cantidad + " SMGMA"),
    UMAS(cantidad -> cantidad + " UMAS"),
    IMPT(cantidad -> {
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));
        return format.format(cantidad);
    });


    private interface UnidadMedidaFormater {

        String format(Double cantidad);

    }

    private UnidadMedidaFormater formater;

    private UnidadMedida(UnidadMedidaFormater formater) {
        this.formater = formater;
    }

    public String format(Double cantidad) {
        return this.formater.format(cantidad);
    }

}
