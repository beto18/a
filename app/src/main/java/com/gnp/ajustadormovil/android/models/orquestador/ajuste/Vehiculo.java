package com.gnp.ajustadormovil.android.models.orquestador.ajuste;

import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClave;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Vehiculo de la Poliza.
 */
public class Vehiculo extends RealmObject implements Serializable {

	/**
	 * Vin.
	 */
	private String vin;

	/**
	 * Tipo.
	 */
	private CatalogoClave tipoVehiculo;

	/**
	 * Marca.
	 */
	private CatalogoClave armadora;

	/**
	 * Clase.
	 */
	private CatalogoClave carroceria;

	/**
	 * Versión.
	 */
	private CatalogoClave version;

	/**
	 * Modelo.
	 */
	private Short modelo;

	/**
	 * Placas.
	 */
	private String placas;

	/**
	 * Número de motor.
	 */
	private String numeroMotor;

	/**
	 * Uso del vehículo.
	 */
	private CatalogoClave uso;

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public CatalogoClave getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(CatalogoClave tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public CatalogoClave getArmadora() {
		return armadora;
	}

	public void setArmadora(CatalogoClave armadora) {
		this.armadora = armadora;
	}

	public CatalogoClave getCarroceria() {
		return carroceria;
	}

	public void setCarroceria(CatalogoClave carroceria) {
		this.carroceria = carroceria;
	}

	public CatalogoClave getVersion() {
		return version;
	}

	public void setVersion(CatalogoClave version) {
		this.version = version;
	}

	public Short getModelo() {
		return modelo;
	}

	public void setModelo(Short modelo) {
		this.modelo = modelo;
	}

	public String getPlacas() {
		return placas;
	}

	public void setPlacas(String placas) {
		this.placas = placas;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public CatalogoClave getUso() {
		return uso;
	}

	public void setUso(CatalogoClave uso) {
		this.uso = uso;
	}

	public String getFullVehicle (){
		String nameVersion;
		if (version!=null){
			if (version.getNombre()!=null){
				nameVersion = version.getNombre();
			}
			else {
				nameVersion = "";
			}
		}
		else {
			nameVersion = "";
		}
		return (tipoVehiculo!=null?tipoVehiculo.getNombre()!=null?tipoVehiculo.getNombre():" ":" ")
				+" "
				+(armadora!=null?armadora.getNombre()!=null?armadora.getNombre():" ":" ")
				+" "
				+(carroceria!=null?carroceria.getNombre()!=null?carroceria.getNombre():" ":" ")
				+" "
				+(modelo!=null?modelo:" ")
				+" "
				+(nameVersion);
	}

	@Override
	public String toString() {
		return "Vehiculo{" +
				"vin='" + vin + '\'' +
				", tipoVehiculo=" + tipoVehiculo +
				", armadora=" + armadora +
				", carroceria=" + carroceria +
				", version=" + version +
				", modelo=" + modelo +
				", placas='" + placas + '\'' +
				", numeroMotor='" + numeroMotor + '\'' +
				", uso=" + uso +
				'}';
	}
}
