package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;

public class AfectadoIncompletoResumen extends AfectadoResumen {

    private boolean datos = true;

    public boolean isDatos() {
        return datos;
    }

    public void setDatos(boolean datos) {
        this.datos = datos;
    }
}
