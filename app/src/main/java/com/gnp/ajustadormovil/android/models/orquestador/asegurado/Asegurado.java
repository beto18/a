package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo del asegurado.
 *
 */
public class Asegurado extends RealmObject implements Serializable {

	@PrimaryKey
	private String ajuste;

	/**
	 * Vehículo.
	 */
	private VehiculoAsegurado vehiculo;

	/**
	 * Conductor.
	 */
	private Persona conductor;

	/**
	 * Ocupantes,
	 */
	private RealmList<Ocupante> ocupantes;

	public String getAjuste() {
		return ajuste;
	}

	public void setAjuste(String ajuste) {
		this.ajuste = ajuste;
	}

	public VehiculoAsegurado getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(VehiculoAsegurado vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Persona getConductor() {
		return conductor;
	}

	public void setConductor(Persona conductor) {
		this.conductor = conductor;
	}

	public List<Ocupante> getOcupantes() {
		if(ocupantes == null){
			ocupantes = new RealmList<>();
		}
		return ocupantes;
	}

	public void setOcupantes(List<Ocupante> ocupantes) {
		this.ocupantes = CollectionsUtils.toRealmList(ocupantes);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(ocupantes != null) {
			sb.append("[");
			for (Ocupante ocupante : ocupantes) {
				sb.append("{");
				sb.append(ocupante);
				sb.append("}");
			}
			sb.append("]");
		}
		return "AseguradoAmis{" +
				"ajuste='" + ajuste + '\'' +
				", vehiculo=" + vehiculo +
				", conductor=" + conductor +
				", ocupantes=" + sb.toString() +
				'}';
	}
}
