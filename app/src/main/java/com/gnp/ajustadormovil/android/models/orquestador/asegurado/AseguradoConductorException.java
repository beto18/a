package com.gnp.ajustadormovil.android.models.orquestador.asegurado;


public class AseguradoConductorException extends AseguradoException {

    public AseguradoConductorException(Throwable e){
        super(e);
    }

}
