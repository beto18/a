package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public abstract class AseguradoException extends ModuloException {

    public AseguradoException(Throwable e){
        super(e);
    }

}
