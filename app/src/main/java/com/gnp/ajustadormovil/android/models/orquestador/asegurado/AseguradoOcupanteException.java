package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

public class AseguradoOcupanteException extends AseguradoException {

    private String ocupante;

    public AseguradoOcupanteException(String ocupante, Throwable e){
        super(e);
        this.ocupante = ocupante;
    }

    @Override
    public Object[] getParams() {
        return new Object[] { ocupante };
    }
}
