package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;

public class AseguradoResumen {

    private AfectadoIncompletoResumen conductor = new AfectadoIncompletoResumen();

    private AfectadoIncompletoResumen vehiculo = new AfectadoIncompletoResumen();

    private AfectadoResumen ocupantes = new AfectadoResumen();

    public AfectadoIncompletoResumen getConductor() {
        return conductor;
    }

    public void setConductor(AfectadoIncompletoResumen conductor) {
        this.conductor = conductor;
    }

    public AfectadoIncompletoResumen getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(AfectadoIncompletoResumen vehiculo) {
        this.vehiculo = vehiculo;
    }

    public AfectadoResumen getOcupantes() {
        return ocupantes;
    }

    public void setOcupantes(AfectadoResumen ocupantes) {
        this.ocupantes = ocupantes;
    }

    public EstatusResumen getEstatus(){
        return EstatusResumen.merge(conductor.getEstatus(),
                vehiculo.getEstatus(),
                ocupantes.getEstatus());
    }

}
