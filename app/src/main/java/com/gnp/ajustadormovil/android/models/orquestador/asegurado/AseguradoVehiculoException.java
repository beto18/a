package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

public class AseguradoVehiculoException extends AseguradoException {

    public AseguradoVehiculoException(Throwable e){
        super(e);
    }

}
