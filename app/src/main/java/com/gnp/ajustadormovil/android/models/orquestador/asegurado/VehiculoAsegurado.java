package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografiable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VehiculoAfectado;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;

import io.realm.RealmList;
import io.realm.RealmObject;

public class VehiculoAsegurado extends RealmObject implements VehiculoAfectado<VolanteVehiculoAsegurado>, Modificable, Fotografiable, Serializable {

    /**
     * Vin.
     */
    @NotNull(message = "vehiculoAsegurado.vin.NotNull")
    @MatchPattern(message = "vehiculoAsegurado.vin.MatchPattern", pattern = "^[A-Za-z0-9]+$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    @MaxLength(message = "vehiculoAsegurado.vin.Size", value  = 30)
    private String vin;

    /**
     * Tipo.
     */
    @NotNull(message = "vehiculoAsegurado.tipo.NotNull")
    private String tipoVehiculo;

    /**
     * Marca.
     */
    @NotNull(message = "vehiculoAsegurado.armadora.NotNull")
    private String armadora;

    /**
     * Clase.
     */
    @NotNull(message = "vehiculoAsegurado.carroceria.NotNull")
    private String carroceria;

    /**
     * Versión.
     */
    @NotNull(message = "vehiculoAsegurado.version.NotNull")
    private String version;

    /**
     * Modelo.
     */
    @NotNull(message = "vehiculoAsegurado.modelo.NotNull")
    private Short modelo;

    /**
     * Placas.
     */
    @NotNull(message = "vehiculoAsegurado.placas.NotNull")
    @MatchPattern(message = "vehiculoAsegurado.placas.MatchPattern", pattern = "^[A-Za-z0-9]+$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    @MaxLength(message = "vehiculoAsegurado.placas.Size", value  = 15)
    private String placas;

    /**
     * Número de motor.
     */
    @MaxLength(message = "vehiculoAsegurado.numMotor.Size", value  = 30)
    private String numeroMotor;
    /**
     * Color.
     */
    @NotNull(message = "vehiculoTercero.color.NotNull")
    private Long color;
    /**
     * Primer impacto.
     */
    private Long primerImpacto;

    /**
     * Daño consecuencia.
     */
    private Boolean danoConsecuencia;

    /**
     * Estimado de daños en el vehículo.
     */
    private Double danoEstimado;

    /**
     * Pérdida total evidente.
     */
    private Boolean perdidaTotalEvidente;

    /**
     * Observación.
     */
    @NotBlank(message = "vehiculoAsegurado.observacion.NotBlank")
    @MaxLength(message = "vehiculoAsegurado.observacion.Size",value  = 255)
    private String observacion;

    /**
     * Daños del vehículo.
     */
    private RealmList<PiezaVehiculo> danos;

    /**
     * Daños preexistentes del vehículo.
     */
    private RealmList<PiezaVehiculo> danosPreexistentes;

    /**
     * Fotografías.
     */
    private RealmList<Fotografia> fotografias;

    /**
     * Valor comercial.
     */
    private Double valorComercial;

    /**
     * Volante del vehículo.
     */
    private VolanteVehiculoAsegurado volante;

    private boolean modificado = false;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getArmadora() {
        return armadora;
    }

    public void setArmadora(String armadora) {
        this.armadora = armadora;
    }

    public String getCarroceria() {
        return carroceria;
    }

    public void setCarroceria(String carroceria) {
        this.carroceria = carroceria;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Short getModelo() {
        return modelo;
    }

    public void setModelo(Short modelo) {
        this.modelo = modelo;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

    public String getNumeroMotor() {
        return numeroMotor;
    }

    public void setNumeroMotor(String numeroMotor) {
        this.numeroMotor = numeroMotor;
    }
    public Long getColor() {
        return color;
    }

    public void setColor(Long color) {
        this.color = color;
    }

    public Long getPrimerImpacto() {
        return primerImpacto;
    }

    public void setPrimerImpacto(Long primerImpacto) {
        this.primerImpacto = primerImpacto;
    }

    public Boolean getDanoConsecuencia() {
        return danoConsecuencia;
    }

    public void setDanoConsecuencia(Boolean danoConsecuencia) {
        this.danoConsecuencia = danoConsecuencia;
    }

    public Double getDanoEstimado() {
        return danoEstimado;
    }

    public void setDanoEstimado(Double danoEstimado) {
        this.danoEstimado = danoEstimado;
    }

    public Boolean getPerdidaTotalEvidente() {
        return perdidaTotalEvidente;
    }

    public void setPerdidaTotalEvidente(Boolean perdidaTotalEvidente) {
        this.perdidaTotalEvidente = perdidaTotalEvidente;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<PiezaVehiculo> getDanos() {
        if (danos == null){
            danos = new RealmList<>();
        }
        return danos;
    }

    public void setDanos(List<PiezaVehiculo> danos) {
        this.danos = CollectionsUtils.toRealmList(danos);
    }

    public List<PiezaVehiculo> getDanosPreexistentes() {
        if (danosPreexistentes == null){
            danosPreexistentes = new RealmList<>();
        }
        return danosPreexistentes;
    }

    public void setDanosPreexistentes(List<PiezaVehiculo> danosPreexistentes) {
        this.danosPreexistentes = CollectionsUtils.toRealmList(danosPreexistentes);
    }

    public List<Fotografia> getFotografias() {
        if (fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public Double getValorComercial() {
        return valorComercial;
    }

    public void setValorComercial(Double valorComercial) {
        this.valorComercial = valorComercial;
    }

    public VolanteVehiculoAsegurado getVolante() {
        return volante;
    }

    public void setVolante(VolanteVehiculoAsegurado volante) {
        this.volante = volante;
    }

    @Override
    public boolean isModificado() {
        return modificado;
    }

    @Override
    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    public String getNombre(){
        return (tipoVehiculo!=null?tipoVehiculo:" ")
                +" "
                +(armadora!=null?armadora:" ")
                +" "
                +(carroceria!=null?carroceria:"")
                +" "
                +(modelo!=null?modelo:"")
                +" "
                +(version!=null?version:"");
    }

    @Override
    public String toString() {
        return "VehiculoAsegurado{" +
                "vin='" + vin + '\'' +
                ", tipoVehiculo='" + tipoVehiculo + '\'' +
                ", armadora='" + armadora + '\'' +
                ", carroceria='" + carroceria + '\'' +
                ", version='" + version + '\'' +
                ", modelo=" + modelo +
                ", placas='" + placas + '\'' +
                ", numeroMotor='" + numeroMotor + '\'' +
                ", primerImpacto=" + primerImpacto +
                ", danoConsecuencia=" + danoConsecuencia +
                ", danoEstimado=" + danoEstimado +
                ", perdidaTotalEvidente=" + perdidaTotalEvidente +
                ", observacion='" + observacion + '\'' +
                ", danos=" + danos +
                ", danosPreexistentes=" + danosPreexistentes +
                ", fotografias=" + fotografias +
                ", valorComercial=" + valorComercial +
                ", volante=" + volante +
                '}';
    }
}
