package com.gnp.ajustadormovil.android.models.orquestador.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.commons.CoberturasTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volante;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MaxSize;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Modelo del volante del vehículo.
 */
public class VolanteVehiculoAsegurado extends RealmObject implements Volante, Serializable {

    /**
     * Volante condicionado.
     */
    @NotNull(message = "volanteVehiculoAsegurado.volanteCondicionado.NotNull")
    private Boolean volanteCondicionado;

    /**
     * Causa de condicionamiento.
     */
    @NotNull(message = "volanteVehiculoAsegurado.causaCondicionamiento.NotNull", when = "jexl:_this.volanteCondicionado")
    private Long causaCondicionamiento;

    /**
     * Aplica deducible.
     */
    @NotNull(message = "volanteVehiculoAsegurado.aplicaDeducible.NotNull")
    private Boolean aplicaDeducible;

    /**
     * Id de la razón del deducible.
     */
    @NotNull(message = "volanteVehiculoAsegurado.razonNoDeducible.NotNull", when = "jexl:!_this.aplicaDeducible")
    private Long razonNoDeducible;

    /**
     * Requiere grúa.
     */
    @NotNull(message = "volanteVehiculoAsegurado.requiereGrua.NotNull")
    private Boolean requiereGrua;

    /**
     * Folio de segmentación.
     */
    private String folioSegmentacion;

    /**
     * Impresión física.
     */
    @NotNull(message = "volanteVehiculoAsegurado.impresionFisica.NotNull")
    private Boolean impresionFisica;

    /**
     * Envio por correo electrónico.
     */
    @NotNull(message = "volanteVehiculoAsegurado.envioEmail.NotNull")
    private Boolean envioEmail;

    /**
     * Coberturas del volante.
     */
    @MaxSize(message = "volanteVehiculoAsegurado.coberturas.MaxSize", value = 1)
    @JsonAdapter(CoberturasTypeAdapter.class)
    private RealmList<String> coberturas;

    /**
     * Piezas faltantes.
     */
    private RealmList<PiezaVehiculo> piezasFaltantes;

    /**
     * Centro de remate.
     */
    private Long centroRemate;

    /**
     * Folio del documento.
     */
    private String folio;

    /**
     * URL del PDF del documento.
     */
    private String url;

    /**
     *Código taller
     */
    @NotBlank(message = "volanteVehiculoAsegurado.codigoTaller.NotBlank")
    private String codigoTaller;
    /**
     *Bandera de conexión online
     */
    private boolean online = false;

    public Boolean getVolanteCondicionado() {
        return volanteCondicionado;
    }

    public void setVolanteCondicionado(Boolean volanteCondicionado) {
        this.volanteCondicionado = volanteCondicionado;
    }

    public Long getCausaCondicionamiento() {
        return causaCondicionamiento;
    }

    public void setCausaCondicionamiento(Long causaCondicionamiento) {
        this.causaCondicionamiento = causaCondicionamiento;
    }

    public Boolean getAplicaDeducible() {
        return aplicaDeducible;
    }

    public void setAplicaDeducible(Boolean aplicaDeducible) {
        this.aplicaDeducible = aplicaDeducible;
    }

    public Long getRazonNoDeducible() {
        return razonNoDeducible;
    }

    public void setRazonNoDeducible(Long razonNoDeducible) {
        this.razonNoDeducible = razonNoDeducible;
    }

    public Boolean getRequiereGrua() {
        return requiereGrua;
    }

    public void setRequiereGrua(Boolean requiereGrua) {
        this.requiereGrua = requiereGrua;
    }

    public String getFolioSegmentacion() {
        return folioSegmentacion;
    }

    public void setFolioSegmentacion(String folioSegmentacion) {
        this.folioSegmentacion = folioSegmentacion;
    }

    public Boolean getImpresionFisica() {
        return impresionFisica;
    }

    public void setImpresionFisica(Boolean impresionFisica) {
        this.impresionFisica = impresionFisica;
    }

    public Boolean getEnvioEmail() {
        return envioEmail;
    }

    public void setEnvioEmail(Boolean envioEmail) {
        this.envioEmail = envioEmail;
    }

    public List<String> getCoberturas() {
        if (coberturas == null) {
            coberturas = new RealmList<>();
        }
        return coberturas;
    }

    public void setCoberturas(List<String> coberturas) {
        this.coberturas = CollectionsUtils.toRealmList(coberturas);
    }

    public List<PiezaVehiculo> getPiezasFaltantes() {
        if (piezasFaltantes == null) {
            piezasFaltantes = new RealmList<>();
        }
        return piezasFaltantes;
    }

    public void setPiezasFaltantes(List<PiezaVehiculo> piezasFaltantes) {
        this.piezasFaltantes = CollectionsUtils.toRealmList(piezasFaltantes);
    }

    public Long getCentroRemate() {
        return centroRemate;
    }

    public void setCentroRemate(Long centroRemate) {
        this.centroRemate = centroRemate;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCodigoTaller() {
        return codigoTaller;
    }

    public void setCodigoTaller(String codigoTaller) {
        this.codigoTaller = codigoTaller;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    @Override
    public String toString() {
        return "VolanteVehiculoAsegurado{" +
                "volanteCondicionado=" + volanteCondicionado +
                ", causaCondicionamiento=" + causaCondicionamiento +
                ", aplicaDeducible=" + aplicaDeducible +
                ", razonNoDeducible=" + razonNoDeducible +
                ", requiereGrua=" + requiereGrua +
                ", folioSegmentacion='" + folioSegmentacion + '\'' +
                ", impresionFisica=" + impresionFisica +
                ", envioEmail=" + envioEmail +
                ", coberturas=" + coberturas +
                ", piezasFaltantes=" + piezasFaltantes +
                ", centroRemate=" + centroRemate +
                '}';
    }
}
