package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.util.Date;
import java.util.List;

public interface Afectado extends LocalEntity, Volanteable<VolanteMedico> {

    String getNombreCompleto();

    String getNombre();

    String getApellidoPaterno();

    String getApellidoMaterno();

    String getSexo();

    Date getFechaNacimiento();

    String getTipoIdentificacion();

    String getTipoLicencia();

    String getEstadoLicencia();

    Boolean getLicenciaPermanente();

    Date getFechaFinLicencia();

    CondicionPersona getCondicion();

    Boolean getRequiereAmbulancia();

    Long getHospital();

    Long getNivelLesion();

    Boolean getAutorizaTratamiento();

    String getOtroHospital();

    String getNumeroIdentificacion();

    List<Telefono> getTelefonos();

    List<String> getCorreos();

    List<Long> getObjetosPerdidos();

    List<Long> getLesiones();

    List<Fotografia> getFotografias();

    VolanteMedico getVolante();

    String getInvolucradoId();

    void setInvolucradoId(String involucradoId);

    void setNombre(String nombre);

    void setApellidoPaterno(String apellidoPaterno);

    void setApellidoMaterno(String apellidoMaterno);

    void setSexo(String sexo);

    void setFechaNacimiento(Date fechaNacimiento);

    void setTipoIdentificacion(String tipoIdentificacion);

    void setTipoLicencia(String tipoLicencia);

    void setEstadoLicencia(String estadoLicencia);

    void setLicenciaPermanente(Boolean licenciaPermanente);

    void setFechaFinLicencia(Date fechaFinLicencia);

    void setCondicion(CondicionPersona condicion);

    void setRequiereAmbulancia(Boolean requiereAmbulancia);

    void setHospital(Long hospital);

    void setNivelLesion(Long nivelLesion);

    void setAutorizaTratamiento(Boolean autorizaTratamiento);

    void setOtroHospital(String otroHospital);

    void setNumeroIdentificacion(String numeroIdentificacion);

}
