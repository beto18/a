package com.gnp.ajustadormovil.android.models.orquestador.commons;

public class AfectadoResumen extends Resumen {

    private Boolean fotografias;

    private Boolean volante;

    public Boolean isFotografias() {
        return fotografias;
    }

    public void setFotografias(Boolean fotografias) {
        this.fotografias = fotografias;
    }

    public Boolean isVolante() {
        return volante;
    }

    public void setVolante(Boolean volante) {
        this.volante = volante;
    }
}
