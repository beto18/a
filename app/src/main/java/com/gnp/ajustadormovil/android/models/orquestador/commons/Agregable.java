package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface Agregable {

    boolean isAgregado();

    void setAgregado(boolean agregado);

}
