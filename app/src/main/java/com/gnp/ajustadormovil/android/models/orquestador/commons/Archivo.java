package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;

public class Archivo implements Serializable {

    private String url;

    public Archivo(){}

    public Archivo(String url){
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
