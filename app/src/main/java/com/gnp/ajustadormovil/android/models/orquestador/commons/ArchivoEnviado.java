package com.gnp.ajustadormovil.android.models.orquestador.commons;

public class ArchivoEnviado extends Archivo {

    public ArchivoEnviado(String url) {
        super(url);
    }

    public boolean enviado = false;

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }
}
