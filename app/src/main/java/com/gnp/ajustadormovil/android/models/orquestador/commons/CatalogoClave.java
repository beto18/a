package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;

import io.realm.RealmObject;

public class CatalogoClave extends RealmObject implements Serializable {

    /**
     * Clave.
     */
    private String clave;

    /**
     * Nombre.
     */
    private String nombre;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "CatalogoClave{" +
                "clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
