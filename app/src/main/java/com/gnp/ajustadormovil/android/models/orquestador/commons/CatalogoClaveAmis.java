package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;

import io.realm.RealmObject;

public class CatalogoClaveAmis extends RealmObject implements Serializable {

    /**
     * Clave.
     */
    private String clave;

    /**
     * Nombre.
     */
    private String nombre;

    /**
     * Clave de AMIS.
     */
    private String claveAmis;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClaveAmis() {
        return claveAmis;
    }

    public void setClaveAmis(String claveAmis) {
        this.claveAmis = claveAmis;
    }

}
