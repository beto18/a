package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;

import io.realm.RealmObject;

public class CatalogoClaveInfo extends RealmObject implements Serializable {

    private String clave;

    private String claveInfo;

    private String nombre;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClaveInfo() {
        return claveInfo;
    }

    public void setClaveInfo(String claveInfo) {
        this.claveInfo = claveInfo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "CatalogoClaveInfo{" +
                "clave='" + clave + '\'' +
                ", claveInfo='" + claveInfo + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
