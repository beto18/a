package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;

import io.realm.RealmObject;

public class CatalogoClaveInfoAmis extends RealmObject implements Serializable {

    private String clave;

    private String claveInfo;

    private String nombre;

    private String claveAmis;

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClaveInfo() {
        return claveInfo;
    }

    public void setClaveInfo(String claveInfo) {
        this.claveInfo = claveInfo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClaveAmis() {
        return claveAmis;
    }

    public void setClaveAmis(String claveAmis) {
        this.claveAmis = claveAmis;
    }

}
