package com.gnp.ajustadormovil.android.models.orquestador.commons;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import io.realm.RealmList;

public class CoberturasTypeAdapter extends TypeAdapter<RealmList<String>> {

    private static final String PROPERTY = "cobertura";

    @Override
    public void write(JsonWriter out, RealmList<String> values) throws IOException {
        if (values == null){
            out.nullValue();
        } else {
            out.beginArray();
            for(String value : values) {
                out.value(value);
            }
            out.endArray();
        }
    }

    @Override
    public RealmList<String> read(JsonReader in) throws IOException {
        JsonToken token = in.peek();
        if(token == JsonToken.NULL){
            return null;
        }
        RealmList<String> result = new RealmList<>();
        JsonArray array = new JsonParser().parse(in).getAsJsonArray();
        for(JsonElement element : array){
            if(element.isJsonPrimitive()){
                result.add(element.getAsJsonPrimitive().getAsString());
            } else {
                result.add(element
                        .getAsJsonObject()
                        .getAsJsonPrimitive(PROPERTY)
                        .getAsString());
            }
        }
        return result;
    }

}
