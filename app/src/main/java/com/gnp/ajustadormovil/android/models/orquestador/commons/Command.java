package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface Command<T> {

    T toModel();

}
