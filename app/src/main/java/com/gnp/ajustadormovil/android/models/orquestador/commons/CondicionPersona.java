package com.gnp.ajustadormovil.android.models.orquestador.commons;

/**
 * Condiciones de persona.
 */
public enum CondicionPersona {

	ILESO, FALLECIDO , LESIONADO

}
