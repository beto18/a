package com.gnp.ajustadormovil.android.models.orquestador.commons;

public class Costo {

    private Double costo;

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    @Override
    public String toString() {
        return "Costo{" +
                "costo=" + costo +
                '}';
    }
}
