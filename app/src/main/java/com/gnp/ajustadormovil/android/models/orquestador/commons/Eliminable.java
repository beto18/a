package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface Eliminable {

    boolean isEliminado();

    void setEliminado(boolean eliminado);

}
