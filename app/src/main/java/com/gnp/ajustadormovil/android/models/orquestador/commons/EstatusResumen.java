package com.gnp.ajustadormovil.android.models.orquestador.commons;

public enum EstatusResumen {

    SIN_ESTATUS,
    OK,
    ERROR,
    CARGANDO,
    PENDIENTE;

    public static EstatusResumen merge(EstatusResumen ... estatus){
        if(contains(estatus, EstatusResumen.CARGANDO)){
            return EstatusResumen.CARGANDO;
        }
        if(contains(estatus, EstatusResumen.ERROR)){
            return EstatusResumen.ERROR;
        }
        if(contains(estatus, EstatusResumen.PENDIENTE)){
            return EstatusResumen.PENDIENTE;
        }
        if(contains(estatus, EstatusResumen.OK)){
            return EstatusResumen.OK;
        }
        return EstatusResumen.SIN_ESTATUS;
    }

    private static boolean contains(EstatusResumen[] estatus, EstatusResumen e){
        for(EstatusResumen resumen : estatus){
            if(e.equals(resumen)){
                return true;
            }
        }
        return false;
    }

}
