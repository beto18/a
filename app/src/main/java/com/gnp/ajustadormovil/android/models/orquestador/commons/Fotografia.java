package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Fotografia extends RealmObject implements LocalEntity, Agregable, Eliminable, Serializable {

    /**
     * Id.
     */
    @PrimaryKey
    private Long id;

    /**
     * Local id
     */
    private Long localId;

    /**
     * Ruta del archivo.
     */
    private String url;


    /**
     * Ruta local del archivo.
     */
    private String localUrl;

    /**
     * Ruta de los servidores de AMIS.
     */
    private String amisUrl;

    private boolean agregado;

    private boolean eliminado;

    public Fotografia(){}

    public Fotografia(Long id, String url){
        this.id = id;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }

    public String getAmisUrl() {
        return amisUrl;
    }

    public void setAmisUrl(String amisUrl) {
        this.amisUrl = amisUrl;
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    @Override
    public String toString() {
        return "Fotografia{" +
                "id=" + id +
                ", url='" + url + '\'' +
                '}';
    }
}
