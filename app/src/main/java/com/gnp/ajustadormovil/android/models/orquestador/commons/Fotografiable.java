package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.util.List;

public interface Fotografiable {

    List<Fotografia> getFotografias();

    void setFotografias(List<Fotografia> fotografias);

}
