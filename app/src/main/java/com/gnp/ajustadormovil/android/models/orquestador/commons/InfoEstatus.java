package com.gnp.ajustadormovil.android.models.orquestador.commons;

import com.gnp.ajustadormovil.android.models.orquestador.asegurado.AseguradoResumen;

public class InfoEstatus {

    private Resumen declaracion;

    private AseguradoResumen asegurado;

    private AfectadoResumen tercerosAuto;

    private AfectadoResumen tercerosNoAuto;

    private AfectadoResumen tercerosPersona;

    private Resumen responsabilidad;

    private Resumen recuperacion;

    private Resumen volantes;

    private Resumen servicios;

    public Resumen getDeclaracion() {
        synchronized (this) {
            return declaracion;
        }
    }

    public void setDeclaracion(Resumen declaracion) {
        synchronized (this) {
            this.declaracion = declaracion;
        }
    }

    public AseguradoResumen getAsegurado() {
        synchronized (this) {
            return asegurado;
        }
    }

    public void setAsegurado(AseguradoResumen asegurado) {
        synchronized (this) {
            this.asegurado = asegurado;
        }
    }

    public AfectadoResumen getTercerosAuto() {
        synchronized (this) {
            return tercerosAuto;
        }
    }

    public void setTercerosAuto(AfectadoResumen tercerosAuto) {
        synchronized (this) {
            this.tercerosAuto = tercerosAuto;
        }
    }

    public AfectadoResumen getTercerosNoAuto() {
        synchronized (this) {
            return tercerosNoAuto;
        }
    }

    public void setTercerosNoAuto(AfectadoResumen tercerosNoAuto) {
        synchronized (this) {
            this.tercerosNoAuto = tercerosNoAuto;
        }
    }

    public AfectadoResumen getTercerosPersona() {
        synchronized (this) {
            return tercerosPersona;
        }
    }

    public void setTercerosPersona(AfectadoResumen tercerosPersona) {
        synchronized (this) {
            this.tercerosPersona = tercerosPersona;
        }
    }

    public Resumen getResponsabilidad() {
        synchronized (this) {
            return responsabilidad;
        }
    }

    public void setResponsabilidad(Resumen responsabilidad) {
        synchronized (this) {
            this.responsabilidad = responsabilidad;
        }
    }

    public Resumen getRecuperacion() {
        synchronized (this) {
            return recuperacion;
        }
    }

    public void setRecuperacion(Resumen recuperacion) {
        synchronized (this) {
            this.recuperacion = recuperacion;
        }
    }

    public Resumen getVolantes() {
        synchronized (this) {
            return volantes;
        }
    }

    public void setVolantes(Resumen volantes) {
        synchronized (this) {
            this.volantes = volantes;
        }
    }

    public Resumen getServicios() {
        synchronized (this) {
            return servicios;
        }
    }

    public void setServicios(Resumen servicios) {
        synchronized (this) {
            this.servicios = servicios;
        }
    }

    public boolean isComplete(){
        synchronized (this) {
            return getDeclaracion() != null
                    && getAsegurado() != null
                    && getTercerosAuto() != null
                    && getTercerosNoAuto() != null
                    && getTercerosPersona() != null
                    && getResponsabilidad() != null
                    && getRecuperacion() != null
                    && getVolantes() != null
                    && getServicios() != null;
        }
    }

}
