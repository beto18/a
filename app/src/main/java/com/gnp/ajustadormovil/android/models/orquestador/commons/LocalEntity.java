package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface LocalEntity {

    void setId(Long id);

    Long getId();

    void setLocalId(Long id);

    Long getLocalId();

}
