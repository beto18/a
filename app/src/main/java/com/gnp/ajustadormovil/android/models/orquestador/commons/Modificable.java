package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface Modificable {

    boolean isModificado();

    void setModificado(boolean modificado);

}
