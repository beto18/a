package com.gnp.ajustadormovil.android.models.orquestador.commons;

public abstract class ModuloException extends RuntimeException {

    public ModuloException(Throwable e){
        super(e);
    }

    public Object[] getParams(){ return new Object[]{}; }

}
