package com.gnp.ajustadormovil.android.models.orquestador.commons;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.Email;
import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.MinSize;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Persona extends RealmObject implements Afectado, Modificable, Fotografiable, Serializable {

    /**
     * Id persona.
     */
    @PrimaryKey
    private Long id;

    /**
     * Nombre.
     */
    @NotNull(message = "persona.nombre.NotNull")
    @MaxLength(message = "persona.nombre.Size", value = 50)
    @MatchPattern(message = "persona.nombre.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    private String nombre;

    /**
     * Apellido paterno.
     */
    @NotNull(message = "persona.apellidoPaterno.NotNull")
    @MaxLength(message = "persona.apellidoPaterno.Size", value = 30)
    @MatchPattern(message = "persona.apellidoPaterno.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    private String apellidoPaterno;

    /**
     * Apellido materno.
     */
    @MaxLength(message = "persona.apellidoMaterno.Size", value = 30)
    @MatchPattern(message = "persona.apellidoMaterno.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    private String apellidoMaterno;

    /**
     * Sexo.
     */
    @NotNull(message =  "persona.sexo.NotNull")
    @MaxLength(message = "persona.sexo.Size", value = 1)
    private String sexo;

    /**
     * Fecha de nacimiento.
     */
    @NotNull(message = "persona.fechaNacimiento.NotNull")
    @JsonAdapter(DateTypeAdapter.class)
    private Date fechaNacimiento;

    /**
     * Tipo identificacion.
     */
    @NotNull(message = "persona.tipoIdentificacion.NotNull")
    @MaxLength(message = "persona.tipoIdentificacion.Size", value = 2)
    private String tipoIdentificacion;

    /**
     * Tipo licencia.
     */
    @NotNull(when = "jexl:'LC' == _this.tipoIdentificacion", message = "persona.tipoLicencia.NotNull")
    @MaxLength(message = "persona.tipoLicencia.Size", value = 2)
    private String tipoLicencia;

    /**
     * Estado licencia.
     */
    @NotNull(when = "jexl:'LC' == _this.tipoIdentificacion", message = "persona.estadoLicencia.NotNull")
    @MaxLength(value = 2, message = "persona.estadoLicencia.Size")
    private String estadoLicencia;

    /**
     * Licencia permanente.
     */
    @NotNull(when = "jexl:'LC' == _this.tipoIdentificacion", message = "persona.licenciaPermanente.NotNull")
    private Boolean licenciaPermanente;

    /**
     * Fin licencia.
     */
    @NotNull(when = "jexl:'LC' == _this.tipoIdentificacion && !_this.licenciaPermanente", message = "persona.fechaFinLicencia.NotNull")
    @JsonAdapter(DateTypeAdapter.class)
    private Date fechaFinLicencia;

    /**
     * Estado.
     */
    private String condicion;

    /**
     * Ambulancia.
     */
    @NotNull(message = "persona.requiereAmbulancia.NotNull")
    private Boolean requiereAmbulancia;

    /**
     * Hospital.
     */
    private Long hospital;

    /**
     * Nivel de lesión.
     */
    private Long nivelLesion;

    /**
     * Bandera de autorizacion tratamiento.
     */
    @NotNull(message = "persona.autorizaTratamiento.NotNull")
    private Boolean autorizaTratamiento;

    /**
     * Otro hospital.
     */
    @MaxLength(message = "persona.otroHospital.Size", value = 50)
    private String otroHospital;

    /**
     * Número de identificación.
     */

    @NotNull(message = "persona.numIdentificacion.NotNull")
    @MatchPattern(when = "jexl:'CV' == _this.tipoIdentificacion",message = "persona.numIdentificacion.MatchPattern", pattern = "^[A-Z]{6}[0-9]{8}[0-9A-Z]{4}$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    @MaxLength(message = "persona.numIdentificacion.Size", value = 30)
    private String numeroIdentificacion;

    /**
     * Lista de teléfonos.
     */
    @NotNull(message = "persona.telefonos.NotNull")
    @MinSize(value = 1,  message = "persona.telefonos.MinSize")
    private RealmList<Telefono> telefonos;

    /**
     * Lista de correos.
     */
    @NotNull(message = "persona.correos.NotNull")
    @Email(message = "persona.correos.MatchPattern")
    @MinSize(value = 1,  message = "persona.correos.MinSize")
    private RealmList<String> correos;

    /**
     * Lista de objetos perdidos.
     */
    private RealmList<Long> objetosPerdidos;

    /**
     * Lista de lesiones.
     */
    private RealmList<Long> lesiones;

    /**
     * Fotografías.
     */
    private RealmList<Fotografia> fotografias;

    /**
     * Volante médico.
     */
    private VolanteMedico volante;

    /**
     * Involucrado Id.
     */
    private String involucradoId;

    /**
     * Código de afectado INFO.
     */
    private String codigoAfectado;

    /**
     * Si el objeto esta sincronizado a internet.
     */
    private boolean modificado = false;

    public String getNombreCompleto() {
        if (nombre == null && apellidoMaterno == null && apellidoMaterno == null) {
            return null;
        }
        return ((nombre != null ? nombre : "")  + " "
                + (apellidoPaterno !=null ? apellidoPaterno : "") + " "
                + (apellidoMaterno !=null ? apellidoMaterno : "") + " ")
                .trim();
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setLocalId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getTipoLicencia() {
        return tipoLicencia;
    }

    public void setTipoLicencia(String tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }

    public String getEstadoLicencia() {
        return estadoLicencia;
    }

    public void setEstadoLicencia(String estadoLicencia) {
        this.estadoLicencia = estadoLicencia;
    }

    public Boolean getLicenciaPermanente() {
        return licenciaPermanente;
    }

    public void setLicenciaPermanente(Boolean licenciaPermanente) {
        this.licenciaPermanente = licenciaPermanente;
    }

    public Date getFechaFinLicencia() {
        return fechaFinLicencia;
    }

    public void setFechaFinLicencia(Date fechaFinLicencia) {
        this.fechaFinLicencia = fechaFinLicencia;
    }

    public CondicionPersona getCondicion() {
        return condicion == null ? null : CondicionPersona.valueOf(condicion);
    }

    public void setCondicion(CondicionPersona condicion) {
        this.condicion = condicion == null ? null : condicion.name();
    }

    public Boolean getRequiereAmbulancia() {
        return requiereAmbulancia;
    }

    public void setRequiereAmbulancia(Boolean requiereAmbulancia) {
        this.requiereAmbulancia = requiereAmbulancia;
    }

    public Long getHospital() {
        return hospital;
    }

    public void setHospital(Long hospital) {
        this.hospital = hospital;
    }

    public Long getNivelLesion() {
        return nivelLesion;
    }

    public void setNivelLesion(Long nivelLesion) {
        this.nivelLesion = nivelLesion;
    }

    public Boolean getAutorizaTratamiento() {
        return autorizaTratamiento;
    }

    public void setAutorizaTratamiento(Boolean autorizaTratamiento) {
        this.autorizaTratamiento = autorizaTratamiento;
    }

    public String getOtroHospital() {
        return otroHospital;
    }

    public void setOtroHospital(String otroHospital) {
        this.otroHospital = otroHospital;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public List<Telefono> getTelefonos() {
        if (telefonos== null){
            telefonos = new RealmList<>();
        }
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos = CollectionsUtils.toRealmList(telefonos);
    }

    public List<String> getCorreos() {
        if (correos == null){
            correos = new RealmList<>();
        }
        return correos;
    }

    public void setCorreos(List<String> correos) {
        this.correos = CollectionsUtils.toRealmList(correos);
    }

    public List<Long> getObjetosPerdidos() {
        if (objetosPerdidos == null){
            objetosPerdidos = new RealmList<>();
        }
        return objetosPerdidos;
    }

    public void setObjetosPerdidos(List<Long> objetosPerdidos) {
        this.objetosPerdidos = CollectionsUtils.toRealmList(objetosPerdidos);
    }

    public List<Long> getLesiones() {
        if (lesiones == null){
            lesiones = new RealmList<>();
        }
        return lesiones;
    }

    public void setLesiones(List<Long> lesiones) {
        this.lesiones = CollectionsUtils.toRealmList(lesiones);
    }

    public List<Fotografia> getFotografias() {
        if(fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public VolanteMedico getVolante() {
        return volante;
    }

    public void setVolante(VolanteMedico volante) {
        this.volante = volante;
    }

    @Override
    public String getInvolucradoId() {
        return involucradoId;
    }

    public void setInvolucradoId(String involucradoId) {
        this.involucradoId = involucradoId;
    }

    public String getCodigoAfectado() {
        return codigoAfectado;
    }

    public void setCodigoAfectado(String codigoAfectado) {
        this.codigoAfectado = codigoAfectado;
    }

    @Override
    public boolean isModificado() {
        return modificado;
    }

    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "Conductor{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidoPaterno='" + apellidoPaterno + '\'' +
                ", apellidoMaterno='" + apellidoMaterno + '\'' +
                ", sexo='" + sexo + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", tipoIdentificacion='" + tipoIdentificacion + '\'' +
                ", tipoLicencia='" + tipoLicencia + '\'' +
                ", estadoLicencia='" + estadoLicencia + '\'' +
                ", licenciaPermanente=" + licenciaPermanente +
                ", fechaFinLicencia=" + fechaFinLicencia +
                ", condicion='" + condicion + '\'' +
                ", requiereAmbulancia=" + requiereAmbulancia +
                ", hospital=" + hospital +
                ", nivelLesion=" + nivelLesion +
                ", autorizaTratamiento=" + autorizaTratamiento +
                ", otroHospital='" + otroHospital + '\'' +
                ", numeroIdentificacion='" + numeroIdentificacion + '\'' +
                ", telefonos=" + telefonos +
                ", correos=" + correos +
                ", objetosPerdidos=" + objetosPerdidos +
                ", lesiones=" + lesiones +
                ", fotografias=" + fotografias +
                ", volante=" + volante +
                '}';
    }
}
