package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.io.Serializable;
import java.util.Objects;

import io.realm.RealmObject;

/**
 * Pieza faltante del vehículo.
 */
public class PiezaVehiculo extends RealmObject implements Serializable{

	/**
	 * Id de la parte afectada.
	 */
	private Long parteAfectada;

	/**
	 * Id del área afectada.
	 */
	private Long areaAfectada;

	public PiezaVehiculo(){}

	public PiezaVehiculo(Long areaAfectada, Long parteAfectada){
		this.areaAfectada = areaAfectada;
		this.parteAfectada = parteAfectada;
	}

	public Long getParteAfectada() {
		return parteAfectada;
	}

	public void setParteAfectada(Long parteAfectada) {
		this.parteAfectada = parteAfectada;
	}

	public Long getAreaAfectada() {
		return areaAfectada;
	}

	public void setAreaAfectada(Long areaAfectada) {
		this.areaAfectada = areaAfectada;
	}

	@Override
	public String toString() {
		return "PiezaVehiculo{" +
				"parteAfectada=" + parteAfectada +
				", areaAfectada=" + areaAfectada +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PiezaVehiculo)) return false;
		PiezaVehiculo that = (PiezaVehiculo) o;
		return Objects.equals(getParteAfectada(), that.getParteAfectada()) &&
				Objects.equals(getAreaAfectada(), that.getAreaAfectada());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getParteAfectada(), getAreaAfectada());
	}
}
