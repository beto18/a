package com.gnp.ajustadormovil.android.models.orquestador.commons;

public enum PosicionOcupante {

    /**
     * Asiento delantero izquierdo
     */
    ADI,
    /**
     * Asiento delantero centro
     */
    ADC,
    /**
     * Asiento delantero derecho
     */
    ADD,
    /**
     * Asiento trasero izquierdo
     */
    ATI,
    /**
     * Asiento trasero centro
     */
    ATC,
    /**
     * Asiento trasero derecho
     */
    ATD,
    /**
     * Otro
     */
    OT,
    /*
     * Otro
     */
}
