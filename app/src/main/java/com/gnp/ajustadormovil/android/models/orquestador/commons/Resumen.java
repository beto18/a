package com.gnp.ajustadormovil.android.models.orquestador.commons;

import java.util.ArrayList;
import java.util.List;

public class Resumen {

    private EstatusResumen estatus = EstatusResumen.SIN_ESTATUS;

    private List<String> errores;

    private String error;

    public EstatusResumen getEstatus() {
        return estatus;
    }

    public void setEstatus(EstatusResumen estatus) {
        this.estatus = estatus;
    }

    public void setError(String error){
        this.error = error;
    }

    public List<String> getErrores() {
        if(errores == null){
            errores = new ArrayList<>();
        }
        return errores;
    }

    public String getError(){
        if(error == null) {
            StringBuilder sb = new StringBuilder();
            for (String error : getErrores()) {
                if (sb.length() > 0) {
                    sb.append("\n\n");
                }
                sb.append("\u2022 ");
                sb.append(error);
            }
            return sb.toString();
        } else {
            return error;
        }
    }

    public boolean hasErrores(){
        return !getErrores().isEmpty() || error != null ;
    }

}
