package com.gnp.ajustadormovil.android.models.orquestador.commons;

import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Número de teléfono.
 */
public class Telefono extends RealmObject implements Serializable {

	/**
	 * Teléfono.
	 */
	@NotNull(message = "telefono.telefono.NotNull")
	@MaxLength(message = "telefono.telefono.Size", value = 15)
	@MatchPattern(message = "telefono.telefono.MatchPattern", pattern = "^[0-9]+$")
	private String telefono;

	/**
	 * Tipo de teléfono.
	 */
	@NotNull(message = "telefono.tipoTelefono.NotNull")
	@MaxLength(message = "telefono.tipoTelefono.Size", value = 1)
	private String tipo;

	/**
	 * Extensión.
	 */
	@MaxLength(message = "telefono.extension.Size",  value =  15)
	@MatchPattern(message = "telefono.extension.MatchPattern", pattern = "^[0-9]+$")
	private String extension;

	public Telefono(){}

	public Telefono(String telefono, String tipo, String extension) {
		this.telefono = telefono;
		this.tipo = tipo;
		this.extension = extension;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Override
	public String toString() {
		return "Telefono{" +
				"telefono='" + telefono + '\'' +
				", tipo='" + tipo + '\'' +
				", extension='" + extension + '\'' +
				'}';
	}
}
