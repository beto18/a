package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface VehiculoAfectado<V extends Volante> extends Volanteable<V> {

    String getVin();

    String getTipoVehiculo();

    String getArmadora();

    String getCarroceria();

    String getVersion();

    Short getModelo();

    String getPlacas();

    String getNumeroMotor();

    V getVolante();

}
