package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface Volante {

    String getUrl();

    String getFolio();

    void setUrl(String url);

    void setFolio(String folio);

}
