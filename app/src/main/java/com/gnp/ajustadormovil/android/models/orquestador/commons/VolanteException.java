package com.gnp.ajustadormovil.android.models.orquestador.commons;

public class VolanteException extends ModuloException {

    private String descripcion;

    public VolanteException(String descripcion, Throwable e) {
        super(e);
        this.descripcion = descripcion;
    }

    @Override
    public Object[] getParams() {
        return new Object[]{ descripcion };
    }
}
