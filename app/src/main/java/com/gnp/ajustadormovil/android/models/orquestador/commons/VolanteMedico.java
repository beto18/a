package com.gnp.ajustadormovil.android.models.orquestador.commons;

import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MaxSize;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class VolanteMedico extends RealmObject implements Volante, Serializable {

    /**
     * Bandera aplica deducible.
     */
    @NotNull(message = "volanteMedico.aplicaDeducible.NotNull")
    private Boolean aplicaDeducible;

    /**
     * Razón no deducible.
     */
    @NotNull(when = "jexl:!_this.aplicaDeducible", message = "volanteMedico.razonNoDeducible.NotNull")
    private Long razonNoDeducible;

    /**
     * Bandera impresión física.
     */
    @NotNull(message = "volanteMedico.impresionFisica.NotNull")
    private Boolean impresionFisica;

    /**
     * Bandera envío email;
     */
    @NotNull(message = "volanteMedico.envioEmail.NotNull")
    private Boolean envioEmail;

    /**
     * Coberturas
     */
    @MaxSize(message = "volanteMedico.coberturas.MaxSize", value = 1)
    @JsonAdapter(CoberturasTypeAdapter.class)
    private RealmList<String> coberturas;

    /**
     * Folio del volante.
     */
    private String folio;

    /**
     * Url del volante.
     */
    private String url;

    public Boolean getAplicaDeducible() {
        return aplicaDeducible;
    }

    public void setAplicaDeducible(Boolean aplicaDeducible) {
        this.aplicaDeducible = aplicaDeducible;
    }

    public Long getRazonNoDeducible() {
        return razonNoDeducible;
    }

    public void setRazonNoDeducible(Long razonNoDeducible) {
        this.razonNoDeducible = razonNoDeducible;
    }

    public Boolean getImpresionFisica() {
        return impresionFisica;
    }

    public void setImpresionFisica(Boolean impresionFisica) {
        this.impresionFisica = impresionFisica;
    }

    public Boolean getEnvioEmail() {
        return envioEmail;
    }

    public void setEnvioEmail(Boolean envioEmail) {
        this.envioEmail = envioEmail;
    }

    public List<String> getCoberturas() {
        if (coberturas == null){
            coberturas = new RealmList<>();
        }
        return coberturas;
    }

    public void setCoberturas(List<String> coberturas) {
        this.coberturas = CollectionsUtils.toRealmList(coberturas);
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "VolanteMedico{" +
                "aplicaDeducible=" + aplicaDeducible +
                ", razonNoDeducible=" + razonNoDeducible +
                ", impresionFisica=" + impresionFisica +
                ", envioEmail=" + envioEmail +
                ", coberturas=" + coberturas +
                '}';
    }

}
