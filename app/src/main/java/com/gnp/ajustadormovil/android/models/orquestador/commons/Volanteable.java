package com.gnp.ajustadormovil.android.models.orquestador.commons;

public interface Volanteable<V extends Volante> {

    V getVolante();

    void setVolante(V volante);

}
