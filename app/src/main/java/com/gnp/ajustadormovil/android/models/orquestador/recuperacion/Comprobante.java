package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.Max;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.Min;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Comprobante extends RealmObject implements Serializable {

    /**
     * Recibimos de.
     */
    @NotNull(message = "comprobante_recibimosDe_NotNull")
    @NotBlank(message = "comprobante_recibimosDe_NotBlank")
    @MaxLength(message = "comprobante_recibimosDe_Size", value = 100)
    private String recibimosDe;

    /**
     * Bandera que indica si es deducible.
     */
    @NotNull(message = "comprobante_deducible_NotNull")
    private Boolean deducible;

    /**
     * Monto acordado.
     */
    @NotNull(message = "comprobante_montoAcordado_NotNull")
    @Max(message = "comprobante_montoAcordado_Max", value = 9999999999999.99)
    @Min(message = "comprobante_montoAcordado_Min", value = 0.0)
    private Double montoAcordado;

    /**
     * Monto recibido.
     */
    @NotNull(message = "comprobante_montoRecibido_NotNull")
    @Max(message = "comprobante_montoRecibido_Max", value = 9999999999999.99)
    @Min(message = "comprobante_montoRecibido_Min", value = 0.0)
    private Double montoRecibido;

    /**
     * Fecha promesa.
     */
    @NotNull(message = "comprobante_fechaPromesa_NotNull")
    @JsonAdapter(DateTypeAdapter.class)
    private Date fechaPromesa;

    /**
     * Observaciones.
     */
    @MaxLength(message = "comprobante_observaciones_Size", value = 100)
    private String observaciones;

    /**
     * Importe total.
     */
    @NotNull(message = "comprobante_importeTotal_NotNull")
    @Max(message = "comprobante_importeTotal_Max", value = 9999999999999.99)
    @Min(message = "comprobante_importeTotal_Min", value = 0)
    private Double importeTotal;

    /**
     * Importe total con letra.
     */
    @NotBlank(message = "comprobante_importeTotalLetra_NotNull")
    @NotNull(message = "comprobante_importeTotalLetra_NotBlank")
    @MaxLength(message = "comprobante_importeTotalLetra_Size", value = 255)
    private String importeTotalLetra;

    /**
     * Código postal.
     */
    @NotBlank(message = "comprobante_codigoPostal_NotBlank")
    @NotNull(message = "comprobante_codigoPostal_NotNull")
    @MatchPattern(message = "comprobante_codigoPostal_MatchPattern", pattern = "^[0-9]+$")
    private String codigoPostal;

    /**
     * Estado.
     */
    @NotBlank(message = "comprobante_estado_NotNull")
    @NotNull(message = "comprobante_estado_NotBlank")
    private String estado;

    /**
     * Municipio.
     */
    @NotBlank(message = "comprobante_municipio_NotNull")
    @NotNull(message = "comprobante_municipio_NotBlank")
    private String municipio;

    /**
     * Colonia.
     */
    @NotBlank(message = "comprobante_colonia_NotNull")
    @NotNull(message = "comprobante_colonia_NotBlank")
    private String colonia;

    /**
     * Calle.
     */
    @NotBlank(message = "comprobante_calle_NotNull")
    @NotNull(message = "comprobante_calle_NotBlank")
    @MaxLength(message = "comprobante_calle_Size", value = 50)
    private String calle;

    /**
     * Número exterior.
     */
    @NotBlank(message = "comprobante_numeroExterior_NotNull")
    @NotNull(message = "comprobante_numeroExterior_NotBlank")
    @MaxLength(message = "comprobante_numeroExterior_Size", value = 10)
    private String numeroExterior;

    /**
     * Número interior.
     */
    @MaxLength(message = "comprobante_numeroInterior_Size", value = 10)
    private String numeroInterior;

    /**
     * Referencia.
     */
    @MaxLength(message = "comprobante_entreCalles_Size", value = 100)
    private String entreCalles;

    /**
     * Teléfono.
     */
    @NotBlank(message = "comprobante_telefono_NotNull")
    @NotNull(message = "comprobante_telefono_NotBlank")
    @MaxLength(message = "comprobante_telefono_Size", value = 20)
    private String telefono;

    /**
     * Url del comprobante.
     */
    private String url;

    /**
     * Folio del comprobante.
     */
    @PrimaryKey
    private String folio;

    private boolean agregado = false;

    public String getRecibimosDe() {
        return recibimosDe;
    }

    public void setRecibimosDe(String recibimosDe) {
        this.recibimosDe = recibimosDe;
    }

    public Boolean getDeducible() {
        return deducible;
    }

    public void setDeducible(Boolean deducible) {
        this.deducible = deducible;
    }

    public Double getMontoAcordado() {
        return montoAcordado;
    }

    public void setMontoAcordado(Double montoAcordado) {
        this.montoAcordado = montoAcordado;
    }

    public Double getMontoRecibido() {
        return montoRecibido;
    }

    public void setMontoRecibido(Double montoRecibido) {
        this.montoRecibido = montoRecibido;
    }

    public Date getFechaPromesa() {
        return fechaPromesa;
    }

    public void setFechaPromesa(Date fechaPromesa) {
        this.fechaPromesa = fechaPromesa;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Double getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(Double importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getImporteTotalLetra() {
        return importeTotalLetra;
    }

    public void setImporteTotalLetra(String importeTotalLetra) {
        this.importeTotalLetra = importeTotalLetra;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getEntreCalles() {
        return entreCalles;
    }

    public void setEntreCalles(String entreCalles) {
        this.entreCalles = entreCalles;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getDireccion(){
        return (calle !=null ?calle + " " : "")
                +(numeroExterior != null ? numeroExterior + " ":"")
                +(numeroInterior !=null ? numeroInterior : "");
    }

    @Override
    public String toString() {
        return "Comprobante{" +
                "recibimosDe='" + recibimosDe + '\'' +
                ", deducible=" + deducible +
                ", montoAcordado=" + montoAcordado +
                ", montoRecibido=" + montoRecibido +
                ", fechaPromesa=" + fechaPromesa +
                ", observaciones='" + observaciones + '\'' +
                ", importeTotal=" + importeTotal +
                ", importeTotalLetra='" + importeTotalLetra + '\'' +
                ", codigoPostal='" + codigoPostal + '\'' +
                ", estado='" + estado + '\'' +
                ", municipio='" + municipio + '\'' +
                ", colonia='" + colonia + '\'' +
                ", calle='" + calle + '\'' +
                ", numeroExterior='" + numeroExterior + '\'' +
                ", numeroInterior='" + numeroInterior + '\'' +
                ", entreCalles='" + entreCalles + '\'' +
                ", telefono='" + telefono + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
