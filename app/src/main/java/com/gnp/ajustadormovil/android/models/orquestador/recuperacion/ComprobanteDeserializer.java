package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ComprobanteDeserializer implements JsonDeserializer<Comprobante> {
    @Override
    public Comprobante deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json.isJsonNull()){
            return null;
        }
        JsonObject jsonObject = json.getAsJsonObject();
        Comprobante comprobante = context.deserialize(jsonObject, Comprobante.class);
        JsonObject ubicacion = jsonObject.getAsJsonObject("ubicacion");
        comprobante.setEstado(ubicacion.getAsJsonPrimitive("estado").getAsString());
        comprobante.setMunicipio(ubicacion.getAsJsonPrimitive("municipio").getAsString());
        comprobante.setColonia(ubicacion.getAsJsonPrimitive("colonia").getAsString());
        comprobante.setCalle(ubicacion.getAsJsonPrimitive("calle").getAsString());
        comprobante.setNumeroExterior(ubicacion.getAsJsonPrimitive("numeroExterior").getAsString());
        comprobante.setNumeroInterior(ubicacion.getAsJsonPrimitive("numeroInterior").getAsString());
        comprobante.setCodigoPostal(ubicacion.getAsJsonPrimitive("codigoPostal").getAsString());
        comprobante.setEntreCalles(ubicacion.getAsJsonPrimitive("entreCalles").getAsString());
        return comprobante;
    }
}