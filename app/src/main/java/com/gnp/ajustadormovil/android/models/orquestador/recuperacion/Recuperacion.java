package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Agregable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografiable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.LocalEntity;
import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(RecuperacionDeserializer.class)
public interface Recuperacion extends LocalEntity, Agregable, Eliminable, Fotografiable {

    Long getId();

    void setId(Long id);

    String getTipoRecuperacion();

    String getDescripcion();

}
