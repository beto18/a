package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RecuperacionCompaniaSeguros extends RealmObject implements Recuperacion, Serializable {

    public static final String TIPO = "CS";

    /**
     * Identificador.
     */
    @PrimaryKey
    private Long id;

    /**
     * Identificador Local.
     */
    private Long localId;

    /**
     * Clave de otra aseguradora.
     */
    @NotNull(message = "recCompaniaSeguros.aseguradora.NotNull")
    @NotBlank(message = "recCompaniaSeguros.aseguradora.NotBlank")
    private String aseguradora;

    /**
     * Nombre de la aseguradora.
     */
    private String nombreAseguradora = "Compañía de Seguros";

    /**
     * Otra aseguradora.
     */
    private String otraAseguradora;

    /**
     * Número de póliza.
     */
    @NotNull(message = "recCompaniaSeguros.numeroPoliza.NotNull")
    @NotBlank(message = "recCompaniaSeguros.numeroPoliza.NotBlank")
    @MaxLength(message = "recCompaniaSeguros.numeroPoliza.Size",value = 50)
    private String numeroPoliza;

    /**
     * Tipo de cobertura.
     */
    @NotNull(message = "recCompaniaSeguros.tipoCobertura.NotNull")
    @NotBlank(message = "recCompaniaSeguros.tipoCobertura.NotBlank")
    @MaxLength(message = "recCompaniaSeguros.tipoCobertura.Size",value = 50)
    private String tipoCobertura;

    /**
     * Vigencia de póliza.
     */
    @NotNull(message = "recCompaniaSeguros.vigenciaPoliza.NotNull")
    @JsonAdapter(DateTypeAdapter.class)
    private Date vigenciaPoliza;

    /**
     * Titular póliza.
     */
    @NotNull(message = "recCompaniaSeguros.titularPoliza.NotNull")
    @NotBlank(message = "recCompaniaSeguros.titularPoliza.NotBlank")
    @MaxLength(message = "recCompaniaSeguros.titularPoliza.Size",value = 100)
    private String titularPoliza;

    /**
     * Número de siniestro.
     */
    @NotNull(message = "recCompaniaSeguros.numeroSiniestro.NotNull")
    @NotBlank(message = "recCompaniaSeguros.numeroSiniestro.NotBlank")
    @MaxLength(message = "recCompaniaSeguros.numeroSiniestro.Size", value = 50)
    private String numeroSiniestro;

    /**
     * Folio de admisión.
     */
    @NotNull(message = "recCompaniaSeguros.folioAdmision.NotNull")
    @NotBlank(message = "recCompaniaSeguros.folioAdmision.NotBlank")
    @MaxLength(message = "recCompaniaSeguros.folioAdmision.Size",value = 50)
    private String folioAdmision;

    /**
     * Tipo de volante.
     */
    @NotNull(message = "recCompaniaSeguros.tipoVolante.NotNull")
    @NotBlank(message = "recCompaniaSeguros.tipoVolante.NotBlank")
    private String tipoVolante;

    /**
     * Nombre del ajustador de la otra compañía.
     */
    @NotNull(message = "recCompaniaSeguros.nombreAjustador.NotNull")
    @NotBlank(message = "recCompaniaSeguros.nombreAjustador.NotBlank")
    @MaxLength(message = "recCompaniaSeguros.nombreAjustador.Size",value = 64)
    private String nombreAjustador;

    /**
     * Estimado de daños.
     */
    @NotNull(message = "recCompaniaSeguros.danoEstimado.NotNull")
    private Double danoEstimado;

    /**
     * Número de ordenes médicas.
     */
    private Integer numeroOrdenMedicas;

    /**
     * Fotografías de recuperación.
     */
    private RealmList<Fotografia> fotografias;

    /**
     * vale digital
     */
    private String valeDigital;


    /**
     *Firma digital
     */
    private String firmaDigital;

    private boolean agregado = false;

    private boolean eliminado = false;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getTipoRecuperacion() {
        return TIPO;
    }

    @Override
    public String getDescripcion() {
        return nombreAseguradora + " - " + tipoCobertura;
    }

    public String getNombreAseguradora() {
        return nombreAseguradora;
    }

    public void setNombreAseguradora(String nombreAseguradora) {
        this.nombreAseguradora = nombreAseguradora;
    }

    @Override
    public List<Fotografia> getFotografias() {
        if(fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    @Override
    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public String getAseguradora() {
        return aseguradora;
    }

    public void setAseguradora(String aseguradora) {
        this.aseguradora = aseguradora;
    }

    public String getOtraAseguradora() {
        return otraAseguradora;
    }

    public void setOtraAseguradora(String otraAseguradora) {
        this.otraAseguradora = otraAseguradora;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(String tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public Date getVigenciaPoliza() {
        return vigenciaPoliza;
    }

    public void setVigenciaPoliza(Date vigenciaPoliza) {
        this.vigenciaPoliza = vigenciaPoliza;
    }

    public String getTitularPoliza() {
        return titularPoliza;
    }

    public void setTitularPoliza(String titularPoliza) {
        this.titularPoliza = titularPoliza;
    }

    public String getNumeroSiniestro() {
        return numeroSiniestro;
    }

    public void setNumeroSiniestro(String numeroSiniestro) {
        this.numeroSiniestro = numeroSiniestro;
    }

    public String getFolioAdmision() {
        return folioAdmision;
    }

    public void setFolioAdmision(String folioAdmision) {
        this.folioAdmision = folioAdmision;
    }

    public String getTipoVolante() {
        return tipoVolante;
    }

    public void setTipoVolante(String tipoVolante) {
        this.tipoVolante = tipoVolante;
    }

    public String getNombreAjustador() {
        return nombreAjustador;
    }

    public void setNombreAjustador(String nombreAjustador) {
        this.nombreAjustador = nombreAjustador;
    }

    public Double getDanoEstimado() {
        return danoEstimado;
    }

    public void setDanoEstimado(Double danoEstimado) {
        this.danoEstimado = danoEstimado;
    }

    public Integer getNumeroOrdenMedicas() {
        return numeroOrdenMedicas;
    }

    public void setNumeroOrdenMedicas(Integer numeroOrdenMedicas) {
        this.numeroOrdenMedicas = numeroOrdenMedicas;
    }

    public String getValeDigital() {
        return valeDigital;
    }

    public void setValeDigital(String valeDigital) {
        this.valeDigital = valeDigital;
    }

    public String getFirmaDigital() {
        return firmaDigital;
    }

    public void setFirmaDigital(String firmaDigital) {
        this.firmaDigital = firmaDigital;
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    @Override
    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

}
