package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.json.PropertyInheritanceDeserializer;

import java.util.HashMap;
import java.util.Map;

public class RecuperacionDeserializer extends PropertyInheritanceDeserializer<Recuperacion> {

    private static final String PROPERTY = "tipoRecuperacion";

    private static final Map<String, Class<? extends Recuperacion>> TIPOS = new HashMap<>();

    static {
        TIPOS.put(RecuperacionCompaniaSeguros.TIPO, RecuperacionCompaniaSeguros.class);
        TIPOS.put(RecuperacionGarantiaPrendaria.TIPO, RecuperacionGarantiaPrendaria.class);
        TIPOS.put(RecuperacionMesaControversia.TIPO, RecuperacionMesaControversia.class);
        TIPOS.put(RecuperacionMonetaria.TIPO, RecuperacionMonetaria.class);
        TIPOS.put(RecuperacionTurnadoAsistenciaLegal.TIPO, RecuperacionTurnadoAsistenciaLegal.class);
    }

    @Override
    protected String getProperty() {
        return PROPERTY;
    }

    @Override
    protected Map<String, Class<? extends Recuperacion>> getTypeMappings() {
        return TIPOS;
    }
}
