package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class RecuperacionException extends ModuloException {

    private String recuperacion;

    public RecuperacionException(String recuperacion, Throwable e) {
        super(e);
        this.recuperacion = recuperacion;
    }

    @Override
    public Object[] getParams() {
        return new Object[]{recuperacion};
    }
}
