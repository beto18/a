package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RecuperacionGarantiaPrendaria extends RealmObject implements Recuperacion, Serializable {

    public static final String TIPO = "GP";

    /**
     * Identificador.
     */
    @PrimaryKey
    private Long id;

    /**
     * Identificador Local.
     */
    private Long localId;

    /**
     * Bandera que indica si es el mismo vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.mismoVehiculo.NotNull")
    private Boolean mismoVehiculo;

    /**
     * Tipo de vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.tipoVehiculo.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.tipoVehiculo.NotBlank")
    private String tipoVehiculo;

    /**
     * Armadora del vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.armadoraVehiculo.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.armadoraVehiculo.NotBlank")
    private String armadoraVehiculo;

    /**
     * Carrocería del vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.carroceriaVehiculo.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.carroceriaVehiculo.NotBlank")
    private String carroceriaVehiculo;

    /**
     * Versión del vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.versionVehiculo.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.versionVehiculo.NotBlank")
    private String versionVehiculo;

    /**
     * Modelo del vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.modeloVehiculo.NotNull")
    private Short modeloVehiculo;

    /**
     * Placas del vehículo.
     */
    @NotBlank(message = "recGarantiaPrendaria.placasVehiculo.NotNull")
    @NotNull(message = "recGarantiaPrendaria.placasVehiculo.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.placasVehiculo.Size", value = 10)
    private String placasVehiculo;

    /**
     * VIN del vehículo.
     */
    @NotNull(message = "recGarantiaPrendaria.vinVehiculo.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.vinVehiculo.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.vinVehiculo.Size", value = 17)
    private String vinVehiculo;

    /**
     * Número del motor.
     */
    @NotNull(message = "recGarantiaPrendaria.numeroMotor.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.numeroMotor.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.numeroMotor.Size", value = 20)
    private String numeroMotor;

    /**
     * Número de factura.
     */
    @NotNull(message = "recGarantiaPrendaria.numeroFactura.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.numeroFactura.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.numeroFactura.Size", value = 20)
    private String numeroFactura;

    /**
     * De quien fue recibido.
     */
    @NotNull(message = "recGarantiaPrendaria.recibidoDe.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.recibidoDe.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.recibidoDe.Size", value = 100)
    private String recibidoDe;

    /**
     * Fecha de vencimiento.
     */
    @NotNull(message = "recGarantiaPrendaria.vencimiento.NotNull")
    @JsonAdapter(DateTypeAdapter.class)
    private Date vencimiento;

    /**
     * Número de contrato.
     */
    @NotNull(message = "recGarantiaPrendaria.numeroContrato.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.numeroContrato.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.numeroContrato.Size", value = 10)
    private String numeroContrato;

    /**
     * Folio de grúa.
     */
    @NotNull(message = "recGarantiaPrendaria.folioGrua.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.folioGrua.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.folioGrua.Size", value = 10)
    private String folioGrua;

    /**
     * Documento propiedad.
     */
    @NotNull(message = "recGarantiaPrendaria.documentoPropiedad.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.documentoPropiedad.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.documentoPropiedad.Size", value = 3)
    private String documentoPropiedad;

    /**
     * Otro documento propiedad.
     */
    @MaxLength(message = "recGarantiaPrendaria.otroDocumentoPropiedad.MaxSize", value = 100)
    private String otroDocumentoPropiedad;

    /**
     * Código postal.
     */
    @NotNull(message = "recGarantiaPrendaria.codigoPostal.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.codigoPostal.NotBlank")
    @MatchPattern(message = "recGarantiaPrendaria.codigoPostal.MatchPattern", pattern = "^[0-9]+$")
    private String codigoPostal;

    /**
     * Estado.
     */
    @NotNull(message = "recGarantiaPrendaria.estado.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.estado.NotBlank")
    private String estado;

    /**
     * Municipio.
     */
    @NotNull(message = "recGarantiaPrendaria.municipio.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.municipio.NotBlank")
    private String municipio;

    /**
     * Colonia.
     */
    @NotNull(message = "recGarantiaPrendaria.colonia.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.colonia.NotBlank")
    private String colonia;

    /**
     * Calle.
     */
    @NotNull(message = "recGarantiaPrendaria.calle.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.calle.NotNull")
    @MaxLength(message = "recGarantiaPrendaria_calle.Size",value = 30)
    private String calle;

    /**
     * Número exterior.
     */
    @NotNull(message = "recGarantiaPrendaria.numeroExterior.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.numeroExterior.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.numeroExterior.Size",value  = 5)
    private String numeroExterior;

    /**
     * Número interior.
     */
    @MaxLength(message = "recGarantiaPrendaria_numeroInterior_Size",value  = 5)
    private String numeroInterior;

    /**
     * Referencia.
     */
    @NotNull(message = "recGarantiaPrendaria.referencia.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.referencia.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.referencia.Size",value = 50)
    private String referencia;

    /**
     * Referencia.
     */
    @NotNull(message = "recGarantiaPrendaria.folioDocumento.NotNull")
    @NotBlank(message = "recGarantiaPrendaria.folioDocumento.NotBlank")
    @MaxLength(message = "recGarantiaPrendaria.folioDocumento.Size", value = 20)
    private String folioDocumento;

    /**
     * Fotografías de recuperación.
     */
    private RealmList<Fotografia> fotografias;

    private boolean agregado = false;

    private boolean eliminado = false;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getTipoRecuperacion() {
        return TIPO;
    }

    @Override
    public String getDescripcion() {
        return documentoPropiedad != null ? "Garantía Prendaria" : otroDocumentoPropiedad;
    }

    @Override
    public List<Fotografia> getFotografias() {
        if (fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    @Override
    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public Boolean getMismoVehiculo() {
        return mismoVehiculo;
    }

    public void setMismoVehiculo(Boolean mismoVehiculo) {
        this.mismoVehiculo = mismoVehiculo;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getArmadoraVehiculo() {
        return armadoraVehiculo;
    }

    public void setArmadoraVehiculo(String armadoraVehiculo) {
        this.armadoraVehiculo = armadoraVehiculo;
    }

    public String getCarroceriaVehiculo() {
        return carroceriaVehiculo;
    }

    public void setCarroceriaVehiculo(String carroceriaVehiculo) {
        this.carroceriaVehiculo = carroceriaVehiculo;
    }

    public String getVersionVehiculo() {
        return versionVehiculo;
    }

    public void setVersionVehiculo(String versionVehiculo) {
        this.versionVehiculo = versionVehiculo;
    }

    public Short getModeloVehiculo() {
        return modeloVehiculo;
    }

    public void setModeloVehiculo(Short modeloVehiculo) {
        this.modeloVehiculo = modeloVehiculo;
    }

    public String getPlacasVehiculo() {
        return placasVehiculo;
    }

    public void setPlacasVehiculo(String placasVehiculo) {
        this.placasVehiculo = placasVehiculo;
    }

    public String getVinVehiculo() {
        return vinVehiculo;
    }

    public void setVinVehiculo(String vinVehiculo) {
        this.vinVehiculo = vinVehiculo;
    }

    public String getNumeroMotor() {
        return numeroMotor;
    }

    public void setNumeroMotor(String numeroMotor) {
        this.numeroMotor = numeroMotor;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getRecibidoDe() {
        return recibidoDe;
    }

    public void setRecibidoDe(String recibidoDe) {
        this.recibidoDe = recibidoDe;
    }

    public Date getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(Date vencimiento) {
        this.vencimiento = vencimiento;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getFolioGrua() {
        return folioGrua;
    }

    public void setFolioGrua(String folioGrua) {
        this.folioGrua = folioGrua;
    }

    public String getDocumentoPropiedad() {
        return documentoPropiedad;
    }

    public void setDocumentoPropiedad(String documentoPropiedad) {
        this.documentoPropiedad = documentoPropiedad;
    }

    public String getOtroDocumentoPropiedad() {
        return otroDocumentoPropiedad;
    }

    public void setOtroDocumentoPropiedad(String otroDocumentoPropiedad) {
        this.otroDocumentoPropiedad = otroDocumentoPropiedad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getFolioDocumento() {
        return folioDocumento;
    }

    public void setFolioDocumento(String folioDocumento) {
        this.folioDocumento = folioDocumento;
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    @Override
    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    @Override
    public String toString() {
        return "RecuperacionGarantiaPrendaria{" +
                "id=" + id +
                ", mismoVehiculo=" + mismoVehiculo +
                ", tipoVehiculo='" + tipoVehiculo + '\'' +
                ", armadoraVehiculo='" + armadoraVehiculo + '\'' +
                ", carroceriaVehiculo='" + carroceriaVehiculo + '\'' +
                ", versionVehiculo='" + versionVehiculo + '\'' +
                ", modeloVehiculo=" + modeloVehiculo +
                ", placasVehiculo='" + placasVehiculo + '\'' +
                ", vinVehiculo='" + vinVehiculo + '\'' +
                ", numeroMotor='" + numeroMotor + '\'' +
                ", numeroFactura='" + numeroFactura + '\'' +
                ", recibidoDe='" + recibidoDe + '\'' +
                ", vencimiento=" + vencimiento +
                ", numeroContrato='" + numeroContrato + '\'' +
                ", folioGrua='" + folioGrua + '\'' +
                ", documentoPropiedad='" + documentoPropiedad + '\'' +
                ", otroDocumentoPropiedad='" + otroDocumentoPropiedad + '\'' +
                ", codigoPostal='" + codigoPostal + '\'' +
                ", estado='" + estado + '\'' +
                ", municipio='" + municipio + '\'' +
                ", colonia='" + colonia + '\'' +
                ", calle='" + calle + '\'' +
                ", numeroExterior='" + numeroExterior + '\'' +
                ", numeroInterior='" + numeroInterior + '\'' +
                ", referencia='" + referencia + '\'' +
                ", folioDocumento='" + folioDocumento + '\'' +
                '}';
    }
}
