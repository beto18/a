package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo de recuperación por mesa de controversia.
 *
 */
public class RecuperacionMesaControversia extends RealmObject implements Recuperacion, Modificable, Serializable {

	public static final String TIPO = "MC";

	/**
	 * Identificador.
	 */
	@PrimaryKey
	private Long id;

	/**
	 * Identificador Local.
	 */
	private Long localId;

	/**
	 * Nombre del abogado.
	 */
	@MaxLength(message = "recMesaControversia.nombreAbogado.Size", value = 70)
	private String nombreAbogado;

	/**
	 * Despacho del abogado.
	 */
	@MaxLength(message = "recMesaControversia.despachoAbogado.Size", value = 70)
	private String despachoAbogado;

	/**
	 * Observaciones.
	 */
	@NotNull(message = "recMesaControversia.observaciones.NotNull")
	@MaxLength(message = "recMesaControversia.observaciones.Size", value = 100)
	private String observaciones;

	/**
	 * Fotografías de recuperación.
	 */
	private RealmList<Fotografia> fotografias;

	private boolean agregado = false;

	private boolean modificado = false;

	private boolean eliminado = false;

	public static String getTIPO() {
		return TIPO;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getLocalId() {
		return localId;
	}

	@Override
	public void setLocalId(Long localId) {
		this.localId = localId;
	}

	@Override
	public String getTipoRecuperacion() {
		return TIPO;
	}

	@Override
	public String getDescripcion() {
		return nombreAbogado;
	}

	public String getNombreAbogado() {
		return nombreAbogado;
	}

	public void setNombreAbogado(String nombreAbogado) {
		this.nombreAbogado = nombreAbogado;
	}

	public String getDespachoAbogado() {
		return despachoAbogado;
	}

	public void setDespachoAbogado(String despachoAbogado) {
		this.despachoAbogado = despachoAbogado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public List<Fotografia> getFotografias() {
		if (fotografias == null){
			fotografias = new RealmList<>();
		}
		return fotografias;
	}

	public void setFotografias(List<Fotografia> fotografias) {
		this.fotografias = CollectionsUtils.toRealmList(fotografias);
	}

	@Override
	public boolean isAgregado() {
		return agregado;
	}

	@Override
	public void setAgregado(boolean agregado) {
		this.agregado = agregado;
	}

	public boolean isModificado() {
		return modificado;
	}

	public void setModificado(boolean modificado) {
		this.modificado = modificado;
	}

	@Override
	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}
}
