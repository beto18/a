package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.Max;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RecuperacionMonetaria extends RealmObject implements Recuperacion, Serializable {

    public static final String TIPO = "MO";

    /**
     * Identificador.
     */
    @PrimaryKey
    private Long id;

    /**
     * Identificador Local.
     */
    private Long localId;

    /**
     * Clave de la entidad financiera.
     */
    private String entidadFinanciera;

    /**
     * Monto.
     */
    @NotNull(message = "recMonetaria.monto.NotNull")
    private Double monto;

    /**
     * Número de autorización.
     */
    @MaxLength(message = "recMonetaria.numeroAutorizacion.Size", value = 10)
    private String numeroAutorizacion;

    /**
     * Folio de comprobante.
     */
    @MaxLength(message = "recMonetaria.folioComprobante.Size", value = 10)
    private String folioComprobante;

    /**
     * Clave de la moneda.
     */
    @NotNull(message = "recMonetaria.moneda.NotNull")
    @NotBlank(message = "recMonetaria.moneda.NotBlank")
    private String moneda;

    /**
     * Tipo de cambio.
     */
    private Double tipoCambio;

    /**
     * Monto calculado.
     */
    private Double montoCalculado;

    /**
     * Tipo de recuperación monetaria.
     */
    @NotNull(message = "recMonetaria.tipoRecuperacionMonetaria.NotNull")
    @Max(message = "recMonetaria.tipoRecuperacionMonetaria.Max", value = 999999999)
    private Long tipoRecuperacionMonetaria;

    /**
     * Nombre del tipo de la recuparación monetaria.
     */
    private String nombreTipoRecuperacionMonetaria = "Monetaria";

    /**
     * Tipo de recuperación monetaría por tarjeta.
     */
    private String tipoRecuperacionMonetariaTarjeta;

    /**
     * Comprobante.
     */
    @JsonAdapter(ComprobanteDeserializer.class)
    private Comprobante comprobante;

    /**
     * Fotografías de recuperación.
     */
    private RealmList<Fotografia> fotografias;

    private boolean agregado = false;

    private boolean eliminado = false;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getTipoRecuperacion() {
        return TIPO;
    }

    @Override
    public String getDescripcion() {
        return String.format("%s - $%,.2f", nombreTipoRecuperacionMonetaria, montoCalculado);
    }

    @Override
    public List<Fotografia> getFotografias() {
        if (fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    @Override
    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public String getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(String entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    public void setNumeroAutorizacion(String numeroAutorizacion) {
        this.numeroAutorizacion = numeroAutorizacion;
    }

    public String getFolioComprobante() {
        return folioComprobante;
    }

    public void setFolioComprobante(String folioComprobante) {
        this.folioComprobante = folioComprobante;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Double getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(Double tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public Double getMontoCalculado() {
        return montoCalculado;
    }

    public void setMontoCalculado(Double montoCalculado) {
        this.montoCalculado = montoCalculado;
    }

    public Long getTipoRecuperacionMonetaria() {
        return tipoRecuperacionMonetaria;
    }

    public void setTipoRecuperacionMonetaria(Long tipoRecuperacionMonetaria) {
        this.tipoRecuperacionMonetaria = tipoRecuperacionMonetaria;
    }

    public String getNombreTipoRecuperacionMonetaria() {
        return nombreTipoRecuperacionMonetaria;
    }

    public void setNombreTipoRecuperacionMonetaria(String nombreTipoRecuperacionMonetaria) {
        this.nombreTipoRecuperacionMonetaria = nombreTipoRecuperacionMonetaria;
    }

    public String getTipoRecuperacionMonetariaTarjeta() {
        return tipoRecuperacionMonetariaTarjeta;
    }

    public void setTipoRecuperacionMonetariaTarjeta(String tipoRecuperacionMonetariaTarjeta) {
        this.tipoRecuperacionMonetariaTarjeta = tipoRecuperacionMonetariaTarjeta;
    }

    public Comprobante getComprobante() {
        return comprobante;
    }

    public void setComprobante(Comprobante comprobante) {
        this.comprobante = comprobante;
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    @Override
    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    @Override
    public String toString() {
        return "RecuperacionMonetaria{" +
                "id=" + id +
                ", entidadFinanciera='" + entidadFinanciera + '\'' +
                ", monto=" + monto +
                ", numeroAutorizacion='" + numeroAutorizacion + '\'' +
                ", folioComprobante='" + folioComprobante + '\'' +
                ", moneda='" + moneda + '\'' +
                ", tipoCambio=" + tipoCambio +
                ", montoCalculado=" + montoCalculado +
                ", tipoRecuperacionMonetaria=" + tipoRecuperacionMonetaria +
                ", tipoRecuperacionMonetariaTarjeta='" + tipoRecuperacionMonetariaTarjeta + '\'' +
                ", comprobante=" + comprobante +
                ", fotografias=" + fotografias +
                '}';
    }
}
