package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RecuperacionTercero extends RealmObject {

    @PrimaryKey
    private Long tercero;

    private RealmList<RecuperacionCompaniaSeguros> companiasSeguros;

    private RealmList<RecuperacionGarantiaPrendaria> garantiasPrendarias;

    private RealmList<RecuperacionMesaControversia> mesasControversias;

    private RealmList<RecuperacionMonetaria> monetarias;

    private RealmList<RecuperacionTurnadoAsistenciaLegal> turnadoAsistenciaLegales;

    public Long getTercero() {
        return tercero;
    }

    public void setTercero(Long tercero) {
        this.tercero = tercero;
    }

    public RealmList<RecuperacionCompaniaSeguros> getCompaniasSeguros() {
        if(companiasSeguros == null){
            companiasSeguros = new RealmList<>();
        }
        return companiasSeguros;
    }

    public void setCompaniasSeguros(RealmList<RecuperacionCompaniaSeguros> companiasSeguros) {
        this.companiasSeguros = companiasSeguros;
    }

    public RealmList<RecuperacionGarantiaPrendaria> getGarantiasPrendarias() {
        if(garantiasPrendarias == null){
            garantiasPrendarias = new RealmList<>();
        }
        return garantiasPrendarias;
    }

    public void setGarantiasPrendarias(RealmList<RecuperacionGarantiaPrendaria> garantiasPrendarias) {
        this.garantiasPrendarias = garantiasPrendarias;
    }

    public RealmList<RecuperacionMesaControversia> getMesasControversias() {
        if(mesasControversias == null){
            mesasControversias = new RealmList<>();
        }
        return mesasControversias;
    }

    public void setMesasControversias(RealmList<RecuperacionMesaControversia> mesasControversias) {
        this.mesasControversias = mesasControversias;
    }

    public RealmList<RecuperacionMonetaria> getMonetarias() {
        if(monetarias == null){
            monetarias = new RealmList<>();
        }
        return monetarias;
    }

    public void setMonetarias(RealmList<RecuperacionMonetaria> monetarias) {
        this.monetarias = monetarias;
    }

    public RealmList<RecuperacionTurnadoAsistenciaLegal> getTurnadoAsistenciaLegales() {
        if(turnadoAsistenciaLegales == null){
            turnadoAsistenciaLegales = new RealmList<>();
        }
        return turnadoAsistenciaLegales;
    }

    public void setTurnadoAsistenciaLegales(RealmList<RecuperacionTurnadoAsistenciaLegal> turnadoAsistenciaLegales) {
        this.turnadoAsistenciaLegales = turnadoAsistenciaLegales;
    }
}
