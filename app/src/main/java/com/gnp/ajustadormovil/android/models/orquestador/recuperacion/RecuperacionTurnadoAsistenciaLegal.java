package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RecuperacionTurnadoAsistenciaLegal extends RealmObject implements Recuperacion, Modificable, Serializable {

    public static final String TIPO = "TA";

    /**
     * Identificador.
     */
    @PrimaryKey
    private Long id;

    /**
     * Identificador Local.
     */
    private Long localId;

    /**
     * Otra aseguradora.
     */
    @NotNull(message = "recTurnadoAsistenciaLegal.aseguradora.NotNull")
    private String aseguradora;

    /**
     * Nombre de la aseguradora.
     */
    private String nombreAseguradora = "Turnado Asistencia Legal";

    /**
     * Observaciones.
     */
    @NotNull(message = "recTurnadoAsistenciaLegal.observaciones.NotNull")
    @MaxLength(message = "recTurnadoAsistenciaLegal.observaciones.Size", value = 100)
    private String observaciones;

    /**
     * Fotografías de recuperación.
     */
    private RealmList<Fotografia> fotografias;

    private boolean agregado = false;

    private boolean modificado = false;

    private boolean eliminado = false;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getTipoRecuperacion() {
        return TIPO;
    }

    @Override
    public String getDescripcion() {
        return nombreAseguradora;
    }

    @Override
    public List<Fotografia> getFotografias() {
        if (fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    @Override
    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public String getAseguradora() {
        return aseguradora;
    }

    public void setAseguradora(String aseguradora) {
        this.aseguradora = aseguradora;
    }

    public String getNombreAseguradora() {
        return nombreAseguradora;
    }

    public void setNombreAseguradora(String nombreAseguradora) {
        this.nombreAseguradora = nombreAseguradora;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    @Override
    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isModificado() {
        return modificado;
    }

    @Override
    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    @Override
    public String toString() {
        return "RecuperacionTurnadoAsistenciaLegal{" +
                "id=" + id +
                ", aseguradora='" + aseguradora + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", fotografias=" + fotografias +
                '}';
    }
}
