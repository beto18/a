package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Recuperaciones extends RealmObject {

    @PrimaryKey
    private String ajuste;

    private RealmList<RecuperacionTercero> terceros;

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public List<RecuperacionTercero> getTerceros() {
        if(terceros == null){
            terceros = new RealmList<>();
        }
        return terceros;
    }

    public void setTerceros(List<RecuperacionTercero> terceros) {
        this.terceros = CollectionsUtils.toRealmList(terceros);
    }

}
