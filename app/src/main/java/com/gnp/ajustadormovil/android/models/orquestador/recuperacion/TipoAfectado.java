package com.gnp.ajustadormovil.android.models.orquestador.recuperacion;

public enum TipoAfectado {
	ASEGURADO, TERCERO
}
