package com.gnp.ajustadormovil.android.models.orquestador.responsabilidad;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Agregable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografiable;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo de Responsabilidad del ajuste.
 */
public class Responsabilidad extends RealmObject implements Agregable, Eliminable, Fotografiable, Serializable {

	/**
	 * Id del Ajuste
	 */
	@PrimaryKey
	private String ajuste;

	/**
	 * Clave responsabilidad asignado por la guía.
	 */
	@NotNull(message = "responsabilidad.responsabilidadGuia.NotNull", when = "jexl:'TA' == _this.tipoTercero")
	private String responsabilidadGuia;

	/**
	 * Clave responsabilidad asignada por el ajustador.
	 */
	@NotNull(message = "responsabilidad.responsabilidadAjustador.NotNull")
	private String responsabilidadAjustador;

	/**
	 * Circunstancia del vehiculo A.
	 */
	@NotNull(message = "responsabilidad.circunstanciaA.NotNull", when = "jexl:'TA' == _this.tipoTercero")
	private Long circunstanciaA;

	/**
	 * Circunstancia del vehículo B.
	 */
	@NotNull(message = "responsabilidad.circunstanciaB.NotNull", when = "jexl:'TA' ==_this.tipoTercero")
	private Long circunstanciaB;

	/**
	 * Bandera asegurado responsable.
	 */
	@NotNull(message = "responsabilidad.aseguradoResponsable.NotNull")
	private Boolean aseguradoResponsable;

	/**
	 * Bandera de tercero responsable.
	 */
	@NotNull(message  = "responsabilidad.terceroResponsable.NotNull")
	private Boolean terceroResponsable;

	/**
	 * Tipo Tercero.
	 */
	@NotNull(message ="responsabilidad.tipoTercero.NotNull" )
	private String tipoTercero;


	/**
	 * Responsabilidad.
	 */
	private RealmList<Fotografia> fotografias;

	private boolean agregado = false;

	private boolean eliminado = false;

	public String getAjuste() {
		return ajuste;
	}

	public void setAjuste(String ajuste) {
		this.ajuste = ajuste;
	}

	public String getResponsabilidadGuia() {
		return responsabilidadGuia;
	}

	public void setResponsabilidadGuia(String responsabilidadGuia) {
		this.responsabilidadGuia = responsabilidadGuia;
	}

	public String getResponsabilidadAjustador() {
		return responsabilidadAjustador;
	}

	public void setResponsabilidadAjustador(String responsabilidadAjustador) {
		this.responsabilidadAjustador = responsabilidadAjustador;
	}

	public Long getCircunstanciaA() {
		return circunstanciaA;
	}

	public void setCircunstanciaA(Long circunstanciaA) {
		this.circunstanciaA = circunstanciaA;
	}

	public Long getCircunstanciaB() {
		return circunstanciaB;
	}

	public void setCircunstanciaB(Long circunstanciaB) {
		this.circunstanciaB = circunstanciaB;
	}

	public Boolean getAseguradoResponsable() {
		return aseguradoResponsable;
	}

	public void setAseguradoResponsable(Boolean aseguradoResponsable) {
		this.aseguradoResponsable = aseguradoResponsable;
	}

	public Boolean getTerceroResponsable() {
		return terceroResponsable;
	}

	public void setTerceroResponsable(Boolean terceroResponsable) {
		this.terceroResponsable = terceroResponsable;
	}

	public String getTipoTercero() {
		return tipoTercero;
	}

	public void setTipoTercero(String tipoTercero) {
		this.tipoTercero = tipoTercero;
	}



	public List<Fotografia> getFotografias() {
		if (fotografias == null){
			fotografias = new RealmList<>();
		}
		return fotografias;
	}

	public void setFotografias(List<Fotografia> fotografias) {
		this.fotografias = CollectionsUtils.toRealmList(fotografias);
	}

	@Override
	public boolean isAgregado() {
		return agregado;
	}

	@Override
	public void setAgregado(boolean agregado) {
		this.agregado = agregado;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	@Override
	public String toString() {
		return "Responsabilidad{" +
				"ajuste='" + ajuste + '\'' +
				", responsabilidadGuia='" + responsabilidadGuia + '\'' +
				", responsabilidadAjustador='" + responsabilidadAjustador + '\'' +
				", circunstanciaA=" + circunstanciaA +
				", circunstanciaB=" + circunstanciaB +
				", aseguradoResponsable=" + aseguradoResponsable +
				", terceroResponsable=" + terceroResponsable +
				", tipoTercero='" + tipoTercero + '\'' +
				", fotografias=" + fotografias +
				'}';
	}
}
