package com.gnp.ajustadormovil.android.models.orquestador.responsabilidad;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Command;

import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;

/**
 * Entidad que contiene la información necesaria para crear una entidad de
 * {@link Responsabilidad}.
 */
public class ResponsabilidadCommand implements Command<Responsabilidad>, Serializable {

	/**
	 * Circunstancia del vehículo A.
	 */
	private Long circunstanciaA;

	/**
	 * Circunstancia del vehículo B.
	 */
	private Long circunstanciaB;

	/**
	 * Clave de responsabilidad obtenida por el ajustador.
	 */
	@NotNull(message = "responsabilidadCommand_responsabilidadAjustador_NotNull")
	@NotBlank(message = "responsabilidadCommand_responsabilidadAjustador_NotBlank")
	@MaxLength(message = "responsabilidadCommand_responsabilidadAjustador_Size", value = 2)
	private String responsabilidadAjustador;

	/**
	 * Clave tipo tercero.
	 */
	@NotNull(message = "responsabilidadCommand_tipoTercero_NotNull")
	@NotBlank(message = "responsabilidadCommand_tipoTercero_NotBlank")
	@MaxLength(message = "responsabilidadCommand_tipoTercero_Size", value = 3)
	private String tipoTercero;

	/**
	 * Id del tercero.
	 */
	private Long tercero;

	public Long getCircunstanciaA() {
		return circunstanciaA;
	}

	public void setCircunstanciaA(Long circunstanciaA) {
		this.circunstanciaA = circunstanciaA;
	}

	public Long getCircunstanciaB() {
		return circunstanciaB;
	}

	public void setCircunstanciaB(Long circunstanciaB) {
		this.circunstanciaB = circunstanciaB;
	}

	public String getResponsabilidadAjustador() {
		return responsabilidadAjustador;
	}

	public void setResponsabilidadAjustador(String responsabilidadAjustador) {
		this.responsabilidadAjustador = responsabilidadAjustador;
	}

	public String getTipoTercero() {
		return tipoTercero;
	}

	public void setTipoTercero(String tipoTercero) {
		this.tipoTercero = tipoTercero;
	}

	public Long getTercero() {
		return tercero;
	}

	public void setTercero(Long tercero) {
		this.tercero = tercero;
	}

	@Override
	public String toString() {
		return "ResponsabilidadCommand{" +
				"circunstanciaA=" + circunstanciaA +
				", circunstanciaB=" + circunstanciaB +
				", responsabilidadAjustador='" + responsabilidadAjustador + '\'' +
				", tipoTercero='" + tipoTercero + '\'' +
				", tercero=" + tercero +
				'}';
	}

	/**
	 * Crea una entidad {@link Responsabilidad}.
	 */
	public Responsabilidad toModel() {
		Responsabilidad responsabilidad = new Responsabilidad();
		responsabilidad.setCircunstanciaA(circunstanciaA);
		responsabilidad.setCircunstanciaB(circunstanciaB);
		responsabilidad.setResponsabilidadAjustador(responsabilidadAjustador);
		responsabilidad.setTipoTercero(tipoTercero);
		return responsabilidad;
	}

}
