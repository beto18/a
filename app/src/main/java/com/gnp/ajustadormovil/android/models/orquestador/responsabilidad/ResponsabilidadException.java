package com.gnp.ajustadormovil.android.models.orquestador.responsabilidad;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class ResponsabilidadException extends ModuloException {

    public ResponsabilidadException(Throwable e) {
        super(e);
    }
}
