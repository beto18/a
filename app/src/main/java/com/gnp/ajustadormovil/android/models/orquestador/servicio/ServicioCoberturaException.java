package com.gnp.ajustadormovil.android.models.orquestador.servicio;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class ServicioCoberturaException extends ModuloException {

    public ServicioCoberturaException(Throwable e) {
        super(e);
    }
}
