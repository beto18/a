package com.gnp.ajustadormovil.android.models.orquestador.servicio;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ServiciosCobertura extends RealmObject implements Serializable, Modificable {

    @PrimaryKey
    private String ajuste;

    private boolean modificado = false;

    private RealmList<String> servicios;

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public List<String> getServicios() {
        if (servicios== null){
            servicios = new RealmList<>();
        }
        return servicios;
    }

    public void setServicios(List<String> servicios) {
        this.servicios = CollectionsUtils.toRealmList(servicios);
    }

    @Override
    public boolean isModificado() {
        return modificado;
    }

    @Override
    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (servicios != null) {
            sb.append("[");
            for (String servicio : servicios) {
                sb.append(servicio);
            }
            sb.append("]");
        }
        return "ServiciosCobertura{" +
                "ajuste='" + ajuste + '\'' +
                ", servicios=" + sb.toString() +
                '}';
    }
}
