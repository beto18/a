package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.Email;
import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.MinSize;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Afectado extends RealmObject implements Serializable {

    /**
     * Nombre de la persona fisica / moral.
     */
    @NotNull(message = "afectado.nombre.NotNull")
    @NotBlank(message = "afectado.nombre.NotBlank")
    @MaxLength(message = "afectado.nombre.Size", value = 50)
    @MatchPattern(message = "afectado.nombre.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    private String nombre;

    /**
     * Tipo persona.
     */
    @NotNull(message = "afectado.tipoPersona.NotNull")
    @NotBlank(message = "afectado.tipoPersona.NotBlank")
    private String tipoPersona;

    /**
     * RFC.
     */
    @MatchPattern(message = "afectado.rfc.MatchPattern", pattern = "(^[A-Z]{3}[0-9]{6}[0-9A-Z]{3}$)|(^[A-Z]{4}[0-9]{6}[0-9A-Z]{3}$)")
    private String rfc;

    /**
     * Apellido Paterno.
     */
    @NotNull(when = "jexl:'F' == _this.tipoPersona",message = "afectado.apellidoPaterno.NotNull")
    @MaxLength(message = "afectado.apellidoPaterno.Size", value = 30)
    @MatchPattern(message = "afectado.apellidoPaterno.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    private String apellidoPaterno;

    /**
     * Apellido Materno.
     */
    @NotNull(when = "jexl:'F' == _this.tipoPersona", message = "afectado.apellidoMaterno.NotNull")
    @MaxLength(message = "afectado.apellidoMaterno.Size", value = 30)
    @MatchPattern(message = "afectado.apellidoMaterno.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    private String apellidoMaterno;

    /**
     * Género.
     */
    @NotNull(when = "jexl:'F' == _this.tipoPersona", message = "afectado.genero.NotNull")
    @MaxLength(message = "afectado.genero.Size", value = 1)
    private String genero;
    /**
     * Fecha de nacimiento.
     */
    @NotNull(when = "jexl:'F' == _this.tipoPersona", message = "persona.fechaNacimiento.NotNull")
    @JsonAdapter(DateTypeAdapter.class)
    private Date fechaNacimiento;

    /**
     * Telefonos.
     */
    @NotNull(message = "afectado.telefonos.NotNull")
    @MinSize(value = 1,  message = "afectado.telefonos.MinSize")
    private RealmList<Telefono> telefonos;

    /**
     * Correos.
     */

    @NotNull(message = "afectado.correos.NotNull")
    @MinSize(value = 1,  message = "afectado.correos.MinSize")
    @Email(message = "afectado.correos.MatchPattern")
    private RealmList<String> correos;



    public String getNombreCompleto() {
        StringBuilder nombreCompleto = new StringBuilder();
        nombreCompleto.append(nombre != null ? nombre : "");
        if (apellidoPaterno != null) {
            nombreCompleto.append(" ");
            nombreCompleto.append(apellidoPaterno);
        }
        if (apellidoMaterno != null) {
            nombreCompleto.append(" ");
            nombreCompleto.append(apellidoMaterno);
        }
        return nombreCompleto.toString();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public List<Telefono> getTelefonos() {
        if (telefonos == null){
            telefonos = new RealmList<>();
        }
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos = CollectionsUtils.toRealmList(telefonos);
    }

    public List<String> getCorreos() {
        if (correos == null){
            correos = new RealmList<>();
        }
        return correos;
    }

    public void setCorreos(List<String> correos) {
        this.correos = CollectionsUtils.toRealmList(correos);
    }

    @Override
    public String toString() {
        return "Afectado{" +
                "nombre='" + nombre + '\'' +
                ", tipoPersona='" + tipoPersona + '\'' +
                ", rfc='" + rfc + '\'' +
                ", apellidoPaterno='" + apellidoPaterno + '\'' +
                ", apellidoMaterno='" + apellidoMaterno + '\'' +
                ", genero='" + genero + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", telefonos=" + telefonos +
                ", correos=" + correos +
                '}';
    }
}
