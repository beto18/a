package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.LocalEntity;
import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(TerceroDeserializer.class)
public interface Tercero extends LocalEntity, Eliminable {

    Long getId();

    void setId(Long id);

    String getTipo();

    String getDescripcion();

    void setDescripcion(String descripcion);
}
