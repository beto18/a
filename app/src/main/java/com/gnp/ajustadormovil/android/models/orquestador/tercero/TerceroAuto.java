package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Agregable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo del tercero auto.
 *
 */
public class TerceroAuto extends RealmObject implements Tercero, Agregable, Eliminable, Serializable {

	/**
	 * Identificador del tipo de Tercero.
	 */
	public static final String TIPO = "TA";

	/**
	 * Identificador del tercero.
	 */
	@PrimaryKey
	private Long id;


	/**
	 * Id tercero local.
	 */
	private Long localId;

	/**
	 * Descripción del tercero.
	 */
	private String descripcion;

	/**
	 * Vehículo.
	 */
	private VehiculoTercero vehiculo;

	/**
	 * Conductor.
	 */
	private Persona conductor;

	/**
	 * Ocupantes.
	 */
	private RealmList<Ocupante> ocupantes;

	/**
	 * Folio de AMIS.
	 */
	private String folioAmis;

	private boolean agregado = false;

	private boolean eliminado = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLocalId() {
		return localId;
	}

	public void setLocalId(Long localId) {
		this.localId = localId;
	}

	@Override
	public String getDescripcion() {
		return descripcion;
	}

	@Override
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public VehiculoTercero getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(VehiculoTercero vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Persona getConductor() {
		return conductor;
	}

	public void setConductor(Persona conductor) {
		this.conductor = conductor;
	}

	public List<Ocupante> getOcupantes() {
		if (ocupantes == null){
			ocupantes = new RealmList<>();
		}
		return ocupantes;
	}

	public void setOcupantes(List<Ocupante> ocupantes) {
		this.ocupantes = CollectionsUtils.toRealmList(ocupantes);
	}

	@Override
	public boolean isAgregado() {
		return agregado;
	}

	public void setAgregado(boolean agregado) {
		this.agregado = agregado;
	}

	@Override
	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	@Override
	public String getTipo(){
		return TIPO;
	}

	public String getFolioAmis() {
		return folioAmis;
	}

	public void setFolioAmis(String folioAmis) {
		this.folioAmis = folioAmis;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(ocupantes != null) {
			sb.append("[");
			for (Ocupante ocupante : ocupantes) {
				sb.append("{");
				sb.append(ocupante);
				sb.append("}");
			}
			sb.append("]");
		}
		return "TerceroAuto{" +
				"id='" + id + '\'' +
				"descripcion='" + descripcion + '\'' +
				", vehiculo=" + vehiculo +
				", conductor=" + conductor +
				", ocupantes=" + sb.toString() +
				'}';
	}
}
