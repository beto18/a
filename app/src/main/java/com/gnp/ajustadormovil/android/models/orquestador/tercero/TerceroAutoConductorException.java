package com.gnp.ajustadormovil.android.models.orquestador.tercero;

public class TerceroAutoConductorException extends TerceroAutoException {


    public TerceroAutoConductorException(String descripcion, Throwable e){
        super(descripcion, e);
    }
}
