package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class TerceroAutoException extends ModuloException {

    private String descripcion;

    public TerceroAutoException(String descripcion, Throwable e) {
        super(e);
        this.descripcion = descripcion;
    }

    @Override
    public Object[] getParams() {
        return new Object[]{descripcion};
    }
}
