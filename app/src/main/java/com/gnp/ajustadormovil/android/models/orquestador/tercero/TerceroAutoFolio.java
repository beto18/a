package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import net.sf.oval.constraint.MaxLength;

public class TerceroAutoFolio {

    @MaxLength(message = "terceroAutoFolio.folioAmis.MaxLenth", value =  30)
    private String folioAmis;

    public TerceroAutoFolio(String folioAmis) {
        this.folioAmis = folioAmis;
    }

    public TerceroAutoFolio() { }

    public String getFolioAmis() {
        return folioAmis;
    }

    public void setFolioAmis(String folioAmis) {
        this.folioAmis = folioAmis;
    }

}
