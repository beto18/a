package com.gnp.ajustadormovil.android.models.orquestador.tercero;

public class TerceroAutoOcupanteException extends TerceroAutoException {

    private String ocupante;

    private String descripcion;

    public TerceroAutoOcupanteException(String descripcion, String ocupante, Throwable e){
        super(descripcion, e);
        this.descripcion = descripcion;
        this.ocupante = ocupante;
    }

    @Override
    public Object[] getParams() {
        return new Object[] { descripcion, ocupante };
    }

}
