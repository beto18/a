package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.AfectadoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Resumen;

import java.util.List;

public class TerceroAutoResumen extends Resumen {

    private AfectadoResumen conductor = new AfectadoResumen();

    private AfectadoResumen vehiculo = new AfectadoResumen();

    private AfectadoResumen ocupantes = new AfectadoResumen();

    public AfectadoResumen getConductor() {
        return conductor;
    }

    public void setConductor(AfectadoResumen conductor) {
        this.conductor = conductor;
    }

    public AfectadoResumen getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(AfectadoResumen vehiculo) {
        this.vehiculo = vehiculo;
    }

    public AfectadoResumen getOcupantes() {
        return ocupantes;
    }

    public void setOcupantes(AfectadoResumen ocupantes) {
        this.ocupantes = ocupantes;
    }

    @Override
    public List<String> getErrores() {
        return super.getErrores();
    }

}
