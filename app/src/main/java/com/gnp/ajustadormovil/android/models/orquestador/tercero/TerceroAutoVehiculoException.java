package com.gnp.ajustadormovil.android.models.orquestador.tercero;

public class TerceroAutoVehiculoException extends TerceroAutoException {

    public TerceroAutoVehiculoException(String descripcion, Throwable e){
        super(descripcion, e);
    }
}
