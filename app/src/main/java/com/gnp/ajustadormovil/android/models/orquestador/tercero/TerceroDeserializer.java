package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.json.PropertyInheritanceDeserializer;

import java.util.HashMap;
import java.util.Map;

public class TerceroDeserializer extends PropertyInheritanceDeserializer<Tercero> {

    private static final String PROPERTY = "tipoTercero";

    private static final Map<String, Class<? extends Tercero>> TERCEROS = new HashMap<>();

    static {
        TERCEROS.put(TerceroAuto.TIPO, TerceroAuto.class);
        TERCEROS.put(TerceroPersona.TIPO, TerceroPersona.class);
        TERCEROS.put(TerceroNoAuto.TIPO, TerceroNoAuto.class);
    }

    @Override
    protected String getProperty() {
        return PROPERTY;
    }

    @Override
    protected Map<String, Class<? extends Tercero>> getTypeMappings() {
        return TERCEROS;
    }
}
