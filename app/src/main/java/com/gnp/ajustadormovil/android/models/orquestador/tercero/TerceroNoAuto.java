package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Agregable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografiable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volanteable;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;
import net.sf.oval.constraint.Size;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TerceroNoAuto extends RealmObject implements Tercero, Agregable, Modificable, Eliminable, Fotografiable, Volanteable<VolanteTerceroNoAuto>, Serializable {

    public static final String TIPO = "TNA";

    /**
     * Identificador del tercero.
     */
    @PrimaryKey
    private Long id;

    /**
     * Local Id.
     */
    private Long localId;

    /**
     * Descripción del tercero.
     */
    private String descripcion;

    private boolean agregado = false;

    private boolean modificado = false;

    private boolean eliminado = false;

    /**
     * Categoria No Auto
     */
    @NotNull(message = "terceroNoAuto.categoriaNoAuto.NotNull")
    private Long categoriaNoAuto;

    /**
     * Id tipo no auto.
     */
    @NotNull(message = "terceroNoAuto.tipoNoAuto.NotNull")
    private Long tipoNoAuto;

    /**
     * Area dañada.
     */
    @NotNull(message = "terceroNoAuto.areaDanada.NotNull")
    @NotBlank(message = "terceroNoAuto.areaDanada.NotBlank")
    @Size(message = "terceroNoAuto.areaDanada.Size", min = 1, max = 100)
    private String areaDanada;

    /**
     * Daño preexistente.
     */
    @NotNull(message = "terceroNoAuto.danoPreexistente.NotNull")
    @NotBlank(message = "terceroNoAuto.danoPreexistente.NotBlank")
    @Size(message = "terceroNoAuto.danoPreexistente.Size", min = 1, max = 100)
    private String danoPreexistente;

    /**
     * Bandera reclamo.
     */
    @NotNull(message = "terceroNoAuto.reclamo.NotNull")
    private Boolean reclamo;

    /**
     * Id de la persona.
     */
    @NotNull(when = "jexl:_this.reclamo", message = "terceroNoAuto.afectado.NotNull")
    private Afectado afectado;

    /**
     * Ubicaciones.
     */
    private String tipoUbicacion;

    /**
     * Calle.
     */
    @Size(message = "terceroNoAuto.calle.Size", min = 1, max = 70)
    private String calle;

    /**
     * Código Postal.
     */
    private String codigoPostal;

    /**
     * Colonia.
     */
    private String colonia;

    /**
     * Municipio.
     */
    private String municipio;

    /**
     * Estado.
     */
    private String estado;

    /**
     * Número Interior.
     */
    @Size(message = "terceroNoAuto.numeroInterior.Size",  max = 15)
    private String numeroInterior;

    /**
     * Númeero Exterior.
     */
    @Size(message = "terceroNoAuto.numeroExterior.Size", min = 1, max = 15)
    private String numeroExterior;

    /**
     * Entre Calles.
     */
    @Size(message = "terceroNoAuto.entreCalles.Size", min = 1, max = 50)
    private String entreCalles;

    /**
     * Referencias.
     */
    @Size(message = "terceroNoAuto.referencias.Size", min = 1, max = 70)
    private String referencias;

    /**
     * Kilometro.
     */
    @Size(message = "terceroNoAuto.kilometro.Size", min = 1, max = 15)
    private String kilometro;

    /**
     * Fotografías.
     */
    private RealmList<Fotografia> fotografias;

    /**
     * Volante.
     */
    private VolanteTerceroNoAuto volante;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getCategoriaNoAuto() {
        return categoriaNoAuto;
    }

    public void setCategoriaNoAuto(Long categoriaNoAuto) {
        this.categoriaNoAuto = categoriaNoAuto;
    }

    public Long getTipoNoAuto() {
        return tipoNoAuto;
    }

    public void setTipoNoAuto(Long tipoNoAuto) {
        this.tipoNoAuto = tipoNoAuto;
    }

    public String getAreaDanada() {
        return areaDanada;
    }

    public void setAreaDanada(String areaDanada) {
        this.areaDanada = areaDanada;
    }

    public String getDanoPreexistente() {
        return danoPreexistente;
    }

    public void setDanoPreexistente(String danoPreexistente) {
        this.danoPreexistente = danoPreexistente;
    }

    public Boolean getReclamo() {
        return reclamo;
    }

    public void setReclamo(Boolean reclamo) {
        this.reclamo = reclamo;
    }

    public Afectado getAfectado() {
        return afectado;
    }

    public void setAfectado(Afectado afectado) {
        this.afectado = afectado;
    }

    public String getTipoUbicacion() {
        return tipoUbicacion;
    }

    public void setTipoUbicacion(String tipoUbicacion) {
        this.tipoUbicacion = tipoUbicacion;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getEntreCalles() {
        return entreCalles;
    }

    public void setEntreCalles(String entreCalles) {
        this.entreCalles = entreCalles;
    }

    public String getReferencias() {
        return referencias;
    }

    public void setReferencias(String referencias) {
        this.referencias = referencias;
    }

    public String getKilometro() {
        return kilometro;
    }

    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }

    public List<Fotografia> getFotografias() {
        if (fotografias == null){
            fotografias = new RealmList<>();
        }
        return fotografias;
    }

    public void setFotografias(List<Fotografia> fotografias) {
        this.fotografias = CollectionsUtils.toRealmList(fotografias);
    }

    public VolanteTerceroNoAuto getVolante() {
        return volante;
    }

    public void setVolante(VolanteTerceroNoAuto volante) {
        this.volante = volante;
    }

    @Override
    public String getTipo(){
        return TIPO;
    }

    public String getNombreCompleto(){
        return (areaDanada !=null ? areaDanada : "")
                + " "
                + (danoPreexistente !=null ? danoPreexistente : "");
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isModificado() {
        return modificado;
    }

    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    @Override
    public String toString() {
        return "TerceroNoAuto{" +
                "id=" + id +
                ", descripcion='" + descripcion + '\'' +
                ", categoriaNoAuto=" + categoriaNoAuto +
                ", tipoNoAuto=" + tipoNoAuto +
                ", areaDanada='" + areaDanada + '\'' +
                ", danoPreexistente='" + danoPreexistente + '\'' +
                ", reclamo=" + reclamo +
                ", afectado=" + afectado +
                ", tipoUbicacion='" + tipoUbicacion + '\'' +
                ", calle='" + calle + '\'' +
                ", codigoPostal='" + codigoPostal + '\'' +
                ", colonia='" + colonia + '\'' +
                ", municipio='" + municipio + '\'' +
                ", estado='" + estado + '\'' +
                ", numeroInterior='" + numeroInterior + '\'' +
                ", numeroExterior='" + numeroExterior + '\'' +
                ", entreCalles='" + entreCalles + '\'' +
                ", referencias='" + referencias + '\'' +
                ", kilometro='" + kilometro + '\'' +
                ", fotografias=" + fotografias +
                ", volante=" + volante +
                '}';
    }
}
