package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class TerceroNoAutoException extends ModuloException {

    private String tercero;

    public TerceroNoAutoException(String tercero, Throwable e) {
        super(e);
        this.tercero = tercero;
    }

    @Override
    public Object[] getParams() {
        return new Object[]{tercero};
    }
}
