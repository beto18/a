package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.Agregable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Eliminable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Modificable;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TerceroPersona extends RealmObject implements Tercero, Agregable, Modificable, Eliminable, Serializable {

    public static final String TIPO = "TP";

    /**
     * Identificador del tercero.
     */
    @PrimaryKey
    private Long id;

    /**
     * Identificador local del tercero.
     */
    private Long localId;

    /**
     * Descripción del tercero.
     */
    private String descripcion;

    /**
     * Persona tercero.
     */
    private Persona persona;

    private boolean agregado = false;

    private boolean modificado = false;

    private boolean eliminado = false;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getLocalId() {
        return localId;
    }

    @Override
    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    @Override
    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public String getTipo(){
        return TIPO;
    }

    @Override
    public boolean isAgregado() {
        return agregado;
    }

    public void setAgregado(boolean agregado) {
        this.agregado = agregado;
    }

    @Override
    public boolean isModificado() {
        return modificado;
    }

    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public boolean isEliminado() {
        return eliminado;
    }

    public void setEliminado(boolean eliminado) {
        this.eliminado = eliminado;
    }

    @Override
    public String toString() {
        return "TerceroPersona{" +
                "id=" + id +
                ", descripcion='" + descripcion + '\'' +
                ", persona=" + persona +
                '}';
    }
}
