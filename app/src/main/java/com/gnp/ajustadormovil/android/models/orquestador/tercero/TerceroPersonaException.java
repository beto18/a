package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;

public class TerceroPersonaException extends ModuloException {

    private String tercero;

    public TerceroPersonaException(String tercero, Throwable e) {
        super(e);
        this.tercero = tercero;
    }

    @Override
    public Object[] getParams() {
        return new Object[]{tercero};
    }
}
