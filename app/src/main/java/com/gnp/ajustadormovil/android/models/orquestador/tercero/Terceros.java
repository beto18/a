package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Terceros extends RealmObject implements Serializable {

    @PrimaryKey
    private String ajuste;

    private RealmList<TerceroAuto> autos;

    private RealmList<TerceroNoAuto> noAutos;

    private RealmList<TerceroPersona> personas;

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public List<TerceroAuto> getAutos() {
        if(autos == null){
            autos = new RealmList<>();
        }
        return autos;
    }

    public void setAutos(List<TerceroAuto> autos) {
        this.autos = CollectionsUtils.toRealmList(autos);
    }

    public List<TerceroNoAuto> getNoAutos() {
        if(noAutos == null){
            noAutos = new RealmList<>();
        }
        return noAutos;
    }

    public void setNoAutos(List<TerceroNoAuto> noAutos) {
        this.noAutos = CollectionsUtils.toRealmList(noAutos);
    }

    public List<TerceroPersona> getPersonas() {
        if(personas == null){
            personas = new RealmList<>();
        }
        return personas;
    }

    public void setPersonas(List<TerceroPersona> personas) {
        this.personas = CollectionsUtils.toRealmList(personas);
    }
}
