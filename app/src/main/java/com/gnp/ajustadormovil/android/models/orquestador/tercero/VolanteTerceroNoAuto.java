package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.orquestador.commons.CoberturasTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volante;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.MaxSize;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;

import io.realm.RealmList;
import io.realm.RealmObject;

public class VolanteTerceroNoAuto extends RealmObject implements Volante, Serializable {

    /**
     * Tipo volante.
     */
    @NotNull(message = "volanteTerceroNoAuto.tipoVolante.NotNull")
    @MaxLength(message = "volanteTerceroNoAuto.tipoVolante.Size", value = 2)
    private String tipoVolante;

    /**
     * Contratista.
     */
    private Long contratista;

    /**
     * Nacionalidad.
     */
    @MaxLength(message = "volanteTerceroNoAuto.nacionalidad.Size", value = 1)
    private String nacionalidad;

    /**
     * Monto.
     */
    @NotNull(message = "volanteTerceroNoAuto.monto.NotNull")
    private Double monto;

    /***
     * RFC beneficiario.
     */

    @MatchPattern(when = "jexl:'PP' == _this.tipoVolante",message = "volanteTerceroNoAuto.rfcBeneficiario.MatchPattern", pattern = "(^[A-Z]{3}[0-9]{6}[0-9A-Z]{3}$)|(^[A-Z]{4}[0-9]{6}[0-9A-Z]{3}$)")
    @MaxLength(message = "volanteTerceroNoAuto.rfcBeneficiario.Size", value = 13)
    private String rfcBeneficiario;

    /**
     * Nombre
     */
    @NotNull(message = "volanteTerceroNoAuto.nombre.NotNull")
    @NotBlank(message = "volanteTerceroNoAuto.nombre.NotBlank")
    @MatchPattern(message = "volanteTerceroNoAuto.nombre.MatchPattern", pattern = "^(?![a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*([a-zA-Z áéíóúüñÁÉÍÓÚÑÜ'])\\1{2,})[a-zA-Z áéíóúüñÁÉÍÓÚÑÜ']*$", flags = { Pattern.CASE_INSENSITIVE, Pattern.UNICODE_CASE })
    @MaxLength(message = "volanteTerceroNoAuto.nombre.Size", value = 70)
    private String nombre;

    /**
     * Observaciones.
     */
    @MaxLength(message = "volanteTerceroNoAuto.observaciones.Size", value = 100)
    private String observaciones;

    /**
     * Calle.
     */
    @MaxLength(message = "volanteTerceroNoAuto.calle.Size", value = 70)
    private String calle;

    /**
     * Tipo ubicación.
     */
    private String tipoUbicacion;

    /**
     * Código postal.
     */
    private String codigoPostal;

    /**
     * Colonia.
     */
    private String colonia;

    /**
     * Municipio.
     */
    private String municipio;

    /**
     * Estado.
     */
    private String estado;

    /**
     * Numero interior.
     */

    @MaxLength(message = "volanteTerceroNoAuto.numeroInterior.Size", value = 15)
    private String numeroInterior;

    /**
     * Número exterior.
     */
    @MaxLength(message = "volanteTerceroNoAuto.numeroExterior.Size", value = 15)
    private String numeroExterior;

    /**
     * Entre calles.
     */
    @MaxLength(message = "volanteTerceroNoAuto.entreCalles.Size", value = 50)
    private String entreCalles;

    /**
     * Referencias.
     */
    @MaxLength(message = "volanteTerceroNoAuto.referencias.Size", value =  70)
    private String referencias;

    /**
     * Kilómetro
     */
    @MaxLength(message = "volanteTerceroNoAuto.kilometro.Size", value = 50)
    private String kilometro;

    /**
     * Tipo de teléfono.
     */
    private String tipoTelefono;

    /**
     * Teléfono.
     */
    @NotNull(message = "volanteTerceroNoAuto.telefono.NotNull")
    @NotEmpty(message = "volanteTerceroNoAuto.telefono.NotEmpty")
    @MaxLength(message = "volanteTerceroNoAuto.telefono.Size",value = 15)
    private String telefono;

    /***
     * Extensión.
     */
    @MaxLength(message = "volanteTerceroNoAuto.extension.Size", value = 10)
    private String extension;

    /**
     * Correo del afectado.
     */
    @MaxLength(message = "volanteTerceroNoAuto.correo.Size", value = 70)
    private String correo;

    /**
     * Coberturas.
     */
    @MaxSize(message = "volanteTerceroNoAuto.coberturas.MaxSize", value = 1)
    @JsonAdapter(CoberturasTypeAdapter.class)
    private RealmList<String> coberturas;

    /**
     * Bandera impresión física.
     */
    @NotNull(message = "volanteTerceroNoAuto.impresionFisica.NotNull")
    private Boolean impresionFisica;

    /**
     * Bandera envío email;
     */
    @NotNull(message = "volanteTerceroNoAuto.envioEmail.NotNull")
    private Boolean envioEmail;

    /**
     * Folio del volante.
     */
    private String folio;

    /**
     * URL del archivo del volante.
     */
    private String url;

    public String getTipoVolante() {
        return tipoVolante;
    }

    public void setTipoVolante(String tipoVolante) {
        this.tipoVolante = tipoVolante;
    }

    public Long getContratista() {
        return contratista;
    }

    public void setContratista(Long contratista) {
        this.contratista = contratista;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getRfcBeneficiario() {
        return rfcBeneficiario;
    }

    public void setRfcBeneficiario(String rfcBeneficiario) {
        this.rfcBeneficiario = rfcBeneficiario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getTipoUbicacion() {
        return tipoUbicacion;
    }

    public void setTipoUbicacion(String tipoUbicacion) {
        this.tipoUbicacion = tipoUbicacion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getEntreCalles() {
        return entreCalles;
    }

    public void setEntreCalles(String entreCalles) {
        this.entreCalles = entreCalles;
    }

    public String getReferencias() {
        return referencias;
    }

    public void setReferencias(String referencias) {
        this.referencias = referencias;
    }

    public String getKilometro() {
        return kilometro;
    }

    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }

    public String getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(String tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<String> getCoberturas() {
        if (coberturas == null) {
            coberturas = new RealmList<>();
        }
        return coberturas;
    }

    public void setCoberturas(List<String> coberturas) {
        this.coberturas = CollectionsUtils.toRealmList(coberturas);
    }

    public Boolean getImpresionFisica() {
        return impresionFisica;
    }

    public void setImpresionFisica(Boolean impresionFisica) {
        this.impresionFisica = impresionFisica;
    }

    public Boolean getEnvioEmail() {
        return envioEmail;
    }

    public void setEnvioEmail(Boolean envioEmail) {
        this.envioEmail = envioEmail;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "VolanteTerceroNoAuto{" +
                "tipoVolante='" + tipoVolante + '\'' +
                ", contratista=" + contratista +
                ", nacionalidad='" + nacionalidad + '\'' +
                ", monto=" + monto +
                ", rfcBeneficiario='" + rfcBeneficiario + '\'' +
                ", nombre='" + nombre + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", calle='" + calle + '\'' +
                ", tipoUbicacion='" + tipoUbicacion + '\'' +
                ", codigoPostal='" + codigoPostal + '\'' +
                ", colonia='" + colonia + '\'' +
                ", municipio='" + municipio + '\'' +
                ", estado='" + estado + '\'' +
                ", numeroInterior='" + numeroInterior + '\'' +
                ", numeroExterior='" + numeroExterior + '\'' +
                ", entreCalles='" + entreCalles + '\'' +
                ", referencias='" + referencias + '\'' +
                ", kilometro='" + kilometro + '\'' +
                ", tipoTelefono='" + tipoTelefono + '\'' +
                ", telefono='" + telefono + '\'' +
                ", extension='" + extension + '\'' +
                ", correo='" + correo + '\'' +
                ", coberturas=" + coberturas +
                ", impresionFisica=" + impresionFisica +
                ", envioEmail=" + envioEmail +
                '}';
    }

}
