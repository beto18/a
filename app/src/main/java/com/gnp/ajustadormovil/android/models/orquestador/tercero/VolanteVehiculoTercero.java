package com.gnp.ajustadormovil.android.models.orquestador.tercero;

import com.gnp.ajustadormovil.android.models.json.DateTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CoberturasTypeAdapter;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volante;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.google.gson.annotations.JsonAdapter;

import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.MaxSize;
import net.sf.oval.constraint.NotNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class VolanteVehiculoTercero extends RealmObject implements Volante, Serializable {

    /**
     * Clave tipo tercero.
     */
    @NotNull(message = "volanteVehiculoTercero.tipoTercero.NotNull")
    @MaxLength(message = "volanteVehiculoTercero.tipoTercero.Size", value = 3)
    private String tipoTercero;

    /**
     * Otra aseguradora.
     */
    @MaxLength(value = 2, message = "volanteVehiculoTercero.otraAseguradora.Size")
    private String otraAseguradora;

    /**
     * Tipo poliza.
     */
    private Long tipoPoliza;

    /**
     * Número de póliza.
     */
    @MaxLength(value = 14, message = "volanteVehiculoTercero.numeroPoliza.Size")
    private String numeroPoliza;

    /**
     * Fecha fin de póliza.
     */
    @JsonAdapter(DateTypeAdapter.class)
    private Date fechaFinPoliza;

    /**
     * Titular de póliza.
     */
    @MaxLength(message = "volanteVehiculoTercero.titularPoliza.Size", value = 50)
    private String titularPoliza;

    /**
     * Clave del tipo de volante.
     */
    @NotNull(message = "volanteVehiculoTercero.tipoVolante.NotNull")
    @MaxLength(message = "volanteVehiculoTercero.tipoVolante.Size", value = 5)
    private String tipoVolante;

    /**
     * Clave nacionalidad.
     */
    @MaxLength(message = "volanteVehiculoTercero.nacionalidad.Size", value = 1)
    private String nacionalidad;

    /**
     * Monto pronto de pago.
     */
    private Double montoProntoPago;

    /**
     * RFC beneficiario.
     */
    @MatchPattern(when = "jexl:'TSS' == _this.tipoTercero",message = "volanteVehiculoTercero.rfcBeneficiario.MatchPattern", pattern = "(^[A-Z]{3}[0-9]{6}[0-9A-Z]{3}$)|(^[A-Z]{4}[0-9]{6}[0-9A-Z]{3}$)")
    @MaxLength(message = "volanteVehiculoTercero.rfcBeneficiario.Size", value = 20)
    private String rfcBeneficiario;

    /**
     * Nombre.
     */
    @MaxLength(message = "volanteVehiculoTercero.nombre.Size", value = 50)
    private String nombre;

    /**
     * Código postal.
     */
    @MaxLength(message = "volanteVehiculoTercero.codigoPostal.Size",value = 8)
    private String codigoPostal;

    /**
     * Clave del estado.
     */
    @MaxLength(message = "volanteVehiculoTercero.estado.Size",value = 2)
    private String estado;

    /**
     * Clave del municipio.
     */
    @MaxLength(message = "volanteVehiculoTercero.municipio.Size",value = 3)
    private String municipio;

    /**
     * Clave de la colonia.
     */
    @MaxLength(message = "volanteVehiculoTercero.colonia.Size", value = 4)
    private String colonia;

    /**
     * Calle.
     */
    @MaxLength(value = 30, message = "volanteVehiculoTercero.calle.Size" )
    private String calle;

    /**
     * Número exterior.
     */
    @MaxLength(value = 5, message = "volanteVehiculoTercero.numeroExterior.Size" )
    private String numeroExterior;

    /**
     * Número interior.
     */
    @MaxLength(value = 5, message = "volanteVehiculoTercero.numeroInterior.Size" )
    private String numeroInterior;

    /**
     * Entre calle.
     */
    @MaxLength(message = "volanteVehiculoTercero.entreCalle.Size", value = 50)
    private String entreCalle;

    /**
     * Referencia.
     */
    @MaxLength(message = "volanteVehiculoTercero.referencia.Size", value = 50)
    private String referencia;

    /**
     * Teléfono.
     */
    @MaxLength(message = "volanteVehiculoTercero.telefono.Size",value = 15)
    private String telefono;

    /**
     * Correo.
     */
    @MaxLength(message = "volanteVehiculoTercero.correo.Size", value = 50)
    private String correo;

    /**
     * Folio de la segmentación.
     */
    @MaxLength(message = "volanteVehiculoTercero.folioSegmentacion.Size", value = 6)
    private String folioSegmentacion;

    /**
     * Coberturas.
     */
    @MaxSize(message = "volanteVehiculoTercero.coberturas.MaxSize", value = 1)
    @JsonAdapter(CoberturasTypeAdapter.class)
    private RealmList<String> coberturas;

    /**
     * Requiere grúa.
     */
    @NotNull(message = "volanteVehiculoTercero.requiereGrua.NotNull")
    private Boolean requiereGrua;

    /**
     * Bandera impresión física.
     */
    @NotNull(message = "volanteVehiculoTercero.impresionFisica.NotNull")
    private Boolean impresionFisica;

    /**
     * Bandera envío email;
     */
    @NotNull(message = "volanteVehiculoTercero.envioEmail.NotNull")
    private Boolean envioEmail;

    /**
     * Número de siniestro.
     */
    @MaxLength(message = "volanteVehiculoTercero.numeroSiniestro.Size", value = 30)
    private String numeroSiniestro;

    /**
     * Volante condicionado.
     */
    @NotNull(message = "volanteVehiculoTercero.volanteCondicionado.NotNull")
    private Boolean volanteCondicionado;

    /**
     * Causa de condicionamiento.
     */
    @NotNull(message = "volanteVehiculoTercero.causaCondicionamiento.NotNull", when = "jexl:_this.volanteCondicionado")
    private Long causaCondicionamiento;

    /**
     * Vale Digital de eDUA.
     */
    private String valeDigital;

    /**
     * Firma Digital de eDUA.
     */
    private String firmaDigital;

    /**
     * Piezas Faltantes.
     */
    private RealmList<PiezaVehiculo> piezasFaltantes;

    /**
     * Sub-tipo del volante.
     */
    private String subTipoVolante;

    /**
     * Clave centro de remate
     */
    private Long centroRemate;

    /**
     * Folio del volante.
     */
    private String folio;

    /**
     * Url del documento.
     */
    private String url;

    public String getTipoTercero() {
        return tipoTercero;
    }

    public void setTipoTercero(String tipoTercero) {
        this.tipoTercero = tipoTercero;
    }

    public String getOtraAseguradora() {
        return otraAseguradora;
    }

    public void setOtraAseguradora(String otraAseguradora) {
        this.otraAseguradora = otraAseguradora;
    }

    public Long getTipoPoliza() {
        return tipoPoliza;
    }

    public void setTipoPoliza(Long tipoPoliza) {
        this.tipoPoliza = tipoPoliza;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public Date getFechaFinPoliza() {
        return fechaFinPoliza;
    }

    public void setFechaFinPoliza(Date fechaFinPoliza) {
        this.fechaFinPoliza = fechaFinPoliza;
    }

    public String getTitularPoliza() {
        return titularPoliza;
    }

    public void setTitularPoliza(String titularPoliza) {
        this.titularPoliza = titularPoliza;
    }

    public String getTipoVolante() {
        return tipoVolante;
    }

    public void setTipoVolante(String tipoVolante) {
        this.tipoVolante = tipoVolante;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Double getMontoProntoPago() {
        return montoProntoPago;
    }

    public void setMontoProntoPago(Double montoProntoPago) {
        this.montoProntoPago = montoProntoPago;
    }

    public String getRfcBeneficiario() {
        return rfcBeneficiario;
    }

    public void setRfcBeneficiario(String rfcBeneficiario) {
        this.rfcBeneficiario = rfcBeneficiario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getEntreCalle() {
        return entreCalle;
    }

    public void setEntreCalle(String entreCalle) {
        this.entreCalle = entreCalle;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFolioSegmentacion() {
        return folioSegmentacion;
    }

    public void setFolioSegmentacion(String folioSegmentacion) {
        this.folioSegmentacion = folioSegmentacion;
    }

    public List<String> getCoberturas() {
        if (coberturas==null){
            coberturas = new RealmList<>();
        }
        return coberturas;
    }

    public void setCoberturas(List<String> coberturas) {
        this.coberturas = CollectionsUtils.toRealmList(coberturas);
    }

    public Boolean getRequiereGrua() {
        return requiereGrua;
    }

    public void setRequiereGrua(Boolean requiereGrua) {
        this.requiereGrua = requiereGrua;
    }

    public Boolean getImpresionFisica() {
        return impresionFisica;
    }

    public void setImpresionFisica(Boolean impresionFisica) {
        this.impresionFisica = impresionFisica;
    }

    public Boolean getEnvioEmail() {
        return envioEmail;
    }

    public void setEnvioEmail(Boolean envioEmail) {
        this.envioEmail = envioEmail;
    }

    public String getNumeroSiniestro() {
        return numeroSiniestro;
    }

    public void setNumeroSiniestro(String numeroSiniestro) {
        this.numeroSiniestro = numeroSiniestro;
    }

    public Boolean getVolanteCondicionado() {
        return volanteCondicionado;
    }

    public void setVolanteCondicionado(Boolean volanteCondicionado) {
        this.volanteCondicionado = volanteCondicionado;
    }

    public Long getCausaCondicionamiento() {
        return causaCondicionamiento;
    }

    public void setCausaCondicionamiento(Long causaCondicionamiento) {
        this.causaCondicionamiento = causaCondicionamiento;
    }

    public List<PiezaVehiculo> getPiezasFaltantes() {
        if (piezasFaltantes == null){
            piezasFaltantes = new RealmList<>();
        }
        return piezasFaltantes;
    }

    public void setPiezasFaltantes(List<PiezaVehiculo> piezasFaltantes) {
        this.piezasFaltantes = CollectionsUtils.toRealmList(piezasFaltantes);
    }

    public String getSubTipoVolante() {
        return subTipoVolante;
    }

    public void setSubTipoVolante(String subTipoVolante) {
        this.subTipoVolante = subTipoVolante;
    }

    public Long getCentroRemate() {
        return centroRemate;
    }

    public void setCentroRemate(Long centroRemate) {
        this.centroRemate = centroRemate;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setCoberturas(RealmList<String> coberturas) {
        this.coberturas = coberturas;
    }

    public void setPiezasFaltantes(RealmList<PiezaVehiculo> piezasFaltantes) {
        this.piezasFaltantes = piezasFaltantes;
    }

    public String getValeDigital() {
        return valeDigital;
    }

    public void setValeDigital(String valeDigital) {
        this.valeDigital = valeDigital;
    }

    public String getFirmaDigital() {
        return firmaDigital;
    }

    public void setFirmaDigital(String firmaDigital) {
        this.firmaDigital = firmaDigital;
    }

    @Override
    public String toString() {
        return "VolanteVehiculoTercero{" +
                "tipoTercero='" + tipoTercero + '\'' +
                ", otraAseguradora='" + otraAseguradora + '\'' +
                ", tipoPoliza=" + tipoPoliza +
                ", numeroPoliza='" + numeroPoliza + '\'' +
                ", fechaFinPoliza=" + fechaFinPoliza +
                ", titularPoliza='" + titularPoliza + '\'' +
                ", tipoVolante='" + tipoVolante + '\'' +
                ", nacionalidad='" + nacionalidad + '\'' +
                ", montoProntoPago=" + montoProntoPago +
                ", rfcBeneficiario='" + rfcBeneficiario + '\'' +
                ", nombre='" + nombre + '\'' +
                ", codigoPostal='" + codigoPostal + '\'' +
                ", estado='" + estado + '\'' +
                ", municipio='" + municipio + '\'' +
                ", colonia='" + colonia + '\'' +
                ", calle='" + calle + '\'' +
                ", numeroExterior='" + numeroExterior + '\'' +
                ", numeroInterior='" + numeroInterior + '\'' +
                ", entreCalle='" + entreCalle + '\'' +
                ", referencia='" + referencia + '\'' +
                ", telefono='" + telefono + '\'' +
                ", correo='" + correo + '\'' +
                ", folioSegmentacion='" + folioSegmentacion + '\'' +
                ", coberturas=" + coberturas +
                ", requiereGrua=" + requiereGrua +
                ", impresionFisica=" + impresionFisica +
                ", envioEmail=" + envioEmail +
                ", numeroSiniestro='" + numeroSiniestro + '\'' +
                ", volanteCondicionado=" + volanteCondicionado +
                ", causaCondicionamiento=" + causaCondicionamiento +
                ", piezasFaltantes=" + piezasFaltantes +
                ", subTipoVolante='" + subTipoVolante + '\'' +
                ", centroRemate=" + centroRemate +
                ", folio='" + folio + '\'' +
                '}';
    }
}
