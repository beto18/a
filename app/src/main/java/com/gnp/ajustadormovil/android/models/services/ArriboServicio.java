package com.gnp.ajustadormovil.android.models.services;

import com.google.gson.annotations.SerializedName;

public class ArriboServicio {

    @SerializedName("estatus")
    public String estatus;
    @SerializedName("idSolicitud")
    public Integer idSolicitud;
    @SerializedName("sistemaOrigen")
    String sistemaOrigen;
    @SerializedName("usuarioAudit")
    String usuarioAudit;

    public ArriboServicio(String estatus, Integer idSolicitud, String sistemaOrigen, String usuarioAudit) {
        this.estatus = estatus;
        this.idSolicitud = idSolicitud;
        this.sistemaOrigen = sistemaOrigen;
        this.usuarioAudit = usuarioAudit;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(String sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public String getUsuarioAudit() {
        return usuarioAudit;
    }

    public void setUsuarioAudit(String usuarioAudit) {
        this.usuarioAudit = usuarioAudit;
    }
}
