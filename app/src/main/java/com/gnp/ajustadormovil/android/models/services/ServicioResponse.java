package com.gnp.ajustadormovil.android.models.services;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abraham on 24/03/18.
 */

public class ServicioResponse {
    @SerializedName("acompanante")
    private String acompanante;
    @SerializedName("ambulanciaResponsiva")
    private Boolean ambulanciaResponsiva;
    @SerializedName("comunicacionAbogado")
    private String comunicacionAbogado;
    @SerializedName("conductorDetenido")
    private Boolean conductorDetenido;
    @SerializedName("danoPropiedad")
    private Boolean danoPropiedad;
    @SerializedName("destino")
    private ServicioUbicacion destino;
    @SerializedName("diagnostico")
    private String diagnostico;
    @SerializedName("gruaPresente")
    private Boolean gruaPresente;
    @SerializedName("id")
    private Integer id;
    @SerializedName("lesionados")
    private Boolean lesionados;
    @SerializedName("medico")
    private String medico;
    @SerializedName("motivoInvestigacion")
    private String motivoInvestigacion;
    @SerializedName("motivoPatrulla")
    private String motivoPatrulla;
    @SerializedName("observaciones")
    private String observaciones;
    @SerializedName("ocupante")
    private Integer ocupante;
    @SerializedName("origen")
    private ServicioUbicacion origen;
    @SerializedName("para")
    private String para;
    @SerializedName("solicitud")
    private String solicitud;
    @SerializedName("tercero")
    private Integer tercero;
    @SerializedName("tipoAbogado")
    private String tipoAbogado;
    @SerializedName("tipoGestion")
    private String tipoGestion;
    @SerializedName("tipoGrua")
    private String tipoGrua;
    @SerializedName("tipoServicio")
    private String tipoServicio;
    @SerializedName("vehiculoDetenenido")
    private Boolean vehiculoDetenenido;
    @SerializedName("estatusServicio")
    private String estatusServicio;
    @SerializedName("estatus")
    private String status;
    @SerializedName("motivoCancelacion")
    private String motivoCancelacion;
    @SerializedName("comentarioCancelacion")
    private String comentarioCancelacion;



    public ServicioResponse() {
    }

    public String getStatus() {
       return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

   /* public String getStatus() {
        if(status== null || status.equals(""))
            status= "SOLICITADO";
        return status;
    }*/


    /*@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServicioResponse that = (ServicioResponse) o;
        return Objects.equals(id, that.id);
    }
*/
    /*@Override
    public int hashCode() {
        return Objects.hash(id);
    }*/

    public String getAcompanante() {
        return acompanante;
    }

    public Boolean getAmbulanciaResponsiva() {
        return ambulanciaResponsiva;
    }

    public String getComunicacionAbogado() {
        return comunicacionAbogado;
    }

    public Boolean getConductorDetenido() {
        return conductorDetenido;
    }

    public Boolean getDanoPropiedad() {
        return danoPropiedad;
    }

    public ServicioUbicacion getDestino() {
        return destino;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public Boolean getGruaPresente() {
        return gruaPresente;
    }

    public Integer getId() {
        return id;
    }

    public Boolean getLesionados() {
        return lesionados;
    }

    public String getMedico() {
        return medico;
    }

    public String getMotivoInvestigacion() {
        return motivoInvestigacion;
    }

    public String getMotivoPatrulla() {
        return motivoPatrulla;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public Integer getOcupante() {
        return ocupante;
    }

    public ServicioUbicacion getOrigen() {
        return origen;
    }

    public String getPara() {
        return para;
    }

    public String getSolicitud() {
        return solicitud;
    }

    public Integer getTercero() {
        return tercero;
    }

    public String getTipoAbogado() {
        return tipoAbogado;
    }

    public String getTipoGestion() {
        return tipoGestion;
    }

    public String getTipoGrua() {
        return tipoGrua;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public Boolean getVehiculoDetenenido() {
        return vehiculoDetenenido;
    }

    public void setAcompanante(String acompanante) {
        this.acompanante = acompanante;
    }

    public void setAmbulanciaResponsiva(Boolean ambulanciaResponsiva) {
        this.ambulanciaResponsiva = ambulanciaResponsiva;
    }

    public void setComunicacionAbogado(String comunicacionAbogado) {
        this.comunicacionAbogado = comunicacionAbogado;
    }

    public void setConductorDetenido(Boolean conductorDetenido) {
        this.conductorDetenido = conductorDetenido;
    }

    public void setDanoPropiedad(Boolean danoPropiedad) {
        this.danoPropiedad = danoPropiedad;
    }

    public void setDestino(ServicioUbicacion destino) {
        this.destino = destino;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public void setGruaPresente(Boolean gruaPresente) {
        this.gruaPresente = gruaPresente;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLesionados(Boolean lesionados) {
        this.lesionados = lesionados;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public void setMotivoInvestigacion(String motivoInvestigacion) {
        this.motivoInvestigacion = motivoInvestigacion;
    }

    public void setMotivoPatrulla(String motivoPatrulla) {
        this.motivoPatrulla = motivoPatrulla;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public void setOcupante(Integer ocupante) {
        this.ocupante = ocupante;
    }

    public void setOrigen(ServicioUbicacion origen) {
        this.origen = origen;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public void setSolicitud(String solicitud) {
        this.solicitud = solicitud;
    }

    public void setTercero(Integer tercero) {
        this.tercero = tercero;
    }

    public void setTipoAbogado(String tipoAbogado) {
        this.tipoAbogado = tipoAbogado;
    }

    public void setTipoGestion(String tipoGestion) {
        this.tipoGestion = tipoGestion;
    }

    public void setTipoGrua(String tipoGrua) {
        this.tipoGrua = tipoGrua;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public void setVehiculoDetenenido(Boolean vehiculoDetenenido) {
        this.vehiculoDetenenido = vehiculoDetenenido;
    }

    public String getEstatusServicio() {
        return estatusServicio;
    }

    public void setEstatusServicio(String estatusServicio) {
        this.estatusServicio = estatusServicio;
    }

    public String getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public String getComentarioCancelacion() {
        return comentarioCancelacion;
    }

    public void setComentarioCancelacion(String comentarioCancelacion) {
        this.comentarioCancelacion = comentarioCancelacion;
    }
}
