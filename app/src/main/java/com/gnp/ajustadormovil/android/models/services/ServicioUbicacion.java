package com.gnp.ajustadormovil.android.models.services;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abraham on 24/03/18.
 */

public class ServicioUbicacion {

    @SerializedName("latitud")
    private double latitud;
    @SerializedName("longitud")
    private double longitud;
    @SerializedName("asentamiento")
    private String asentamiento;
    @SerializedName("codigoPostal")
    private String codigoPostal;
    @SerializedName("colonia")
    private String colonia;
    @SerializedName("estado")
    private String estado;
    @SerializedName("municipio")
    private String municipio;
    @SerializedName("numeroExterior")
    private String numeroExterior;
    @SerializedName("numeroInterior")
    private String numeroInterior;
    @SerializedName("referencias")
    private String referencias;
    @SerializedName("tipoUbicacion")
    private String tipoUbicacion;
    @SerializedName("tipoVia")
    private String tipoVia;
    @SerializedName("via")
    private String via;

    public ServicioUbicacion(){

    }

    public ServicioUbicacion(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getAsentamiento() {
        return asentamiento;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public String getEstado() {
        return estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public String getReferencias() {
        return referencias;
    }

    public String getTipoUbicacion() {
        return tipoUbicacion;
    }

    public String getTipoVia() {
        return tipoVia;
    }

    public String getVia() {
        return via;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }
}
