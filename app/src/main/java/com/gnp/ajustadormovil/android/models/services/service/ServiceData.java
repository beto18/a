package com.gnp.ajustadormovil.android.models.services.service;

import com.gnp.ajustadormovil.android.models.services.ServicioUbicacion;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abraham on 24/03/18.
 */

public class ServiceData {

    @SerializedName("destino")
    private ServicioUbicacion destino;
    @SerializedName("gruaPresente")
    private Boolean gruaPresente;
    @SerializedName("observaciones")
    private String observaciones;
    @SerializedName("origen")
    private ServicioUbicacion origen;
    @SerializedName("para")
    private String para;
    @SerializedName("tercero")
    private Long tercero;
    @SerializedName("tipoGrua")
    private String tipoGrua;
    @SerializedName("comunicacionAbogado")
    private String comunicacionAbogado;
    @SerializedName("conductorDetenido")
    private Boolean isDetenido;
    @SerializedName("danoPropiedad")
    private Boolean danoPropiedad;
    @SerializedName("lesionados")
    private Boolean lesionados;
    @SerializedName("ocupante")
    private Long ocupante;
    @SerializedName("tipoAbogado")
    private String tipoAbogado;
    @SerializedName("tipoGestion")
    private String tipoGestion;
    @SerializedName("vehiculoDetenido")
    private Boolean isVehiculoDetenido;
    @SerializedName("motivoPatrulla")
    private String motivoPatrulla;
    @SerializedName("acompanante")
    private String acompanante;
    @SerializedName("diagnostico")
    private String diagnostico;
    @SerializedName("medico")
    private String medico;
    @SerializedName("tipo")
    private String tipoAmbulancia;
    @SerializedName("ambulanciaResponsiva")
    private Boolean ambulanciaResponsiva;
    @SerializedName("motivoInvestigacion")
    private String motivoInvestigacion;

    public ServiceData(ServicioUbicacion destino, Boolean gruaPresente, String observaciones,
                       ServicioUbicacion origen, String para, Long tercero, String tipoGrua,
                       String comunicacionAbogado, Boolean isDetenido, Boolean danoPropiedad,
                       Boolean lesionados, Long ocupante, String tipoAbogado, String tipoGestion,
                       Boolean isVehiculoDetenido, String motivoPatrulla, String acompanante,
                       Boolean ambulanciaResponsiva, String diagnostico, String medico,
                       String tipoAmbulancia, String motivoInvestigacion) {
        this.destino = destino;
        this.gruaPresente = gruaPresente;
        this.observaciones = observaciones;
        this.origen = origen;
        this.para = para;
        this.tercero = tercero;
        this.tipoGrua = tipoGrua;
        this.comunicacionAbogado = comunicacionAbogado;
        this.isDetenido = isDetenido;
        this.danoPropiedad = danoPropiedad;
        this.lesionados = lesionados;
        this.ocupante = ocupante;
        this.tipoAbogado = tipoAbogado;
        this.tipoGestion = tipoGestion;
        this.isVehiculoDetenido = isVehiculoDetenido;
        this.motivoPatrulla = motivoPatrulla;
        this.acompanante = acompanante;
        this.ambulanciaResponsiva = ambulanciaResponsiva;
        this.diagnostico = diagnostico;
        this.medico = medico;
        this.tipoAmbulancia = tipoAmbulancia;
        this.motivoInvestigacion = motivoInvestigacion;
    }

    public String getMotivoInvestigacion() {
        return motivoInvestigacion;
    }

    public void setMotivoInvestigacion(String motivoInvestigacion) {
        this.motivoInvestigacion = motivoInvestigacion;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public String getTipoAmbulancia() {
        return tipoAmbulancia;
    }

    public void setTipoAmbulancia(String tipoAmbulancia) {
        this.tipoAmbulancia = tipoAmbulancia;
    }

    public Boolean getAmbulanciaResponsiva() {
        return ambulanciaResponsiva;
    }

    public void setAmbulanciaResponsiva(Boolean ambulanciaResponsiva) {
        this.ambulanciaResponsiva = ambulanciaResponsiva;
    }

    public String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(String acompanante) {
        this.acompanante = acompanante;
    }

    public String getMotivoPatrulla() {
        return motivoPatrulla;
    }

    public void setMotivoPatrulla(String motivoPatrulla) {
        this.motivoPatrulla = motivoPatrulla;
    }

    public ServicioUbicacion getDestino() {
        return destino;
    }

    public void setDestino(ServicioUbicacion destino) {
        this.destino = destino;
    }

    public Boolean getGruaPresente() {
        return gruaPresente;
    }

    public void setGruaPresente(Boolean gruaPresente) {
        this.gruaPresente = gruaPresente;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public ServicioUbicacion getOrigen() {
        return origen;
    }

    public void setOrigen(ServicioUbicacion origen) {
        this.origen = origen;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public Long getTercero() {
        return tercero;
    }

    public void setTercero(Long tercero) {
        this.tercero = tercero;
    }

    public String getTipoGrua() {
        return tipoGrua;
    }

    public void setTipoGrua(String tipoGrua) {
        this.tipoGrua = tipoGrua;
    }

    public String getComunicacionAbogado() {
        return comunicacionAbogado;
    }

    public void setComunicacionAbogado(String comunicacionAbogado) {
        this.comunicacionAbogado = comunicacionAbogado;
    }

    public Boolean getDetenido() {
        return isDetenido;
    }

    public void setDetenido(Boolean detenido) {
        isDetenido = detenido;
    }

    public Boolean getDanoPropiedad() {
        return danoPropiedad;
    }

    public void setDanoPropiedad(Boolean danoPropiedad) {
        this.danoPropiedad = danoPropiedad;
    }

    public Boolean getLesionados() {
        return lesionados;
    }

    public void setLesionados(Boolean lesionados) {
        this.lesionados = lesionados;
    }

    public Long getOcupante() {
        return ocupante;
    }

    public void setOcupante(Long ocupante) {
        this.ocupante = ocupante;
    }

    public String getTipoAbogado() {
        return tipoAbogado;
    }

    public void setTipoAbogado(String tipoAbogado) {
        this.tipoAbogado = tipoAbogado;
    }

    public String getTipoGestion() {
        return tipoGestion;
    }

    public void setTipoGestion(String tipoGestion) {
        this.tipoGestion = tipoGestion;
    }

    public Boolean getVehiculoDetenido() {
        return isVehiculoDetenido;
    }

    public void setVehiculoDetenido(Boolean vehiculoDetenido) {
        isVehiculoDetenido = vehiculoDetenido;
    }

    @Override
    public String toString() {
        return "ServiceData{" +
                "destino=" + destino +
                ", gruaPresente=" + gruaPresente +
                ", observaciones='" + observaciones + '\'' +
                ", origen=" + origen +
                ", para='" + para + '\'' +
                ", tercero=" + tercero +
                ", tipoGrua='" + tipoGrua + '\'' +
                ", comunicacionAbogado='" + comunicacionAbogado + '\'' +
                ", isDetenido=" + isDetenido +
                ", danoPropiedad=" + danoPropiedad +
                ", lesionados=" + lesionados +
                ", ocupante=" + ocupante +
                ", tipoAbogado='" + tipoAbogado + '\'' +
                ", tipoGestion='" + tipoGestion + '\'' +
                ", isVehiculoDetenido=" + isVehiculoDetenido +
                ", motivoPatrulla='" + motivoPatrulla + '\'' +
                ", acompanante='" + acompanante + '\'' +
                ", diagnostico='" + diagnostico + '\'' +
                ", medico='" + medico + '\'' +
                ", tipoAmbulancia='" + tipoAmbulancia + '\'' +
                ", ambulanciaResponsiva=" + ambulanciaResponsiva +
                ", motivoInvestigacion='" + motivoInvestigacion + '\'' +
                '}';
    }
}
