package com.gnp.ajustadormovil.android.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.reactivex.disposables.CompositeDisposable;

import static android.app.PendingIntent.getActivity;

public class FcmMessageService extends FirebaseMessagingService {

    private static final String TAG = FcmMessageService.class.getSimpleName();

    private AjusteInteractor ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();

    private CompositeDisposable compositeDisposable;

    private static NotificationManager notificationManager;

    private static final String INSISTENT_CHANNEL = "INSISTENT_CHANNEL";

    private static final String DEFAULT_CHANNEL = "DEFAULT_CHANNEL";

    private static final String INSISTENT_NAME = "INSISTENT";

    private static final String DEFAULT_NAME = "DEFAULT";

    private static final long DELAY = 1000L;

    private static SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss",  Locale.US);

    private void createInsistentChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(INSISTENT_CHANNEL, INSISTENT_NAME, NotificationManager.IMPORTANCE_HIGH);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                    .build();
            channel.setSound(Uri.parse("android.resource://com.ajustadormovil.gnp.android/" + R.raw.nuevo), attributes);
            channel.enableLights(true);
            channel.enableVibration(true);
            channel.setShowBadge(true);
            channel.setLightColor(Color.GREEN);
            channel.setBypassDnd(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            getNotificationManager().createNotificationChannel(channel);
        }
    }

    private void createDefaultChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(DEFAULT_CHANNEL, DEFAULT_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), attributes);
            channel.enableLights(true);
            channel.enableVibration(true);
            channel.setShowBadge(true);
            channel.setLightColor(Color.GREEN);
            channel.setBypassDnd(true);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void delay() {
        try {
            Thread.sleep(DELAY);
        } catch (InterruptedException e){
            Log.w(TAG, "Delay interrumpido", e);
        }
    }

    private synchronized NotificationManager getNotificationManager(){
        if(notificationManager == null ) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            createInsistentChannel();
            createDefaultChannel();
        }
        return notificationManager;
    }

    private int getNotificationId() {
        return Integer.parseInt(sdf.format(new Date()));
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        synchronized (this) {
            Log.i(TAG, "Inicio: " + System.currentTimeMillis());
            String message = remoteMessage.getData().get(Constants.PAYLOAD_BODY);
            if (message == null) {
                message = "";
            }
            String type = remoteMessage.getData().get(Constants.TYPE_PAYLOAD);
            if (type != null) {
                switch (type) {
                    case Constants.PAYLOAD_ALLOCATION:
                        sendNotificationInsistent(type, message);
                        break;
                    case Constants.PAYLOAD_STATUS:
                        if (!"POR_CONFIRMAR".equals(remoteMessage.getData().get("estatus"))) {
                            sendNotification(type, message);
                        }
                        break;
                    case Constants.PAYLOAD_EVOLUTION:
                        evolucionarSiniestro(remoteMessage.getData().get(Constants.PAYLOAD_AJUSTE));
                    case Constants.PAYLOAD_ESTATUS_AJUSTE:
                    case Constants.PAYLOAD_SESSION:
                    case Constants.PAYLOAD_SERVICIO:
                        sendNotification(type, message);
                        break;
                }
            }
        }
    }

    private void evolucionarSiniestro(String ajuste){
        compositeDisposable.add(ajusteInteractor.evolucionar(ajuste)
                .subscribe(() -> {
                    Log.i(TAG, "El ajuste " + ajuste + " fue actualizado");
                    Intent intent = new Intent(Constants.PAYLOAD_EVOLUTION);
                    intent.putExtra("ajuste", ajuste);
                    sendBroadcast(intent);
                }, throwable -> {
                    Log.e(TAG, "El ajuste " + ajuste + " no se pudo actualizar: ", throwable);
                }));
    }

    @SuppressLint("LongLogTag")
    private void sendNotificationInsistent(String type, String message) {
        try {

            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            PendingIntent pIntent = getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notification = new NotificationCompat.Builder(getApplicationContext(),INSISTENT_CHANNEL)
                    .setAutoCancel(true)
                    .setContentIntent(pIntent)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_notificacion_ic, 0)
                    .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ajustador_icon))
                    .setColor(Color.TRANSPARENT)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[]{0, 2000, 1000, 0, 1000, 2000})
                    .setLights(Color.RED, 3000, 3000)
                    .setSound(Uri.parse("android.resource://com.ajustadormovil.gnp.android/" + R.raw.nuevo))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .build();

            notification.flags |= Notification.FLAG_INSISTENT;

            getNotificationManager().notify(type, getNotificationId(), notification);

        } catch (Exception e) {
            Log.e(TAG, "Error al enviar la notificación insistente: ",e);
        }
    }

    @SuppressLint("LongLogTag")
    private void sendNotification(String type, String message) {
        try {

            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            PendingIntent pIntent = getActivity(getApplicationContext(), 0, intent, 0);

            Notification notification = new NotificationCompat.Builder(getApplicationContext(), DEFAULT_CHANNEL)
                    .setAutoCancel(true)
                    .setContentIntent(pIntent)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_notificacion_ic, 0)
                    .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ajustador_icon))
                    .setColor(Color.TRANSPARENT)
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setVibrate(new long[]{0, 100, 200, 0, 100, 200})
                    .setLights(Color.RED, 3000, 3000)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .build();

            getNotificationManager().notify(type, getNotificationId(), notification);

            delay();

        } catch (Exception e) {
            Log.e(TAG, "Error al enviar la notificación: ",e);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}

