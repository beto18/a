package com.gnp.ajustadormovil.android.services;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FcmTokenService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        AppPreferences.setString(AppPreferences.FCM_TOKEN, token);
    }
}
