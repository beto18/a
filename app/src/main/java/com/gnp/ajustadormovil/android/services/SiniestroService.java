package com.gnp.ajustadormovil.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.splash.SplashActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by tixmedia on 3/21/18.
 * edit by Giovanni on 23/10/18
 */

public class SiniestroService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Context context = this;
    public static boolean IS_RUNNING = false;


    @Override
    public void onCreate() {
        super.onCreate();
        checkFireBaseToUpdate();
        IS_RUNNING = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IS_RUNNING = false;
    }

    private void checkFireBaseToUpdate() {

        FirebaseDatabase firebaseDataBase;
        DatabaseReference databaseReference;
        firebaseDataBase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDataBase.getReference();

        final DatabaseReference initParse = databaseReference.child(Constants.ADJUSTER)
                .child(AjustadorMovilApplication.getCedula()).child(Constants.STATUS_ADJUSTER);
        initParse.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot childSnapshot) { //dataSnapshot) {

                AppPreferences.setString(AppPreferences.USER_STATUS, childSnapshot.getValue(String.class));
                sendBroadCast(Constants.STATUS_CHANGED, "");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FireBaseService", "error:." + databaseError.getMessage());
            }
        });

        final DatabaseReference tokenChange = databaseReference.child(Constants.ADJUSTER)
                .child(AjustadorMovilApplication.getCedula()).child(Constants.TOKEN_MOBILE);
        tokenChange.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String token = AppPreferences.getString(AppPreferences.GCM_TOKEN);
                if (!token.equals(dataSnapshot.getValue(String.class))) {
                    AppPreferences.cleanAll();
                    BaseActivity.stopService();
                    Toast.makeText(context, "Se inició sesión en otro dispositivo", Toast.LENGTH_LONG).show();

                    if (dataSnapshot.getValue(String.class) != null)
                        startActivity(new Intent(getApplicationContext(), SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FireBaseService", "error:." + databaseError.getMessage());
            }
        });

    }

    private void sendBroadCast(String filter, String value) {
        try {
            Intent intentBroadCast = new Intent();
            intentBroadCast.setAction(filter);
            intentBroadCast.putExtra(Constants.ID_SINIESTRO, value);
            sendBroadcast(intentBroadCast);
        } catch (Exception e) {
            Log.d("error exception", "" + e.getMessage());
        }
    }

}
