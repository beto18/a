package com.gnp.ajustadormovil.android.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjustadorInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.PosicionCommand;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by hgzavala on 16/04/2018
 */
public class UbicacionService extends Service {

    private static final String TAG = UbicacionService.class.getSimpleName();
    private LocationManager locationManager = null;
    private static final int LOCATION_INTERVAL = 30000;
    private static final float LOCATION_DISTANCE = 0;

    private class DefaultLocationListener implements LocationListener {

        private AjustadorInteractor ajustadorInteractor;

        private synchronized AjustadorInteractor getAjustadorInteractor() {
            if(ajustadorInteractor == null) {
                ajustadorInteractor = OrquestadorInteractorFactory.getAjustadorInteractor();
            }
            return ajustadorInteractor;
        }

        @Override
        public void onLocationChanged(Location location) {
            if(location != null) {
                Log.d(TAG, "onLocationChanged " + location.getProvider() + "; lat=" + location.getLatitude() + ", lon=" + location.getLongitude());
                new Thread(() -> {
                    try {
                        String isLogueado = AppPreferences.getString(AppPreferences.USER_INFO_TOKEN);
                        Log.d(TAG, "[" + location.getProvider() + "] Logueado: " + !"".equals(isLogueado));
                        if(!"".equals(isLogueado)){
                            Log.d(TAG, "[" + location.getProvider() + "] Enviando notificación");
                            RXUtils.sync(getAjustadorInteractor().modifyUbicacion(new PosicionCommand(location.getLatitude(), location.getLongitude())));
                            Log.i(TAG, "[" + location.getProvider() + "] Posición enviada: " + location.getLatitude() + ", " + location.getLongitude());
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "[" + location.getProvider() + "] Error al obtener la ubicación", e);
                    }
                }).start();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "onStatusChanged " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "onProviderEnabled " + provider);
            try {
                if(LocationManager.GPS_PROVIDER.equals(provider)) {
                    FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(true);
                    Log.d(TAG, "banEstGPS " + true);
                }
            } catch (Exception ex) {
                Log.e(TAG, "onProviderEnabled: " + provider, ex);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "onProviderDisabled " + provider);
            try {
                if(LocationManager.GPS_PROVIDER.equals(provider)) {
                    FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(false);
                    Log.d(TAG, "banEstGPS " + false);
                }
            } catch (Exception ex) {
                Log.e(TAG, "onProviderDisabled: " + provider, ex);
            }
        }
    }

    private LocationListener networkLocationListener = new DefaultLocationListener();

    private LocationListener gpsLocationListener = new DefaultLocationListener();

    private LocationManager getLocationManager() {
        if(locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
        return locationManager;
    }

    private void addLocation(int interval, String provider, LocationListener listener) {
        try {
            getLocationManager().requestLocationUpdates(
                    provider, interval, LOCATION_DISTANCE,
                    listener);
        } catch (SecurityException ex) {
            Log.e(TAG, "Error al registrar localización " + provider, ex);
        } catch (Exception ex) {
            Log.e(TAG, "Error al registrar localización " + provider, ex);
        }
    }

    private void removeLocation(String provider, LocationListener listener) {
        try {
            getLocationManager().removeUpdates(networkLocationListener);
        } catch (Exception ex) {
            Log.e(TAG, "Error al remover el servicio de localización " + provider, ex);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        String configInterval = AppPreferences.getString(AppPreferences.UBICACION_PETICION);
        Integer interval = configInterval != null && !"".equals(configInterval) ?
                Integer.parseInt(configInterval) :
                LOCATION_INTERVAL;
        addLocation(interval, LocationManager.GPS_PROVIDER, gpsLocationListener);
        addLocation(interval, LocationManager.NETWORK_PROVIDER, networkLocationListener);
        startInForeground();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
        removeLocation(LocationManager.GPS_PROVIDER, gpsLocationListener);
        removeLocation(LocationManager.NETWORK_PROVIDER, networkLocationListener);
    }

    private void startInForeground() {
        Intent notificationIntent = new Intent(this, UbicacionService.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,notificationIntent,0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"ch_4")
                .setContentTitle("Ajustador Móvil")
                .setContentText("Tu aplicación esta activa")
                .setTicker("TICKER")
                .setContentIntent(pendingIntent);
        Notification notification = builder.build();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("ch_4", "GNP", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("canal de gnp");
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
        startForeground(4, notification);
    }

}
