package com.gnp.ajustadormovil.android.utils;

import android.os.Environment;

import com.gnp.ajustadormovil.android.BuildConfig;

import java.io.File;

/**
 * Created by tixmedia on 3/14/18.
 * Modified by hgzavala on 25/04/2018
 */

public class Constants {
    public static final String NOT_AVAILABLE = "NO DISPONIBLE";
    public static final String AVAILABLE = "DISPONIBLE";
    public static final String ON_PAUSE = "EN RECESO";
    public static final String RETIRED = "RETIRADO";
    public static final String ATTENDING = "ATENDIENDO";
    public static final String ON_HOLD = "EN ESPERA";
    public static final String TO_CONFIRMED = "POR CONFIRMAR";
    public static final String TO_LEAVE = "POR SALIR";
    public static final String TO_FINISH = "POR TERMINAR";
    public static final String ON_ARRIVE = "EN CAMINO";
    public static final String ADJUSTER = "ajustadores";
    public static final String STATUS_ADJUSTER = "estOperativo";
    public static final String TOKEN_MOBILE = "tokenMovil";
    public static final String STATUS_CHANGED = "STATUS_CHANGED";
    public static final String ADJUST_REMOVED = "ADJUST_REMOVED";

    //Rol de ajustador aceptado para login
    public static final String ROL_AJUSTADOR_AUTOS = "Ajustador Autos";

    //dir save image
    public static final String MEDIA_DIR = Environment.getExternalStorageDirectory() + "/adjusterMobile/media";
    public static final String NAME_DOC = "archivos";

    //branch
    public static final String BRANCH_EMPLOYEE = "empleados";
    public static final String BRANCH_ADJUSTER = "ajustadores";

    public static final String PERSONA_FISICA = "Fisica";
    public static final String PERSONA_MORAL = "Moral";
    public static final String FOUND_NO_AUTO = "foundNoAuto";

    //Type Recycler
    public static final int TYPE_EMPTY = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_HEADER = 2;

    //RequestCode
    public static final int REQUEST_CODE_MAP_ORIGIN = 200;
    public static final int REQUEST_CODE_MAP_DESTINY = 201;

    //Location
    public static final String LNG = "longitud";
    public static final String LAT = "latitud";
    public static final Integer REQUEST_CODE_FINE_LOCATION_PERMISSION = 100;


    //Constants Services
    public static final String INSURE = "ASEGURADOS";
    public static final String THIRD = "TERCEROS";
    public static final String CRANE = "grua";
    public static final String POLICE = "Patrulla";
    public static final String AMBULANCE = "ambulancia";
    public static final String RECUPERATOR = "recuperador";
    public static final String FUNERARY = "Servicios Funerarios";
    public static final String TAXI = "taxi";
    public static final String CHECKER = "Verificador";
    public static final String LAWYER = "Abogado";
    public static final String CONTRACTOR = "contratista";

    //Operaciones sobre los ajustes
    public static final String RECHAZADO = "RECHAZADO";
    public static final String CANCELADO = "CANCELADO";
    public static final String TERMINADO = "TERMINADO";

    //Pattern
    public static final String PATTERN_NUMBER = "#,###";

    //Tags fragment
    public static final String TAG_INSURE_VEHICLE = "INSURE_VEHICLE";
    public static final String TAG_INSURE_DRIVER = "INSURE_DRIVER";
    public static final String TAG_THIRD_VEHICLE = "THIRD_VEHICLE";
    public static final String TAG_THIRD_DRIVER = "THIRD_DRIVER";
    public static final String TAG_THIRD_PEDESTRIAN = "THIRD_PEDESTRIAN";
    public static final String TAG_THIRD_NO_VEHICLE = "THIRD_NO_VEHICLE";
    public static final String TAG_INSURE_OCCUPANT = "INSURE_OCCUPANT";
    public static final String TAG_THIRD_OCCUPANT = "THIRD_OCCUPANT";
    public static final String TAG_DECLARACION = "TAG_DECLARACION";
    public static final String TAG_VOLANTE_TALLER = "TAG_VOLANTE_TALLER";
    public static final String TAG_RECOVERY = "TAG_RECOVERY";


    public static final Integer ERROR_NETWORK = 0;

    public static final String NAME_BUNDLE_DATA_PRINTER = "DATA_PRINTER";
    public static final String ID_SINIESTRO = "Siniestro";
    public static final String ID_PRESINIESTRO = "presiniestro";
    public static int SERVICIO = 1;
    public static int CARPETA = 2;
    public static int GUARDADDO = 3;

    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    //Para calcular el tamaño del diálogo en mostrar QR
    public static final int PADDING = 60;

    //para el resize de imagen
    public static final int MAX_SIZE = 50;

    //RequestCode de ActivityResult
    public static int SIGNATURE_REQUEST_CODE = 2;

    //Principio de folio para eDUA
    public static final String CLAVE_COMPANIA = "GNP";

    //Color de margen para foto de eDUA
    public static final String TRANSPARENT_BLACK = "#19000000";
    public static final int TAMAÑO_MARGEN = 300;
    public static final String IMAGEN_TOMADA = "T";
    public static final String IMAGEN_CARGADA = "C";

    //Numero de compañia de GNP

    public static final Long AMIS_ID_GNP = 31L;

    //Valor de máximo de caracteres en narrativa
    public static int MAX_CARACTERES = 500;

    public interface WHAT {
        int SUCCESS = 0;
        int FAILURE = 1;
        int ERROR = 2;
    }

    public interface KEY {
        String IMG_PATH = "IMG_PATH";
        String VIDEO_PATH = "VIDEO_PATH";
        String PIC_WIDTH = "PIC_WIDTH";
        String PIC_HEIGHT = "PIC_HEIGHT";
        String PIC_URI = "PIC_URI";
    }

    public interface REQUEST_CODE {
        int CAMERA = 0;
    }

    public interface RESULT_CODE {
        int RESULT_OK = -1;
        int RESULT_CANCELED = 0;
        int RESULT_ERROR = 1;
        int RESULT_RESTART = 2;
    }

    //GCM Notification token validation
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    //GCM notification payload status and id notification
    public static String NOTIFICATION_ID = "notification-id";
    public static final String TYPE_PAYLOAD = "tipo";
    public static final String PAYLOAD_ALLOCATION = "ASIGNACION_AJUSTE";
    public static final String PAYLOAD_STATUS = "ESTATUS_AJUSTADOR";
    public static final String PAYLOAD_SESSION = "INICIO_SESION";
    public static final String PAYLOAD_EVOLUTION = "EVOLUCION_PRESINIESTRO";
    public static final String ON_REFOLEO = "REFOLEO";
    public static final String PAYLOAD_ESTATUS_AJUSTE = "ESTATUS_AJUSTE";
    public static final String PAYLOAD_SERVICIO = "SERVICIO_COBERTURA";
    public static final String PAYLOAD_AJUSTE = "ajuste";

    public static final String PAYLOAD_BODY = "message";
    public static final Integer NOTIFICATION_ACCEPT_ADJUSTMENT = 0;
    public static final Integer NOTIFICATION_OTHER = 1;

    //constantes content Fragmmet Damage
    public static final String TAG_DAMAGE = "DamageartsConsecuencia";
    public static final String TAG_DAMAGEPREEXTISTENTE = "DamageartsPreexisting";
    public static final String TAG_PARTE_FALTANTE = "parte_faltante";

    //constantes dir save Image
    public static String CONDUCTOR = "Conductor";
    public static String VEHICULO = "Vehículo";
    public static String OCUPANTE = "Ocupante";
    public static String ASEGURADOS = "Asegurado";
    public static String TERCEROS = "Terceros";
    public static String DECLARACION = "Declaración";
    public static String RESPONSABILIDAD = "Responsabilidad";
    public static String RECUPERACION = "Recuperacion";
    public static String ARRIBO = "Arribo";
    public static String AUTOS = "Auto";
    public static String NO_AUTOS = "NoAuto";
    public static String PEATON = "Peaton";
    public static String MEDIA_PATH_DIR = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath()
            + File.separator + "AjustadorMovil" + File.separator;

    public static String CARD_DECLARACION = "1";
    public static String CARD_ASEGURADO = "2";
    public static String CARD_TERCEROS = "3";
    public static String CARD_RESPONSABILIDAD = "Responsabilidad";
    public static String SUB_MODULE_RESPONSABILIDAD = "Detalle de responsabilidad";
    public static String CARD_DOCUMENTOS = "Documentos";
    public static String SUB_MODULE_DOCUMENTS = "Impresión de volantes";
    public static String CARD_RECUPERACION = "6";
    public static String CARD_SERVICIOS = "7";

    public static String TAG_NAME_CAPTURE = "NameCapture";
    public static String TAG_AGE_CAPTURE = "AgeCaptureInsure";

    public static boolean turnOnAmis = false;


    public static final String FIRMA_DIGITAL = "FIRMA_DIGITAL";
    public static final String CLAVE_LICENCIA_CONDUCIR = "LC";


    //Para armar URL de Croquis
    public static String URL_CROQUIS = "http://maps.google.com/maps/api/staticmap?zoom=15&size=600x600&key=" + BuildConfig.GOOGLEMAPSAPIKEY + "&markers={0},{1}";

}

