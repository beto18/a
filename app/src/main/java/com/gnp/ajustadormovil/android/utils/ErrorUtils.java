package com.gnp.ajustadormovil.android.utils;

import android.util.Log;

import com.gnp.ajustadormovil.android.error.DataError;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.error.FieldError;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.error.ResourceError;
import com.gnp.ajustadormovil.android.error.ServerDataException;
import com.gnp.ajustadormovil.android.models.amis.AmisException;
import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import net.sf.oval.expression.ExpressionLanguageJEXLImpl;

import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;
import okhttp3.Response;
import okio.Buffer;

import static com.gnp.ajustadormovil.android.utils.core.ResourceUtils.getString;

public class ErrorUtils {

    public static final String TAG = ErrorUtils.class.getCanonicalName();

    public static String NOT_ACCEPTABLE_CODE = "not_acceptable";

    private static Validator VALIDATOR = new Validator();

    static {
        Validator.setMessageResolver(ResourceUtils::getString);
        VALIDATOR.getExpressionLanguageRegistry().registerExpressionLanguage("jexl",
                new ExpressionLanguageJEXLImpl());
    }

    private static String getField(ConstraintViolation violation){
        return violation
                .getContext()
                .toString()
                .substring(violation.getValidatedObject().getClass().getName().length() + 1);
    }

    /**
     * Obtiene un mensaje de error para mostrar.
     * @param throwable Error al que se desea mostrar su información.
     * @return Mensaje de error.
     */
    public static String getErrorMessage(Throwable throwable){
        if(throwable instanceof ModuloException) {
            return getErrorMessage((ModuloException) throwable);
        }
        if(throwable instanceof DataException) {
            return getErrorMessage((DataException) throwable);
        }
        if(throwable instanceof LogicException) {
            return getErrorMessage((LogicException) throwable);
        }
        if(throwable instanceof AmisException) {
            return getErrorMessage((AmisException) throwable);
        }
        String code = throwable.getClass().getName() + ".message";
        code = code.replaceAll("\\.", "_");
        String message = getString(code);
        if(message.equals(code)) {
            message = throwable.getMessage();
        }
        return message;
    }

    private static String getErrorMessage(DataException exception){
        String message = "";
        for(FieldError field : exception.getError().getFields()){
            if(message.length() > 0){
                message += "\n\n";
            }
            if(exception instanceof ServerDataException){
                message += "\u2022 " + field.getFieldName() + ": " + field.getMessage();
            } else {
                message += "\u2022 " + field.getMessage();
            }
        }
        return message;
    }

    private static String getErrorMessage(LogicException exception){
        String msg = getString(exception.getError().getError());
        if(msg.equals(exception.getError().getError())){
            msg = exception.getMessage();
        }
        return msg;
    }

    private static String getErrorMessage(ModuloException exception){
        String code = exception.getClass().getName() + ".message";
        code = code.replaceAll("\\.", "_");
        StringBuilder message = new StringBuilder(MessageFormat.format(getString(code), exception.getParams()));
        message.append("\n\n");
        if(!(exception.getCause() instanceof DataException)){
            message.append("\u2022 ");
        }
        message.append(getErrorMessage(exception.getCause()));
        return message.toString();
    }

    private static String getErrorMessage(AmisException exception){
        String code = exception.getClass().getName() + ".message";
        code = code.replaceAll("\\.", "_");
        StringBuilder message = new StringBuilder(getString(code))
                .append("\n\n")
                .append("\u2022 ")
                .append(exception.getMessage());
        return message.toString();
    }

    /**
     * Valida si un objeto es válido usando las anotaciones de OVal.
     * @param obj Objeto a validar.
     */
    public static DataError validate(Object obj){
        List<ConstraintViolation> violations = VALIDATOR.validate(obj);
        DataError error = new DataError();
        error.setFields(new ArrayList<>());
        if(!violations.isEmpty()) {
            error.setError(NOT_ACCEPTABLE_CODE);
            error.setMessage(getString(NOT_ACCEPTABLE_CODE));
            for(ConstraintViolation violation : violations){
                error.getFields().add(new FieldError(getField(violation), violation.getMessage()));
            }
        }
        return error;
    }

    /**
     * Valida si un objeto es válido usando las anotaciones de OVal.
     * @param obj Objeto a validar.
     * @throws DataException si el objeto tiene errores de datos.
     */
    public static void assertValues(Object obj){
        DataError error = validate(obj);
        if(error.hasErrors()){
            throw new DataException(error);
        }
    }

    /**
     * Encapsula el error en un modelo.
     * @param request Petición al servidor.
     * @param response Respuesta del servidor.
     * @return {@link ResourceError}.
     */
    public static ResourceError getResourceError(Request request, Response response){
        String req = "-";
        String res = "-";
        try {
            if(response.body() != null) {
                res = response.body().string();
            }
        } catch (Exception e){
            Log.w(TAG, "Error al obtener los datos del response");
        }
        try {
            if(request.body() != null) {
                Buffer buffer = new Buffer();
                request.body().writeTo(buffer);
                req = buffer.readString(Charset.defaultCharset());
            }
        } catch (Exception e){
            Log.w(TAG, "Error al obtener lso datos el request");
        }
        ResourceError error = new ResourceError();
        error.setMethod(request.method());
        error.setUrl(request.url().toString());
        error.setRequest(req);
        error.setResponse(res);
        return error;
    }

}
