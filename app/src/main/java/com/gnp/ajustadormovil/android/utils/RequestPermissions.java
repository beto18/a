package com.gnp.ajustadormovil.android.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

public class RequestPermissions {

    public static final int REQUEST_WRITE_EXTERNAL_STORAGE = 11;
    public static final int REQUEST_LOCATION_PERMISSION = 10;
    public static final int REQUEST_CAMERA_PERMISSION = 9;
    public static final int REQUEST_CALL_PERMISSION = 8;
    public static final int REQUEST_STORAGE_PERMISSION = 7;

    private static String[] locationRequestPermissons = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private static String[] callRequestPermissons = new String[]{Manifest.permission.CALL_PHONE};
    private static String[] cameraRequestPermissons = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static String[] storageRequestPermissons = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};


    public static void requestPermission(int option, final Activity activity) {
        switch (option) {
            case REQUEST_LOCATION_PERMISSION:
                DialogUtils.createAlert(activity, "Permisos necesarios",
                        "Se requieren permisos para obtener la localización",
                        AjustadorMovilApplication.getInstance().getString(R.string.text_button_continue),
                        "", (dialog, which)
                                -> ActivityCompat.requestPermissions(activity,
                                locationRequestPermissons, REQUEST_LOCATION_PERMISSION), null);

                break;
            case REQUEST_CAMERA_PERMISSION:
                DialogUtils.createAlert(activity, "Permisos necesarios",
                        "Se requieren permisos para tomar fotos de la cámara",
                        AjustadorMovilApplication.getInstance().getString(R.string.text_button_continue),
                        "", (dialog, which)
                                -> ActivityCompat.requestPermissions(activity,
                                cameraRequestPermissons, REQUEST_CAMERA_PERMISSION), null);
                break;
            case REQUEST_CALL_PERMISSION:
                DialogUtils.createAlert(activity, "Permisos necesarios",
                        "Se requieren permisos para realizar llamadas del celular",
                        AjustadorMovilApplication.getInstance().getString(R.string.text_button_continue),
                        "", (dialog, which)
                                -> ActivityCompat.requestPermissions(activity,
                                callRequestPermissons, REQUEST_CALL_PERMISSION), null);
                break;
            case REQUEST_STORAGE_PERMISSION:
                DialogUtils.createAlert(activity, "Permisos necesarios",
                        "Se requieren permisos para leer archivos del celular",
                        AjustadorMovilApplication.getInstance().getString(R.string.text_button_continue),
                        "", (dialog, which)
                                -> ActivityCompat.requestPermissions(activity,
                                storageRequestPermissons, REQUEST_CALL_PERMISSION), null);
                break;
        }
    }


    private static boolean checkPermissons(String[] permissonsRequested) {
        for (String aPermissonsRequested : permissonsRequested) {
            if (Build.VERSION.SDK_INT >= 23 && AjustadorMovilApplication.getInstance()
                    .checkSelfPermission(aPermissonsRequested) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkPermissions(int option) {
        switch (option) {
            case REQUEST_LOCATION_PERMISSION:
                return checkPermissons(locationRequestPermissons);

            case REQUEST_CALL_PERMISSION:
                return checkPermissons(callRequestPermissons);

            case REQUEST_CAMERA_PERMISSION:
                return checkPermissons(cameraRequestPermissons);

            case REQUEST_STORAGE_PERMISSION:
                return checkPermissons(storageRequestPermissons);

            default:
                return false;
        }
    }

}
