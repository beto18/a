package com.gnp.ajustadormovil.android.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;

import static com.gnp.ajustadormovil.android.models.GlobalUrlEngine.GlobalUrlEngine_Catalogos;
import static com.gnp.ajustadormovil.android.models.GlobalUrlEngine.GlobalUrlEngine_LogIn_Aut;
import static com.gnp.ajustadormovil.android.models.GlobalUrlEngine.GlobalUrlEngine_Orquestador;
import static com.gnp.ajustadormovil.android.models.GlobalUrlEngine.GlobalUrlEngine_Version;

/**
 * Created by Abraham on 19/12/2017.
 */

public class Utils {

    public static final Gson GSON = new Gson();

    public final static String domainLink = "https://rjf85.app.goo.gl";
    public static final String BASE_URL_LOGIN= GlobalUrlEngine_LogIn_Aut;
    public static final String BASE_URL_METADATA= GlobalUrlEngine_Version;
    public static final String AUTH_URL= "login";

    public static final String BASE_URL_CAPTURE = GlobalUrlEngine_Orquestador;
    public static final String BASE_URL_CATALOGOS = GlobalUrlEngine_Catalogos;
    public static final String DATA_FOLDER = Environment.getExternalStorageDirectory().getPath() + "/data";
    public static final String TEMP_FOLDER = Environment.getExternalStorageDirectory().getPath() + "/tmp";
    public static final String DATA_CAT_NAME = "cat.db";
    public static final String DATA_SINAU_NAME = "sinau.db";
    public static final String DATA_TPR_NAME = "tpr.db";
    public static final String BASE_URL_ADD = GlobalUrlEngine_Orquestador;
    public static final String BASE_URL_DEV = GlobalUrlEngine_Orquestador;
    public static final String NUMBER_AMBULANCE = "911";

    public static String removeFormat(String s){
        return s.replaceAll("[$,]", "");
    }

    public static String objectoToJsonString(Object object) {
        return GSON.toJson(object);
    }

    public static <T> T jsonToObject(Class<T> clazz, String jsonString) {
        return GSON.fromJson(jsonString,clazz);
    }

    public static String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }

    public static File compressImage(Bitmap image,String name) throws IOException {
        Bitmap photo = image;
        photo = Bitmap.createScaledBitmap(photo, (int) (image.getWidth() * 0.25), (int) (image.getWidth() * 0.25), false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 80, bytes);

        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + name);
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }

    public static byte[] bytesPeerImage(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap,
                bitmap.getWidth(),bitmap.getWidth(), false);
        bitmapResized.compress(Bitmap.CompressFormat.PNG, 70, stream);
        return stream.toByteArray();
    }

    private static Pattern patternMoral = Pattern.compile("^[A-Z]{3}[0-9]{6}[0-9A-Z]{3}$");
    private static Pattern patternFisica = Pattern.compile("^[A-Z]{4}[0-9]{6}[0-9A-Z]{3}$");

    public static boolean validateRFCMoral(String rfc){
        return patternMoral.matcher(rfc).find();
    }

    public static boolean validateRFCFisica(String rfc){
        return patternFisica.matcher(rfc).find();
    }

    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(activity);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(activity, result,
                        69).show();
            }
            else {
                Log.i("PLAYCHECK", "Dispositivo no soportado.");
            }
            return false;
        }
        return true;
    }

    public static void hideKeyboard(EditText myEditText,Context context){
        InputMethodManager imm = (InputMethodManager)context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    public static void showKeyboard(EditText myEditText, Context context){
        myEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    //TODO: validar generacion de folio para AMIS
    @SuppressLint("DefaultLocale")
    public static String getFolio(String cedula, String fechaAtencion, String codigo){
        return String.format("%s%s%s%03d", "GNP", cedula, fechaAtencion, codigo);
    }

}

