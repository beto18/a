package com.gnp.ajustadormovil.android.utils.codec;

import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Lennken Group on 25/04/19.
 */
public class DecodeEngine {

    private static final String TAG = DecodeEngine.class.getSimpleName();

    private static final String HASH_ALGORITHM = "SHA-256";
    private static final String CHARSET = "UTF-8";
    private static final String AES_MODE = "AES/CBC/PKCS5Padding";
    private static final String SHA_RSA = "SHA1withRSA";
    private static final String RSA = "RSA";
    private static final String AES = "AES";

    /**
     * Método para decodificar la cadena de texto
     *
     * @param key   Folio eDUA
     * @param value Valor a decodificar
     */
    public static String decode(String key, String value) {
        String messageAfterDecrypt = "";
        try {
            final SecretKeySpec secretKeySpec = generateKey(key);
            byte[] decodedCipherText = decodeBase64(value);
            byte[] iv = new byte[16];
            for (int i = 0; i < 16; i++) {
                iv[i] = secretKeySpec.getEncoded()[i];
            }
            byte[] decryptedBytes = decrypt(secretKeySpec, iv, decodedCipherText);
            messageAfterDecrypt = new String(decryptedBytes, CHARSET);
        } catch (UnsupportedEncodingException | GeneralSecurityException e) {
            Log.e(TAG, "Error al realizar decodificación", e);
        }
        return messageAfterDecrypt;
    }

    /**
     * Método para codificar la cadena de texto
     *
     * @param key   Folio eDUA
     * @param value Valor a codificar
     */
    public static String encode(final String key, String value) {
        String encryptedMsg = "";
        try {
            final SecretKeySpec secretKeySpec = generateKey(key);
            byte[] iv = new byte[16];
            for (int i = 0; i < 16; i++) {
                iv[i] = secretKeySpec.getEncoded()[i];
            }
            byte[] cipherText = encrypt(secretKeySpec, iv, value.getBytes(CHARSET));
            encryptedMsg = encodeBase64ToString(cipherText);
        } catch (UnsupportedEncodingException | GeneralSecurityException e) {
            Log.e(TAG, "Error al realizar codificación", e);
        }
        return encryptedMsg;
    }

    /**
     * More flexible AES encrypt that doesn't encode
     *
     * @param key     AES key typically 128, 192 or 256 bit
     * @param iv      Initiation Vector
     * @param message in bytes (assumed it's already been decoded)
     * @return Encrypted cipher text (not encoded)
     * @throws GeneralSecurityException if something goes wrong during
     *                                  encryption
     */
    public static byte[] encrypt(final SecretKeySpec key, final byte[] iv, final byte[] message)
            throws GeneralSecurityException {
        final Cipher cipher = Cipher.getInstance(AES_MODE);
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
        byte[] cipherText = cipher.doFinal(message);
        return cipherText;
    }

    /**
     * More flexible AES decrypt that doesn't encode
     *
     * @param key               AES key typically 128, 192 or 256 bit
     * @param iv                Initiation Vector
     * @param decodedCipherText in bytes (assumed it's already been decoded)
     * @return Decrypted message cipher text (not encoded)
     * @throws GeneralSecurityException if something goes wrong during
     *                                  encryption
     */
    public static byte[] decrypt(final SecretKeySpec key, final byte[] iv, final byte[] decodedCipherText)
            throws GeneralSecurityException {
        final Cipher cipher = Cipher.getInstance(AES_MODE);
        IvParameterSpec ivSpec = new IvParameterSpec(iv);
        cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
        byte[] decryptedBytes = cipher.doFinal(decodedCipherText);
        return decryptedBytes;
    }

    /**
     * Generates SHA256 hash of the password which is used as key
     *
     * @param password used to generated key
     * @return SHA256 of the password
     */
    private static SecretKeySpec generateKey(final String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
        byte[] bytes = password.getBytes(CHARSET);
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, AES);
        return secretKeySpec;
    }

    public static String getKey() {
        try {
            byte[] bytes = {76, 110, 107, 46, 73, 110, 110, 111, 118, 97, 99, 105, 111, 110, 46, 50, 48, 49, 56};
            Charset charset = Charset.forName(CHARSET);
            CharsetDecoder decoder = charset.newDecoder();
            ByteBuffer srcBuffer = ByteBuffer.wrap(bytes);
            CharBuffer resBuffer = decoder.decode(srcBuffer);
            StringBuilder key = new StringBuilder(resBuffer);
            return key.toString();
        } catch (Exception e) {
            StringBuilder key = new StringBuilder();
            return key.toString();
        }

    }

    public static PrivateKey getPrivateKeyFromBase64(String base64) throws GeneralSecurityException {
        String pk = new String(Base64.decode(base64, Base64.DEFAULT));
        pk = pk.replace("-----BEGIN PRIVATE KEY-----", "");
        pk = pk.replace("-----END PRIVATE KEY-----", "");
        byte[] encoded = decodeBase64(pk);
        KeyFactory kf = KeyFactory.getInstance(RSA);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        return kf.generatePrivate(keySpec);
    }

    public static PublicKey getPublicKeyFromBase64(String base64) throws GeneralSecurityException {
        String pk = new String(Base64.decode(base64, Base64.DEFAULT));
        pk = pk.replace("-----BEGIN PUBLIC KEY----", "");
        pk = pk.replace("-----END PUBLIC KEY-----", "");
        byte[] encoded = decodeBase64(pk);
        KeyFactory kf = KeyFactory.getInstance(RSA);
        return kf.generatePublic(new X509EncodedKeySpec(encoded));
    }

    public static String sign(PrivateKey privateKey, String message) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException {
        Signature sign = Signature.getInstance(SHA_RSA);
        sign.initSign(privateKey);
        sign.update(message.getBytes(CHARSET));
        return new String(encodeBase64(sign.sign()), CHARSET);
    }

    public static boolean verifySignature(PublicKey publicKey, String message, String signature) throws SignatureException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        Signature sign = Signature.getInstance(SHA_RSA);
        sign.initVerify(publicKey);
        sign.update(message.getBytes(CHARSET));
        return sign.verify(decodeBase64(signature.getBytes(CHARSET)));
    }

    /**
     * Obtiene el número serial del certificado.
     *
     * @param base64 Base 64 del certificado.
     * @return Número serial del certificado.
     */
    public static String getSerialNumber(String base64) throws CertificateException {
        byte[] bytes = decodeBase64(base64);
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) cf.generateCertificate(in);
        return certificate.getSerialNumber().toString(16);
    }

    private static byte[] decodeBase64(byte[] bytes) {
        return Base64.decode(bytes, Base64.DEFAULT);
    }

    private static byte[] encodeBase64(byte[] bytes) {
        return Base64.encode(bytes, Base64.DEFAULT);
    }

    private static byte[] decodeBase64(String base64) {
        return Base64.decode(base64, Base64.DEFAULT);
    }

    private static String encodeBase64ToString(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

}