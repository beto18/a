package com.gnp.ajustadormovil.android.utils.commons;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.realm.RealmList;

public final class CollectionsUtils {

    public static class CollectionException extends RuntimeException {

        public CollectionException(Throwable cause){
            super(cause);
        }

    }

    public static <T> RealmList<T> toRealmList(Collection<T> collection){
        if(collection == null){
            return null;
        }
        RealmList<T> result = new RealmList<>();
        for(T element : collection){
            result.add(element);
        }
        return result;
    }

    public static <T> List<T> toList(Collection<? extends T>... collections){
        if(collections == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (Collection<? extends T> collection : collections) {
            if(collection != null) {
                for (T element : collection) {
                    result.add(element);
                }
            }
        }
        return result;
    }

    public static <T> void removeIf(Collection<T> collection, Predicate<T> predicate) {
        if(collection != null) {
            final Iterator<T> each = collection.iterator();
            while (each.hasNext()) {
                try {
                    if (predicate.test(each.next())) {
                        each.remove();
                    }
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
    }

    public static <T> void forEach(Collection<T> collection, Consumer<T> consumer) {
        if(collection != null) {
            for(T item : collection){
                try {
                    consumer.accept(item);
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
    }

    public static <T> boolean anyMatch(Collection<T> collection, Predicate<T> predicate){
        boolean match = false;
        if(collection != null) {
            for(T item : collection){
                try {
                    match |= predicate.test(item);
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
        return match;
    }

    public static <T> boolean allMatch(Collection<T> collection, Predicate<T> predicate){
        boolean match = true;
        if(collection != null) {
            for(T item : collection){
                try {
                    match &= predicate.test(item);
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
        return match;
    }

    public static <T> T firstMatch(Collection<T> collection, Predicate<T> predicate){
        if(collection != null) {
            for(T item : collection){
                try {
                    if(predicate.test(item)) {
                        return item;
                    }
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
        return null;
    }

    public static <T> Collection<T> filter(Collection<T> collection, Predicate<T> predicate){
        List<T> result = new ArrayList<>();
        if(collection != null) {
            for(T item : collection){
                try {
                    if(predicate.test(item)) {
                        result.add(item);
                    }
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
        return result;
    }

    public static <T, O> Collection<O> map(Collection<T> collection, Function<T, O> mapper){
        List<O> result = new ArrayList<>();
        if(collection != null) {
            for(T item : collection){
                try {
                    result.add(mapper.apply(item));
                } catch (Exception e) {
                    throw new CollectionException(e);
                }
            }
        }
        return result;
    }

    public static <T> Collection<T> distinct(Collection<T> collection){
        List<T> result = new ArrayList<>();
        if(collection != null) {
            for(T item : collection){
                if(!result.contains(item)) {
                    result.add(item);
                }
            }
        }
        return result;
    }

}
