package com.gnp.ajustadormovil.android.utils.commons;

import android.databinding.InverseMethod;
import android.view.View;

public class Converters {

    @InverseMethod("stringToInteger")
    public static String integerToString(Integer value) {
        return value == null ? "" : value.toString();
    }

    @InverseMethod("integerToString")
    public static Integer stringToInteger(String value) {
        return value == null || "".equals(value.trim()) ? null : Integer.parseInt(value);
    }

    @InverseMethod("stringToLong")
    public static String longToString(Long value) {
        return value == null ? "" : value.toString();
    }

    @InverseMethod("longToString")
    public static Long stringToLong(String value) {
        return value == null || "".equals(value.trim()) ? null : Long.parseLong(value);
    }


    @InverseMethod("booleantToVisibility")
    public static boolean visibilityToBolean(int value) {
        return value == View.VISIBLE;
    }

    @InverseMethod("visibilityToBolean")
    public static int booleantToVisibility(boolean value) {
        return value ? View.VISIBLE : View.GONE;
    }

    @InverseMethod("stringToShort")
    public static String shortToString(Short value){
        return value == null ? "" : value.toString();
    }

    @InverseMethod("shortToString")
    public static Short stringToShort(String value){
        return value == null || "".equals(value.trim()) ? null : Short.parseShort(value);
    }

    @InverseMethod("stringToDouble")
    public static String doubleToString(Double value) {
        return value == null ? "" : value.toString();
    }

    @InverseMethod("doubleToString")
    public static Double stringToDouble(String value) {
        return value == null || "".equals(value.trim()) ? null : Double.parseDouble(value);
    }
}
