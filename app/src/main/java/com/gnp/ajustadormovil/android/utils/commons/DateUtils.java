package com.gnp.ajustadormovil.android.utils.commons;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtils {

    public static final String DATETIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    public static final String DATE_PATTERN = "dd/MM/yyyy";

    public static final String DATE_ISO_PATTERN = "yyyy-MM-dd";

    public static final String TIME_PATTERN = "hh:mm:ss aa";

    public static final String eDUA_PATTERN = "ddMMyyHHmmss";

    public static final String TIME_ISO_PATTERN = "HH:mm:ss";

    public static String date(Date date){ return toString(date, DATE_PATTERN); }

    public static String datetime(Date date){ return toString(date, DATETIME_PATTERN); }

    public static String time(Date date){
        return toString(date, TIME_PATTERN);
    }

    public static Date date(String value){
        return toDate(value, DATE_PATTERN);
    }

    public static Date dateIso(String value){
        return toDate(value, DATE_ISO_PATTERN);
    }

    public static Date timeIso(String value){
        return toDate(value, TIME_ISO_PATTERN);
    }

    public static String dateTimeEDUA(Date date){
        return toString(date,eDUA_PATTERN);
    }

    public static String timeIso(Date value){
        return toString(value, TIME_ISO_PATTERN);
    }

    public static String dateIso(Date date){ return toString(date, DATE_ISO_PATTERN); }

    public static Date time(String value){
        return toDate(value, TIME_PATTERN);
    }

    public static Date datetime(String value){
        return toDate(value, DATETIME_PATTERN);
    }

    public static int yearsAgo(Date date) {
        if(date == null) {
            return -1;
        }
        return yearsBetween(date, new Date());
    }

    public static int yearsBetween(Date start, Date end) {
        if(start == null || end == null) {
            return -1;
        }
        return Years.yearsBetween(LocalDate.fromDateFields(start), LocalDate.fromDateFields(end))
                .getYears();
    }

    public static Date toDate(String value, String pattern){
        if(value == null) {
            return null;
        }
        try {
            return new SimpleDateFormat(pattern).parse(value);
        } catch (ParseException e){
            throw new RuntimeException(e);
        }
    }

    public static String toString(Date value, String pattern){
        if(value == null) {
            return null;
        }
        return new SimpleDateFormat(pattern).format(value);
    }

    public static String dateToDateIso(String value){
        return dateIso(date(value));
    }

    public static String dateIsoToDate(String value){
        return date(dateIso(value));
    }

}
