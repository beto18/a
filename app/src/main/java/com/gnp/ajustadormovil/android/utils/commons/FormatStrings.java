package com.gnp.ajustadormovil.android.utils.commons;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * Created by hgzavala on 10/04/2018.
 */

public final class FormatStrings {

    private static final String TAG = FormatStrings.class.getCanonicalName();

    private final static Pattern EMAIL_ADDRESS_PATTERN = Pattern
            .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");


    private FormatStrings() {
        throw new RuntimeException("No instaciar esta clase");
    }

    public static String formatearFecha(String f) {
        String fechaFormateada = null;
        DateFormat entrada = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        DateFormat salida = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date;
        try {
            if (f != null) {
                date = entrada.parse(f);
                fechaFormateada = salida.format(date) + " hrs";
            } else {
                fechaFormateada = "";
            }
        } catch (ParseException e) {
            Log.e(TAG, "Error al formatear fecha", e);
        }
        return fechaFormateada;
    }

    public static String formatearFechaYYYMMDD(String f, char separador) {
        String fechaFormateada = null;
        DateFormat entrada = new SimpleDateFormat("yyyy" + separador + "MM" + separador + "dd");
        DateFormat salida = new SimpleDateFormat("dd/MM/yyyy");
        Date date;
        try {
            if (f != null) {
                date = entrada.parse(f);
                fechaFormateada = salida.format(date);
            } else {
                fechaFormateada = "";
            }
        } catch (ParseException e) {
            Log.e(TAG, "Error al formatear fecha", e);
        }
        return fechaFormateada;
    }

    public static String formatDateMobile(String f) {
        String dateFormatted = null;
        DateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ROOT);
        DateFormat output = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ROOT);
        Date date;
        try {
            if (f != null) {
                input.setTimeZone(TimeZone.getTimeZone(String.valueOf(TimeZone.getDefault())));
                date = input.parse(f);
                dateFormatted = output.format(date) + " hrs";
            } else {
                dateFormatted = "";
            }
        } catch (ParseException e) {
            Log.e(TAG, "Error al formatear fecha", e);
        }
        return dateFormatted;
    }

    public static boolean validateText(String text) {
        return text.matches("[a-zA-ZÀ-ÿ 'ñÑ]*");

    }

    private static String substituyeFecha(String f) {

        if (f != null)
            return f.replace("T", " ").replace(".000", "").replace("Z", "");
        else
            return f;

    }

    public static boolean validarEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }


}
