package com.gnp.ajustadormovil.android.utils.commons;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.util.concurrent.CountDownLatch;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RXUtils {

    private RXUtils() {
    }

    public static <T> ObservableTransformer<T, T> applySchedulers() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> SingleTransformer<T, T> applySingleSchedulers() {
        return upstream -> upstream.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static boolean isNetworkEnabled() {
        boolean connected = false;
        ConnectivityManager cm = (ConnectivityManager) AjustadorMovilApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (typeConnectionInternet(activeNetwork)) {
                connected = true;
            }
        }
        return connected;
    }

    private static boolean typeConnectionInternet(NetworkInfo activeNetwork){
        return activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    /**
     * Hace sincronó una operación de completable.
     * @param completable El completable que se requiere hacer sincróno.
     * @throws Exception Si existe un error en la ejecución del completable.
     */
    public static void sync(Completable completable) throws Exception {
        final CountDownLatch signal = new CountDownLatch(1);
        ValueObject<Throwable> error = new ValueObject<>();
        Disposable disposable = completable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(() -> signal.countDown(), throwable -> {
                    error.setValue(throwable);
                    signal.countDown();
                });
        try {
            signal.await();
            disposable.dispose();
        } catch (InterruptedException e){
            Log.w("RXUtils", "Se suspendio el hilo de ejecución", e);
            disposable.dispose();
            throw new NetworkException("Comunicación interrumpida");
        }
        if(error.getValue() != null){
            throw error.getValue() instanceof Exception ? (Exception) error.getValue() : new Exception(error.getValue());
        }
    }


    /**
     * Hace sincronó una operación de observable.
     * @param observable El observable que se desea tener sincronó.
     * @param <T> El tipo parámetro que arroja el observable.
     * @return El valor de returno del onNext del observable.
     */
    public static <T> T sync(Observable<T> observable) throws Exception {
        final CountDownLatch signal = new CountDownLatch(1);
        ValueObject<T> result = new ValueObject<>();
        ValueObject<Throwable> error = new ValueObject<>();
        Disposable disposable = observable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(res -> {
                    result.setValue(res);
                    signal.countDown();
                }, throwable -> {
                    error.setValue(throwable);
                    signal.countDown();
                });
        try {
            signal.await();
            disposable.dispose();
        } catch (InterruptedException e){
            Log.w("RXUtils", "Se suspendio el hilo de ejecución", e);
            if(!disposable.isDisposed()) {
                disposable.dispose();
            }
            throw new NetworkException("Comunicación interrumpida");
        }
        if(error.getValue() != null){
            throw error.getValue() instanceof Exception ? (Exception) error.getValue() : new Exception(error.getValue());
        }
        return result.getValue();
    }

    public static <T> T sync(Single<T> single) throws Exception {
        final CountDownLatch signal = new CountDownLatch(1);
        ValueObject<T> result = new ValueObject<>();
        ValueObject<Throwable> error = new ValueObject<>();
        Disposable disposable = single
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(res -> {
                    result.setValue(res);
                    signal.countDown();
                }, throwable -> {
                    error.setValue(throwable);
                     signal.countDown();
                });
        try {
            signal.await();
            disposable.dispose();
        } catch (InterruptedException e){
            Log.w("RXUtils", "Se suspendio el hilo de ejecución", e);
            disposable.dispose();
            throw new NetworkException("Comunicación interrumpida");
        }
        if(error.getValue() != null){
            throw error.getValue() instanceof Exception ? (Exception) error.getValue() : new Exception(error.getValue());
        }
        return result.getValue();
    }


}
