package com.gnp.ajustadormovil.android.utils.core;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

public class ConnectivityUtils {


    public static NetworkInfo getNetworkInfo(){
        Context context = AjustadorMovilApplication.getInstance().getBaseContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(){
        NetworkInfo info = ConnectivityUtils.getNetworkInfo();
        return (info != null && info.isConnected());
    }

    public static boolean isConnectedWifi(){
        NetworkInfo info = ConnectivityUtils.getNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public static boolean isConnectedMobile(){
        NetworkInfo info = ConnectivityUtils.getNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public static boolean isConnectedFast(){
        NetworkInfo info = ConnectivityUtils.getNetworkInfo();
        return (info != null && info.isConnected() && ConnectivityUtils.isConnectionFast(info.getType(),info.getSubtype()));
    }


    public static boolean isConnectionFast(int type, int subType){
        if(type==ConnectivityManager.TYPE_WIFI){
            return true;
        } else if(type==ConnectivityManager.TYPE_MOBILE) {
            return subType > TelephonyManager.NETWORK_TYPE_EDGE;
        } else {
            return false;
        }
    }

}
