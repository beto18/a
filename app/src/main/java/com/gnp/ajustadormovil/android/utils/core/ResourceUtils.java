package com.gnp.ajustadormovil.android.utils.core;

import android.content.Context;

import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;

public class ResourceUtils {

    private static Context CONTEXT = AjustadorMovilApplication.getInstance().getBaseContext();


    public static String getString(String key){
        int id = CONTEXT.getResources().getIdentifier(key.replaceAll("\\.", "_"),
                "string", CONTEXT.getPackageName());
        return id == 0 ? key : CONTEXT.getResources().getString(id);
    }

    public static String getString(String key, Object ... params){
        String message = getString(key);
        return MessageFormat.format(message, params);
    }

    public static String getString(int identifier){
        return CONTEXT.getResources().getString(identifier);
    }

    public static InputStream getFile(String filename) throws IOException {
        return CONTEXT.getResources().getAssets().open(filename);
    }

}
