package com.gnp.ajustadormovil.android.utils.media;

import android.content.Context;

import com.gnp.ajustadormovil.android.interactors.orquestador.documents.DocumentException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class FileUtils {

    /**
     * Crea un nuevo archivo
     * @param file Archivo a crear.
     * @throws IOException Si existe algún problema de acceso a datos.
     */
    public static void createFile(File file) throws IOException {
        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
        }
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
    }

    public static InputStream getFile(Context context, String asset) throws IOException {
        InputStream template = context.getAssets().open(asset);
        return template;
    }



    /**
     * Obtiene los bytes de un archivo.
     * @param in Archivo.
     * @return Bytes del archivo.
     * @throws IOException
     */
    public static byte[] getBytes(InputStream in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int count;
            while ((count = in.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }
            out.flush();
            out.close();
            in.close();
            return out.toByteArray();
        } catch (IOException e){
            throw new DocumentException(e);
        }
    }

    /**
     * Obtiene el contenido en texto de un archivo.
     * @param in Archivo.
     * @return Contenido del archivo.
     * @throws IOException
     */
    public static String getContentFile(InputStream in) {
        return new String(getBytes(in));
    }

    /**
     * Copia un archivo a otra dirección.
     * @param source Archivo fuente.
     * @param target Archivo destino.
     * @throws IOException Si ocurre un problema a acceder a datos.
     */
    public static void copy(InputStream source, OutputStream target) throws IOException {
        byte[] buffer = new byte[1024];
        int lengthRead;
        while ((lengthRead = source.read(buffer)) > 0) {
            target.write(buffer, 0, lengthRead);
        }
        target.close();
        source.close();
    }

    /**
     * Copia un archivo a otra dirección.
     * @param source Archivo fuente.
     * @param target Archivo destino.
     * @throws IOException Si ocurre un problema a acceder a datos.
     */
    public static void copy(File source, File target) throws IOException {
        if(!source.exists()){
            throw new FileNotFoundException(source.getAbsolutePath());
        }
        createFile(target);
        InputStream in = new BufferedInputStream(new FileInputStream(source));
        OutputStream out = new BufferedOutputStream(new FileOutputStream(target));
        copy(in, out);
    }

    /**
     * Copia un archivo a otra dirección.
     * @param source Archivo fuente.
     * @param target Archivo destino.
     * @throws IOException Si ocurre un problema a acceder a datos.
     */
    public static void copy(String source, String target) throws IOException {
        copy(new File(source), new File(target));
    }

    /**
     * Copia un archivo a otra dirección.
     * @param source Archivo fuente.
     * @param target Archivo destino.
     * @throws IOException Si ocurre un problema a acceder a datos.
     */
    public static void copy(File source, String target) throws IOException {
        copy(source, new File(target));
    }

}
