package com.gnp.ajustadormovil.android.utils.media

import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.webkit.MimeTypeMap
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath
import com.gnp.ajustadormovil.android.utils.Constants
import com.gnp.ajustadormovil.android.AjustadorMovilApplication
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import javax.annotation.Nonnull
import android.R.attr.bitmap




class ImageUtils {

    private val TAG: String = ImageUtils.javaClass.canonicalName

    fun saveImage(filePath: String, fileName: String, image: Bitmap): Boolean {
        return try {

            val fos = FileOutputStream(File(filePath, fileName))
            image.compress(Bitmap.CompressFormat.PNG, 95, fos)

            fos.flush()
            fos.close()
            true
        } catch (e: Exception) {
            Log.e(TAG, "Error al guardar imagen", e)
            false
        }
    }

    fun saveImages(bitmapImage: Bitmap): String? {

        val root = PersistenciaPath.pathFolder
        val myDir = File(root)
        if (!myDir.exists()) {
            myDir.mkdirs()
        }

        val n = System.currentTimeMillis()
        val iname = "$n.jpeg"
        val file = File(myDir, iname)
        if (file.exists())
            file.delete()
        try {
            val out = FileOutputStream(file)
            val bitmapImageResize = getResizedBitmap(bitmapImage,400, "", "");
            bitmapImageResize.compress(Bitmap.CompressFormat.PNG, 100, out)

            out.flush()
            out.close()

        } catch (e: Exception) {
            Log.e(TAG, "Error al guardar imagenes", e)
        }

        return file.absolutePath

    }




    fun getImageFromPath(path: String): Bitmap {
        val image = decodeSampledBitmapFromFile(path, 1200, 700)
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 70, baos)
        return image
    }

    fun getImageFromPathResponsability(path: String): Bitmap {
        val image = decodeSampledBitmapFromFile(path, 1200, 300)
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 70, baos)
        return image
    }

    fun getBitmapToFile(@Nonnull phat:String): Bitmap? {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        return BitmapFactory.decodeFile(phat, options)
    }

    fun getImageFromPath(path: String,width:Int,height: Int): Bitmap {
        val image = decodeSampledBitmapFromFile(path!!, width, height)
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 100, baos)
        return image
    }

    fun getMimeType(uri: Uri): String? {
        val mimeType: String?
        mimeType = if (uri.scheme == ContentResolver.SCHEME_CONTENT) {
            val cr = AjustadorMovilApplication.getInstance().contentResolver
            cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString())
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase())
        }
        return mimeType
    }

    fun bitmapToBase64(bitmap: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    fun base64ToBitmap(base64: String) : Bitmap{
        val decodedString = Base64.decode(base64, Base64.DEFAULT)
        var decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        decodedByte = Bitmap.createScaledBitmap(decodedByte,800, 200, false)
        return decodedByte

    }

    private fun decodeSampledBitmapFromFile(path: String, reqWidth: Int, reqHeight: Int): Bitmap {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        val first = BitmapFactory.decodeFile(path, options)
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)
        options.inJustDecodeBounds = false
        val second = BitmapFactory.decodeFile(path, options)
        if (second == null && first != null) {
            return first
        } else if(second != null) {
            return second
        } else {
            val bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_4444)
            val canvas = Canvas(bitmap)
            canvas.drawColor(Color.WHITE)
            return bitmap
        }
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())

            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }

        return inSampleSize
    }

    fun rotateImageBitmap(phat:String,bitmap:Bitmap):Bitmap{
        val ei = ExifInterface(phat)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> return  rotateImage(bitmap, 90f)

            ExifInterface.ORIENTATION_ROTATE_180 -> return rotateImage(bitmap, 180f)

            ExifInterface.ORIENTATION_ROTATE_270 -> return  rotateImage(bitmap, 270f)

            ExifInterface.ORIENTATION_NORMAL -> return  BitmapFactory.decodeFile(phat)
            else -> return  BitmapFactory.decodeFile(phat)
        }
    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height,
                matrix, true)
    }

    fun dp2px(context: Context, dipValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dipValue * scale + 0.5f).toInt()
    }




    fun deleteImage(activity: Activity,imageList:List<ImageGeneralModel> ) {

        for (f in imageList.indices) {
            if (imageList[f].newFile!!) {

                val contentResolver = activity.contentResolver
                contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Images.ImageColumns.DATA + "=?", arrayOf<String>(imageList[f].filePhat))

            }
        }

    }

    fun deleteImageForpath(path: String, activity: Activity){

        val contentResolver = activity.contentResolver
        contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DATA + "=?", arrayOf(path))

    }

    fun renameImageUpload(imageList: List<ImageGeneralModel>) {

        for (i in imageList.indices){
            if(imageList[i].filePhat != null) {
                val photo = File(imageList[i].filePhat)
                val fileName = photo.name.split(".")

                val newFile = File(photo.parentFile, fileName[0] + "_1" +".jpeg")
                photo.renameTo(newFile)
            }
        }

    }

    fun getResizedBitmap(image: Bitmap, maxSize: Int, tipoFoto: String, dataAjuste: String): Bitmap {
        var width = image.width
        var height = image.height

        var newWidth = 0
        var newHeigth = 0
//        if(width > height){
//            newHeigth = MAX_SIZE
//            newWidth = (width / height) * MAX_SIZE
//        }
//        else{
//            newHeigth = MAX_SIZE
//            newWidth = (height / width) * MAX_SIZE
//        }
        val bitmapRatio = width.toFloat() / height.toFloat()
        if (bitmapRatio > 1) {
            width = 1200
            height = (width / bitmapRatio).toInt()
        } else {
            height = 700
            width = (height * bitmapRatio).toInt()
        }
        Log.d("WIDTH:", newHeigth.toString())
        Log.d("HEIGHT", newWidth.toString())
        var bitmapGenerado = Bitmap.createScaledBitmap(image, width, height, true)

        bitmapGenerado = addBorderToBitmap(bitmapGenerado, tipoFoto, dataAjuste)
        return bitmapGenerado
    }


    // Método para dibujar marco a imagen
    fun addBorderToBitmap(srcBitmap: Bitmap, tipoFoto: String, dataAjuste: String): Bitmap {

        val borderWidth = Constants.TAMAÑO_MARGEN

        val workingBitmap = Bitmap.createBitmap(srcBitmap)
        val mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true)

        val canvasImg = Canvas(mutableBitmap)

        val paintMarco = Paint()
        paintMarco.color = Color.parseColor(Constants.TRANSPARENT_BLACK)
        paintMarco.style = Paint.Style.STROKE
        paintMarco.strokeWidth = borderWidth.toFloat()
        paintMarco.isAntiAlias = true

        val paintTipoImagen: Paint
        paintTipoImagen = Paint()
        paintTipoImagen.color = Color.WHITE
        paintTipoImagen.textSize = 200f  //set text size
        paintTipoImagen.textAlign = Paint.Align.CENTER

        val paintTextSiniestro: Paint
        paintTextSiniestro = Paint()
        paintTextSiniestro.color = Color.WHITE
        paintTextSiniestro.textSize = 80f  //set text size

        val rect = Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvasImg.width - borderWidth / 2,
                canvasImg.height - borderWidth / 2
        )

        canvasImg.drawRect(rect, paintMarco)
        canvasImg.drawText(tipoFoto, (canvasImg.width - 200).toFloat(), (canvasImg.height - 100).toFloat(), paintTipoImagen)
        canvasImg.drawText(dataAjuste, (borderWidth / 2 + borderWidth).toFloat(), (borderWidth / 2 + 50).toFloat(), paintTextSiniestro)

        //canvas.drawBitmap(mutableBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle()

        return mutableBitmap
    }

    companion object {

        fun toMultipart(image: ImageGeneralModel, name : String): MultipartBody.Part {
            return toMultipart(File(image.filePhat), name)
        }

        fun toMultipart(file: File, name : String): MultipartBody.Part {
            return MultipartBody.Part.createFormData(name, file.name,
                    RequestBody.create(MediaType.parse(file.toString()), file))
        }

        fun toMultiparts(images: List<ImageGeneralModel>, name : String): List<MultipartBody.Part> {
            var parts: MutableList<MultipartBody.Part> = ArrayList()
            for(image in images){
                if(image.newFile){
                    parts.add(toMultipart(image, name))
                }
            }
            return parts
        }



    }



}
