package com.gnp.ajustadormovil.android.views.ajustador;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.bluetoothdevice.DeviceItem;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.error.UnknownException;
import com.gnp.ajustadormovil.android.interactors.context.ContextInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjustadorInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.login.ChangePassword;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.EstatusAjustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.EstatusCommand;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Horario;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.ajuste.SiniestrosFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.PagerAdapter_Fragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.custom.ItemDecorator;
import com.gnp.ajustadormovil.android.views.commons.custom.ViewPagerCustom;
import com.gnp.ajustadormovil.android.views.commons.events.AsyncResponse;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.utils.impresora.DeviceBluetoothAdapter;
import com.gnp.ajustadormovil.android.views.splash.SplashActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.OnLongClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static com.gnp.ajustadormovil.android.models.AppStatus.context;

/**
 * Created by Lima on 04/10/17.
 * Modified by hgzavala on 16/04/2018
 */

public class HomeActivity extends BaseActivity implements CallbackItemImage<String>, AsyncResponse, TextWatcher { //AppCompatActivity implements RadioGroup.OnCheckedChangeListener //BaseActivity

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final String TAG = HomeActivity.class.getCanonicalName();
    private static HomeActivity homeActivity;
    final private int[] radio = {R.id.home_rbutton_siniestros, R.id.home_rbutton_messages, R.id.home_rbutton_printer, R.id.home_rbutton_info, R.id.home_rbutton_menu};

    public double Current_latitude;
    public double Current_longitude;

    private int code = 0;

    public String nombre;
    public String aMaterno;
    public String aPaterno;

    public String phone;

    public String estOperativo_User;
    public String tipoContrato;
    @BindView(R.id.home_viewpager)
    ViewPagerCustom mViewPager;
    GoogleApiClient googleApiClient;
    int REQUEST_LOCATION_PERMISSION_CODE = 101;
    String all_servicioAjuste_id;

    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.header_name)
    AppCompatTextView mName;
    @BindView(R.id.header_status)
    AppCompatTextView mStatus;
    @BindView(R.id.header_phone)
    AppCompatTextView mPhone;
    @BindView(R.id.gpsline)
    LinearLayoutCompat mGPSline;
    @BindView(R.id.icoSm1_foto)
    AppCompatImageView menuEngrane;
    @BindView(R.id.icoSm1_volante)
    AppCompatImageView menuEstatus;
    @BindView(R.id.header_version)
    AppCompatTextView header_version;

    //Firebase db instances
    FirebaseDatabase firebaseDataBase;
    DatabaseReference databaseReference;
    String intentLogInUser_agent_intent;
    @Nullable
    @BindView(R.id.home_rgroup_menu)
    RadioGroup mRadioGroup;
    boolean alertIsBeingShown = false;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private Menu menu;
    private PagerAdapter_Fragment mPagerAdapterFragment;
    private int lastActivity = -1;
    private String imageDocument;
    private boolean fisrtsBackOnChange;
    //
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    //
    String localVersion = "";
    public static boolean bandStatus;

    boolean gpsIsActive;
    boolean verIsActive;
    private Dialog dialogFragment;
    DeviceBluetoothAdapter adapterBluetoothDevice;
    File newFile;
    DeviceItem dItem;
    ArrayList<DeviceItem> deviceItem = new ArrayList<>();
    RecyclerView rvListDevice;
    BluetoothAdapter bluetoothAdapter;
    AlertDialog.Builder builder;
    private List<Horario> horarios = new ArrayList<>();
    private AjustadorInteractor ajustadorInteractor;

    private TextInputLayout textInputPassActual;
    private TextInputEditText editTPassActual;

    private TextInputLayout textInputPassNuevo;
    private TextInputEditText editTPassNuevo;

    private TextInputLayout textInputPassValidaNuevo;
    private TextInputEditText editTPassValidaNuevo;

    private Button btnAcceptPass;
    private Button btnCancelPass;


    public HomeActivity() {
        //ViewPager
        mPagerAdapterFragment = new PagerAdapter_Fragment(getSupportFragmentManager());
        mPagerAdapterFragment.addFragment(SiniestrosFragment.class);


    }



    public static String toLocalDateString() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm:ss", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT")); //GMT //UTC //CST
        Log.i(TAG, "DateTime = " + sdf.format(new Date()));
        return sdf.format(new Date());
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        homeActivity = this;
        ajustadorInteractor = OrquestadorInteractorFactory.getAjustadorInteractor();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        adapterBluetoothDevice = new DeviceBluetoothAdapter(deviceItem, this);
        cleanPreferences();
        findViewById(R.id.menu_content).setVisibility(View.VISIBLE);
        menuEstatus.setOnClickListener(v -> statusMenu(v, this));
        menuEngrane.setOnClickListener(v -> toolsMenu(v, this));
        getCompositeDisposable().add(ajustadorInteractor.init()
                .andThen(ajustadorInteractor.getAjustador())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ajustador -> {
                    Crashlytics.setUserEmail(ajustador.getCorreoElectronico());
                    Crashlytics.setUserIdentifier(ajustador.getCedula());
                    Crashlytics.setUserName(ajustador.getNombreCompleto());
                    Gson gson = new Gson();
                    Ajustador a = gson.fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
                    if (a == null) {
                        AppPreferences.setString(AppPreferences.DATA_AJUSTADOR, gson.toJson(ajustador));
                    }
                    initSettings();
                }, throwable -> {
                    Log.e(TAG, "Error a obtener el ajustador", throwable);
                    cerrarSesion();
                }));
    }

    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public void initSettings() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        } else {
        }

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        assert locationManager != null;


        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mGPSline.setBackgroundResource(R.color.colorGreen);
            FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(true);
            gpsIsActive = true;
            try {
                //updatedMenu();
                MenuItem gpsMenuItem = menu.findItem(R.id.tools_gps);
                gpsMenuItem.setTitle(R.string.txt_tools_gps_on);
                gpsMenuItem.setIcon(R.drawable.baseline_location_on_black_24dp);
            } catch (Exception e) {
                Log.w(TAG, "Error al iniciar menu");
            }
        } else {
            mGPSline.setBackgroundResource(R.color.colorRedStatus);
            FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(false);
            gpsIsActive = false;
            try {
                //updatedMenu();
                MenuItem gpsMenuItem = menu.findItem(R.id.tools_gps);
                gpsMenuItem.setTitle(R.string.txt_tools_gps_off);
                gpsMenuItem.setIcon(R.drawable.baseline_location_off_black_24dp);
            } catch (Exception e) {
                Log.w(TAG, "Error al iniciar menu");
            }
        }
        setSupportActionBar(mToolbar);
        mViewPager.setAdapter(mPagerAdapterFragment);
        mViewPager.setOffscreenPageLimit(mPagerAdapterFragment.getCount());
        Log.i(TAG, " limit " + mViewPager.getOffscreenPageLimit());
        //RGroupMenu
        //mRadioGroup.setOnCheckedChangeListener(this);


        PackageInfo pInfo;
        Date realTime = Calendar.getInstance().getTime();
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            localVersion = pInfo.versionName.replace('.', '_');
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error al obtener el número de paquete", e);
        }
        builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Nueva versión");
        builder.setMessage("Existe una nueva versión del Ajustador Móvil");
        builder.setCancelable(false);
        builder.setPositiveButton("DESCARGAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://app.gnp.com.mx/"));
                startActivity(intent);
            }
        });

        getCompositeDisposable().add(ContextInteractor.getInstance()
                .getLastVersion(17, 2)
                .subscribe(response -> {
                    String dateString = response != null ? response.getFecha_publicacion() : "";
                    if (localVersion.compareTo(response.getVersion()) != 0 && BuildConfig.ENVIRONMENT.equals("PROD")
                            && validateExpiration(dateString, realTime)) {
                        verIsActive = true;
                    }
                    if (!response.isRequerida()) {
                        builder.setMessage("Existe una nueva versión del Ajustador Móvil, ¿Desea descargarla?");
                        builder.setNegativeButton("DESPUÉS", (dialog, which) -> dialog.dismiss());
                    }
                    try {
                        updatedMenu();
                    } catch (Exception e) {
                        Log.w(TAG, "Error al iniciar menu");
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al obtener la versión de la aplicación", throwable);
                    if (!(throwable instanceof NetworkException) && !(throwable instanceof UnknownException)) {
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }

                }));

        Gson gson = new Gson();
        Ajustador data = gson.fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);


        header_version.setText("Versión: " + BuildConfig.VERSION_NAME);
        //received data user-id from login
        FirebaseDatabase.getInstance().getReference("ajustadores")
                .child(AjustadorMovilApplication.getCedula()).child("userAgent").setValue(intentLogInUser_agent_intent);

        //GET INSTANCE OF THE DB
        firebaseDataBase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDataBase.getReference();


        FirebaseDatabase firebaseDataBase;
        DatabaseReference databaseReference;
        firebaseDataBase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDataBase.getReference();

        final DatabaseReference ParseoInicial = databaseReference.child("ajustadores").child(AjustadorMovilApplication.getCedula());
        ParseoInicial.addValueEventListener(new ValueEventListener() {
            public HashMap<String, Object> timestampCreated;

            @SuppressLint("ResourceAsColor")
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(DataSnapshot childSnapshot) { //dataSnapshot) {
                updateAjustadorFirebase(childSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                updateAjustadorLocal();
            }
        });
        if (!ConnectionProvider.getInstance().isConnected() && data != null) {
            updateAjustadorLocal();
            mStatus.setText("SIN CONEXIÓN");
        }
    }

    @OnLongClick(R.id.header_version)
    public boolean muestraVersion() {

        PackageInfo pInfo;
        String version = "";

        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error al obtener la versión de aplicación", e);
        }

        Toast.makeText(this, "Número de compilación:\n"
                + version + "-"
                + BuildConfig.ENVIRONMENT + "-"
                + BuildConfig.COMMIT
                + "-" + BuildConfig.LOCAL_CHANGES, Toast.LENGTH_LONG).show();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        //updatedMenu();
        return true;
    }

    public void updatedMenu() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        MenuItem wirelesMenuItem = menu.findItem(R.id.tools_wireles);
        MenuItem verMenuItem = menu.findItem(R.id.tools_version);
        MenuItem gpsMenuItem = menu.findItem(R.id.tools_gps);
        MenuItem menuHorario = menu.findItem(R.id.horario_option);
        MenuItem menuItemReceso = menu.findItem(R.id.menu_receso);
        MenuItem menuItem = menu.findItem(R.id.menu_disponible);
        MenuItem menuItemCerrar = menu.findItem(R.id.menu_cerrar);
        String status = estOperativo_User;
        if (status != null) {
            switch (status) {
                case Constants.NOT_AVAILABLE:
                case Constants.ON_PAUSE:
                case Constants.RETIRED:
                    menuItem.setTitle(Constants.AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(true);
                    bandStatus = true;

                    break;
                case Constants.AVAILABLE:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(true);
                    menuItemCerrar.setEnabled(false);
                    bandStatus = false;
                    break;
                case Constants.TO_LEAVE:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    bandStatus = false;
                    break;
                case Constants.ON_ARRIVE:
                case Constants.ATTENDING:
                case Constants.TO_CONFIRMED:
                case Constants.TO_FINISH:
                case Constants.ON_HOLD:
                    bandStatus = false;
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(false);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    break;
            }
        }

    }

    public static void displayPromptForEnablingGPS(Context activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setTitle("GPS Desactivado");
        builder.setMessage("¿Quiere encender su GPS?")
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.show();
    }

    public static void displayPromptForEnablingWirles(Context activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setTitle("Red no disponible");
        builder.setMessage("¿Quiere activar su conexión?")
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.show();
    }

    public static void displayPromptForUpdateApp(final Activity activity) {
        final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://app.gnp.com.mx/")));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://app.gnp.com.mx/")));
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public void showDownload() {
        PackageInfo pInfo;
        Date realTime = Calendar.getInstance().getTime();
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            localVersion = pInfo.versionName.replace('.', '_');
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error al obtener número de paquete", e);
        }
        getCompositeDisposable().add(ContextInteractor.getInstance()
                .getLastVersion(17, 2)
                .subscribe(response -> {
                    String dateString = response != null ? response.getFecha_publicacion() : "";
                    if (localVersion.compareTo(response.getVersion()) != 0 && BuildConfig.ENVIRONMENT.equals("PROD")
                            && validateExpiration(dateString, realTime)) {
                        verIsActive = true;
                    }
                    if (!response.isRequerida()) {
                        builder.setMessage("Existe una nueva versión del Ajustador Móvil, ¿Desea descargarla?");
                        builder.setNegativeButton("DESPUÉS", (dialog, which) -> dialog.dismiss());
                    }
                    try {
                        updatedMenu();
                    } catch (Exception e) {
                        Log.w(TAG, "Error al iniciar menu");
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al obtener la versión de la aplicación", throwable);
                    if (!(throwable instanceof NetworkException) && !(throwable instanceof UnknownException)) {
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }
                }));
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    final BroadcastReceiver bReciever = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d("DEVICELIST", "Bluetooth device found\n");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.d("Device", "name: " + device.getName() + " address " + device.getAddress());
                if (device.getName() != null) {
                    DeviceItem item = new DeviceItem(device.getName(), device.getAddress());
                    if (!deviceItem.contains(item)) {
                        deviceItem.add(item);
                        adapterBluetoothDevice.notifyDataSetChanged();
                    }
                }
            }
        }
    };

    public void displayPromptForUpdatePrinter(Context activity) {

        dItem = Utils.jsonToObject(DeviceItem.class, AppPreferences.getString(AppPreferences.IMPRESORA_AJUSTE));
        if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_LOCATION_PERMISSION)) {
            getListBluetooth();
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            activity.registerReceiver(bReciever, filter);
            bluetoothAdapter.startDiscovery();
        } else
            Toast.makeText(this, "Concede los permisos para escanear dispositivos", Toast.LENGTH_SHORT).show();
    }


    NotificationManager nMgr;

    public void statusMenu(View v, Context context) {
        Ajustador ajustador = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        PopupMenu popup = new PopupMenu(context, v);
        popup.getMenuInflater().inflate(R.menu.menu_tatus, popup.getMenu());
        nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        MenuItem menuItem = popup.getMenu().findItem(R.id.menu_disponible);
        MenuItem menuItemReceso = popup.getMenu().findItem(R.id.menu_receso);
        MenuItem menuItemCerrar = popup.getMenu().findItem(R.id.menu_cerrar);
        MenuItem menuHorario = popup.getMenu().findItem(R.id.horario_option);
        MenuItem menuPassword = popup.getMenu().findItem(R.id.menu_password);
        String status = estOperativo_User;
        if (ajustador == null || ajustador.getCorreoElectronico().toLowerCase().contains("@gnp")) {
            menuPassword.setVisible(false);
        }
        if (status != null) {
            switch (status) {
                case Constants.NOT_AVAILABLE:
                case Constants.ON_PAUSE:
                case Constants.RETIRED:
                    menuItem.setTitle(Constants.AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(true);
                    bandStatus = true;

                    break;
                case Constants.AVAILABLE:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(true);
                    menuItemCerrar.setEnabled(false);
                    bandStatus = false;
                    break;
                case Constants.TO_LEAVE:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    bandStatus = false;
                    break;
                case Constants.ON_ARRIVE:
                case Constants.ATTENDING:
                case Constants.TO_CONFIRMED:
                case Constants.TO_FINISH:
                case Constants.ON_HOLD:
                    bandStatus = false;
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(false);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    break;
            }
        }

        /** Adding menu items to the popumenu */
        nMgr.cancelAll();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.menu_cerrar:
                        cerrarSesion();
                        return true;
                    case R.id.menu_receso:
                        if (status != null) {
                            switch (status) {
                                case Constants.NOT_AVAILABLE:
                                case Constants.ON_PAUSE:
                                case Constants.RETIRED:
                                    menuItem.setTitle(Constants.AVAILABLE);
                                    menuItem.setEnabled(true);
                                    menuItemReceso.setEnabled(false);
                                    menuItemCerrar.setEnabled(true);
                                    bandStatus = true;

                                    break;
                                case Constants.AVAILABLE:
                                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                                    menuItem.setEnabled(true);
                                    menuItemReceso.setEnabled(true);
                                    menuItemCerrar.setEnabled(false);
                                    bandStatus = false;
                                    break;
                                case Constants.TO_LEAVE:
                                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                                    menuItem.setEnabled(true);
                                    menuItemReceso.setEnabled(false);
                                    menuItemCerrar.setEnabled(false);
                                    bandStatus = false;
                                    break;
                                case Constants.ON_ARRIVE:
                                case Constants.ATTENDING:
                                case Constants.TO_CONFIRMED:
                                case Constants.TO_FINISH:
                                case Constants.ON_HOLD:
                                    bandStatus = false;
                                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                                    menuItem.setEnabled(false);
                                    menuItemReceso.setEnabled(false);
                                    menuItemCerrar.setEnabled(false);
                                    break;
                            }
                        }
                        Toast.makeText(getApplicationContext(), "Notificando Receso", Toast.LENGTH_SHORT).show();
                        //set off-duty
                        menuItem.setTitle("DISPONIBLE");
                        menuItem.setEnabled(true);
                        menuItemReceso.setEnabled(false);
                        menuItemCerrar.setEnabled(true);

                        getCompositeDisposable().add(ajustadorInteractor.modifyStatus(new EstatusCommand(EstatusAjustador.EN_RECESO))
                                .subscribe(() -> {
                                    Log.d(TAG, "Notificando Receso");
                                }, throwable -> {
                                    Log.e(TAG, "Error al cambiar estatus", throwable);
                                    new GeneralSnackBar().largeSnackError( HomeActivity.this, throwable);
                                }));
                        // set unavailable
                        FirebaseDatabase.getInstance().getReference("ajustadores").child(AjustadorMovilApplication.getCedula()).child("estOperativo").setValue("NO DISPONIBLE");
                        FirebaseDatabase.getInstance().getReference("ajustadores")
                                .child(AjustadorMovilApplication.getCedula()).child("fechaHoraEstatus")
                                .setValue(HomeActivity.toLocalDateString());
                        // Clear all notification

                        return true;
                    case R.id.menu_disponible:
                        String status = estOperativo_User;
                        //set available
                        if ("DISPONIBLE".equals(status) ||
                                "POR SALIR".equals(status)) {
                            Toast.makeText(getBaseActivity(), "NO DISPONIBLE", Toast.LENGTH_SHORT).show();
                            getCompositeDisposable().add(ajustadorInteractor.modifyStatus(new EstatusCommand(EstatusAjustador.NO_DISPONIBLE))
                                    .subscribe(() -> {
                                        Log.d(TAG, "No disponible");
                                    }, throwable -> {
                                        Log.e(TAG, "Error al cambiar estatus", throwable);
                                        new GeneralSnackBar().largeSnackError(HomeActivity.this, throwable);
                                    }));

                        }

                        if ("NO DISPONIBLE".equals(status) ||
                                "EN RECESO".equals(status) ||
                                "RETIRADO".equals(status)) {
                            Toast.makeText(HomeActivity.this, "DISPONIBLE", Toast.LENGTH_SHORT).show();
                            getCompositeDisposable().add(ajustadorInteractor.modifyStatus(new EstatusCommand(EstatusAjustador.DISPONIBLE))
                                    .subscribe(() -> {
                                        Log.d(TAG, "Disponible");
                                    }, throwable -> {
                                        Log.e(TAG, "Error al cambiar estatus", throwable);
                                        new GeneralSnackBar().largeSnackError(HomeActivity.this, throwable);
                                    }));
                            // Clear all notification
                            nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            if (nMgr != null) {
                                nMgr.cancelAll();
                            }

                        }
                        return true;
                    case R.id.horario_option:
                        getHorario(menuHorario);
                        break;
                    case R.id.menu_password:
                        changePassword();
                        break;
                }

                return false;
            }
        });
        popup.show();

    }

    private void getHorario(MenuItem menuHorario) {
        getCompositeDisposable().add(ajustadorInteractor.getHorario()
                .subscribe(horariosList -> {
                    horarios = horariosList;
                    AlertDialog.Builder mDialog = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
                    LayoutInflater inflater = getLayoutInflater();
                    final View mView = inflater.inflate(R.layout.dialog_horario_dispobible, null);
                    mDialog.setView(mView);
                    RecyclerView rvHorario = mView.findViewById(R.id.rvHorario);
                    if (horarios != null && horarios.size() != 0) {
                        HorarioAdapter adapter = new HorarioAdapter(horarios);
                        rvHorario.setAdapter(adapter);
                        rvHorario.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        rvHorario.setHasFixedSize(true);
                        RecyclerView.ItemDecoration dividerItemDecoration = new
                                ItemDecorator(ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.line_divider_horario));
                        rvHorario.addItemDecoration(dividerItemDecoration);
                        mDialog.setCancelable(false);
                        mDialog.setPositiveButton("Aceptar", (dialog, which) -> dialog.dismiss());
                        mDialog.show();

                    }

                }, throwable -> {
                    Log.e(TAG,"Error al obtener el horario", throwable);
                    menuHorario.setTitle("Horario no configurado");
                    new GeneralSnackBar().largeSnackError(this, "Horario no configurado");
                }));
    }

    public void toolsMenu(View v, Context context) {
        PopupMenu popup = new PopupMenu(context, v);
        /** Adding menu items to the popumenu */
        try {
            @SuppressLint("PrivateApi") Method method = popup.getMenu().getClass().getDeclaredMethod("setOptionalIconsVisible", boolean.class);
            method.setAccessible(true);
            method.invoke(popup.getMenu(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.getMenuInflater().inflate(R.menu.menu_scrolling, popup.getMenu());
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        MenuItem wirelesMenuItem = popup.getMenu().findItem(R.id.tools_wireles);
        MenuItem verMenuItem = popup.getMenu().findItem(R.id.tools_version);
        MenuItem gpsMenuItem = popup.getMenu().findItem(R.id.tools_gps);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gpsMenuItem.setTitle(R.string.txt_tools_gps_on);
            gpsMenuItem.setIcon(R.drawable.baseline_location_on_black_24dp);
        } else {
            gpsMenuItem.setTitle(R.string.txt_tools_gps_off);
            gpsMenuItem.setIcon(R.drawable.baseline_location_off_black_24dp);
        }
        if (RXUtils.isNetworkEnabled()) {
            wirelesMenuItem.setTitle(R.string.txt_tools_wirles_on);
            wirelesMenuItem.setIcon(R.drawable.baseline_wifi_black_24dp);
        } else {
            wirelesMenuItem.setTitle(R.string.txt_tools_wirles_off);
            wirelesMenuItem.setIcon(R.drawable.baseline_wifi_off_black_24dp);
        }
        if (verIsActive) {
            verMenuItem.setEnabled(true);
            verMenuItem.setIcon(R.drawable.ic_new_version);
        } else {
            verMenuItem.setEnabled(false);
            verMenuItem.setIcon(R.drawable.ic_new_version_disable);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // TODO Auto-generated method stub
                switch (item.getItemId()) {
                    case R.id.tools_gps:
                        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                            displayPromptForEnablingGPS(context);
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
                            alertDialog.setTitle("GPS");
                            alertDialog.setMessage("El GPS esta conectado");
                            alertDialog.show();
                        }
                        return true;
                    case R.id.tools_wireles:
                        if (!RXUtils.isNetworkEnabled()) {
                            displayPromptForEnablingWirles(context);
                        } else {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
                            alertDialog.setTitle("Conexión");
                            alertDialog.setMessage("Cuenta con internet");
                            alertDialog.show();
                        }
                        return true;
                    case R.id.tools_version:
                        showDownload();
                        return true;
                    case R.id.tools_printer:
                        displayPromptForUpdatePrinter(context);

                        return true;
                }

                return false;
            }
        });
        popup.show();
    }

    private void getListBluetooth() {
        dialogFragment = new Dialog(this, R.style.CustomThemeDialog);
        dialogFragment.setContentView(R.layout.dialog_list_device);
        rvListDevice = dialogFragment.findViewById(R.id.rvListDeviceBluetooth);
        Button buttonCancel = dialogFragment.findViewById(R.id.btnCancelBluetooth);
        Button buttonOK = dialogFragment.findViewById(R.id.btnOkBluetooth);
        Button buttonUpdate = dialogFragment.findViewById(R.id.btnUpdateBluetooth);
        buttonUpdate.setVisibility(View.VISIBLE);
        final LinearLayoutManager layoutManagerM = new LinearLayoutManager(context);
        layoutManagerM.setOrientation(LinearLayoutManager.VERTICAL);
        rvListDevice.setLayoutManager(layoutManagerM);
        rvListDevice.hasFixedSize();
        rvListDevice.setAdapter(adapterBluetoothDevice);
        dialogFragment.setCancelable(true);
        dialogFragment.show();
        buttonCancel.setOnClickListener(v -> {
            dialogFragment.dismiss();
            adapterBluetoothDevice.clear();
            adapterBluetoothDevice.notifyDataSetChanged();
        });
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    adapterBluetoothDevice.clear();
                    adapterBluetoothDevice.notifyDataSetChanged();
                    bluetoothAdapter.startDiscovery();
                    adapterBluetoothDevice.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e(TAG, "Error al iniciar bluethooth", e);
                }
            }
        });
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapterBluetoothDevice.getPrint() != null) {
                    unregisterReceiver(bReciever);
                    AppPreferences.setString(AppPreferences.IMPRESORA_AJUSTE, Utils.objectoToJsonString(adapterBluetoothDevice.getPrint()));
                    dItem = adapterBluetoothDevice.getPrint();
                    dialogFragment.dismiss();
                    bluetoothAdapter.cancelDiscovery();
                    adapterBluetoothDevice.clear();
                    adapterBluetoothDevice.notifyDataSetChanged();
                    new GeneralSnackBar().largeSnackOk(HomeActivity.this, "Impresora seleccionada correctamente");
                } else {
                    dialogFragment.dismiss();
                    new GeneralSnackBar().largeSnackError(HomeActivity.this, "Seleccione su impresora por favor");
                }
            }
        });
    }


    private void changePassword() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_change_password);
        textInputPassActual = dialog.findViewById(R.id.txtIPassActual);
        textInputPassNuevo = dialog.findViewById(R.id.txtIPassNew);
        textInputPassValidaNuevo = dialog.findViewById(R.id.txtIpassValidaNew);

        editTPassActual = dialog.findViewById(R.id.edTPassActual);
        editTPassNuevo = dialog.findViewById(R.id.edTPassNew);
        editTPassValidaNuevo = dialog.findViewById(R.id.edTValidaNew);

        editTPassActual.addTextChangedListener(this);
        editTPassValidaNuevo.addTextChangedListener(this);
        editTPassNuevo.addTextChangedListener(this);

        btnAcceptPass = dialog.findViewById(R.id.btnAcceptPass);
        btnCancelPass = dialog.findViewById(R.id.btnCancelPass);
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        if (!dialog.isShowing()) {
            dialog.show();
        }
        View.OnClickListener onClick = view -> {
            switch (view.getId()) {
                case R.id.btnAcceptPass:
                    if (editTPassActual.getText().toString().isEmpty()) {
                        textInputPassActual.setError(getResources().getString(R.string.pass_empty));

                    } else if (editTPassNuevo.getText().toString().isEmpty()) {
                        textInputPassNuevo.setError(getResources().getString(R.string.pass_empty));

                    } else if (editTPassValidaNuevo.getText().toString().isEmpty()) {
                        textInputPassValidaNuevo.setError(getResources().getString(R.string.pass_empty));

                    } else if (!editTPassNuevo.getText().toString().equals(editTPassValidaNuevo.getText().toString())) {
                        textInputPassValidaNuevo.setError(getResources().getString(R.string.pass_not_valid));
                    } else {
                        getCompositeDisposable().add(ajustadorInteractor
                                .changePassword(new ChangePassword(editTPassActual.getText().toString(), editTPassNuevo.getText().toString()))
                                .subscribe(() -> {
                                    dialog.dismiss();
                                    new GeneralSnackBar().largeSnackOk(this, "Tú contraseña se ha actulizado correctamente");

                                }, throwable -> {
                                    Log.e(TAG, "Ocurrio un error al actualizar la contraseña", throwable);
                                    ToastUtils.toastLongError(throwable.getMessage());
                                }));
                    }
                    break;
                case R.id.btnCancelPass:
                    dialog.cancel();
                    break;
            }


        };

        btnAcceptPass.setOnClickListener(onClick);
        btnCancelPass.setOnClickListener(onClick);
    }


    private void cerrarSesion() {
        Intent i = new Intent(this, SplashActivity.class);
        i.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        stopService();
        LoadingIndicator.start();
        getCompositeDisposable().add(ajustadorInteractor.cerrarSesion()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    LoadingIndicator.finish();
                    startActivity(i);
                    finish();
                }, throwable -> {
                    Log.e(TAG, "Error al cerrar sesión", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                    LoadingIndicator.finish();
                }));
    }

    public void showFragment(int fragmentNumber) {

        mViewPager.setCurrentItem(fragmentNumber);
    }

    public void BienvenidaDespacho() {
        if (alertIsBeingShown) return;
        alertIsBeingShown = true;
        String[] s = {"“Al Sistema de Atención de Siniestros de GNP”"};
        //Intent intent = new Intent(this, HomeActivity.class);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Bienvenido");
        alert.setItems(s, (dialogInterface, i) -> {
        });
        alert.setNegativeButton("Aceptar", (dialogInterface, i) -> FirebaseDatabase
                .getInstance().getReference("ajustadores").child(AjustadorMovilApplication.getCedula())
                .child("tipoContrato").setValue("DESPACHO."));
        alert.show();

        alert.setOnCancelListener(arg0 -> finish());
    }


    public String sendMyUserTo_Tab_Fragment_SiniestrosList() {
        return AjustadorMovilApplication.getCedula();
    }

    public String sendMyNameTo_Tab_Fragment_SiniestrosList() {
        return mName.getText().toString();
    }

    public String sendMyTelTo__Tab_Fragment_SiniestrosList() {
        return mPhone.getText().toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homeActivity = null;
        Log.d("onDestroy", "isDestroyed elements");
        if (bandStatus) {
            FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(false);
        }
    }

    @Override
    public void onBackPressed() {
        DialogUtils.createAlert(this, "Saliendo",
                "¿Desea salir de la aplicación?",
                "Aceptar", "Cancelar",
                (dialog, which) -> HomeActivity.super.onBackPressed(),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    protected void onResume() {
        super.onResume();
        validateLocation();
        cleanPreferences();


    }

    private boolean validateExpiration(String fechaSistemaDB, Date fechaOSAndroid) {
        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        DateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.forLanguageTag("es-MX"));
        try {
            Date cal = sdf.parse(fechaSistemaDB);
            return (fechaOSAndroid.after(cal));
        } catch (ParseException ex) {
            return false;
        }
    }


    @Override
    public void validateLocation() {
        super.validateLocation();
    }

    @Override
    public void onCallbackItemId(int position, String id) {

    }

    @Override
    public void onCallItemId(int position, String id) {

    }

    @Override
    public void processFinish() {
        new GeneralSnackBar().largeSnackOk(this, "Impresora Vinculada");
    }

    @Override
    public void processError() {
        ToastUtils.toastLong("Ocurrio un error al Vincular. Vuelva a intentarlo");
        adapterBluetoothDevice.clear();
        adapterBluetoothDevice.notifyDataSetChanged();
        AppPreferences.setString(AppPreferences.IMPRESORA_AJUSTE, "");
    }

    @Override
    protected void onDisconnected() {
        super.onDisconnected();
        mStatus.setText("SIN CONEXIÓN");
    }

    @Override
    protected void onConnected() {
        super.onConnected();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child("ajustadores")
                .child(AjustadorMovilApplication.getCedula())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        updateAjustadorFirebase(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        updateAjustadorLocal();
                    }
                });
    }

    private void updateAjustadorFirebase(DataSnapshot childSnapshot) {
        Ajustador data = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        if (childSnapshot.exists() && data != null) {
            mName.setText(String.format("%s %s %s", childSnapshot.child("nombres").exists() ? childSnapshot.child("nombres").getValue(String.class) : data.getNombre()
                    , childSnapshot.child("aPaterno").exists() ? childSnapshot.child("aPaterno").getValue(String.class) : data.getApellidoPaterno()
                    , childSnapshot.child("aMaterno").exists() ? childSnapshot.child("aMaterno").getValue(String.class) : data.getApellidoMaterno()));
            mPhone.setText(data.getCedula() + " - " + data.getTelefono());
            estOperativo_User = childSnapshot.child("estOperativo").exists() ? childSnapshot.child("estOperativo").getValue(String.class) : "NO DISPONIBLE";
            tipoContrato = childSnapshot.child("tipoContrato").getValue(String.class) != null ? childSnapshot.child("tipoContrato").getValue(String.class) : "No hay contrato";
            if (!ConnectionProvider.getInstance().isConnected()) {
                mStatus.setText("SIN CONEXIÓN");
            } else {
                mStatus.setText(estOperativo_User);
            }
        } else {
            updateAjustadorLocal();
        }
    }

    public void cleanPreferences() {

        AppPreferences.cleanAjustes(Constants.TAG_AGE_CAPTURE);
        AppPreferences.cleanAjustes(AppPreferences.JSON_AJUSTE_DETALLE);
        AppPreferences.cleanAjustes(AppPreferences.TIPO_UBICACION);
        AppPreferences.cleanAjustes(AppPreferences.JSON_AJUSTES_OPERACIONES);
        AppPreferences.cleanAjustes(AppPreferences.ID_AJUSTE_ACTUAL);
        AppPreferences.cleanAjustes(AppPreferences.ESTATUS_ASEGURADO);
        AppPreferences.cleanAjustes(AppPreferences.ESTATUS_TERCEROS_NO_AUTO);
        AppPreferences.cleanAjustes(AppPreferences.ESTATUS_RESPONSABILIDAD);
        AppPreferences.cleanAjustes(AppPreferences.ESTIMADO_DE_DANO_ASEGURADO);
        AppPreferences.cleanAjustes(AppPreferences.PRIVATE_DECLARACION);
        AppPreferences.cleanAjustes(Constants.TAG_NAME_CAPTURE);


    }

    private void updateAjustadorLocal() {
        Ajustador ajustador = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        if (ajustador != null) {
            mName.setText(String.format("%s %s %s", ajustador.getNombre(), ajustador.getApellidoPaterno(), ajustador.getApellidoMaterno()));
            mPhone.setText(ajustador.getCedula() + " - " + ajustador.getTelefono());

        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (editable.hashCode() == editTPassActual.getText().hashCode() && !editTPassActual.getText().toString().isEmpty()) {
            textInputPassActual.setError(null);
        } else if (editable.hashCode() == editTPassNuevo.getText().hashCode() && !editTPassNuevo.getText().toString().isEmpty()) {
            textInputPassNuevo.setError(null);
        } else if (editable.hashCode() == editTPassValidaNuevo.getText().hashCode() && (!editTPassValidaNuevo.getText().toString().isEmpty()
                || editTPassValidaNuevo.getText().toString().equals(editTPassNuevo.getText().toString()))) {
            textInputPassValidaNuevo.setError(null);
        }
    }
}
