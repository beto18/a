package com.gnp.ajustadormovil.android.views.ajustador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Horario;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;

import java.util.List;

public class HorarioAdapter extends RecyclerView.Adapter<HorarioAdapter.HorarioHolder>{

    private List<com.gnp.ajustadormovil.android.models.orquestador.ajustador.Horario> horarioList;

    public HorarioAdapter(List<Horario> horarioList){
        this.horarioList = horarioList;
    }

    @NonNull
    @Override
    public HorarioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_horario,parent,false);
        return new HorarioHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull HorarioHolder holder, int position) {
        if (horarioList.get(position).getDiaEntrada().equals(horarioList.get(position).getDiaSalida())){
            holder.diaEntrada.setText(horarioList.get(position).getDiaEntrada().name());
            holder.horarioE.setText(DateUtils.time(horarioList.get(position).getHrEntrada()));
            holder.horarioS.setText(DateUtils.time(horarioList.get(position).getHrSalida()));
            holder.salida.setVisibility(View.GONE);
            holder.itemView.findViewById(R.id.line_divider).setVisibility(View.GONE);
        }
        else {
            holder.diaEntrada.setText(horarioList.get(position).getDiaEntrada().name());
            holder.diaSalida.setText(horarioList.get(position).getDiaSalida().name());
            holder.horarioE.setText(DateUtils.time(horarioList.get(position).getHrEntrada()));
            holder.horarioSalida.setText(DateUtils.time(horarioList.get(position).getHrSalida()));

        }
    }

    @Override
    public int getItemCount() {
        return horarioList.size();
    }

    class HorarioHolder extends RecyclerView.ViewHolder{

        TextView diaEntrada, diaSalida, horarioE, horarioS, horarioSalida;
        LinearLayout salida;

        HorarioHolder(View itemView) {
            super(itemView);
            diaEntrada = itemView.findViewById(R.id.tvDiaE);
            horarioE = itemView.findViewById(R.id.tvHorarioE);
            horarioS = itemView.findViewById(R.id.tvHorarioS1);
            diaSalida = itemView.findViewById(R.id.tvDiaS);
            horarioSalida = itemView.findViewById(R.id.tvHorarioS);
            salida = itemView.findViewById(R.id.diaSalida);


        }
    }
}
