package com.gnp.ajustadormovil.android.views.ajustador;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.services.GcmMessageService;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.FormatStrings;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

public class
LoginActivity extends BaseActivity implements LoginMvpView {

    private static final String TAG = LoginActivity.class.getCanonicalName();
    @BindView(R.id.login_button)
    Button loginButton;
    LoginPresenter loginPresenter;
    @BindView(R.id.login_edit_email1)
    TextInputEditText mEmail;
    @BindView(R.id.login_edit_psw1)
    TextInputEditText mPsw;
    @BindView(R.id.amVersion)
    TextView amVersion;
    @BindView(R.id.login_container)
    RelativeLayout container;
    private TextInputEditText editCorreo;
    private Button btnResetPass;
    private Button btnCancelPass;
    private TextInputLayout textInputemail;
    int PERMISSION_ALL = 1;
    private String[] PERMISSIONS = {Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    public static boolean startLogin;
    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        loginPresenter = new LoginPresenter(compositeDisposable);
        loginPresenter.onAttach(this);
        Log.i(TAG, "onCreate :::");
        amVersion.setText("Versión: " + BuildConfig.VERSION_NAME);
        startLogin = true;

        if (BuildConfig.ENVIRONMENT.equals("QA") || BuildConfig.ENVIRONMENT.equals("UAT")) {
            mEmail.setText("nestor.palestina@gnp.com.mx");
            mPsw.setText("Mexico.18");
        }
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        loginButton.setOnClickListener(v ->
                onLoginButtonClick());
        amVersion.setOnClickListener(v -> showBuild());
    }

    @OnClick(R.id.amVersion)
    public void showBuild() {
        PackageInfo pInfo;
        String version = "";
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "No se encontro la información del paquete", e);
        }
        Toast.makeText(this, "Número de compilación:\n" + version + "-" + BuildConfig.ENVIRONMENT + "-" + BuildConfig.COMMIT + "-" + BuildConfig.LOCAL_CHANGES, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.password_recovery)
    public void showPassMessage() {

        dialog = new Dialog(getBaseActivity());
        dialog.setContentView(R.layout.dialog_recovery_password);
        editCorreo = dialog.findViewById(R.id.editCorreo);
        btnResetPass = dialog.findViewById(R.id.btnAcceptPass);
        btnCancelPass = dialog.findViewById(R.id.btnCancelPass);
        textInputemail = dialog.findViewById(R.id.textInputemail);
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        if (!dialog.isShowing()) {
            dialog.show();
        }
        View.OnClickListener onClick = view -> {
            switch (view.getId()){
                case R.id.btnAcceptPass:
                    if (editCorreo.getText().toString().contains("@gnp")) {
                        DialogUtils.createAlert(this, "Para restaurar su contraseña",
                                "Favor de comunicarse a la mesa de servicios de GNP, ext.0911.",
                                "Aceptar", "",
                                (dialog, which) -> dialog.dismiss(), null);
                    } else if (editCorreo.getText().toString().isEmpty() || !FormatStrings.validarEmail(editCorreo.getText().toString())) {
                        textInputemail.setError("Debe ingresar un correo electrónico válido");
                    } else {
                        dialog.dismiss();
                        loginPresenter.resetPassword(editCorreo.getText().toString());
                    }
                    break;
                case R.id.btnCancelPass:
                    dialog.dismiss();
                    break;
            }


        };

        btnResetPass.setOnClickListener(onClick);
        btnCancelPass.setOnClickListener(onClick);


    }

    @Override
    public void openHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public void onLoginButtonClick() {
        loginPresenter.validateData(mEmail.getText().toString(), mPsw.getText().toString());
    }

    @Override
    public void showError(Throwable e) {
        new GeneralSnackBar().largeSnackError(this, e);
    }

    @Override
    public void showError(String message) {
        new GeneralSnackBar().largeSnackError(this, message);
    }

    @Override
    public void initTokenGCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = AppPreferences.getBoolean(AppPreferences.SENT_TOKEN_TO_SERVER);
                if (sentToken) {
                    Log.d("", getString(R.string.gcm_send_message));
                    loginPresenter.makeRequestTokenGCM();
                } else {
                    Log.d("", getString(R.string.token_error_message));
                }
            }
        };
        // Registering BroadcastReceiver
        registerReceiver();

        if (Utils.checkPlayServices(this)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, GcmMessageService.class);
            startService(intent);
        }
    }

    @Override
    public void finishValidation(String email, String pass, String branch) {
        loginPresenter.startLogin(email, pass, branch);
    }

    @Override
    public void showOK(String message) {
        new GeneralSnackBar().largeSnackOk(this, message);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }


    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(AppPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

}
