package com.gnp.ajustadormovil.android.views.ajustador;

import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {
    void startLogin(String email,String pass,String branch);
    void validateData(String email,String pass);
    void makeRequestTokenGCM();
    void resetPassword(String email);
}
