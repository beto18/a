package com.gnp.ajustadormovil.android.views.ajustador;

import com.gnp.ajustadormovil.android.views.commons.base.MvpView;

public interface LoginMvpView extends MvpView{
    void openHomeActivity();
    void onLoginButtonClick();
    void showError(Throwable throwable);
    void showError(String message);
    void initTokenGCM();
    void finishValidation(String email,String pass,String branch);
    void showOK(String message);
}
