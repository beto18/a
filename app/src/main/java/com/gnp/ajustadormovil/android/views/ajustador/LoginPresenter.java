package com.gnp.ajustadormovil.android.views.ajustador;

import android.util.Log;

import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.UserNotFoundException;
import com.gnp.ajustadormovil.android.interactors.context.ContextInteractor;
import com.gnp.ajustadormovil.android.interactors.context.JwtInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjustadorInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.AppStatus;
import com.gnp.ajustadormovil.android.models.GlobalUrlEngine;
import com.gnp.ajustadormovil.android.models.context.ExpireCommand;
import com.gnp.ajustadormovil.android.models.context.LoginCommand;
import com.gnp.ajustadormovil.android.models.context.Usuario;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Dispositivo;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.reactivex.disposables.CompositeDisposable;

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {


    private FirebaseDatabase firebaseDataBase;
    private ContextInteractor contextInteractor;
    private AjustadorInteractor ajustadorInteractor;

    LoginPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        contextInteractor = ContextInteractor.getInstance();
        ajustadorInteractor = OrquestadorInteractorFactory.getAjustadorInteractor();
    }

    @Override
    public void startLogin(String email, String pass, String branch) {

        LoginCommand command = new LoginCommand();
        command.setMail(email);
        command.setPassword(pass);
        command.setProjectId(GlobalUrlEngine.GlobalUrlEngine_LogIn_Aut_VAR);
        command.setTipo(branch);
        command.setExpire(new ExpireCommand());
        command.getExpire().setDays(1);

        getmCompositeDisposable().add(contextInteractor.login(command)
                .subscribe(login -> {
                    Usuario usuario = JwtInteractor.getUser(login.getJwt());
                    if(!"PROD".equals(BuildConfig.ENVIRONMENT) || usuario.getRoles().contains(Constants.ROL_AJUSTADOR_AUTOS)) {
                        AppPreferences.setString(AppPreferences.USER_INFO_TOKEN, login.getJwt());
                        AppPreferences.setString(AppPreferences.USER_INFO_REFRESH_TOKEN, login.getRefreshToken());
                        getDataAdjuster();
                    } else {
                        getMvpView().showError("El usuario no tiene permisos para usar la aplicación");
                    }
                }, throwable -> {
                    if (throwable instanceof UserNotFoundException && branch.equals(Constants.BRANCH_EMPLOYEE)) {
                        Log.i(TAG, "Buscar ajustador en otra rama.");
                        startLogin(email, pass, Constants.BRANCH_ADJUSTER);
                    } else {
                        Log.e(TAG, "Error al llamar el login", throwable);
                        getMvpView().showError(throwable);
                    }
                }));
    }

    @Override
    public void validateData(String email, String pass) {
        if (AppStatus.isOnline()) {
            String inputUsuario = email.toLowerCase();
            if (pass.trim().length() == 0) {
                getMvpView().showError(AjustadorMovilApplication.getInstance().getString(R.string.text_field_required));
            } else if (inputUsuario.trim().length() == 0) {
                getMvpView().showError(AjustadorMovilApplication.getInstance().getString(R.string.text_field_required));
            } else if (inputUsuario.trim().length() != 0 && pass.trim().length() != 0) {
                if (email.toLowerCase().contains("@gnp.com.mx"))
                    getMvpView().finishValidation(inputUsuario, pass, Constants.BRANCH_EMPLOYEE);
                else
                    getMvpView().finishValidation(inputUsuario, pass, Constants.BRANCH_ADJUSTER);
            }
        } else {
            getMvpView().showError(AjustadorMovilApplication.getInstance().getString(R.string.text_error_connection));
        }
    }

    private void getDataAdjuster() {
        getmCompositeDisposable().add(ajustadorInteractor.getAjustador()
                .subscribe(adjuster -> {
                    firebaseDataBase = FirebaseDatabase.getInstance();
                    DatabaseReference databaseReference = firebaseDataBase.getReference();

                    final DatabaseReference ParseoInicial = databaseReference.child("Settings");
                    ParseoInicial.addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot childSnapshot) { //dataSnapshot) {
                            AppPreferences.setString(AppPreferences.GEOCERCA, "1000");//childSnapshot.child("geoFence").getValue(Integer.class).toString());
                            AppPreferences.setString(AppPreferences.UBICACION_PETICION, childSnapshot.child("timerMinute").getValue().toString());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    String cedula = adjuster.getCedula() != null ? adjuster.getCedula() : "";
                    AppPreferences.setString(AppPreferences.USER_ID_CEDULA, cedula);
                    AppPreferences.setString(AppPreferences.USER_ID_CORREO, adjuster.getCorreoElectronico());
                    AppPreferences.setString(AppPreferences.DATA_AJUSTADOR, Utils.objectoToJsonString(adjuster));
                    AppPreferences.setString(AppPreferences.JSON_ADJUSTER, Utils.objectoToJsonString(adjuster));
                    getMvpView().initTokenGCM();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener los datos del ajustador", throwable);
                    getMvpView().showError(ErrorUtils.getErrorMessage(throwable));
                }));
    }

    @Override
    public void makeRequestTokenGCM() {
        String token = AppPreferences.getString(AppPreferences.GCM_TOKEN);
        if (token != null) {
            if (token.length() > 0) {
                getmCompositeDisposable().add(ajustadorInteractor.addDispositivo(new Dispositivo(token))
                        .subscribe(dispositivo -> {
                            getMvpView().openHomeActivity();
                        }, throwable -> {
                            Log.e(TAG,"Error al agrgar dispositivo", throwable);
                            getMvpView().showError(throwable);

                        }));
            }
        }
    }

    @Override
    public void resetPassword(String email) {
        getmCompositeDisposable().add(ajustadorInteractor.resetPassword(email)
        .subscribe(() -> {
            getMvpView().showOK("Se enviará a tu correo electrónico tu nueva contraseña");
        }, throwable -> {
            Log.e(TAG, "Ocurrio un erro al recuperar la contraseña", throwable);
            getMvpView().showError(throwable.getMessage());
        }));

    }
}
