package com.gnp.ajustadormovil.android.views.ajuste;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity;
import com.gnp.ajustadormovil.android.views.ajuste.poliza.PolizaFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.PagerAdapter_Fragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.OnLongClick;

import static java.lang.Thread.sleep;

public class AjusteActivity extends BaseActivity {

    public static final String TAG = "TAG";
    public static final String defaultFormatDate = "yyyy-MM-dd HH:mm:ss";
    private static AjusteActivity capture_activity;
    @BindView(R.id.header_name)
    AppCompatTextView mName;
    @BindView(R.id.header_status)
    AppCompatTextView mStatus;
    @BindView(R.id.header_phone)
    AppCompatTextView mPhone;
    @BindView(R.id.header_version)
    AppCompatTextView header_version;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.result_tabs)
    TabLayout tabs;
    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.gpsline)
    LinearLayoutCompat mGPSline;
    String intentPoliza;
    String idAjuste;
    String ConfirmarArribo;
    String statusAdjustmend;
    public String estOperativo_User;
    private Bundle args;
    private Menu menu;
    private ModulosFragment cards;
    private String tipoUbicacion = "--";
    private Ajuste data;
    private Ajustador ajustador;
    private AjusteInteractor ajusteInteractor;


    public static AjusteActivity getCapture_activity() {
        return capture_activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_capture_parent);
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        Log.i(TAG, "CAPTURE");
        ajustador = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        capture_activity = this;
        FirebaseDatabase firebaseDataBase;
        DatabaseReference databaseReference;
        firebaseDataBase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDataBase.getReference();
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        header_version.setText("Versión: " + BuildConfig.VERSION_NAME);
        final DatabaseReference parseoInicial = databaseReference.child("ajustadores").child(AjustadorMovilApplication.getCedula());
        parseoInicial.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot childSnapshot) {
                updateAjustadorFirebase(childSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                updateAjustadorLocal();
            }
        });
        if(!ConnectionProvider.getInstance().isConnected() && ajustador != null){
            updateAjustadorLocal();
            mStatus.setText("SIN CONEXIÓN");
        }
        if (getIntent().getExtras() != null) {
            ConfirmarArribo = getIntent().getStringExtra("intentConfirmarArribo");
            intentPoliza = getIntent().getStringExtra("intentPoliza");
            idAjuste = getIntent().getStringExtra("idAjuste");
            statusAdjustmend = getIntent().getStringExtra("statusAdjustment");
            Log.d(TAG, "Capture_Activity Poliza: " + intentPoliza);
        }

        //5/1/2018 CLOSE
        args = new Bundle();
        args.putString("data", getIntent().getStringExtra("data"));
        String dataResponse = getIntent().getStringExtra("data");
        data = new Gson().fromJson(dataResponse, Ajuste.class);

        if (data != null
                && data.getSiniestro() != null
                && data.getSiniestro().getUbicacion() != null
                && data.getSiniestro().getUbicacion().getTipoUbicacion() != null
                && data.getSiniestro().getUbicacion().getTipoUbicacion().getClave() != null) {
            switch (data.getSiniestro().getUbicacion().getTipoUbicacion().getClave()) {
                case "AGC":
                    tipoUbicacion = "AGENCIA";
                    break;
                case "CRR":
                    tipoUbicacion = "CARRETERA";
                    break;
                case "CCR":
                    tipoUbicacion = "CIUDAD/CRUCERO";
                    break;
                case "ODM":
                    tipoUbicacion = "OFICINA/DOMICILIO";
                    break;
                default:
                    tipoUbicacion = "--";
                    break;
            }
        } else {
            tipoUbicacion = "No hay ubicación";
        }
        AppPreferences.setString(AppPreferences.TIPO_UBICACION,tipoUbicacion);

        initPager(viewPager, args);
        tabs.setupWithViewPager(viewPager);


        setmToolbar(mToolbar);
        newActivity = ACTIVITY_CAPTURE;

        if (getIntent().getSerializableExtra("fechaHoraArribo") != null) {
            showTimeArrived(tipoUbicacion);
        }

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        assert locationManager != null;

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mGPSline.setBackgroundResource(R.color.colorGreen);//.setBackgroundR.color.colorGreen);
        } else {
            mGPSline.setBackgroundResource(R.color.colorRedStatus);
        }

    }

    private void updateAjustadorFirebase(DataSnapshot childSnapshot){
        if (childSnapshot.exists()){
            mName.setText(String.format("%s %s %s", childSnapshot.child("nombres").exists()?childSnapshot.child("nombres").getValue(String.class):ajustador.getNombre()
                    , childSnapshot.child("aPaterno").exists()?childSnapshot.child("aPaterno").getValue(String.class):ajustador.getApellidoPaterno()
                    , childSnapshot.child("aMaterno").exists()?childSnapshot.child("aMaterno").getValue(String.class):ajustador.getApellidoMaterno()));
            mPhone.setText(ajustador.getCedula() + " - " + ajustador.getTelefono());
            estOperativo_User = childSnapshot.child("estOperativo").exists()?childSnapshot.child("estOperativo").getValue(String.class):"NO DISPONIBLE";
            if (!ConnectionProvider.getInstance().isConnected()) {
                mStatus.setText("SIN CONEXIÓN");
            } else {
                mStatus.setText(estOperativo_User);
            }
        }
        else{
            if(ajustador != null) {
                mName.setText(String.format("%s %s %s", ajustador.getNombre(), ajustador.getApellidoPaterno(), ajustador.getApellidoMaterno()));
                mPhone.setText(ajustador.getCedula() + " - " + ajustador.getTelefono());
            }
        }

    }

    private void updateAjustadorLocal(){
        ajustador = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        if(ajustador != null) {
            mName.setText(String.format("%s %s %s", ajustador.getNombre(), ajustador.getApellidoPaterno(), ajustador.getApellidoMaterno()));
            mPhone.setText(ajustador.getCedula() + " - " + ajustador.getTelefono());
        }
    }

    public String sendPolizaTo_Tab_Fragment_CapturePoliza() {
        return intentPoliza;
    }


    public void initPager(ViewPager viewPager, Bundle arguments) {
        PagerAdapter_Fragment mPagerAdapterFragment = new PagerAdapter_Fragment(getSupportFragmentManager());
        mPagerAdapterFragment.addFragment(SiniestroFragment.class, "Siniestro", arguments);
        mPagerAdapterFragment.addFragment(PolizaFragment.class, "Póliza", arguments);
        mPagerAdapterFragment.addFragment(ModulosFragment.class, "Captura", arguments);
        viewPager.setAdapter(mPagerAdapterFragment);
        viewPager.setCurrentItem(2);
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        tabs.setupWithViewPager(viewPager);
    }

    public void showTimeArrived(String tipoUbicacion) {
        ArriboDialog customAlert = new ArriboDialog();

        AlertDialog.Builder alert = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        alert.setTitle("Arribo Confirmado");

        String[] singleChoiceItems = getResources().getStringArray(R.array.alert_arribo_confirmado);
        int itemSelected = 0;
        CatalogoClaveAmis catalogo =
                new CatalogoClaveAmis();

        try {
            if (getIntent().getSerializableExtra("fechaHoraArribo") != null) {

                if (tipoUbicacion.equals("CIUDAD/CRUCERO") || tipoUbicacion.equals("CARRETERA")) {
                    if (tipoUbicacion.equals("CIUDAD/CRUCERO")){
                        catalogo.setClave("CCR");
                        catalogo.setNombre("CIUDAD/CRUCERO");
                    }
                    else{
                        catalogo.setClave("CRR");
                        catalogo.setNombre("CARRETERA");
                    }
                    String codigo = tipoUbicacion.equals("CIUDAD/CRUCERO") ? "CCR" : "CRR";
                    itemSelected = tipoUbicacion.equals("CIUDAD/CRUCERO") ? 0 : 1;
                    new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                            .setTitle("Arribo Confirmado" + "\n" + dateActualDate())
                            .setSingleChoiceItems(singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                                    switch (selectedIndex) {
                                        case 0:
                                            catalogo.setClave("CCR");
                                            catalogo.setNombre("CIUDAD/CRUCERO");
                                            data.getSiniestro().getUbicacion().setTipoUbicacion(catalogo);
                                            AppPreferences.setString(AppPreferences.TIPO_UBICACION,catalogo.getNombre());
                                            break;
                                        case 1:
                                            catalogo.setClave("CRR");
                                            catalogo.setNombre("CARRETERA");
                                            data.getSiniestro().getUbicacion().setTipoUbicacion(catalogo);
                                            AppPreferences.setString(AppPreferences.TIPO_UBICACION,catalogo.getNombre());
                                            break;
                                    }
                                }
                            })
                            .setPositiveButton("Aceptar", (dialog, which) -> {
                                try {
                                    sleep(1000);
                                    getCompositeDisposable().add(ajusteInteractor.modifyTipoUbicacion(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), catalogo.getClave())
                                            .subscribe(()->{
                                            }, throwable -> {
                                                Log.e(TAG, "Error al modificar el tipo de ubicación", throwable);
                                                new GeneralSnackBar().largeSnackError(AjusteActivity.this, throwable);
                                            }));
                                    data.getSiniestro().getUbicacion().setTipoUbicacion(catalogo);
                                    AppPreferences.setString(AppPreferences.TIPO_UBICACION,catalogo.getNombre());
                                    AppPreferences.setString(AppPreferences.JSON_AJUSTE_DETALLE,
                                            Utils.objectoToJsonString(data));
                                } catch (Exception e) {
                                    Log.e(TAG, "Error al modificar la ubicación", e);
                                }
                            })
                            .setCancelable(false)
                            .show();

                } else {
                    data.getSiniestro().getUbicacion().setTipoUbicacion(catalogo);
                    AppPreferences.setString(AppPreferences.TIPO_UBICACION,tipoUbicacion);
                    alert.setMessage(dateActualDate() + "\n\nTipo de ubicación: " + tipoUbicacion);
                    alert.setPositiveButton("Aceptar", null);
                    alert.setCancelable(false);
                    alert.show();
                }
            } else {
                alert.setMessage("Sin fecha de arribo" + "\n\nTipo de ubicación: " + tipoUbicacion);
                AppPreferences.setString(AppPreferences.TIPO_UBICACION,tipoUbicacion);
                alert.setPositiveButton("Aceptar", null);
                alert.show();
            }

        } catch (Exception e) {
            Log.e(TAG, "Error desconocido", e);
        }
    }

    public String dateActualDate() {
        String formatted;
        SimpleDateFormat sdf = new SimpleDateFormat(defaultFormatDate, Locale.forLanguageTag("es-MX"));
        sdf.setTimeZone(TimeZone.getDefault());
        Date datev = (Date) getIntent().getSerializableExtra("fechaHoraArribo");
        formatted = sdf.format(datev);
        return formatted;
    }

    public void updatedMenu() {
        MenuItem menuItem = menu.findItem(R.id.menu_disponible);
        MenuItem menuItemReceso = menu.findItem(R.id.menu_receso);
        MenuItem menuItemCerrar = menu.findItem(R.id.menu_cerrar);

        if (ajustador != null && ajustador.getEstatus() != null) {
            switch (ajustador.getEstatus()) {
                case NO_DISPONIBLE:
                case EN_RECESO:
                case RETIRADO:
                    menuItem.setTitle(Constants.AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(true);
                    break;
                case DISPONIBLE:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(true);
                    menuItemCerrar.setEnabled(false);
                    break;
                case POR_SALIR:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    break;
                case EN_CAMINO:
                case ATENDIENDO:
                case POR_CONFIRMAR:
                case POR_TERMINAR:
                case EN_ESPERA_SERVICIO_COBERTURA:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(false);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    break;
            }
        }

    }

    @OnLongClick(R.id.header_version)
    public boolean muestraVersion() {

        PackageInfo pInfo;
        String version = "";

        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error al obtener la versión de aplicación", e);
        }

        Toast.makeText(this, "Número de compilación:\n"
                + version + "-"
                + BuildConfig.ENVIRONMENT + "-"
                + BuildConfig.COMMIT
                + "-" + BuildConfig.LOCAL_CHANGES, Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture_activity = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDisconnected() {
        super.onDisconnected();
        mStatus.setText("SIN CONEXIÓN");
    }

    @Override
    protected void onConnected() {
        super.onConnected();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child("ajustadores")
                .child(AjustadorMovilApplication.getCedula())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        updateAjustadorFirebase(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        updateAjustadorLocal();
                    }
                });
    }
}
