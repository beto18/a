package com.gnp.ajustadormovil.android.views.ajuste;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lima on 09/10/17.
 * Modified by hgzavala on 10/04/2018
 * Modified by ABH on 14/06/2018
 */

public class AjustesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @BindView(R.id.siniestro_color)
    public LinearLayoutCompat mColorStatus;
    private List<Ajuste> list;
    private OnItemRemovedListener mClickListener;
    private EventBus bus;


    public AjustesAdapter(List<Ajuste> list) {
        this.list = list;
        this.bus = EventBus.getDefault();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DataViewHolder) {
            String reporta;
            if (list.get(position).getSiniestro() != null) {

                if (list.get(position).getSiniestro().getReporta() != null) {
                    String string_NombreReporta = list.get(position).getSiniestro().getReporta().getNombre() != null ? list.get(position).getSiniestro().getReporta().getNombre() : "";
                    String string_apellidoPReporta = list.get(position).getSiniestro().getReporta().getApellidoPaterno() != null ? list.get(position).getSiniestro().getReporta().getApellidoPaterno() : "";
                    String string_apellidoMReporta = list.get(position).getSiniestro().getReporta().getApellidoMaterno() != null ? list.get(position).getSiniestro().getReporta().getApellidoMaterno() : "";
                    reporta = string_NombreReporta + " " + string_apellidoPReporta + " " + string_apellidoMReporta;
                } else {
                    reporta = "No hay datos de la persona que reporta";
                }


                ((DataViewHolder) holder).idAjuste.setText(list.get(position).getSiniestro().getId() != null ? list.get(position).getSiniestro().getId() : "No hay id de siniestro");
                ((DataViewHolder) holder).estadoAjuste.setText(list.get(position).getEstatus() != null ? list.get(position).getEstatus().name() : "No hay estatus");
                ((DataViewHolder) holder).nombreReporta.setText(reporta);

                //
                if (list.get(position).getSiniestro().getConductor() == null) {
                    ((DataViewHolder) holder).nombreConductor.setText("No hay datos");
                } else {
                    ((DataViewHolder) holder).nombreConductor.setText(list.get(position).getSiniestro().getConductor().getNombreCompleto() != null ? list.get(position).getSiniestro().getConductor().getNombreCompleto() : "");
                }

                if (list.get(position).getSiniestro().getPoliza() != null) {
                    if (list.get(position).getSiniestro().getPoliza().getSegmentoCliente() == null) {
                        ((DataViewHolder) holder).polizaSegmentoCliente.setText("--");
                    } else {
                        ((DataViewHolder) holder).polizaSegmentoCliente.setText(list.get(position).getSiniestro().getPoliza().getSegmentoCliente().getNombre());
                    }
                    if (list.get(position).getSiniestro().getPoliza().getCliente() != null && !"".equals(list.get(position).getSiniestro().getPoliza().getCliente())) {
                        ((DataViewHolder) holder).cliente.setVisibility(View.VISIBLE);
                        ((DataViewHolder) holder).cliente.setText(list.get(position).getSiniestro().getPoliza().getCliente());
                    }
                    else {
                        ((DataViewHolder) holder).cliente.setVisibility(View.GONE);
                    }
                }

                if (list.get(position).getUbicacion() != null) {
                    ((DataViewHolder) holder).text_ubicacion_ajuste.setText(list.get(position).getUbicacion().getUbicationCompleta());
                } else {
                    ((DataViewHolder) holder).text_ubicacion_ajuste.setText("Ajuste sin ubicación");
                }


                if (list.get(position).getUbicacion() != null) {
                    if (list.get(position).getUbicacion().getReferencias() != null) {
                        ((DataViewHolder) holder).referencia.setText(list.get(position).getUbicacion().getReferencias());
                    } else {
                        ((DataViewHolder) holder).referencia.setText("Ajuste sin referencias");
                    }
                }

                ((DataViewHolder) holder).fechaReporte.setText(list.get(position).getSiniestro().getFechaReporte() != null ? DateUtils.datetime(list.get(position).getSiniestro().getFechaReporte()) : "no hay fecha");
                ((DataViewHolder) holder).fechaAsignacion.setText(list.get(position).getFechaAsignacion() != null ? DateUtils.datetime(list.get(position).getFechaAsignacion()) : "No hay fecha");

                if (list.get(position).getSiniestro().getVehiculo() != null) {
                    ((DataViewHolder) holder).marcaVehiculo.setText(
                            list.get(position).getSiniestro().getVehiculo().getFullVehicle() != null ?
                                    list.get(position).getSiniestro().getVehiculo().getFullVehicle()
                                    : "no hay datos del vehiculo");
                } else {
                    ((DataViewHolder) holder).marcaVehiculo.setText("Sin datos del vehículo");

                }

                switch (list.get(position).getEstatus()) {
                    case CONFIRMADO:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorConfirmedSatus));
                        break;
                    case ASIGNADO:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorStatusYellow));
                        break;

                    case CANCELADO:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorRedStatus));
                        break;

                    case LLEGADO:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorArivedStatus));
                        break;

                    case TERMINADO:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorGreen));
                        break;

                    case ERROR:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorRedStatus));
                        break;

                    default:
                        ((DataViewHolder) holder).mColorStatus.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorLigthGray));
                        break;
                }
            } else
                Log.e(this.toString(), "\n\n\tEl Ajuste " + list.get(position).getId() + ", no tiene datos asociados a un Siniestro.");
        } else {

        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_siniestro_item, parent, false);
            return new AjustesAdapter.DataViewHolder(v, mClickListener);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_siniestro_empty, parent, false);
            return new AjustesAdapter.EmptyViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.isEmpty()) {
            return Constants.TYPE_EMPTY;
        } else {
            return Constants.TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        if (list.isEmpty()) return 1;
        else return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.indexOf(getItem(position));
    }

    public OnItemRemovedListener getClickListener() {
        return mClickListener;
    }

    public void setClickListener(OnItemRemovedListener clickListener) {
        mClickListener = clickListener;
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_idAjuste)
        AppCompatTextView idAjuste;

        @BindView(R.id.sinisestro_item_nombre)
        AppCompatTextView nombreReporta;

        @BindView(R.id.polizaSegmentoCliente)
        AppCompatTextView polizaSegmentoCliente;

        @BindView(R.id.text_estadoAjuste)
        AppCompatTextView estadoAjuste;

        @BindView(R.id.text_NombreReporta)
        AppCompatTextView nombreConductor;

        @BindView(R.id.text_ubicacion_ajuste)
        AppCompatTextView text_ubicacion_ajuste;

        @BindView(R.id.text_fechaReporte)
        AppCompatTextView fechaReporte;

        @BindView(R.id.text_fechaAsignacion)
        AppCompatTextView fechaAsignacion;

        @BindView(R.id.text_marcaVehiculo)
        AppCompatTextView marcaVehiculo;

        @BindView(R.id.siniestro_color)
        LinearLayoutCompat mColorStatus;

        @BindView(R.id.text_referencia)
        AppCompatTextView referencia;

        @BindView(R.id.tvCliente)
        AppCompatTextView cliente;

        /*
         */
        DataViewHolder(View itemView, OnItemRemovedListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(itemView, getAdapterPosition());//getAdapterPosition
                    Log.i("SiniestrosAdapter", "Position = " + getAdapterPosition());
                    bus.post(list.get(getAdapterPosition()).getId());
                }
            });
            itemView.setOnLongClickListener(view -> {
                itemClickListener.onLongItemClick(itemView, getAdapterPosition());
                return true;
            });
        }

    }

}
