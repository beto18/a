package com.gnp.ajustadormovil.android.views.ajuste;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjustadorInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.EstatusAjuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.ModuloException;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity;
import com.gnp.ajustadormovil.android.views.commons.adapters.RechazosAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 *
 */

public class AjustesFragment extends BaseFragment implements OnItemRemovedListener {
    private static final String TAG = AjustesFragment.class.getSimpleName();
    public static String PositionForcodSiniestro;
    public static String PositionForServicioAjuste;
    public static String PositionForEstServicioAjuste;
    public static String PositionAjustador;
    public static String PositionToken;
    public static String PositionLat;
    public static String PositionLng;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;
    private RecyclerView recycler;
    private String intentFromHomeActivity_User;
    private String nameFromHomeActivity;
    private String phoneFromHomeActivity;
    private CompositeDisposable compositeDisposable;
    private Unbinder mUnbinder;
    private int posicionRechazo;
    private ArrayList<CatalogoClave> rechazos = new ArrayList<>();
    private List<String> nombreRechazos = new ArrayList<>();
    private int posicion;
    private Ajuste mResponse;
    private List<Ajuste> ajustes = new ArrayList<>();
    private RechazosAdapter rechazosAdapter;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private TprInteractor tprService;
    private AjusteInteractor ajusteInteractor;
    private AjustadorInteractor ajustadorInteractor;

    private HomeActivity mActivity; //HomeActivity

    private final BroadcastReceiver presiniestroListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(Constants.PAYLOAD_EVOLUTION.equals(intent.getAction())){
                getAjustesWS();
            }
        }

    };

    public static AjustesFragment newInstance() {
        AjustesFragment fragment = new AjustesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (HomeActivity) getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_siniestros, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        recycler = view.findViewById(R.id.seguimiento_recycler);
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        compositeDisposable = new CompositeDisposable();
        tprService = CatalogoInteractorsFactory.getTprService();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        ajustadorInteractor = OrquestadorInteractorFactory.getAjustadorInteractor();
        initElements();

        HomeActivity activity = (HomeActivity) getActivity();
        assert activity != null;
        intentFromHomeActivity_User = activity.sendMyUserTo_Tab_Fragment_SiniestrosList();
        nameFromHomeActivity = activity.sendMyNameTo_Tab_Fragment_SiniestrosList();
        phoneFromHomeActivity = activity.sendMyTelTo__Tab_Fragment_SiniestrosList();

        PositionAjustador = intentFromHomeActivity_User;

        getRechazos();
        checkFirebaseTable();

        //hgzavala - 26042018 - Se inicializa el ID Ajuste Actual a null
        AppPreferences.setString(AppPreferences.ID_AJUSTE_ACTUAL, null);
        //hgzavala - 26042018 - Se inicializa el Ajuste Detalle a null
        AppPreferences.setString(AppPreferences.JSON_AJUSTE_DETALLE, null);

        getActivity().registerReceiver(presiniestroListener, new IntentFilter(Constants.PAYLOAD_EVOLUTION));

        return view;

    }

    private void initElements() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);
        recycler.setNestedScrollingEnabled(true);
        recycler.hasFixedSize();
        swipeContainer.setOnRefreshListener(() -> {
            swipeContainer.setRefreshing(false);
            AppPreferences.cleanAjustes(AppPreferences.JSON_AJUSTES_OPERACIONES);
            getAjustesWS();
        });
    }

    private void getRechazos() {
        getCompositeDisposable().add(siniestrosInteractor.getMotivosRechazoAjuste()
                .subscribe(this::setRechazos,
                        throwable -> {
                    if(!(throwable instanceof NetworkException)) {
                        new GeneralSnackBar().largeSnackError(mActivity, throwable);
                    }
                }));
    }


    private void setRechazos(List<CatalogoClave> rechazosList) {
        rechazosAdapter = new RechazosAdapter(rechazosList);
    }

    private void checkSync() {
        Log.i(TAG, "Sincronizar ajustes");
        boolean connected = ConnectionProvider.getInstance().isConnected();
        if (ajustes != null && !ajustes.isEmpty() && connected) {
            for (Ajuste ajuste : ajustes) {
                if (ajuste.getEstatus().equals(EstatusAjuste.SINCRONIZANDO)) {
                    Log.i(TAG, "Sincronizando ajuste " + ajuste.getId());
                    getCompositeDisposable().add(OrquestadorInteractorFactory
                            .getSyncService()
                            .synchronizeData(ajuste.getId())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .andThen(ajusteInteractor.cerrar(ajuste.getId(), ajuste.getMotivoCierre(), ajuste.getConclusionCierre(), true))
                            .subscribe(() -> {
                                Log.i(TAG, "Se sincronizó el ajuste: " + ajuste.getId());
                                ajuste.setEstatus(EstatusAjuste.TERMINANDO);
                                adapter.notifyDataSetChanged();
                            }, throwable -> {
                                if (throwable instanceof ModuloException) {
                                    Log.e(TAG, "Ocurrió un error al sincronizar el ajuste: " + ajuste.getId(), throwable);
                                    ajuste.setEstatus(EstatusAjuste.ERROR);
                                    adapter.notifyDataSetChanged();
                                    ToastUtils.toastLongError(throwable);
                                }
                            }));
                }
                if (ajuste.getFechaTerminado() != null) {
                    long timeout = 5 * 60 * 1000;
                    boolean isTimeout = (System.currentTimeMillis() - timeout) > ajuste.getFechaTerminado().getTime();
                    if (ajuste.getEstatus().equals(EstatusAjuste.TERMINANDO) && isTimeout) {
                        getCompositeDisposable().add(ajusteInteractor.cerrar(ajuste.getId(), ajuste.getMotivoCierre(), ajuste.getConclusionCierre(), true)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(() -> {
                                    Log.i(TAG, "El ajuste " + ajuste.getId() + " se cerró correctamente");
                                }, throwable -> {
                                    Log.e(TAG, "Error al cerrar ajuste " + ajuste.getId(), throwable);
                                }));
                    }
                }
            }
        }
    }

    private void getAjustesWS() {
        compositeDisposable.add(ajusteInteractor.getAjustes()
                .subscribe(response -> {
                    Log.d(TAG, "onResponse: " + response);
                    ajustes = response;
                    AppPreferences.setString(AppPreferences.JSON_AJUSTES,
                            Utils.objectoToJsonString(response));
                    if (ajustes != null) {
                        ServicioAjuste_Screen();
                        checkSync();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al obtener los ajustes", throwable);
                    new GeneralSnackBar().largeSnackError(mActivity, throwable);
                }));
    }

    private void checkFirebaseTable() {
        FirebaseDatabase firebaseDataBase;
        DatabaseReference databaseReference;
        firebaseDataBase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDataBase.getReference();
        String idCedula = AppPreferences.getString(AppPreferences.USER_ID_CEDULA);
        final DatabaseReference tableFB = databaseReference.child("ajustadores")
                .child(idCedula).child("fechaHoraPersistencia");
        tableFB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("Data", "data is change on table ");
                final Handler handler = new Handler();
                handler.postDelayed(() ->
                                getAjustesWS(),
                        1000);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (!ConnectionProvider.getInstance().isConnected()) {
            getAjustesWS();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    AjustesAdapter adapter;

    public void ServicioAjuste_Screen() {
        adapter = new AjustesAdapter(ajustes);
        adapter.setClickListener(this);
        adapter.getClickListener();
        AppPreferences.setString(AppPreferences.LOGUEO_PRIMERA_VEZ, "true");
        recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /*
    Send User to Adapter
     */
    public String SendUserTo_SiniestrosAdapter() {
        return intentFromHomeActivity_User;
    }

    @OnClick(R.id.seguimiento_recycler)
    public void showAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
        alert.show();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        getActivity().unregisterReceiver(presiniestroListener);
    }

    public String getDetalleEngine(int position) {

        String codSiniestro_Data = ajustes.get(position).getSiniestro().getId();
        String selectedAdj = ajustes.get(position).getId();
        String esServicioAjuste_Data = ajustes.get(position).getEstatus().name();
        String Token_Data = AppPreferences.getString(AppPreferences.USER_INFO_TOKEN);

        String lat_cursor;
        String lng_cursor;
        if (ajustes.get(position).getSiniestro().getUbicacion() != null) {
            lat_cursor = "" + ajustes.get(position).getSiniestro().getUbicacion().getLatitud();
            lng_cursor = "" + ajustes.get(position).getSiniestro().getUbicacion().getLongitud();
        } else {
            lat_cursor = "0";
            lng_cursor = "0";
        }
        Log.i(TAG, "DataFrom Siniestro Id = " + codSiniestro_Data);
        Log.i(TAG, "DataFrom Ajuste Id = " + selectedAdj);
        Log.i(TAG, "DataFrom Estatus Ajuste = " + esServicioAjuste_Data);

        PositionForcodSiniestro = codSiniestro_Data;
        PositionForServicioAjuste = selectedAdj;
        PositionForEstServicioAjuste = esServicioAjuste_Data;
        PositionToken = Token_Data;
        PositionLat = lat_cursor;
        PositionLng = lng_cursor;
        return esServicioAjuste_Data;
    }

    @Subscribe
    public void getServicioReal(String servicio) {
        String servicio_ajustes = servicio;
        Log.d(TAG, "getServicioReal: " + servicio_ajustes);
    }

    public void onItemClick(View view, int x) {
        this.posicion = x;
        Log.d(AjustesFragment.TAG, "\n\tview:" + view.toString() + " position:" + posicion);
        if (ajustes.get(posicion).getSiniestro() != null) {
            getDetalleEngine(posicion);

            AppPreferences.setString(AppPreferences.ID_AJUSTE_ACTUAL, PositionForServicioAjuste);

            switch (ajustes.get(posicion).getEstatus()) {
                case ASIGNADO:
                    Log.d(TAG, "Ajuste ASIGNADO...\n\n\t\t #Ajuste:" + ajustes.get(posicion).getId() + " #Siniestro:" + ajustes.get(posicion).getSiniestro().getId());
                    AlertDialog.Builder alert = new AlertDialog.Builder(mActivity, R.style.AlertDialogTheme);
                    alert.setTitle("Iniciar atención ");
                    alert.setMessage("Al entrar al detalle estas aceptando la atención del siniestro");
                    alert.setPositiveButton("Confirmar", onClickListenerAccept);
                    alert.setNegativeButton("Rechazar", onClickListenerCancel);
                    alert.show();
                    AppPreferences.removeString(AppPreferences.JSON_AJUSTE_DETALLE);
                    break;

                case CONFIRMADO:
                    Log.d(TAG, "Ajuste CONFIRMADO...\n\n\t\t #Ajuste:" + ajustes.get(posicion).getId() + " #Siniestro:" + ajustes.get(posicion).getSiniestro().getId());

                    Intent intent = new Intent(getContext(), ConfirmacionActivity.class);
                    intent.putExtra("intentCodSiniestro", PositionForcodSiniestro);
                    intent.putExtra("intentLogIn", intentFromHomeActivity_User);
                    intent.putExtra("mName", nameFromHomeActivity);
                    intent.putExtra("mPhone", nameFromHomeActivity);


                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    PendingIntent pendingIntent =
                            TaskStackBuilder.create(mActivity.getApplicationContext())
                                    .addNextIntentWithParentStack(intent)
                                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(mActivity.getApplicationContext());
                    builder.setContentIntent(pendingIntent);

                    AppPreferences.removeString(AppPreferences.JSON_AJUSTE_DETALLE);
                    startActivity(intent);
                    mActivity.finish();
                    break;
                case ERROR:
                case LLEGADO:
                    getCompositeDisposable().add(ajusteInteractor.getAjuste(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                            .subscribe(response -> {
                                mResponse = response;

                                AppPreferences.setString(AppPreferences.JSON_AJUSTE_DETALLE,
                                        Utils.objectoToJsonString(response));

                                String mData = new Gson().toJson(mResponse);
                                Intent intentLLegado = new Intent(mActivity, AjusteActivity.class);
                                intentLLegado.putExtra("intentCodSiniestro", PositionForcodSiniestro);
                                intentLLegado.putExtra("statusAdjustment", ajustes.get(posicion).getEstatus().name());
                                //intent.putExtra ("intentIdAjuste", id_ajuste); //list.indexOf(getItem(position));
                                intentLLegado.putExtra("intentLogIn", intentFromHomeActivity_User);
                                intentLLegado.putExtra("servicio_ajuste", ajustes.get(posicion).getId());

                                intentLLegado.putExtra("data", mData);
                                intentLLegado.putExtra("intentCodSiniestro", mResponse.getSiniestro().getId());
                                intentLLegado.putExtra("idAjuste", mResponse.getId());

                                if (mResponse.getSiniestro().getPoliza() != null
                                        && mResponse.getSiniestro().getPoliza().getConductorHabitual() != null
                                        && mResponse.getSiniestro().getPoliza().getConductorHabitual().getRfc() != null) {
                                    intentLLegado.putExtra("intentPoliza", mResponse.getSiniestro().getPoliza().getConductorHabitual().getRfc());
                                } else {
                                    Log.e(this.toString(), "No hay datos de la Póliza");
                                }

                                intentLLegado.putExtra("mNameText", nameFromHomeActivity);
                                intentLLegado.putExtra("mPhoneText", phoneFromHomeActivity);

                                intentLLegado.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                intentLLegado.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intentLLegado.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                //intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                                PendingIntent pendingIntentLLegado =
                                        TaskStackBuilder.create(mActivity.getApplicationContext())
                                                .addNextIntentWithParentStack(intentLLegado)
                                                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                                NotificationCompat.Builder builderLLegado = new NotificationCompat.Builder(mActivity.getApplicationContext());
                                builderLLegado.setContentIntent(pendingIntentLLegado);

                                startActivity(intentLLegado);
                                mActivity.finish();
                            }, throwable -> {
                                new GeneralSnackBar().largeSnackError(mActivity, throwable.getMessage());
                            }));
                    break;
                default:
                    Log.d(TAG, "Estatus no valido...\n\n\t\t #Ajuste" + ajustes.get(posicion).getId() + " #Siniestro:" + ajustes.get(posicion).getSiniestro().getId());
                    break;
            }
        } else {
            Toast.makeText(mActivity, "El Ajuste " + ajustes.get(posicion).getId() + ", no tiene datos de Siniestro", Toast.LENGTH_SHORT).show();
            Log.e(AjustesFragment.TAG, "El Ajuste " + ajustes.get(posicion).getId() + ", no tiene datos de Siniestro");
        }

    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    DialogInterface.OnClickListener onClickListenerAccept = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            NotificationManager notificationManager = (NotificationManager)
                    mActivity.getApplicationContext()
                            .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.NOTIFICATION_ACCEPT_ADJUSTMENT);
            getCompositeDisposable().add(ajusteInteractor.confirmar(AppPreferences.getString(
                    AppPreferences.ID_AJUSTE_ACTUAL))
                    .subscribe(() -> {
                        Intent intent = new Intent(getContext(), ConfirmacionActivity.class);
                        intent.putExtra("intentLogIn", intentFromHomeActivity_User);

                        //Inicializa en 0, la latitud y longitud, esta se debe de actualizar con la verdadera ubicación cuando se ACEPTE el Servicio de Ajuste
                        intent.putExtra("intentLat", 0);
                        intent.putExtra("intentLng", 0);

                        intent.putExtra("servicio_ajuste", PositionForServicioAjuste);

                        intent.putExtra("mName", nameFromHomeActivity);
                        intent.putExtra("mPhone", phoneFromHomeActivity);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        PendingIntent pendingIntent =
                                TaskStackBuilder.create(mActivity.getApplicationContext())
                                        .addNextIntentWithParentStack(intent)
                                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(mActivity.getApplicationContext());
                        builder.setContentIntent(pendingIntent);

                        startActivity(intent);
                        mActivity.finish();
                    }, throwable -> {
                        Log.e(TAG, "Error al confirmar", throwable);
                        new GeneralSnackBar().largeSnackError(mActivity, throwable);
                    }));
        }
    };

    DialogInterface.OnClickListener onClickListenerCancel = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            NotificationManager notificationManager2 = (NotificationManager)
                    mActivity.getApplicationContext()
                            .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager2.cancel(Constants.NOTIFICATION_ACCEPT_ADJUSTMENT);
            AlertDialog.Builder mDialog = new AlertDialog.Builder(mActivity, R.style.AlertDialogTheme);
            LayoutInflater inflater = getLayoutInflater();
            View mView = inflater.inflate(R.layout.dialog_cancelaciones, null);
            mDialog.setView(mView);
            RecyclerView rvCancelaciones = mView.findViewById(R.id.rvCancelaciones);
            rvCancelaciones.setAdapter(rechazosAdapter);
            rvCancelaciones.setLayoutManager(new LinearLayoutManager(mActivity));
            rvCancelaciones.setHasFixedSize(true);
            mDialog.setTitle(getString(R.string.rechazo_message));
            mDialog.setCancelable(false);
            mDialog.setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss());
            mDialog.setPositiveButton("Aceptar", (dialog, which) ->
                    getCompositeDisposable().add(ajusteInteractor.rechazar(PositionForServicioAjuste, rechazosAdapter.getKey())
                            .subscribe(() -> {
                                new GeneralSnackBar().largeSnackOk(mActivity, "Se rechazo correctamente el siniestro");
                                new Handler().postDelayed(() -> getAjustesWS(),1000);
                            }, throwable -> {
                                new GeneralSnackBar().largeSnackError(mActivity, throwable.getMessage());
                            })));
            mDialog.show();
        }
    };

    @Override
    public void onConnected() {
        super.onConnected();
        getAjustesWS();
    }
}
