package com.gnp.ajustadormovil.android.views.ajuste;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;

public class ArriboDialog {

    TextView titulo ;
    TextView contenido ;
    LinearLayout layout ;
    RadioGroup radioGroup ;
    RadioButton radioButton1 ;
    RadioButton radioButton2;
    Button dialogButton;


    public void showDialog(Activity activity, String tt,String msg,String tipo){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_arribo_confirmado);

        titulo = dialog.findViewById(R.id.txt_title_cd_arribo_confirmado);
        titulo.setText(tt);
        contenido = dialog.findViewById(R.id.txt_content_cd_arribo_confirmado);
        layout = dialog.findViewById(R.id.linear_layout_arribo_confirmado);
        radioGroup = dialog.findViewById(R.id.radio_grup_arribo_confirmado);
        radioButton1 = dialog.findViewById(R.id.radio_button_arribo_confirmado_1);
        radioButton2 = dialog.findViewById(R.id.radio_button_arribo_confirmado_2);
        dialogButton = dialog.findViewById(R.id.btn_confirmar_arribo_confirmado);
        if(tipo.equals("crt")|| tipo.equals("crc")) {
            contenido.setVisibility(View.INVISIBLE);
            layout.setVisibility(View.VISIBLE);
            dialogButton.setEnabled(false);
        }
        if(tipo.equals("crt")|| tipo.equals("crc")) {
            contenido.setVisibility(View.VISIBLE);
            layout.setVisibility(View.INVISIBLE);
            dialogButton.setEnabled(true);
            contenido.setText(msg);
        }

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_button_arribo_confirmado_1:
                if (checked)
                    dialogButton.setEnabled(true);
                    break;
            case R.id.radio_button_arribo_confirmado_2:
                if (checked)
                    dialogButton.setEnabled(true);
                    break;
        }
    }
}
