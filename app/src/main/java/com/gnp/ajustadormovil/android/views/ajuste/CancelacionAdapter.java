package com.gnp.ajustadormovil.android.views.ajuste;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

import java.util.List;

public class CancelacionAdapter extends RecyclerView.Adapter<CancelacionAdapter.CancelacionesHolder>{

    private List<CatalogoClave> list;
    private int mSelectedItem = -1;
    private String key;

    public CancelacionAdapter(List<CatalogoClave> list){
        this.list = list;
    }

    @NonNull
    @Override
    public CancelacionesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cancelaciones,parent,false);
        return new CancelacionesHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull CancelacionesHolder holder, int position) {
        if (list.get(position).getClave()!=null){
            holder.rbCancelacion.setText(list.get(position).getNombre());
        }
        holder.rbCancelacion.setChecked(position==mSelectedItem);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public String getKey(){
        return key;
    }

    class CancelacionesHolder extends RecyclerView.ViewHolder{

        private RadioButton rbCancelacion;

        public CancelacionesHolder(View itemView) {
            super(itemView);
            rbCancelacion = itemView.findViewById(R.id.rbCancelacion);
            View.OnClickListener clickListener = v -> {
                mSelectedItem=getAdapterPosition();
                notifyDataSetChanged();
            };
            itemView.setOnClickListener(clickListener);
            rbCancelacion.setOnClickListener(clickListener);
            rbCancelacion.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (mSelectedItem==getAdapterPosition()){
                    key = list.get(getAdapterPosition()).getClave();
                }
            });
        }
    }
}
