package com.gnp.ajustadormovil.android.views.ajuste;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.ajuste.AjusteOperacion;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.ajustador.Ajustador;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.AjusteArriboException;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity;
import com.gnp.ajustadormovil.android.views.ajuste.poliza.PolizaCoberturaAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.CustomCamera;
import com.gnp.ajustadormovil.android.views.commons.custom.ItemDecorator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import io.reactivex.functions.Action;
import retrofit2.Retrofit;

import static android.app.PendingIntent.getActivity;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_ARRIBO;
import static com.gnp.ajustadormovil.android.views.ajuste.AjustesFragment.PositionForServicioAjuste;


public class ConfirmacionActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int REQUEST_LOCATION_PERMISSION_CODE = 101;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final String TAG = ConfirmacionActivity.class.getCanonicalName();//Home_Fragment_Info.class.getSimpleName();
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    static Handler handlerSendingPosition;
    private static ConfirmacionActivity confirm_activity;
    private final int CALL_REQUEST = 100;
    public float distancia = 0.0f;
    String estOperativo_User;
    //Firebase db instances
    DatabaseReference databaseReference;
    boolean startedGeoEngine = false;
    //-->
    String intentLogIn_intent;
    String intentCodSiniestro;
    String intentAjuste;
    String lat0b;
    String lng0b;
    //create textview for the body
    TextView mEncamino_siniestro,
            mEncamino_status,
            mEncamino_tipo_siniestro,
            mEncamino_asignacion,
            mEncamino_conductor,
            mEncamino_contratante,
            mEncamino_tipo_cliente,
            mEncamino_vehiculo,
            mEncamino_serie_vehiculo;
    String TIPO_PHONE_data;
    Handler handlerClock;
    @BindView(R.id.header_name)
    AppCompatTextView mName;
    @BindView(R.id.header_status)
    AppCompatTextView mStatus;
    @BindView(R.id.header_phone)
    AppCompatTextView mPhone;
    @BindView(R.id.header_version)
    AppCompatTextView header_version;
    //-->
    @BindView(R.id.gpsline)
    LinearLayoutCompat mGPSline;
    @BindView(R.id.encamino_detail)
    LinearLayout mLayaultDetail;
    //DataBaseHelper db;
    @BindView(R.id.detail_content_asegurado)
    LinearLayoutCompat mLayaultAsegurado;
    @BindView(R.id.detail_content_reportador)
    LinearLayoutCompat mLayaultReportador;
    @BindView(R.id.detail_content_car)
    LinearLayoutCompat mLayaultCar;
    @BindView(R.id.imgDetailAse)
    AppCompatImageView detailAsegurado;
    @BindView(R.id.imgDetailPol)
    AppCompatImageView detailPoliza;
    @BindView(R.id.imgDetailCober)
    AppCompatImageView detailCobertura;
    @BindView(R.id.detail_content_poliza)
    LinearLayoutCompat mLayaultPoliza;
    @BindView(R.id.encamino_route)
    LinearLayoutCompat mLayaultRoute;
    @BindView(R.id.detail_cobertura)
    LinearLayoutCompat detail_cobertura;
    @BindView(R.id.encamnino_confirmar)
    AppCompatButton mbuttonConfirmar;
    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;
    //@BindView(R.id.home_rgroup_menu) RadioGroup mRadioGroup;
    @BindView(R.id.poliza_cobertutas)
    RecyclerView recyclerCoberturas;
    LinearLayoutCompat[] acordeon = {null, null, null};
    @BindView(R.id.image)
    ImageView image;
    @BindViews({R.id.detail_reportador_name,
            R.id.detail_reportador_phone,
            R.id.detail_reportador_reporte,
            R.id.detail_reportador_estado,
            R.id.detail_reportador_municipio,
            R.id.detail_reportador_colonia,
            R.id.detail_reportador_calle,
            R.id.detail_reportador_numero,
            R.id.detail_reportador_codigo_postal,
            R.id.detail_reportador_coments})
    List<TextView> datos_reportador;
    @BindViews({R.id.detail_poliza_numero, R.id.detail_poliza_status, R.id.detail_poliza_cobranza})
    List<TextView> datos_poliza;
    @BindView(R.id.FOOTER)
    AppCompatButton FOOTERFocus;
    LocationManager lm;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    boolean gpsIsActive;
    Timer timer1;
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            locationResult.gotLocation(location);
            lm.removeUpdates(this);
            lm.removeUpdates(locationListenerGps);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            timer1.cancel();
            locationResult.gotLocation(location);
            lm.removeUpdates(this);
            lm.removeUpdates(locationListenerNetwork);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    boolean proximityDialogShown;
    private Menu menu;
    private int position;
    private GoogleMap googleMap;
    private GeofencingRequest geofencingRequest;
    private GoogleApiClient googleApiClient;
    private boolean isMonitoring = false;
    private MarkerOptions markerOptions;
    private Marker currentLocationMarker;
    private PendingIntent pendingIntent;
    //private CameraUtils cameraUtils;
    private Ajuste mResponse;
    private List<PolizaCobertura> l = new ArrayList<>();
    private LocationManager locationManager;
    private LocationManager mLocMgr;
    private ArrayList<CatalogoClave> cancelaciones = new ArrayList<>();
    private List<String> nombreCancelaciones = new ArrayList<>();
    private CancelacionAdapter cancelacionesAdapter;
    private Retrofit mRetrofit;
    private RecyclerView.LayoutManager lManager;
    private PolizaCoberturaAdapter adapter;
    private Uri imageUri;
    private Bitmap tempImage;
    private double latitud;
    private double longitud;
    private SiniestrosInteractor siniestrosInteractor;
    private AjusteInteractor ajusteInteractor;
    private Ajustador ajustador;

    public LocationResult locationResult = new LocationResult() {

        @Override
        public void gotLocation(Location location) {
            try {
                if(location != null) {
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();
                } else {
                    latitud = 0.0;
                    longitud = 0.0;
                }
            } catch (Exception e) {
                Log.e(TAG, "Error al colocar la información de la posición", e);
            }
        }
    };
    public Runnable runnableSendingPosition = new Runnable() {

        public void run() {
            try {
                boolean r = getLocation(getApplicationContext(), locationResult);
                if (r) {
                    if (latitud != 0 & longitud != 0) {
                        Log.d(TAG, "GPS(" + latitud + "," + longitud + ")");
                        ConfirmacionActivity.this.validateButton(latitud, longitud);
                    } else if(latitud == 0 && longitud == 0 && !ConnectionProvider.getInstance().isConnected()){
                        displayConfirmBtn();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Error al obtener posición", e);
            }

            if (distancia >= 10000) {
                handlerSendingPosition.postDelayed(this, 60000);
            } else if (distancia >= 5000) {
                handlerSendingPosition.postDelayed(this, 30000);
            } else if (distancia >= 1000) {
                handlerSendingPosition.postDelayed(this, 10000);
            } else if (distancia > 0) {
                handlerSendingPosition.postDelayed(this, 4000);
            } else if (distancia == 0) {
                handlerSendingPosition.postDelayed(this, 1000);
            }

        }
    };

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public boolean getLocation(Context context, LocationResult result) {
        locationResult = result;
        if (lm == null)
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }


        if (!gps_enabled && !network_enabled)
            return false;

        if (gps_enabled)
            if (ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return true;
            }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        if (network_enabled)
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        timer1 = new Timer();

        timer1.schedule(new GetLastLocation(), 3000);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        ajustador = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        FirebaseDatabase firebaseDataBase;
        DatabaseReference databaseReference;
        firebaseDataBase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDataBase.getReference();
        final DatabaseReference parseoInicial = databaseReference.child("ajustadores").child(AjustadorMovilApplication.getCedula());
        parseoInicial.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot childSnapshot) {
                updateAjustadorFirebase(childSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                updateAjustadorLocal();
            }
        });
        if(!ConnectionProvider.getInstance().isConnected()){
            new Handler().post(() -> {
                updateAjustadorLocal();
                mStatus.setText("SIN CONEXIÓN");
            });
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        } else {
        }
        confirm_activity = this;
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        assert locationManager != null;


        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mGPSline.setBackgroundResource(R.color.colorGreen);
            FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(true);
            gpsIsActive = true;
            try {
                //updatedMenu();
                MenuItem gpsMenuItem = menu.findItem(R.id.tools_gps);
                gpsMenuItem.setTitle(R.string.txt_tools_gps_on);
                gpsMenuItem.setIcon(R.drawable.baseline_location_on_black_24dp);
            } catch (Exception e) {
                Log.w(TAG, "Error al iniciar menu");
            }
        } else {
            mGPSline.setBackgroundResource(R.color.colorRedStatus);
            FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(false);
            gpsIsActive = false;
            try {
                //updatedMenu();
                MenuItem gpsMenuItem = menu.findItem(R.id.tools_gps);
                gpsMenuItem.setTitle(R.string.txt_tools_gps_off);
                gpsMenuItem.setIcon(R.drawable.baseline_location_off_black_24dp);
            } catch (Exception e) {
                Log.w(TAG, "Error al iniciar menu");
            }
        }
        header_version.setText("Versión: " + BuildConfig.VERSION_NAME);
        FOOTERFocus.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        handlerClock = new Handler();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        //cameraUtils = new CameraUtils(this, 0);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        ButterKnife.bind(this);
        initSettings();

    }

    /**
    show detail layer (green text in UI)
     */
    HashSet<AjusteOperacion> ao;

    @OnClick(R.id.encamnino_cancelar)
    public void cancelarSiniestro() {
        AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View mView = inflater.inflate(R.layout.dialog_cancelaciones,null);
        mDialog.setView(mView);
        final RecyclerView rvCancelaciones = mView.findViewById(R.id.rvCancelaciones);
        rvCancelaciones.setAdapter(cancelacionesAdapter);
        rvCancelaciones.setLayoutManager(new LinearLayoutManager(this));
        rvCancelaciones.setHasFixedSize(true);
        mDialog.setTitle("Cancelar");
        mDialog.setMessage(getString(R.string.cancel_message));
        mDialog.setCancelable(false);
        mDialog.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mDialog.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ao = new Gson().fromJson(
                        AppPreferences.getString(AppPreferences.JSON_AJUSTES_OPERACIONES),
                        new TypeToken<Set<AjusteOperacion>>() {
                        }.getType());

                AjusteOperacion ajusteOperacion = new AjusteOperacion(PositionForServicioAjuste, Constants.CANCELADO);

                if (ao != null) {
                    ao.add(ajusteOperacion);
                } else {
                    ao = new HashSet<AjusteOperacion>();
                    ao.add(ajusteOperacion);
                }

                AppPreferences.setString(AppPreferences.JSON_AJUSTES_OPERACIONES,
                        Utils.objectoToJsonString(ao));
            }
        });
        mDialog.show();
    }


    @OnClick(R.id.detail_reportador)
    public void showReportador() {
        //showSeccion(0);
        if(mLayaultReportador.getVisibility() == View.GONE){
            mLayaultReportador.setVisibility(View.VISIBLE);
            detailAsegurado.setBackgroundDrawable(getDrawable(R.drawable.ic_less));
            detailAsegurado.setRotation(0);
        }
        else{
            mLayaultReportador.setVisibility(View.GONE);
            detailAsegurado.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
            detailAsegurado.setRotation(270);
        }
        mLayaultPoliza.setVisibility(View.GONE);
        detailPoliza.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
        detailPoliza.setRotation(270);
        recyclerCoberturas.setVisibility(View.GONE);
        detailCobertura.setRotation(270);
        detailCobertura.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
    }

    @OnClick(R.id.detail_car)
    public void showCar() {
        //showSeccion(1);
    }

    @OnClick(R.id.detail_poliza)
    public void showPoliza() {
        //showSeccion(2);
        if(mLayaultPoliza.getVisibility() == View.GONE){
            mLayaultPoliza.setVisibility(View.VISIBLE);
            detailPoliza.setBackgroundDrawable(getDrawable(R.drawable.ic_less));
            detailPoliza.setRotation(0);
        }
        else{
            mLayaultPoliza.setVisibility(View.GONE);
            detailPoliza.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
            detailPoliza.setRotation(270);
        }
        mLayaultReportador.setVisibility(View.GONE);
        detailAsegurado.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
        detailAsegurado.setRotation(270);
        recyclerCoberturas.setVisibility(View.GONE);
        detailCobertura.setRotation(270);
        detailCobertura.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.encamnino_confirmar)
    public void confirmCamera() {
        CameraActivity.modulo = MODULO_ARRIBO;
        if(mResponse.getSiniestro().getIdSiniestro() != null) AppPreferences.setString(Constants.ID_SINIESTRO,mResponse.getSiniestro().getIdSiniestro());
        else AppPreferences.setString(Constants.ID_SINIESTRO,mResponse.getSiniestro().getIdPresiniestro());
        if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_CAMERA_PERMISSION)) {
            Intent intentCamera = new Intent(this, CustomCamera.class);
            startActivityForResult(intentCamera, 204);
        } else {
            RequestPermissions.requestPermission(RequestPermissions.REQUEST_CAMERA_PERMISSION, this);
        }
    }

    @OnClick(R.id.detail_cobertura)
    public void showCobertura() {
        if (recyclerCoberturas.getVisibility() == View.VISIBLE) {
            recyclerCoberturas.setVisibility(View.GONE);
            detailCobertura.setRotation(270);
            detailCobertura.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
        } else {
            detailCobertura.setRotation(0);
            detailCobertura.setBackgroundDrawable(getDrawable(R.drawable.ic_less));
            recyclerCoberturas.setVisibility(View.VISIBLE);
        }
        mLayaultPoliza.setVisibility(View.GONE);
        detailPoliza.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
        detailPoliza.setRotation(270);
        mLayaultReportador.setVisibility(View.GONE);
        detailAsegurado.setBackgroundDrawable(getDrawable(R.drawable.ic_arrow_left_v));
        detailAsegurado.setRotation(270);
    }


    @OnClick(R.id.encamino_call)
    public void makedCall() {
        //openRoute(3000); quitar
        CallToAction();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.encamino_route)
    public void makedRoute() {
        double lat = mResponse.getUbicacion() == null ? mResponse.getSiniestro().getUbicacion().getLatitud() : mResponse.getUbicacion().getLatitud();
        double log = mResponse.getUbicacion() == null ? mResponse.getSiniestro().getUbicacion().getLongitud() : mResponse.getUbicacion().getLongitud();
        if (lat != 0 && log != 0){
            String uri = "http://maps.google.com/maps?q=loc:" + lat + "," + log;
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e){
                new GeneralSnackBar().largeSnackError(this, "Error al abrir Google Maps. Verifique la configuración de su equipo y vuelva a intentarlo.");
            }
        }
    }

    public void displayConfirmBtn() {
        Handler handler = new Handler();
        handler.post(new Runnable() {
            public void run() {
                mbuttonConfirmar.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_arround_actived));
                mbuttonConfirmar.setEnabled(true);
                mbuttonConfirmar.setTextColor(getResources().getColor(R.color.colorWhite));
                FOOTERFocus.setFocusable(true);
                FOOTERFocus.setFocusableInTouchMode(true);
                FOOTERFocus.requestFocus();
                mbuttonConfirmar.animate()
                        .alpha(1.0f)
                        .setDuration(600);
            }
        });
    }

    public void disableConfirmBtn() {
        Handler handler = new Handler();
        handler.post(new Runnable() {
            public void run() {
                mbuttonConfirmar.setBackgroundDrawable(getResources().getDrawable(R.drawable.background_arround_disable));
                mbuttonConfirmar.setEnabled(false);
                mbuttonConfirmar.setTextColor(getResources().getColor(R.color.colorPrimary));
                mbuttonConfirmar.animate()
                        .alpha(1.0f)
                        .setDuration(600);
            }
        });
    }

    public void showSeccion(int seccion) {
        for (int i = 0; i < acordeon.length; i++) {
            if (i == seccion) {
                if (acordeon[i].getVisibility() == View.GONE) {
                    acordeon[i].setVisibility(View.VISIBLE);
                } else {
                    acordeon[i].setVisibility(View.GONE);
                }
            } else {
                acordeon[i].setVisibility(View.GONE);
            }
        }
    }

    /*
    void to start the call
     */
    public void CallToAction() {

        if (TIPO_PHONE_data != null) {
        /*
        call to action
        */

            //hgzavala 21/06/2018 Temporalmente si habilita botón después de la llamada
            Log.i(String.valueOf(this), "Read TIPO_PHONE_data FB = " + TIPO_PHONE_data);

            try {
                Intent intent = new Intent(Intent.ACTION_CALL);
                String mobileNo = TIPO_PHONE_data;
                String uri = "tel:" + mobileNo.trim();
                intent.setData(Uri.parse(uri));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    ActivityCompat.requestPermissions(ConfirmacionActivity.this, new String[]{Manifest.permission.CALL_PHONE}, CALL_REQUEST);
                    return;
                }
                startActivity(intent);
                //toast
                CharSequence text = "LLamando " +
                        TIPO_PHONE_data;
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(ConfirmacionActivity.this, text, duration);
                toast.show();
            } catch (ActivityNotFoundException e) {
                //Toast
                CharSequence text = "Error de llamada " +
                        TIPO_PHONE_data;
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(ConfirmacionActivity.this, text, duration);
                toast.show();
            }
        } else {
            CharSequence text = "No tiene teléfono registrado";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(ConfirmacionActivity.this, text, duration);
            toast.show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, HomeActivity.class); //Home_Activity // FinalizaAtencion_Activity
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "DO IN BACKGROUND :::");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        confirm_activity = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.reconnect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
        Log.i(TAG, "::: googleApiClient.disconnected :::");
    }

    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Google Api Client Connected");
        isMonitoring = true;
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Google Connection Suspended");
    }


    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection Failed:" + connectionResult.getErrorMessage());
    }

    /*

     */

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean checkPermissions(int permission) {
        boolean Resp = false;
        switch (permission) {
            case REQUEST_CAMERA:
                if (checkSelfPermission(
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    Resp = true;
                    Log.d("camera permison", "true");
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.CAMERA)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Ajustador Móvil necesita acceder a la memoria interna para almacenar las fotos. " +
                                "Vaya a ajustes y otorgue los permisos necesarios");
                        builder.setPositiveButton("ACEPTAr", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                            }
                        });
                        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        builder.show();

                    } else {
                        requestPermissions(
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA);

                    }
                } else Resp = true;
                break;
            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    Log.d("write permison", "true");
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.d("write", "false");
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Ajustador Móvil necesita acceder a la memoria interna para almacenar las fotos. " +
                                "Vaya a ajustes y otorgue los permisos necesarios");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
                            }
                        });
                        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        builder.show();

                    } else {
                        requestPermissions(
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_WRITE_EXTERNAL_STORAGE);

                    }
                } else Resp = true;
                break;
            default:
                break;
        }
        return Resp;
    }

    public void initSettings() {
        getAjusteData();
        setmToolbar(mToolbar);
        this.newActivity = ACTIVITY_CONFIRM;
        LinearLayoutCompat[] linear = {mLayaultReportador, mLayaultCar, mLayaultPoliza};
        acordeon = linear;
        mEncamino_siniestro = findViewById(R.id.encamino_siniestro);
        mEncamino_status = findViewById(R.id.encamino_status);
        mEncamino_tipo_siniestro = findViewById(R.id.encamino_tipo_siniestro);
        mEncamino_asignacion = findViewById(R.id.encamino_asignacion);
        mEncamino_conductor = findViewById(R.id.encamino_conductor);
        mEncamino_contratante = findViewById(R.id.encamino_contratante);
        mEncamino_tipo_cliente = findViewById(R.id.encamino_tipo_cliente);
        mEncamino_vehiculo = findViewById(R.id.encamino_vehiculo);
        mEncamino_serie_vehiculo = findViewById(R.id.encamino_serie_vehiculo);

        //loadText();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Log.i(TAG, "reques Code: " + requestCode);
        switch (requestCode) {
            case REQUEST_CAMERA: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ContentValues values = new ContentValues();
                    imageUri = this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

                    File imagesFolder = new File(Environment.getExternalStorageDirectory(), "/Arribo" + PositionForServicioAjuste + "/");
                    imagesFolder.mkdirs();
                    //Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    File image = new File(imagesFolder, "arribo_img.jpg");
                    Uri uriSavedImage = Uri.fromFile(image);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

                    if (intent.resolveActivity(this.getPackageManager()) != null) {
                        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;


            }
            case REQUEST_WRITE_EXTERNAL_STORAGE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissions(REQUEST_CAMERA);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;


            }
        }
    }

    private void getAjusteData() {
        getCompositeDisposable().add(ajusteInteractor.getAjuste(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(object -> {
                    mResponse = object;

                    AppPreferences.setString(AppPreferences.JSON_AJUSTE_DETALLE,
                            Utils.objectoToJsonString(object));

                    // OPEN - Primera validación GeoCerca

                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getBaseActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
                    }

                    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                    Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    double longitude;
                    double latitude;
                    if (location != null) {
                        longitude = location.getLongitude();
                        latitude = location.getLatitude();

                        if (longitude != 0.0 && latitude != 0.0) {
                            validateButton(latitude, longitude);
                        }
                    }

                    handlerSendingPosition = new Handler();
                    handlerSendingPosition.post(runnableSendingPosition);


                    // CLOSE - Primera validación GeoCerca

                    if (mResponse.getUrlDeclaracion() != null) {
                        Gson gson = new Gson();
                        String fotografiasDeclaracion = gson.toJson(mResponse.getFotografiasDeclaracion());

                        if (!AppPreferences.getString(AppPreferences.PRIVATE_DECLARACION).equals(fotografiasDeclaracion)) {
                            AppPreferences.setString(AppPreferences.PRIVATE_DECLARACION, fotografiasDeclaracion);
                        } else {
                            AppPreferences.setString(AppPreferences.PRIVATE_DECLARACION, "");
                        }
                    } else {
                        AppPreferences.setString(AppPreferences.PRIVATE_DECLARACION, "");
                    }

                    if (mResponse.getSiniestro() != null) {
                        mEncamino_siniestro.setText(mResponse.getSiniestro().getId() != null ? mResponse.getSiniestro().getId() : "");
                        mEncamino_tipo_siniestro.setText(mResponse.getSiniestro().getTipoSiniestro() != null ? mResponse.getSiniestro().getTipoSiniestro().getNombre() : "");


                        if (mResponse.getSiniestro().getReporta() != null) {
                            TIPO_PHONE_data = mResponse.getSiniestro().getReporta().getTelefono() != null ? mResponse.getSiniestro().getReporta().getTelefono() : "--";

                            datos_reportador.get(0).setText(
                                    String.format(
                                            "%s%s%s%s%s",
                                            (mResponse.getSiniestro().getReporta().getNombre() != null && !mResponse.getSiniestro().getReporta().getNombre().toLowerCase().contains("null") ?
                                                    mResponse.getSiniestro().getReporta().getNombre() : ""),
                                            " ",
                                            (mResponse.getSiniestro().getReporta().getApellidoPaterno() != null && !mResponse.getSiniestro().getReporta().getApellidoPaterno().toLowerCase().contains("null") ?
                                                    mResponse.getSiniestro().getReporta().getApellidoPaterno() : ""),
                                            " ",
                                            (mResponse.getSiniestro().getReporta().getApellidoMaterno() != null && !mResponse.getSiniestro().getReporta().getApellidoMaterno().toLowerCase().contains("null") ?
                                                    mResponse.getSiniestro().getReporta().getApellidoMaterno() : "")
                                    )
                            );
                            if (mResponse.getSiniestro().getReporta().getTelefono() != null) {
                                datos_reportador.get(1).setText(mResponse.getSiniestro().getReporta().getTelefono() != null ? mResponse.getSiniestro().getReporta().getTelefono() : "");
                            }
                            if (mResponse.getSiniestro().getUbicacion()!=null) {
                                if (mResponse.getSiniestro().getUbicacion().getEstado() != null) {
                                    datos_reportador.get(3).setText(mResponse.getSiniestro().getUbicacion().getEstado().getNombre() != null ? mResponse.getSiniestro().getUbicacion().getEstado().getNombre() : "");

                                }
                                if (mResponse.getSiniestro().getUbicacion().getMunicipio() != null) {
                                    datos_reportador.get(4).setText(mResponse.getSiniestro().getUbicacion().getMunicipio().getNombre() != null ? mResponse.getSiniestro().getUbicacion().getMunicipio().getNombre() : "");
                                }
                                if (mResponse.getSiniestro().getUbicacion().getColonia() != null) {
                                    datos_reportador.get(5).setText(mResponse.getSiniestro().getUbicacion().getColonia().getNombre() != null ? mResponse.getSiniestro().getUbicacion().getColonia().getNombre() : "");
                                }
                                datos_reportador.get(2).setText(mResponse.getSiniestro().getFechaReporte() != null ? DateUtils.datetime(mResponse.getSiniestro().getFechaReporte()) : "");
                                datos_reportador.get(6).setText(
                                        mResponse.getSiniestro().getUbicacion().getVia() != null && !mResponse.getSiniestro().getUbicacion().getVia().contains("null") ?
                                                mResponse.getSiniestro().getUbicacion().getVia() : "Sin calle"
                                );
                                datos_reportador.get(7).setText("ext. " + mResponse.getSiniestro().getUbicacion().getNumeroExterior() != null ? mResponse.getSiniestro().getUbicacion().getNumeroExterior() : "S/N" + " int." + mResponse.getSiniestro().getUbicacion().getNumeroInterior() != null ? mResponse.getSiniestro().getUbicacion().getNumeroInterior() : "S/N");
                                datos_reportador.get(8).setText(mResponse.getSiniestro().getUbicacion().getCodigoPostal() != null ? mResponse.getSiniestro().getUbicacion().getCodigoPostal() : "--");
                                datos_reportador.get(9).setText(mResponse.getSiniestro().getObservaciones() != null ? mResponse.getSiniestro().getObservaciones() : "--");
                            }
                        }

                        if (mResponse.getSiniestro().getPoliza() != null) {
                            datos_poliza.get(0).setText(mResponse.getSiniestro().getPoliza().getNumero() != null ? mResponse.getSiniestro().getPoliza().getNumero() : "--");
                            datos_poliza.get(1).setText(mResponse.getSiniestro().getPoliza().getEstatus() != null ? mResponse.getSiniestro().getPoliza().getEstatus().getNombre() : "--");
                            datos_poliza.get(2).setText(mResponse.getSiniestro().getPoliza().getEstatusCobranza() != null ? mResponse.getSiniestro().getPoliza().getEstatusCobranza().getNombre() : "--");

                        }

                        if (mResponse.getSiniestro().getVehiculo() != null) {
                            mEncamino_vehiculo.setText(mResponse.getSiniestro().getVehiculo().getVersion() != null && mResponse.getSiniestro().getVehiculo().getVersion().getNombre() != null ? mResponse.getSiniestro().getVehiculo().getVersion().getNombre() : "--");

                        }

                        if (mResponse.getSiniestro().getVehiculo() != null && mResponse.getSiniestro().getVehiculo().getVin() != null) {
                            mEncamino_serie_vehiculo.setText(mResponse.getSiniestro().getVehiculo().getVin() != null ? mResponse.getSiniestro().getVehiculo().getVin() : "--");
                        }

                        if (mResponse.getUrlDeclaracion() != null) {
                            Gson gson = new Gson();
                            String fototgrafiasDeclaracion = gson.toJson(mResponse.getFotografiasDeclaracion());
                            if (!Objects.equals(AppPreferences.getString(AppPreferences.PRIVATE_DECLARACION), fototgrafiasDeclaracion)) {
                                AppPreferences.setString(AppPreferences.PRIVATE_DECLARACION, fototgrafiasDeclaracion);
                            }
                        }
                        if (mResponse.getSiniestro().getPoliza() != null) {
                            //Tipo de cliente
                            if (mResponse.getSiniestro().getPoliza().getSegmentoCliente() != null) {
                                mEncamino_tipo_cliente.setText(mResponse.getSiniestro().getPoliza().getSegmentoCliente() != null ? mResponse.getSiniestro().getPoliza().getSegmentoCliente().getNombre() : "--");
                            }

                            if (mResponse.getSiniestro().getPoliza().getContratante() != null) {
                                if (mResponse.getSiniestro().getPoliza().getContratante() != null) {
                                    if (mResponse.getSiniestro().getPoliza().getContratante().getNombreCompleto() != null) {
                                        mEncamino_conductor.setText(mResponse.getSiniestro().getPoliza().getContratante().getNombreCompleto());
                                    } else {
                                        mEncamino_conductor.setText("--");
                                    }
                                }

                                if (mResponse.getSiniestro().getPoliza().getTitular() != null) {
                                    if (mResponse.getSiniestro().getPoliza().getTitular().getNombreCompleto() != null) {
                                        mEncamino_contratante.setText(mResponse.getSiniestro().getPoliza().getTitular().getNombreCompleto());
                                    } else {
                                        mEncamino_contratante.setText("--");
                                    }
                                }
                            }
                            if (mResponse.getSiniestro().getPoliza().getCoberturas() != null || mResponse.getSiniestro().getPoliza().getCoberturas().size() != 0) {
                                adapter = new PolizaCoberturaAdapter();
                                recyclerCoberturas.setHasFixedSize(true);
                                recyclerCoberturas.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                l = mResponse.getSiniestro().getPoliza().getCoberturas();
                                adapter.addAll(l);
                                RecyclerView.ItemDecoration dividerItemDecoratio2 = new ItemDecorator(ContextCompat.getDrawable(ConfirmacionActivity.this, R.drawable.line_divider));
                                recyclerCoberturas.addItemDecoration(dividerItemDecoratio2);
                                recyclerCoberturas.setAdapter(adapter);
                            }
                        }
                    }
                    mEncamino_status.setText(mResponse.getEstatus() != null ? mResponse.getEstatus().name() : "--");
                    /*OPEN - Modified by hgzavala on 4/APR/2018 - APP172*/
                    mEncamino_asignacion.setText(mResponse.getFechaAsignacion() != null ? DateUtils.datetime(mResponse.getFechaAsignacion()) : "--");
                    /*CLOSE - Modified by hgzavala on 4/APR/2018 - APP172*/



                }, throwable -> {
                    Log.e(TAG, "Error al obtener los datos del ajuste", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 204) {
                String img_path = data.getStringExtra(Constants.KEY.IMG_PATH);
                if (!img_path.equals("")) {
                    mostrarDialogoEspera("Subiendo imágen...");
                    File file = new File(img_path);
                    MaterialDialog dialogoArribo = new MaterialDialog.Builder(this)
                            .title("Arribando")
                            .content("Espere por favor...")
                            .progress(true, 0)
                            .cancelable(false)
                            .widgetColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                            .show();
                    Action last = () -> {
                        Log.d(TAG, "Confirm_Activity arribar onResponse body = null");
                        String mData = new Gson().toJson(mResponse);
                        Log.d(TAG, "onActivityResult: " + mData);
                        Intent intent = new Intent(this, AjusteActivity.class);
                        intent.putExtra("intentCodSiniestro", intentCodSiniestro);
                        intent.putExtra("idAjuste", intentAjuste);
                        intent.putExtra("intentPoliza", mEncamino_tipo_cliente.getText().toString());
                        intent.putExtra("data", mData);
                        intent.putExtra("fechaHoraArribo", Calendar.getInstance().getTime());
                        dialogoArribo.dismiss();
                        startActivity(intent);
                        finish();
                    };
                    Action next = () -> {
                        getCompositeDisposable().add(ajusteInteractor.getAjuste(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                                .subscribe(ajuste -> {
                                    AppPreferences.setString(AppPreferences.JSON_AJUSTE_DETALLE, Utils.objectoToJsonString(ajuste));
                                    last.run();
                                }, throwable -> {
                                    Log.e(TAG, "Error al obtener el ajuste: " , throwable);
                                    last.run();
                                }));
                    };
                    getCompositeDisposable().add(ajusteInteractor.arribar(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), file).subscribe(
                        motivoCancelServicio -> next.run(), throwable -> {
                            if(throwable instanceof AjusteArriboException) {
                                DialogUtils.alert(this,
                                        "Arribo Offline",
                                        "Debido a la falta de conectividad a internet, debe hablar a cabina para solicitar el arribo del siniestro.",
                                        "Aceptar",
                                        (d, e) -> {
                                            try {
                                                next.run();
                                            } catch (Exception ex) {
                                                d.dismiss();
                                                new GeneralSnackBar().largeSnackError(this, throwable);
                                            }
                                        });
                            } else {
                                dialogoArribo.dismiss();
                                Log.e(TAG, "Error al arribar ajuste", throwable);
                                new GeneralSnackBar().largeSnackError(this, throwable);
                            }
                        }));
                }
            }
        }
    }



    public void updatedMenu() {
        MenuItem menuItem = menu.findItem(R.id.menu_disponible);
        MenuItem menuItemReceso = menu.findItem(R.id.menu_receso);
        MenuItem menuItemCerrar = menu.findItem(R.id.menu_cerrar);


        if (ajustador != null && ajustador.getEstatus() != null) {

            switch (ajustador.getEstatus()) {
                case NO_DISPONIBLE:
                case EN_RECESO:
                case RETIRADO:
                    menuItem.setTitle(Constants.AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(true);
                    break;
                case DISPONIBLE:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(true);
                    menuItemCerrar.setEnabled(false);
                    break;
                case POR_SALIR:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(true);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    break;
                case EN_CAMINO:
                case ATENDIENDO:
                case POR_CONFIRMAR:
                case POR_TERMINAR:
                case EN_ESPERA_SERVICIO_COBERTURA:
                    menuItem.setTitle(Constants.NOT_AVAILABLE);
                    menuItem.setEnabled(false);
                    menuItemReceso.setEnabled(false);
                    menuItemCerrar.setEnabled(false);
                    break;
            }
        }
    }

    public void validateButton(double latitud, double longitud) {
        Location location = new Location("location");
        Location destinyLocation = new Location("destiny");
        if (latitud != 0.0 && longitud != 0.0) {
            location.setLatitude(latitud);
            location.setLongitude(longitud);
        }
        if (mResponse != null) {
            if (mResponse.getUbicacion() != null) {
                destinyLocation.setLatitude(mResponse.getUbicacion().getLatitud());
                destinyLocation.setLongitude(mResponse.getUbicacion().getLongitud());
                distancia = location.distanceTo(destinyLocation);
                calculateDistance(distancia);

            } else if (mResponse.getSiniestro() != null) {
                if (mResponse.getSiniestro().getUbicacion() != null) {
                    destinyLocation.setLatitude(mResponse.getSiniestro().getUbicacion().getLatitud());
                    destinyLocation.setLongitude(mResponse.getSiniestro().getUbicacion().getLongitud());
                    distancia = location.distanceTo(destinyLocation);
                    calculateDistance(distancia);
                } else {
                    new GeneralSnackBar().largeSnackError(ConfirmacionActivity.this, "no se pudo actualizar la ubicación");
                }
            }
        }
    }

    @SuppressLint("DefaultLocale")
    public void calculateDistance(float distance) {
        String d = String.format("%,.2f m", distance);
        Log.d(TAG, ":::::metros: " + d);
        Double geocerca;
        if (AppPreferences.getString(AppPreferences.GEOCERCA)!=null&&!AppPreferences.getString(AppPreferences.GEOCERCA).equals("")){
            geocerca = Double.parseDouble(AppPreferences.getString(AppPreferences.GEOCERCA));
        }
        else {
            geocerca = new Double(BuildConfig.GEOCERCA);
        }
        if (distance <= geocerca) {

            if (!proximityDialogShown) {

                Intent resultIntent = new Intent(getApplicationContext(), HomeActivity.class);

                final PendingIntent resultPendingIntent =
                        getActivity(
                                getApplicationContext(),
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                assert mNotifyMgr != null;



                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                    NotificationChannel channel = new NotificationChannel("aproxima", "aproxima", NotificationManager.IMPORTANCE_HIGH);
                    AudioAttributes attributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                            .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED)
                            .build();
                    channel.setSound(Uri.parse("android.resource://com.ajustadormovil.gnp.android/" + R.raw.aproxima),attributes);
                    channel.enableLights(true);
                    channel.enableVibration(true);
                    channel.setShowBadge(true);
                    channel.setLightColor(Color.GREEN);
                    channel.setBypassDnd(true);
                    channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                    mNotifyMgr.createNotificationChannel(channel);


                }

                Notification notification = new NotificationCompat.Builder(getApplicationContext(),"aproxima")
                        .setAutoCancel(true)
                        .setContentIntent(resultPendingIntent)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText("Se aproxima al siniestro")
                        .setSmallIcon(R.drawable.ic_notificacion_ic, 0)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ajustador_icon))
                        .setColor(Color.TRANSPARENT)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setVibrate(new long[]{0, 2000, 1000, 0, 1000, 2000})
                        .setLights(Color.RED, 3000, 3000)
                        .setSound(Uri.parse("android.resource://com.ajustadormovil.gnp.android/" + R.raw.aproxima))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText("Se aproxima al siniestro"))
                        .build();

                mNotifyMgr.notify(getTaskId(), notification);



                proximityDialogShown = true;

            }


            displayConfirmBtn();

        } else {
            disableConfirmBtn();
        }
    }

    @OnLongClick(R.id.header_version)
    public boolean muestraVersion() {

        PackageInfo pInfo;
        String version = "";

        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "No se encontro el nombre del paquete de versión", e);
        }

        Toast.makeText(this, "Número de compilación:\n"
                + version + "-"
                + BuildConfig.ENVIRONMENT + "-"
                + BuildConfig.COMMIT
                + "-" + BuildConfig.LOCAL_CHANGES, Toast.LENGTH_LONG).show();
        return true;
    }

    public static abstract class LocationResult {
        public abstract void gotLocation(Location location);
    }

    class GetLastLocation extends TimerTask {
        @SuppressLint("MissingPermission")
        @Override

        public void run() {

            Location net_loc = null, gps_loc = null;
            if (gps_enabled)
            gps_loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (network_enabled)
            net_loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (gps_loc != null && net_loc != null) {
                if (gps_loc.getTime() > net_loc.getTime())
                    locationResult.gotLocation(gps_loc);
                else
                    locationResult.gotLocation(net_loc);
                return;
            }

            if (gps_loc != null) {
                locationResult.gotLocation(gps_loc);
                return;
            }
            if (net_loc != null) {
                locationResult.gotLocation(net_loc);
                return;
            }
            locationResult.gotLocation(null);
        }
    }

    @Override
    protected void onDisconnected() {
        super.onDisconnected();
        mStatus.setText("SIN CONEXIÓN");
    }

    @Override
    protected void onConnected() {
        super.onConnected();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child("ajustadores")
                .child(AjustadorMovilApplication.getCedula())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        updateAjustadorFirebase(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        updateAjustadorLocal();
                    }
                });
    }

    private void updateAjustadorFirebase(DataSnapshot childSnapshot){
        Ajustador data = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        if (childSnapshot.exists() && mName != null && data != null) {
            mName.setText(String.format("%s %s %s", childSnapshot.child("nombres").exists() ? childSnapshot.child("nombres").getValue(String.class) : data.getNombre()
                    , childSnapshot.child("aPaterno").exists() ? childSnapshot.child("aPaterno").getValue(String.class) : data.getApellidoPaterno()
                    , childSnapshot.child("aMaterno").exists() ? childSnapshot.child("aMaterno").getValue(String.class) : data.getApellidoMaterno()));
            mPhone.setText(data.getCedula() + " - " + data.getTelefono());
            estOperativo_User = childSnapshot.child("estOperativo").exists() ? childSnapshot.child("estOperativo").getValue(String.class) : "NO DISPONIBLE";
            if (!ConnectionProvider.getInstance().isConnected()){
              mStatus.setText("SIN CONEXIÓN");
            }else {
                mStatus.setText(estOperativo_User);
            }
        } else {
            updateAjustadorLocal();
        }
    }

    private void updateAjustadorLocal(){
        Ajustador ajustador = new Gson().fromJson(AppPreferences.getString(AppPreferences.DATA_AJUSTADOR), Ajustador.class);
        if(ajustador != null  && mName != null) {
            mName.setText(String.format("%s %s %s", ajustador.getNombre(), ajustador.getApellidoPaterno(), ajustador.getApellidoMaterno()));
            mPhone.setText(ajustador.getCedula() + " - " + ajustador.getTelefono());
        }
    }

}