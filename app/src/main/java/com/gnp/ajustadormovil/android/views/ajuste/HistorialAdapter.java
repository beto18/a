package com.gnp.ajustadormovil.android.views.ajuste;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lima on 10/10/17.
 * Modified by hgzavala on 11042018
 */

public class HistorialAdapter extends RecyclerView.Adapter<HistorialAdapter.DataViewHolder> {

    private List<Ajuste> list;

    public HistorialAdapter() {
        list = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            if (list.get(position).getSiniestro().getVehiculo()!= null
                    && list.get(position).getSiniestro().getVehiculo().getVersion() != null
                    && list.get(position).getSiniestro().getVehiculo().getVersion().getNombre()!=null ) {
                holder.mVehiculoDescripcion.setText(list.get(position).getSiniestro().getVehiculo().getVersion().getNombre());
            } else {
                holder.mVehiculoDescripcion.setText("--");

            }

            if (list.get(position).getSiniestro().getId() != null) {
                holder.mIDhistorial.setText(list.get(position).getSiniestro().getId());
            } else {
                holder.mIDhistorial.setText("--");

            }

            if (list.get(position).getEstatus() != null) {
                holder.mTitle.setText(list.get(position).getEstatus().name());
            } else {
                holder.mTitle.setText("--");

            }

            if (list.get(position).getFechaAsignacion() != null) {
                holder.mAsignacion.setText(DateUtils.datetime(list.get(position).getFechaAsignacion()));

            } else {
                holder.mAsignacion.setText("--");
            }

            if (list.get(position).getFechaConfirmacion() != null) {
                holder.mConfirmacion.setText(DateUtils.datetime(list.get(position).getFechaConfirmacion()));
            } else {
                holder.mConfirmacion.setText("--");
            }

            if (list.get(position).getFechaArribo() != null) {
                holder.mArribo.setText(DateUtils.datetime(list.get(position).getFechaArribo()));
            } else {
                holder.mArribo.setText("--");
            }

            switch (list.get(position).getEstatus()) {
                case CANCELADO:
                    holder.mColorStatus.setBackgroundResource(R.color.colorRedStatus);
                    if (list.get(position).getFechaCancelado() != null) {
                        holder.mCierre.setText(DateUtils.datetime(list.get(position).getFechaCancelado()));

                    } else {
                        holder.mCierre.setText("--");
                    }
                    break;
                case TERMINADO:
                    holder.mColorStatus.setBackgroundResource(R.color.colorGreen);
                    if (list.get(position).getFechaTerminado() != null) {
                        holder.mCierre.setText(DateUtils.datetime(list.get(position).getFechaTerminado()));

                    } else {
                        holder.mCierre.setText("--");
                    }
                    break;
            }
        } catch (Exception e) {
            Log.e(this.toString(), "\n\t\tError" + e.getMessage());
        }
    }

    public List<Ajuste> getAjustesHistorial() {
        return list;
    }

    public void addAll(List<Ajuste> results) {
        for (Ajuste result : results) {
            add(result);
        }
    }

    public void add(Ajuste r) {
        list.add(r);
        notifyItemInserted(list.size() - 1);
    }

    public void clear() {
        final int size = list.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                list.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_history_item, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return list.indexOf(getItem(position));
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.history_title)
        AppCompatTextView mTitle;
        @BindView(R.id.vehiculoDescripcion)
        AppCompatTextView mVehiculoDescripcion;
        @BindView(R.id.history_asignacion)
        AppCompatTextView mAsignacion;
        @BindView(R.id.history_confirmacion)
        AppCompatTextView mConfirmacion;
        @BindView(R.id.history_arribo)
        AppCompatTextView mArribo;
        @BindView(R.id.history_cierre)
        AppCompatTextView mCierre;
        @BindView(R.id.history_color)
        LinearLayoutCompat mColorStatus;
        @BindView(R.id.id_sa_historial)
        AppCompatTextView mIDhistorial;

        DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}