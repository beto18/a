package com.gnp.ajustadormovil.android.views.ajuste;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Lima on 06/10/17.
 * Modified by hgzavala on 10/04/2018
 */

public class HistorialFragment extends BaseFragment {

    private static final String TAG = SiniestrosFragment.class.getSimpleName();
    @BindView(R.id.history_recycler)
    RecyclerView recyclerhistory;
    @BindView(R.id.fecha_historia)
    AppCompatTextView fecha_historia;
    @BindView(R.id.right_fecha)
    AppCompatImageButton right_fecha;
    @BindView(R.id.left_fecha)
    AppCompatImageButton left_fecha;
    @BindView(R.id.historial_container)
    LinearLayoutCompat mLayout;
    int contador = 30;
    private long yearLimit = 31556952000L;
    Calendar calendar = Calendar.getInstance(Locale.forLanguageTag("es-MX"));
    int month = calendar.get(Calendar.MONTH);
    int year = calendar.get(Calendar.YEAR);
    GregorianCalendar gc = new GregorianCalendar();
    Calendar calendar1 = Calendar.getInstance();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Unbinder mUnbinder;
    private RecyclerView.LayoutManager lManagerhistory;
    private HistorialAdapter adapterhistory;
    private AjusteInteractor ajusteInteractor;

    public static HistorialFragment newInstance() {
        HistorialFragment fragment = new HistorialFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_siniestro_history, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();

        calendar1.add(Calendar.DAY_OF_MONTH, 0);
        fecha_historia.setText(dateFormat.format(calendar1.getTime()));

        if (ConnectionProvider.getInstance().isConnected()) {
            getHistorial(dateFormat.format(calendar1.getTime()));
        }

        left_fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (contador == 0) {
                    new GeneralSnackBar().largeSnackError(getActivity(), "No es posible visualizar siniestros de más de 30 días a la fecha actual");
                } else {
                    calendar1.add(Calendar.DAY_OF_MONTH, -1);
                    contador--;
                    fecha_historia.setText(dateFormat.format(calendar1.getTime()));
                    getHistorial(dateFormat.format(calendar1.getTime()));
                }
            }
        });

        right_fecha.setOnClickListener(v -> {
            calendar1.add(Calendar.DAY_OF_MONTH, 1);
            if (calendar1.compareTo(Calendar.getInstance()) < 0) {
                calendar1.add(Calendar.DAY_OF_MONTH, -1);
                if (contador < 30) {
                    contador++;
                }
                calendar1.add(Calendar.DAY_OF_MONTH, 1);
                fecha_historia.setText(dateFormat.format(calendar1.getTime()));
                getHistorial(dateFormat.format(calendar1.getTime()));
            } else {
                calendar1.add(Calendar.DAY_OF_MONTH, -1);
            }
        });

        ServicioAjuste_Screen();
        return view;
    }

    @OnClick(R.id.dateHistory)
    @SuppressLint("NewApi")
    public void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            if (date!=null && !date.equals("")) {
                fecha_historia.setText(DateUtils.dateToDateIso(date));
                getHistorial(DateUtils.dateToDateIso(date));
            }
        });
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "dataPickerBirthday");
    }

    private void getHistorial(String fecha) {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(ajusteInteractor.getHistorial(fecha)
                .subscribe(historial -> {
                            if (adapterhistory.getAjustesHistorial().size() == 0) {
                                for (int i = 0; i < historial.size(); i++) {
                                    adapterhistory.add(historial.get(i));
                                }
                            } else {
                                adapterhistory.clear();
                                for (int i = 0; i < historial.size(); i++) {
                                    adapterhistory.add(historial.get(i));
                                }
                            }
                        }
                        , throwable -> {
                            Log.e(TAG, "Error al cargar historial", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }));
    }

    public void ServicioAjuste_Screen() {

        this.adapterhistory = new HistorialAdapter();
        this.lManagerhistory = new LinearLayoutManager(getActivity());

        this.recyclerhistory.setHasFixedSize(false);
        this.recyclerhistory.setLayoutManager(this.lManagerhistory);
        this.recyclerhistory.setAdapter(this.adapterhistory);

        this.adapterhistory.notifyDataSetChanged();
    }

    @Override
    public void onConnected() {
        super.onConnected();
        getHistorial(dateFormat.format(calendar1.getTime()));
    }
}