package com.gnp.ajustadormovil.android.views.ajuste;

import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.model.StatusChanged;
import com.gnp.ajustadormovil.android.models.CardList;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lima on 19/10/17.
 */

public class ModulosAdapter extends RecyclerView.Adapter<ModulosAdapter.DataViewHolder> {
    private ArrayList<CardList> list;
    private List<String> listCards;
    private StatusChanged statusChanged;
    private OnItemRemovedListener mClickListener;

    public ModulosAdapter(ArrayList<CardList> list) {
        this.list = list;

    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_capture_card, parent, false);
        return new DataViewHolder(v, mClickListener);
    }


    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {


        holder.icoSm1_guardado.setOnClickListener(v -> {
            if (list.get(position).getIcoSm1_status() == R.drawable.ic_error) {
                ToastUtils.toastLongError(list.get(position).getMsgSm1());
            }
        });

        holder.icoSm2_guardado.setOnClickListener(v -> {
            if (list.get(position).getIcoSm2_status() == R.drawable.ic_error) {
                ToastUtils.toastLongError(list.get(position).getMsgSm2());
            }
        });
        holder.icoSm3_guardado.setOnClickListener(v -> {
            if (list.get(position).getIcoSm3_status() == R.drawable.ic_error) {
                ToastUtils.toastLongError(list.get(position).getMsgSm3());
            }
        });


        holder.ico_tipo_tarjeta.setImageResource(list.get(position).getImageCard());
        holder.card_title.setText(list.get(position).getTittleCard());

        if (" ".equals(list.get(position).getSubModule1())) {
            holder.txt_sm1.setVisibility(View.INVISIBLE);

        } else {
            holder.txt_sm1.setText(list.get(position).getSubModule1());
        }
        holder.icoSm1_guardado.setImageResource(list.get(position).getIcoSm1_status());
        holder.icoSm1_foto.setImageResource(list.get(position).getIcoSm1_foto());
        holder.icoSm1_volante.setImageResource(list.get(position).getIcoSm1_volante());


        if (" ".equals(list.get(position).getSubModule2())) {
            holder.txt_sm2.setVisibility(View.INVISIBLE);
        } else {
            holder.txt_sm2.setText(list.get(position).getSubModule2());
        }
        holder.icoSm2_guardado.setImageResource(list.get(position).getIcoSm2_status());
        holder.icoSm2_foto.setImageResource(list.get(position).getIcoSm2_foto());
        holder.icoSm2_volante.setImageResource(list.get(position).getIcoSm2_volante());

        if (" ".equals(list.get(position).getSubModule3())) {
            holder.txt_sm3.setVisibility(View.INVISIBLE);

        } else {
            holder.txt_sm3.setText(list.get(position).getSubModule3());
        }
        holder.icoSm3_guardado.setImageResource(list.get(position).getIcoSm3_status());
        holder.icoSm3_foto.setImageResource(list.get(position).getIcoSm3_foto());
        holder.icoSm3_volante.setImageResource(list.get(position).getIcoSm3_volante());

        if (" ".equals(list.get(position).getSubModule4())) {
            holder.txt_sm4.setVisibility(View.INVISIBLE);

        } else {
            holder.txt_sm4.setText(list.get(position).getSubModule4());
        }
        holder.icoSm4_guardado.setImageResource(list.get(position).getIcoSm4_status());
        holder.icoSm4_foto.setImageResource(list.get(position).getIcoSm4_foto());
        holder.icoSm4_volante.setImageResource(list.get(position).getIcoSm4_volante());


        // }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setItems(ArrayList<CardList> list) {
        this.list = list;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public OnItemRemovedListener getClickListener() {
        return mClickListener;
    }

    public void setClickListener(OnItemRemovedListener clickListener) {
        mClickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setStatus(StatusChanged status) {
        this.statusChanged = status;
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.ico_tipo_tarjeta)
        AppCompatImageView ico_tipo_tarjeta;
        @Nullable
        @BindView(R.id.card_title)
        AppCompatTextView card_title;
        @Nullable
        @BindView(R.id.txt_sm1)
        AppCompatTextView txt_sm1;
        @Nullable
        @BindView(R.id.icoSm1_guardado)
        AppCompatImageView icoSm1_guardado;
        @Nullable
        @BindView(R.id.icoSm1_foto)
        AppCompatImageView icoSm1_foto;
        @Nullable
        @BindView(R.id.icoSm1_volante)
        AppCompatImageView icoSm1_volante;
        @Nullable
        @BindView(R.id.txt_sm2)
        AppCompatTextView txt_sm2;
        @Nullable
        @BindView(R.id.icoSm2_guardado)
        AppCompatImageView icoSm2_guardado;
        @Nullable
        @BindView(R.id.icoSm2_foto)
        AppCompatImageView icoSm2_foto;
        @Nullable
        @BindView(R.id.icoSm2_volante)
        AppCompatImageView icoSm2_volante;
        @Nullable
        @BindView(R.id.txt_sm3)
        AppCompatTextView txt_sm3;
        @Nullable
        @BindView(R.id.icoSm3_guardado)
        AppCompatImageView icoSm3_guardado;
        @Nullable
        @BindView(R.id.icoSm3_foto)
        AppCompatImageView icoSm3_foto;
        @Nullable
        @BindView(R.id.icoSm3_volante)
        AppCompatImageView icoSm3_volante;
        @Nullable
        @BindView(R.id.txt_sm4)
        AppCompatTextView txt_sm4;
        @Nullable
        @BindView(R.id.icoSm4_guardado)
        AppCompatImageView icoSm4_guardado;
        @Nullable
        @BindView(R.id.icoSm4_foto)
        AppCompatImageView icoSm4_foto;
        @Nullable
        @BindView(R.id.icoSm4_volante)
        AppCompatImageView icoSm4_volante;


        DataViewHolder(View itemView, OnItemRemovedListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemClickListener.changeStatus(itemView, getAdapterPosition());
            itemView.setOnClickListener(view -> {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(itemView, getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(view -> {
                itemClickListener.onLongItemClick(itemView, getAdapterPosition());
                return true;
            });
        }


    }
}
