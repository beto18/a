package com.gnp.ajustadormovil.android.views.ajuste;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ServiciosCoberturaInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.SyncInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.CardList;
import com.gnp.ajustadormovil.android.models.ajuste.AjusteOperacion;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparte;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.EstatusResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.InfoEstatus;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.views.amis.ContraparteAdapater;
import com.gnp.ajustadormovil.android.views.asegurado.AseguradoContentActivity;
import com.gnp.ajustadormovil.android.views.commons.events.ItemClickListener;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.ajuste.declaracion.DeclaracionActivity;
import com.gnp.ajustadormovil.android.views.amis.DialogoAmisFragment;
import com.gnp.ajustadormovil.android.views.asegurado.AseguradoActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.recuperaciones.TercerosRecuperacionActivity;
import com.gnp.ajustadormovil.android.views.responsabilidad.ResponsabilidadActivity;
import com.gnp.ajustadormovil.android.views.servicios.ServiciosActivity;
import com.gnp.ajustadormovil.android.views.terceros.TercerosActivity;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.volantes.VolanteActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.sf.oval.constraint.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.zxing.integration.android.IntentIntegrator.REQUEST_CODE;


/**
 * Contiene la lógica de los cards (Asegurado, Terceros, Declaración, Responsabilidad, Recuperaciones y Servcios)
 * <p>
 * Created by Lima on 19/10/17
 * <p>
 * Modified by hgzavala 27/04/2018
 *
 * @author lima
 * @version 27/04/2018
 */

public class ModulosFragment extends BaseFragment implements OnItemRemovedListener, DialogInterface.OnClickListener, ItemClickListener {

    private static final String TAG = ModulosFragment.class.getSimpleName();
    public static int segundos = 0;
    public static int minutos = 0;
    public static int horas = 0;

    static long StartTimeHere = 0L;
    static Handler handler = new Handler();
    static Handler handlerHere;
    @BindView(R.id.seguimiento_recycler)
    RecyclerView recycler;
    @BindView(R.id.capture_terminar_siniestro)
    Button btnTerminarSiniestro;
    @BindView(R.id.ID_COUNTER)
    AppCompatTextView mCounter;
    private CancelacionAdapter cancelacionesAdapter;
    public Runnable runnableHere = new Runnable() {
        public void run() {
            if (ajuste != null && ajuste.getFechaArribo() != null) {
                long diff = (System.currentTimeMillis() - ajuste.getFechaArribo().getTime()) / 1000;
                horas = (int) diff / (60 * 60);
                diff %= 60 * 60;
                minutos = (int) diff / 60;
                segundos = (int) diff % 60;
                if (mCounter != null) {
                    mCounter.setText(String.format("%02d:%02d:%02d", horas, minutos, segundos));
                }
                handlerHere.postDelayed(this, 1000);
            }
        }
    };
    @BindView(R.id.ID_SINIESTRO)
    AppCompatTextView mSiniestro;
    @BindView(R.id.tvStatus)
    TextView status;
    @BindView(R.id.txtFolioeDUA)
    TextView txtFolioeDUA;
    @BindView(R.id.titleFolio)
    TextView txtTitleFolioEdua;
    @BindView(R.id.btnleerEscribirQR)
    Button btnleerEscribirQR;
    @BindView(R.id.contentFolioEdua)
    LinearLayoutCompat contentFolioEdua;
    @BindView(R.id.rvContrapartes)
    RecyclerView rvContraparte;
    @BindView(R.id.openContraparte)
    ImageView openContraparte;


    String idAjuste;
    ArrayList<CardList> cardLists = new ArrayList<>();
    private Unbinder mUnbinder;
    private RecyclerView.LayoutManager lManager;
    private ModulosAdapter adapter;
    private Intent intent;
    private List<String> cardsList = new ArrayList<>();
    private String card = "", folioeDUA = "";
    private boolean isActive;
    private int selected;
    private TerceroAutoInteractor terceroAutoService;
    private AseguradoInteractor aseguradoInteractor;
    private TerceroNoAutoInteractor tercerosNoAutoService;
    private TerceroPersonaInteractor tercerosPersonaService;
    private ResponsabilidadInteractor responsabilidadesServices;
    private ServiciosCoberturaInteractor serviciosCoberturaInteractor;
    private VolanteInteractor volanteService;
    private RecuperacionInteractor recuperacionInteractor;
    private AjusteInteractor ajusteInteractor;
    private SiniestrosInteractor siniestrosInteractor;
    private InfoEstatus infoEstatus;
    private Ajuste ajuste;
    private CompositeDisposable compositeDisposable;
    private AmisInteractor amisInteractor;
    private GeneralSnackBar snackBar;
    private Activity activity;
    private Bundle args;
    private ContraparteAdapater contraparteAdapter;
    private LinearLayoutManager linearLayoutManager;


    private BroadcastReceiver presiniestroListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.PAYLOAD_EVOLUTION.equals(intent.getAction())) {
                String id = intent.getExtras().getString("ajuste");
                String dataResponse = getArguments().getString("data");
                Gson gson = new Gson();
                Ajuste data = gson.fromJson(dataResponse, Ajuste.class);
                if (id.equals(data.getId())) {
                    getCompositeDisposable().add(ajusteInteractor.getAjuste(id)
                            .subscribe(ajusteL -> {
                                ajuste = ajusteL;
                                initView();
                            }, throwable -> {
                                Log.e(TAG, "Error al obtener el ajuste", throwable);
                            }));
                }
            }
        }

    };

    private BroadcastReceiver refoleoListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.ON_REFOLEO.equals(intent.getAction())) {
                String id = intent.getExtras().getString("idAjuste");
                String dataResponse = getArguments().getString("data");
                Gson gson = new Gson();
                Ajuste data = gson.fromJson(dataResponse, Ajuste.class);
                if (id.equals(data.getId())) {
                    generarFolioeDUA();
                }
            }
        }
    };


    public static ModulosFragment newInstance() {
        ModulosFragment fragment = new ModulosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializecardsviews();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        tercerosNoAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
        tercerosPersonaService = OrquestadorInteractorFactory.getTerceroPersonaService();
        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor();
        serviciosCoberturaInteractor = OrquestadorInteractorFactory.getServiciosCoberturaInteractor();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
        responsabilidadesServices = OrquestadorInteractorFactory.getResponsabilidadesService();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        compositeDisposable = getCompositeDisposable();
        amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_capture_child, container, false);
        snackBar = new GeneralSnackBar();
        mUnbinder = ButterKnife.bind(this, view);
        lManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        recycler.setLayoutManager(lManager);
        loadAdapter();
        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        rvContraparte.setLayoutManager(linearLayoutManager);
        compositeDisposable.add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(ajuste -> {
                    this.ajuste = ajuste;
                    initView();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el ajsute", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        snackBar.largeSnackError(activity, throwable);
                    }
                }));

        return view;
    }

    private void initView() {
        if (ajuste.getSiniestro().getIdSiniestro() != null)
            AppPreferences.setString(Constants.ID_SINIESTRO, ajuste.getSiniestro().getIdSiniestro());
        else
            AppPreferences.setString(Constants.ID_SINIESTRO, ajuste.getSiniestro().getIdPresiniestro());
        idAjuste = ajuste.getId();

        Log.d("ID_AJUSTE: ", idAjuste);
        if (isInflate(btnTerminarSiniestro)) {
            btnTerminarSiniestro.setEnabled(false);
            deshabilitaBotonConfirmacion();
        }

        if (isInflate(mSiniestro)) {
            mSiniestro.setText(ajuste.getSiniestro().getId() != null ? ajuste.getSiniestro().getId() : "Sin numero");
        }

        if (ajuste.getSiniestro().isPresiniestro() || Constants.turnOnAmis) {
            setVisibility(View.GONE);
        } else {
            setVisibility(View.VISIBLE);
            generarFolioeDUA();
        }
        if (isInflate(status)) {
            status.setText(String.format("%s%s", "Status: ", this.ajuste.getEstatus()));
        }
        handlerHere = new Handler();
        StartTimeHere = SystemClock.uptimeMillis();
        handlerHere.postDelayed(runnableHere, 0);

        iniciaContador();
        updateEstatus();
        activity.registerReceiver(presiniestroListener, new IntentFilter(Constants.PAYLOAD_EVOLUTION));
        activity.registerReceiver(refoleoListener, new IntentFilter(Constants.ON_REFOLEO));
    }

    private void setVisibility(int isVisible) {
        if (isInflate(contentFolioEdua)) {
            contentFolioEdua.setVisibility(isVisible);
        }
        if (isInflate(btnleerEscribirQR)) {
            btnleerEscribirQR.setVisibility(isVisible);
        }
    }

    /**
     * Inicializa el contador con el tiempo transcurrido después del arribo
     *
     * @author hgzavala
     * @version 17042018
     */
    private void iniciaContador() {
        ajuste = new Gson().fromJson(AppPreferences.getString(AppPreferences.JSON_AJUSTE_DETALLE), Ajuste.class);
        compositeDisposable.add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(a -> {
                    ajuste = a;
                    AppPreferences.setString(AppPreferences.JSON_AJUSTE_DETALLE, new Gson().toJson(ajuste));
                    if (ajuste.getFechaArribo() != null) {
                        long milisegundosDesdeArribo = System.currentTimeMillis() - ajuste.getFechaArribo().getTime();
                        int modH = (int) milisegundosDesdeArribo % 3600000;
                        horas = (int) milisegundosDesdeArribo / 3600000;
                        int modM = modH % 60000;
                        minutos = modH / 60000;
                        int modS = modM % 1000;
                        segundos = modM / 1000;
                    } else {
                        horas = minutos = segundos = 0;
                    }

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el ajuste", throwable);
                }));

    }

    HashSet<AjusteOperacion> ao;


    /**
     * Click boton
     * funcionadlidad para compartir y leer informacion de Amis
     */
    @OnClick(R.id.btnleerEscribirQR)
    public void captureOptions() {
        args = new Bundle();
        args.putString("ajuste", new Gson().toJson(this.ajuste));
        args.putString("folioEdua", folioeDUA);
        FragmentTransaction fragmentTransaction = ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((AppCompatActivity) activity).getSupportFragmentManager().findFragmentByTag(DialogoAmisFragment.class.getName());
        if (prev != null) {
            fragmentTransaction.remove(prev);
        }
        DialogoAmisFragment dialogo = DialogoAmisFragment.newInstance(args);
        dialogo.setTargetFragment(this, REQUEST_CODE);
        dialogo.setCancelable(true);
        dialogo.show(fragmentTransaction, DialogoAmisFragment.class.getName());
    }


    /**
     * Clicks de botones
     * Lógica para Terminar o Cancelar el Siniestro
     */

    @OnClick(R.id.capture_terminar_siniestro)
    public void terminarAjuste() {
        getCompositeDisposable().add(ajusteInteractor.terminando(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(() -> {
                    intent = new Intent(activity, TerminacionActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                }, throwable -> {
                    Log.e(TAG, "Error al terminar Ajuste", throwable);
                    snackBar.largeSnackError(activity, throwable);
                }));
    }

    @OnClick(R.id.capture_cancelar)
    public void cancelarAjuste() {
        getCompositeDisposable().add(siniestrosInteractor.getMotivosCancelacionAjuste()
                .subscribe(cancelaciones -> {
                    AlertDialog.Builder mDialog = new AlertDialog.Builder(activity);
                    LayoutInflater inflater = this.getLayoutInflater();
                    final View mView = inflater.inflate(R.layout.dialog_cancelaciones, null);
                    mDialog.setView(mView);
                    final RecyclerView rvCancelaciones = mView.findViewById(R.id.rvCancelaciones);
                    cancelacionesAdapter = new CancelacionAdapter(cancelaciones);
                    rvCancelaciones.setAdapter(cancelacionesAdapter);
                    rvCancelaciones.setLayoutManager(new LinearLayoutManager(activity));
                    rvCancelaciones.setHasFixedSize(true);
                    mDialog.setTitle("Cancelar");
                    mDialog.setMessage(getString(R.string.cancel_message));
                    mDialog.setCancelable(false);
                    mDialog.setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss());
                    mDialog.setPositiveButton("Aceptar", (dialog, which) -> {
                        ao = new Gson().fromJson(
                                AppPreferences.getString(AppPreferences.JSON_AJUSTES_OPERACIONES),
                                new TypeToken<Set<AjusteOperacion>>() {
                                }.getType());
                        AjusteOperacion ajusteOperacion = new AjusteOperacion(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), Constants.CANCELADO);
                        if (ao != null) {
                            ao.add(ajusteOperacion);
                        } else {
                            ao = new HashSet<AjusteOperacion>();
                            ao.add(ajusteOperacion);
                        }

                        AppPreferences.setString(AppPreferences.JSON_AJUSTES_OPERACIONES,
                                Utils.objectoToJsonString(ao));
                    });
                    mDialog.show();
                }, throwable -> {
                    Log.e(TAG, "Error al obtner las cancelaciones", throwable);
                    snackBar.largeSnackError(activity, throwable.getMessage());
                }));
    }


    /**
     * Invocación las secciones según el Card seleccionado:
     * <ul>
     * <li>Asegurado</li>
     * <li>Teceros</li>
     * <li>Declaración</li>
     * <li>Recuperaciones</li>
     * <li>Servicios</li>
     * <li>Responsabilidad</li>
     * </ul>
     */
    @Override
    public void onItemClick(View view, int position) {
        AppCompatTextView mtitle = view.findViewById(R.id.card_title);
        Intent intent;
        switch (mtitle.getText().toString()) {
            case "Asegurado":
                view.setEnabled(false);
                card = "Asegurado";
                String aData = new Gson().toJson(ajuste);
                intent = new Intent(activity, AseguradoContentActivity.class);
                intent.putExtra("this.ajuste", aData);
                startActivity(intent);
                view.setEnabled(true);
                break;
            case "Terceros":
                card = "Tercero";
                String tData = new Gson().toJson(ajuste);
                intent = new Intent(activity, TercerosActivity.class);
                intent.putExtra("idAjuste", idAjuste);
                intent.putExtra("show", false);
                intent.putExtra("data", tData);
                intent.putExtra("tipoUbicacion", getArguments().getString("tipoUbicacion"));
                startActivity(intent);
                break;

            case "Recuperación":
                card = "Recuperación";
                intent = new Intent(activity, TercerosRecuperacionActivity.class);
                intent.putExtra("idAjuste", idAjuste);
                startActivity(intent);
                break;

            case "Servicios":
                card = "Servicios";
                compositeDisposable.add(ajusteInteractor.getAjuste(getCurrentAjuste())
                        .subscribe(ajuste -> {
                            Intent i = new Intent(activity, ServiciosActivity.class);
                            String mData = new Gson().toJson(this.ajuste);
                            i.putExtra("idAjuste", idAjuste);
                            i.putExtra("data", mData);
                            i.putExtra("tipoUbicacion", getArguments().getString("tipoUbicacion"));
                            if (!ajuste.getSiniestro().isPresiniestro()) {
                                startActivity(i);
                            } else {
                                DialogUtils.createAlert(activity,
                                        getString(R.string.title_only_rob_alert),
                                        getString(R.string.mensaje_solo_siniestros_servicios),
                                        getString(R.string.btn_continuar_text),
                                        null, (dialog, which) -> {
                                            dialog.dismiss();
                                        }, null);
                            }

                        }, throwable -> {
                            Log.e(TAG, "Error al obtener el ajuste", throwable);
                            snackBar.largeSnackError(activity, throwable);
                        }));
                break;

            case "Declaración":
                card = "Declaración";
                String dData = new Gson().toJson(ajuste);
                intent = new Intent(activity, DeclaracionActivity.class);
                intent.putExtra("idAjuste", idAjuste);
                intent.putExtra("data", dData);
                startActivity(intent);
                break;

            case "Responsabilidad":
                card = "Responsabilidad";
                intent = new Intent(activity, ResponsabilidadActivity.class);
                intent.putExtra("idAjuste", idAjuste);
                startActivity(intent);
                break;
            case "Documentos":

                compositeDisposable.add(responsabilidadesServices.getResponsabilidad(getCurrentAjuste())
                        .subscribe(res -> {
                            AppPreferences.setString(AppPreferences.ESTATUS_RESPONSABILIDAD, Utils.objectoToJsonString(res));
                            Intent i = new Intent(activity, VolanteActivity.class);
                            String iData = new Gson().toJson(this.ajuste);
                            i.putExtra("data", iData);
                            i.putExtra("idAjuste", idAjuste);
                            startActivity(i);
                        }, throwable -> {
                            if (throwable instanceof ResourceNotFoundException) {
                                DialogUtils.createAlert(activity, getString(R.string.message_res),
                                        getString(R.string.message_res_content),
                                        "Aceptar", null,
                                        (dialog, which) -> dialog.dismiss(),
                                        null);
                            } else {
                                Log.e(TAG, "Error al obtener la responsabilidad", throwable);
                                snackBar.largeSnackError(activity, throwable);
                            }
                        }));

                break;
        }
    }


    /**
     * Activa el botón de Termino de Siniestro
     */
    private boolean habilitaBotonConfirmacion() {
        btnTerminarSiniestro.setEnabled(true);
        btnTerminarSiniestro.setBackground(ContextCompat.getDrawable(activity, R.drawable.background_arround_actived));
        return true;
    }

    /**
     * Desactiva el botón de Termino de Siniestro
     */
    private boolean deshabilitaBotonConfirmacion() {
        btnTerminarSiniestro.setEnabled(false);
        btnTerminarSiniestro.setBackground(ContextCompat.getDrawable(activity, R.drawable.background_arround_disable));
        return true;
    }

    /**
     * Lógica para eliminar un card
     *
     * @deprecated Se quitó la funcionalidad de poder agregar o quitar Cards desde el menú.
     */
    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {
        AppCompatTextView mtitle = view.findViewById(R.id.card_title);
        AppCompatImageView mStatusCard = view.findViewById(R.id.icoSm1_guardado);
        switch (mtitle.getText().toString()) {
            case "Asegurado":

                break;

            case "Terceros":

                break;

            case "Recuperación":


                break;

            case "Servicios":

                break;

            case "Declaración":

                break;

            case "Responsabilidad":

                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -1:
                Log.d("alert", "aceptar");
                cardsList.remove(selected);
                recycler.removeViewAt(selected);
                adapter.notifyItemRemoved(selected);
                adapter.notifyItemRangeChanged(selected, cardsList.size());
                break;
            case -2:
                break;
            default:
                break;
        }
    }


    public void loadAdapter() {

        if (adapter == null) {
            adapter = new ModulosAdapter(cardLists);
            recycler.setAdapter(adapter);
            recycler.setItemViewCacheSize(7);
            adapter.setClickListener(this);
        } else {
            cardLists.clear();
            initializecardsviews();
            adapter.setItems(cardLists);
            recycler.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

    }

    /**
     * Inicializa las Leyendas de las Cards
     */
    public void initializecardsviews() {


        for (int i = 1; i <= 7; i++) {

            if (i == 1) {
                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.ic_declaracion_page);
                cardList.setTittleCard("Declaración");
                cardList.setSubModule1(" ");
                cardList.setSubModule2(" ");
                cardList.setSubModule3(" ");
                cardList.setSubModule4(" ");
                cardList.setIcoSm1_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm1_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm1_status(R.drawable.ic_obligatorio);

                cardLists.add(cardList);

            } else if (i == 2) {
                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.ic_asegurado_page);
                cardList.setTittleCard("Asegurado");
                cardList.setSubModule1("Conductor");
                cardList.setIcoSm1_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm1_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm1_status(R.drawable.ic_opcional);
                cardList.setSubModule2("Vehículo");
                cardList.setIcoSm2_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm2_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm2_status(R.drawable.ic_opcional);
                cardList.setSubModule3("Ocupantes");
                cardList.setIcoSm3_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm3_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm3_status(R.drawable.ic_opcional);
                cardList.setSubModule4(" ");

                cardLists.add(cardList);

            } else if (i == 3) {
                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.ic_terceros_page);
                cardList.setTittleCard("Terceros");
                cardList.setSubModule1("Auto");
                cardList.setIcoSm1_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm1_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm1_status(R.drawable.ic_opcional);
                cardList.setSubModule2("No auto");
                cardList.setIcoSm2_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm2_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm2_status(R.drawable.ic_opcional);
                cardList.setSubModule3("Peatón");
                cardList.setIcoSm3_foto(R.drawable.ic_foto_no);
                cardList.setIcoSm3_volante(R.drawable.ic_volante_no);
                cardList.setIcoSm3_status(R.drawable.ic_opcional);
                cardList.setSubModule4(" ");

                cardLists.add(cardList);

            } else if (i == 4) {
                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.ic_responsabilidad_page);
                cardList.setTittleCard("Responsabilidad");
                cardList.setSubModule1("Detalle de responsabilidad");
                cardList.setSubModule2(" ");
                cardList.setSubModule3(" ");
                cardList.setSubModule4(" ");
                cardList.setIcoSm1_status(R.drawable.ic_opcional);

                cardLists.add(cardList);


            } else if (i == 5) {
                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.imprimir);
                cardList.setTittleCard("Documentos");
                cardList.setSubModule1("Impresión de volantes");
                cardList.setSubModule2(" ");
                cardList.setSubModule3(" ");
                cardList.setSubModule4(" ");
                cardLists.add(cardList);

            } else if (i == 6) {


                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.recuperacion);
                cardList.setTittleCard("Recuperación");
                cardList.setSubModule1("Monetaria");
                cardList.setSubModule2("Documentos");
                cardList.setSubModule3("Garantía prendaría");

                cardList.setIcoSm1_status(R.drawable.ic_opcional);

                cardLists.add(cardList);
            }
            if (i == 7) {
                CardList cardList = new CardList();
                cardList.setImageCard(R.drawable.servicios);
                cardList.setTittleCard("Servicios");
                cardList.setSubModule1("Servicios de cobertura");
                cardList.setSubModule2(" ");
                cardList.setSubModule3(" ");
                cardList.setSubModule4(" ");

                cardLists.add(cardList);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateEstatus();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        handlerHere.removeCallbacks(runnableHere);
        if (null != presiniestroListener) {
            activity.unregisterReceiver(presiniestroListener);
            presiniestroListener = null;
        }
        if (null != refoleoListener) {
            activity.unregisterReceiver(refoleoListener);
            refoleoListener = null;
        }
    }

    private void updateEstatus() {
        infoEstatus = new InfoEstatus();
        updateDeclaracion();
        updateAsegurado();
        updateTercerosAuto();
        updateTercerosNoAuto();
        updateTercerosPeaton();
        updateResponsabilidad();
        updateRecuperaciones();
        updateServiciosDeCobertura();
        updateVolante();
        adapter.notifyDataSetChanged();
    }

    private String getCurrentAjuste() {
        return AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
    }

    private int getFotografiaIcon(Boolean valid) {
        if (valid == null) {
            return R.drawable.ic_opcional;
        } else if (valid) {
            return R.drawable.ic_foto;
        } else {
            return R.drawable.ic_foto_no;
        }
    }

    private int getVolanteIcon(Boolean valid) {
        if (valid == null) {
            return R.drawable.ic_opcional;
        } else if (valid) {
            return R.drawable.ic_volante;
        } else {
            return R.drawable.ic_volante_no;
        }
    }

    private int getEstatusIcon(EstatusResumen estatus) {
        int icon = R.drawable.ic_opcional;
        switch (estatus) {
            case OK:
                icon = R.drawable.ic_cloud_done_black_24dp;
                break;
            case ERROR:
                icon = R.drawable.ic_error;
                break;
            case CARGANDO:
                icon = R.drawable.ic_update;
                break;
            case PENDIENTE:
                icon = R.drawable.ic_completed;
                break;
            case SIN_ESTATUS:
                icon = R.drawable.ic_opcional;
                break;
        }
        return icon;
    }

    private void putEstatus(CardList card, EstatusResumen items, EstatusResumen status, int posicion) {
        if (posicion == 1 || posicion == 0) {
            if (items != null) {
                card.setIcoSm1_foto(getEstatusIcon(items));
                card.setIcoSm1_volante(getEstatusIcon(items));
                card.setIcoSm1_status(getEstatusIcon(status));
            }
        }

        if (posicion == 2 || posicion == 0) {
            if (items != null) {
                card.setIcoSm2_foto(getEstatusIcon(items));
                card.setIcoSm2_volante(getEstatusIcon(items));
            }
            card.setIcoSm2_status(getEstatusIcon(status));
        }

        if (posicion == 3 || posicion == 0) {
            if (items != null) {
                card.setIcoSm3_foto(getEstatusIcon(items));
                card.setIcoSm3_volante(getEstatusIcon(items));
            }
            card.setIcoSm3_status(getEstatusIcon(status));
        }
        adapter.notifyDataSetChanged();
    }

    private void putCargando(CardList card, boolean withItems, int posicion) {
        putEstatus(card, withItems ? EstatusResumen.CARGANDO : null, EstatusResumen.CARGANDO, posicion);
    }

    private void putError(CardList card, boolean withItems, Throwable throwable, int posicion) {
        putEstatus(card, withItems ? EstatusResumen.SIN_ESTATUS : null, EstatusResumen.ERROR, posicion);
        if (posicion == 1 || posicion == 0) {
            card.setMsgSm1(ErrorUtils.getErrorMessage(throwable));
        }
        if (posicion == 2 || posicion == 0) {
            card.setMsgSm2(ErrorUtils.getErrorMessage(throwable));
        }
        if (posicion == 3 || posicion == 0) {
            card.setMsgSm3(ErrorUtils.getErrorMessage(throwable));
        }
        adapter.notifyDataSetChanged();
    }

    private void putCargando(CardList card, boolean withItems) {
        putCargando(card, withItems, 0);
    }

    private void putError(CardList card, boolean withItems, Throwable throwable) {
        putError(card, withItems, throwable, 0);
    }

    private void updateDeclaracion() {
        CardList card = cardLists.get(0);
        putCargando(card, true, 1);
        compositeDisposable.add(ajusteInteractor.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {
                    card.setIcoSm1_foto(getFotografiaIcon(estatus.isEvidencias()));
                    card.setIcoSm1_volante(getVolanteIcon(estatus.isDocumento()));
                    card.setIcoSm1_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm1(estatus.getError());
                    }
                    infoEstatus.setDeclaracion(estatus);
                    adapter.notifyDataSetChanged();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de la declaración", throwable);
                    putError(card, true, throwable);
                }));
    }

    public void updateAsegurado() {
        CardList card = cardLists.get(1);
        putCargando(card, true);
        compositeDisposable.add(aseguradoInteractor.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm1_foto(getFotografiaIcon(estatus.getConductor().isFotografias()));
                    card.setIcoSm1_volante(getVolanteIcon(estatus.getConductor().isVolante()));
                    card.setIcoSm1_status(getEstatusIcon(estatus.getConductor().getEstatus()));
                    if (estatus.getConductor().hasErrores()) {
                        card.setMsgSm1(estatus.getConductor().getError());
                    }

                    card.setIcoSm2_foto(getFotografiaIcon(estatus.getVehiculo().isFotografias()));
                    card.setIcoSm2_volante(getVolanteIcon(estatus.getVehiculo().isVolante()));
                    card.setIcoSm2_status(getEstatusIcon(estatus.getVehiculo().getEstatus()));
                    if (estatus.getVehiculo().hasErrores()) {
                        card.setMsgSm2(estatus.getVehiculo().getError());
                    }

                    card.setIcoSm3_foto(getFotografiaIcon(estatus.getOcupantes().isFotografias()));
                    card.setIcoSm3_volante(getVolanteIcon(estatus.getOcupantes().isVolante()));
                    card.setIcoSm3_status(getEstatusIcon(estatus.getOcupantes().getEstatus()));
                    if (estatus.getOcupantes().hasErrores()) {
                        card.setMsgSm3(estatus.getOcupantes().getError());
                    }

                    infoEstatus.setAsegurado(estatus);
                    adapter.notifyDataSetChanged();

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus del asegurado", throwable);
                    putError(card, true, throwable);
                }));
    }

    public void updateTercerosAuto() {
        CardList card = cardLists.get(2);
        putCargando(card, true, 1);
        compositeDisposable.add(terceroAutoService.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm1_foto(getFotografiaIcon(estatus.isFotografias()));
                    card.setIcoSm1_volante(getVolanteIcon(estatus.isVolante()));
                    card.setIcoSm1_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm1(estatus.getError());
                    }

                    infoEstatus.setTercerosAuto(estatus);
                    adapter.notifyDataSetChanged();

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de los terceros autos", throwable);
                    putError(card, true, throwable, 1);
                }));
    }

    public void updateTercerosNoAuto() {
        CardList card = cardLists.get(2);
        putCargando(card, true, 2);
        compositeDisposable.add(tercerosNoAutoService.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm2_foto(getFotografiaIcon(estatus.isFotografias()));
                    card.setIcoSm2_volante(getVolanteIcon(estatus.isVolante()));
                    card.setIcoSm2_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm2(estatus.getError());
                    }

                    infoEstatus.setTercerosNoAuto(estatus);
                    adapter.notifyDataSetChanged();

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de los terceros no autos", throwable);
                    putError(card, true, throwable, 2);
                }));
    }

    private void updateTercerosPeaton() {
        CardList card = cardLists.get(2);
        putCargando(card, true, 3);
        compositeDisposable.add(tercerosPersonaService.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm3_foto(getFotografiaIcon(estatus.isFotografias()));
                    card.setIcoSm3_volante(getVolanteIcon(estatus.isVolante()));
                    card.setIcoSm3_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm3(estatus.getError());
                    }

                    infoEstatus.setTercerosPersona(estatus);
                    adapter.notifyDataSetChanged();

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de los terceros persona", throwable);
                    putError(card, true, throwable, 3);
                }));
    }

    public void updateRecuperaciones() {
        CardList card = cardLists.get(5);
        putCargando(card, false, 1);
        compositeDisposable.add(recuperacionInteractor.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm1_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm1(estatus.getError());
                    }
                    infoEstatus.setRecuperacion(estatus);
                    adapter.notifyDataSetChanged();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de las recuperaciones", throwable);
                    putError(card, false, throwable, 1);
                }));
    }

    public void updateServiciosDeCobertura() {
        CardList card = cardLists.get(6);
        putCargando(card, false, 1);
        compositeDisposable.add(serviciosCoberturaInteractor.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm1_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm1(estatus.getError());
                    }
                    infoEstatus.setServicios(estatus);
                    adapter.notifyDataSetChanged();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de los servicios de cobertura", throwable);
                    putError(card, false, throwable, 1);
                }));
    }

    public void updateVolante() {
        CardList card = cardLists.get(4);
        putCargando(card, false, 1);
        compositeDisposable.add(volanteService.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {
                    card.setIcoSm1_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm1(estatus.getError());
                    }
                    infoEstatus.setVolantes(estatus);
                    adapter.notifyDataSetChanged();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de los volantes", throwable);
                    putError(card, false, throwable, 1);
                }));
    }

    public void updateResponsabilidad() {
        CardList card = cardLists.get(3);
        putCargando(card, false, 1);
        compositeDisposable.add(responsabilidadesServices.getEstatus(getCurrentAjuste())
                .doOnTerminate(this::correReglasDeCierre)
                .subscribe(estatus -> {

                    card.setIcoSm1_status(getEstatusIcon(estatus.getEstatus()));
                    if (estatus.hasErrores()) {
                        card.setMsgSm1(estatus.getError());
                    }
                    infoEstatus.setResponsabilidad(estatus);
                    adapter.notifyDataSetChanged();

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el estatus de la responsabilidad", throwable);
                    putError(card, false, throwable, 1);
                }));
    }

    private void correReglasDeCierre() {
        if (isInflate(btnTerminarSiniestro)) {
            if (ajusteInteractor.canCerrar(infoEstatus)) {
                habilitaBotonConfirmacion();
            } else {
                deshabilitaBotonConfirmacion();
            }
        }
    }

    /**
     * Obtiene el folio eDUA
     */
    private void generarFolioeDUA() {
        generarFolioeDUA(false);
    }

    /**
     * Obtiene el folio eDua
     */
    private void generarFolioeDUA(boolean offline) {
        getCompositeDisposable().add(amisInteractor.getFolio(idAjuste, offline)
                .subscribe(folio -> {
                    folioeDUA = folio.getFolio().getFolio() != null ? folio.getFolio().getFolio() : folio.getFolio().getFolioInicial();
                    if (isInflate(txtFolioeDUA)) {
                        txtFolioeDUA.setText(folioeDUA);
                    }
                    String msm = ResourceUtils.getString("capture_child_txt_folio_eDUA");
                    if (isInflate(txtTitleFolioEdua)) {
                        msm = msm + " " + (folio.isOnline() ? "(Online)" : "(Offline)");
                        txtTitleFolioEdua.setText(msm);
                    }
                    Log.d(TAG, "FOLIO_EDUA: " + folioeDUA);
                }, throwable -> {
                    Log.e(TAG, "Ocurrio un error al obtetener el folio amis", throwable);
                    if (isInflate(txtFolioeDUA)) {
                        txtFolioeDUA.setText(getString(R.string.confirm_txt_en_camino));
                    }
                    DialogUtils.alert(DialogUtils.DialogType.WARNING,
                            "Error",
                            "Ocurrio un error al obtetener el folio amis.\n\n\u2022" + " " + throwable.getMessage() + "\n",
                            new DialogUtils.DialogButton("Reintentar", this::generarFolioeDUA),
                            new DialogUtils.DialogButton("Offline", () -> generarFolioeDUA(true)));
                }));
    }


    @OnClick(R.id.openContraparteBar)
    public void openContraparte() {
        if (!rvContraparte.isShown()) {
            openContraparte.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.rectangle_19));
            rvContraparte.setVisibility(View.VISIBLE);
            getContrapartes();

        } else {
            openContraparte.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.fill_1_copy));
            rvContraparte.setVisibility(View.GONE);
        }
    }


    private void getContrapartes() {
        compositeDisposable.add(amisInteractor.getContrapartes(idAjuste)
                .subscribe(this::setContrapartes,
                        throwable -> {
                            Log.e(TAG, "Error al obtener las contrapartes", throwable);
                            setContrapartes(new ArrayList<>());
                            if (!(throwable instanceof ResourceNotFoundException)) {
                                new GeneralSnackBar().largeSnackError(activity, throwable);
                            }
                        }));
    }

    public void setContrapartes(List<FolioContraparte> contrapartes) {
        if (contraparteAdapter == null) {
            contraparteAdapter = new ContraparteAdapater(contrapartes, this);
            rvContraparte.setAdapter(contraparteAdapter);
        } else {
            contraparteAdapter.setItems(contrapartes);
            rvContraparte.setAdapter(contraparteAdapter);
            contraparteAdapter.notifyDataSetChanged();
        }
    }


    private Boolean isInflate(View mView) {
        return mView != null;
    }

    @Override
    public void onConnected() {
        super.onConnected();
        SyncInteractor syncInteractor = OrquestadorInteractorFactory.getSyncService();
        compositeDisposable.add(ajusteInteractor.getAjuste(getCurrentAjuste())
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(ajuste -> {
                    if (!syncInteractor.isSynchronized(ajuste.getId())) {
                        compositeDisposable.add(syncInteractor.synchronizeData(ajuste.getId())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(() -> {
                                    this.updateEstatus();
                                    Log.i(TAG, "El ajuste se sincronizo correctamente");
                                }, throwable -> {
                                    Log.e(TAG, "Error al sincronizar el ajuste", throwable);
                                }));
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el ajuste", throwable);
                }));
    }

    @Override
    public void onItemClick(int position, View view) {
        if (contraparteAdapter!= null){
            FolioContraparte contraparte = contraparteAdapter.getItem(position);
            if (contraparte.isCompleto()){
                ToastUtils.toastLongSuccess("Información completa");
            }else {
                ToastUtils.toastLongError("Información incompleta");
            }
        }

    }
}

