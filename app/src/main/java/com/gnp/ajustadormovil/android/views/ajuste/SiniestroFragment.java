package com.gnp.ajustadormovil.android.views.ajuste;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.databinding.FragmentSiniestroDataBinding;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Lima on 23/10/17.
 */
public class SiniestroFragment extends BaseFragment {

    public static final String TAG = "TAG";
    public String tipoSiniestro;
    @BindView(R.id.detail_siniestro_content)
    LinearLayoutCompat detailSiniestro;
    @BindView(R.id.detail_vehiculo_content)
    LinearLayoutCompat detailVehiculo;
    @BindView(R.id.detail_observaciones_content)
    LinearLayoutCompat detailObservaciones;
    @BindViews({R.id.img_detalle_siniestro, R.id.img_vehiculo_detalle_siniestro, R.id.img_detalle_siniestro_observaciones})
    List<AppCompatImageView> imageViewList;
    private Unbinder mUnbinder;

    private AjusteInteractor ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
    private final BroadcastReceiver presiniestroListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(Constants.PAYLOAD_EVOLUTION.equals(intent.getAction())){
                String id = intent.getExtras().getString("ajuste");
                String dataResponse = getArguments().getString("data");
                Gson gson = new Gson();
                Ajuste data = gson.fromJson(dataResponse, Ajuste.class);
                if (id.equals(data.getId())) {
                    getCompositeDisposable().add(ajusteInteractor.getAjuste(id)
                            .subscribe(ajuste -> {
                                getArguments().putString("data", gson.toJson(ajuste));
                            }, throwable -> {
                                Log.e(TAG, "Error al obtener el ajuste", throwable);
                            }));
                }
            }
        }

    };

    public static SiniestroFragment newInstance() {
        SiniestroFragment fragment = new SiniestroFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSiniestroDataBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_siniestro_data,container,false);Gson gson = new Gson();
        getActivity().registerReceiver(presiniestroListener, new IntentFilter(Constants.PAYLOAD_EVOLUTION));
        String dataResponse = getArguments().getString("data");
        Ajuste data = gson.fromJson(dataResponse, Ajuste.class);
        Log.d(TAG, "onCreateView: " + dataResponse);
        Log.d(TAG, "onCreateView: " + data);
        mUnbinder = ButterKnife.bind(this, binding.getRoot());
        binding.setSiniestro(data.getSiniestro());
        return binding.getRoot();
    }


    @OnClick({R.id.detail_observaciones, R.id.detailSiniestro, R.id.detail_vehiculo})
    public void showOptions(View view) {
        switch (view.getId()) {
            case R.id.detailSiniestro:
                if (detailSiniestro.getVisibility() == View.VISIBLE) {
                    detailSiniestro.setVisibility(View.GONE);
                    imageViewList.get(0).setRotation(270);
                    imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    detailSiniestro.setVisibility(View.VISIBLE);
                    imageViewList.get(0).setRotation(0);
                    imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                detailVehiculo.setVisibility(View.GONE);
                detailObservaciones.setVisibility(View.GONE);
                imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(1).setRotation(270);
                imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(2).setRotation(270);
                break;

            case R.id.detail_vehiculo:

                if (detailVehiculo.getVisibility() == View.VISIBLE) {
                    detailVehiculo.setVisibility(View.GONE);
                    imageViewList.get(1).setRotation(270);
                    imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    detailVehiculo.setVisibility(View.VISIBLE);
                    imageViewList.get(1).setRotation(0);
                    imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                detailObservaciones.setVisibility(View.GONE);
                detailSiniestro.setVisibility(View.GONE);
                imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(0).setRotation(270);
                imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(2).setRotation(270);

                break;

            case R.id.detail_observaciones:

                if (detailObservaciones.getVisibility() == View.VISIBLE) {
                    imageViewList.get(2).setRotation(270);
                    detailObservaciones.setVisibility(View.GONE);
                    imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    detailObservaciones.setVisibility(View.VISIBLE);
                    imageViewList.get(2).setRotation(0);
                    imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                detailVehiculo.setVisibility(View.GONE);
                detailSiniestro.setVisibility(View.GONE);
                imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(0).setRotation(270);
                imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(1).setRotation(270);

                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        getActivity().unregisterReceiver(presiniestroListener);
    }
}