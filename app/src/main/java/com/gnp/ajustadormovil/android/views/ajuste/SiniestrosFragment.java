package com.gnp.ajustadormovil.android.views.ajuste;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.commons.adapters.PagerAdapter_Fragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lima on 05/10/17.
 */

public class SiniestrosFragment extends BaseFragment {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.result_tabs) TabLayout tabs;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_siniestro_parent, container, false);
        ButterKnife.bind(this, view);
        initPager(viewPager);
        tabs.setupWithViewPager(viewPager);
        return view;
    }

    public static SiniestrosFragment newInstance() {
        SiniestrosFragment fragment = new SiniestrosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    public void initPager(ViewPager viewPager){
        PagerAdapter_Fragment mPagerAdapterFragment = new PagerAdapter_Fragment(getChildFragmentManager());
        mPagerAdapterFragment.addFragment(AjustesFragment.class,"Atención de servicios");
        mPagerAdapterFragment.addFragment(HistorialFragment.class,"Historial");
        viewPager.setAdapter(mPagerAdapterFragment);
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        tabs.setupWithViewPager(viewPager);
    }
}
