package com.gnp.ajustadormovil.android.views.ajuste;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.ajuste.AjusteOperacion;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Completable;

/**
 * Created by Lima on 14/11/17.
 * Modified by OM on 05/03/2018
 */

public class TerminacionActivity extends BaseActivity {

    private static final String TAG = TerminacionActivity.class.getSimpleName();
    @BindView(R.id.toolbar_top)
    Toolbar mToolbar;
    @Nullable
    @BindView(R.id.home_rgroup_menu)
    RadioGroup mRadioGroup;
    @BindView(R.id.spTipoCierre)
    Spinner spCierre;
    @BindView(R.id.spMotivoRechazo)
    Spinner spMotivo;
    @BindView(R.id.rechazo_title)
    TextView titulo_rechazo;
    @BindView(R.id.edtConclusiones)
    EditText conclusiones;
    @BindView(R.id.tvPorTerminar)
    AppCompatTextView porTerminar;
    @BindView(R.id.finalizar_button)
    Button btnFinalizar;

    private Menu menu;
    private List<CatalogoClave> cierres = new ArrayList<>();
    private List<CatalogoClave> rechazos = new ArrayList<>();
    MaterialDialog dialog;
    private boolean flag = false;
    private AjusteInteractor ajusteInteractor;
    private SiniestrosInteractor siniestrosInteractor;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_finalizar_atencion);
        //setmRadioGroup(mRadioGroup);
        setmToolbar(mToolbar);
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        spCierre.setVisibility(View.GONE);
        spMotivo.setVisibility(View.GONE);
        titulo_rechazo.setVisibility(View.GONE);
        btnFinalizar.setEnabled(false);
        getCompositeDisposable().add(getCatalogos()
                .subscribe(() -> {
                    setArrays();
                    setEvents();
                }, throwable -> {
                    Log.e(TAG, "Ocurrio un error al obtener los catalogos", throwable);
                    new GeneralSnackBar().largeSnackError(this,throwable);
                }));
        newActivity = 1;
    }

    private void setEvents() {
        spCierre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spCierre.getSelectedItem().toString().equals("SINIESTRO RECHAZADO")) {
                    spMotivo.setVisibility(View.VISIBLE);
                    titulo_rechazo.setVisibility(View.VISIBLE);
                } else {
                    spMotivo.setVisibility(View.GONE);
                    titulo_rechazo.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setArrays(){

        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item,rechazos,CatalogoClave::getNombre);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotivo.setAdapter(adapter);

        AdapterSpinnerGeneral<CatalogoClave> adapterCierre = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, cierres, CatalogoClave::getNombre);
        adapterCierre.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCierre.setAdapter(adapterCierre);
        spCierre.setVisibility(View.VISIBLE);

    }


    private Completable getCatalogos() {
        return Completable.mergeArrayDelayError(
                getMotivosRechazo(),
                getMotivosCierre()

        );
    }

    private Completable getMotivosRechazo() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getMotivosRechazoAjuste()
                    .subscribe(listRechazos -> {
                        rechazos = listRechazos;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al obtener los motivos de rechazo", throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }));

        });
    }


    private Completable getMotivosCierre() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getMotivosCierreAjuste()
                    .subscribe(listCierres -> {
                        cierres = listCierres;
                        e.onComplete();
                        btnFinalizar.setEnabled(true);
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al obtener los motivos de cierre", throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);
                        btnFinalizar.setEnabled(true);
                    }));
        });


    }


    HashSet<AjusteOperacion> ao;

    @OnClick(R.id.finalizar_button)
    public void backHome() {
        if (conclusiones.getText().toString().equals("")) {
            conclusiones.setError("Debe agregar sus conclusiones");
            return;
        }

        AppPreferences.setString(AppPreferences.JSON_AJUSTES_OPERACIONES,
                Utils.objectoToJsonString(ao));
        LoadingIndicator.start();

        if (!cierres.isEmpty()){
            cerrarAjuste();
        }


    }


    private void cerrarAjuste() {
        getCompositeDisposable().add(ajusteInteractor.cerrar(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                cierres.get(spCierre.getSelectedItemPosition()).getClave(),
                conclusiones.getText().toString(), false)
                .subscribe(() -> {
                    LoadingIndicator.finish();
                    flag = true;
                    Intent intent = new Intent(TerminacionActivity.this, HomeActivity.class);
                    intent.putExtra("intentLogIn", AppPreferences.getString(AppPreferences.USER_ID_CEDULA));
                    intent.putExtra("intentLogIntoken", AjustesFragment.PositionToken);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finishAffinity();
                }, throwable -> {

                    LoadingIndicator.finish();

                    if (!(throwable instanceof ResourceNotFoundException))
                        new GeneralSnackBar().largeSnackError(this, throwable);

                    flag = true;
                    Intent intent = new Intent(TerminacionActivity.this, HomeActivity.class);
                    intent.putExtra("intentLogIn", AppPreferences.getString(AppPreferences.USER_ID_CEDULA));
                    intent.putExtra("intentLogIntoken", AjustesFragment.PositionToken);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finishAffinity();
                    Log.e(TAG, "Error al cerrar el ajuste", throwable);

                }));

    }


    @Override
    public void onBackPressed() {
        if (flag) {
            super.onBackPressed();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}
