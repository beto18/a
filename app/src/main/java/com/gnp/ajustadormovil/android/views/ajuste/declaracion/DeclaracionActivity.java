package com.gnp.ajustadormovil.android.views.ajuste.declaracion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.asegurado.AseguradoActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Predicate;

import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_DECLARACION;

/**
 * Created by OscarMichael on 2/4/18.
 * modified by hgzavala on 26/0472018
 */
public class DeclaracionActivity extends BaseActivity implements DeclaracionMvpView {

    public static final String TAG = "TAG";
    @BindView(R.id.toolbar_declaracion)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title_declaracion)
    TextView title;
    @BindView(R.id.declaracion_container)
    RelativeLayout mDeclaracion;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.btnGuardarDeclaracion)
    Button btnGuardarDeclaracion;
    @BindView(R.id.btnDescargaFormato)
    Button btnDescargaFormato;
    @BindView(R.id.contentCameraFragmentDeclaracion)
    FrameLayout frameLayout;
    @BindView(R.id.txtNarrativa)
    TextView txtNarrativa;
    @BindView(R.id.layoutNarrativa)
    LinearLayout layoutNarrativa;
    @BindView(R.id.layoutBotones)
    RelativeLayout layoutBotones;
    @BindView(R.id.lblNarrativa)
    TextView lblNarrativa;
    @BindView(R.id.firma)
    ImageView firma;
    @BindView(R.id.contentFirma)
    LinearLayout contentFirma;

    private String idAjuste = "";
    private Menu menu;
    private DeclaracionPresenter presenter;
    private GeneralCameraFragment generalCameraFragment;

    private AseguradoInteractor aseguradoInteractor;
    private final TprInteractor tprService = CatalogoInteractorsFactory.getTprService();
    private AjusteInteractor ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
    private int REQUEST_CODE_NARRATIVA = 2;
    private Ajuste ajuste;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declaracion);
        setmToolbar(mToolbar);
        generalCameraFragment = new GeneralCameraFragment();
        generalCameraFragment.disableImport("Declaracion");
        addFragmentCamara(generalCameraFragment);
        CameraActivity.modulo = MODULO_DECLARACION;
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        CompositeDisposable compositeDisposable = getCompositeDisposable();
        presenter = new DeclaracionPresenter(compositeDisposable);
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        presenter.onAttach(this);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        getAjuste();
    }

    private void getAjuste() {
        getCompositeDisposable().add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(this::updateDeclaracion,
                        throwable -> {
                            Log.e(TAG, "Error al obtener los datos del ajuste");
                            if (!(throwable instanceof ResourceNotFoundException)) {
                                new GeneralSnackBar().largeSnackError(this, throwable);
                            }

                        })
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void updateDeclaracion(Ajuste ajusteDetalle) {
        ajuste = ajusteDetalle;
        ImageUtils imageUtils = new ImageUtils();
        AjusteRepository repository = RepositoryFactory.getAjusteRepository();
        Ajuste ajuste = repository.findById(idAjuste);
        if (ajusteDetalle.getNarrativa() != null && ajusteDetalle.getUrlDeclaracion() != null) {
            btnDescargaFormato.setText(getString(R.string.reprint_declaration));
            txtNarrativa.setText(ajuste.getNarrativa());
            btnGuardarDeclaracion.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.VISIBLE);
            layoutNarrativa.setVisibility(View.VISIBLE);
            contentFirma.setVisibility(View.VISIBLE);
            if (ajusteDetalle.getFirma() != null) {
                firma.setVisibility(View.VISIBLE);
                firma.setImageBitmap(imageUtils.base64ToBitmap(ajusteDetalle.getFirma()));
            } else {
                firma.setVisibility(View.GONE);
            }
        } else {
            btnGuardarDeclaracion.setVisibility(View.GONE);
        }
        //Si el volante está impreso y tiene fotos.
        if (!ajusteDetalle.getFotografiasDeclaracion().isEmpty()) {
            mostrarDatosDeclaracion(getImagenesEvidencia(ajuste));

        } else if (ajusteDetalle.getUrlDeclaracion() != null && ajusteDetalle.getFotografiasDeclaracion().isEmpty()) {
            btnGuardarDeclaracion.setVisibility(View.VISIBLE);
            enabledPhoto(View.VISIBLE, true);
            enabledDeclaracion(1, true);
        } else {
            enabledDeclaracion(0.7f, false);
        }

    }

    @NotNull
    private ArrayList<ImageGeneralModel> getImagenesEvidencia(Ajuste ajusteDetalle) {
        ArrayList<ImageGeneralModel> filesPhotos = new ArrayList<>();
        for (Fotografia photo : ajusteDetalle.getFotografiasDeclaracion()) {
            filesPhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
        }
        return filesPhotos;
    }

    @OnClick(R.id.btnGuardarDeclaracion)
    public void addDeclaracion() {
        List<ImageGeneralModel> imageList = generalCameraFragment.getFileList();
        if (!imageList.isEmpty())
            presenter.sendImage(imageList, idAjuste);
        else
            new GeneralSnackBar().largeSnackError(this, getResources().getString(R.string.declaracion_error_img));
    }

    private void existenDatosAsegurado() {
        getCompositeDisposable().add(aseguradoInteractor.get(idAjuste)
                .doOnSubscribe(it -> LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .subscribe(asegurado -> {
                    if (asegurado.getConductor() == null) {
                        showDialog(getResources().getString(R.string.declaracion_data_conductor));
                        Log.d(TAG, "Error al obtener los datos del conductor asegurado");
                    } else if (asegurado.getVehiculo() == null || asegurado.getVehiculo().getArmadora() == null
                            || asegurado.getVehiculo().getCarroceria() == null) {

                        showDialog(getResources().getString(R.string.declaracion_data_vehiculo));
                        Log.d(TAG, "Error al obtener los datos del vehiculo asegurado");

                    } else {
                        getCompositeDisposable().add(tprService.getVersiones(asegurado.getVehiculo().getTipoVehiculo(),
                                asegurado.getVehiculo().getArmadora(),
                                asegurado.getVehiculo().getCarroceria(),
                                asegurado.getVehiculo().getModelo())
                                .subscribe(versiones -> {
                                    Predicate<VehiculoAsegurado> isExists = vehiculo -> {
                                        for (VersionVehiculoResumen version : versiones) {
                                            if (version.getVersion().equals(vehiculo.getVersion())) {
                                                return true;
                                            }
                                        }
                                        return false;
                                    };

                                    if (!isExists.test(asegurado.getVehiculo())) {
                                        showDialog(getResources().getString(R.string.declaracion_data_vehiculo));
                                        Log.d(TAG, "Error al obtener los datos del vehiculo asegurado");
                                    } else {
                                        Intent intent = new Intent(this, DeclaracionNarrativaActivity.class);
                                        startActivityForResult(intent, REQUEST_CODE_NARRATIVA);
                                    }
                                }, throwable -> {
                                    Log.e(TAG, "Error al obtener la version del vehiculo", throwable);
                                    new GeneralSnackBar().largeSnackError(this, throwable);
                                }));

                    }
                }, throwable -> {
                    Log.e(TAG, "Error al obtener datos del asegurado" + idAjuste, throwable);
                    if (throwable instanceof ResourceNotFoundException) {
                        showDialog("conductor");
                    } else {
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }
                }));
    }

    @OnClick(R.id.btnDescargaFormato)
    public void addDescargaDeclaracion() {
        if (ajuste.getUrlDeclaracion() != null) {
            showDeclaracion(ajuste.getUrlDeclaracion());
        } else {
            existenDatosAsegurado();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_NARRATIVA) {

                String narrativa = data.getStringExtra("NARRATIVA");
                String firma = data.getStringExtra("FIRMA_BASE64");
                /*
                 * Seteando narrativa a ajuste.
                 */
                DeclaracionCommand declaracion = new DeclaracionCommand();
                declaracion.setFirma(firma);
                declaracion.setNarrativa(narrativa);

                try {
                    ErrorUtils.assertValues(declaracion);
                    presenter.downloadDeclaracion(idAjuste, declaracion);
                    txtNarrativa.setText(narrativa);
                } catch (Exception ex) {
                    Log.i(TAG, "Errors: \n" + ErrorUtils.getErrorMessage(ex));
                    GeneralSnackBar snackBar = new GeneralSnackBar();
                    snackBar.largeSnackError(this, ErrorUtils.getErrorMessage(ex));
                }
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    @Override
    public void onSuccess(String message) {
        new GeneralSnackBar().largeSnackOk(DeclaracionActivity.this, message);


    }

    @Override
    public void onError(String message) {
        new GeneralSnackBar().largeSnackError(DeclaracionActivity.this, "Error: " + message);
    }

    @Override
    public void dismissDialog() {


    }

    @Override
    public void showDeclaracion(String declaracion) {
        Intent pdfViewer = new Intent(DeclaracionActivity.this, PrinterActivity.class);
        pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, declaracion);
        enabledPhoto(View.VISIBLE, true);
        startActivity(pdfViewer);
    }

    private void enabledPhoto(int visible, boolean isActive) {
        frameLayout.setVisibility(visible);
    }


    private void enabledDeclaracion(float alpha, boolean isActive) {
        btnGuardarDeclaracion.setAlpha(alpha);
        btnGuardarDeclaracion.setEnabled(isActive);
    }

    private void enabledDownloadFormat(float alpha, boolean enabled) {
        btnDescargaFormato.setAlpha(alpha);
        btnDescargaFormato.setEnabled(enabled);

    }


    @Override
    public void onFailure(Throwable t) {
        new GeneralSnackBar().largeSnackError(DeclaracionActivity.this, t);
    }

    @Override
    public void showImagesUpload(ArrayList<ImageGeneralModel> filePhotos) {
        mostrarDatosDeclaracion(filePhotos);
    }

    private void mostrarDatosDeclaracion(ArrayList<ImageGeneralModel> filePhotos) {
        enabledDeclaracion(0, false);
        enabledDownloadFormat(0, false);
        btnGuardarDeclaracion.setVisibility(View.GONE);
        layoutBotones.setVisibility(View.GONE);
        generalCameraFragment.disableCapture();
        frameLayout.setVisibility(View.VISIBLE);
        showImages(filePhotos);
    }

    private void showImages(ArrayList<ImageGeneralModel> filePhotos) {
        generalCameraFragment.disableCapture();
        generalCameraFragment.setImageList(filePhotos);
    }


    private void addFragmentCamara(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraFragmentDeclaracion, fragment, Constants.TAG_DECLARACION);
        transaction.commit();

    }

    private void showDialog(String data) {

        DialogUtils.createAlert(this, getResources().getString(R.string.declaracion_dialog_title),
                getResources().getString(R.string.declaracion_dialog_content) + " " + data
                , getResources().getString(R.string.declaracion_dialog_btn_p)
                , getResources().getString(R.string.declaracion_dialog_btn_n),
                (dialog, which) -> {
                    Intent intent = new Intent(this, AseguradoActivity.class);
                    intent.putExtra(data, true);
                    startActivity(intent);
                    finish();
                }, (dialog, which) -> {
                    finish();
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getAjuste();
    }


}

