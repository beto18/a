package com.gnp.ajustadormovil.android.views.ajuste.declaracion;

import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionCommand;
import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

import java.util.List;

public interface DeclaracionMvpPresenter <V extends DeclaracionMvpView> extends MvpPresenter<V> {
    void downloadDeclaracion(String idAjuste, DeclaracionCommand declaracionCommand);
    void sendImage(List<ImageGeneralModel> part, String idAjuste);
}
