package com.gnp.ajustadormovil.android.views.ajuste.declaracion;

import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.views.commons.base.MvpView;

import java.util.ArrayList;

public interface DeclaracionMvpView extends MvpView {

    void onSuccess(String message);

    void onError(String message);

    void showDeclaracion(String declaracion);

    void onFailure(Throwable t);

    void showImagesUpload(ArrayList<ImageGeneralModel> filePhotos);

    void dismissDialog();

}
