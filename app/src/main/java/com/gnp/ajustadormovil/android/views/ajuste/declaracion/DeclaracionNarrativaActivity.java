package com.gnp.ajustadormovil.android.views.ajuste.declaracion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.utils.signature.SignatureUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

import butterknife.BindView;
import io.reactivex.Single;

public class DeclaracionNarrativaActivity extends BaseActivity {

    @BindView(R.id.editNarrativa)
    EditText editNarrativa;
    @BindView(R.id.txtCountdown)
    TextView txtCountdown;
    @BindView(R.id.btnCapturarFirma)
    Button btnFirma;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.declaracion_narrativa_activity);

        GeneralSnackBar snackBar = new GeneralSnackBar();
        AjusteRepository repository = RepositoryFactory.getAjusteRepository();
        Ajuste ajuste = repository.findById(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL));
        if(ajuste.getNarrativa() != null) {
            editNarrativa.setText(ajuste.getNarrativa());
            editNarrativa.setSelection(editNarrativa.getText().length());
            txtCountdown.setText(String.format("%d/%d",Constants.MAX_CARACTERES - editNarrativa.getText().toString().length(),  Constants.MAX_CARACTERES));
        }

        btnFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editNarrativa.getText().toString().equals("")){
                    snackBar.largeSnackError(DeclaracionNarrativaActivity.this, "La narrativa no puede estar vacía");
                    return;
                }
                Single<String> qr = SignatureUtils.firma();
                getCompositeDisposable().add(qr.subscribe(result-> {

                    DeclaracionPresenter presenter;
                    Log.d("RESULTADO", result);
                    Intent returnIntent = new Intent();
                        returnIntent.putExtra("FIRMA_BASE64",result);
                        returnIntent.putExtra("NARRATIVA", editNarrativa.getText().toString().trim());
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(DeclaracionNarrativaActivity.this, "No se registró la firma");
                }));
            }
        });
        editNarrativa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                txtCountdown.setText(Constants.MAX_CARACTERES - editable.toString().length() + "/" + String.valueOf(Constants.MAX_CARACTERES));
            }
        });
    }
}
