package com.gnp.ajustadormovil.android.views.ajuste.declaracion;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.DeclaracionCommand;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DeclaracionPresenter<V extends DeclaracionMvpView> extends BasePresenter<V> implements
        DeclaracionMvpPresenter<V> {

    public static final String TAG = DeclaracionPresenter.class.getCanonicalName();
    private AjusteInteractor ajusteInteractor;


    DeclaracionPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
    }

    @Override
    public void downloadDeclaracion(String idAjuste, DeclaracionCommand declaracionCommand) {
        getmCompositeDisposable().add(ajusteInteractor.getDeclaracion(idAjuste, declaracionCommand)
                .doOnSubscribe(it->LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .subscribe(archivo -> {
                            Log.d(TAG, "archivo");
                            getMvpView().showDeclaracion(archivo.getUrl());
                            getMvpView().dismissDialog();
                        }
                        , throwable -> {
                    getMvpView().dismissDialog();
                            Log.e(TAG, "Error al obtener la declaración", throwable);
                            getMvpView().onFailure(throwable);

                        }));
    }

    @Override
    public void sendImage(List<ImageGeneralModel> part, String idAjuste) {
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : part) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        int size = images.size();

        ValueObject<Integer> count = new ValueObject<>(1);
        getmCompositeDisposable().add(Observable.fromIterable(images)
                .map(image -> new File(image.getFilePhat()))
                .flatMap(file -> ajusteInteractor.addEvidenciasDeclaracion(idAjuste, file).toObservable(),false, 1)
                .doFinally(()->{
                    if (count.getValue() < size) {
                        count.setValue(count.getValue() + 1);
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    }
                })
                .onErrorReturn(throwable -> {
                    Log.e(TAG, "Error al subir la evidencia", throwable);
                    return new Fotografia();
                }).toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(it -> {
                    LoadingIndicator.start();
                    LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                })
                .doFinally(LoadingIndicator::finish)
                .subscribe(evidencias -> {
                    boolean ok = true;
                    for (Fotografia pic : evidencias) {
                        ok &= pic.getUrl() != null;
                    }
                    if (ok) {
                        getFilesUpload(evidencias);
                    } else {
                        getMvpView().onError("Error al subir evidencias de la declaración");
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar tipos de vehículos", throwable);
                    getMvpView().onFailure(throwable);
                }));

    }


    private void getFilesUpload(List<Fotografia> response) {
        ArrayList<ImageGeneralModel> filesPhotos = new ArrayList<>();
        for (int i = 0; i < response.size(); i++) {
            filesPhotos.add(new ImageGeneralModel(String.valueOf(i), new Fotografia((long) i,
                    response.get(i).getUrl()), null, false));
        }

        getMvpView().onSuccess("Se subió correctamente la evidencia");
        getMvpView().showImagesUpload(filesPhotos);
    }
}
