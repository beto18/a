package com.gnp.ajustadormovil.android.views.ajuste.poliza;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.databinding.ClausulaItemBinding;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.ClausulaConvenioSacc;

import java.util.ArrayList;
import java.util.List;

public class ClausulasAdapter extends RecyclerView.Adapter<ClausulasAdapter.ClausulasHolder> {

    private ClausulaItemBinding itemBinding;

    private List<ClausulaConvenioSacc> clausulas =  new ArrayList<>();

    public void addAll(List<ClausulaConvenioSacc> clausulas){
        this.clausulas.addAll(clausulas);
    }

    @NonNull
    @Override
    public ClausulasHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        itemBinding = ClausulaItemBinding.inflate(inflater,parent,false);
        return new ClausulasHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ClausulasHolder holder, int position) {
        holder.bind(clausulas.get(position));
    }

    @Override
    public int getItemCount() {
        return clausulas.size();
    }

    public class ClausulasHolder extends RecyclerView.ViewHolder{

        private ClausulaItemBinding itemBinding;

        public ClausulasHolder(ClausulaItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }

        public void bind(ClausulaConvenioSacc clausula){
            itemBinding.setClausula(clausula);
            itemBinding.executePendingBindings();
        }
    }
}
