package com.gnp.ajustadormovil.android.views.ajuste.poliza;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.databinding.ContentCoberturaItemBinding;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.UnidadMedida;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lima on 24/10/17.
 * Modified by hgzavala 10/04/2018
 */

public class PolizaCoberturaAdapter extends RecyclerView.Adapter<PolizaCoberturaAdapter.DataViewHolder> {

    private ContentCoberturaItemBinding itemBinding;

    private List<PolizaCobertura> list;

    public PolizaCoberturaAdapter(List<PolizaCobertura> coberturas) {
        this.list = coberturas;
    }

    public PolizaCoberturaAdapter() {
        list = new ArrayList<>();
    }

    public void addAll(List<PolizaCobertura> results) {
        list.clear();
        list.addAll(results);
    }

    public void add(PolizaCobertura r) {
        list.add(r);
        notifyItemInserted(list.size() - 1);
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        itemBinding = ContentCoberturaItemBinding.inflate(inflater, parent, false);
        return new DataViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.bind(list.get(position),this);
    }

    public String setValue(PolizaCobertura cobertura) {
        Double price = cobertura.getSumaAsegurada();
        if (cobertura.getUnidadMedidaSumaAsegurada() != null && price != null) {
            return "Suma Asegurada: " + UnidadMedida.valueOf(cobertura.getUnidadMedidaSumaAsegurada().name()).format(price);
        }
        return "Suma Asegurada: " + price;
    }

    public String setDecucible(PolizaCobertura cobertura) {
        Double price = cobertura.getDeducible();
        if(cobertura.getUnidadMedidaDeducible() != null && price != null) {
            return "Deducible: " + UnidadMedida.valueOf(cobertura.getUnidadMedidaDeducible().name()).format(price);
        }
        return "Deducible: " + price;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {

        private ContentCoberturaItemBinding binding;

        DataViewHolder(ContentCoberturaItemBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(PolizaCobertura cobertura, PolizaCoberturaAdapter adapter) {
            binding.setPolizaCobertura(cobertura);
            binding.setAdapter(adapter);
            binding.executePendingBindings();
        }

    }

}