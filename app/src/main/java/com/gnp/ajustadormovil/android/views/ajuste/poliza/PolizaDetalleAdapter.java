package com.gnp.ajustadormovil.android.views.ajuste.poliza;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.PolizaDetalle;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by Abraham on 25/11/2017.
 */

public class PolizaDetalleAdapter extends RecyclerView.Adapter<PolizaDetalleAdapter.PolizaDetalleHolder>{

    private List<PolizaDetalle> polizaDetalles;

    public PolizaDetalleAdapter (List<PolizaDetalle> polizaDetalles) {
        this.polizaDetalles = polizaDetalles;
    }

    @Override
    public PolizaDetalleHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_poliza_detalles,parent,false);
        return new PolizaDetalleHolder (mView);
    }

    @Override
    public void onBindViewHolder (PolizaDetalleHolder holder, int position) {
        holder.atvCoche.setText (polizaDetalles.get (position).getCoche ());
        holder.atvPlaca.setText (polizaDetalles.get (position).getPlaca ());
        holder.atvNumSiniestro.setText (String.format ("%s%s","Siniestro: ",polizaDetalles.get (position).getNumeroSiniestro ()));
        holder.atvModelo.setText (String.format ("%s%s","MODELO ",polizaDetalles.get (position).getModelo ()));
    }

    @Override
    public int getItemCount () {
        return polizaDetalles.size ();
    }

    class PolizaDetalleHolder extends RecyclerView.ViewHolder{
        @BindView (R.id.atvCoche)
        AppCompatTextView atvCoche;
        @BindView (R.id.atvPlaca)
        AppCompatTextView atvPlaca;
        @BindView (R.id.atvNumSiniestro)
        AppCompatTextView atvNumSiniestro;
        @BindView (R.id.atvModelo)
        AppCompatTextView atvModelo;
        PolizaDetalleHolder (View itemView) {
            super (itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
