package com.gnp.ajustadormovil.android.views.ajuste.poliza;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.catalogos.CatalogsFactory;
import com.gnp.ajustadormovil.android.databinding.FragmentPolizaDetailBinding;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.PolizaDetalle;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Poliza;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity;
import com.gnp.ajustadormovil.android.views.ajuste.AjusteActivity;
import com.gnp.ajustadormovil.android.views.ajuste.AjustesFragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.custom.ItemDecorator;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Single;
import retrofit2.Retrofit;

/**
 * Created by Lima on 24/10/17.
 * UPDATED BY OSCAR January/2018
 */

public class PolizaFragment extends BaseFragment {
    private Unbinder mUnbinder;
    @BindView(R.id.poliza_cobertutas)
    RecyclerView recyclerCoberturas;
    @BindView(R.id.detail_generales_content)
    LinearLayoutCompat generalesContent;
    @BindView(R.id.detail_vehiculo_content)
    LinearLayoutCompat vehiculoContent;
    @BindView(R.id.detail_coberturas_content)
    LinearLayoutCompat observacionesContent;
    @BindView(R.id.detail_condiciones_content)
    LinearLayout condicionesContent;
    //@BindView(R.id.rvSiniestrosAsociados)
    //RecyclerView rvSiniestrosAsociados;
    @BindViews({R.id.img_detalles_generales_poliza, R.id.img_vehiculo_poliza, R.id.img_cobertura_poliza, R.id.img_condiciones_saac_poliza})
    List<AppCompatImageView> imageViewList;

    private List<PolizaDetalle> polizaDetalles;
    private PolizaDetalleAdapter polizaDetalleAdapter;
    private RecyclerView.LayoutManager lManager;
    private LinearLayoutManager manager;
    private String db_id_poliza;
    private Retrofit retrofit;
    public static final String TAG = "TAG";
    private AjusteInteractor ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
    String intentStringPoliza;
    private SiniestrosInteractor siniestrosInteractor;
    private Ajuste data;
    private String cashF;
    @BindView(R.id.tvProducto)
    TextView tvProducto;

    private HomeActivity mActivity;

    private final BroadcastReceiver presiniestroListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.PAYLOAD_EVOLUTION.equals(intent.getAction())) {
                String id = intent.getExtras().getString("ajuste");
                String dataResponse = getArguments().getString("data");
                Gson gson = new Gson();
                Ajuste data = gson.fromJson(dataResponse, Ajuste.class);
                if (id.equals(data.getId())) {
                    getCompositeDisposable().add(ajusteInteractor.getAjuste(id)
                            .subscribe(ajuste -> {
                                getArguments().putString("data", gson.toJson(ajuste));
                                display(ajuste);
                            }, throwable -> {
                                Log.e(TAG, "Error al obtener el ajuste", throwable);
                            }));
                }
            }
        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentPolizaDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_poliza_detail, container, false);
        AjusteActivity activity = (AjusteActivity) getActivity();
        assert activity != null;
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        intentStringPoliza = activity.sendPolizaTo_Tab_Fragment_CapturePoliza();
        mUnbinder = ButterKnife.bind(this, binding.getRoot());
        db_id_poliza = AjustesFragment.PositionForServicioAjuste;
        Gson gson = new Gson();
        getActivity().registerReceiver(presiniestroListener, new IntentFilter(Constants.PAYLOAD_EVOLUTION));
        String dataResponse = getArguments().getString("data");
        data = gson.fromJson(dataResponse, Ajuste.class);
        Log.d(TAG, "onCreateView: " + dataResponse);
        Log.d(TAG, "onCreateView: " + data);
        PolizaCoberturaAdapter coberturasAdapter = new PolizaCoberturaAdapter();
        ClausulasAdapter clausulasAdapter = new ClausulasAdapter();
        if (data.getSiniestro().getPoliza() != null) {
            isCashflow(data.getSiniestro().getPoliza());
            coberturasAdapter.addAll(data.getSiniestro().getPoliza().getCoberturas());
            clausulasAdapter.addAll(data.getSiniestro().getPoliza().getClausulas());
        }
        display(data);
        binding.setFragment(this);
        binding.setCoberturasAdapter(coberturasAdapter);
        binding.setClausulasAdapter(clausulasAdapter);
        binding.setSiniestro(data.getSiniestro());
        return binding.getRoot();
    }

    public void isCashflow(Poliza poliza) {
        getCompositeDisposable().add(
                siniestrosInteractor.getProductoCashflow(poliza.getProductoComercial(),
                        poliza.getProductoTecnico())
                        .subscribe(cashflow ->
                                        tvProducto.setText(setProducto(poliza, cashflow)),
                                throwable ->
                                        tvProducto.setText(setProducto(poliza, false))
                        ));

    }

    private String setProducto(Poliza poliza, Boolean isCashflow) {
        String producto;
        if (isCashflow) {
            producto = String.format("%s%s", "(CASHFLOW) ", !"".equals(poliza.getNombrePaquete()) ? poliza.getNombrePaquete() : "");
        } else {
            producto = !"".equals(poliza.getNombrePaquete()) ? poliza.getNombrePaquete() : "";
        }
        return producto;
    }

    private void display(Ajuste data) {
        if (data.getSiniestro().getPoliza() != null) {
            recyclerCoberturas.setHasFixedSize(true);
            RecyclerView.ItemDecoration dividerItemDecoratio2 = new ItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.line_divider));
            recyclerCoberturas.addItemDecoration(dividerItemDecoratio2);
        }
    }

    @OnClick({R.id.detail_gral_poliza, R.id.detail_vehiculo, R.id.detail_coberturas, R.id.detail_condiciones})
    public void optionsPoliza(View view) {
        switch (view.getId()) {
            case R.id.detail_gral_poliza:
                if (generalesContent.getVisibility() == View.VISIBLE) {
                    generalesContent.setVisibility(View.GONE);
                    imageViewList.get(0).setRotation(270);
                    imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    generalesContent.setVisibility(View.VISIBLE);
                    imageViewList.get(0).setRotation(0);
                    imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                vehiculoContent.setVisibility(View.GONE);
                observacionesContent.setVisibility(View.GONE);
                condicionesContent.setVisibility(View.GONE);
                imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(1).setRotation(270);
                imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(2).setRotation(270);
                imageViewList.get(3).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(3).setRotation(270);
                break;
            case R.id.detail_vehiculo:
                if (vehiculoContent.getVisibility() == View.VISIBLE) {
                    vehiculoContent.setVisibility(View.GONE);
                    imageViewList.get(1).setRotation(270);
                    imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    vehiculoContent.setVisibility(View.VISIBLE);
                    imageViewList.get(1).setRotation(0);
                    imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                generalesContent.setVisibility(View.GONE);
                observacionesContent.setVisibility(View.GONE);
                condicionesContent.setVisibility(View.GONE);
                imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(0).setRotation(270);
                imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(2).setRotation(270);
                imageViewList.get(3).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(3).setRotation(270);
                break;

            case R.id.detail_coberturas:
                if (observacionesContent.getVisibility() == View.VISIBLE) {
                    observacionesContent.setVisibility(View.GONE);
                    imageViewList.get(2).setRotation(270);
                    imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    observacionesContent.setVisibility(View.VISIBLE);
                    imageViewList.get(2).setRotation(0);
                    imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                generalesContent.setVisibility(View.GONE);
                vehiculoContent.setVisibility(View.GONE);
                condicionesContent.setVisibility(View.GONE);
                imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(0).setRotation(270);
                imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(1).setRotation(270);
                imageViewList.get(3).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(3).setRotation(270);
                break;

            case R.id.detail_condiciones:
                if (condicionesContent.getVisibility() == View.VISIBLE) {
                    condicionesContent.setVisibility(View.GONE);
                    imageViewList.get(3).setRotation(270);
                    imageViewList.get(3).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                } else {
                    condicionesContent.setVisibility(View.VISIBLE);
                    imageViewList.get(3).setRotation(0);
                    imageViewList.get(3).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_less));
                }
                generalesContent.setVisibility(View.GONE);
                vehiculoContent.setVisibility(View.GONE);
                observacionesContent.setVisibility(View.GONE);
                imageViewList.get(0).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(0).setRotation(270);
                imageViewList.get(1).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(1).setRotation(270);
                imageViewList.get(2).setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_left_v));
                imageViewList.get(2).setRotation(270);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        getActivity().unregisterReceiver(presiniestroListener);
    }

    public static PolizaFragment newInstance() {
        PolizaFragment fragment = new PolizaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }
}
