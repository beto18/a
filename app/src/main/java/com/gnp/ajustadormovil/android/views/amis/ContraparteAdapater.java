package com.gnp.ajustadormovil.android.views.amis;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparte;
import com.gnp.ajustadormovil.android.views.commons.events.ItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContraparteAdapater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<FolioContraparte> contrapartes;
    private ItemClickListener listener;


    public ContraparteAdapater(List<FolioContraparte> contrapartes, ItemClickListener listener) {
        this.contrapartes = contrapartes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_items, parent, false));
        } else {
            return new ContraparteHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contraparte, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ContraparteHolder) {
            ((ContraparteHolder) holder).content.setText(contrapartes.get(position).getFolio() != null ? contrapartes.get(position).getFolio() : contrapartes.get(position).getFolioInicial());
            ((ContraparteHolder) holder).status.setImageResource(contrapartes.get(position).isCompleto() ? R.drawable.ic_completed : R.drawable.ic_error);

        } else if (holder instanceof EmptyHolder) {
            ((EmptyHolder) holder).tvEmpty.setText("No hay información de contrapartes");

        }
    }


    public FolioContraparte getItem(int position) {
        return contrapartes.get(position);
    }

    @Override
    public int getItemCount() {
        if (contrapartes.isEmpty()) return 1;
        else return contrapartes.size();
    }

    public void setItems(List<FolioContraparte> contrapartes) {
        this.contrapartes = contrapartes;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (contrapartes.isEmpty()) return 1;
        else return 2;
    }

    class ContraparteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvContent)
        AppCompatTextView content;
        @BindView(R.id.itemStatus)
        ImageButton status;

        ContraparteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            status.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(getAdapterPosition(), view);
        }
    }

    class EmptyHolder extends RecyclerView.ViewHolder {
        TextView tvEmpty;

        public EmptyHolder(View itemView) {
            super(itemView);

            tvEmpty = itemView.findViewById(R.id.tvEmptyItems);

        }
    }
}
