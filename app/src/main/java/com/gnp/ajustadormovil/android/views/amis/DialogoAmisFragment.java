package com.gnp.ajustadormovil.android.views.amis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrData;
import com.gnp.ajustadormovil.android.views.utils.qr.QrNotFoundException;
import com.gnp.ajustadormovil.android.views.utils.qr.QrUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseBottomSheetDialogFragment;
import com.gnp.ajustadormovil.android.views.terceros.TercerosActivity;
import com.gnp.ajustadormovil.android.views.terceros.autos.TerceroAutoActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DialogoAmisFragment extends BaseBottomSheetDialogFragment {


    private View view;
    @BindView(R.id.imgVincular)
    ImageButton imgVincular;
    @BindView(R.id.imgQR)
    ImageButton imgQR;
    @BindView(R.id.imgScan)
    ImageButton imgScan;
    @BindView(R.id.imgEnviarDatos)
    ImageButton imgEnviarDatos;
    @BindView(R.id.imgDescargarDatos)
    ImageButton imgDescargarDatos;
    @BindView(R.id.txt_enviar_datos)
    TextView enviarDatos;
    @BindView(R.id.txt_descargar_datos)
    TextView descargarDatos;


    private CompositeDisposable compositeDisposable;
    private Activity activity;
    private AmisInteractor amisInteractor;
    private GeneralSnackBar snackbar;
    private final String TAG = DialogoAmisFragment.class.getSimpleName();
    private String idAjuste;
    private Intent intent;
    private TerceroAutoInteractor terceroAutoService;
    private Ajuste ajuste;
    private String folioEdua;


    public static DialogoAmisFragment newInstance(Bundle args) {
        DialogoAmisFragment fragment = new DialogoAmisFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();

        if (getArguments() != null) {
            ajuste = new Gson().fromJson(getArguments().getString("ajuste"), Ajuste.class);
            folioEdua = getArguments().getString("folioEdua");
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_edua_compartir, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        snackbar = new GeneralSnackBar();
        if (!ConnectionProvider.getInstance().isConnected()) {
            imgEnviarDatos.setVisibility(View.GONE);
            imgDescargarDatos.setVisibility(View.GONE);
            enviarDatos.setVisibility(View.GONE);
            descargarDatos.setVisibility(View.GONE);
        } else {
            imgEnviarDatos.setVisibility(View.VISIBLE);
            imgDescargarDatos.setVisibility(View.VISIBLE);
            enviarDatos.setVisibility(View.VISIBLE);
            descargarDatos.setVisibility(View.VISIBLE);

        }
    }


    /**
     * Click boton
     * Función para vincular folios de las contrapartes
     */
    @OnClick(R.id.imgVincular)
    public void vincularFoliosEdua() {
        compositeDisposable.add(QrUtils.readMultiple(value -> value.contains("|"))
                .map(folio -> folio.substring(folio.lastIndexOf("|") + 1))
                .toList()
                .subscribe(folios -> {
                    for(String folio: folios) {
                        Log.d(TAG, "Folio vinculado: " + folio);
                    }
                    compositeDisposable.add(amisInteractor.vincularFolios(idAjuste, folios)
                            .subscribe(() -> {
                                snackbar.largeSnackOk(activity, "Folios vinculados con éxito");
                                Ajuste ajuste = RepositoryFactory.getAjusteRepository().findById(idAjuste);
                                RepositoryFactory.getAjusteRepository().save(ajuste);
                                closeDialog();
                                enviarRefoleo();
                            }, throwable -> {
                                closeDialog();
                                snackbar.largeSnackError(activity,throwable);
                                Log.e(TAG, "Error al vincular folios", throwable);

                            }));
                }, throwable -> {
                    closeDialog();
                    if (throwable instanceof QrNotFoundException) {
                        snackbar.largeSnackError(activity, ResourceUtils.getString("qr_not_found"));
                    }else {
                        snackbar.largeSnackError(activity, throwable);
                    }
                    Log.e(TAG, "Error al obtener el QR", throwable);
                }));

    }

    /**
     * Click boton
     * Refolea los folios
     */

    /**
     * Click boton
     * Geenera el QR de los modulos del asegurado
     */
    @OnClick(R.id.imgQR)
    public void generarQR() {
        compositeDisposable.add(amisInteractor.getModulos(idAjuste)
                .flatMap(amisInteractor::encriptarModulos)
                .map(modulos -> {
                    List<QrData> qrs = new ArrayList<>(modulos.size());
                    for (String modulo : modulos) {
                        String[] qr = modulo.split("\\|");
                        String title = ResourceUtils.getString("amis_modulo_" + qr[1]);
                        Log.d(TAG, title + ": " + modulo);
                        QrData data = new QrData();
                        data.setTitulo(title);
                        data.setQr(modulo);
                        qrs.add(data);
                    }
                    return qrs;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(qrs-> {
                    closeDialog();
                    QrUtils.showMultiple(qrs);
                }, throwable -> {
                    closeDialog();
                    Log.e(TAG, "Error al mostrar QRs d información", throwable);
                    snackbar.largeSnackError(activity, throwable );
                }));

    }


    /**
     * Click boton
     * Escanea los codigos QR de la contraparte
     */
    @OnClick(R.id.imgScan)
    public void ScanQr() {
        DialogUtils.alert(getActivity(), "Aviso",
                ResourceUtils.getString("descarga_datos_edua_aviso"),
                ResourceUtils.getString("btn_accept_text"),
                (dialog, which) -> {
                    leerQR();
                    dialog.dismiss();
                });
    }


    private void leerQR() {
            compositeDisposable.add(QrUtils.readMultiple(value -> value.contains("|"))
                    .toList()
                    .flatMap(amisInteractor::desencriptarModulos)
                    .flatMap(modulos -> amisInteractor.saveModulos(idAjuste, modulos)
                            .andThen(Single.just(modulos.get(0).getFolio())))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(folio -> {
                        compositeDisposable.add(terceroAutoService.get(idAjuste, folio)
                                .subscribe(tercero -> {
                                    String stringConductor = "";
                                    String stringVehiculo = "";
                                    String stringOcupantes = "";
                                    if (tercero.getConductor() != null) {
                                        Persona foundConductor = tercero.getConductor();
                                        stringConductor = new Gson().toJson(foundConductor);
                                    }
                                    if (tercero.getVehiculo() != null) {
                                        VehiculoTercero foundVehiculo = tercero.getVehiculo();
                                        stringVehiculo = new Gson().toJson(foundVehiculo);
                                    }
                                    if (tercero.getOcupantes() != null) {
                                        List<Ocupante> foundOcupantes = tercero.getOcupantes();
                                        stringOcupantes = new Gson().toJson(foundOcupantes);
                                    }
                                    Intent intent = new Intent(activity, TerceroAutoActivity.class);
                                    intent.putExtra("id", tercero.getId());
                                    intent.putExtra("show", true);
                                    intent.putExtra("foundConductor", stringConductor);
                                    intent.putExtra("foundVehiculo", stringVehiculo);
                                    intent.putExtra("foundOcupantes", stringOcupantes);
                                    intent.putExtra("data", getArguments().getString("data"));
                                    startActivity(intent);
                                    enviarRefoleo();
                                }, throwable -> {
                                    if (throwable instanceof ResourceNotFoundException) {
                                        snackbar.largeSnackError(activity, "No se pudo agregar terceros con la información proporcionada.");
                                    } else {
                                        Log.e(TAG, "Error al obtener el nuevo tercero", throwable);
                                        snackbar.largeSnackError(activity, throwable);
                                    }

                                }));

                        snackbar.largeSnackOk(activity, "Módulos capturados con éxito.");
                        closeDialog();

                    }, throwable -> {
                        closeDialog();
                        Log.e(TAG, "Error al guardar los módulos", throwable);
                        if (throwable instanceof QrNotFoundException) {
                            snackbar.largeSnackError(activity, ResourceUtils.getString("qr_not_found"));
                        }
                        else {
                            snackbar.largeSnackError(activity, throwable);
                        }
                    }));

    }

    private void enviarRefoleo() {
        Intent intent = new Intent(Constants.ON_REFOLEO);
        intent.putExtra("idAjuste", idAjuste);
        getActivity().sendBroadcast(intent);
    }

    /**
     * Click boton
     * sincroniza informacion del asegurado
     */
    @OnClick(R.id.imgEnviarDatos)
    public void sincronizarModulos() {
        compositeDisposable.add(amisInteractor.sincronizar(idAjuste)
                .subscribe(() -> {
                    snackbar.largeSnackOk(activity, "Módulos sincronizados correctamente");
                    enviarRefoleo();
                    closeDialog();
                }, throwable -> {
                    closeDialog();
                    Log.e(TAG, "Error al sincronizar módulos", throwable);
                    snackbar.largeSnackError(activity, throwable);
                }));
    }


    private void closeDialog(){
        if (getDialog() != null) {
            getDialog().dismiss();
        }
    }

    /**
     * Click boton
     * Descarga datos de la contraparte
     */
    @OnClick(R.id.imgDescargarDatos)
    public void descargarDatos() {
        DialogUtils.alert(getActivity(), "Aviso",
                ResourceUtils.getString("descarga_datos_edua_aviso"),
                ResourceUtils.getString("btn_accept_text"),
                (dialog1, which) -> {
                    getModulos();
                });

    }

    /**
     * Click boton
     * Genera el código QR del folio para compartir información
     */
    @OnClick(R.id.compartirFolio)
    public void compartirFolio() {
        closeDialog();
        if (folioEdua != null && !folioEdua.isEmpty()) {
            QrUtils.show("Folio eDUA", folioEdua + "|-1|" + folioEdua);
        }else {
            new GeneralSnackBar().largeSnackError(activity, "No cuenta con folio para compartir.");
        }

    }


    private void getModulos() {
        compositeDisposable.add(amisInteractor.getInformacion(idAjuste)
                .subscribe(() -> {
                    String tData = new Gson().toJson(ajuste);
                    intent = new Intent(activity, TercerosActivity.class);
                    intent.putExtra("idAjuste", idAjuste);
                    intent.putExtra("show", true);
                    intent.putExtra("data", tData);
                    intent.putExtra("tipoUbicacion", getArguments().getString("tipoUbicacion"));
                    startActivity(intent);
                    snackbar.largeSnackOk(activity, "Módulos sincronizados correctamente");
                    closeDialog();
                    enviarRefoleo();

                }, throwable -> {
                    closeDialog();
                    Log.e(TAG, "Error al sincronizar módulos", throwable);
                    snackbar.largeSnackError(activity, throwable);
                })
        );
    }


}