package com.gnp.ajustadormovil.android.views.asegurado;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModuleCar;
import com.gnp.ajustadormovil.android.data.model.ModuleDriver;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.PolizaInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.FormatStrings;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.CorreoAdapter;
import com.gnp.ajustadormovil.android.views.commons.adapters.PhoneAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.events.OnImageListener;
import com.gnp.ajustadormovil.android.views.commons.lesiones.GeneralInjuredFragment;
import com.gnp.ajustadormovil.android.views.commons.piezas.GeneralDamageFragment;
import com.gnp.ajustadormovil.android.views.volantes.asegurado.VolanteAseguradoPersonaFragment;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmList;

import static android.view.View.GONE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_ASEGURADOS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_CONDUCTOR;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_OCUPANTE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_VEHICULO;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getAllImages;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getImagePathCar;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getImagePathDriver;
import static com.gnp.ajustadormovil.android.utils.Constants.ASEGURADOS;
import static com.gnp.ajustadormovil.android.utils.Constants.CONDUCTOR;
import static com.gnp.ajustadormovil.android.utils.Constants.TAG_NAME_CAPTURE;
import static com.gnp.ajustadormovil.android.utils.Constants.VEHICULO;

/**
 * Created by Lima on 14/11/17.
 */

public class AseguradoActivity extends BaseActivity implements CallbackItemImage<Long>, CallBackItemCheck, CallbackClickItem,
        OnImageListener, TextWatcher, AdapterView.OnItemSelectedListener {

    public static final String TAG = AseguradoActivity.class.getSimpleName();
    private static final int AGREGAR_OCUPANTE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private static final int PERMISSION_REQUEST = 100;
    private boolean isUploadedP = false;
    private boolean isUploadedV = false;
    private VehiculoAsegurado foundVehiculo;
    @BindView(R.id.etFirstName)
    EditText firstName;
    @BindView(R.id.tvNumIdentificacion)
    TextView numIdentificacion;
    @BindView(R.id.etLastName)
    EditText lastName;
    @BindView(R.id.etMotherLastName)
    EditText motherLastName;
    @BindView(R.id.dateNac)
    EditText dateNacimiento;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.etNumeroIdentificacion)
    EditText etNumeroIdentificacion;
    @BindView(R.id.dateVigencia)
    EditText dateVigencia;
    @BindView(R.id.edtOtroHospital)
    EditText edtOtroHospital;
    @BindView(R.id.lesionado_container)
    LinearLayout lesionado_container;
    @BindView(R.id.rbMasculino)
    RadioButton rbMasculino;
    @BindView(R.id.btnGuardarAseguradoAuto)
    Button btnGuardarVehiculo;
    @BindView(R.id.title_otro)
    TextView otro_hospital;
    @BindView(R.id.spinnerIdentificacion)
    Spinner spinnerIdentificacion;
    @BindView(R.id.spinnerEstadoEmisor)
    Spinner spinnerEstadoEmisor;
    @BindView(R.id.spinnerTipoLicencia)
    Spinner spinnerTipoLicencia;
    @BindView(R.id.spinnerStatus)
    Spinner spinnerStatus;
    @BindView(R.id.spinnerNivelLesion)
    Spinner spinnerNivelLesion;
    @BindView(R.id.open_driver)
    ImageView openDriver;
    @BindView(R.id.open_vehiculo)
    ImageView openVehiculo;
    @BindView(R.id.open_ocupante)
    ImageView openOcupante;
    @BindView(R.id.open_driver_bar)
    FrameLayout openDriverBar;
    @BindView(R.id.open_vehiculo_bar)
    FrameLayout openVehiculoBar;
    @BindView(R.id.open_ocupante_bar)
    FrameLayout openOcupanteBar;

    @BindView(R.id.btnGuardarConductor)
    Button btnGuardarConductor;
    @BindView(R.id.cbOtro)
    CheckBox cbOtro;
    @BindView(R.id.otro_container)
    LinearLayout otro_container;
    @BindView(R.id.cbPermanente)
    CheckBox checkBoxPermanente;
    @BindView(R.id.consequence_damage)
    CheckBox checkBoxConsequence;
    @BindView(R.id.cbAmbulancia)
    CheckBox cbAmbulancia;
    @BindView(R.id.spPrimerImpacto)
    Spinner spPrimerImpacto;
    long id_conductor = 0;
    @BindView(R.id.recyclerEmail)
    RecyclerView recyclerEmail;
    @BindView(R.id.recyclerPhone)
    RecyclerView recyclerPhone;
    @BindView(R.id.title_estado)
    TextView titleEstado;
    @BindView(R.id.content_hospital)
    LinearLayout contentHospital;
    @BindView(R.id.content_titile_hospital)
    LinearLayout contentTitleHospital;
    @BindView(R.id.btnSolicitarAmbulancia)
    Button solicitarAmbulancia;
    @BindView(R.id.etObservaciones)
    EditText etObservaciones;
    @BindView(R.id.cbConcecuencia)
    CheckBox cbPerdida;
    @BindView(R.id.content_driver)
    View contentDriver;
    @BindView(R.id.content_car)
    LinearLayoutCompat contentCar;
    @BindView(R.id.content_ocupant)
    View contentOcupant;
    @BindView(R.id.main_toolbar)
    Toolbar mToolbar;
    @Nullable
    @BindView(R.id.asegurado_radio_buttons)
    RadioGroup mRadioGroup;
    @BindView(R.id.rgSexo)
    RadioGroup sexos;
    @BindViews({R.id.spTipoVehiculoP, R.id.spArmadora, R.id.spCarroceria, R.id.spModeloP, R.id.spVersionP})
    List<Spinner> spVehiculoDatos;
    @BindViews({R.id.edtPlacasP, R.id.edtMotorV, R.id.edtSerieV})
    List<EditText> vehiculo_datos;
    @BindView(R.id.etAge)
    EditText edad;


    @BindView(R.id.btnuploadImages)
    Button uploadImage;
    @BindView(R.id.edtEstimadoDano)
    EditText etEstimadoDano;
    @BindView(R.id.btnuploadImages2)
    Button uploadImageVehicle;
    boolean statusLesionado = false;
    @BindView(R.id.spinnerHospital)
    Spinner spinnerHospital;
    @BindView(R.id.spinnerEstadoAmbulancia)
    Spinner spinnerEstadoAmbulancia;
    @BindView(R.id.title_nivel_lesion)
    TextView titulo_lesion;
    @BindView(R.id.tvNumeroPoliza)
    TextView poliza;
    @BindView(R.id.recyclerOcupantes)
    RecyclerView recyclerOcupantes;
    @BindView(R.id.asegurados_container)
    RelativeLayout layout;
    Calendar calendar = Calendar.getInstance();
    @BindView(R.id.daños_container)
    LinearLayout daño_container;
    @BindView(R.id.tvTipoVehiculoV)
    TextView tvTipo;

    @BindView(R.id.tvArmadoraV)
    TextView tvArmadora;

    @BindView(R.id.tvCarroceriaV)
    TextView tvCarroceria;
    @BindView(R.id.tvModeloV)
    TextView tvModelo;
    @BindView(R.id.tvVersionV)
    TextView tvVersion;
    @BindView(R.id.spColorVehiculo)
    Spinner spColor;
    private Menu menu;
    private Boolean autoriza;
    private MaterialDialog mBuilder;
    private Ajuste ajuste;
    private CheckBox si, no;
    private List<Telefono> list = new RealmList<>();
    private List<ImpactoVehiculoAmis> impactos = new ArrayList<>();
    private List<CatalogoClave> identificaciones = new ArrayList<>();
    private VersionVehiculo categoria;
    private Persona conductorAsegurado;
    private List<CatalogoId> niveles_lesion = new ArrayList<>();
    private List<HospitalResumen> hospitales = new ArrayList<>();
    private List<Supervisoria> supervisorias = new ArrayList<>();
    private List<CatalogoClaveInfo> estadosLicencia = new ArrayList<>();
    private CorreoAdapter adapterEmail;
    private PhoneAdapter adapterPhone;
    private AseguradoOcupanteAdapter ocupantesAdapter;

    private String idAjuste = "";
    private String placa = "", motor = "", serie = "";
    private EditText phone, email, extension;
    private Spinner spinnerTipoTelefono;
    private List<CatalogoClave> armadorasList = new ArrayList<>();
    private List<CatalogoClave> carroceriaList = new ArrayList<>();
    private List<CatalogoClave> tipoVehiculos = new ArrayList<>();
    private List<Short> modelos = new ArrayList<>();
    private List<VersionVehiculoResumen> versiones = new ArrayList<>();
    private List<String> listCoberturasAgregadas = new ArrayList<>();
    private GeneralCameraFragment generalCameraFragmentDriver;
    private GeneralCameraFragment generalCameraFragmentVehicle;
    @BindView(R.id.toolbar_title)
    TextView title;

    private ModuleDriver moduleDriver;
    private List<ImageGeneralModel> fotos = new ArrayList<>();
    private List<ImageGeneralModel> filesPhotos = new ArrayList<>();
    private List<ImageGeneralModel> filesPhotosVehiculo = new ArrayList<>();

    private String path;
    private GeneralDamageFragment generalDamageFragment;
    private GeneralDamageFragment generalDamageFragmentPreexisting;
    private GeneralInjuredFragment generalInjuredFragment;


    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private TprInteractor tprService;

    private AseguradoInteractor aseguradoInteractor;
    private AjusteInteractor ajusteInteractor;
    private PolizaInteractor polizaInteractor;

    private NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));

    private AdapterSpinnerGeneral<CatalogoClave> adapterVehiculo;
    private List<CatalogoClaveAmis> tiposTelefonos = new ArrayList<>();
    private String tipoVehiculo;
    private String armadora;
    private String carroceria;
    private Short modelo;
    private String version;
    private Long colorVehiculo;
    private String tipoLicencia;
    private Hospital hospital;

    private List<CatalogoId> coloresList = new ArrayList<>();
    private List<CatalogoClave> tiposLicenciaList = new ArrayList<>();


    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asegurado_forms);
        ButterKnife.bind(this);
        setmToolbar(mToolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        newActivity = 1;
        spVehiculoDatos.get(1).setVisibility(View.GONE);
        spVehiculoDatos.get(2).setVisibility(View.GONE);
        spVehiculoDatos.get(3).setVisibility(View.GONE);
        spVehiculoDatos.get(4).setVisibility(View.GONE);
        otro_container.setVisibility(View.INVISIBLE);
        enabledSpinners(false);

        initializeFragments();
        initServices();


        uploadImage.setVisibility(GONE);
        enabledSpinners(false);

        if (contentCar.getVisibility() == View.GONE) {
            isUploadedV = true;
        }
        setEvents();
        initComponents();
        downloadOcupantes();
        getAjuste();
        initListeners();
        initListenersAuto();


        newActivity = 1;
        sexos.setClickable(true);


        if (getIntent().getBooleanExtra("conductor", false)) {
            openDriverInfo();
        } else if (getIntent().getBooleanExtra("vehículo", false)) {
            openCarInfo();
        } else if (getIntent().getBooleanExtra("version", false)) {
            openCarInfo();
        }


    }

    private void initServices() {
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        tprService = CatalogoInteractorsFactory.getTprService();

        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        polizaInteractor = OrquestadorInteractorFactory.getPolizaInteractor();
    }


    private void initListenersAuto() {
        spVehiculoDatos.get(0).setOnItemSelectedListener(this);
        spVehiculoDatos.get(1).setOnItemSelectedListener(this);
        spVehiculoDatos.get(2).setOnItemSelectedListener(this);
        spVehiculoDatos.get(3).setOnItemSelectedListener(this);
        spVehiculoDatos.get(4).setOnItemSelectedListener(this);
    }


    private void getAjuste() {
        getCompositeDisposable().add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(ajuste -> {
                    this.ajuste = ajuste;
                }, throwable -> {
                    Log.e(TAG, "Error al descargar el ajuste", throwable);
                }));
    }

    private Completable getCatalogosPersona() {
        return Completable.mergeArrayDelayError(
                downloadIdentificaciones(),
                downloadNivelLesion(),
                downloadEstados(),
                downloadSupervisorias());
    }


    private Completable getCatalogosVehiculo() {
        return Completable.mergeArrayDelayError(
                downloadTypeVehicle(),
                downloadImpactos(),
                downloadColors());

    }

    private void getDatosVehiculo() {
        getCompositeDisposable().add(getCatalogosVehiculo()
                .andThen(getVehiculoAsegurado())
                .subscribe(this::setVehiculoAsegurado,
                        throwable -> new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable)));

    }


    //DESCARGA EL TIPO DE IMPACTOS
    private Completable downloadImpactos() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getImpactosVehiculo()
                    .subscribe(impactosList -> {
                        impactos = impactosList;
                        setImpactos();
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spPrimerImpacto.setAdapter(null);
                        // Crashlytics.log(Log.DEBUG, "getImpacts", throwable.getMessage());
                        Log.e(TAG, "Error al descargar impactos de vehículos", throwable);
                    })
            );
        });


    }


    //DESCARGA EL LISTADO DE TIPO DE VEHICULOS
    private Completable downloadTypeVehicle() {
        return Completable.create(e -> {
            getCompositeDisposable().add(tprService.getTiposVehiculo()
                    .subscribe(vehiculosList -> {
                        tipoVehiculos = vehiculosList;
                        if (!tipoVehiculos.isEmpty()) {
                            adapterVehiculo = new AdapterSpinnerGeneral<>(getApplicationContext()
                                    , R.layout.sp_little_item, tipoVehiculos, CatalogoClave::getNombre);
                            spVehiculoDatos.get(0).setAdapter(adapterVehiculo);
                            if (foundVehiculo != null && foundVehiculo.getTipoVehiculo() != null) {
                                spVehiculoDatos.get(0).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(0), foundVehiculo.getTipoVehiculo()));
                            }
                        }
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spVehiculoDatos.get(0).setAdapter(null);
                        Log.e(TAG, "Error al descargar tipos de vehículos", throwable);
                    })
            );

        });


    }


    // DESCARGA EL LISTADO DE LA ARMADORA CON RESPECTO AL TIPO DE AUTO
    private void downloadArmadora(String tipoAuto) {
        Action sinDatosArmadora = () -> {
            for (int i = 1; i < spVehiculoDatos.size(); i++) {
                spVehiculoDatos.get(i).setAdapter(null);
            }

            armadorasList = new ArrayList<>();
            carroceriaList = new ArrayList<>();
            modelos = new ArrayList<>();
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getArmadoras(tipoAuto)
                .subscribe(armadoras -> {
                    armadorasList = armadoras;
                    if (!armadorasList.isEmpty()) {
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, armadorasList, CatalogoClave::getNombre);
                        spVehiculoDatos.get(1).setAdapter(adapter);
                        spVehiculoDatos.get(1).setVisibility(View.VISIBLE);
                        if (foundVehiculo != null && foundVehiculo.getArmadora() != null) {
                            spVehiculoDatos.get(1).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(1), foundVehiculo.getArmadora()));
                        }
                    } else {
                        sinDatosArmadora.run();
                    }
                }, throwable -> {
                    sinDatosArmadora.run();
                    Log.e(TAG, "Error al descargar armadoras", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);

                })
        );


    }


    //DESCARGA DE CARROCERIA
    private void downloadCarroceria(String tipoVehiculo, String tipoArmadora) {
        Action sinDatosCarroceria = () -> {
            for (int i = 2; i < spVehiculoDatos.size(); i++) {
                spVehiculoDatos.get(i).setAdapter(null);
            }
            carroceriaList = new ArrayList<>();
            modelos = new ArrayList<>();
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getCarrocerias(tipoVehiculo, tipoArmadora)
                .subscribe(carroceria -> {
                    carroceriaList = carroceria;
                    if (!carroceriaList.isEmpty()) {
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, carroceriaList, CatalogoClave::getNombre);
                        spVehiculoDatos.get(2).setAdapter(adapter);
                        spVehiculoDatos.get(2).setVisibility(View.VISIBLE);
                        if (foundVehiculo != null && foundVehiculo.getCarroceria() != null) {
                            spVehiculoDatos.get(2).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(2), foundVehiculo.getCarroceria()));
                        }
                    } else {
                        sinDatosCarroceria.run();
                    }
                }, throwable -> {
                    sinDatosCarroceria.run();
                    Log.e(TAG, "Error al descargar carrocerías", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                })
        );


    }


    // DESCARGA DE TIPO DE MODELOS
    private void downloadModelos(String tipoVehiculo, String tipoArmadora, String tipoCarroceria) {
        Action sinDatosModelos = () -> {
            spVehiculoDatos.get(3).setAdapter(null);
            spVehiculoDatos.get(4).setAdapter(null);
            modelos = new ArrayList<>();
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getModelos(tipoVehiculo, tipoArmadora, tipoCarroceria)
                .subscribe(modelosList -> {
                    modelos = modelosList;
                    if (!modelos.isEmpty()) {
                        AdapterSpinnerGeneral<Short> modeloAdapter = new AdapterSpinnerGeneral<>(AseguradoActivity.this
                                , R.layout.sp_little_item, modelos, Object::toString);
                        spVehiculoDatos.get(3).setAdapter(modeloAdapter);
                        spVehiculoDatos.get(3).setVisibility(View.VISIBLE);
                        if (foundVehiculo != null && foundVehiculo.getModelo() != null) {
                            spVehiculoDatos.get(3).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(3), foundVehiculo.getModelo()));
                        }
                    } else {
                        sinDatosModelos.run();
                    }
                }, throwable -> {
                    sinDatosModelos.run();
                    Log.e(TAG, "Error al descargar modelos de vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                })
        );
    }


    // DESCARGA DE TIPO DE VERSIONES
    private void downloadVersiones(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int yearModelo) {
        Action sinDatosVersiones = () -> {
            spVehiculoDatos.get(4).setAdapter(null);
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getVersiones(tipoVehiculo, tipoArmadora, tipoCarroceria, yearModelo)
                .subscribe(versions -> {
                    versiones = versions;
                    if (!versiones.isEmpty()) {
                        AdapterSpinnerGeneral<VersionVehiculoResumen> versionAdapter = new AdapterSpinnerGeneral<>(AseguradoActivity.this
                                , R.layout.sp_little_item, versiones, VersionVehiculoResumen::getNombre);
                        spVehiculoDatos.get(4).setAdapter(versionAdapter);
                        spVehiculoDatos.get(4).setVisibility(View.VISIBLE);
                        if (foundVehiculo != null && foundVehiculo.getVersion() != null) {
                            if (!isExistVersion(versiones)) {
                                spVehiculoDatos.get(4).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                                spVehiculoDatos.get(4).setEnabled(true);
                            } else {
                                spVehiculoDatos.get(4).getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                                spVehiculoDatos.get(4).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(4), foundVehiculo.getVersion()));
                            }
                        }

                    } else {
                        sinDatosVersiones.run();

                    }
                }, throwable -> {
                    sinDatosVersiones.run();
                    Log.e(TAG, "Error al descargar versiones de vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                })
        );
    }

    // DESCARGA DE CATEGORIA
    private void downloadCategoria(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int modelo, String version) {
        getCompositeDisposable().add(tprService.getVersion(tipoVehiculo, tipoArmadora, tipoCarroceria, modelo, version)
                .subscribe(categoriaRx -> {
                    categoria = categoriaRx;
                    if (generalDamageFragment != null && !generalDamageFragment.getDataList().isEmpty()) {
                        generalDamageFragment.getDataList().clear();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                })
        );

    }

    private Completable downloadColors() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getColoresVehiculo()
                    .subscribe(colores -> {
                        coloresList = colores;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, coloresList, CatalogoId::getNombre);
                        spColor.setAdapter(adapter);

                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spColor.setAdapter(null);
                        Log.e(TAG, "Error al descargar los colores", throwable);
                    }));

        });
    }

    private void setEvents() {
        firstName.addTextChangedListener(this);
        lastName.addTextChangedListener(this);
        motherLastName.addTextChangedListener(this);
    }

    private boolean validFormConductor() {
        boolean isRequestFocus = false;
        if (firstName.getText().toString().trim().length() <= 0) {
            firstName.setError(getString(R.string.mandatory_field));
            firstName.requestFocus();
            isRequestFocus = true;
        }
        if (lastName.getText().toString().trim().length() <= 0) {
            lastName.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                lastName.requestFocus();
                isRequestFocus = true;
            }
        }

        if (dateNacimiento.getText().toString().trim().length() <= 0) {
            dateNacimiento.setError(getString(R.string.mandatory_field));
            dateNacimiento.requestFocus();
            if (!isRequestFocus) {
                isRequestFocus = true;
            }
        }
        if (!identificaciones.isEmpty() && identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
            if (!checkBoxPermanente.isChecked()) {
                if (dateVigencia.getText().toString().trim().length() <= 0) {
                    dateVigencia.setError(getString(R.string.mandatory_field));
                    dateVigencia.requestFocus();
                    if (!isRequestFocus) {
                        isRequestFocus = true;
                    }
                }
            }
        }
        if (etNumeroIdentificacion.getText().toString().trim().length() <= 0) {
            etNumeroIdentificacion.setError(getString(R.string.mandatory_field));
            etNumeroIdentificacion.requestFocus();
            if (!isRequestFocus) {
                isRequestFocus = true;
            }
        }
        if (sexos.getCheckedRadioButtonId() == -1) {
            isRequestFocus = true;
            ToastUtils.toastShort("Debe seleccionar un género por favor");
        }
        if (adapterPhone.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(this, ResourceUtils.getString("telefono_empty"), Toast.LENGTH_LONG).show();
        }
        if (adapterEmail.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(this, ResourceUtils.getString("correo_empty"), Toast.LENGTH_LONG).show();
        }
        if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
            if (generalInjuredFragment.getDataList().isEmpty()) {
                isRequestFocus = true;
                Toast.makeText(this, ResourceUtils.getString("lesion_empty"), Toast.LENGTH_LONG).show();
            }
        }
        return !isRequestFocus;
    }


    // IICIALIZACION DE FRAGMENTOS
    private void initializeFragments() {
        generalCameraFragmentDriver = new GeneralCameraFragment();
        generalCameraFragmentVehicle = new GeneralCameraFragment();
        generalInjuredFragment = new GeneralInjuredFragment();
        generalDamageFragment = new GeneralDamageFragment();
        generalDamageFragment.setDiscriminator("dano");
        generalDamageFragmentPreexisting = new GeneralDamageFragment();
        generalDamageFragmentPreexisting.setDiscriminator("pree");
        addFragmentDriver(generalCameraFragmentDriver, Constants.TAG_INSURE_DRIVER);
        addFragmentVehicle(generalCameraFragmentVehicle, Constants.TAG_INSURE_VEHICLE);
        addFragmentDamgeParts(generalDamageFragment);
        addFragmentDamgePartsPreexisting(generalDamageFragmentPreexisting);
        addFragmentDamgePartsDriver(generalInjuredFragment);

    }


    private void getConductor() {
        getCompositeDisposable().add(aseguradoInteractor.getConductor(idAjuste)
                .subscribe(aseguradoConductor -> {
                    conductorAsegurado = aseguradoConductor;
                    setConductorAsegurado();
                }, throwable -> {
                    isUploadedP = false;
                    Log.e(TAG, "Error al descargar datos del conductor del ajuste: " + idAjuste, throwable);
                    if (!(throwable instanceof ResourceNotFoundException))
                        new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                })
        );

    }

    // CARGA DE DATOS DE ASEGURADO CONDUCTOR
    private void setConductorAsegurado() {

        Log.d(TAG, "Asignando valorea a conductor");
        moduleDriver = new ModuleDriver((idAjuste + MODULO_ASEGURADOS + TIPO_CONDUCTOR), ASEGURADOS, CONDUCTOR, "", idAjuste);
        path = getImagePathDriver(moduleDriver);
        generalCameraFragmentDriver.setFolder(MODULO_ASEGURADOS, TIPO_CONDUCTOR, 0, "", "");
        PersistenciaPath.showMessageSizeImages(getBaseActivity(), getAllImages(getBaseActivity(), path));
        Log.d(TAG, "path imagenes condcutor :" + path);
        if (path != null) {
            filesPhotos.clear();
            filesPhotos.addAll(getAllImages(getBaseActivity(), path));
            generalCameraFragmentDriver.setImageList(filesPhotos);
        }

        if (conductorAsegurado.getAutorizaTratamiento() != null)
            setAutoriza(conductorAsegurado.getAutorizaTratamiento());

        firstName.setText(conductorAsegurado.getNombre() != null ? conductorAsegurado.getNombre() : "");
        lastName.setText(conductorAsegurado.getApellidoPaterno() != null ? conductorAsegurado.getApellidoPaterno() : "");
        motherLastName.setText(conductorAsegurado.getApellidoMaterno() != null ? conductorAsegurado.getApellidoMaterno() : "");

        if (!conductorAsegurado.getCorreos().isEmpty()) {
            adapterEmail.addAll(conductorAsegurado.getCorreos());
            recyclerEmail.setAdapter(adapterEmail);
            adapterEmail.notifyDataSetChanged();
        }
        if (conductorAsegurado.getId() != 0) id_conductor = conductorAsegurado.getId();

        if (!conductorAsegurado.getTelefonos().isEmpty()) {
            list.clear();
            list.addAll(conductorAsegurado.getTelefonos());
            adapterPhone.addAll(list);
            recyclerPhone.setAdapter(adapterPhone);
            list = conductorAsegurado.getTelefonos();
            adapterPhone.notifyDataSetChanged();
        }
        if (conductorAsegurado.getFechaNacimiento() != null) {
            dateNacimiento.setText(DateUtils.date(conductorAsegurado.getFechaNacimiento()));
            int years = Years
                    .yearsBetween(LocalDate.fromDateFields(conductorAsegurado.getFechaNacimiento()), LocalDate.now())
                    .getYears();
            edad.setText(Integer.toString(years));
            isUploadedP = true;
        }
        etNumeroIdentificacion.setText(conductorAsegurado.getNumeroIdentificacion() != null ? conductorAsegurado.getNumeroIdentificacion() : "");
        setSexo(conductorAsegurado);
        if (conductorAsegurado.getCondicion() != null) {
            if (conductorAsegurado.getCondicion().equals(CondicionPersona.LESIONADO)) {
                titulo_lesion.setVisibility(View.VISIBLE);
                lesionado_container.setVisibility(View.VISIBLE);
                spinnerNivelLesion.setVisibility(View.VISIBLE);

                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, conductorAsegurado.getCondicion().name()));
                if (conductorAsegurado.getNivelLesion() != null && conductorAsegurado.getNivelLesion() != 0) {
                    spinnerNivelLesion.setSelection(SpinnerUtils.getPosition(spinnerNivelLesion, conductorAsegurado.getNivelLesion()));
                }
                statusLesionado = true;
                if (conductorAsegurado.getRequiereAmbulancia()) {
                    cbAmbulancia.setChecked(conductorAsegurado.getRequiereAmbulancia());
                }

                if (conductorAsegurado.getOtroHospital() != null) {
                    cbOtro.setChecked(true);
                    edtOtroHospital.setText(conductorAsegurado.getOtroHospital());
                    otro_container.setVisibility(View.VISIBLE);
                    contentTitleHospital.setVisibility(View.INVISIBLE);
                    otro_hospital.setVisibility(View.VISIBLE);
                    edtOtroHospital.setVisibility(View.VISIBLE);
                    findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                    findViewById(R.id.spinnerHospital).setVisibility(GONE);
                    edtOtroHospital.clearFocus();
                } else {
                    //getSupervisoria(Integer.toString(response.body().getIdHospital()));
                    findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                    findViewById(R.id.spinnerHospital).setVisibility(View.VISIBLE);
                    cbOtro.setChecked(false);
                }
                if (conductorAsegurado.getHospital() != null) {
                    getSupervisoria(conductorAsegurado.getHospital());
                } else {
                    findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                    findViewById(R.id.spinnerHospital).setVisibility(GONE);
                }
            } else {
                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, conductorAsegurado.getCondicion().name()));
            }

        }
        if (!conductorAsegurado.getFotografias().isEmpty()) {
            getUpdatePhotosDriver();
            uploadImage.setVisibility(View.VISIBLE);
            generalCameraFragmentDriver.setCallBackItem(callbackDeleteImageDriver);
        } else {
            uploadImage.setVisibility(View.VISIBLE);

        }
        if (conductorAsegurado.getTipoIdentificacion() != null) {
            spinnerIdentificacion.setSelection(SpinnerUtils.getPosition(spinnerIdentificacion, conductorAsegurado.getTipoIdentificacion()));

            for (int i = 0; i < identificaciones.size(); i++) {
                if (conductorAsegurado.getTipoIdentificacion().equals(identificaciones.get(i).getClave())) {
                    spinnerIdentificacion.setSelection(i);
                    break;
                }
            }
            if (identificaciones != null && identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
                findViewById(R.id.content_tipo_licencia).setVisibility(View.VISIBLE);
                findViewById(R.id.content_title_tipo_licencia).setVisibility(View.VISIBLE);
                findViewById(R.id.content_fecha_vigencia).setVisibility(View.VISIBLE);
                findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
                findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
                findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
                findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
            }

        }
        if (conductorAsegurado.getEstadoLicencia() != null) {
            spinnerEstadoEmisor.setSelection(SpinnerUtils.getPosition(spinnerEstadoEmisor,conductorAsegurado.getEstadoLicencia()));
            downloadTiposLicencia(conductorAsegurado.getEstadoLicencia());
        }
        if (conductorAsegurado.getLicenciaPermanente() != null) {
            checkBoxPermanente.setChecked(conductorAsegurado.getLicenciaPermanente());
        }
        if (conductorAsegurado.getTipoLicencia() != null) {
            spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, conductorAsegurado.getTipoLicencia()));
        }
        dateVigencia.setText(conductorAsegurado.getFechaFinLicencia() != null ? DateUtils.date(conductorAsegurado.getFechaFinLicencia()) : "");
        if (!conductorAsegurado.getLesiones().isEmpty()) {
            generalInjuredFragment.loadDataList(conductorAsegurado.getLesiones());
        }
        btnGuardarConductor.setVisibility(View.VISIBLE);
    }


    private void getMoney(String categoria, List<Long> partes) {
        if (categoria != null) {
            if (partes.size() != 0) {
                getCompositeDisposable().add(polizaInteractor.getCostosTercero(categoria, partes)
                        .subscribe(costoResponse -> {
                            String currency = numberFormat.format(costoResponse.getCosto());
                            etEstimadoDano.setText(currency);

                        }, throwable -> {
                            Log.e(TAG, "Error al descargar costos de partes dañadas de tercero vehiculo", throwable);
                            new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                        }));

            } else {
                etEstimadoDano.setText("$" + 0.00);
            }
        } else {
            etEstimadoDano.setText("$" + 0.00);
        }
    }


    private Completable getVehiculoAsegurado() {
        return Completable.create(e -> {
            Action sinDatosVehiculo = () -> {
                enabledSpinners(true);
                vehiculo_datos.get(2).setEnabled(true);
            };
            getCompositeDisposable().add(aseguradoInteractor.getVehiculo(idAjuste)
                    .subscribe(aseguradoVehiculo -> {
                        foundVehiculo = aseguradoVehiculo;
                        Log.d(TAG, "onResponse: " + aseguradoVehiculo);
                        e.onComplete();
                    }, throwable -> {
                        sinDatosVehiculo.run();
                        isUploadedV = false;
                        Log.e(TAG, "Error al descargar los datos del vehículo del asegurado del ajuste: " + idAjuste, throwable);
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }
                    }));

        });


    }

    private void downloadTiposLicencia(String estado) {
        getCompositeDisposable().add(siniestrosInteractor.getTiposLicencia(estado)
                .subscribe(tiposLicencia -> {
                    tiposLicenciaList = tiposLicencia;
                    AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                            , R.layout.sp_little_item, tiposLicenciaList, CatalogoClave::getNombre);
                    spinnerTipoLicencia.setAdapter(adapter);
                    if (conductorAsegurado != null && conductorAsegurado.getTipoLicencia() != null) {
                        spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, conductorAsegurado.getTipoLicencia()));
                    }
                }, throwable -> {
                    spinnerTipoLicencia.setAdapter(null);
                    Log.e(TAG, "Error al descargar los colores", throwable);
                }));

    }

    private void setVehiculoAsegurado() {
        Log.d(TAG, "obteniendo Datos del vehiculo:");
        ModuleCar moduleCar = new ModuleCar((idAjuste + MODULO_ASEGURADOS + TIPO_VEHICULO), ASEGURADOS, VEHICULO, "", idAjuste);
        path = getImagePathCar(moduleCar);
        generalCameraFragmentVehicle.setFolder(MODULO_ASEGURADOS, TIPO_VEHICULO, 0, "", "");
        PersistenciaPath.showMessageSizeImages(getBaseActivity(), getAllImages(getBaseActivity(), path));
        Log.d(TAG, "obteniendo path de imagenes de vehiculo: " + path);
        if (path != null) {
            filesPhotosVehiculo.clear();
            filesPhotosVehiculo.addAll(getAllImages(getBaseActivity(), path));
            generalCameraFragmentDriver.setImageList(filesPhotosVehiculo);
        }
        if (foundVehiculo != null) {
            if (foundVehiculo.getTipoVehiculo() != null) {
                spVehiculoDatos.get(0).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(0), foundVehiculo.getTipoVehiculo()));
            } else {
                enabledSpinners(true);
                tvTipo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AseguradoActivity.this, R.drawable.ic_error_text), null);
            }
            if (foundVehiculo.getArmadora() != null) {
                spVehiculoDatos.get(1).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(1), foundVehiculo.getArmadora()));
            } else {
                spVehiculoDatos.get(1).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(1).setEnabled(true);

                spVehiculoDatos.get(2).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(2).setEnabled(true);

                spVehiculoDatos.get(3).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(3).setEnabled(true);

                spVehiculoDatos.get(4).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(4).setEnabled(true);

                tvArmadora.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AseguradoActivity.this, R.drawable.ic_error_text), null);
            }
            if (foundVehiculo.getCarroceria() != null) {
                spVehiculoDatos.get(2).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(2), foundVehiculo.getCarroceria()));
            } else {
                spVehiculoDatos.get(2).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(2).setEnabled(true);
                spVehiculoDatos.get(3).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(3).setEnabled(true);
                spVehiculoDatos.get(4).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(4).setEnabled(true);
                tvModelo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AseguradoActivity.this, R.drawable.ic_error_text), null);
            }
            if (foundVehiculo.getModelo() != null) {
                spVehiculoDatos.get(3).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(3), foundVehiculo.getModelo()));
            } else {
                tvModelo.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AseguradoActivity.this, R.drawable.ic_error_text), null);
                spVehiculoDatos.get(3).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(3).setEnabled(true);
                spVehiculoDatos.get(4).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(4).setEnabled(true);
            }
            if (foundVehiculo.getVersion() != null) {
                spVehiculoDatos.get(4).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(4), foundVehiculo.getVersion()));
            } else {
                spVehiculoDatos.get(4).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(4).setEnabled(true);
                tvVersion.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(AseguradoActivity.this, R.drawable.ic_error_text), null);
            }
            if (foundVehiculo.getPlacas() != null) {
                vehiculo_datos.get(0).setText(foundVehiculo.getPlacas());
            }
            if (foundVehiculo.getNumeroMotor() != null) {
                vehiculo_datos.get(1).setText(foundVehiculo.getNumeroMotor());
            }
            if (foundVehiculo.getVin() != null) {
                vehiculo_datos.get(2).setText(foundVehiculo.getVin());
            }

            if (foundVehiculo.getColor() != null) {
                spColor.setSelection(SpinnerUtils.getPosition(spColor, foundVehiculo.getColor()));
            }
            if (foundVehiculo.getObservacion() != null) {
                etObservaciones.setText(foundVehiculo.getObservacion());
            }
            if (ajuste != null && ajuste.getSiniestro() != null &&
                    ajuste.getSiniestro().getIdPresiniestro() != null
                    && ajuste.getSiniestro().getId() == null) {
                enabledSpinners(true);
                vehiculo_datos.get(2).setEnabled(false);
            }

            if (foundVehiculo.getDanoConsecuencia() != null) {
                isUploadedV = true;
                if (!foundVehiculo.getDanoConsecuencia()) {
                    checkBoxConsequence.setChecked(true);
                    daño_container.setVisibility(View.GONE);
                } else {
                    checkBoxConsequence.setChecked(false);
                    daño_container.setVisibility(View.VISIBLE);
                }
            }

            if (foundVehiculo.getPerdidaTotalEvidente() != null) {
                isUploadedV = true;
                if (foundVehiculo.getPerdidaTotalEvidente()) {
                    cbPerdida.setChecked(true);
                    daño_container.setVisibility(View.GONE);
                } else {
                    if (!foundVehiculo.getDanoConsecuencia()) {
                        cbPerdida.setChecked(false);
                        daño_container.setVisibility(View.GONE);
                    } else {
                        cbPerdida.setChecked(false);
                        daño_container.setVisibility(View.VISIBLE);
                    }
                }
            }
            if (!foundVehiculo.getFotografias().isEmpty()) {
                getUpdatePhotosVehicle();
                generalCameraFragmentVehicle.setCallBackItem(callbackDeleteImageVehicle);
            } else {
                generalCameraFragmentVehicle.setImageList(filesPhotosVehiculo);
                generalCameraFragmentVehicle.setCallBackItem(callbackDeleteImageVehicle);
            }

            if (foundVehiculo.getVersion() != null) {

                if (!foundVehiculo.getDanos().isEmpty()) {
                    generalDamageFragment.loadDataList(getListIntDanos(foundVehiculo.getDanos()));
                }
                if (!foundVehiculo.getDanosPreexistentes().isEmpty()) {
                    generalDamageFragmentPreexisting.loadDataList(getListIntDanos(foundVehiculo.getDanosPreexistentes()));

                }

            }

            if (foundVehiculo.getPrimerImpacto() != null) {
                spPrimerImpacto.setSelection(SpinnerUtils.getPosition(spPrimerImpacto, foundVehiculo.getPrimerImpacto()));

            }
            if (foundVehiculo.getDanoEstimado() != null) {
                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));
                String currency = format.format(foundVehiculo.getDanoEstimado());
                etEstimadoDano.setText(currency);
            }

            if (categoria != null) {
                getMoney(categoria.getCategoria(), generalDamageFragment.getDataListPartes());
            }
        }
    }

    private boolean isExistVersion(List<VersionVehiculoResumen> versiones) {
        boolean isExistVersion = false;
        for (VersionVehiculoResumen v : versiones) {
            if (v.getVersion().equals(foundVehiculo.getVersion())) {
                isExistVersion = true;
            }
        }

        return isExistVersion;

    }


    private ArrayList<Long> getListIntDanos(List<PiezaVehiculo> piezasAfectadas) {
        ArrayList<Long> result = new ArrayList<>();
        for (PiezaVehiculo pieza : piezasAfectadas) {
            if (pieza.getParteAfectada() != null)
                result.add(pieza.getParteAfectada());
        }
        return result;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean checkPermissions(int permission) {
        boolean Resp = false;
        switch (permission) {
            case REQUEST_CAMERA:
                if (checkSelfPermission(
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    Resp = true;
                    Log.d("camera permison", "true");
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.CAMERA)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Ajustador Móvil necesita acceder a la memoria interna para almacenar las fotos. " +
                                "Vaya a ajustes y otorgue los permisos necesarios");
                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                            }
                        });
                        builder.setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                        builder.show();

                    } else {
                        requestPermissions(
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CAMERA);

                    }
                } else Resp = true;
                break;
            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    Log.d("write permison", "true");
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.d("write", "false");
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Ajustador Móvil necesita acceder a la memoria interna para almacenar las fotos. " +
                                "Vaya a ajustes y otorgue los permisos necesarios");
                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
                            }
                        });
                        builder.setNegativeButton("Cancelar", (dialog, id) -> {
                        });
                        builder.show();

                    } else {
                        requestPermissions(
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQUEST_WRITE_EXTERNAL_STORAGE);

                    }
                } else Resp = true;
                break;
            default:
                break;
        }
        return Resp;
    }


    private void enabledSpinners(boolean isActive) {
        for (int i = 0; i < spVehiculoDatos.size(); i++) {
            if (!isActive) {
                spVehiculoDatos.get(i).setEnabled(isActive);
                spVehiculoDatos.get(i).getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            } else {
                spVehiculoDatos.get(i).getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
                spVehiculoDatos.get(i).setEnabled(isActive);
            }
        }
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void reloadData(String load) {
        if (load.equals("load")) {
            ocupantesAdapter.clear();
            recyclerOcupantes.setAdapter(ocupantesAdapter);
            ocupantesAdapter.notifyDataSetChanged();
            downloadOcupantes();
        }
    }

    public void deleteDriverImage(long foto) {
        getCompositeDisposable().add(aseguradoInteractor.deleteFotografiaConductor(idAjuste, foto)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, "Se eliminó correctamente la fotografía del asegurado");
                }, throwable -> {
                    Log.e(TAG, "Error al eliminar las fotografías del conductor del ajuste " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                })
        );

    }

    public void deleteVehiclePhoto(long foto) {
        getCompositeDisposable().add(aseguradoInteractor.deleteFotografiaVehiculo(idAjuste, foto)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, "Se eliminó correctamente la fotografía del vehículo");
                }, throwable -> {
                    Log.e(TAG, "Error al borrar las fotografías del vehículo del asegurado del ajuste" + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                }));

    }


    private void downloadPhones() {
        getCompositeDisposable().add(siniestrosInteractor.getTiposTelefono()
                .subscribe(tiposTelefonos -> {
                    this.tiposTelefonos = tiposTelefonos;
                    showDialogAddPhone();
                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(this, throwable);
                    Log.e(TAG, "Error al obtener los tipos de telefonos", throwable);
                }));

    }


    private Completable downloadSupervisorias() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getSupervisorias()
                    .subscribe(supervisioriaList -> {
                        supervisorias = supervisioriaList;
                        if (!supervisorias.isEmpty()) {
                            AdapterSpinnerGeneral<Supervisoria> adapterStates = new AdapterSpinnerGeneral<>(AseguradoActivity.this
                                    , R.layout.spinner_item, supervisorias, Supervisoria::getNombre);
                            spinnerEstadoAmbulancia.setAdapter(adapterStates);
                            spinnerEstadoAmbulancia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    downloadHospitales(supervisorias.get(spinnerEstadoAmbulancia.getSelectedItemPosition()).getClave());
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        }
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerEstadoAmbulancia.setAdapter(null);

                        Log.e(TAG, "Error al descargar supervisorías", throwable);
                    })
            );
        });


    }


    @OnClick(R.id.btnAgregarOcupante)
    public void agregarOcupante() {
        generalCameraFragmentVehicle.setFolder(MODULO_ASEGURADOS, TIPO_OCUPANTE, 0, "", "");
        Intent intent = new Intent(this, AseguradoOcupanteActivity.class);
        intent.putExtra("idAjuste", idAjuste);
        String aData = new Gson().toJson(ajuste);
        intent.putExtra("data2", aData);
        intent.putExtra("discriminator", "nuevo");
        startActivityForResult(intent, AGREGAR_OCUPANTE);
    }


    @OnClick(R.id.btnSolicitarAmbulancia)
    public void callAmbulancia() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(layout, "Necesitas brindar el permiso para realizar llamadas",
                            Snackbar.LENGTH_LONG).setAction("Aceptar", new View.OnClickListener() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                call();
            }
        } else {
            call();
        }

    }

    @SuppressLint("MissingPermission")
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Utils.NUMBER_AMBULANCE));
        startActivity(intent);
    }

    @OnClick(R.id.btnuploadImages)
    public void uploadImagesDriver() {
        ImageUtils imageUtils = new ImageUtils();
        List<ImageGeneralModel> imageList = generalCameraFragmentDriver.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        if (!images.isEmpty()) {
            int size = images.size();
            uploadImage.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            getCompositeDisposable().add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> aseguradoInteractor.addFotografiaConductor(idAjuste, file).toObservable(), false, 1)
                    .doOnEach(fotografia -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    }
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir fotografía", throwable);
                        return new Fotografia();
                    }).toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        uploadImage.setEnabled(true);
                        boolean ok = true;
                        for (Fotografia pic : fotografias) {
                            ok &= pic.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, getResources().getString(R.string.imagenes_upload_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(AseguradoActivity.this, getResources().getString(R.string.imagenes_upload_error));
                        }
                        Log.d(TAG, "renomabrando imagenes de carpeta de conductor: " + images.size());
                        imageUtils.renameImageUpload(images);
                        Log.d(TAG, "Eliminado imagenes de carpeta de conductor: " + images.size());
                        imageUtils.deleteImage(getBaseActivity(), images);
                        getUpdatePhotosDriver();
                    }, throwable -> {
                        uploadImage.setEnabled(true);
                        btnGuardarConductor.setEnabled(true);
                        Log.e(TAG, "Error al subir fotografías del conductor del ajuste " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                    }));
        }
    }


    private void getUpdatePhotosDriver() {
        getCompositeDisposable().add(aseguradoInteractor.getFotografiasConductor(idAjuste)
                .subscribe(filesPhotosLis -> {

                    for (Fotografia photo : filesPhotosLis) {
                        filesPhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                        Log.d(TAG, "obteniendo imagenes de servicio conductor:" + photo.getUrl());
                    }
                    generalCameraFragmentDriver.updateImageList(filesPhotos);
                    btnGuardarConductor.setEnabled(true);
                    uploadImage.setEnabled(true);
                    filesPhotos.clear();

                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                    uploadImage.setEnabled(true);
                    btnGuardarConductor.setEnabled(true);
                }));
    }

    @OnClick(R.id.btnuploadImages2)
    public void uploadImagesVehicle() {

        ImageUtils imageUtils = new ImageUtils();
        List<ImageGeneralModel> imageList = generalCameraFragmentVehicle.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        if (!images.isEmpty()) {
            int size = images.size();
            uploadImageVehicle.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            getCompositeDisposable().add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> aseguradoInteractor.addFotografiaVehiculo(idAjuste, file), false, 1)
                    .doOnEach(fotografias -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        }
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir fotografía del vehículo asegurado", throwable);
                        return new Fotografia();
                    }).toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        boolean ok = true;
                        for (Fotografia pic : fotografias) {
                            ok &= pic.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, getResources().getString(R.string.imagenes_upload_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(AseguradoActivity.this, getResources().getString(R.string.imagenes_upload_error));
                        }
                        uploadImageVehicle.setEnabled(true);
                        new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, getResources().getString(R.string.imagenes_upload_ok));
                        Log.d(TAG, "renomabrando imagenes de carpeta de vehiculo: " + images.size());
                        imageUtils.renameImageUpload(images);
                        Log.d(TAG, "Eliminado imagenes de carpeta de vehiculo: " + images.size());
                        imageUtils.deleteImage(getBaseActivity(), images);
                        getUpdatePhotosVehicle();
                    }, throwable -> {
                        uploadImageVehicle.setEnabled(true);
                        Log.e(TAG, "Error al descargar tipos de vehículos", throwable);
                        new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                    }));
        } else {
            uploadImageVehicle.setEnabled(true);
            new GeneralSnackBar().largeSnackError(AseguradoActivity.this, getString(R.string.image_void));
            uploadImage.setEnabled(true);
        }
    }


    private void getUpdatePhotosVehicle() {
        getCompositeDisposable().add(aseguradoInteractor.getFotografiasVehiculo(idAjuste)
                .subscribe(filesPhotosList -> {
                    for (Fotografia photo : filesPhotosList) {
                        filesPhotosVehiculo.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                        Log.d(TAG, "obteniendo imagenes de servicio vehiculo:" + photo.getUrl());

                    }
                    generalCameraFragmentVehicle.updateImageList(filesPhotosVehiculo);
                    btnGuardarVehiculo.setEnabled(true);
                    uploadImage.setEnabled(true);
                    filesPhotosVehiculo.clear();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener las fotografías del vehículo asegurado del ajuste" + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                    btnGuardarVehiculo.setEnabled(true);
                }));
    }

    private void deletePhone(int position) {
        MaterialDialog deletePhoneBuilder = new MaterialDialog.Builder(this)
                .title("Eliminar número...")
                .content("¿Desea eliminar este número?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapterPhone.remove(adapterPhone.getTelono(position));
                    list.remove(position);
                    recyclerPhone.setAdapter(adapterPhone);
                    adapterPhone.notifyDataSetChanged();
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        MDButton aceptar = deletePhoneBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = deletePhoneBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(deletePhoneBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        deletePhoneBuilder.show();
    }

    private void deleteEmail(int position) {
        MaterialDialog deleteMailBuilder = new MaterialDialog.Builder(this)
                .title("Eliminar correo...")
                .content("¿Desea eliminar este correo?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapterEmail.remove(adapterEmail.getCorreo(position));
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        MDButton aceptar = deleteMailBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = deleteMailBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(deleteMailBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        deleteMailBuilder.show();
    }

    private void initListeners() {

        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
                    titulo_lesion.setVisibility(View.VISIBLE);
                    lesionado_container.setVisibility(View.VISIBLE);
                    spinnerNivelLesion.setVisibility(View.VISIBLE);
                    contentHospital.setVisibility(View.VISIBLE);
                    contentTitleHospital.setVisibility(View.VISIBLE);
                    spinnerHospital.setVisibility(View.VISIBLE);
                    cbOtro.setVisibility(View.VISIBLE);
                    otro_container.setVisibility(View.VISIBLE);
                    solicitarAmbulancia.setVisibility(View.VISIBLE);
                    statusLesionado = true;
                } else {
                    titulo_lesion.setVisibility(GONE);
                    lesionado_container.setVisibility(GONE);
                    spinnerNivelLesion.setVisibility(GONE);
                    contentHospital.setVisibility(View.GONE);
                    otro_container.setVisibility(View.INVISIBLE);
                    contentTitleHospital.setVisibility(View.INVISIBLE);
                    cbOtro.setVisibility(View.INVISIBLE);
                    solicitarAmbulancia.setVisibility(View.GONE);
                    spinnerHospital.setVisibility(View.GONE);
                    statusLesionado = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        edad.setFocusable(false);

        cbOtro.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            if (isChecked) {
                otro_hospital.setVisibility(View.VISIBLE);
                edtOtroHospital.setVisibility(View.VISIBLE);
                edtOtroHospital.requestFocus();
                findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                findViewById(R.id.spinnerHospital).setVisibility(GONE);
                findViewById(R.id.title_otros).setVisibility(View.VISIBLE);
            } else {
                otro_hospital.setVisibility(GONE);
                findViewById(R.id.title_otros).setVisibility(GONE);
                edtOtroHospital.setVisibility(View.INVISIBLE);
                edtOtroHospital.clearFocus();
                findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                findViewById(R.id.spinnerHospital).setVisibility(View.VISIBLE);
            }
        }));


    }


    private void initComponents() {

        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        adapterEmail = new CorreoAdapter(this::deleteEmail);
        adapterPhone = new PhoneAdapter(this::deletePhone);
        ocupantesAdapter = new AseguradoOcupanteAdapter(this::eliminarOcupante, this);
        dateNacimiento.setOnClickListener(v -> {
            dateNacimiento.setError(null);
            showDatePikerDialog();
        });
        dateVigencia.setOnClickListener(v -> {
            dateVigencia.setError(null);
            showDatePikerDialogVigencia();
        });
        recyclerPhone.setHasFixedSize(true);
        recyclerPhone.setLayoutManager(new LinearLayoutManager(this));
        recyclerPhone.setItemAnimator(new DefaultItemAnimator());
        recyclerPhone.setAdapter(adapterPhone);


        recyclerEmail.setHasFixedSize(true);
        recyclerEmail.setLayoutManager(new LinearLayoutManager(this));
        recyclerEmail.setItemAnimator(new DefaultItemAnimator());
        recyclerEmail.setAdapter(adapterEmail);

        //recyclerOcupantes.setHasFixedSize(true);
        recyclerOcupantes.setLayoutManager(new LinearLayoutManager(this));
        recyclerOcupantes.setItemAnimator(new DefaultItemAnimator());
        recyclerOcupantes.setAdapter(ocupantesAdapter);

        checkBoxPermanente.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                findViewById(R.id.dateVigencia).setVisibility(View.INVISIBLE);
                findViewById(R.id.title_vigencia).setVisibility(View.INVISIBLE);
            } else {
                findViewById(R.id.dateVigencia).setVisibility(View.VISIBLE);
                findViewById(R.id.title_vigencia).setVisibility(View.VISIBLE);
                dateVigencia.setText("");
                dateVigencia.requestFocus();

            }
        });

        checkBoxConsequence.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isChecked) {
                daño_container.setVisibility(View.VISIBLE);
            } else {
                cbPerdida.setChecked(false);
                daño_container.setVisibility(GONE);
            }
        });
        cbPerdida.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isChecked) {
                daño_container.setVisibility(View.VISIBLE);

            } else {
                checkBoxConsequence.setChecked(false);
                daño_container.setVisibility(GONE);
            }
        });
        cbOtro.setChecked(false);


        spinnerIdentificacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave() != null) {
                    if (Objects.equals(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave(), "LC")) {
                        findViewById(R.id.content_tipo_licencia).setVisibility(View.VISIBLE);
                        findViewById(R.id.content_title_tipo_licencia).setVisibility(View.VISIBLE);
                        findViewById(R.id.content_fecha_vigencia).setVisibility(View.VISIBLE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
                        numIdentificacion.setText("Número de identificación");
                    } else {
                        findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
                        findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
                        findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
                        numIdentificacion.setText("Número de identificación");
                        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("CV")) {
                            numIdentificacion.setText("Clave de elector");
                            etNumeroIdentificacion.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (spinnerIdentificacion.getSelectedItem().toString().equals("Credencial para Votar") && !s.toString().matches("^[A-Z]{6}[0-9]{8}[0-9A-Z]{4}$")) {
                                        etNumeroIdentificacion.setError("ingrese una credencial válida");
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> estatus = Arrays.asList(getResources().getStringArray(R.array.spinner_status));
        AdapterSpinnerGeneral<String> spinnerArrayAdapter = new AdapterSpinnerGeneral<>(AseguradoActivity.this
                , R.layout.spinner_item, estatus, Object::toString);

        spinnerStatus.setAdapter(spinnerArrayAdapter);
        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                colorVehiculo = coloresList.get(spColor.getSelectedItemPosition()).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerTipoLicencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipoLicencia = tiposLicenciaList.get(position).getClave();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerEstadoEmisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadTiposLicencia(estadosLicencia.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void eliminarOcupante(int position) {
        getCompositeDisposable().add(aseguradoInteractor.deleteOcupante(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                ocupantesAdapter.getCorreo(position).getId())
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, "Se elimino correctamente el ocupante");
                    ocupantesAdapter.remove(ocupantesAdapter.getCorreo(position));
                }, throwable -> {
                    Log.e(TAG, "Error al borrar ocupante del asegurado", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                }));
    }


    private void setImpactos() {
        ImpactoVehiculoAmis impactoL = new ImpactoVehiculoAmis();
        AdapterSpinnerGeneral<ImpactoVehiculoAmis> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, impactos, ImpactoVehiculoAmis::getNombre);
        spPrimerImpacto.setAdapter(adapter);
        if (ajuste != null && ajuste.getSiniestro() != null && ajuste.getSiniestro().getTipoSiniestro() != null) {
            if (ajuste.getSiniestro().getTipoSiniestro().getNombre() != null) {
                if (ajuste.getSiniestro().getTipoSiniestro().getClave().equals("CR")) {
                    for (ImpactoVehiculoAmis impacto : impactos) {
                        if (impacto.getNombre().equals("Cristales")) {
                            impactoL = impacto;
                        }
                    }
                    int spinnerPosition = adapter.getPosition(impactoL);
                    spPrimerImpacto.setSelection(spinnerPosition);
                    spPrimerImpacto.setEnabled(false);
                    generalDamageFragment.setTypeImpact("Cristales");
                }
            }

        }


    }

    private void showDatePikerDialog() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateNacimiento.setText(date);
            if (age.contains("-")) {
                edad.setText("");
            } else {
                edad.setText(age);
            }
        });
        datePickerFragment.show(getSupportFragmentManager(), "dataPickerBirthday");
    }

    private String calculateAge(int year, int month, int day) {
        LocalDate birthdate = new LocalDate(year, month, day);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);

        return String.valueOf(age.getYears());
    }

    @SuppressLint("NewApi")
    private void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateVigencia.setText(date);
        });
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }


    private void getDatosConductorAsegurado() {
        getCompositeDisposable().add(getCatalogosPersona()
                .subscribe(this::getConductor,
                        throwable ->
                                new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable)));
    }

    private Completable downloadIdentificaciones() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getTiposIdentificacion()
                    .subscribe(identificacionesL -> {
                        identificaciones = identificacionesL;
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.spinner_item, identificaciones, CatalogoClave::getNombre);
                        spinnerIdentificacion.setAdapter(adapter);
                        spinnerIdentificacion.setSelection(SpinnerUtils.getPosition(spinnerIdentificacion, Constants.CLAVE_LICENCIA_CONDUCIR));
                        spinnerIdentificacion.setPrompt("Tipo de Identificación");
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerIdentificacion.setAdapter(null);
                        Log.e(TAG, "Error al descargar los tipos de identificaciones", throwable);
                    }));
        });


    }


    private Completable downloadNivelLesion() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getNivelLesion()
                    .subscribe(nivelesLesion -> {
                        niveles_lesion = nivelesLesion;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, niveles_lesion, CatalogoId::getNombre);
                        spinnerNivelLesion.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerNivelLesion.setAdapter(null);
                        Log.e(TAG, "Error al descargar niveles de lesión", throwable);
                    })
            );

        });


    }


    private String getSexo() {
        RadioButton rbM = findViewById(R.id.rbMasculino);
        if (rbM.isChecked()) {
            return "M";
        } else {
            return "F";
        }
    }

    private void setSexo(Persona response) {
        if ("M".equals(response.getSexo())) {
            sexos.check(R.id.rbMasculino);
        } else if ("F".equals(response.getSexo())) {
            sexos.check(R.id.rbFemenino);
        }
    }

    @OnClick(R.id.open_driver_bar)
    public void openDriverInfo() {
        if (!contentDriver.isShown()) {
            getDatosConductorAsegurado();
            contentDriver.setVisibility(View.VISIBLE);
            contentCar.setVisibility(View.GONE);
            contentOcupant.setVisibility(View.GONE);
            openDriver.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.rectangle_19));
            openVehiculo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
            openOcupante.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));

        } else {
            contentDriver.setVisibility(View.GONE);
            openDriver.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));

        }
    }


    @OnClick(R.id.open_vehiculo_bar)
    public void openCarInfo() {
        if (!contentCar.isShown()) {
            isUploadedV = false;
            getDatosVehiculo();
            contentDriver.setVisibility(View.GONE);
            contentCar.setVisibility(View.VISIBLE);
            contentOcupant.setVisibility(View.GONE);
            openVehiculo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.rectangle_19));
            openOcupante.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
            openDriver.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
        } else {
            contentCar.setVisibility(View.GONE);
            openVehiculo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
        }
    }

    @OnClick(R.id.open_ocupante_bar)
    public void openOcupantInfo() {
        if (!contentOcupant.isShown()) {
            generalCameraFragmentVehicle.setFolder(MODULO_ASEGURADOS, TIPO_OCUPANTE, 0, "", "");
            contentDriver.setVisibility(View.GONE);
            contentCar.setVisibility(View.GONE);
            contentOcupant.setVisibility(View.VISIBLE);
            openOcupante.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.rectangle_19));
            openVehiculo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
            openDriver.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
        } else {
            contentOcupant.setVisibility(View.GONE);
            openOcupante.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.fill_1_copy));
        }
    }


    public void insertConductor() {
        Log.d(TAG, "modify: ");
        Boolean isComplete = true;
        if (autoriza == null) {
            isComplete = false;
            Toast.makeText(getApplicationContext(), ResourceUtils.getString("aviso_privacidad_error"), Toast.LENGTH_LONG).show();
            btnGuardarConductor.setEnabled(true);

        }
        if (isComplete) {
            if (edtOtroHospital.getText().toString().equals("") && cbOtro.isChecked()) {
                edtOtroHospital.requestFocus();
                edtOtroHospital.setError(getResources().getString(R.string.volante_coberturas_required));
                btnGuardarConductor.setEnabled(true);
                return;
            }
            if (adapterEmail.getCorreoList().size() == 0) {
                Toast.makeText(getApplicationContext(), getString(R.string.correo_mesage), Toast.LENGTH_LONG).show();
                btnGuardarConductor.setEnabled(true);
                return;
            }
            getCompositeDisposable().add(aseguradoInteractor.modifyConductor(idAjuste, createConductor())
                    .subscribe(() -> {
                        btnGuardarConductor.setEnabled(true);
                        uploadImage.setVisibility(View.VISIBLE);
                        moduleDriver = new ModuleDriver((idAjuste + MODULO_ASEGURADOS + TIPO_CONDUCTOR), ASEGURADOS, CONDUCTOR, "", idAjuste);
                        path = getImagePathDriver(moduleDriver);
                        if (!getAllImages(getBaseActivity(), path).isEmpty()) {
                            uploadImagesDriver();
                        } else {
                            btnGuardarConductor.setEnabled(true);
                        }
                        new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, "Registro realizado correctamente");
                        AppPreferences.setString(TAG_NAME_CAPTURE, firstName.getText().toString()
                                + " " + lastName.getText() + " " + motherLastName.getText().toString());

                        isUploadedP = true;

                        if (CondicionPersona.LESIONADO.equals(createConductor().getCondicion())) {
                            mostrarDialogoImpresion();
                        }

                    }, throwable -> {
                        Log.e(TAG, "Error al modificar conductor", throwable);
                        new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                        // Crashlytics.log(Log.DEBUG, "insertConductorAsegurado", throwable.getMessage());
                        btnGuardarConductor.setEnabled(true);
                        isUploadedP = false;

                    }));

        }
    }

    private void mostrarDialogoImpresion() {
        DialogUtils.createAlert(this, "Impresión de volante médico",
                "Desea imprimir el volante médico", ResourceUtils.getString("yes"), ResourceUtils.getString("no"),
                (dialog, i) -> {
                    dialog.dismiss();
                    showDialogMedico(createConductor());

                }, (dialog, i) -> {
                    dialog.dismiss();
                });
    }

    @OnClick(R.id.btnGuardarAseguradoAuto)
    public void sendData() {
        btnGuardarVehiculo.setEnabled(false);
        addAseguradoAuto();
    }

    @OnClick(R.id.btnGuardarConductor)
    public void sendConductor() {
        btnGuardarConductor.setEnabled(false);
        if (validFormConductor()) {
            insertConductor();
        } else {
            btnGuardarConductor.setEnabled(true);
        }
    }


    private void showDialogMedico(Afectado afectado) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        DialogFragment dialogFragment = VolanteAseguradoPersonaFragment.newInstance(afectado);
        dialogFragment.setCancelable(false);
        dialogFragment.show(transaction, "diálogo");

    }


    @Override
    public void onItemClickItem(long position, View view) {

    }

    @Override
    public void onClickImage(View view, int position) {

    }

    @Override
    public void onLongClickImage(View view, int position) {

    }

    @Override
    public void onCallbackItemId(int position, Long id) {
        getOcupanteData(String.valueOf(ocupantesAdapter.getCorreo(position).getId()));
    }

    @Override
    public void onCallItemId(int position, Long id) {

    }

    private void addAseguradoAuto() {
        validForm();
        if (tipoVehiculo == null) {
            new GeneralSnackBar().largeSnackError(this, ResourceUtils.getString("tipo_vehiculo_error"));
            btnGuardarVehiculo.setEnabled(true);
            return;
        }
        if (armadora == null) {
            new GeneralSnackBar().largeSnackError(this, ResourceUtils.getString("armadora_vehiculo_error"));
            btnGuardarVehiculo.setEnabled(true);
            return;
        }
        if (carroceria == null) {
            new GeneralSnackBar().largeSnackError(this, ResourceUtils.getString("carroceria_vehiculo_error"));
            btnGuardarVehiculo.setEnabled(true);
            return;
        }
        if (modelo == null) {
            new GeneralSnackBar().largeSnackError(this, ResourceUtils.getString("modelo_vehiculo_error"));
            btnGuardarVehiculo.setEnabled(true);
            return;
        }
        if (version == null) {
            new GeneralSnackBar().largeSnackError(this, ResourceUtils.getString("modelo_vehiculo_error"));
            btnGuardarVehiculo.setEnabled(true);
            return;
        }
        if (colorVehiculo == null) {
            new GeneralSnackBar().largeSnackError(this, "Verifique el color del vehículo por favor");
            btnGuardarVehiculo.setEnabled(true);
            return;
        }

        placa = vehiculo_datos.get(0).getText().toString();
        motor = vehiculo_datos.get(1).getText().toString();
        serie = vehiculo_datos.get(2).getText().toString();
        VehiculoAsegurado data = new VehiculoAsegurado();
        data.setArmadora(armadora);
        data.setCarroceria(carroceria);
        data.setModelo(Integer.valueOf(modelo).shortValue());
        data.setNumeroMotor(motor);
        data.setPlacas(placa);
        data.setTipoVehiculo(tipoVehiculo);
        data.setVersion(version);
        data.setVin(serie);
        data.setColor(Long.valueOf(colorVehiculo));
        if (!checkBoxConsequence.isChecked() && !cbPerdida.isChecked()) {
            data.setDanoConsecuencia(true);
            if (etEstimadoDano.getText().toString().equals("")) {
                data.setDanoEstimado(0.0);
            } else {
                try {
                    data.setDanoEstimado(numberFormat.parse(etEstimadoDano.getText().toString()).doubleValue());
                } catch (ParseException ex) {
                    data.setDanoEstimado(0.0);
                }
            }
            if (generalDamageFragment.getList().isEmpty()) {
                btnGuardarVehiculo.setEnabled(true);
                new GeneralSnackBar().largeSnackError(AseguradoActivity.this, getString(R.string.partes_danadas));
                return;
            } else {
                data.setDanos(generalDamageFragment.getDataList());
            }
            if (!generalDamageFragmentPreexisting.getList().isEmpty()) {
                data.setDanosPreexistentes(generalDamageFragmentPreexisting.getDataList());
            } else {
                data.setDanosPreexistentes(null);
            }
            data.setPrimerImpacto(impactos.get(spPrimerImpacto.getSelectedItemPosition()).getId());
            data.setObservacion(etObservaciones.getText().toString());
            data.setPerdidaTotalEvidente(false);
        } else {
            if (cbPerdida.isChecked() && !checkBoxConsequence.isChecked()) {
                data.setPerdidaTotalEvidente(true);
                data.setDanoConsecuencia(false);
            }
            if (checkBoxConsequence.isChecked() && !cbPerdida.isChecked()) {
                data.setPerdidaTotalEvidente(false);
                data.setDanoConsecuencia(false);
            }
        }
        String log = new Gson().toJson(data);
        Log.d(TAG, "addAseguradoAuto: " + log);

        getCompositeDisposable().add(aseguradoInteractor.modifyVehiculo(idAjuste, data)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(AseguradoActivity.this, "Se agregó correctamente el vehículo");
                    AppPreferences.setString(AppPreferences.ESTIMADO_DE_DANO_ASEGURADO, String.valueOf(data.getDanoEstimado()));
                    tvTipo.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    tvArmadora.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    tvCarroceria.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    tvModelo.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    tvVersion.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    enabledSpinners(false);
                    uploadImageVehicle.setVisibility(View.VISIBLE);
                    ModuleCar moduleCar = new ModuleCar((idAjuste + MODULO_ASEGURADOS + TIPO_VEHICULO), ASEGURADOS, VEHICULO, "", idAjuste);
                    path = getImagePathCar(moduleCar);
                    if (!getAllImages(getBaseActivity(), path).isEmpty()) {
                        uploadImagesVehicle();
                    } else {
                        btnGuardarVehiculo.setEnabled(true);
                    }
                    isUploadedV = true;


                }, throwable -> {
                    isUploadedV = false;
                    btnGuardarVehiculo.setEnabled(true);
                    Log.e(TAG, "Error al modificar los datos del vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                }));


    }

    private Persona createConductor() {
        Persona conductor = new Persona();
        conductor.setNombre(firstName.getText().toString());
        conductor.setApellidoPaterno(lastName.getText().toString());
        conductor.setApellidoMaterno(motherLastName.getText().toString());
        if ("".equals(conductor.getApellidoMaterno())) {
            conductor.setApellidoMaterno(null);
        }

        conductor.setSexo(getSexo());
        conductor.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString()));
        conductor.setNumeroIdentificacion(etNumeroIdentificacion.getText().toString());
        conductor.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
        conductor.setTelefonos(list);
        conductor.setCorreos(adapterEmail.getCorreoList());
        conductor.setTipoIdentificacion(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave());
        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
            conductor.setTipoLicencia(tiposLicenciaList.get(spinnerTipoLicencia.getSelectedItemPosition()).getClave());
            if (checkBoxPermanente.isChecked()) {
                conductor.setLicenciaPermanente(true);
            } else {
                conductor.setLicenciaPermanente(false);
                conductor.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString()));
            }
        }
        conductor.setEstadoLicencia(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getNombre().equals("Licencia de Conducir") ? estadosLicencia.get(spinnerEstadoEmisor.getSelectedItemPosition()).getClave() : null);
        conductor.setAutorizaTratamiento(autoriza);
        if (statusLesionado) {
            conductor.setNivelLesion(niveles_lesion.get(spinnerNivelLesion.getSelectedItemPosition()).getId());
            conductor.setRequiereAmbulancia(cbAmbulancia.isChecked());

            conductor.setLesiones(new ArrayList<>());
            conductor.getLesiones().addAll(generalInjuredFragment.getDataList());
            if (hospitales != null && hospitales.size() != 0) {
                conductor.setHospital(!cbOtro.isChecked() ?
                        hospitales.get(spinnerHospital.getSelectedItemPosition()).getId() : null);
            } else {
                conductor.setHospital(null);
            }
            conductor.setOtroHospital(cbOtro.isChecked() ? edtOtroHospital.getText().toString() : null);
        } else {
            conductor.setRequiereAmbulancia(false);
            conductor.setNivelLesion(null);
        }
        String c = new Gson().toJson(conductor);
        Log.d(TAG, "modify: " + c);
        return conductor;
    }


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @OnClick(R.id.btnAddEmail)
    public void addEmail() {
        showDialogAddEmail();
    }

    @OnClick(R.id.btnAddPhone)
    public void addPhone() {
        downloadPhones();


    }

    private void showDialogAddEmail() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar correo electrónico")
                .customView(R.layout.dialog_add_e_mail, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();

        email = (EditText) builder.findViewById(R.id.etEmail);
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_e_mail));
        builder.show();
    }

    @SuppressLint("NewApi")
    private void showDialogAddPhone() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar teléfono")
                .customView(R.layout.dialog_add_phone, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        spinnerTipoTelefono = (Spinner) builder.findViewById(R.id.spinnerTipoTelefono);
        AdapterSpinnerGeneral<CatalogoClaveAmis> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.spinner_item, tiposTelefonos, CatalogoClave::getNombre);
        spinnerTipoTelefono.setAdapter(adapter);
        phone = (EditText) builder.findViewById(R.id.etPhone);
        TextView tituloExtencion = (TextView) builder.findViewById(R.id.titleExtencion);
        phone.requestFocus();
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        extension = (EditText) builder.findViewById(R.id.etExtension);
        spinnerTipoTelefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (((CatalogoClave) spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                    extension.setVisibility(View.VISIBLE);
                    tituloExtencion.setVisibility(View.VISIBLE);
                } else {
                    extension.setVisibility(View.GONE);
                    tituloExtencion.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setHeight(40);
        cancelar.setHeight(40);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_phone));
        builder.show();
    }


    private void downloadOcupantes() {
        getCompositeDisposable().add(aseguradoInteractor.getOcupantes(idAjuste)
                .subscribe(ocupantes -> {

                    if (ocupantes != null) {
                        ocupantesAdapter.addAll(ocupantes);
                        recyclerOcupantes.setAdapter(ocupantesAdapter);
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al obtener los ocupantes", throwable);
                    if (!(throwable instanceof ResourceNotFoundException))
                        new GeneralSnackBar().largeSnackError(AseguradoActivity.this, throwable);
                }));
    }

    public void getOcupanteData(String p) {
        Intent intent = new Intent(AseguradoActivity.this, AseguradoOcupanteActivity.class);
        intent.putExtra("data", p);
        intent.putExtra("idAjuste", idAjuste);
        String aData = new Gson().toJson(ajuste);
        intent.putExtra("data2", aData);
        intent.putExtra("discriminator", "ocupante");
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    private Completable downloadEstados() {
        return Completable.create(e -> {
            getCompositeDisposable().add(catInteractor.getEstados()
                    .subscribe(estados -> {
                        estadosLicencia = estados;
                        AdapterSpinnerGeneral<CatalogoClaveInfo> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, estadosLicencia, CatalogoClaveInfo::getNombre);
                        spinnerEstadoEmisor.setAdapter(adapter);

                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerEstadoEmisor.setAdapter(null);
                        Log.e(TAG, "Error al descargar los estados", throwable);
                    }));

        });


    }


    private void validForm() {
        boolean isRequestFocus = false;
        if (vehiculo_datos.get(0).getText().toString().trim().length() <= 0) {
            vehiculo_datos.get(0).setError(getString(R.string.mandatory_field));
            vehiculo_datos.get(0).requestFocus();
            return;
        }
        if (vehiculo_datos.get(1).getText().toString().trim().length() <= 0) {
            vehiculo_datos.get(1).setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                vehiculo_datos.get(1).requestFocus();
                isRequestFocus = true;
            }
            return;
        }
        if (vehiculo_datos.get(2).getText().toString().trim().length() <= 0) {
            vehiculo_datos.get(2).setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                vehiculo_datos.get(2).requestFocus();
                isRequestFocus = true;
            }
            return;
        }
    }

    @OnClick(R.id.btnAvisoPrivacidad)
    public void showAvisoPrivacidad() {
        mBuilder = new MaterialDialog.Builder(this)
                .title("Aviso de privacidad")
                .customView(R.layout.dialog_notice_of_privacy, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    if (autoriza != null) {
                        setAutoriza(autoriza);
                    }
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        si = (CheckBox) mBuilder.findViewById(R.id.cbPrivacidadSi);
        no = (CheckBox) mBuilder.findViewById(R.id.cbPrivacidadNo);
        if (autoriza != null) {
            if (autoriza) {
                si.setChecked(true);
                no.setChecked(false);
            } else {
                si.setChecked(false);
                no.setChecked(true);
            }
        }
        si.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(true);
                no.setChecked(false);
                autoriza = true;
            } else {
                no.setChecked(true);
                si.setChecked(false);
                autoriza = false;
            }
        });
        no.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(false);
                no.setChecked(true);
                autoriza = false;
            } else {
                no.setChecked(false);
                si.setChecked(true);
                autoriza = true;
            }
        });
        MDButton aceptar = mBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = mBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(mBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        mBuilder.show();
    }

    private void setAutoriza(boolean autoriza) {
        this.autoriza = autoriza;
    }


    @Override
    public void onBackPressed() {
        if (isUploadedP && isUploadedV) {
            super.onBackPressed();
        } else {
            String message = "";
            if (!isUploadedP && !isUploadedV) {
                message = getString(R.string.image_message_out);
            } else {
                if (!isUploadedP) {
                    message = getString(R.string.message_conductor_save);
                }
                if (!isUploadedV) {
                    message = getString(R.string.message_vehicle_save);
                }
            }
            DialogUtils.createAlert(this, "Salir",
                    message,
                    "Aceptar", "Cancelar",
                    (dialog, which) -> {
                        dialog.dismiss();
                        AseguradoActivity.super.onBackPressed();
                    },
                    (dialog, which) -> dialog.dismiss());
        }
    }

    @Override
    public void itemCheck(@org.jetbrains.annotations.Nullable Long id, boolean checked) {

    }

    @Override
    public void listChecks(@NotNull List<Long> list) {
        if (categoria != null) {
            getMoney(categoria.getCategoria(), list);
        } else {
            etEstimadoDano.setText("$" + 0.00);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String nombre = firstName.getText().toString().trim();
        if (!FormatStrings.validateText(nombre)) {
            firstName.setError(getString(R.string.message_character_special));
        }
        String apellidoP = lastName.getText().toString().trim();

        if (!FormatStrings.validateText(apellidoP)) {
            lastName.setError(getString(R.string.message_character_special));
        }
        String apellidoM = motherLastName.getText().toString().trim();

        if (!FormatStrings.validateText(apellidoM)) {
            motherLastName.setError(getString(R.string.message_character_special));
        }
    }


    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone != null) {
                    if (phone.getText().toString().trim().length() < 10) {
                        phone.setError(ResourceUtils.getString("invalid_number"));
                        if (spinnerTipoTelefono.getSelectedItem() != null) {
                            if (((CatalogoClave) spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                                if (extension.getText().toString().trim().length() < 6) {
                                    extension.setError(ResourceUtils.getString("extension_error"));
                                }
                            }
                        }
                    } else {
                        String key = "";
                        if (tiposTelefonos.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave() != null) {
                            key = tiposTelefonos.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave();
                        }
                        adapterPhone.add(new Telefono(phone.getText().toString(), key, extension.getText().toString().equals("") ? null : extension.getText().toString()));
                        list.add(new Telefono(phone.getText().toString(), key, extension.getText().toString().equals("") ? null : extension.getText().toString()));
                        recyclerPhone.setAdapter(adapterPhone);
                        adapterPhone.notifyDataSetChanged();
                        dialog.dismiss();
                    }

                }

            } else {
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError("Correo inválido");

                } else {
                    adapterEmail.add(email.getText().toString());
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        tipoVehiculo = !tipoVehiculos.isEmpty() ? tipoVehiculos.get(spVehiculoDatos.get(0).getSelectedItemPosition()).getClave() : null;
        armadora = tipoVehiculo != null && !armadorasList.isEmpty() ? armadorasList.get(spVehiculoDatos.get(1).getSelectedItemPosition()).getClave() : null;
        carroceria = armadora != null && !carroceriaList.isEmpty() ?
                carroceriaList.get(spVehiculoDatos.get(2).getSelectedItemPosition()).getClave() : null;
        modelo = carroceria != null && !modelos.isEmpty() ?
                modelos.get(spVehiculoDatos.get(3).getSelectedItemPosition()) : null;
        version = modelo != null && !versiones.isEmpty() ?
                versiones.get(spVehiculoDatos.get(4).getSelectedItemPosition()).getVersion() : null;


        switch (adapterView.getId()) {
            case R.id.spTipoVehiculoP:
                if (tipoVehiculo != null) {
                    downloadArmadora(tipoVehiculo);
                }
                break;
            case R.id.spArmadora:
                if (armadora != null) {
                    downloadCarroceria(tipoVehiculo, armadora);
                }
                break;
            case R.id.spCarroceria:
                if (carroceria != null) {
                    downloadModelos(tipoVehiculo, armadora, carroceria);
                }
                break;
            case R.id.spModeloP:
                if (modelo != null) {
                    downloadVersiones(tipoVehiculo, armadora, carroceria, modelo);
                }
                break;
            case R.id.spVersionP:
                if (version != null) {
                    downloadCategoria(tipoVehiculo, armadora, carroceria, modelo, version);
                }

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void getSupervisoria(Long id) {

        getCompositeDisposable().add(siniestrosInteractor.getHospital(id)
                .subscribe(supervisoriasResponse -> {
                    hospital = supervisoriasResponse;
                    setHospital();
                }, throwable -> {
                    Log.e(TAG, "Error al descargar supervisoria", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));

    }

    private void setHospital() {
        for (int i = 0; i < supervisorias.size(); i++) {
            if (supervisorias.get(i).getClave().equals(hospital.getSupervisoria())) {
                Log.d("", "position" + i);
                spinnerEstadoAmbulancia.setSelection(i);
                downloadHospitales(hospital.getSupervisoria());
                break;
            }
        }


    }

    private void downloadHospitales(String supervisora) {

        getCompositeDisposable().add(siniestrosInteractor.getHospitales(supervisora)
                .subscribe(hospitales -> {
                    this.hospitales = hospitales;
                    AdapterSpinnerGeneral<HospitalResumen> adapter = new AdapterSpinnerGeneral<>(this
                            , R.layout.sp_little_item, hospitales, HospitalResumen::getNombre);
                    spinnerHospital.setAdapter(adapter);
                    if (hospital != null) {
                        for (int j = 0; j < hospitales.size(); j++) {
                            if (this.hospitales.get(j).getNombre().equals(hospital.getNombre())) {
                                spinnerHospital.setSelection(j);
                                break;
                            }
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar hospitales", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));
    }


    private void addFragmentDamgeParts(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentDamageParts, fragment, Constants.TAG_DAMAGE);
        transaction.commit();
    }


    private void addFragmentDamgePartsPreexisting(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentDamagePartsPreexisting, fragment, Constants.TAG_DAMAGEPREEXTISTENTE);
        transaction.commit();
    }

    private void addFragmentDriver(Fragment fragment, String tag) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraInsureDriver, fragment, tag);
        transaction.commit();
    }

    private void addFragmentVehicle(Fragment fragment, String tag) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraInsureVehicle, fragment, tag);
        transaction.commit();
    }


    private void addFragmentDamgePartsDriver(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentInjuredPartsInsureDriver, fragment, Constants.TAG_DAMAGE);
        transaction.commit();
    }

    CallbackItemImage<Long> callbackDeleteImageVehicle = new CallbackItemImage<Long>() {
        @Override
        public void onCallbackItemId(int position, Long id) {
            deleteVehiclePhoto(id);
        }

        @Override
        public void onCallItemId(int position, Long id) {

        }
    };

    CallbackItemImage<Long> callbackDeleteImageDriver = new CallbackItemImage<Long>() {
        @Override
        public void onCallbackItemId(int position, Long id) {
            deleteDriverImage(id);
        }

        @Override
        public void onCallItemId(int position, Long id) {

        }
    };

    CallbackItemImage callbackItemImageOcupante = new CallbackItemImage<Long>() {
        @Override
        public void onCallbackItemId(int position, Long id) {
            Ocupante occupantResponse = ocupantesAdapter.getOcupante(position);
            ToastUtils.toastLong(getBaseContext(), occupantResponse.getId().intValue());
        }

        @Override
        public void onCallItemId(int position, Long id) {

        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
