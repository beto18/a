package com.gnp.ajustadormovil.android.views.asegurado;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.views.commons.adapters.PagerAdapter_Fragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.google.gson.Gson;

import butterknife.BindView;

public class AseguradoContentActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.result_tabs)
    TabLayout tabs;
    private AseguradoInteractor aseguradoInteractor;
    private String idAjuste;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asegurado_content);
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);

        getDatosAsegurado();

    }


    private void getDatosAsegurado(){
        getCompositeDisposable().add(aseguradoInteractor.get(idAjuste)
                .subscribe(this::initPager,
                        throwable -> {
                    Log.e(TAG, "Error al obtener datos del asegurado", throwable);
                    new GeneralSnackBar().largeSnackError(this,throwable);

                }));
    }



    private void initPager(Asegurado asegurado){
        Bundle argsConductor;
        argsConductor = new Bundle();
        String conductor = new Gson().toJson(asegurado.getConductor());
        argsConductor.putString("conductor",conductor);
        Bundle argsVehiculo;
        argsVehiculo = new Bundle();
        String vehiculo = new Gson().toJson(asegurado.getConductor());
        argsVehiculo.putString("vehiculo",vehiculo);
        Bundle argsOCupantes;
        argsOCupantes = new Bundle();
        String ocupantes = new Gson().toJson(asegurado.getOcupantes());
        argsOCupantes.putString("ocupantes",ocupantes);

        PagerAdapter_Fragment pagerAdapterFragment = new PagerAdapter_Fragment(getSupportFragmentManager());
        pagerAdapterFragment.addFragment(PersonaFragment.class,"Conductor",argsConductor);
        pagerAdapterFragment.addFragment(VehiculoFragment.class,"Vehículo", argsVehiculo);
        pagerAdapterFragment.addFragment(OcupanteFragment.class,"Ocupantes", argsOCupantes);

        viewPager.setAdapter(pagerAdapterFragment);
        viewPager.setCurrentItem(0);
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));
        //tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        tabs.setupWithViewPager(viewPager);


    }

}
