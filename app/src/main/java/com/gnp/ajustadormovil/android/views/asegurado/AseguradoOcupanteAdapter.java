package com.gnp.ajustadormovil.android.views.asegurado;
/*
 * Created by NOE on 09/02/2018.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class AseguradoOcupanteAdapter extends RecyclerView.Adapter<AseguradoOcupanteAdapter.CorreoHolder> {

    private final List<Ocupante> ocupanteList;
    private final OnClickListener clickListener;
    private final CallbackItemImage listenerImage;

    public AseguradoOcupanteAdapter(OnClickListener clickListener, CallbackItemImage listener) {
        this.clickListener = clickListener;
        this.ocupanteList = new LinkedList<>();
        this.listenerImage = listener;
    }

    public void addAll(List<Ocupante> results) {
        ocupanteList.clear();
        for (Ocupante result : results) {
            add(result);
        }
    }

    public void add(Ocupante r) {
        ocupanteList.add(r);
        notifyItemInserted(ocupanteList.size() - 1);
    }

    public void remove(Ocupante r) {
        int position = ocupanteList.indexOf(r);
        if (position > -1) {
            ocupanteList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        final int size = ocupanteList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                ocupanteList.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    public Ocupante getCorreo(int position) {
        return ocupanteList.get(position);
    }


    public RealmList<String> getIdsList() {
        RealmList<String> idOcupantesList = new RealmList<>();
        for (int i = 0; i < ocupanteList.size(); i++) {
            idOcupantesList.add(ocupanteList.get(i).getId().toString());
        }
        return idOcupantesList;
    }

    public Ocupante getOcupante(int position){
        return ocupanteList.get(position);
    }


    @Override
    public CorreoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_correo, parent, false);
        return new CorreoHolder(mView);
    }

    @Override
    public void onBindViewHolder(CorreoHolder holder, int position) {
        holder.email.setText(ocupanteList.get(position).getNombreCompleto());
    }

    @Override
    public int getItemCount() {
        return ocupanteList.size();
    }

    public interface OnClickListener {
        void onClick(int position);
    }

    class CorreoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.delete)
        ImageButton delete;

        CorreoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            delete.setOnClickListener(this);
            itemView.setOnClickListener(v ->
                    listenerImage.onCallbackItemId(getAdapterPosition(),0L));
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(getAdapterPosition());
        }
    }
}
