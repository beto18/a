package com.gnp.ajustadormovil.android.views.asegurado;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModuleDriver;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.error.DataException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.CorreoAdapter;
import com.gnp.ajustadormovil.android.views.commons.adapters.PhoneAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.lesiones.GeneralInjuredFragment;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.volantes.asegurado.VolanteAseguradoPersonaFragment;
import com.google.gson.Gson;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmList;

import static android.view.View.GONE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_ASEGURADOS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_CONDUCTOR;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getAllImages;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getImagePathDriver;
import static com.gnp.ajustadormovil.android.utils.Constants.ASEGURADOS;
import static com.gnp.ajustadormovil.android.utils.Constants.CONDUCTOR;
import static com.gnp.ajustadormovil.android.utils.Constants.TAG_NAME_CAPTURE;

public class PersonaFragment extends Fragment {


    private AppCompatActivity activity;

    public static final String TAG = PersonaFragment.class.getSimpleName();
    private static final int PERMISSION_REQUEST = 100;

    @BindView(R.id.etFirstName)
    EditText firstName;
    @BindView(R.id.tvNumIdentificacion)
    TextView numIdentificacion;
    @BindView(R.id.etLastName)
    EditText lastName;
    @BindView(R.id.etMotherLastName)
    EditText motherLastName;
    @BindView(R.id.dateNac)
    EditText dateNacimiento;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.etNumeroIdentificacion)
    EditText etNumeroIdentificacion;
    @BindView(R.id.dateVigencia)
    EditText dateVigencia;
    @BindView(R.id.edtOtroHospital)
    EditText edtOtroHospital;
    @BindView(R.id.lesionado_container)
    LinearLayout lesionado_container;
    @BindView(R.id.rbMasculino)
    RadioButton rbMasculino;
    @BindView(R.id.title_otro)
    TextView otro_hospital;
    @BindView(R.id.spinnerIdentificacion)
    Spinner spinnerIdentificacion;
    @BindView(R.id.spinnerEstadoEmisor)
    Spinner spinnerEstadoEmisor;
    @BindView(R.id.spinnerTipoLicencia)
    Spinner spinnerTipoLicencia;
    @BindView(R.id.spinnerStatus)
    Spinner spinnerStatus;
    @BindView(R.id.spinnerNivelLesion)
    Spinner spinnerNivelLesion;
    @BindView(R.id.btnGuardarConductor)
    Button btnGuardarConductor;
    @BindView(R.id.cbOtro)
    CheckBox cbOtro;
    @BindView(R.id.otro_container)
    LinearLayout otro_container;
    @BindView(R.id.cbPermanente)
    CheckBox checkBoxPermanente;
    @BindView(R.id.cbAmbulancia)
    CheckBox cbAmbulancia;
    @BindView(R.id.recyclerEmail)
    RecyclerView recyclerEmail;
    @BindView(R.id.recyclerPhone)
    RecyclerView recyclerPhone;
    @BindView(R.id.title_estado)
    TextView titleEstado;
    @BindView(R.id.content_hospital)
    LinearLayout contentHospital;
    @BindView(R.id.content_titile_hospital)
    LinearLayout contentTitleHospital;
    @BindView(R.id.btnSolicitarAmbulancia)
    Button solicitarAmbulancia;
    @BindView(R.id.spinnerHospital)
    Spinner spinnerHospital;
    @BindView(R.id.spinnerEstadoAmbulancia)
    Spinner spinnerEstadoAmbulancia;
    @BindView(R.id.title_nivel_lesion)
    TextView titulo_lesion;
    @BindView(R.id.asegurados_container)
    RelativeLayout layout;
    @Nullable
    @BindView(R.id.asegurado_radio_buttons)
    RadioGroup mRadioGroup;
    @BindView(R.id.rgSexo)
    RadioGroup sexos;
    @BindView(R.id.etAge)
    EditText edad;
    @BindView(R.id.btnuploadImages)
    Button uploadImage;

    boolean statusLesionado = false;

    private List<CatalogoId> nivelesDeLesion;
    private List<HospitalResumen> hospitales;
    private List<Supervisoria> supervisorias;
    private List<CatalogoClaveInfo> estadosLicencia;
    private List<Telefono> telefonos;
    private List<CatalogoClave> identificaciones;
    private List<ImageGeneralModel> fotosConductor;
    private List<CatalogoClaveAmis> tiposTelefonos;
    private List<CatalogoClave> tiposLicencia;

    Calendar calendar = Calendar.getInstance();


    private Menu menu;
    private Boolean autoriza;
    private MaterialDialog mBuilder;
    private Ajuste ajuste;
    private CheckBox si, no;

    private Persona conductorAsegurado;

    private CorreoAdapter adapterEmail;
    private PhoneAdapter adapterPhone;

    private String idAjuste = "";
    private EditText phone, email, extension;
    private Spinner spinnerTipoTelefono;

    private GeneralCameraFragment generalCameraFragmentDriver;

    private ModuleDriver moduleDriver;


    private String path;
    private GeneralInjuredFragment generalInjuredFragment;


    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;

    private AseguradoInteractor aseguradoInteractor;


    private Hospital hospital;


    private CompositeDisposable compositeDisposable;
    private View view;
    private String conductor;
    private long idConductor = 0;
    private String tipoLicencia;


    public static PersonaFragment newInstance() {
        PersonaFragment fragment = new PersonaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);

        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity)context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        compositeDisposable = ((BaseActivity)activity).getCompositeDisposable();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        generalCameraFragmentDriver = new GeneralCameraFragment();
        generalInjuredFragment = new GeneralInjuredFragment();
        addFragmentDamgePartsDriver(generalInjuredFragment);
        addFragmentDriver(generalCameraFragmentDriver);


        if (getArguments() != null) {
            conductor = getArguments().getString("conductor");
            conductorAsegurado = new Gson().fromJson(conductor,Persona.class);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.persona_fragment, container, false);
        ButterKnife.bind(this, view);
        initComponents();
        initListeners();
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        compositeDisposable.add(getCatalogos()
                .subscribe(this::setConductorAsegurado,
                        throwable ->{
                    Log.e(TAG, "Error al obtener los catálogos");
                            new GeneralSnackBar().largeSnackError(activity, throwable);

                }));
    }


    private Completable getCatalogos() {
        return Completable.mergeArrayDelayError(
                getIdentificaciones(),
                getNivelLesion(),
                getEstados(),
                getSupervisorias());
    }



    private Completable getIdentificaciones() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getTiposIdentificacion()
                    .subscribe(identificaciones -> {
                        this.identificaciones = identificaciones;

                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, identificaciones, CatalogoClave::getNombre);
                        spinnerIdentificacion.setAdapter(adapter);
                        spinnerIdentificacion.setSelection(SpinnerUtils.getPosition(spinnerIdentificacion, Constants.CLAVE_LICENCIA_CONDUCIR));
                        spinnerIdentificacion.setPrompt("Tipo de Identificación");

                        e.onComplete();
                    }, throwable -> {
                        identificaciones = new ArrayList<>();
                        e.tryOnError(throwable);
                        spinnerIdentificacion.setAdapter(null);
                        Log.e(TAG, "Error al descargar los tipos de identificaciones", throwable);
                    }));
        });


    }

    private Completable getNivelLesion() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getNivelLesion()
                    .subscribe(nivelesLesion -> {
                        nivelesDeLesion = nivelesLesion;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(activity
                                , R.layout.sp_little_item, nivelesDeLesion, CatalogoId::getNombre);
                        spinnerNivelLesion.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        nivelesDeLesion = new ArrayList<>();
                        e.tryOnError(throwable);
                        spinnerNivelLesion.setAdapter(null);
                        Log.e(TAG, "Error al descargar niveles de lesión", throwable);
                    })
            );

        });


    }

    private Completable getEstados() {
        return Completable.create(e -> {
            compositeDisposable.add(catInteractor.getEstados()
                    .subscribe(estados -> {
                        estadosLicencia = estados;
                        AdapterSpinnerGeneral<CatalogoClaveInfo> adapter = new AdapterSpinnerGeneral<>(activity
                                , R.layout.sp_little_item, estadosLicencia, CatalogoClaveInfo::getNombre);
                        spinnerEstadoEmisor.setAdapter(adapter);

                        e.onComplete();
                    }, throwable -> {
                        estadosLicencia = new ArrayList<>();
                        e.tryOnError(throwable);
                        spinnerEstadoEmisor.setAdapter(null);
                        Log.e(TAG, "Error al descargar los estados", throwable);
                    }));

        });


    }


    private Completable getSupervisorias() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getSupervisorias()
                    .subscribe(supervisioriaList -> {
                        supervisorias = supervisioriaList;
                            AdapterSpinnerGeneral<Supervisoria> adapterStates = new AdapterSpinnerGeneral<>(activity,R.layout.spinner_item, supervisorias, Supervisoria::getNombre);
                            spinnerEstadoAmbulancia.setAdapter(adapterStates);
                            spinnerEstadoAmbulancia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    downloadHospitales(supervisorias.get(spinnerEstadoAmbulancia.getSelectedItemPosition()).getClave());
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        e.onComplete();
                    }, throwable -> {
                        supervisorias = new ArrayList<>();
                        e.tryOnError(throwable);
                        spinnerEstadoAmbulancia.setAdapter(null);

                        Log.e(TAG, "Error al descargar supervisorías", throwable);
                    })
            );
        });


    }


    @OnClick(R.id.btnSolicitarAmbulancia)
    public void callAmbulancia() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(layout, "Necesitas brindar el permiso para realizar llamadas",
                            Snackbar.LENGTH_LONG).setAction("Aceptar", new View.OnClickListener() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                call();
            }
        } else {
            call();
        }

    }

    @SuppressLint("MissingPermission")
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Utils.NUMBER_AMBULANCE));
        startActivity(intent);
    }

    @OnClick(R.id.btnuploadImages)
    public void addFotografiaConductor() {
        ImageUtils imageUtils = new ImageUtils();
        List<ImageGeneralModel> imageList = generalCameraFragmentDriver.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        if (!images.isEmpty()) {
            int size = images.size();
            uploadImage.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            compositeDisposable.add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> aseguradoInteractor.addFotografiaConductor(idAjuste, file).toObservable(), false, 1)
                    .doOnEach(fotografia -> {
                        count.setValue(count.getValue() + 1);
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir fotografía", throwable);
                        return new Fotografia();
                    }).toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        uploadImage.setEnabled(true);
                        boolean ok = true;
                        for (Fotografia pic : fotografias) {
                            ok &= pic.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(activity, getResources().getString(R.string.imagenes_upload_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(activity, getResources().getString(R.string.imagenes_upload_error));
                        }
                        Log.d(TAG, "renomabrando imagenes de carpeta de conductor: " + images.size());
                        imageUtils.renameImageUpload(images);
                        Log.d(TAG, "Eliminado imagenes de carpeta de conductor: " + images.size());
                        imageUtils.deleteImage(activity, images);
                        getFotografiasConductor();
                    }, throwable -> {
                        uploadImage.setEnabled(true);
                        btnGuardarConductor.setEnabled(true);
                        Log.e(TAG, "Error al subir fotografías del conductor del ajuste " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(activity, throwable);
                    }));
        }
    }


    private void getFotografiasConductor() {
        compositeDisposable.add(aseguradoInteractor.getFotografiasConductor(idAjuste)
                .subscribe(filesPhotosLis -> {

                    for (Fotografia photo : filesPhotosLis) {
                        fotosConductor.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                        Log.d(TAG, "obteniendo imagenes de servicio conductor:" + photo.getUrl());
                    }
                    generalCameraFragmentDriver.updateImageList(fotosConductor);
                    btnGuardarConductor.setEnabled(true);
                    uploadImage.setEnabled(true);
                    fotosConductor.clear();

                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                    uploadImage.setEnabled(true);
                    btnGuardarConductor.setEnabled(true);
                }));
    }


    private void eliminarTelefono(int position) {
        MaterialDialog deletePhoneBuilder = new MaterialDialog.Builder(activity)
                .title("Eliminar número...")
                .content("¿Desea eliminar este número?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapterPhone.remove(adapterPhone.getTelono(position));
                    telefonos.remove(position);
                    recyclerPhone.setAdapter(adapterPhone);
                    adapterPhone.notifyDataSetChanged();
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        MDButton aceptar = deletePhoneBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = deletePhoneBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(deletePhoneBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        deletePhoneBuilder.show();
    }

    private void deleteEmail(int position) {
        MaterialDialog deleteMailBuilder = new MaterialDialog.Builder(activity)
                .title("Eliminar correo...")
                .content("¿Desea eliminar este correo?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapterEmail.remove(adapterEmail.getCorreo(position));
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        MDButton aceptar = deleteMailBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = deleteMailBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(deleteMailBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        deleteMailBuilder.show();
    }

    private void initListeners() {

        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
                    titulo_lesion.setVisibility(View.VISIBLE);
                    lesionado_container.setVisibility(View.VISIBLE);
                    spinnerNivelLesion.setVisibility(View.VISIBLE);
                    contentHospital.setVisibility(View.VISIBLE);
                    contentTitleHospital.setVisibility(View.VISIBLE);
                    spinnerHospital.setVisibility(View.VISIBLE);
                    cbOtro.setVisibility(View.VISIBLE);
                    otro_container.setVisibility(View.VISIBLE);
                    solicitarAmbulancia.setVisibility(View.VISIBLE);
                    statusLesionado = true;
                } else {
                    titulo_lesion.setVisibility(GONE);
                    lesionado_container.setVisibility(GONE);
                    spinnerNivelLesion.setVisibility(GONE);
                    contentHospital.setVisibility(View.GONE);
                    otro_container.setVisibility(View.INVISIBLE);
                    contentTitleHospital.setVisibility(View.INVISIBLE);
                    cbOtro.setVisibility(View.INVISIBLE);
                    solicitarAmbulancia.setVisibility(View.GONE);
                    spinnerHospital.setVisibility(View.GONE);
                    statusLesionado = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        edad.setFocusable(false);

        cbOtro.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            if (isChecked) {
                otro_hospital.setVisibility(View.VISIBLE);
                edtOtroHospital.setVisibility(View.VISIBLE);
                edtOtroHospital.requestFocus();
                view.findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                view.findViewById(R.id.spinnerHospital).setVisibility(GONE);
                view.findViewById(R.id.title_otros).setVisibility(View.VISIBLE);
            } else {
                otro_hospital.setVisibility(GONE);
                view.findViewById(R.id.title_otros).setVisibility(GONE);
                edtOtroHospital.setVisibility(View.INVISIBLE);
                edtOtroHospital.clearFocus();
                view.findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                view.findViewById(R.id.spinnerHospital).setVisibility(View.VISIBLE);
            }
        }));


    }


    private void initComponents() {

        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        adapterEmail = new CorreoAdapter(this::deleteEmail);
        adapterPhone = new PhoneAdapter(this::eliminarTelefono);
        dateNacimiento.setOnClickListener(v -> {
            dateNacimiento.setError(null);
            showDatePikerDialog();
        });
        dateVigencia.setOnClickListener(v -> {
            dateVigencia.setError(null);
            showDatePikerDialogVigencia();
        });
        recyclerPhone.setHasFixedSize(true);
        recyclerPhone.setLayoutManager(new LinearLayoutManager(activity));
        recyclerPhone.setItemAnimator(new DefaultItemAnimator());
        recyclerPhone.setAdapter(adapterPhone);


        recyclerEmail.setHasFixedSize(true);
        recyclerEmail.setLayoutManager(new LinearLayoutManager(activity));
        recyclerEmail.setItemAnimator(new DefaultItemAnimator());
        recyclerEmail.setAdapter(adapterEmail);



        checkBoxPermanente.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.dateVigencia).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.title_vigencia).setVisibility(View.INVISIBLE);
            } else {
                view.findViewById(R.id.dateVigencia).setVisibility(View.VISIBLE);
                view.findViewById(R.id.title_vigencia).setVisibility(View.VISIBLE);
                dateVigencia.setText("");
                dateVigencia.requestFocus();

            }
        });


        cbOtro.setChecked(false);


        spinnerIdentificacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave() != null) {
                    if (Objects.equals(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave(), "LC")) {
                        view.findViewById(R.id.content_tipo_licencia).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.content_title_tipo_licencia).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.content_fecha_vigencia).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
                        numIdentificacion.setText("Número de identificación");
                    } else {
                        view.findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
                        view.findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
                        view.findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
                        view.findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
                        numIdentificacion.setText("Número de identificación");
                        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("CV")) {
                            numIdentificacion.setText("Clave de elector");
                            etNumeroIdentificacion.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (spinnerIdentificacion.getSelectedItem().toString().equals("Credencial para Votar") && !s.toString().matches("^[A-Z]{6}[0-9]{8}[0-9A-Z]{4}$")) {
                                        etNumeroIdentificacion.setError("ingrese una credencial válida");
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> estatus = Arrays.asList(getResources().getStringArray(R.array.spinner_status));
        AdapterSpinnerGeneral<String> spinnerArrayAdapter = new AdapterSpinnerGeneral<>(activity
                , R.layout.spinner_item, estatus, Object::toString);

        spinnerStatus.setAdapter(spinnerArrayAdapter);

        spinnerTipoLicencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipoLicencia = tiposLicencia.get(position).getClave();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerEstadoEmisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadTiposLicencia(estadosLicencia.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void showDatePikerDialog() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateNacimiento.setText(date);
            if (age.contains("-")) {
                edad.setText("");
            } else {
                edad.setText(age);
            }
        });
        datePickerFragment.show(activity.getSupportFragmentManager(), "dataPickerBirthday");
    }



    @SuppressLint("NewApi")
    private void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateVigencia.setText(date);
        });
        datePickerFragment.show(activity.getSupportFragmentManager(), "datePicker");
    }


    private String getSexo() {
        RadioButton rbM = view.findViewById(R.id.rbMasculino);
        if (rbM.isChecked()) {
            return "M";
        } else {
            return "F";
        }
    }

    private void setSexo(Persona response) {
        if ("M".equals(response.getSexo())) {
            sexos.check(R.id.rbMasculino);
        } else if ("F".equals(response.getSexo())) {
            sexos.check(R.id.rbFemenino);
        }
    }



    public void insertConductor() {
        Log.d(TAG, "modify: ");
        Boolean isComplete = true;
        if (autoriza == null) {
            isComplete = false;
            Toast.makeText(activity, ResourceUtils.getString("aviso_privacidad_error"), Toast.LENGTH_LONG).show();
            btnGuardarConductor.setEnabled(true);

        }
        if (isComplete) {
            if (edtOtroHospital.getText().toString().equals("") && cbOtro.isChecked()) {
                edtOtroHospital.requestFocus();
                edtOtroHospital.setError(getResources().getString(R.string.volante_coberturas_required));
                btnGuardarConductor.setEnabled(true);
                return;
            }
            if (adapterEmail.getCorreoList().size() == 0) {
                Toast.makeText(activity, getString(R.string.correo_mesage), Toast.LENGTH_LONG).show();
                btnGuardarConductor.setEnabled(true);
                return;
            }
            compositeDisposable.add(aseguradoInteractor.modifyConductor(idAjuste, createConductor())
                    .subscribe(() -> {
                        btnGuardarConductor.setEnabled(true);
                        uploadImage.setVisibility(View.VISIBLE);
                        moduleDriver = new ModuleDriver((idAjuste + MODULO_ASEGURADOS + TIPO_CONDUCTOR), ASEGURADOS, CONDUCTOR, "", idAjuste);
                        path = getImagePathDriver(moduleDriver);
                        if (!getAllImages(activity, path).isEmpty()) {
                            addFotografiaConductor();
                        } else {
                            btnGuardarConductor.setEnabled(true);
                        }
                        new GeneralSnackBar().largeSnackOk(activity, "Registro realizado correctamente");
                        AppPreferences.setString(TAG_NAME_CAPTURE, firstName.getText().toString()
                                + " " + lastName.getText() + " " + motherLastName.getText().toString());


                        if (CondicionPersona.LESIONADO.equals(createConductor().getCondicion())) {
                            mostrarDialogoImpresion();
                        }

                    }, throwable -> {
                        Log.e(TAG, "Error al modificar conductor", throwable);
                        new GeneralSnackBar().largeSnackError(activity, throwable);
                        // Crashlytics.log(Log.DEBUG, "insertConductorAsegurado", throwable.getMessage());
                        btnGuardarConductor.setEnabled(true);

                    }));

        }
    }

    private void mostrarDialogoImpresion() {
        DialogUtils.createAlert(activity, "Impresión de volante médico",
                "Desea imprimir el volante médico", ResourceUtils.getString("yes"), ResourceUtils.getString("no"),
                (dialog, i) -> {
                    dialog.dismiss();
                    showDialogMedico(createConductor());

                }, (dialog, i) -> {
                    dialog.dismiss();
                });
    }


    @OnClick(R.id.btnGuardarConductor)
    public void sendConductor() {
        btnGuardarConductor.setEnabled(false);
        if (validFormConductor()) {
            insertConductor();
        } else {
            btnGuardarConductor.setEnabled(true);
        }
    }

    private boolean validFormConductor() {
        try {
            ErrorUtils.assertValues(conductorAsegurado);
            return true;
        }catch (DataException e){
            new GeneralSnackBar().largeSnackError(activity, e);
            return false;
        }
    }


    private void showDialogMedico(Afectado afectado) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        DialogFragment dialogFragment = VolanteAseguradoPersonaFragment.newInstance(afectado);
        dialogFragment.setCancelable(false);
        dialogFragment.show(transaction, "diálogo");

    }



    private Persona createConductor() {
        Persona conductor = new Persona();
        conductor.setNombre(firstName.getText().toString());
        conductor.setApellidoPaterno(lastName.getText().toString());
        conductor.setApellidoMaterno(motherLastName.getText().toString());
        if ("".equals(conductor.getApellidoMaterno())) {
            conductor.setApellidoMaterno(null);
        }

        conductor.setSexo(getSexo());
        conductor.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString()));
        conductor.setNumeroIdentificacion(etNumeroIdentificacion.getText().toString());
        conductor.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
        conductor.setTelefonos(telefonos);
        conductor.setCorreos(adapterEmail.getCorreoList());
        conductor.setTipoIdentificacion(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave());
        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
            conductor.setTipoLicencia(tiposLicencia.get(spinnerTipoLicencia.getSelectedItemPosition()).getClave());
            if (checkBoxPermanente.isChecked()) {
                conductor.setLicenciaPermanente(true);
            } else {
                conductor.setLicenciaPermanente(false);
                conductor.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString()));
            }
        }
        conductor.setEstadoLicencia(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getNombre().equals("Licencia de Conducir") ? estadosLicencia.get(spinnerEstadoEmisor.getSelectedItemPosition()).getClave() : null);
        conductor.setAutorizaTratamiento(autoriza);
        if (statusLesionado) {
            conductor.setNivelLesion(nivelesDeLesion.get(spinnerNivelLesion.getSelectedItemPosition()).getId());
            conductor.setRequiereAmbulancia(cbAmbulancia.isChecked());

            conductor.setLesiones(new ArrayList<>());
            conductor.getLesiones().addAll(generalInjuredFragment.getDataList());
            if (hospitales != null && hospitales.size() != 0) {
                conductor.setHospital(!cbOtro.isChecked() ?
                        hospitales.get(spinnerHospital.getSelectedItemPosition()).getId() : null);
            } else {
                conductor.setHospital(null);
            }
            conductor.setOtroHospital(cbOtro.isChecked() ? edtOtroHospital.getText().toString() : null);
        } else {
            conductor.setRequiereAmbulancia(false);
            conductor.setNivelLesion(null);
        }
        String c = new Gson().toJson(conductor);
        Log.d(TAG, "modify: " + c);
        return conductor;
    }


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @OnClick(R.id.btnAddEmail)
    public void addEmail() {
        showDialogAddEmail();
    }

    @OnClick(R.id.btnAddPhone)
    public void addPhone() {
        getTiposTelefonos();


    }


    private void showDialogAddEmail() {
        MaterialDialog builder = new MaterialDialog.Builder(activity)
                .title("Agregar correo electrónico")
                .customView(R.layout.dialog_add_e_mail, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();

        email = (EditText) builder.findViewById(R.id.etEmail);
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_e_mail));
        builder.show();
    }



    @OnClick(R.id.btnAvisoPrivacidad)
    public void showAvisoPrivacidad() {
        mBuilder = new MaterialDialog.Builder(activity)
                .title("Aviso de privacidad")
                .customView(R.layout.dialog_notice_of_privacy, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    if (autoriza != null) {
                        setAutoriza(autoriza);
                    }
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        si = (CheckBox) mBuilder.findViewById(R.id.cbPrivacidadSi);
        no = (CheckBox) mBuilder.findViewById(R.id.cbPrivacidadNo);
        if (autoriza != null) {
            if (autoriza) {
                si.setChecked(true);
                no.setChecked(false);
            } else {
                si.setChecked(false);
                no.setChecked(true);
            }
        }
        si.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(true);
                no.setChecked(false);
                autoriza = true;
            } else {
                no.setChecked(true);
                si.setChecked(false);
                autoriza = false;
            }
        });
        no.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(false);
                no.setChecked(true);
                autoriza = false;
            } else {
                no.setChecked(false);
                si.setChecked(true);
                autoriza = true;
            }
        });
        MDButton aceptar = mBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = mBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(mBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        mBuilder.show();
    }

    private void setAutoriza(boolean autoriza) {
        this.autoriza = autoriza;
    }


    private void getSupervisoria(Long id) {

        compositeDisposable.add(siniestrosInteractor.getHospital(id)
                .subscribe(supervisoriasResponse -> {
                    hospital = supervisoriasResponse;
                    setHospital();
                }, throwable -> {
                    Log.e(TAG, "Error al descargar supervisoria", throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                }));

    }

    private void setHospital() {
        for (int i = 0; i < supervisorias.size(); i++) {
            if (supervisorias.get(i).getClave().equals(hospital.getSupervisoria())) {
                Log.d("", "position" + i);
                spinnerEstadoAmbulancia.setSelection(i);
                downloadHospitales(hospital.getSupervisoria());
                break;
            }
        }


    }

    private void downloadHospitales(String supervisora) {

        compositeDisposable.add(siniestrosInteractor.getHospitales(supervisora)
                .subscribe(hospitales -> {
                    this.hospitales = hospitales;
                    AdapterSpinnerGeneral<HospitalResumen> adapter = new AdapterSpinnerGeneral<>(activity
                            , R.layout.sp_little_item, hospitales, HospitalResumen::getNombre);
                    spinnerHospital.setAdapter(adapter);
                    if (hospital != null) {
                        for (int j = 0; j < hospitales.size(); j++) {
                            if (this.hospitales.get(j).getNombre().equals(hospital.getNombre())) {
                                spinnerHospital.setSelection(j);
                                break;
                            }
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar hospitales", throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                }));
    }


    private void addFragmentDriver(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraInsureDriver, fragment, Constants.TAG_INSURE_DRIVER);
        transaction.commit();
    }

    private void addFragmentDamgePartsDriver(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentInjuredPartsInsureDriver, fragment, Constants.TAG_DAMAGE);
        transaction.commit();
    }


    CallbackItemImage<Long> callbackDeleteImageDriver = new CallbackItemImage<Long>() {
        @Override
        public void onCallbackItemId(int position, Long id) {
            deleteDriverImage(id);
        }

        @Override
        public void onCallItemId(int position, Long id) {

        }
    };

    public void deleteDriverImage(long foto) {
        compositeDisposable.add(aseguradoInteractor.deleteFotografiaConductor(idAjuste, foto)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(activity, "Se eliminó correctamente la fotografía del asegurado");
                }, throwable -> {
                    Log.e(TAG, "Error al eliminar las fotografías del conductor del ajuste " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                })
        );

    }

    private void downloadTiposLicencia(String estado) {
        compositeDisposable.add(siniestrosInteractor.getTiposLicencia(estado)
                .subscribe(tiposLicencia -> {
                    this.tiposLicencia = tiposLicencia;
                    AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(activity
                            , R.layout.sp_little_item, this.tiposLicencia, CatalogoClave::getNombre);
                    spinnerTipoLicencia.setAdapter(adapter);
                    if (conductorAsegurado != null && conductorAsegurado.getTipoLicencia() != null) {
                        spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, conductorAsegurado.getTipoLicencia()));
                    }
                }, throwable -> {
                    spinnerTipoLicencia.setAdapter(null);
                    Log.e(TAG, "Error al descargar los colores", throwable);
                }));

    }

    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone != null) {
                    if (phone.getText().toString().trim().length() < 10) {
                        phone.setError(ResourceUtils.getString("invalid_number"));
                        if (spinnerTipoTelefono.getSelectedItem() != null) {
                            if (((CatalogoClave) spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                                if (extension.getText().toString().trim().length() < 6) {
                                    extension.setError(ResourceUtils.getString("extension_error"));
                                }
                            }
                        }
                    } else {
                        String key = "";
                        if (tiposTelefonos.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave() != null) {
                            key = tiposTelefonos.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave();
                        }
                        telefonos = new RealmList<>();
                        adapterPhone.add(new Telefono(phone.getText().toString(), key, extension.getText().toString().equals("") ? null : extension.getText().toString()));
                        telefonos.add(new Telefono(phone.getText().toString(), key, extension.getText().toString().equals("") ? null : extension.getText().toString()));
                        recyclerPhone.setAdapter(adapterPhone);
                        adapterPhone.notifyDataSetChanged();
                        dialog.dismiss();
                    }

                }

            } else {
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError("Correo inválido");

                } else {
                    adapterEmail.add(email.getText().toString());
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        }
    }


    // CARGA DE DATOS DE ASEGURADO CONDUCTOR
    private void setConductorAsegurado() {

        Log.d(TAG, "Asignando valorea a conductor");
        moduleDriver = new ModuleDriver((idAjuste + MODULO_ASEGURADOS + TIPO_CONDUCTOR), ASEGURADOS, CONDUCTOR, "", idAjuste);
        path = getImagePathDriver(moduleDriver);
        generalCameraFragmentDriver.setFolder(MODULO_ASEGURADOS, TIPO_CONDUCTOR, 0, "", "");
        PersistenciaPath.showMessageSizeImages(activity, getAllImages(activity, path));
        Log.d(TAG, "path imagenes condcutor :" + path);
        if (path != null) {
            fotosConductor = new ArrayList<>();
            fotosConductor.addAll(getAllImages(activity, path));
            generalCameraFragmentDriver.setImageList(fotosConductor);
        }

        if (conductorAsegurado.getAutorizaTratamiento() != null){
            setAutoriza(conductorAsegurado.getAutorizaTratamiento());
        }

        firstName.setText(conductorAsegurado.getNombre() != null ? conductorAsegurado.getNombre() : "");
        lastName.setText(conductorAsegurado.getApellidoPaterno() != null ? conductorAsegurado.getApellidoPaterno() : "");
        motherLastName.setText(conductorAsegurado.getApellidoMaterno() != null ? conductorAsegurado.getApellidoMaterno() : "");

        if (!conductorAsegurado.getCorreos().isEmpty()) {
            adapterEmail.addAll(conductorAsegurado.getCorreos());
            recyclerEmail.setAdapter(adapterEmail);
            adapterEmail.notifyDataSetChanged();
        }
        if (conductorAsegurado.getId() != 0) {
            idConductor = conductorAsegurado.getId();
        }

        if (!conductorAsegurado.getTelefonos().isEmpty()) {
            telefonos.clear();
            telefonos.addAll(conductorAsegurado.getTelefonos());
            adapterPhone.addAll(telefonos);
            recyclerPhone.setAdapter(adapterPhone);
            telefonos = conductorAsegurado.getTelefonos();
            adapterPhone.notifyDataSetChanged();
        }
        if (conductorAsegurado.getFechaNacimiento() != null) {
            dateNacimiento.setText(DateUtils.date(conductorAsegurado.getFechaNacimiento()));
            int years = Years
                    .yearsBetween(LocalDate.fromDateFields(conductorAsegurado.getFechaNacimiento()), LocalDate.now())
                    .getYears();
            edad.setText(Integer.toString(years));
        }
        etNumeroIdentificacion.setText(conductorAsegurado.getNumeroIdentificacion() != null ? conductorAsegurado.getNumeroIdentificacion() : "");
        setSexo(conductorAsegurado);

        if (conductorAsegurado.getCondicion() != null) {
            if (conductorAsegurado.getCondicion().equals(CondicionPersona.LESIONADO)) {
                titulo_lesion.setVisibility(View.VISIBLE);
                lesionado_container.setVisibility(View.VISIBLE);
                spinnerNivelLesion.setVisibility(View.VISIBLE);

                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, conductorAsegurado.getCondicion().name()));
                if (conductorAsegurado.getNivelLesion() != null && conductorAsegurado.getNivelLesion() != 0) {
                    spinnerNivelLesion.setSelection(SpinnerUtils.getPosition(spinnerNivelLesion, conductorAsegurado.getNivelLesion()));
                }
                statusLesionado = true;
                if (conductorAsegurado.getRequiereAmbulancia()) {
                    cbAmbulancia.setChecked(conductorAsegurado.getRequiereAmbulancia());
                }

                if (conductorAsegurado.getOtroHospital() != null) {
                    cbOtro.setChecked(true);
                    edtOtroHospital.setText(conductorAsegurado.getOtroHospital());
                    otro_container.setVisibility(View.VISIBLE);
                    contentTitleHospital.setVisibility(View.INVISIBLE);
                    otro_hospital.setVisibility(View.VISIBLE);
                    edtOtroHospital.setVisibility(View.VISIBLE);
                    view.findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                    view.findViewById(R.id.spinnerHospital).setVisibility(GONE);
                    edtOtroHospital.clearFocus();
                } else {
                    //getSupervisoria(Integer.toString(response.body().getIdHospital()));
                    view.findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.spinnerHospital).setVisibility(View.VISIBLE);
                    cbOtro.setChecked(false);
                }
                if (conductorAsegurado.getHospital() != null) {
                    getSupervisoria(conductorAsegurado.getHospital());
                } else {
                    view.findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                    view.findViewById(R.id.spinnerHospital).setVisibility(GONE);
                }
            } else {
                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, conductorAsegurado.getCondicion().name()));
            }

        }
        if (!conductorAsegurado.getFotografias().isEmpty()) {
            getFotografiasConductor();
            uploadImage.setVisibility(View.VISIBLE);
            generalCameraFragmentDriver.setCallBackItem(callbackDeleteImageDriver);
        } else {
            uploadImage.setVisibility(View.VISIBLE);

        }
        if (conductorAsegurado.getTipoIdentificacion() != null) {
            spinnerIdentificacion.setSelection(SpinnerUtils.getPosition(spinnerIdentificacion, conductorAsegurado.getTipoIdentificacion()));

            if (identificaciones != null && identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
               view.findViewById(R.id.content_tipo_licencia).setVisibility(View.VISIBLE);
               view.findViewById(R.id.content_title_tipo_licencia).setVisibility(View.VISIBLE);
               view.findViewById(R.id.content_fecha_vigencia).setVisibility(View.VISIBLE);
               view.findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
            } else {
               view.findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
               view.findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
               view.findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
               view.findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
            }

        }
        if (conductorAsegurado.getEstadoLicencia() != null) {
            spinnerEstadoEmisor.setSelection(SpinnerUtils.getPosition(spinnerEstadoEmisor,conductorAsegurado.getEstadoLicencia()));
            downloadTiposLicencia(conductorAsegurado.getEstadoLicencia());
        }
        if (conductorAsegurado.getLicenciaPermanente() != null) {
            checkBoxPermanente.setChecked(conductorAsegurado.getLicenciaPermanente());
        }
        if (conductorAsegurado.getTipoLicencia() != null) {
            spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, conductorAsegurado.getTipoLicencia()));
        }
        dateVigencia.setText(conductorAsegurado.getFechaFinLicencia() != null ? DateUtils.date(conductorAsegurado.getFechaFinLicencia()) : "");
        if (!conductorAsegurado.getLesiones().isEmpty()) {
            generalInjuredFragment.loadDataList(conductorAsegurado.getLesiones());
        }
        btnGuardarConductor.setVisibility(View.VISIBLE);
    }




}
