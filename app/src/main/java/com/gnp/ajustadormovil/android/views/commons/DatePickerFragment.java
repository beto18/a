package com.gnp.ajustadormovil.android.views.commons;
/*
 * Created by NOE on 08/02/2018.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnSelectetTime;

import org.jetbrains.annotations.NotNull;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Field;


public class DatePickerFragment extends BaseDialogFragment implements View.OnClickListener {

    private OnSelectetTime listener;
    private String TAG = DatePickerFragment.class.getCanonicalName();
    private DatePicker datePicker;
    private Button btnAceptar, btnCancelar;
    private TextView diaFecha;
    private View view;
    private LinearLayout linearLayout;
    private String age;
    private String tag;
    private int dia, mes, anio;

    public static DatePickerFragment newInstance(OnSelectetTime listener) {
        DatePickerFragment pickerFragment = new DatePickerFragment();
        pickerFragment.setListener(listener);
        return pickerFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.dialogThemeDate);


    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.datapicker_sipnner, container, false);
        tag = getTag();
        return view;

    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        datePicker = view.findViewById(R.id.datePicker);
        btnAceptar = view.findViewById(R.id.btnAceptar);
        btnCancelar = view.findViewById(R.id.btnCancel);
        diaFecha = view.findViewById(R.id.diaFecha);
        linearLayout = view.findViewById(R.id.linearLayout);
        btnAceptar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
        initializeDataPicker();
    }

    private void initializeDataPicker() {
        LocalDate currentDate = LocalDate.now();
        diaFecha.setText(currentDate.toString(dateFormated()).replace(",", " de"));
        if (tag.equals("dataPickerBirthday")) {

            datePicker.setMaxDate(System.currentTimeMillis());
        }
        dia = currentDate.getDayOfMonth();
        mes = currentDate.getMonthOfYear();
        anio = currentDate.getYear();
        age = calculateAge(currentDate.getYear(), currentDate.getMonthOfYear(), currentDate.getDayOfMonth());
        //listener.onTimeSelected(datePicker,getDate(currentDate.getDayOfMonth(),currentDate.getMonthOfYear(),currentDate.getYear()),age);
        datePicker.init(currentDate.getYear(), currentDate.getMonthOfYear(), currentDate.getDayOfMonth(), (datePicker, y, m, d) -> {
            dia = d;
            mes = m + 1;
            anio = y;
            age = calculateAge(y, m + 1, d);
            if (age.contains("-") && tag.equals("dataPickerBirthday")) {
                diaFecha.setText("");
                new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "La fecha seleccionada es incorreta");
            }
            //listener.onTimeSelected(datePicker,getDate(d,m+1,y), age);

            LocalDate selectedDate = new LocalDate(y, m + 1, d);
            diaFecha.setText(selectedDate.toString(dateFormated()).replace(",", " de"));
        });


        ChangeColorLine();
    }

    private DateTimeFormatter dateFormated() {
        return DateTimeFormat.forPattern("d, MMMM, yyyy");
    }

    private String getDate(int day, int month, int year) {
        return twoDigits(day) + "/" + twoDigits(month) + "/" + year;
    }

    private String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }


    private String calculateAge(int year, int month, int day) {
        LocalDate birthdate = new LocalDate(year, month, day);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);

        return String.valueOf(age.getYears());
    }


    private void ChangeColorLine() {
        DatePicker dpView = datePicker;
        LinearLayout llFirst = (LinearLayout) dpView.getChildAt(0);
        LinearLayout llSecond = (LinearLayout) llFirst.getChildAt(0);
        for (int i = 0; i < llSecond.getChildCount(); i++) {
            NumberPicker picker = (NumberPicker) llSecond.getChildAt(i);
            Field[] pickerFields = NumberPicker.class.getDeclaredFields();
            for (Field pf : pickerFields) {
                if (pf.getName().equals("mSelectionDivider")) {
                    pf.setAccessible(true);
                    try {
                        pf.set(picker, getResources().getDrawable(R.drawable.divider_orange));
                    } catch (Exception e) {
                        Log.e(TAG, "Error al pintar las lineas en dataPicker");
                    }
                    break;
                }
            }
        }
    }

    public void setListener(OnSelectetTime listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null) {
            if (view.getId() == R.id.btnAceptar) {
                if (getDate(dia, mes, anio) != null && calculateAge(anio, mes, dia) != null) {
                    listener.onTimeSelected(datePicker, getDate(dia, mes, anio), calculateAge(anio, mes, dia));
                }
                getDialog().dismiss();
            } else if (view.getId() == R.id.btnCancel) {
                getDialog().dismiss();
            }
        }
    }


}
