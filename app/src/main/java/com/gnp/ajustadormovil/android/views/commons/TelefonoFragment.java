package com.gnp.ajustadormovil.android.views.commons;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class TelefonoFragment extends BaseFragment {


    private AppCompatActivity activity;
    private CompositeDisposable compositeDisposable;
    private SiniestrosInteractor siniestrosInteractor;
    @BindView(R.id.spinnerTipoTelefono)
    Spinner spinnerTipoTelefono;
    @BindView(R.id.etPhone)
    EditText eTextTelefono;
    @BindView(R.id.etExtension)
    EditText eTextExtension;
    @BindView(R.id.titleExtencion)
    TextView titleExtension;
    private View view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = getCompositeDisposable();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_add_phone, container, false);
        ButterKnife.bind(this,view);
        return view;
    }


    private void getTiposTelefonos() {
        compositeDisposable.add(siniestrosInteractor.getTiposTelefono()
                .subscribe(tiposTelefonos -> {
                    AdapterSpinnerGeneral<CatalogoClaveAmis> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, tiposTelefonos, CatalogoClave::getNombre);
                    spinnerTipoTelefono.setAdapter(adapter);
                }, throwable -> {
                    tiposTelefonos = new ArrayList<>();
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                    Log.e(TAG, "Error al obtener los tipos de telefonos", throwable);
                }));

    }





    private void showDialogAddPhone() {


        spinnerTipoTelefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (((CatalogoClave) spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                    eTextExtension.setVisibility(View.VISIBLE);
                    titleExtension.setVisibility(View.VISIBLE);
                } else {
                    eTextExtension.setVisibility(View.GONE);
                    titleExtension.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }



}
