package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;

import java.util.List;

public class AdapterSpinnerGeneral<T>  extends ArrayAdapter<T> {

    public interface PrinterValue<T> {
        String print(T value);
    }


        // Your sent context
        private Context context;
        // Your custom values for the spinner (User)
        private List<T> values;
        private final PrinterValue<T> printer;

        public AdapterSpinnerGeneral(Context context, int textViewResourceId, List<T> values, PrinterValue<T> printer) {
            super(context, textViewResourceId, values);
            this.context = context;
            this.values = values;
            this.printer = printer;
        }

        @Override
        public int getCount(){
            return values.size();
        }

        @Override
        public T getItem(int position){
            return values.get(position);
        }

        @Override
        public long getItemId(int position){
            return position;
        }


        // And the "magic" goes here
        // This is for the "passive" state of the spinner
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
            TextView label = (TextView) super.getView(position, convertView, parent);
            //label.setBackgroundResource(R.color.colorWhite);
            //label.setTextColor(Color.BLACK);
            //label.setText(values.get(position).toString());
            // Then you can get the current item using the values array (Users array) and the current position
            // You can NOW reference each method you has created in your bean object (User class)
            //label.setTextColor(Color.BLACK);
            label.setText(printer.print(values.get(position)));


            // And finally return your dynamic (or custom) view for each spinner item
            return label;
        }

        // And here is when the "chooser" is popped up
        // Normally is the same view, but you can customize it if you want@Override
       /* public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            TextView label = (TextView) super.getDropDownView(position, convertView, parent);
            label.setBackgroundResource(R.color.colorWhite);
            label.setTextColor(Color.BLACK);
            label.setText(values.get(position).toString());

            return label;
        }*/


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomDropDownView(position, convertView, parent);
    }

    public View getCustomDropDownView(int position, View convertView, ViewGroup parent){
        View mySpinnerLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_list_custom_multiple_dropdown, parent,false);
        TextView main_text = mySpinnerLayout.findViewById(R.id.spinnerCustomText);
        View lineDivider = mySpinnerLayout.findViewById(R.id.line_spinner);
        if (position == values.size()-1){
            lineDivider.setVisibility(View.GONE);
        }
        if(values != null && !values.isEmpty()){
            main_text.setText(printer.print(values.get(position)));
        }
        else{
            main_text.setText(printer.print(getItem(position)));

        }
        return mySpinnerLayout;
    }







}
