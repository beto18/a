package com.gnp.ajustadormovil.android.views.commons.adapters;
/*
 * Created by NOE on 09/02/2018.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class CorreoAdapter extends RecyclerView.Adapter<CorreoAdapter.CorreoHolder> {

    private final List<String> correoList;
    private final OnClickListener clickListener;

    public CorreoAdapter(OnClickListener clickListener) {
        this.clickListener = clickListener;
        this.correoList = new LinkedList<>();
    }

    public void addAllString(List<String> list){
        for (String correo : list) {
            add(correo);
        }
    }

    public void addAll(List<String> results) {
        correoList.clear();
        for (String result : results) {
            add(result);
        }
    }

    public void add(String r) {
        correoList.add(r);
        notifyItemInserted(correoList.size() - 1);
    }

    public void remove(String r) {
        int position = correoList.indexOf(r);
        if (position > -1) {
            correoList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        final int size = correoList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                correoList.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    public String getCorreo(int position) {
        return correoList.get(position);
    }


    public RealmList<String> getCorreoList() {
        RealmList<String> list = new RealmList<>();
        for (String correo : correoList) {
            list.add(correo);
        }
        return list;
    }


    @Override
    public CorreoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_correo, parent, false);
        return new CorreoHolder(mView);
    }

    @Override
    public void onBindViewHolder(CorreoHolder holder, int position) {
        holder.email.setText(correoList.get(position));
    }

    @Override
    public int getItemCount() {
        return correoList.size();
    }

    public interface OnClickListener {
        void onClick(int position);
    }

    class CorreoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.delete)
        ImageButton delete;

        CorreoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(getAdapterPosition());
        }
    }
}
