package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmptyHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvEmptyItem)
    public TextView tvEmptyItem;

    public EmptyHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
