package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.utils.media.CircleTransform;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by abraham on 15/04/18.
 *
 */

public class ImagesWSOcupanteAdapter extends RecyclerView.Adapter<ImagesWSOcupanteAdapter.ImagesWSOcupanteHolder>{

    private EventBus bus = EventBus.getDefault();
    private List<Fotografia> fotografias;

    public ImagesWSOcupanteAdapter() {
        this.fotografias = new ArrayList<>();
    }

    public void addAll(List<Fotografia> results) {
        for (Fotografia result : results) {
            add(result);
        }
    }

    public void add(Fotografia r) {
        fotografias.add(r);
        notifyItemInserted(fotografias.size() - 1);
    }

    public void remove(Fotografia r) {
        int position = fotografias.indexOf(r);
        if (position > -1) {
            fotografias.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Fotografia getFotografia(int position){
        return fotografias.get(position);
    }

    public List<Fotografia> getFotografias(){
        return fotografias;
    }

    public void clear() {
        final int size = fotografias.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                fotografias.remove(0);
            }
            notifyItemRangeRemoved(0, size);
        }
    }

    @NonNull
    @Override
    public ImagesWSOcupanteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images_ws,parent,false);
        return new ImagesWSOcupanteHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesWSOcupanteHolder holder, int position) {
        Picasso.with(holder.itemView.getContext())
                .load(fotografias.get(position).getUrl())
                .placeholder(R.drawable.ic_no_photo)
                .transform(new CircleTransform())
                .error(R.drawable.ic_close_orange)
                .into(holder.imgWS);
    }

    @Override
    public int getItemCount() {
        return fotografias.size();
    }

    class ImagesWSOcupanteHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.imgWSAsegurado)
        ImageView imgWS;

        ImagesWSOcupanteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnLongClickListener(v -> {
                bus.post(getFotografia(getAdapterPosition()));
                return false;
            });
        }
    }

}
