package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> {

    private List<String> folioInicialList;
    private List<String> folioList;

    public ItemAdapter(List<String> folioInicialList, List<String> folioList) {
        this.folioInicialList = folioInicialList;
        this.folioList = folioList;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.label_item, parent, false);
        return new ItemHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        if (position == 0){
            holder.tvTitleFolio.setText(ResourceUtils.getString("propio"));
        }
        else {
            holder.tvTitleFolio.setText(String.format("%s%s%s",ResourceUtils.getString("contraparte"), " ", position));
        }
        holder.tvFolioInicial.setText(folioInicialList.get(position));
        holder.tvFolio.setText(folioList.get(position));
    }

    @Override
    public int getItemCount() {
        return folioInicialList.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        TextView tvFolio;
        TextView tvFolioInicial;
        TextView tvTitleFolio;

        public ItemHolder(View itemView) {
            super(itemView);
            tvFolioInicial = itemView.findViewById(R.id.tvFolioInicial);
            tvFolio = itemView.findViewById(R.id.tvFolio);
            tvTitleFolio = itemView.findViewById(R.id.tvTitleFolio);
        }
    }
}
