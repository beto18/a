package com.gnp.ajustadormovil.android.views.commons.adapters;

/**
 * Created by Lima on 05/10/17.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by egamarti on 03/09/15.
 */
public class PagerAdapter_Fragment extends FragmentPagerAdapter {

    private static final String TAG = PagerAdapter_Fragment.class.getCanonicalName();

    private List<Class<? extends Fragment>> fragments;
    private List<Bundle> bundles;
    private List<String> titles;

    private FragmentManager mFragmentManager;

    public PagerAdapter_Fragment(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mFragmentManager = fragmentManager;
        this.fragments = new ArrayList<>();
        this.bundles = new ArrayList<>();
        this.titles = new ArrayList<>();
    }

    public void addFragment(Fragment fragment){

    }

    public void addFragment(Class<? extends Fragment> clazz) {
        this.fragments.add(clazz);
    }

    public void addFragment(Class<? extends Fragment> clazz,Bundle bundle) {
        this.fragments.add(clazz);
        this.bundles.add(bundle);
    }

    public void addFragment(Class<? extends Fragment> clazz,String title) {
        this.fragments.add(clazz);
        this.titles.add(title);
    }

    public void addFragment(Class<? extends Fragment> clazz,String title,Bundle bundle) {
        this.fragments.add(clazz);
        this.titles.add(title);
        this.bundles.add(bundle);
    }

    public void removeFragment(int position){
        if(!this.fragments.isEmpty() &&  this.fragments.size() > position){
            this.fragments.remove(position);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(titles.size() > position){
            return titles.get(position);
        }
        else {
            return super.getPageTitle(position);
        }
    }

    @Override
    public Fragment getItem(int position) {
        // TODO Auto-generated method stub
        Fragment fragment = null;
        try {
            Method m = this.fragments.get(position).getDeclaredMethod("newInstance", (Class<?>[]) null);
            fragment = (Fragment) m.invoke(null, (Object[]) null);
            if(bundles.size() > position){
                fragment.setArguments(bundles.get(position));
            }
        }  catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Log.e(TAG, "Error al crear fragment", e);
        }
        return  fragment;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.fragments.size();
    }

    public FragmentManager getFragmentManager() {
        return mFragmentManager;
    }

    public void setFragmentManager(FragmentManager mFragmentManager) {
        this.mFragmentManager = mFragmentManager;
    }
}
