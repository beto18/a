package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gnp.ajustadormovil.android.R;
import com.squareup.picasso.Picasso;

/**
 * Created by oscar on 14/02/18.
 */

public class PagerImagesAdapter extends PagerAdapter {
    Context context;
    String[] images;
    //int images[];
    LayoutInflater layoutInflater;

    public PagerImagesAdapter(Context context, String[] images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);
        System.out.println(images[position]);
        String urlImage = images[position];
        urlImage=urlImage.replace("https://storage.cloud.google.com/gnp-siniestros-documentos","https://storage.googleapis.com/gnp-siniestros-documentos");
        System.out.println(urlImage);
        Picasso.with(context)
                .load(urlImage)
                .placeholder(R.drawable.auto)
                .into(imageView);
        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
