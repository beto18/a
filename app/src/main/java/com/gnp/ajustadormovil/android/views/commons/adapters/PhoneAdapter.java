package com.gnp.ajustadormovil.android.views.commons.adapters;
/*
 * Created by NOE on 09/02/2018.
 */

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhoneAdapter extends RecyclerView.Adapter<PhoneAdapter.CorreoHolder> {

    private final List<Telefono> telefonosResponseList;
    private final OnClickListener clickListener;

    public PhoneAdapter(OnClickListener clickListener) {
        this.clickListener = clickListener;
        this.telefonosResponseList = new LinkedList<>();
    }

    public void addAll(List<Telefono> results) {
        telefonosResponseList.clear();
        for (Telefono result : results) {
            add(result);
        }
    }

    public void add(Telefono r) {
        telefonosResponseList.add(r);
        notifyItemInserted(telefonosResponseList.size() - 1);
    }

    public void remove(Telefono r) {
        int position = telefonosResponseList.indexOf(r);
        if (position > -1) {
            telefonosResponseList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        final int size = telefonosResponseList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                telefonosResponseList.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }


    public Telefono getTelono(int position) {
        return telefonosResponseList.get(position);
    }


    public List<Telefono> getTelefonos() {
        return telefonosResponseList;
    }


    @Override
    public CorreoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_correo, parent, false);
        return new CorreoHolder(mView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(CorreoHolder holder, int position) {
        holder.email.setText(telefonosResponseList.get(position).getTelefono());
    }

    @Override
    public int getItemCount() {
        return telefonosResponseList.size();
    }

    public interface OnClickListener {
        void onClick(int position);
    }

    class CorreoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.email)
        TextView email;
        @BindView(R.id.delete)
        ImageButton delete;

        CorreoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(getAdapterPosition());
        }
    }
}
