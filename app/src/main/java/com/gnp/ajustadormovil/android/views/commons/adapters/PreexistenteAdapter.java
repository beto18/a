package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by abraham on 23/03/18.
 */

public class PreexistenteAdapter extends RecyclerView.Adapter<PreexistenteAdapter.PreexistenteHolder>{

    private List<CatalogoId> areas;
    private CallbackClickItem clickListener;

    public PreexistenteAdapter(CallbackClickItem listener) {
        areas = new ArrayList<>();
        this.clickListener = listener;
    }


    public void addAll (List<CatalogoId> results) {
        for (CatalogoId result : results) {
            add (result);
        }
    }

    public List<CatalogoId> getAreasPreexistentes(){
        return areas;
    }

    public void add (CatalogoId r) {
        areas.add (r);
        notifyItemInserted (areas.size () - 1);
    }

    public void remove (CatalogoId r) {
        int position = areas.indexOf (r);
        if (position > -1) {
            areas.remove (position);
            notifyItemRemoved (position);
        }
    }

    public void remove (int position) {
        if (position > -1) {
            areas.remove (position);
            notifyItemRemoved (position);
        }
    }

    public void clear() {
        final int size = areas.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                areas.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    @NonNull
    @Override
    public PreexistenteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_parte_lesionada, parent, false);
        return new PreexistenteHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull PreexistenteHolder holder, int position) {
        holder.item.setText (areas.get (position).getNombre());
    }

    @Override
    public int getItemCount() {
        return areas.size();
    }

    class PreexistenteHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.tvItem)
        AppCompatTextView item;
        @BindView (R.id.imgDelete)
        ImageButton delete;

        PreexistenteHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClickItem(getAdapterPosition(),v);
        }
    }
}
