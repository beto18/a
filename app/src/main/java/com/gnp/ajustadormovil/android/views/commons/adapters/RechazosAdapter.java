package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;

import java.util.List;

public class RechazosAdapter extends RecyclerView.Adapter<RechazosAdapter.RechazosHolder>{

    private List<CatalogoClave> list;
    private int mSelectedItem = -1;
    private String key;

    public RechazosAdapter(List<CatalogoClave> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RechazosHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cancelaciones,parent,false);
        return new RechazosHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull RechazosHolder holder, int position) {
        if (list.get(position).getNombre()!=null){
            holder.rbRechazo.setText(list.get(position).getNombre());
        }
        holder.rbRechazo.setChecked(position==mSelectedItem);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public String getKey(){
        return key;
    }


    class RechazosHolder extends RecyclerView.ViewHolder{

        private RadioButton rbRechazo;

        public RechazosHolder(View itemView) {
            super(itemView);
            rbRechazo = itemView.findViewById(R.id.rbCancelacion);
            View.OnClickListener clickListener = v -> {
                if (getAdapterPosition()!=-1) {
                    mSelectedItem = getAdapterPosition();
                    key = list.get(getAdapterPosition()).getClave();
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);
            rbRechazo.setOnClickListener(clickListener);
        }
    }
}
