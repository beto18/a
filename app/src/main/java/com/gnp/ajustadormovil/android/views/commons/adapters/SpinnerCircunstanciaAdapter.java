package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.CircunstanciasModel;

import java.util.List;

/**
 * Created by oscar on 14/02/18.
 */

public class SpinnerCircunstanciaAdapter extends ArrayAdapter {
    private Context context;
    private List<CircunstanciasModel> itemList;
    LayoutInflater layoutInflater;

    public SpinnerCircunstanciaAdapter(Context context, int textViewResourceId, List<CircunstanciasModel> itemList) {
        super(context, textViewResourceId, itemList);
        this.context=context;
        this.itemList=itemList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View layout = layoutInflater.inflate(R.layout.spinner_circunstancias, parent, false);

        TextView itmId = layout.findViewById(R.id.spinnerItmID);
        TextView itmName = layout.findViewById(R.id.spinnerItm);

        itmId.setText(itemList.get(position).getId().toString()+".");
        itmName.setText(itemList.get(position).getNombre());
        return layout;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

//    public View getView(int position, View convertView, ViewGroup parent) {
//        View row = layoutInflater.inflate(R.layout.spinner_circunstancias, parent, false);
//        TextView v = (TextView) row.findViewById(R.id.spinnerItm);
//        v.setText(itemList.get(position).getNombre());
//        return row;
//    }
//
//
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//
//        View row = layoutInflater.inflate(R.layout.spinner_circunstancias, parent, false);
//        TextView v = (TextView) row.findViewById(R.id.spinnerItm);
//        v.setText(itemList.get(position).getNombre());
//        return row;
//    }

}
