package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abraham on 11/12/2017.
 */

@SuppressLint ("NewApi")
public class TelefonoAdapter extends RecyclerView.Adapter<TelefonoAdapter.TelefonoHolder> implements View.OnContextClickListener {

    private List<Telefono> telefonos;

    public TelefonoAdapter (List<Telefono> telefonos) {
        this.telefonos = telefonos;
    }

    @Override
    public TelefonoHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_telefono, parent, false);
        return new TelefonoHolder (mView);
    }

    @Override
    public void onBindViewHolder (TelefonoHolder holder, int position) {
        holder.posTelefono.setText (String.format ("%s%s", "Telefono ", position + 1));
        holder.tipoTelefono.setText (telefonos.get (position).getTipo ());
        holder.numero.setText (telefonos.get (position).getTelefono ());
        holder.extension.setText (telefonos.get (position).getExtension ());
    }


    @Override
    public int getItemCount () {
        return telefonos.size ();
    }

    @Override
    public boolean onContextClick (View v) {
        return false;
    }

    class TelefonoHolder extends RecyclerView.ViewHolder {

        @BindView (R.id.tvPosTelefono)
        AppCompatTextView posTelefono;
        @BindView (R.id.tvTipoTelefono)
        AppCompatTextView tipoTelefono;
        @BindView (R.id.tvNumero)
        AppCompatTextView numero;
        @BindView (R.id.tvExtension)
        AppCompatTextView extension;

        TelefonoHolder (View itemView) {
            super (itemView);
            ButterKnife.bind (this, itemView);
        }
    }
}
