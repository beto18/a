package com.gnp.ajustadormovil.android.views.commons.adapters;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.views.commons.events.OnTelefonoListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abraham on 29/01/2018.
 */

public class TelefonoTerceroAdapter extends RecyclerView.Adapter<TelefonoTerceroAdapter.TelefonoTerceroHolder> {

    private List<Telefono> telefonos;
    private OnTelefonoListener listener;

    public TelefonoTerceroAdapter (OnTelefonoListener listener) {
        this.listener = listener;
        this.telefonos = new ArrayList<> ();
    }

    public void addAll (List<Telefono> results) {
        for (Telefono result : results) {
            add (result);
        }
    }

    public void add (Telefono r) {
        telefonos.add (r);
        notifyItemInserted (telefonos.size () - 1);
    }

    public void remove (Telefono r) {
        int position = telefonos.indexOf (r);
        if (position > -1) {
            telefonos.remove (position);
            notifyItemRemoved (position);
        }
    }

    public List<Telefono>getTelefonos(){
        return telefonos;
    }


    @Override
    public TelefonoTerceroHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_list_correo, parent, false);
        return new TelefonoTerceroHolder (mView);
    }

    @Override
    public void onBindViewHolder (TelefonoTerceroHolder holder, int position) {
        holder.item.setText (telefonos.get (position).getTelefono ());
    }

    @Override
    public int getItemCount () {
        return telefonos.size ();
    }

    public Telefono getTelefono (int position) {
        return telefonos.get (position);
    }

    class TelefonoTerceroHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView (R.id.email)
        AppCompatTextView item;
        @BindView (R.id.delete)
        ImageButton delete;

        TelefonoTerceroHolder (View itemView) {
            super (itemView);
            ButterKnife.bind (this, itemView);
            delete.setOnClickListener (this);
        }

        @Override
        public void onClick (View view) {
            listener.onDeleteClick (view, getAdapterPosition ());
        }
    }
}
