package com.gnp.ajustadormovil.android.views.commons.adapters.legacy;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by Abraham on 28/01/2018.
 */

public class CorreoAdapter extends RecyclerView.Adapter<CorreoAdapter.CorreoHolder> {

    private List<String> correos;
    private OnItemRemovedListener listener;

    public CorreoAdapter (OnItemRemovedListener listener) {
        this.listener = listener;
        this.correos = new ArrayList<> ();
    }

    public void addAll (List<String> results) {
        correos.clear();
        for (String result : results) {
            add (result);
        }
    }

    public void add (String r) {
        correos.add (r);
        notifyItemInserted (correos.size () - 1);
    }

    public void remove (String r) {
        int position = correos.indexOf (r);
        if (position > -1) {
            correos.remove (position);
            notifyItemRemoved (position);
        }
    }

    public void clear() {
        final int size = correos.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                correos.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    public List<String>getCorreos(){
        return correos;
    }

    public String getCorreo(int position){
        return correos.get (position);
    }

    @Override
    public CorreoHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_list_correo, parent, false);
        return new CorreoHolder (mView);
    }

    @Override
    public void onBindViewHolder (CorreoHolder holder, int position) {
        holder.item.setText (correos.get (position));
    }

    @Override
    public int getItemCount () {
        return correos.size ();
    }

    class CorreoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView (R.id.email)
        AppCompatTextView item;
        @BindView (R.id.delete)
        ImageButton delete;

        CorreoHolder (View itemView) {
            super (itemView);
            ButterKnife.bind (this, itemView);
            delete.setOnClickListener (this);
        }

        @Override
        public void onClick (View view) {
            listener.onItemClick (view, getAdapterPosition ());
        }
    }
}
