package com.gnp.ajustadormovil.android.views.commons.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;

import com.crashlytics.android.Crashlytics;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.services.SiniestroService;
import com.gnp.ajustadormovil.android.services.UbicacionService;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.utils.components.DialogProgressUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Lima on 09/11/17.
 * Modified by hgzavala on 17/04/2018
 */

public class BaseActivity extends AppCompatActivity implements MvpView, ComponentCallbacks2 {

    protected final String TAG = this.getClass().getSimpleName();
    private static Activity activity;
    private static Intent intentActualizaUbicacion;
    private static Intent intentFireBaseService;
    public final int ACTIVITY_CONFIRM = 0;
    public final int ACTIVITY_CAPTURE = 1;
    public int newActivity;
    public GeneralSnackBar generalSnackBar;
    private Dialog dialog;
    public static final String GCM_ONEOFF_TAG = "oneoff|[0,0]";
    public static final String GCM_REPEAT_TAG = "repeat|[7200,1800]";

    boolean netIsActive;
    boolean gpsIsActive;
    private Menu menu;

    Toolbar mToolbar;
    @Nullable
    ProgressDialog simpleProgressDialog;
    @Nullable
    @BindView(R.id.gpsline)
    LinearLayoutCompat mGPSline;
    private CompositeDisposable compositeDisposable;
    private DialogProgressUtils dialogsUtil;
    private BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().matches(LocationManager.PROVIDERS_CHANGED_ACTION)) {

                final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(true);

                    if (mGPSline != null) {
                        mGPSline.setBackgroundResource(R.color.colorGreen);
                        gpsIsActive = true;
                        try {
                            updatedMenu();
                        } catch (Exception e) {
                            Log.w(TAG, "Error al actualizar menu");
                        }
                    }
                } else if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    FirebaseDatabase.getInstance().getReference(Constants.ADJUSTER).child(AjustadorMovilApplication.getCedula()).child("banEstGPS").setValue(false);
                    if (mGPSline != null) {
                        mGPSline.setBackgroundResource(R.color.colorRedStatus);
                        gpsIsActive = false;
                        try {
                            updatedMenu();
                        } catch (Exception e) {
                            Log.w(TAG, "Error al actualizar menu");
                        }
                    }
                }
            }
        }
    };

    private BroadcastReceiver statusChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Objects.equals(intent.getAction(), Constants.STATUS_CHANGED)) {
                invalidateOptionsMenu();
            }
        }
    };

    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean connection = ConnectionProvider.getInstance().isConnected();
            if (connection) {
                onConnected();
            } else {
                onDisconnected();
            }
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for(Fragment fragment : fragments){
                if(fragment instanceof ConnectableFragment){
                    if (connection) {
                        ((ConnectableFragment)fragment).onConnected();
                    } else {
                        ((ConnectableFragment)fragment).onDisconnected();
                    }
                }
            }


        }
    };

    public static void stopService() {
        Log.w("BaseActivity", "Parar servicios");
        if (activity != null) {
            if (intentFireBaseService != null) activity.stopService(intentFireBaseService);
            if (intentActualizaUbicacion != null) activity.stopService(intentActualizaUbicacion);
        }
    }

    public static BaseActivity getBaseActivity() {
        return (BaseActivity) activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        generalSnackBar = new GeneralSnackBar();
        compositeDisposable = new CompositeDisposable();
        dialogsUtil = new DialogProgressUtils(this);

        if (intentActualizaUbicacion == null) {
            intentActualizaUbicacion = new Intent(this, UbicacionService.class);
        }

        //initTracker();//TODO segunda fase
        if (intentFireBaseService == null) {
            intentFireBaseService = new Intent(this, SiniestroService.class);
        }

        registerReceiver(gpsReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.STATUS_CHANGED);
        intentFilter.addAction(Constants.ADJUST_REMOVED);
        registerReceiver(statusChangedReceiver,intentFilter);
        if (!isMyServiceRunning(UbicacionService.class)) {
            try {
                startService(intentActualizaUbicacion);
            } catch (Exception e) {
                Log.e(TAG, "Error al registrar servicio de ubicación", e);
                Crashlytics.log(Log.ERROR, TAG, "Error al registrar servicio de ubicación" + e.getMessage());
            }
        }
        if (!isMyServiceRunning(SiniestroService.class)) {
            try {
                startService(intentFireBaseService);
            } catch (Exception e) {
                Log.e(TAG, "Error al registrar servicio de ubicación", e);
            }
        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    public DialogProgressUtils getDialogsUtil() {
        return dialogsUtil;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public void updatedMenu() {
        MenuItem wielesMenuItem = menu.findItem(R.id.tools_wireles);
        MenuItem gpsMenuItem = menu.findItem(R.id.tools_gps);
        if (gpsIsActive) {
            gpsMenuItem.setTitle(R.string.txt_tools_gps_on);
            gpsMenuItem.setIcon(R.drawable.baseline_location_on_black_24dp);
        } else {
            gpsMenuItem.setTitle(R.string.txt_tools_gps_off);
            gpsMenuItem.setIcon(R.drawable.baseline_location_off_black_24dp);
        }
        if (netIsActive) {
            wielesMenuItem.setTitle(R.string.txt_tools_wirles_on);
            wielesMenuItem.setIcon(R.drawable.baseline_wifi_black_24dp);
        } else {
            wielesMenuItem.setTitle(R.string.txt_tools_wirles_off);
            wielesMenuItem.setIcon(R.drawable.baseline_wifi_off_black_24dp);
        }
    }

    public RadioGroup getmRadioGroup() {
        //return mRadioGroup;
        return null;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isMyServiceRunning(UbicacionService.class)) {
            try {
                startService(intentActualizaUbicacion);
            } catch (Exception e) {
                Log.e(TAG, "Error al registrar servicio de ubicación", e);
                Crashlytics.log(Log.ERROR, TAG, "Error al registrar servicio de ubicación" + e.getMessage());
            }
        }
    }

    public void setmRadioGroup(RadioGroup mRadioGroup) {
        //this.mRadioGroup = mRadioGroup;
        //mRadioGroup.setOnCheckedChangeListener(this);
    }

    public Toolbar getmToolbar() {
        return mToolbar;
    }

    public void setmToolbar(Toolbar mToolbar) {
        this.mToolbar = mToolbar;
        setSupportActionBar(mToolbar);
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(gpsReceiver);
        unregisterReceiver(networkReceiver);
        unregisterReceiver(statusChangedReceiver);
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onAttach(Context context) {
    }

    protected void mostrarDialogoEspera(String message) {
        if (getApplicationContext() != null && simpleProgressDialog == null) {
            simpleProgressDialog = new ProgressDialog(this);
            simpleProgressDialog.setMessage(message);
            simpleProgressDialog.setCancelable(false);
            simpleProgressDialog.setCanceledOnTouchOutside(false);
            if (!isFinishing()) {
                simpleProgressDialog.show();
            } else {
                simpleProgressDialog.dismiss();
            }
        }
    }

    protected void ocultarDialogoEspera() {
        if (simpleProgressDialog != null)
            try {
                simpleProgressDialog.dismiss();
                simpleProgressDialog = null;
            } catch (Exception ignored) {
            }
    }

    public void validateLocation() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            DialogUtils.alert(this, "GPS", "Favor de habilitar su GPS.", "Entendido", (dialogInterface, i) ->
            {
                //startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            });

        }
    }

    public static ArrayList<Integer> getScreenSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        ArrayList<Integer> tamano = new ArrayList<Integer>();
        tamano.add(width);
        tamano.add(height);

        return tamano;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
    }

    protected void onConnected() {
        Log.i(TAG, "Con conexión a internet");
        netIsActive = true;
        try {
            updatedMenu();
        } catch (Exception e) {
            Log.w(TAG, "Error al actualizar menu");
        }
    }

    protected void onDisconnected() {
        Log.i(TAG, "Sin conexión a internet");
        netIsActive = false;
        try {
            updatedMenu();
        } catch (Exception e) {
            Log.w(TAG, "Error al actualizar menu");
        }
    }

}
