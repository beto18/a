package com.gnp.ajustadormovil.android.views.commons.base;

import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class BaseFragment extends Fragment implements ConnectableFragment{

    protected final String TAG = this.getClass().getSimpleName();

    private CompositeDisposable compositeDisposable;

    @Override
    public void onConnected(){
        Log.d(TAG, "Conectado a internet");
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        for(Fragment fragment : fragments){
            if(fragment instanceof ConnectableFragment){
                ((ConnectableFragment)fragment).onConnected();
            }
        }
    }

    @Override
    public void onDisconnected(){
        Log.d(TAG, "Desconectado de internet");
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        for(Fragment fragment : fragments){
            if(fragment instanceof ConnectableFragment){
                ((ConnectableFragment)fragment).onDisconnected();
            }
        }
    }

    protected CompositeDisposable getCompositeDisposable(){
        if(compositeDisposable == null || compositeDisposable.isDisposed()){
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    @Override
    public void onDestroy() {
        if(compositeDisposable != null){
            compositeDisposable.dispose();
        }
        super.onDestroy();
    }
}
