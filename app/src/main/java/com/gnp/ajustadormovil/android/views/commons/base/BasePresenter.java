package com.gnp.ajustadormovil.android.views.commons.base;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    protected final String TAG = this.getClass().getCanonicalName();

    private V mMvpView;
    private CompositeDisposable mCompositeDisposable;

    public BasePresenter(CompositeDisposable compositeDisposable) {
        this.mCompositeDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V mvpView) {
        this.mMvpView = mvpView;
    }

    public V getMvpView() {
        return mMvpView;
    }


    public CompositeDisposable getmCompositeDisposable() {
        return mCompositeDisposable;
    }
}
