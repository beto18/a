package com.gnp.ajustadormovil.android.views.commons.base;

public interface ConnectableFragment {

    void onConnected();

    void onDisconnected();
}
