package com.gnp.ajustadormovil.android.views.commons.base;

@FunctionalInterface
public interface MvpPresenter<V extends MvpView> {
    void onAttach(V mvpView);
}
