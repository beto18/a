package com.gnp.ajustadormovil.android.views.commons.custom

import android.os.Build
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.view.View

class MoveUpwardBehavior : CoordinatorLayout.Behavior<View>() {

    override fun layoutDependsOn(parent: CoordinatorLayout?, child: View?, dependency: View?): Boolean {
        return SNACKBAR_BEHAVIOR_ENABLED && dependency is Snackbar.SnackbarLayout
    }

//    override fun onDependentViewChanged(parent: CoordinatorLayout?, child: View?, dependency: View?): Boolean {
//        if (parent == null || child == null || dependency == null)
//            return false
//
//        val distanceY = getViewOffsetForSnackbar(parent, child)
//        val fractionComplete = distanceY / dependency.height
//        val scaleFactor = 1 - fractionComplete
//
//        child.scaleX = scaleFactor
//        child.scaleY = scaleFactor
//        return true
//    }
//
//    private fun getViewOffsetForSnackbar(parent: CoordinatorLayout, view: View): Float{
//        var maxOffset = 0f
//        val dependencies = parent.getDependencies(view)
//
//        dependencies.forEach { dependency ->
//            if (dependency is Snackbar.SnackbarLayout && parent.doViewsOverlap(view, dependency)){
//                maxOffset = Math.max(maxOffset, (dependency.translationY - dependency.height) * -1)
//            }
//        }
//
//        return maxOffset
//    }


    override fun onDependentViewChanged(parent: CoordinatorLayout?, child: View?, dependency: View?): Boolean {
        val translationY = Math.min(0f, dependency!!.translationY - dependency.height)
        child!!.translationY = translationY
        return true
    }

    companion object {
        private val SNACKBAR_BEHAVIOR_ENABLED: Boolean

        init {
            SNACKBAR_BEHAVIOR_ENABLED = Build.VERSION.SDK_INT >= 11
        }
    }
}