package com.gnp.ajustadormovil.android.views.commons.custom;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.view.ViewGroup;

public class SnackbarHelper {

    public static void configSnackbar(Context context, Snackbar snack, int  color) {
        addMargins(snack);
        setRoundBordersBg(context, snack, color);
        ViewCompat.setElevation(snack.getView(), 6f);
    }

    private static void addMargins(Snackbar snack) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) snack.getView().getLayoutParams();
        params.setMargins(12, 12, 12, 12);
        snack.getView().setLayoutParams(params);
    }

    private static void setRoundBordersBg(Context context, Snackbar snackbar, int color ) {
        snackbar.getView().setBackground(context.getDrawable(color));
    }
}