package com.gnp.ajustadormovil.android.views.commons.custom;

/**
 * Created by oscar on 08/11/17.
 */


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by egamarti on 03/09/15.
 */
public class ViewPagerCustom extends ViewPager {
    private boolean swipe;

    public ViewPagerCustom(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public ViewPagerCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        return isSwipe() && super.onInterceptTouchEvent(arg0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Inhabilita swipe
        return isSwipe() && super.onTouchEvent(event);
    }

    public boolean isSwipe() {
        return swipe;
    }

    public void setSwipe(boolean swipe) {
        this.swipe = swipe;
    }
}
