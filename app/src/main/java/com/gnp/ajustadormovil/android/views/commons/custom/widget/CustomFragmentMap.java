package com.gnp.ajustadormovil.android.views.commons.custom.widget;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.SupportMapFragment;

public class CustomFragmentMap extends SupportMapFragment {
    private OnTouchListener listener;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View layout = super.onCreateView(layoutInflater, viewGroup, bundle);
        TouchableWrapper frameLayout = new TouchableWrapper(getActivity());
        frameLayout.setBackgroundColor(getBackgroundColor(android.R.color.transparent));
        ((ViewGroup) layout).addView(frameLayout, new ViewGroup.LayoutParams(-1, -1));
        return layout;
    }

    private int getBackgroundColor(int color) {
        return Build.VERSION.SDK_INT >= 23 ? getBackgroundColorPreMarshmallow(color) :
                ContextCompat.getColor(this.getContext(), color);
    }

    private int getBackgroundColorPreMarshmallow(int color) {
        return getResources().getColor(color);
    }

    public interface OnTouchListener {
        void onTouch();
    }

    public void setOnTouchListener(OnTouchListener listener) {
        this.listener = listener;
    }

    class TouchableWrapper extends FrameLayout {

        public TouchableWrapper(@NonNull Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    listener.onTouch();
                    break;
                case MotionEvent.ACTION_UP:
                    listener.onTouch();
                    break;
            }
            return super.dispatchTouchEvent(ev);
        }
    }
}
