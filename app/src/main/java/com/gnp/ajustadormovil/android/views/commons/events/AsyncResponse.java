package com.gnp.ajustadormovil.android.views.commons.events;

public interface AsyncResponse {
    void processFinish();
    void processError();

}
