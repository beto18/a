package com.gnp.ajustadormovil.android.views.commons.events

interface CallBackItemCheck {
    fun itemCheck(id:Long?=null,checked:Boolean)
    fun listChecks(list: List<Long>)
}