package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by tixmedia on 3/20/18.
 *
 */

public interface CallbackClickItem {
    void onItemClickItem(long position, View view);
}
