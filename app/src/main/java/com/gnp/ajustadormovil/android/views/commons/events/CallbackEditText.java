package com.gnp.ajustadormovil.android.views.commons.events;

public interface CallbackEditText {
    void onChangeEditText(int idEditText, int position);
}