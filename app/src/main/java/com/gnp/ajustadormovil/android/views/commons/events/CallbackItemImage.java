package com.gnp.ajustadormovil.android.views.commons.events;

public interface CallbackItemImage<K> {
    void onCallbackItemId(int position, K id);
    void onCallItemId(int position, K id);
}
