package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(int position, View view);
}
