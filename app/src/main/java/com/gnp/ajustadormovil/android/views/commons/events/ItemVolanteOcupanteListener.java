package com.gnp.ajustadormovil.android.views.commons.events;

public interface ItemVolanteOcupanteListener {
    void onOcupanteClick(int position);
}
