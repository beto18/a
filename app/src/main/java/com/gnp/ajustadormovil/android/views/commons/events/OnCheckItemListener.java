package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

public interface OnCheckItemListener {
    void onCheck(int position, View view);
}
