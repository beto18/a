package com.gnp.ajustadormovil.android.views.commons.events;

import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;

public interface OnChildSelect {
    void selectChild(Recuperacion recovery);
}
