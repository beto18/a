package com.gnp.ajustadormovil.android.views.commons.events;

/**
 * Created by abraham on 16/04/18.
 */

public interface OnFragmentChangerListener {
    void onChangeFragment();
    void onChargeData(boolean showGuardar);
}
