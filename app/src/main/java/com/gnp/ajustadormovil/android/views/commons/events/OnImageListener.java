package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by abraham on 11/04/18.
 */
public interface OnImageListener {
    void onClickImage(View view, int position);
    void onLongClickImage(View view, int position);
}
