package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by tixmedia on 4/21/18.
 */

public interface OnItemNoAutoListener {
    void ItemNoAuto(View view, int position);
}
