package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by Abraham on 05/02/2018.
 */

public interface OnItemOcupanteListener {
    void OnItemOcupante(View view, int position);
    void loadData();
}
