package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

public interface OnItemPeatonListener {
    void itemPeaton(View view, int position);
}
