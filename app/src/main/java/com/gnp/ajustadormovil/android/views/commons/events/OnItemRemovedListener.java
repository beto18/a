package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 *
 * Created by Lima on 10/10/17.
 */

public interface OnItemRemovedListener {
    void onItemClick(View view, int position);
    void onLongItemClick(View view, int position);
    void changeStatus(View view, int position);
}
