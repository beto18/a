package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by Abraham on 01/02/2018.
 */

public interface OnItemSelectedListener {
    void selectItem(View view, int position);
}
