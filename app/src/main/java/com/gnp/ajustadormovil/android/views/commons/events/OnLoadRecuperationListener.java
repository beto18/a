package com.gnp.ajustadormovil.android.views.commons.events;

public interface OnLoadRecuperationListener {
    void loadRecuperation(String type, String id_recuperation);
}
