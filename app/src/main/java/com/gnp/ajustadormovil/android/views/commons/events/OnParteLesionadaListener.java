package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by Abraham on 10/02/2018.
 */

public interface OnParteLesionadaListener {
    void onParteClick(View view, int position);
}
