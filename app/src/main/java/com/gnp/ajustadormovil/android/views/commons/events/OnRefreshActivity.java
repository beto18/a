package com.gnp.ajustadormovil.android.views.commons.events;

public interface OnRefreshActivity {

    void onRefreshActivity(boolean bandImagesGalery);
}
