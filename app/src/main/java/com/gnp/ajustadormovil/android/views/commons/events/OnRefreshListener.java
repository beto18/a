package com.gnp.ajustadormovil.android.views.commons.events;

/**
 * Created by tixmedia on 3/21/18.
 *
 */

public interface OnRefreshListener {
    void onRefreshListener();
}
