package com.gnp.ajustadormovil.android.views.commons.events;

import android.widget.DatePicker;

public interface OnSelectetTime {

    void onTimeSelected(DatePicker datePicker, String date, String age);
}
