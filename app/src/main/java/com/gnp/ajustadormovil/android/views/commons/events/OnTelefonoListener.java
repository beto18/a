package com.gnp.ajustadormovil.android.views.commons.events;

import android.view.View;

/**
 * Created by Abraham on 29/01/2018.
 */

public interface OnTelefonoListener {
    void onDeleteClick(View view, int position);
}
