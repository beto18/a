package com.gnp.ajustadormovil.android.views.commons.events;

/**
 * Created by Abraham on 29/11/2017.
 */

public interface OnToolbarListener {
    void onChangeTitle(String title);
    void onDefaulTitle();
}
