package com.gnp.ajustadormovil.android.views.commons.events;

import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.views.utils.components.DialogProgressUtils;

import java.util.ArrayList;

public interface OntaskCompleted {

    void onTaskCompleted(ArrayList<ImageGeneralModel> result, DialogProgressUtils dialog);
}
