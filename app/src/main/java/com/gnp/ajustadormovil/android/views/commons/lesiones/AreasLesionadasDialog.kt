package com.gnp.ajustadormovil.android.views.commons.lesiones

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_areas_danadas_selected.*

class AreasLesionadasDialog : DialogFragment() {

    private var listInjured = ArrayList<Long>()
    var list = ArrayList<CatalogoId>()
    var areas: List<Area>? = null
    var btnAddInjuredParts :Button? = null
    val compositeDisposable = CompositeDisposable()
    val listener by lazy { this }
    private var callBackItemCheck: CallBackItemCheck? = null
    private val areasDanadasAdapter by lazy {
        AreasDanadasAdapter(list)
    }

    fun newInstance(params: Bundle? = null, listener: CallBackItemCheck, areas: List<Area>, selected: ArrayList<Long>, btnAddInjuredParts:Button) = AreasLesionadasDialog().apply {
        this.callBackItemCheck = listener
        this.listInjured = selected
        arguments = params
        this.areas = areas
        this.btnAddInjuredParts = btnAddInjuredParts

    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = super.onCreateDialog(savedInstanceState).apply {

        setCancelable(true)
        setCanceledOnTouchOutside(true)

        with(window) {
            requestFeature(android.view.Window.FEATURE_NO_TITLE)
            setLayout(android.view.WindowManager.LayoutParams.WRAP_CONTENT, android.view.WindowManager.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialogTitle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.dialog_areas_lesionada_selected, container, false)!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        btnCancelAddingAreas.setOnClickListener {
            btnAddInjuredParts!!.isEnabled = true
            dismiss()
        }
        btnContinueAddingAreas.setOnClickListener {
            btnAddInjuredParts!!.isEnabled = true
            callBackItemCheck!!.listChecks(areasDanadasAdapter.getItemsChecked())
            listInjured.clear()
            listInjured.addAll(areasDanadasAdapter.getItemsChecked())
            dismiss()
        }
    }

    override fun onDestroyView() {
        GeneralInjuredFragment.GlobalVairablesLesion.isActive = true
        super.onDestroyView()
    }

    fun setAdapter() {
        areas!!.forEach{
            list.add(it)
            it.partes.forEach { parte -> list.add(parte) }
        }
        if(areas != null) {
            rvAreasLesionadasList.layoutManager = LinearLayoutManager(context)
            rvAreasLesionadasList.setItemViewCacheSize(70)
            rvAreasLesionadasList.adapter = areasDanadasAdapter
            areasDanadasAdapter.setItemsChecked(listInjured)
        }
    }

}
