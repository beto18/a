package com.gnp.ajustadormovil.android.views.commons.lesiones

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem
import com.gnp.ajustadormovil.android.views.terceros.autos.TercerosAutoFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import kotlinx.android.synthetic.main.fragment_general_lesiones.*

class GeneralInjuredFragment : Fragment(), CallBackItemCheck, CallbackClickItem {

    private var listIdParts = ArrayList<Long>()
    val compositeDisposable = CompositeDisposable()
    private var btnAddInjuredParts: Button? = null
    private val siniestroInteractor: SiniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor()
    private var areas: List<Area>? = null
    private var activity:AppCompatActivity? = null


    val dialog by lazy {
        AreasLesionadasDialog()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = (context as AppCompatActivity)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_general_lesiones, container, false)
        btnAddInjuredParts = view.findViewById(R.id.btnAddInjuredParts)
        btnAddInjuredParts!!.isActivated = GlobalVairablesLesion.isActive!!

        return view
    }

    object GlobalVairablesLesion {
        var isActive: Boolean? = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvInjuredPartsList.layoutManager = LinearLayoutManager(context)
        setAdapterInjuresParts()
        btnAddInjuredParts!!.setOnClickListener {
            GlobalVairablesLesion.isActive = false
            val next = Action { dialog.newInstance(listener = this, areas = areas!!, selected = listIdParts, btnAddInjuredParts = btnAddInjuredParts!!)
                    .show(activity!!.supportFragmentManager, "dialogAreas") }
            if(areas == null) loadAreas( next )
            else next.run()
        }
    }

    fun loadDataList(list: List<Long>) {
        listIdParts.clear()
        listIdParts.addAll(list)
        if (rvInjuredPartsList!=null) {
            setAdapterInjuresParts()
        }
    }

    private fun setAdapterInjuresParts() {
        val next = Action {
            val plainList = areas!!.flatMap { it.partes }
            val partesLesionadasList = listIdParts.map { id -> plainList.findLast { it.id == id }!! }
            rvInjuredPartsList.adapter = ListAreasLesionadasAdapter(partesLesionadasList, this)
        }
        if(areas == null) loadAreas( next )
        else next.run()
    }

    private fun loadAreas(next : Action){
        compositeDisposable.add(siniestroInteractor.areasPartesLesionadas
                .subscribe({ all ->
                    areas = all
                    next.run()
                }, { throwable ->
                    Log.e("error", "Error: al cargar las areas/partes lesionadas", throwable)
                    GeneralSnackBar().largeSnackError(activity, throwable)
                }))
    }

    override fun onItemClickItem(id: Long, view: View?) {
        listIdParts.remove(id)
        setAdapterInjuresParts()
    }

    fun getDataList() = listIdParts as List<Long>

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @param bundle Bundle
         * @return A new instance of fragment GeneralInjuredFragment.
         *
         */
        fun newInstance(bundle: Bundle?): TercerosAutoFragment {
            val fragment = TercerosAutoFragment()
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }

    override fun itemCheck(id: Long?, checked: Boolean) {
        Log.d("Parts Injured List", "" + listIdParts)

    }

    override fun listChecks(list: List<Long>) {
        listIdParts.clear()
        listIdParts.addAll(list)
        setAdapterInjuresParts()
    }

}
