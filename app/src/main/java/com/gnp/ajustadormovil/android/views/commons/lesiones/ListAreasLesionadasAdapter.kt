package com.gnp.ajustadormovil.android.views.commons.lesiones

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId
import com.gnp.ajustadormovil.android.utils.Constants
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem
import kotlinx.android.synthetic.main.empty_items.view.*
import kotlinx.android.synthetic.main.item_parte_lesionada.view.*

class ListAreasLesionadasAdapter(private var partes: List<CatalogoId>, val listener: CallbackClickItem) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == Constants.TYPE_ITEM) {
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_parte_lesionada, parent, false))
        } else {
            EmptyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.empty_items, parent, false))

        }
    }

    override fun getItemCount(): Int {
        return if (partes.isEmpty()) 1
        else partes.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder){
            holder.bindItems(partes[position],listener)
        }else if (holder is EmptyViewHolder){
            holder.bindDescriber()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (partes.isEmpty()) Constants.TYPE_EMPTY
        else Constants.TYPE_ITEM

    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(partesLesionadas: CatalogoId, callbackClickItem: CallbackClickItem) {
            itemView.tvInjuredPartDescription.text = partesLesionadas.nombre
            itemView.imgInjuredPartDelete.setOnClickListener{callbackClickItem.onItemClickItem(partesLesionadas.id!!,null)}
        }
    }

    class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindDescriber() {
            itemView.tvEmptyItems.text = "No hay partes lesionadas."
        }
    }
}