package com.gnp.ajustadormovil.android.views.commons.piezas

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId
import com.gnp.ajustadormovil.android.utils.Constants
import kotlinx.android.synthetic.main.adapter_checkbox_item.view.*
import kotlinx.android.synthetic.main.adapter_header_item.view.*

class AreasAfectadasAdapter(private var objectList: ArrayList<Any>): RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private val itemsChecked = ArrayList<Long>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == Constants.TYPE_HEADER){
            return HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_header_item, parent, false))
        }else if (viewType == Constants.TYPE_ITEM){
            return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_checkbox_item, parent, false))
        }

        throw RuntimeException("No match for $viewType.")
    }

    override fun getItemCount() = objectList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HeaderViewHolder){
            val areasAfectadas = objectList[position] as Area
            holder.bindText(areasAfectadas)
        }else if (holder is ItemViewHolder){
            val part = objectList[position] as CatalogoId
            holder.binAllCheckBox(part, isChecked(part), checkedChangeListener(part.id!!))
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (objectList[position] is Area) Constants.TYPE_HEADER
        else Constants.TYPE_ITEM
    }


    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bindText( areas : Area){
            itemView.tvTitlePartBody.text = areas.nombre
            when(areas.id){
                1L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_frontales)
                2L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_derecha)
                3L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_izquierda)
                4L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_traseros)
                5L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_otros)
                6L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_moto)
                7L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_epesado)
                8L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_periferico)
                9L->itemView.ivPartInjuredImage.setImageResource(R.drawable.ic_cristales)


            }
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun binAllCheckBox(part: CatalogoId, checked: Boolean, listener: CompoundButton.OnCheckedChangeListener){
            itemView.checkBoxInjuredParts.text = part.nombre
            itemView.checkBoxInjuredParts.isChecked = checked
            itemView.checkBoxInjuredParts.setOnCheckedChangeListener(listener)
        }
    }

    fun checkedChangeListener(id: Long) = CompoundButton.OnCheckedChangeListener{ buttonView, isChecked ->
        if (isChecked) itemsChecked.add(id)
        else itemsChecked.remove(id)

    }

    fun getItemsChecked() = itemsChecked

    fun setItemsChecked(loadItemas: List<Long>) = itemsChecked.addAll(loadItemas)

    fun isChecked(part: CatalogoId) = !itemsChecked.filter { it.equals(part.id) }.isEmpty()

}