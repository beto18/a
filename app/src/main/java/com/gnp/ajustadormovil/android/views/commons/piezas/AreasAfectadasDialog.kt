package com.gnp.ajustadormovil.android.views.commons.piezas

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area
import com.gnp.ajustadormovil.android.utils.Constants
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_areas_lesionada_selected.*


class AreasAfectadasDialog: DialogFragment(){

    private var listDamage = ArrayList<Long>()
    private  var typeImpact:String? = ""
    private var btnAddAreaDanada : Button? = null
    var list = ArrayList<Any>()
    var areas: List<Area>? = null
    val compositeDisposable = CompositeDisposable()
    val listener by lazy { this }
    private var callBackItemCheck: CallBackItemCheck? = null
    private var siniestrosInteractor: SiniestrosInteractor? = null
    private val areasAfetadasAdapter by lazy {
        AreasAfectadasAdapter(list)
    }

    fun newInstance(params: Bundle? = null, listener: CallBackItemCheck, areas: List<Area>, selected: ArrayList<Long>, typeImpact:String, btnAddAreaDanada :Button) = AreasAfectadasDialog().apply {
        this.callBackItemCheck = listener
        this.listDamage = selected
        arguments = params
        this.areas = areas
        this.typeImpact = typeImpact
        this.btnAddAreaDanada = btnAddAreaDanada
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = super.onCreateDialog(savedInstanceState).apply {

        setCancelable(true)
        setCanceledOnTouchOutside(true)

        with(window) {
            requestFeature(android.view.Window.FEATURE_NO_TITLE)
            setLayout(android.view.WindowManager.LayoutParams.WRAP_CONTENT, android.view.WindowManager.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomThemeDialogTitle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.dialog_areas_danadas_selected, container, false)!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (typeImpact!!.equals("Cristales")) {
            setAdapterCristales()
        }
        else {
            setAdapter()
        }
        btnCancelAddingAreas.setOnClickListener {
            btnAddAreaDanada!!.isEnabled = true
            dismiss()
        }
        btnContinueAddingAreas.setOnClickListener {
            btnAddAreaDanada!!.isEnabled = true
            callBackItemCheck!!.listChecks(areasAfetadasAdapter.getItemsChecked())
            listDamage.clear()
            listDamage.addAll(areasAfetadasAdapter.getItemsChecked())
            dismiss()
        }
        if (tag.equals(Constants.TAG_PARTE_FALTANTE))tvTitleDialogAreas!!.text = getString(R.string.btn_add_damage_admision)
        if (tag.equals(Constants.TAG_DAMAGEPREEXTISTENTE))tvTitleDialogAreas!!.text = getString(R.string.btn_add_damage_preex)
    }

    override fun onDestroyView() {
        GeneralDamageFragment.GlobalVairables.isActive = true
        super.onDestroyView()
    }

    fun setAdapterCristales(){
        areas!!.filter { it.id == 9L }
                .forEach {
                    list.add(it)
                    it.partes.forEach { parte -> list.add(parte) }
                }
        if (list != null) {
            rvAreasLesionadasList.layoutManager = LinearLayoutManager(context)
            rvAreasLesionadasList.setItemViewCacheSize(70)
            rvAreasLesionadasList.adapter = areasAfetadasAdapter
            areasAfetadasAdapter.setItemsChecked(listDamage)
        }

    }

    fun setAdapter() {
        areas!!.forEach{
            list.add(it)
            it.partes.forEach { parte -> list.add(parte) }
        }
        if (list != null) {
            rvAreasLesionadasList.layoutManager = LinearLayoutManager(context)
            rvAreasLesionadasList.setItemViewCacheSize(70)
            rvAreasLesionadasList.adapter = areasAfetadasAdapter
            areasAfetadasAdapter.setItemsChecked(listDamage)
        }
    }

}
