package com.gnp.ajustadormovil.android.views.commons.piezas

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Area
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo
import com.gnp.ajustadormovil.android.utils.Constants
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import kotlinx.android.synthetic.main.fragment_general_damages.*


class GeneralDamageFragment : Fragment(), CallBackItemCheck, CallbackClickItem {

    private var listIdParts = ArrayList<Long>()
    private var areas: List<Area>? = null
    private var listener:CallBackItemCheck? = null
    private var discriminator:String? = null
    private var typeImpact :String? = ""
    val compositeDisposable = CompositeDisposable()
    private var btnAddAreaDanada: Button? = null
    private var siniestrosInteractor: SiniestrosInteractor? = null
    private var activity:AppCompatActivity? = null


    val presenter by lazy {
        val compositeDisposable = CompositeDisposable()
        GeneralDamageFragmentPresenter<GeneralDamageMvpView>(compositeDisposable)

    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null) {
            listener = context as CallBackItemCheck?
        }
        if (context != null && context is CallBackItemCheck){
            listener = context;
        }


        activity = context as AppCompatActivity
    }


    val dialog by lazy {
        AreasAfectadasDialog()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_general_damages, container, false)
        btnAddAreaDanada = view.findViewById(R.id.btnAddAreaDanada)
        btnAddAreaDanada!!.isActivated = GlobalVairables.isActive!!

        siniestrosInteractor =  CatalogoInteractorsFactory.getSiniestrosInteractor()

        return view
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(rvAreasDanadasList!=null) {
            rvAreasDanadasList.layoutManager = LinearLayoutManager(context)
            setAdapterDamageParts()
            btnAddAreaDanada!!.setOnClickListener {
                GlobalVairables.isActive = false
                val next = Action{
                    dialog.newInstance(listener = this, areas = areas!!, selected = listIdParts, typeImpact = typeImpact!!, btnAddAreaDanada = btnAddAreaDanada!!)
                            .show(activity!!.supportFragmentManager, tag)
                }
                if(areas == null) loadAreas( next )
                else next.run()
            }
            if (tag.equals(Constants.TAG_DAMAGEPREEXTISTENTE)) btnAddAreaDanada!!.text = getString(R.string.btn_add_damage_preex)
            if (tag.equals(Constants.TAG_PARTE_FALTANTE))btnAddAreaDanada!!.text = getString(R.string.btn_add_damage_admision)
        }
    }

    object GlobalVairables {
        var isActive: Boolean? = true
    }

    fun setDiscriminator(discrim: String?) {
        discriminator = discrim
    }


    fun loadDataList(list: ArrayList<Long>) {
            listIdParts.clear()
            listIdParts.addAll(list)
        if (rvAreasDanadasList!=null) {
            setAdapterDamageParts()
        }
    }

    fun getList():List<Long>{
        return listIdParts
    }

    fun setCallbackListener(listener: CallBackItemCheck){
        this.listener = listener
    }

    fun getDataList(): List<PiezaVehiculo> {
        return listIdParts.map { id ->
            val idArea = areas!!.findLast { area -> area.partes.any { parte -> parte.id == id } }!!.id
            val idParte = areas!!.flatMap { area -> area.partes }.findLast { parte -> parte.id == id }!!.id
            PiezaVehiculo(idArea, idParte)
        }
    }


    override fun itemCheck(id: Long?, checked: Boolean) {
        Log.d("Parts Damgae List", "" + listIdParts)
    }

    override fun listChecks(list: List<Long>) {
        listIdParts.clear()
        listIdParts.addAll(list)
        setAdapterDamageParts()
        if (discriminator.equals("dano")) {
            listener!!.listChecks(list)
        }
    }

    override fun onItemClickItem(id: Long, view: View?) {
        listIdParts.remove(id)
        setAdapterDamageParts()
        if (discriminator.equals("dano")) {
            listener!!.listChecks(listIdParts)
        }
    }

    fun setTypeImpact(typeImpact: String) {
        this.typeImpact = typeImpact

    }

    fun setAdapterDamageParts() {
        val next = Action {
            val plainList = areas!!.flatMap { it.partes }
            val partesAfectadasList = listIdParts.map { id -> plainList.findLast { it.id == id }!! }
            if(rvAreasDanadasList!=null) {
                rvAreasDanadasList.adapter = ListAreasAfetadasAdapter(partesAfectadasList, this, tag!!)
            }
        }
        if(areas == null) loadAreas( next )
        else next.run()
    }

    private fun loadAreas(next : Action){
        compositeDisposable.add(siniestrosInteractor!!.areasPartesAfectadas
                .subscribe({ all ->
                    areas = all
                    next.run()
                }, { throwable ->
                    Log.e("error", "Error: al cargar las areas/partes afectadas", throwable)
                    GeneralSnackBar().largeSnackError(activity,throwable)
                }))
    }

    fun getDataListPartes() = listIdParts

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Damage", "Se ha eliminado el frgamneto de partes dañadas")
    }

}
