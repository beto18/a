package com.gnp.ajustadormovil.android.views.commons.piezas

import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter
import io.reactivex.disposables.CompositeDisposable

class GeneralDamageFragmentPresenter<V : GeneralDamageMvpView>(compositeDisposable: CompositeDisposable?)
    :BasePresenter<V>(compositeDisposable), GeneralDamageMvpPresenter<V>
