package com.gnp.ajustadormovil.android.views.commons.piezas

import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter

interface GeneralDamageMvpPresenter<V : GeneralDamageMvpView> : MvpPresenter<V>
