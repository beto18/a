package com.gnp.ajustadormovil.android.views.commons.piezas

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId
import com.gnp.ajustadormovil.android.utils.Constants
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem
import kotlinx.android.synthetic.main.empty_item_damages.view.*
import kotlinx.android.synthetic.main.item_parte_lesionada.view.*

class ListAreasAfetadasAdapter(private var partesAfectadas: List<CatalogoId>, val listener: CallbackClickItem, private var tag: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == Constants.TYPE_ITEM) {
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_parte_lesionada, parent, false))
        } else {
            EmptyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.empty_item_damages, parent, false))
        }

    }

    override fun getItemCount(): Int {
        return if (partesAfectadas.isEmpty()) 1
        else partesAfectadas.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is ItemViewHolder) {
            holder.bindItems(partesAfectadas[position], listener)
        } else if (holder is EmptyViewHolder) {
            holder.bindDescriber(tag)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (partesAfectadas.isEmpty()) Constants.TYPE_EMPTY
        else Constants.TYPE_ITEM
    }


    class ItemViewHolder(itenView: View) : RecyclerView.ViewHolder(itenView) {
        fun bindItems(partesAfectadas: CatalogoId, callbackClickItem: CallbackClickItem) {
            itemView.tvInjuredPartDescription.text = partesAfectadas.nombre
            itemView.imgInjuredPartDelete.setOnClickListener { callbackClickItem.onItemClickItem(partesAfectadas.id, null) }
        }
    }


    class EmptyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindDescriber(tag: String) {
            when {
                tag.equals(Constants.TAG_PARTE_FALTANTE) -> itemView.tvEmptyItemDamage.text = "No hay partes faltantes"
                tag.equals(Constants.TAG_DAMAGEPREEXTISTENTE) -> itemView.tvEmptyItemDamage.text = "No hay partes preexistentes"
                else -> itemView.tvEmptyItemDamage.text = "No hay partes dañadas."
            }

        }

    }


}
