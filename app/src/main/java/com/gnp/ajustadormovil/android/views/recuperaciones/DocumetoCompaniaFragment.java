package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.amis.impl.FirmaDigitalHelper;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.PolizaInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.amis.FirmaDigital;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Poliza;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrNotFoundException;
import com.gnp.ajustadormovil.android.views.utils.qr.QrUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Abraham on 29/11/2017.
 */

public class DocumetoCompaniaFragment extends BaseFragment {

    private static final String TAG = DocumetoCompaniaFragment.class.getCanonicalName();

    @BindView(R.id.btnSaveDo)
    Button btnSaveDo;
    @BindView(R.id.btnSendImagesCompany)
    Button btnSendImagesCompany;
    private OnToolbarListener listener;
    private RecuperacionCompaniaSeguros recuperacionRequest;
    public long tercero;
    public String descripcion;
    public String idAjuste;
    private Activity activity;

    @BindView(R.id.docCompania)
    Spinner compania;
    @BindView(R.id.docPoliza)
    EditText poliza;
    @BindView(R.id.docVigencia)
    EditText dateVigencia;
    @BindView(R.id.docTitular)
    EditText titular;
    @BindView(R.id.docSiniestro)
    EditText siniestro;
    @BindView(R.id.docAdmision)
    EditText admision;
    @BindView(R.id.etEstimadoDanosCompania)
    EditText danos;
    @BindView(R.id.edtvolanteRecibido)
    Spinner spinnerVolante;
    @BindView(R.id.docOtroAjustador)
    EditText ajustador;
    @BindView(R.id.edtOrdenesMedicas)
    EditText ordenesMedicas;
    @BindView(R.id.tilNumOrdenes)
    TextInputLayout tilNumOrdenes;
    @BindView(R.id.tvNoOrdenesMedicas)
    TextView tvNoOrdenesMedicas;
    @BindView(R.id.viewTercero)
    TextView terceroTitle;
    @BindView(R.id.etTypeCoverage)
    EditText etTypeCoverage;
    @BindView(R.id.edtCodigo)
    EditText edtCodigo;
    @BindView(R.id.btnScanQr)
    ImageView btnScanQr;
    @BindView(R.id.contentQr)
    LinearLayout contentQr;
    private Ajuste ajuste;

    private RecuperacionCompaniaSeguros recuperacionResponse;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));

    //VolantesRecuperaciones
    public List<CatalogoClave> volanteList = new ArrayList<>();
    public CatalogoClave volanteSelected = new CatalogoClave();
    public ArrayList<String> volante = new ArrayList<>();
    //Insures
    public List<CatalogoClave> aseguradoraList = new ArrayList<>();
    public CatalogoClave aseguradoraSelected = new CatalogoClave();

    private String current = "";

    private OnRefreshListener onRefreshListener;
    private GeneralCameraFragment cameraFragment;

    private RecuperacionInteractor recuperacionInteractor;
    private TerceroAutoInteractor terceroAutoService;
    private View view;
    private PolizaInteractor polizaInteractor;
    private TprInteractor tprService;
    private VersionVehiculo categoria;
    private VehiculoAsegurado vehiculoAsegurado;
    private AseguradoInteractor aseguradoInteractor;
    private AmisInteractor amisInteractor;
    private CompositeDisposable compositeDisposable;
    private TerceroAuto terceroAuto;
    private FirmaDigitalHelper firmaDigitalHelper;
    private FirmaDigital firmaDigital;
    private FolioContraparteAmis contraparte;
    private String message = "Recuerda que el producto de la póliza es de Cashflow y no se podrá recibir como recuperación un documento SIPAC";


    public void setRefreshListener(OnRefreshListener l) {
        onRefreshListener = l;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            listener = (OnToolbarListener) context;
        }
        activity = getActivity();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        polizaInteractor = OrquestadorInteractorFactory.getPolizaInteractor();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();
        cameraFragment = new GeneralCameraFragment();
        tprService = CatalogoInteractorsFactory.getTprService();
        firmaDigitalHelper = new FirmaDigitalHelper();
        firmaDigital = new FirmaDigital();
        addFragment(cameraFragment);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_documentos_compania, container, false);
        ButterKnife.bind(this, view);
        terceroTitle.setText(descripcion);
        listener.onChangeTitle(activity.getString(R.string.text_type_recovery_docs));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            this.tercero = getArguments().getLong("tercero");
            this.descripcion = getArguments().getString("descripcion");
            this.idAjuste = getArguments().getString("idAjuste");
            String type = getArguments().getString("type");
            getDanosAsegurado();
        }
        ajuste = RepositoryFactory.getAjusteRepository().findById(idAjuste);
        compositeDisposable.add(getCatalogos()
                .andThen(getTerceroAuto())
                .subscribe(this::initView, throwable -> {
                    Log.e(TAG, "Error al obtener los catalogos", throwable);
                    new GeneralSnackBar().largeSnackError(activity, "Error al obtener los catálogos");
                }));


    }

    private void showDialgoCashflow() {
        SpannableStringBuilder mBuilder = new SpannableStringBuilder();
        SpannableString spannable = new SpannableString(message);
        spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                message.indexOf("Cashflow"),
                message.indexOf("Cashflow") + "Cashflow".length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mBuilder.append(spannable);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setTitle("Aviso");
        builder.setCancelable(false);
        builder.setMessage(mBuilder)
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.dismiss();
                            }
                        });
        builder.show();
    }

    private void initView() {
        if (terceroAuto != null) {
            getContraparte();
        }
        dateVigencia.setOnClickListener(v -> this.showDatePikerDialogVigencia());
        formatEditText();
        setEventSpinner();


    }


    private void getContraparte() {
        compositeDisposable.add(amisInteractor.getContraparte(idAjuste, terceroAuto.getId())
                .subscribe(contraparte -> {
                    this.contraparte = contraparte;
                    setContraparte();
                }, throwable -> {
                    Log.e(TAG, "Ocurrio un error al obtener la contraparte", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        new GeneralSnackBar().largeSnackError(activity,throwable);
                    }
                }));

    }

    private void setContraparte() {
        if (contraparte != null && terceroAuto.getFolioAmis() != null) {
            poliza.setText(getPopulatedData(contraparte.getPoliza()));
            siniestro.setText(getPopulatedData(contraparte.getSiniestro()));
            titular.setText(getPopulatedData(contraparte.getTitular()));
            dateVigencia.setText(getPopulatedData(contraparte.getVigenciaHasta()));
            try {
                Integer value = Integer.valueOf(contraparte.getCompania() == null ? "0" : contraparte.getCompania());
                String clave = RXUtils.sync(siniestrosInteractor.getOtraAseguradoraAmis(value));
                compania.setSelection(SpinnerUtils.getPosition(compania, clave));
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private String getPopulatedData(String value) {
        if (value == null) {
            return "";
        } else {
            return value;
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private void setEventSpinner() {
        compania.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                aseguradoraSelected = aseguradoraList.get(arg2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spinnerVolante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                if (!volanteList.get(spinnerVolante.getSelectedItemPosition()).getClave().equals("SIPAC")) {
                    ordenesMedicas.setVisibility(View.VISIBLE);
                    tvNoOrdenesMedicas.setVisibility(View.VISIBLE);
                    tilNumOrdenes.setVisibility(View.VISIBLE);
                    contentQr.setVisibility(View.GONE);

                } else {
                    ordenesMedicas.setVisibility(View.INVISIBLE);
                    tilNumOrdenes.setVisibility(View.INVISIBLE);
                    tvNoOrdenesMedicas.setVisibility(View.INVISIBLE);
                    contentQr.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        edtCodigo.setOnTouchListener((view, event) -> {
            final int DRAWABLE_RIGHT = 2;
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (edtCodigo.getRight() - edtCodigo.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    edtCodigo.getText().clear();
                    return true;
                }
            }
            return false;
        });

    }


    public Completable getCatalogos() {
        return Completable.mergeArrayDelayError(
                descargaCatalogoVolantesRecuperacion(),
                descargaCatalogoOtrasAseguradoras());
    }


    private Completable descargaCatalogoOtrasAseguradoras() {
        return Completable.create(e -> {
            compositeDisposable.add(catInteractor.getOtrasAseguradoras()
                    .subscribe(otrasAseguradoras -> {
                                aseguradoraList = otrasAseguradoras;
                                compania.setAdapter(new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, aseguradoraList, CatalogoClave::getNombre));
                                e.onComplete();
                            }
                            , throwable -> {
                                Log.e(TAG, "Error al obtener las otras aseguradoras", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                                e.tryOnError(throwable);
                            }));

        });

    }

    private Completable descargaCatalogoVolantesRecuperacion() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getVolantesRecuperacion()
                    .subscribe(volantesRecuperaciones -> {
                                ajuste = RepositoryFactory.getAjusteRepository().findById(idAjuste);
                                if (ajuste.getSiniestro().getPoliza() != null) {
                                    getProductoCashflow(ajuste.getSiniestro().getPoliza(), volantesRecuperaciones);
                                }
                                volanteList = volantesRecuperaciones;
                                spinnerVolante.setAdapter(new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, volanteList, CatalogoClave::getNombre));

                                e.onComplete();
                            }
                            , throwable -> {
                                Log.e(TAG, "Error al obtener los tipos de volantes de recuperacion", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                                e.tryOnError(throwable);
                            }
                    ));
        });

    }

    private void getProductoCashflow(Poliza poliza, List<CatalogoClave> volantes) {
        compositeDisposable.add(siniestrosInteractor.getProductoCashflow(poliza.getProductoComercial(), poliza.getProductoTecnico())
                .subscribe(cashflow -> {
                            if (cashflow) {
                                CollectionsUtils.removeIf(volantes, volante -> "SIPAC".equals(volante.getClave()));
                                showDialgoCashflow();
                            }
                        },
                        throwable -> {
                            Log.e(TAG, "getProductoCashflow: ", throwable);
                        }));
    }


    private Completable getTerceroAuto() {
        return Completable.create(e -> {
            compositeDisposable.add(terceroAutoService.get(idAjuste, tercero)
                    .subscribe(terceroAuto -> {
                        this.terceroAuto = terceroAuto;
                        e.onComplete();
                    }, throwable -> {
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }
                        Log.e(TAG, "Error al obtner el tercero auto", throwable);

                    }));

        });
    }


    public void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> dateVigencia.setText(date));
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }


    private void downloadCategoria(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int modelo, String version) {
        compositeDisposable.add(tprService.getVersion(tipoVehiculo, tipoArmadora, tipoCarroceria, modelo, version)
                .subscribe(categoriaList -> {
                    categoria = categoriaList;
                    List<Long> partes = new ArrayList<>();
                    for (PiezaVehiculo parte : vehiculoAsegurado.getDanos()) {
                        partes.add(parte.getParteAfectada());
                    }
                    getMoney(categoria.getCategoria(), partes);
                    Log.d(TAG, "categoria del tercero vehiculo: " + tipoVehiculo + tipoArmadora + tipoCarroceria + modelo + version);
                }, throwable -> {
                    Log.e(TAG, "Error al descargar categoria de vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                })
        );
    }


    private void getDanosAsegurado() {
        danos.setEnabled(false);
        btnSendImagesCompany.setEnabled(true);
        compositeDisposable.add(aseguradoInteractor.getVehiculo(idAjuste)
                .subscribe(vehiculoAsegurado -> {
                            this.vehiculoAsegurado = vehiculoAsegurado;
                            downloadCategoria(vehiculoAsegurado.getTipoVehiculo(), vehiculoAsegurado.getArmadora(),
                                    vehiculoAsegurado.getCarroceria(), vehiculoAsegurado.getModelo(), vehiculoAsegurado.getVersion());
                        }, throwable -> {

                            Log.e(TAG, "Error al obtener el tercero " + tercero, throwable);
                            danos.setText("$" + 0.00);

                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                            btnSendImagesCompany.setEnabled(true);
                        }
                ));
    }


    private void getMoney(String categoria, List<Long> partes) {
        if (categoria != null) {
            if (partes.size() != 0) {
                compositeDisposable.add(polizaInteractor.getCostosTercero(categoria, partes)
                        .subscribe(costoResponse -> {
                            String currency = numberFormat.format(costoResponse.getCosto());
                            danos.setText(currency);

                        }, throwable -> {
                            Log.e(TAG, "Error al descargar costos de partes dañadas de tercero vehiculo", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }));

            } else {
                danos.setText("$" + 0.00);
            }
        } else {
            danos.setText("$" + 0.00);
        }
    }


    private void formatEditText() {
        danos.addTextChangedListener(watcher);

    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().isEmpty() && !s.toString().equals(current)) {
                danos.removeTextChangedListener(this);
                String cleanString = s.toString().trim().replaceAll("[$,.]", "");
                double parsed = Double.parseDouble(cleanString);
                String formatted = numberFormat.format((parsed / 100));
                current = formatted;
                danos.setText(formatted);
                danos.setSelection(formatted.length());
                danos.addTextChangedListener(this);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void guardaDocumentos() {
        if (poliza.getText().toString().equals("")) {
            showError("El campo de número de póliza está vacío");
            btnSaveDo.setEnabled(true);
        } else if (dateVigencia.getText().toString().equals("")) {
            showError("El campo de fecha de vigencia está vacío");
            btnSaveDo.setEnabled(true);
        } else if (titular.getText().toString().equals("")) {
            showError("El campo del titular está vacío");
            btnSaveDo.setEnabled(true);
        } else if (siniestro.getText().toString().equals("")) {
            showError("El campo de nuúmero de siniestro está vacío");
            btnSaveDo.setEnabled(true);
        } else if (admision.getText().toString().equals("")) {
            showError("El campo de folio de admisión está vacío");
            btnSaveDo.setEnabled(true);
        } else if (ajustador.getText().toString().equals("")) {
            showError("El campo del nombre del ajustador está vacío");
            btnSaveDo.setEnabled(true);
        } else if (danos.getText().toString().equals("")) {
            showError("El campo de daños está vacío");
            btnSaveDo.setEnabled(true);
        } else if (volanteList.get(spinnerVolante.getSelectedItemPosition()).getClave().equals("OM")
                && ordenesMedicas.getText().toString().trim().isEmpty()) {
            showError("El campo de órdenes médicas está vacío");
            btnSaveDo.setEnabled(true);
        } else if (Objects.equals(volanteSelected.getClave(), "SIPAC") && terceroAuto.getFolioAmis() != null && edtCodigo.getText().toString().isEmpty()) {
            showError("No se ha registrado la orden de pago");
        } else {
            makeObjectReq();
        }
    }


    public void showError(String message) {
        new GeneralSnackBar().largeSnackError(getActivity(), message);
    }

    private void makeObjectReq() {
        btnSaveDo.setEnabled(false);
        recuperacionRequest = new RecuperacionCompaniaSeguros();
        recuperacionRequest.setAseguradora(aseguradoraList.get(compania.getSelectedItemPosition()).getClave());
        recuperacionRequest.setFolioAdmision(admision.getText().toString());
        recuperacionRequest.setNombreAjustador(ajustador.getText().toString());
        if (Objects.equals(volanteSelected.getClave(), "OM")) {
            recuperacionRequest.setNumeroOrdenMedicas(Integer.parseInt(ordenesMedicas.getText().toString()));
        }
        if (Objects.equals(volanteSelected.getClave(), "SIPAC")) {
            recuperacionRequest.setValeDigital(firmaDigital.getValeDigital());
            recuperacionRequest.setFirmaDigital(firmaDigital.getFirmaDigital());
        }
        try {
            recuperacionRequest.setDanoEstimado(numberFormat.parse(danos.getText().toString()).doubleValue());
        } catch (ParseException ex) {
            recuperacionRequest.setDanoEstimado(0.0);
        }
        recuperacionRequest.setNumeroPoliza(poliza.getText().toString());
        recuperacionRequest.setNumeroSiniestro(siniestro.getText().toString());
        recuperacionRequest.setOtraAseguradora("");
        recuperacionRequest.setTipoCobertura(etTypeCoverage.getText().toString());
        recuperacionRequest.setTipoVolante(volanteList.get(spinnerVolante.getSelectedItemPosition()).getClave());
        recuperacionRequest.setTitularPoliza(titular.getText().toString());
        recuperacionRequest.setVigenciaPoliza(DateUtils.date(dateVigencia.getText().toString()));
        if (recuperacionRequest != null) makeRequest();
    }

    private void makeRequest() {
        compositeDisposable.add(
                amisInteractor.reFolear(idAjuste)
                        .andThen(recuperacionInteractor.addCompaniaSeguros(idAjuste, this.tercero, recuperacionRequest))
                        .subscribe(compania -> {
                                    recuperacionResponse = compania;
                                    sendImage(recuperacionResponse.getId());
                                    if (onRefreshListener != null) {
                                        onRefreshListener.onRefreshListener();
                                    } else if (getActivity() instanceof OnRefreshListener && getActivity() != null) {
                                        ((OnRefreshListener) getActivity()).onRefreshListener();
                                    }
                                    btnSaveDo.setEnabled(true);
                                    new GeneralSnackBar().largeSnackOk(activity, getString(R.string.recovery_is_ok));
                                }, throwable -> {
                                    Log.e(TAG, "Error al agregar recuperacion de otra compañía", throwable);
                                    if (!(throwable instanceof ResourceNotFoundException)) {
                                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                                    }
                                    btnSaveDo.setEnabled(true);
                                }
                        ));
    }

    public void sendImage(long idRecovery) {

        List<ImageGeneralModel> imageList = cameraFragment.getFileList();
        if (!imageList.isEmpty()) {
            int size = imageList.size();
            ValueObject<Integer> count = new ValueObject<>(1);
            compositeDisposable.add(Observable.fromIterable(imageList)
                    .filter(ImageGeneralModel::getNewFile)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> recuperacionInteractor.addFotografia(idAjuste, tercero, idRecovery, file).toObservable(), true, 1)
                    .doOnEach(fotografias -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        }
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir fotografías de las recuperaciones", throwable);
                        return new Fotografia();
                    })
                    .toList()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        btnSendImagesCompany.setEnabled(true);
                        boolean ok = true;
                        for (Fotografia fotografia : fotografias) {
                            ok &= fotografia.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(activity, getString(R.string.text_message_image_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(activity, getString(R.string.text_message_image_nok));
                        }
                        if (onRefreshListener != null) {
                            onRefreshListener.onRefreshListener();
                        } else if (getActivity() instanceof OnRefreshListener && getActivity() != null) {
                            ((OnRefreshListener) getActivity()).onRefreshListener();
                        }
                        activity.onBackPressed();
                    }, throwable -> {
                        Log.e(TAG, "Error al subir fotografías de las recuperaciones", throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        btnSendImagesCompany.setEnabled(true);
                    }));
        } else {
            new GeneralSnackBar().largeSnackError(view, "Debe agregar una fotografÍa");
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener.onDefaulTitle();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        cameraFragment.onActivityResult(requestCode, resultCode, data);
    }

    private void addFragment(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraFragment, fragment, "cameraFragment");
        transaction.commit();
    }


    @OnClick(R.id.btnSaveDo)
    public void guardar() {
        if (!cameraFragment.getFileList().isEmpty()) {
            guardaDocumentos();
        } else {
            btnSaveDo.setEnabled(true);
            new GeneralSnackBar().largeSnackError(view, "Debe agregar una fotografÍa");
        }

    }

    @OnClick(R.id.btnDownloadVale)
    public void descargaValeDigital() {
        compositeDisposable.add(amisInteractor.reFolear(idAjuste)
                .doOnSubscribe(it -> LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .andThen(amisInteractor.recibirOrden(idAjuste, terceroAuto.getId()))
                .subscribe(valeDigital -> {
                    firmaDigital.setValeDigital(valeDigital.getValeDigital());
                    firmaDigital.setFirmaDigital(null);
                    edtCodigo.setText(valeDigital.getValeDigital());
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el vale digital", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        new GeneralSnackBar().largeSnackError(activity, throwable);
                    }
                }));
    }

    @OnClick(R.id.btnSendImagesCompany)
    public void guardarImagenes() {
        if (recuperacionResponse != null) {
            btnSendImagesCompany.setEnabled(false);
            sendImage(recuperacionResponse.getId());
        } else {
            new GeneralSnackBar().shortSnack(activity, getString(R.string.message_error_no_recoverys));
        }

    }


    @OnClick({R.id.btnScanQr, R.id.edtCodigo})
    public void leerQR() {
        compositeDisposable.add(QrUtils.read(data -> {
            try {
                FirmaDigital firmaDigital = RXUtils.sync(firmaDigitalHelper.desencriptarModulo(data));
                return firmaDigitalHelper.validarFirmaDigital(firmaDigital);
            } catch (Exception ex) {
                return false;
            }
        }).flatMap(json -> firmaDigitalHelper.desencriptarModulo(json))
                .subscribe(firmaDigital -> {
                            this.firmaDigital = firmaDigital;
                            edtCodigo.setText(firmaDigital.getValeDigital());
                        }, throwable -> {
                            if (throwable instanceof QrNotFoundException) {
                                new GeneralSnackBar().largeSnackError(activity, ResourceUtils.getString("qr_not_found"));
                            } else {
                                Log.e(TAG, "Error al leer el QR", throwable);
                                new GeneralSnackBar().largeSnackError(activity, throwable);
                            }
                        }
                ));

    }


}
