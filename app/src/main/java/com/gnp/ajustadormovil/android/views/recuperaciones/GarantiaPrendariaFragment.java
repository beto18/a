package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Lima on 20/02/18.
 */

public class GarantiaPrendariaFragment extends BaseFragment {

    @BindView(R.id.etTerceroWarranty)
    EditText etTerceroWarranty;
    @BindView(R.id.contentCarThird)
    LinearLayout contentCarThird;
    @BindView(R.id.cbSameVehicle)
    CheckBox cbSameVehicle;
    @BindView(R.id.contentOther)
    LinearLayout contentOther;
    @BindView(R.id.etExpirationDate)
    EditText fechaVencimiento;
    @BindView(R.id.etFolioCrane)
    EditText folioGrua;
    @BindView(R.id.etFolioBill)
    EditText etFolioBill;
    @BindView(R.id.contentFolioDoc)
    LinearLayout contentFolioDoc;
    @BindView(R.id.etFolioOther)
    EditText etFolioOther;
    @BindView(R.id.spnTypeCar)
    Spinner spnTypeCar;
    @BindView(R.id.spnArmadora)
    Spinner spnArmadora;
    @BindView(R.id.spnBodyworkCar)
    Spinner spnBodyworkCar;
    @BindView(R.id.spnVersionCar)
    Spinner spnVersionCar;
    @BindView(R.id.spnModelCar)
    Spinner spnModelCar;
    @BindView(R.id.appCompatTextView3)
    AppCompatTextView appCompatTextView3;
    @BindView(R.id.rgDocument)
    RadioGroup rgDocument;
    @BindView(R.id.etOtherDoc)
    EditText etOtherDoc;
    @BindView(R.id.etNoBill)
    EditText etNoBill;
    @BindView(R.id.etNoContrato)
    EditText etNoContrato;
    @BindView(R.id.btnSaveWarranty)
    Button btnSaveWarranty;
    @BindView(R.id.edtCalle)
    EditText edtCalle;
    @BindView(R.id.edtNumEx)
    EditText edtNumEx;
    @BindView(R.id.edtNumIn)
    EditText edtNumIn;
    @BindView(R.id.edtKm)
    EditText edtKm;
    @BindView(R.id.edtPostal)
    EditText edtPostal;
    @BindView(R.id.edtReferencias)
    EditText edtReferencias;

    private OnToolbarListener listener;
    public static final String TAG = "TAG";
    private Unbinder unbinder;
    public long tercero;
    public String descripcion, idAjuste;

    @BindView(R.id.etVersionCar)
    EditText version;
    @BindView(R.id.etSetterCar)
    EditText armadora;
    @BindView(R.id.etBodyworkCar)
    EditText carroceria;
    @BindView(R.id.etModelCar)
    EditText modelo;
    @BindView(R.id.etMotorCar)
    EditText motor;
    @BindView(R.id.etPlatesCar)
    EditText placas;
    @BindView(R.id.etSeriesCar)
    EditText serie;
    @BindView(R.id.etTypeCar)
    EditText tipoVehiculo;
    @BindView(R.id.etWeReceived)
    EditText recibimosDe;
    @BindView(R.id.contentWarranty)
    LinearLayout contentWarranty;
    @BindView(R.id.spinnerEstado)
    Spinner spEstado;
    @BindView(R.id.spinnerMunicipio)
    Spinner spMunicipio;
    @BindView(R.id.spColonia)
    Spinner spColonia;

    private List<CatalogoClave> typeDocuments = new ArrayList<>();
    private CatalogoClave typeDocSelect = new CatalogoClave();

    private CatalogoClave typeCarCat = new CatalogoClave();
    private ArrayList<String> typeCarName = new ArrayList<>();
    private List<CatalogoClave> typeCarList = new ArrayList<>();

    private CatalogoClave assemblersCat = new CatalogoClave();
    private ArrayList<String> assemblersName = new ArrayList<>();
    private List<CatalogoClave> assemblersList = new ArrayList<>();

    private String modelCarCat = "";
    private ArrayList<String> modelCarName = new ArrayList<>();
    private List<Short> modelCarList = new ArrayList<>();

    private CatalogoClave bodyworkCarCat = new CatalogoClave();
    private List<String> bodyworkCarName = new ArrayList<>();
    private List<CatalogoClave> bodyworkCarList = new ArrayList<>();

    private VersionVehiculoResumen versionCarCat = new VersionVehiculoResumen();
    private List<String> versionCarName = new ArrayList<>();
    private List<VersionVehiculoResumen> versionCarList = new ArrayList<>();
    private TerceroAuto vehiculo;
    private LatLng latLng;
    private String via = "";
    private String postalCode = "";
    private String coloniaCode = "";
    private String estadoCode = "";
    private String municipioCode = "";
    boolean carSome = false;
    private OnRefreshListener onRefreshListener;

    private TprInteractor tprService;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private TerceroInteractor terceroService;
    private TerceroAutoInteractor terceroAutoService;
    private RecuperacionInteractor recuperacionInteractor;

    private List<CatalogoClaveInfo> estados = new ArrayList<>();
    private List<Municipio> municipios = new ArrayList<>();
    private List<ColoniaResumen> colonias = new ArrayList<>();


    public void setRefreshListener(OnRefreshListener l) {
        onRefreshListener = l;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            listener = (OnToolbarListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_garantia, container, false);
        unbinder = ButterKnife.bind(this, mView);
        if (getArguments() != null) {
            tercero = getArguments().getLong("tercero");
            this.descripcion = getArguments().getString("descripcion");
            this.idAjuste = getArguments().getString("idAjuste");
        }
        tprService = CatalogoInteractorsFactory.getTprService();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        terceroService = OrquestadorInteractorFactory.getTerceroService();
        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor();
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();

        getTerceros(tercero);
        getCat();
        loadStates();
        listener.onChangeTitle(getActivity().getString(R.string.text_type_recovery_gar));
        cbSameVehicle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getAutoTercero(tercero);
                toggleSpinnerED(false);
            } else {
                toggleEditText(View.GONE);
                toggleSpinner(View.VISIBLE);
                toggleSpinnerED(true);
                emptyDataCar();
            }
        });

        fechaVencimiento.setOnClickListener(v -> showDatePikerDialogVigencia());

        rgDocument.setOnCheckedChangeListener((group, checkedId) -> {
            etFolioBill.setText("");
            RadioButton radioButton = group.findViewById(group.getCheckedRadioButtonId());
            for (int i = 0; i < typeDocuments.size(); i++) {
                if (typeDocuments.get(i).getNombre().contentEquals(radioButton.getText())) {
                    if (typeDocuments.get(i).getClave().equals("OTR")) {
                        contentOther.setVisibility(View.VISIBLE);
                        etFolioOther.setText("");
                        etOtherDoc.setText("");
                        contentFolioDoc.setVisibility(View.GONE);
                    } else {
                        contentOther.setVisibility(View.GONE);
                        contentFolioDoc.setVisibility(View.VISIBLE);
                    }
                    typeDocSelect = typeDocuments.get(i);
                }

            }
        });

        btnSaveWarranty.setOnClickListener(view -> saveWarranty());
        setupEventSpinner();

        return mView;
    }

    private void loadStates() {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(catInteractor.getEstados()
                .subscribe(this::fillStates, throwable -> {
                    Log.e(TAG, "Error al obtener los estados", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), ErrorUtils.getErrorMessage(throwable));
                }));
    }


    private void fillStates(List<CatalogoClaveInfo> sta) {
        estados = sta;
        List<String> statesName = new ArrayList<>();
        for (CatalogoClaveInfo state : sta) {
            statesName.add(state.getNombre());
        }
        AdapterSpinnerGeneral<CatalogoClaveInfo> adapterS = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, estados, CatalogoClaveInfo::getNombre);
        spEstado.setAdapter(adapterS);

        spEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getMunicipalitiesDialog(estados.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getMunicipalitiesDialog(String clave) {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(catInteractor.getMunicipios(clave)
                .subscribe(this::fillDataMunicipalitiesSpin,throwable -> {
                    Log.e(TAG, "Error al obtener la responsabilidad", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), ErrorUtils.getErrorMessage(throwable));
                }));
    }

    private void fillDataMunicipalitiesSpin(List<Municipio> mun) {
        municipios = mun;
        List<String> municipalitiesName = new ArrayList<>();
        for (Municipio m : mun) {
            municipalitiesName.add(m.getNombre());
        }
        AdapterSpinnerGeneral<Municipio> adapterM = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, municipios, Municipio::getNombre);
        spMunicipio.setAdapter(adapterM);

        spMunicipio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadColonias(estados.get(spEstado.getSelectedItemPosition()).getClave(), mun.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void downloadColonias(String estado, String municipio) {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(catInteractor.getColonias(estado, municipio)
                .subscribe(this::fillColoniesSpin, throwable -> {
                }));

    }

    private void fillColoniesSpin(List<ColoniaResumen> col) {
        colonias = col;
        List<String> coloniesName = new ArrayList<>();
        for (ColoniaResumen c : col) {
            coloniesName.add(c.getNombre());
        }
        AdapterSpinnerGeneral<ColoniaResumen> adapter2 = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, colonias, ColoniaResumen::getNombre);

        spColonia.setAdapter(adapter2);
    }


    public void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            fechaVencimiento.setText(date);
        });
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    AdapterView.OnItemSelectedListener onItemSelectedTypeCar = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            typeCarCat = typeCarList.get(position);
            ((BaseActivity) getActivity()).getCompositeDisposable().add(tprService.getArmadoras(typeCarCat.getClave())
                    .subscribe(armadoras -> {
                                assemblersList = armadoras;
                                assemblersName.clear();
                                for (CatalogoClave assemblers : assemblersList) {
                                    assemblersName.add(assemblers.getNombre());
                                }
                                AdapterSpinnerGeneral<CatalogoClave> adapterAssemblers = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, assemblersList, CatalogoClave::getNombre);
                                spnArmadora.setAdapter(adapterAssemblers);
                                if (carSome) {
                                    for (int i = 0; i < assemblersList.size(); i++) {
                                        if (assemblersList.get(i).getClave().equals(vehiculo.getVehiculo().getArmadora())) {
                                            spnArmadora.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                            }, throwable -> {
                                Log.e(TAG, "Error al recuperar armadoras", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                            }
                    ));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    AdapterView.OnItemSelectedListener onItemSelectedAssembler = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            assemblersCat = assemblersList.get(position);
            ((BaseActivity) getActivity()).getCompositeDisposable().add(tprService.getCarrocerias(typeCarCat.getClave(), assemblersCat.getClave())
                    .subscribe(carrocerias -> {
                                bodyworkCarList = carrocerias;
                                bodyworkCarName.clear();
                                for (CatalogoClave bodyworkCar : bodyworkCarList) {
                                    bodyworkCarName.add(bodyworkCar.getNombre());
                                }
                                AdapterSpinnerGeneral<CatalogoClave> adapterBodywork = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, bodyworkCarList, CatalogoClave::getNombre);
                                spnBodyworkCar.setAdapter(adapterBodywork);

                                if (carSome) {
                                    for (int i = 0; i < bodyworkCarList.size(); i++) {
                                        if (bodyworkCarList.get(i).getClave().equals(vehiculo.getVehiculo().getCarroceria())) {
                                            spnBodyworkCar.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                            }, throwable -> {
                                Log.e(TAG, "Error al recuperar carrocerías", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                            }
                    ));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    AdapterView.OnItemSelectedListener onItemSelectedBodyWork = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            bodyworkCarCat = bodyworkCarList.get(position);
            ((BaseActivity) getActivity()).getCompositeDisposable().add(tprService.getModelos(typeCarCat.getClave(), assemblersCat
                    .getClave(), bodyworkCarCat.getClave())
                    .subscribe(modelos -> {
                                modelCarList = modelos;
                                modelCarName.clear();
                                for (Short model : modelCarList) {
                                    modelCarName.add(model.toString());
                                }
                                AdapterSpinnerGeneral<Short> adapterModel = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, modelCarList, Object::toString);
                                spnModelCar.setAdapter(adapterModel);
                                if (carSome) {
                                    for (int i = 0; i < modelCarList.size(); i++) {
                                        if (modelCarList.get(i).equals(vehiculo.getVehiculo().getModelo())) {
                                            spnModelCar.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                            }, throwable -> {
                                Log.e(TAG, "Error al recuperar modelos de vehículos", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                            }
                    ));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    AdapterView.OnItemSelectedListener onItemSelectedModelCar = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            modelCarCat = modelCarList.get(position).toString();
            ((BaseActivity) getActivity()).getCompositeDisposable().add(tprService.getVersiones(typeCarCat.getClave(), assemblersCat.getClave(),
                    bodyworkCarCat.getClave(), Integer.parseInt(modelCarCat))
                    .subscribe(versiones -> {
                                versionCarList = versiones;
                                versionCarName.clear();
                                for (VersionVehiculoResumen version : versionCarList) {
                                    versionCarName.add(version.getNombre());
                                }
                                AdapterSpinnerGeneral<VersionVehiculoResumen> adapterVersion = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, versionCarList, VersionVehiculoResumen::getNombre);
                                spnVersionCar.setAdapter(adapterVersion);
                                if (carSome) {
                                    for (int i = 0; i < versionCarList.size(); i++) {
                                        if (versionCarList.get(i).getVersion().equals(vehiculo.getVehiculo().getVersion())) {
                                            spnVersionCar.setSelection(i);
                                            break;
                                        }
                                    }
                                }
                            }, throwable -> {
                                Log.e(TAG, "Error al recuperar versiones de vehículos", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                            }
                    ));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };


    AdapterView.OnItemSelectedListener onItemSelectedVersionCar = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            versionCarCat = versionCarList.get(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private void setupEventSpinner() {
        spnTypeCar.setOnItemSelectedListener(onItemSelectedTypeCar);
        spnArmadora.setOnItemSelectedListener(onItemSelectedAssembler);
        spnBodyworkCar.setOnItemSelectedListener(onItemSelectedBodyWork);
        spnModelCar.setOnItemSelectedListener(onItemSelectedModelCar);
        spnVersionCar.setOnItemSelectedListener(onItemSelectedVersionCar);
    }

    private void getCat() {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(tprService.getTiposVehiculo()
                .subscribe(tiposCarrosList -> {
                            typeCarList = tiposCarrosList;
                            loadSpinnerTiposCarro();
                        }, throwable -> {
                            Log.e(TAG, "Error al obtener los tipos de vehículos", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
        ((BaseActivity) getActivity()).getCompositeDisposable().add(siniestrosInteractor.getDocumentosPropiedad()
                .subscribe(documentosPropiedades -> {
                            typeDocuments = documentosPropiedades;
                            loadDocumentosData();
                        }, throwable -> {
                            Log.e(TAG, "Error al obtener los tipos de documentos de propiedad", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
    }

    private void loadSpinnerTiposCarro() {
        for (CatalogoClave typeCar : typeCarList) {
            typeCarName.add(typeCar.getNombre());
        }

        AdapterSpinnerGeneral<CatalogoClave> adapterTypeCar = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, typeCarList, CatalogoClave::getNombre);
        spnTypeCar.setAdapter(adapterTypeCar);
    }

    @SuppressLint("ResourceType")
    private void loadDocumentosData() {
        RadioButton radioButton;
        for (CatalogoClave doc : typeDocuments) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            radioButton = new RadioButton(getActivity());//(RadioButton)inflater.inflate(R.layout.my_radio_button,null);
            radioButton.setText(doc.getNombre());
            radioButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.radioTextColor));
            radioButton.setButtonTintList(getResources().getColorStateList(R.drawable.item_selected));
            rgDocument.addView(radioButton);
        }
    }

    private void getTerceros(long tercero) {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(terceroService.get(idAjuste, tercero)
                .subscribe(terceroObject -> {
                            etTerceroWarranty.setText(terceroObject.getDescripcion() != null ? terceroObject.getDescripcion() : "-");
                            if (terceroObject.getTipo().equals(TerceroAuto.TIPO)) {
                                contentCarThird.setVisibility(View.VISIBLE);
                            } else {
                                contentCarThird.setVisibility(View.GONE);
                            }
                        }, throwable -> {
                            Log.e(TAG, "Error al obtener el tercero", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        listener.onDefaulTitle();
    }

    public void getAutoTercero(long tercero) {

        ((BaseActivity) getActivity()).getCompositeDisposable().add(
                terceroAutoService.get(idAjuste, tercero)
                        .subscribe(terceroAuto -> {
                                    vehiculo = terceroAuto;
                                    if (vehiculo != null && vehiculo.getVehiculo() != null) {
                                        tipoVehiculo.setText(vehiculo.getVehiculo().getTipoVehiculo());
                                        for (int i = 0; i < typeCarList.size(); i++) {
                                            if (typeCarList.get(i).getClave().equals(vehiculo.getVehiculo().getTipoVehiculo())) {
                                                spnTypeCar.setSelection(i);
                                                carSome = true;
                                                break;
                                            }
                                        }
                                        serie.setText(vehiculo.getVehiculo().getVin());
                                        motor.setText(vehiculo.getVehiculo().getNumeroMotor());
                                        placas.setText(vehiculo.getVehiculo().getPlacas());
                                    } else {
                                        new GeneralSnackBar().largeSnackError(getActivity(), AjustadorMovilApplication.getInstance().getResources().getString(R.string.recuperacion_garantia_vehiculo_error));
                                    }

                                }, throwable -> {
                                    Log.e(TAG, "Error al obtener los terceros autos", throwable);
                                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                                }
                        ));
    }

    private void toggleSpinner(int v) {
        spnVersionCar.setVisibility(v);
        spnArmadora.setVisibility(v);
        spnBodyworkCar.setVisibility(v);
        spnModelCar.setVisibility(v);
        spnTypeCar.setVisibility(v);
    }

    private void toggleSpinnerED(boolean value) {
        spnVersionCar.setEnabled(value);
        spnArmadora.setEnabled(value);
        spnBodyworkCar.setEnabled(value);
        spnModelCar.setEnabled(value);
        spnTypeCar.setEnabled(value);
    }

    private void toggleEditText(int v) {
        version.setVisibility(v);
        armadora.setVisibility(v);
        carroceria.setVisibility(v);
        modelo.setVisibility(v);
        tipoVehiculo.setVisibility(v);
    }

    public void emptyDataCar() {
        version.setText("");
        armadora.setText("");
        carroceria.setText("");
        modelo.setText("");
        motor.setText("");
        placas.setText("");
        serie.setText("");
        tipoVehiculo.setText("");
    }

    public boolean validateEditText() {
        if (motor.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay número del motor");
            return false;
        } else if (placas.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay placas");
            return false;
        } else if (serie.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay serie");
            return false;
        } else if (etNoBill.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay No de factura");
            return false;
        } else if (etNoContrato.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay No de contrato");
            return false;
        } else if (recibimosDe.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "Falta recibidos de");
            return false;
        } else if (edtReferencias.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay referencia");
            return false;
        } else if (fechaVencimiento.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay fecha de vencimiento");
            return false;
        } else if (folioGrua.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No hay folio de grua");
            return false;
        } else if (rgDocument.getCheckedRadioButtonId() == -1) {
            new GeneralSnackBar().shortSnack(getActivity(), "No se selecciono un tipo de documento");
            return false;
        } else {
            return true;
        }
    }

    public void saveWarranty() {
        if (validateEditText()) sendRequest();
    }

    public void sendRequest() {
        RecuperacionGarantiaPrendaria warranty;
        String otroDocumento = null;
        String folioDoc = "";
        if (typeDocSelect.getClave().equals("OTR")) {
            otroDocumento = etOtherDoc.getText().toString();
            folioDoc = etFolioOther.getText().toString();
        } else {
            folioDoc = etFolioBill.getText().toString();
        }

        if (typeDocSelect.getNombre().equals("OTR") && otroDocumento.equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "No se especificó el tipo de documento.");
        } else if (folioDoc.equals("")) {
            new GeneralSnackBar().shortSnack(getActivity(), "El folio del documento es requerido.");
        } else {
            warranty = new RecuperacionGarantiaPrendaria();
            warranty.setMismoVehiculo(true);
            warranty.setTipoVehiculo(typeCarCat.getClave());
            warranty.setArmadoraVehiculo(assemblersCat.getClave());
            warranty.setCarroceriaVehiculo(bodyworkCarCat.getClave());
            warranty.setModeloVehiculo(Short.parseShort(modelCarCat));
            warranty.setPlacasVehiculo(placas.getText().toString());
            warranty.setVinVehiculo(serie.getText().toString());
            warranty.setNumeroMotor(motor.getText().toString());
            warranty.setNumeroFactura(etNoBill.getText().toString());
            warranty.setRecibidoDe(recibimosDe.getText().toString());
            warranty.setNumeroContrato(etNoContrato.getText().toString());
            warranty.setFolioGrua(folioGrua.getText().toString());
            warranty.setDocumentoPropiedad(typeDocSelect.getClave());
            warranty.setOtroDocumentoPropiedad(otroDocumento);
            warranty.setFolioDocumento(folioDoc);
            warranty.setCodigoPostal(edtPostal.getText().toString());
            warranty.setEstado(estados.get(spEstado.getSelectedItemPosition()).getClave());
            warranty.setMunicipio(municipios.get(spMunicipio.getSelectedItemPosition()).getClave());
            warranty.setColonia(colonias.get(spColonia.getSelectedItemPosition()).getClave());
            warranty.setCalle(edtCalle.getText().toString());
            warranty.setNumeroExterior(edtNumEx.getText().toString());
            warranty.setNumeroInterior(edtNumIn.getText().toString());
            warranty.setReferencia(edtReferencias.getText().toString());
            warranty.setVencimiento(DateUtils.date(fechaVencimiento.getText().toString()));
            warranty.setVersionVehiculo(versionCarCat.getVersion());


            ((BaseActivity) getActivity()).getCompositeDisposable().add(recuperacionInteractor.addGarantiaPrendaria(
                    idAjuste, this.tercero, warranty)
                    .subscribe(terceroAuto -> {
                                new GeneralSnackBar().largeSnackOk(getActivity(), getString(R.string.success_request_warranty));
                                getActivity().onBackPressed();
                                if (onRefreshListener != null){
                                    onRefreshListener.onRefreshListener();
                                } else if (getActivity() instanceof OnRefreshListener && getActivity() != null){
                                    ((OnRefreshListener)getActivity()).onRefreshListener();
                                }

                            }, throwable -> {
                                Log.e(TAG, "Error al agregar la recuperación de garantía prendaría", throwable);
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                            }
                    ));
        }
    }

//    @OnClick(R.id.ivPinLocation)
//    public void getAddress() {
//        if (latLng != null) {
//
//            ((BaseActivity) getActivity()).getCompositeDisposable().add(catInteractor.getLocationAddress(latLng.latitude,
//                    latLng.longitude)
//                    .subscribe(locationAddress -> {
//                                if (locationAddress != null) {
//                                    if (locationAddress.getColonia() != null) {
//                                        coloniaCode = locationAddress.getColonia().getClave();
//                                    }
//                                    if (locationAddress.getMunicipio() != null) {
//                                        municipioCode = locationAddress.getMunicipio().getClave();
//                                    }
//                                    if (locationAddress.getEstado() != null) {
//                                        estadoCode = locationAddress.getEstado().getClave();
//                                    }
//                                    via = locationAddress.getVia() != null ? locationAddress.getVia() : " ";
//                                    postalCode = locationAddress.getCodigoPostal() != null ?
//                                            locationAddress.getCodigoPostal() : " ";
//                                }
//                                tvAddressDescription.setText(locationAddress.getDireccion() != null ?
//                                        locationAddress.getDireccion() : "-");
//                            }, throwable -> {
//                                Log.e(TAG, "Error al obtener la posición geografíca", throwable);
//                                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
//                            }
//                    ));
//        }
//    }


}
