package com.gnp.ajustadormovil.android.views.recuperaciones

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.data.AppPreferences
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar
import com.gnp.ajustadormovil.android.AjustadorMovilApplication
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_recuperacion_mesa_controversia.*
import kotlinx.android.synthetic.main.fragment_recuperacion_mesa_controversia.view.*

class MesaControversiaFragment : Fragment() {

    private val TAG: String = MesaControversiaFragment.javaClass.canonicalName

    private var listener: OnToolbarListener? = null
    private var tercero: Long? = 0
    private var descripcion: String? = null
    private var sRecuperation: String? = null
    private var discriminator: String? = null
    private var idRecuperacion: String? = null

    private var recuperacionInteractor: RecuperacionInteractor? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null) {
            listener = context as OnToolbarListener?
        }
    }

    fun newInstance(args: Bundle?): MesaControversiaFragment? {
        val fragmentControversialTable = MesaControversiaFragment()
        if (args != null) {
            fragmentControversialTable.arguments = args
        }
        return fragmentControversialTable
    }

    companion object {
        private var onRefreshListener: OnRefreshListener? = null
        fun setRefreshListener(l: OnRefreshListener) {
            onRefreshListener = l
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val mView = inflater.inflate(R.layout.fragment_recuperacion_mesa_controversia, container, false)
        listener!!.onChangeTitle("Mesa Controversia")
        if (arguments != null) {
            tercero = arguments!!.getLong("tercero")
            descripcion = arguments!!.getString("descripcion")
            sRecuperation = arguments!!.getString("recuperacion")
            discriminator = arguments!!.getString("discriminator")
            idRecuperacion = arguments!!.getString("id_recuperacion")
        }
        if (sRecuperation != null) {
            val recuperation: RecuperacionMesaControversia? = Gson().fromJson(sRecuperation, RecuperacionMesaControversia::class.java)
            if (recuperation != null) {
                if (recuperation.nombreAbogado != null) {
                    mView.edtNombreAbogado.setText(recuperation.nombreAbogado)
                }
                if (recuperation.despachoAbogado != null) {
                    mView.edtDespachoAbogado.setText(recuperation.despachoAbogado)
                }
                if (recuperation.observaciones != null) {
                    mView.edtObservaciones.setText(recuperation.observaciones)
                }
            }
        }
        if (descripcion != null) {
            mView.tvDescriptionTercero?.text = descripcion
        } else {
            mView.tvDescriptionTercero?.text = "No hay información del tercero"
        }
        mView.btnGuardarMesa?.setOnClickListener {
            validateData(mView.edtDespachoAbogado.text.toString(),
                    mView.edtNombreAbogado.text.toString(),
                    mView.edtObservaciones.text.toString())
        }

        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor()

        return mView
    }

    private fun validateData(despAbogado: String?, nomAbogado: String?, observ: String?) {
        if (despAbogado == "") {
            edtDespachoAbogado?.error = resources.getString(R.string.volante_coberturas_required)
            return
        }
        if (nomAbogado == "") {
            edtNombreAbogado?.error = resources.getString(R.string.volante_coberturas_required)
            return
        }
        if (observ == "") {
            edtObservaciones?.error = resources.getString(R.string.volante_coberturas_required)
            return
        }
        val recuperacion = RecuperacionMesaControversia()
        recuperacion.despachoAbogado = despAbogado
        recuperacion.nombreAbogado = nomAbogado
        recuperacion.observaciones = observ
        when (discriminator) {
            "new" -> newData(recuperacion)
            "edit" -> updateData(recuperacion)
        }

    }

    private fun newData(recuperacion: RecuperacionMesaControversia) {
        (activity as BaseActivity).compositeDisposable.add(recuperacionInteractor!!.addMesaControversia(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                tercero, recuperacion)!!.subscribe({
            GeneralSnackBar().largeSnackOk(activity, AjustadorMovilApplication.getInstance().getString(R.string.recovery_is_ok))
            if (onRefreshListener != null) {
                onRefreshListener!!.onRefreshListener()

            } else if (activity is OnRefreshListener && activity != null) {
                (activity as OnRefreshListener).onRefreshListener()
            }
            activity!!.onBackPressed()
        }, { t ->
            Log.e(TAG, "Error agregar recuperación de mesa de controversia", t)
            GeneralSnackBar().largeSnackError(activity, t)
        }))
    }

    private fun updateData(recuperacion: RecuperacionMesaControversia) {
        (activity as BaseActivity).compositeDisposable.add(recuperacionInteractor!!.modifyMesaControversia(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                tercero, idRecuperacion!!.toLong(), recuperacion)!!.subscribe({
            GeneralSnackBar().largeSnackOk(activity, AjustadorMovilApplication.getInstance().getString(R.string.recovery_is_ok))
            if (onRefreshListener != null) {
                onRefreshListener!!.onRefreshListener()

            } else if (activity is OnRefreshListener && activity != null) {
                (activity as OnRefreshListener).onRefreshListener()
            }
            activity!!.onBackPressed()
        }, { t ->
            Log.e(TAG, "Error al modificar recuperación de mesa de controversia", t)
            GeneralSnackBar().largeSnackError(activity, t)
        }))
    }

    override fun onDestroyView() {
        listener!!.onDefaulTitle()
        super.onDestroyView()
    }
}