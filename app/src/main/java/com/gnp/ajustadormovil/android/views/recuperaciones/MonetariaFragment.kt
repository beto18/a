package com.gnp.ajustadormovil.android.views.recuperaciones

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor
import com.gnp.ajustadormovil.android.models.ValueObject
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Comprobante
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria
import com.gnp.ajustadormovil.android.utils.Constants
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils
import com.gnp.ajustadormovil.android.utils.Utils
import com.gnp.ajustadormovil.android.utils.commons.DateUtils
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils
import com.gnp.ajustadormovil.android.AjustadorMovilApplication
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.RealmList
import kotlinx.android.synthetic.main.dialog_print_proof_income.*
import kotlinx.android.synthetic.main.fragment_recovery_monetary.*
import java.io.File
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by :) on 02/12/2017.
 */

class MonetariaFragment : Fragment(), View.OnClickListener {

    private var TAG: String = "FragmentMonetary"

    private var coloniesName = ArrayList<String>()
    var coloniesResponse = ArrayList<ColoniaResumen>()
    var coloniesSelected = ColoniaResumen()
    private var statesName = ArrayList<String>()
    var statesResponse = ArrayList<CatalogoClaveInfo>()
    var statesSelected = CatalogoClaveInfo()
    var municipalities = ArrayList<String>()
    var municipalitiesResponse = ArrayList<Municipio>()
    var municipalitiesSelected = Municipio()
    private var unbinder: Unbinder? = null
    private var listener: OnToolbarListener? = null
    //FinancialInstitutions
    private var bancosNombres = ArrayList<String>()
    private var bancosList = ArrayList<CatalogoClave>()
    private var bancoSelected = CatalogoClave()
    //Coins
    private var monedasNombres = ArrayList<String>()
    private var monedasList = ArrayList<CatalogoClaveInfo>()
    private var monedaSelected = CatalogoClaveInfo()
    //Cards
    private var tarjetaNombres = ArrayList<String>()
    private var tarjetaList = ArrayList<CatalogoClave>()
    private var tarjetaSelected = CatalogoClave()
    private var terceros = 0L
    private var idAjuste: String? = ""
    private var descripcion: String? = ""
    private var type: String? = ""
    private var pagosList = ArrayList<CatalogoId>()
    private var pagos = ArrayList<String>()
    private var pagoSelected: CatalogoId? = null
    private var currentMonto: String? = null
    private var currentMontoC: String? = null
    private var currentTypeChange = ""

    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"))
    private var importeTotal: String? = null
    private var currentMontoA: String? = null
    private var currentMontoR: String? = null
    private var recuperacionResponse: Recuperacion? = null
    private lateinit var dialogFragment: Dialog
    private var generalCameraFragment: GeneralCameraFragment? = null
    lateinit var compositeDisposable: CompositeDisposable
    private var activity: Activity? = null
    private var isDeducible: Boolean = false

    private var siniestrosInteractor: SiniestrosInteractor? = null
    private var catInteractor: CatInteractor? = null
    private var recuperacionInteractor: RecuperacionInteractor? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null) {
            listener = context as OnToolbarListener?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val mView = inflater.inflate(R.layout.fragment_recovery_monetary, container, false)
        unbinder = ButterKnife.bind(this, mView)
        activity = getActivity()
        compositeDisposable = (activity as BaseActivity).compositeDisposable

        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor()
        catInteractor = CatalogoInteractorsFactory.getCatInteractor()
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor()

        generalCameraFragment = GeneralCameraFragment()
        listener!!.onChangeTitle(activity?.getString(R.string.text_type_recovery_mon))
        if (recuperacionResponse != null)
            recuperacionResponse = recuperacionResponse
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addFragment(generalCameraFragment!!)
        if (arguments != null) {
            terceros = arguments!!.getLong("tercero")
            idAjuste = arguments!!.getString("idAjuste")
            descripcion = arguments!!.getString("descripcion")
            etTercero?.setText(descripcion)
            type = arguments!!.getString("type")
            assert(type != null)

        }
        getCatalogos()
        etMonto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty() && s.toString() != currentMonto) {
                    etMonto.removeTextChangedListener(this)
                    val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                    val parsed = java.lang.Double.parseDouble(cleanString)
                    val formatted = numberFormat!!.format(parsed / 100)
                    currentMonto = formatted
                    etMonto.setText(formatted)
                    val maxLength = (etMonto.filters.filter { it is InputFilter.LengthFilter }.first() as InputFilter.LengthFilter).max
                    etMonto.setSelection(Math.min(maxLength, formatted.length))
                    etMonto.addTextChangedListener(this)
                    calculate()
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        etTypeChange.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty() && s.toString() != currentTypeChange) {
                    etTypeChange.removeTextChangedListener(this)
                    val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                    val parsed = java.lang.Double.parseDouble(cleanString)
                    val formatted = numberFormat!!.format(parsed / 100)
                    currentTypeChange = formatted
                    etTypeChange.setText(formatted)
                    val maxLength = (etTypeChange.filters.filter { it is InputFilter.LengthFilter }.first() as InputFilter.LengthFilter).max
                    etTypeChange.setSelection(Math.min(maxLength, formatted.length))
                    etTypeChange.addTextChangedListener(this)
                    calculate()
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        etMontoCalculado.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty() && s.toString() != currentMontoC) {
                    etMontoCalculado.removeTextChangedListener(this)
                    val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                    val parsed = java.lang.Double.parseDouble(cleanString)
                    val formatted = numberFormat!!.format(parsed / 100)
                    currentMontoC = formatted
                    etMontoCalculado.setText(formatted)
                    val maxLength = (etMontoCalculado.filters.filter { it is InputFilter.LengthFilter }.first() as InputFilter.LengthFilter).max
                    etMontoCalculado.setSelection(Math.min(maxLength, formatted.length))
                    etMontoCalculado.addTextChangedListener(this)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

    }

    @SuppressLint("SetTextI18n")
    private fun calculate() {
        if (etMonto.text.toString() != "" && etTypeChange.text.toString() != ""
                && etMonto.text.toString() != "0.0" && etTypeChange.text.toString() != "0.0") {

            val typeChange = java.lang.Double.valueOf(Utils.removeFormat(etTypeChange.text.toString()))
            val amount = java.lang.Double.valueOf(Utils.removeFormat(etMonto.text.toString()))
            if (typeChange != 0.0 && amount != 0.0) {
                val calculateAmount = typeChange * amount
                val format = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"))
                val currency = format.format(calculateAmount)
                etMontoCalculado.setText(currency.toString())
            } else {
                etMontoCalculado.setText("$0.0")
            }

        }
    }

    private fun goneContents() {
        contentMonto.visibility = View.GONE
        contentMoneda.visibility = View.GONE
        contentMontoCal.visibility = View.GONE
        contentFCI.visibility = View.GONE
        contentRecibo.visibility = View.GONE
        contentOtherBanco.visibility = View.GONE
        contentNoAuth.visibility = View.GONE
        contentTypeC.visibility = View.GONE
        contentBanco.visibility = View.GONE
    }

    private fun cahngeForm(item: Any) {
        when ((item as CatalogoId).nombre) {
            "Con efectivo" -> {
                goneContents()
                contentMonto.visibility = View.VISIBLE
                contentMoneda.visibility = View.VISIBLE
                contentTypeC.visibility = View.VISIBLE
                contentMontoCal.visibility = View.VISIBLE
                contentFCI.visibility = View.VISIBLE
                contentBanco.visibility = View.GONE
            }
            "Con cheques" -> {
                goneContents()
                contentBanco.visibility = View.GONE
                contentMonto.visibility = View.VISIBLE
                contentMoneda.visibility = View.VISIBLE
                contentTypeC.visibility = View.VISIBLE
                contentMontoCal.visibility = View.VISIBLE
                contentNoAuth.visibility = View.VISIBLE
                contentFCI.visibility = View.VISIBLE
            }
            "Con tarjetas bancarias" -> {
                goneContents()
                contentRecibo.visibility = View.VISIBLE
                contentBanco.visibility = View.VISIBLE
                contentMonto.visibility = View.VISIBLE
                contentNoAuth.visibility = View.VISIBLE
            }
            "Con transferencia bancaria" -> {
                goneContents()
                contentBanco.visibility = View.GONE
                contentMonto.visibility = View.VISIBLE
                contentNoAuth.visibility = View.VISIBLE
                contentMoneda.visibility = View.VISIBLE
                contentTypeC.visibility = View.VISIBLE
                contentMontoCal.visibility = View.VISIBLE
                contentFCI.visibility = View.VISIBLE
            }
        }
    }

    private fun getCatalogos() {
        (getActivity() as BaseActivity).compositeDisposable.add(siniestrosInteractor!!.tiposRecuperacionMonetaria
                .subscribe({ payForList ->
                    pagosList = payForList as ArrayList<CatalogoId>
                    pagos = ArrayList()
                    for (pago in pagosList) {
                        pagos.add(pago.nombre!!)
                    }
                    addingToSpinner(pagos)
                }, { throwable ->
                    Log.e(TAG, "Error al obtener los tipos de recuperación monetaria", throwable)
                    GeneralSnackBar().largeSnackError(getActivity(), throwable)
                }))

        (getActivity() as BaseActivity).compositeDisposable.add(catInteractor!!.entidadesFinancieras
                .subscribe({ entidadesFinancieras ->
                    bancosList = entidadesFinancieras as ArrayList<CatalogoClave>
                    bancosNombres = ArrayList()
                    for (banco in bancosList) {
                        bancosNombres.add(banco.nombre!!)
                    }
                    addingToSpinnerBanco(bancosNombres)

                }, { throwable ->
                    Log.e(TAG, "Error al obtener las entidades financieras", throwable)
                    GeneralSnackBar().largeSnackError(getActivity(), throwable)
                }))

        (getActivity() as BaseActivity).compositeDisposable.add(catInteractor!!.monedas
                .subscribe({ listMonedas ->
                    monedasList = listMonedas as ArrayList<CatalogoClaveInfo>
                    monedasNombres = ArrayList()

                    for (c in monedasList) {
                        monedasNombres.add(c.nombre!!)
                    }
                    addingToSpinnerCoins(monedasNombres)
                }, { throwable ->
                    Log.e(TAG, "Error al obtener los tipos de monedas", throwable)
                    GeneralSnackBar().largeSnackError(getActivity(), throwable)
                }))

        (getActivity() as BaseActivity).compositeDisposable.add(siniestrosInteractor!!.tiposRecuperacionMonetariaTarjeta
                .subscribe({ listTarjetas ->
                    tarjetaList = listTarjetas as ArrayList<CatalogoClave>
                    for (c in tarjetaList) {
                        tarjetaNombres.add(c.nombre!!)
                    }
                    addingToSpinnerCards(tarjetaNombres)
                }, { throwable ->
                    Log.e(TAG, "Error al obtener los tipos de recuperación monetaria de tipo tarjeta", throwable)
                    GeneralSnackBar().largeSnackError(getActivity(), throwable)
                }))
    }

    private fun addingToSpinnerCards(tarjetaNombres: ArrayList<String>) {
        spnRecibo?.adapter = AdapterSpinnerGeneral<CatalogoClave>(activity, R.layout.sp_little_item, tarjetaList, AdapterSpinnerGeneral.PrinterValue<CatalogoClave> { it.getNombre() });
        spnRecibo?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View,
                                        arg2: Int, arg3: Long) {
                tarjetaSelected = tarjetaList[spnRecibo.selectedItemPosition]
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
    }

    private fun addingToSpinnerCoins(monedasNombres: ArrayList<String>) {
        spnMoneda?.adapter = AdapterSpinnerGeneral<CatalogoClaveInfo>(activity, R.layout.sp_little_item, monedasList, AdapterSpinnerGeneral.PrinterValue<CatalogoClaveInfo> { it.getNombre() });
        spnMoneda?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            @SuppressLint("SetTextI18n")
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View,
                                        arg2: Int, arg3: Long) {
                monedaSelected = monedasList[spnMoneda.selectedItemPosition]
                if (monedaSelected.clave == "MXN") {
                    etTypeChange.setText("100")
                    etTypeChange.isEnabled = false
                } else {
                    etTypeChange.setText("0")
                    etTypeChange.isEnabled = true
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
    }

    private fun addingToSpinnerBanco(bancosNombres: ArrayList<String>) {
        spnBanco?.adapter = AdapterSpinnerGeneral<CatalogoClave>(activity, R.layout.sp_little_item, bancosList, AdapterSpinnerGeneral.PrinterValue<CatalogoClave> { it.getNombre() });

        spnBanco?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View,
                                        arg2: Int, arg3: Long) {
                bancoSelected = bancosList[spnBanco.selectedItemPosition]
                if (bancoSelected.clave == "0010")
                    contentOtherBanco.visibility = View.VISIBLE
                else
                    contentOtherBanco.visibility = View.GONE
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }

    }

    private fun addingToSpinner(pagos: ArrayList<String>) {
        spnPagoPor?.adapter = AdapterSpinnerGeneral<CatalogoId>(activity, R.layout.sp_little_item, pagosList, AdapterSpinnerGeneral.PrinterValue<CatalogoId> { it.getNombre() });
        spnPagoPor?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                cahngeForm(spnPagoPor?.adapter!!.getItem(position))
                cleanEditText()
                pagoSelected = pagosList[spnPagoPor.selectedItemPosition]
                when {
                    pagoSelected!!.id == 1L -> {
                        contentCameraFragment.visibility = View.GONE
                        btnSaveImages.visibility = View.GONE
                    }
                    pagoSelected!!.id == 4L -> {
                        contentCameraFragment.visibility = View.GONE
                        btnSaveImages.visibility = View.GONE
                    }
                    else -> {
                        contentCameraFragment.visibility = View.GONE
                        btnSaveImages.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder!!.unbind()
        listener!!.onDefaulTitle()
    }

    private fun sendRequest() {
        when (pagoSelected!!.id) {
            1L -> when {
                etMonto.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Monto vacio")
                    btnGuardarTB.isEnabled = true
                }
                etNoAutorization.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "No. Autorización vacio")
                    btnGuardarTB.isEnabled = true
                }
                btnGuardarTB.isEnabled -> true

                else -> {
                    val amount = java.lang.Double.valueOf(Utils.removeFormat(etMonto.text.toString()))
                    makeObjectRequest(bancoSelected.clave, null, "MXN",
                            amount, 1.0,
                            etNoAutorization.text.toString(), 1, tarjetaSelected.clave)
                }
            }
            2L -> when {


                etCompIngresos.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Folio CI vacio")
                    btnGuardarTB.isEnabled = true
                }
                etMonto.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Monto vacio")
                    btnGuardarTB.isEnabled = true
                }
                etTypeChange.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Tipo cambio vacio")
                    btnGuardarTB.isEnabled = true
                }
                etNoAutorization.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "No. Autorización vacio")
                    btnGuardarTB.isEnabled = true
                }

                else -> {
                    val amount = java.lang.Double.valueOf(Utils.removeFormat(etMonto.text.toString()))
                    val typeChange = java.lang.Double.valueOf(Utils.removeFormat(etTypeChange.text.toString()))
                    makeObjectRequest(bancoSelected.clave, etCompIngresos.text.toString(),
                            monedaSelected.clave, amount,
                            typeChange, etNoAutorization.text.toString(),
                            2, null)
                }
            }
            3L -> when {


                etCompIngresos.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Folio CI vacio")
                    btnGuardarTB.isEnabled = true
                }
                etMonto.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Monto vacio")
                    btnGuardarTB.isEnabled = true
                }
                etTypeChange.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Tipo cambio vacio")
                    btnGuardarTB.isEnabled = true

                }
                else -> {
                    val amount = java.lang.Double.valueOf(Utils.removeFormat(etMonto.text.toString()))
                    val typeChange = java.lang.Double.valueOf(Utils.removeFormat(etTypeChange.text.toString()))
                    makeObjectRequest(null, etCompIngresos.text.toString(), monedaSelected
                            .clave, amount, typeChange, null, 3, null)
                }
            }
            4L -> when {
                etCompIngresos.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Folio CI vacio")
                    btnGuardarTB.isEnabled = true
                }
                etMonto.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Monto vacio")
                    btnGuardarTB.isEnabled = true
                }
                etTypeChange.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "Tipo cambio vacio")
                    btnGuardarTB.isEnabled = true
                }
                etNoAutorization.text.toString() == "" -> {
                    GeneralSnackBar().largeSnackError(activity, "No. Autorización vacio")
                    btnGuardarTB.isEnabled = true
                }


                else -> {
                    val amount = java.lang.Double.valueOf(Utils.removeFormat(etMonto.text.toString()))
                    val typeChange = java.lang.Double.valueOf(Utils.removeFormat(etTypeChange.text.toString()))
                    makeObjectRequest(bancoSelected.clave, etCompIngresos.text.toString(),
                            monedaSelected.clave, amount, typeChange, etNoAutorization
                            .text.toString(), 4, null)
                }
            }
        }

    }


    private fun makeObjectRequest(entFinanciera: String?, fComprobante: String?, moneda: String?,
                                  monto: Double?, tipoCambio: Double?, noAuth: String?, tipoRecMonetaria: Long,
                                  tRMTarjeta: String?) {

        val request = RecuperacionMonetaria()
        request.entidadFinanciera = entFinanciera
        request.folioComprobante = fComprobante
        request.moneda = moneda
        request.monto = monto
        request.numeroAutorizacion = noAuth
        request.tipoCambio = tipoCambio
        request.tipoRecuperacionMonetaria = tipoRecMonetaria
        request.tipoRecuperacionMonetariaTarjeta = tRMTarjeta


        val dataRequest = Gson().toJson(request)
        Log.d("Data Request", dataRequest)

        compositeDisposable.add(recuperacionInteractor!!.addMonetaria(idAjuste, terceros, request)
                .subscribe({ recuperaciones ->
                    recuperacionResponse = recuperaciones

                    if (onRefreshListener != null) {
                        onRefreshListener!!.onRefreshListener()
                    } else if (getActivity() is OnRefreshListener && getActivity() != null) {
                        (getActivity() as OnRefreshListener).onRefreshListener()
                    }
                    GeneralSnackBar().largeSnackOk(activity, AjustadorMovilApplication.getInstance().getString(R.string.recovery_is_ok))
                    if (type != "TNA") btnImprimirPDFTB.visibility = View.VISIBLE
                    btnGuardarTB.isEnabled = false

                }, { t ->
                    Log.e(TAG, "Error al agregar recuperación monetaria", t)
                    GeneralSnackBar().largeSnackError(getActivity(), t)
                    btnGuardarTB.isEnabled = true
                }))
        (getActivity() as BaseActivity).compositeDisposable.add(catInteractor!!.monedas
                .subscribe({ listMonedas ->
                    monedasList = listMonedas as ArrayList<CatalogoClaveInfo>
                    monedasNombres = ArrayList()

                    for (c in monedasList) {
                        monedasNombres.add(c.nombre!!)
                    }
                    addingToSpinnerCoins(monedasNombres)
                }, { throwable ->
                    Log.e(TAG, "Error al obtener los tipos de monedas", throwable)
                    GeneralSnackBar().largeSnackError(getActivity(), throwable)
                }))

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        generalCameraFragment!!.onActivityResult(requestCode, resultCode, data)
        if (recuperacionResponse != null) {
            if ((recuperacionResponse as RecuperacionMonetaria).tipoRecuperacionMonetaria == 3L
                    || (recuperacionResponse as RecuperacionMonetaria).tipoRecuperacionMonetaria == 2L) {
                btnSaveImages.visibility = View.VISIBLE
                contentCameraFragment.visibility = View.VISIBLE

            }
        }

    }

    fun cleanEditText() {
        etTypeChange.setText("0")
        etOtroBanco.setText("")
        etMonto.setText("0")
        etMontoCalculado.setText("0")
        etNoAutorization.setText("")
        etCompIngresos.setText("")
    }

    @OnClick(R.id.btnSaveImages)
    fun sendImages() {

        if (recuperacionResponse != null) {
            if (recuperacionResponse != null) {
                val imageList = generalCameraFragment!!.fileList
                if (!imageList.isEmpty()) {
                    val size = imageList.size
                    val count = ValueObject(1);
                    compositeDisposable.add(Observable.fromIterable(imageList)
                            .filter(ImageGeneralModel::getNewFile)
                            .map { image -> File(image.filePhat) }
                            .flatMap({ file -> recuperacionInteractor!!.addFotografia(idAjuste, terceros, recuperacionResponse!!.id, file).toObservable() }, true, 1)
                            .doOnEach { fotografias ->
                                if (count.getValue() < size) {
                                    count.setValue(count.getValue() + 1);
                                    LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                                }
                            }
                            .onErrorReturn { throwable ->
                                Log.e(TAG, "Error al subir fotografía", throwable)
                                Fotografia()
                            }
                            .toList()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnSubscribe {
                                LoadingIndicator.start()
                                LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.value, size))
                            }
                            .doFinally { LoadingIndicator.finish() }
                            .subscribe({ responses ->
                                LoadingIndicator.finish()
                                if (onRefreshListener != null) {
                                    onRefreshListener!!.onRefreshListener()
                                } else if (getActivity() is OnRefreshListener && getActivity() != null) {
                                    (getActivity() as OnRefreshListener).onRefreshListener()
                                }
                                if (responses.filter { item -> item.url == null }.any()) {
                                    GeneralSnackBar().largeSnackError(activity, getString(R.string.text_message_image_nok))
                                } else {
                                    GeneralSnackBar().largeSnackOk(activity, getString(R.string.text_message_image_ok))
                                }
                                getUpdatePhotosDriver()

                            }, { t ->
                                LoadingIndicator.finish()
                                Log.e(TAG, "Error al subir fotografías", t)
                                GeneralSnackBar().largeSnackError(activity, t)
                            }))

                }
            }
        } else {
            GeneralSnackBar().shortSnack(activity, getString(R.string.message_error_no_recoverys))
        }
    }


    fun getUpdatePhotosDriver() {
        compositeDisposable.add(recuperacionInteractor!!.getFotografias(idAjuste, terceros, recuperacionResponse!!.id)
                .subscribe({ fotos ->
                    val filesPhotos = RealmList<ImageGeneralModel>()
                    for (photo in fotos!!) {
                        filesPhotos.add(ImageGeneralModel(photo.id.toString(), photo, null, false))
                    }
                    generalCameraFragment!!.updateImageList(filesPhotos)

                }, { t ->
                    Log.e(TAG, "Error al obtener fotografías", t)
                    GeneralSnackBar().largeSnackError(activity, t)
                }))

    }


    @OnClick(R.id.btnImprimirPDFTB)
    fun printProofIncome() {
        if (recuperacionResponse != null) {
            dialogFragment = Dialog(activity, R.style.CustomThemeDialog)
            dialogFragment.setContentView(R.layout.dialog_print_proof_income)


            (getActivity() as BaseActivity).compositeDisposable.add(catInteractor!!.estados
                    .subscribe({ listEstados ->
                        addStatesSpin(listEstados as ArrayList<CatalogoClaveInfo>)
                    }, { throwable -> GeneralSnackBar().largeSnackError(getActivity(), throwable.message) }))

            dialogFragment.spnStatesProof.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    statesSelected = statesResponse[position]
                    (getActivity() as BaseActivity).compositeDisposable.add(catInteractor!!.getMunicipios(statesSelected.clave!!)
                            .subscribe({ listMunicipios ->
                                addMunicipalitiesSpin(listMunicipios as ArrayList<Municipio>)
                            }, { throwable ->
                                Log.e(TAG, "Error al obtener los municipios", throwable)
                                ToastUtils.toastLongError(throwable)
                            }))


                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }

            dialogFragment.spnMunicipalitiesProof.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    municipalitiesSelected = municipalitiesResponse[position]
                    (getActivity() as BaseActivity).compositeDisposable.add(catInteractor!!.getColonias(statesSelected.clave!!, municipalitiesSelected.clave!!)
                            .subscribe({ listColonias ->
                                addColoniesSpin(listColonias as ArrayList<ColoniaResumen>)
                            }, { throwable ->
                                Log.e(TAG, "Error al obtener las colonias", throwable)
                                GeneralSnackBar().largeSnackError(activity, throwable)
                            }))
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }

            dialogFragment.spnColoniesProof.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    coloniesSelected = coloniesResponse[position]

                    compositeDisposable.add(catInteractor!!.getColonia(statesSelected.clave,
                            municipalitiesSelected.clave, coloniesSelected.clave)
                            .subscribe({ colonias ->
                                dialogFragment.etCodigoPostal.setText(colonias!!.codigoPostal)
                            }, { t ->
                                Log.e(TAG, "Error al obtener la colonia", t)
                                GeneralSnackBar().largeSnackError(activity, t)
                            }))
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }


            dialogFragment.etFechaPromesa.setOnClickListener {
                val datePickerFragment = DatePickerFragment.newInstance { dataPicker, date, age ->
                    dialogFragment.etFechaPromesa.setText(date)
                }
                datePickerFragment.show(getActivity()!!.supportFragmentManager, "datePicker")
            }

            dialogFragment.etMontoAcordado.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (s.toString().isNotEmpty() && s.toString() != currentMontoA) {
                        dialogFragment.etMontoAcordado.removeTextChangedListener(this)
                        val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                        val parsed = java.lang.Double.parseDouble(cleanString)
                        val formatted = NumberFormat.getCurrencyInstance().format(parsed / 100)
                        currentMontoA = formatted
                        dialogFragment.etMontoAcordado.setText(formatted)
                        val maxLength = (dialogFragment.etMontoAcordado.filters.filter { it is InputFilter.LengthFilter }.first() as InputFilter.LengthFilter).max
                        dialogFragment.etMontoAcordado.setSelection(Math.min(maxLength, formatted.length))
                        dialogFragment.etMontoAcordado.addTextChangedListener(this)
                    }
                }

                override fun afterTextChanged(s: Editable) {}
            })

            dialogFragment.etImporteTotal.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (s.toString().isNotEmpty() && s.toString() != importeTotal) {
                        dialogFragment.etImporteTotal.removeTextChangedListener(this)
                        val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                        val parsed = java.lang.Double.parseDouble(cleanString)
                        val formatted = NumberFormat.getCurrencyInstance().format(parsed / 100)
                        importeTotal = formatted
                        dialogFragment.etImporteTotal.setText(formatted)
                        val maxLength = (dialogFragment.etImporteTotal.filters.filter { it is InputFilter.LengthFilter }.first() as InputFilter.LengthFilter).max
                        dialogFragment.etImporteTotal.setSelection(Math.min(maxLength, formatted.length))
                        dialogFragment.etImporteTotal.addTextChangedListener(this)
                    }
                }

                override fun afterTextChanged(s: Editable) {}
            })

            dialogFragment.etMontoRecibido.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (s.toString().isNotEmpty() && s.toString() != currentMontoR) {
                        dialogFragment.etMontoRecibido.removeTextChangedListener(this)
                        val cleanString = s.toString().replace("[$,.]".toRegex(), "")
                        val parsed = java.lang.Double.parseDouble(cleanString)
                        val formatted = NumberFormat.getCurrencyInstance().format(parsed / 100)
                        currentMontoR = formatted
                        dialogFragment.etMontoRecibido.setText(formatted)
                        val maxLength = (dialogFragment.etMontoRecibido.filters.filter { it is InputFilter.LengthFilter }.first() as InputFilter.LengthFilter).max
                        dialogFragment.etMontoRecibido.setSelection(Math.min(maxLength, formatted.length))
                        dialogFragment.etMontoRecibido.addTextChangedListener(this)
                    }
                }

                override fun afterTextChanged(s: Editable) {}
            })

            dialogFragment.cbDeducibleCI.setOnCheckedChangeListener { _, isChecked -> isDeducible = isChecked }

            dialogFragment.btnGenerateProof.setOnClickListener {
                validateInputs()

            }

            dialogFragment.btnCancelProof.setOnClickListener { dialogFragment.dismiss() }
            dialogFragment.show()
        }
    }

    private fun validateInputs() {
        when {
            dialogFragment.etRecibimosDe.text.toString() == "" -> ToastUtils.toastShort("Falta recibimos de")
            dialogFragment.etMontoAcordado.text.toString() == "" -> ToastUtils.toastShort("Falta monto acordado")
            dialogFragment.etMontoRecibido.text.toString() == "" -> ToastUtils.toastShort("Falta monto recibido")
            dialogFragment.etFechaPromesa.text.toString() == "" -> ToastUtils.toastShort("Falta fecha promesa")
            dialogFragment.etImporteTotal.text.toString() == "" -> ToastUtils.toastShort("Falta importe total")
            dialogFragment.etImporteLetra.text.toString() == "" -> ToastUtils.toastShort("Falta importe letra")
            dialogFragment.etCalle.text.toString() == "" -> ToastUtils.toastShort("Falta calle")
            dialogFragment.etNoExterior.text.toString() == "" -> ToastUtils.toastShort("Falta no Exterior")
            dialogFragment.etTelefono.text.toString() == "" -> ToastUtils.toastShort("Falta telefono")
            dialogFragment.etCodigoPostal.text.length < 5 -> ToastUtils.toastShort("Codigo postal invalido")
            else -> {
                val comprobante = Comprobante()
                comprobante.recibimosDe = dialogFragment.etRecibimosDe.text.toString()
                comprobante.deducible = isDeducible
                comprobante.montoAcordado = Utils.removeFormat(dialogFragment.etMontoAcordado.text.toString()).toDoubleOrNull()
                comprobante.montoRecibido = Utils.removeFormat(dialogFragment.etMontoRecibido.text.toString()).toDoubleOrNull()
                comprobante.fechaPromesa = DateUtils.date(dialogFragment.etFechaPromesa.text.toString())
                comprobante.observaciones = dialogFragment.etObservaciones.text.toString()
                comprobante.importeTotal = Utils.removeFormat(dialogFragment.etImporteTotal.text.toString()).toDoubleOrNull()
                comprobante.importeTotalLetra = dialogFragment.etImporteLetra.text.toString()
                comprobante.codigoPostal = dialogFragment.etCodigoPostal.text.toString()
                comprobante.estado = statesSelected.clave
                comprobante.municipio = municipalitiesSelected.clave
                comprobante.colonia = coloniesSelected.clave
                comprobante.calle = dialogFragment.etCalle.text.toString()
                comprobante.numeroExterior = dialogFragment.etNoExterior.text.toString()
                comprobante.numeroInterior = dialogFragment.etNoInterior.text.toString()
                comprobante.entreCalles = dialogFragment.etEntreCalles.text.toString()
                comprobante.telefono = dialogFragment.etTelefono.text.toString()
                makeRequestProof(comprobante)
            }
        }
    }

    private fun addColoniesSpin(col: ArrayList<ColoniaResumen>) {
        coloniesResponse = col
        coloniesName.clear()
        for (colonies in coloniesResponse) {
            coloniesName.add(colonies.nombre!!)
        }
        val adapter = AdapterSpinnerGeneral<ColoniaResumen>(activity, R.layout.sp_little_item, coloniesResponse, AdapterSpinnerGeneral.PrinterValue<ColoniaResumen> { it.getNombre() })
        dialogFragment.spnColoniesProof.adapter = adapter
    }

    private fun addStatesSpin(states: ArrayList<CatalogoClaveInfo>) {
        statesResponse = states
        statesName.clear()
        for (state in statesResponse) {
            statesName.add(state.nombre!!)
        }
        val adapterStates = AdapterSpinnerGeneral<CatalogoClaveInfo>(activity, R.layout.sp_little_item, states, AdapterSpinnerGeneral.PrinterValue<CatalogoClaveInfo> { it.getNombre() })
        dialogFragment.spnStatesProof.adapter = adapterStates
    }

    private fun addMunicipalitiesSpin(mun: ArrayList<Municipio>) {
        municipalitiesResponse = mun
        municipalities.clear()
        for (municipios in municipalitiesResponse) {
            municipalities.add(municipios.nombre!!)
        }
        val adapterMunicipalities = AdapterSpinnerGeneral<Municipio>(activity, R.layout.sp_little_item, municipalitiesResponse, AdapterSpinnerGeneral.PrinterValue<Municipio> { it.getNombre() });
        dialogFragment.spnMunicipalitiesProof.adapter = adapterMunicipalities
    }

    private fun makeRequestProof(proofRequest: Comprobante) {
        compositeDisposable.add(recuperacionInteractor!!.addComprobante(idAjuste, terceros,
                recuperacionResponse!!.id, proofRequest)
                .subscribe({ pdfResponse ->
                    val pdfViewer = Intent(activity, PrinterActivity::class.java)
                    pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, pdfResponse!!.url)
                    dialogFragment.dismiss()
                    startActivityForResult(pdfViewer, 200)
                    if ((recuperacionResponse as RecuperacionMonetaria).tipoRecuperacionMonetaria != 3L
                            && (recuperacionResponse as RecuperacionMonetaria).tipoRecuperacionMonetaria != 2L) {
                        activity!!.finish()
                    }

                }, { t ->
                    Log.e(TAG, "Error al obtener el comprobante", t)
                    ToastUtils.toastLongError(t)
                }))

    }


    private fun addFragment(fragment: Fragment) {
        fragment.setHasOptionsMenu(true)
        val transaction = getActivity()!!.supportFragmentManager.beginTransaction()
        transaction.add(R.id.contentCameraFragment, fragment, "monetary")
        transaction.commit()
    }

    @OnClick(R.id.btnGuardarTB)
    fun send() {
        btnGuardarTB.isEnabled = false
        sendRequest()
    }

    override fun onClick(v: View) {}

    private var onRefreshListener: OnRefreshListener? = null

    fun setRefreshListener(l: OnRefreshListener) {
        onRefreshListener = l
    }

}
