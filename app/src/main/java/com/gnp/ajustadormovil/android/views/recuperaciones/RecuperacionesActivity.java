package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity;
import com.gnp.ajustadormovil.android.views.commons.events.OnChildSelect;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnLoadRecuperationListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;

import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_RECUPERACION;
import static com.gnp.ajustadormovil.android.models.AppStatus.context;

public class RecuperacionesActivity extends BaseActivity implements OnToolbarListener,
        RecuperacionesFragment.ActionButtonDialog, OnRefreshListener, OnLoadRecuperationListener, OnItemRemovedListener {

    @BindView(R.id.toolbar_recuperacion)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView title;
    public long tercero;
    String descripcion, idAjuste, type;
    Fragment fragmentOption;
    //Fragment fragmentOption;
    DocumetoCompaniaFragment fragmentDocumetosCompania;
    MonetariaFragment fragmentMonetary;
    GarantiaPrendariaFragment fragmentGarantia;
    FragmentManager fragmentManager;
    RecyclerView rvCompany;
    RecyclerView rvMonetary;
    RecyclerView rvWarranty;
    RecyclerView rvControversial;
    RecyclerView rvAssistant;
    @BindView(R.id.ivCompany)
    ImageView ivCompany;
    @BindView(R.id.ivMonetary)
    ImageView ivMonetary;
    @BindView(R.id.ivWarranty)
    ImageView ivWarranty;
    @BindView(R.id.ivControversial)
    ImageView ivControversial;
    @BindView(R.id.title_less)
    TextView title_less;
    @BindView(R.id.ivAssistant)
    ImageView ivAssistant;
    @BindView(R.id.tvTitleThirdRecovery)
    TextView tvTitleThirdRecovery;
    private Menu menu;
    @BindView(R.id.rvRecuperaciones)
    RecyclerView rvRecuperaciones;
    private RecuperacionesAdapter recuperacionesAdapter;

    private OnChildSelect onChildSelect;
    private Activity mActivity;
    private List<CatalogoId> pagosList = new ArrayList<>();
    private List<CatalogoClave> aseguradoras = new ArrayList<>();
    private CatInteractor catInteractor;
    private SiniestrosInteractor siniestrosInteractor;
    private RecuperacionInteractor recuperacionInteractor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperacion);
        ButterKnife.bind(this);
        setmToolbar(mToolbar);
        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor();
        mActivity = this;
        recuperacionesAdapter = new RecuperacionesAdapter(this);
        fragmentManager = getSupportFragmentManager();
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        Intent intent = getIntent();
        if (intent != null) {
            tercero = intent.getLongExtra("tercero", 0);
            descripcion = intent.getStringExtra("descripcion");
            idAjuste = intent.getStringExtra("idAjuste");
            type = intent.getStringExtra("type");
            tvTitleThirdRecovery.setText(descripcion);
        }
        rvMonetary = findViewById(R.id.rvMonetary);
        rvCompany = findViewById(R.id.rvCompany);
        rvWarranty = findViewById(R.id.rvWarranty);
        rvControversial = findViewById(R.id.rvControversial);
        rvAssistant = findViewById(R.id.rvAssistant);
        initiComponents();

    }

    private Completable getCatalogos() {
        return Completable.mergeArrayDelayError(
                downloadTiposRecuperaciones(),
                downloadOtrasAseguradoras());

    }


    private Completable downloadOtrasAseguradoras() {
        return Completable.create(e -> {
            getCompositeDisposable().add(catInteractor.getOtrasAseguradoras()
                    .subscribe(otherInsures -> {
                        aseguradoras = otherInsures;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error obtener las otras aseguradoras", throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }));
        });
    }

    private Completable downloadTiposRecuperaciones() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getTiposRecuperacionMonetaria()
                    .subscribe(paysList -> {
                        pagosList = paysList;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al obtener los tipos de recuperación monetaria", throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }));
        });
    }

    private void initiComponents() {
        getRecuperaciones();
        final LinearLayoutManager layoutManagerM = new LinearLayoutManager(context);
        rvMonetary.setLayoutManager(layoutManagerM);
        rvMonetary.setNestedScrollingEnabled(true);
        rvMonetary.hasFixedSize();
        final LinearLayoutManager layoutManagerC = new LinearLayoutManager(context);
        layoutManagerC.setOrientation(LinearLayoutManager.VERTICAL);
        rvCompany.setLayoutManager(layoutManagerC);
        rvCompany.setNestedScrollingEnabled(true);
        rvCompany.hasFixedSize();
        final LinearLayoutManager layoutManagerW = new LinearLayoutManager(context);
        layoutManagerW.setOrientation(LinearLayoutManager.VERTICAL);
        rvWarranty.setLayoutManager(layoutManagerW);
        rvWarranty.setNestedScrollingEnabled(true);
        rvWarranty.hasFixedSize();
        final LinearLayoutManager layoutManagerCV = new LinearLayoutManager(context);
        layoutManagerCV.setOrientation(LinearLayoutManager.VERTICAL);
        rvControversial.setLayoutManager(layoutManagerCV);
        rvControversial.setNestedScrollingEnabled(true);
        rvControversial.hasFixedSize();
        final LinearLayoutManager layoutManagerA = new LinearLayoutManager(context);
        layoutManagerA.setOrientation(LinearLayoutManager.VERTICAL);
        rvAssistant.setLayoutManager(layoutManagerA);
        rvAssistant.setNestedScrollingEnabled(true);
        rvAssistant.hasFixedSize();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRecuperaciones.setAdapter(recuperacionesAdapter);
        rvRecuperaciones.setLayoutManager(layoutManager);
        rvRecuperaciones.setNestedScrollingEnabled(true);
        rvRecuperaciones.hasFixedSize();
        invisibleRecyclers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void invisibleRecyclers() {
        rvCompany.setVisibility(View.GONE);
        ivCompany.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        rvMonetary.setVisibility(View.GONE);
        ivMonetary.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        rvWarranty.setVisibility(View.GONE);
        ivWarranty.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        rvControversial.setVisibility(View.GONE);
        ivControversial.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        rvAssistant.setVisibility(View.GONE);
        ivAssistant.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
    }

    @OnClick(R.id.btnAgregarRecuperacion)
    public void addRecuperation() {
        CameraActivity.modulo = MODULO_RECUPERACION;
        FragmentManager fm = getSupportFragmentManager();
        RecuperacionesFragment dialogFragment = new RecuperacionesFragment();
        dialogFragment.setActionButtonDialog(this);
        dialogFragment.show(fm, "fragment_dialog");
    }

    @OnClick(R.id.cvCompany)
    public void showCompany() {
        if (rvCompany.getVisibility() != View.VISIBLE) {
            rvCompany.setVisibility(View.VISIBLE);
            ivCompany.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
        } else {
            rvCompany.setVisibility(View.GONE);
            ivCompany.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        }
    }

    @OnClick(R.id.cvMonetary)
    public void showMonetary() {
        if (rvMonetary.getVisibility() != View.VISIBLE) {
            rvMonetary.setVisibility(View.VISIBLE);
            ivMonetary.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
        } else {
            rvMonetary.setVisibility(View.GONE);
            ivMonetary.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        }
    }

    @OnClick(R.id.cvWarranty)
    public void showWarranty() {
        if (rvWarranty.getVisibility() != View.VISIBLE) {
            rvWarranty.setVisibility(View.VISIBLE);
            ivWarranty.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
        } else {
            rvWarranty.setVisibility(View.GONE);
            ivWarranty.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        }
    }

    @OnClick(R.id.ivControversial)
    public void showControversial() {
        if (rvControversial.getVisibility() != View.VISIBLE) {
            rvControversial.setVisibility(View.VISIBLE);
            ivControversial.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
        } else {
            rvControversial.setVisibility(View.GONE);
            ivControversial.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        }
    }

    @OnClick(R.id.ivAssistant)
    public void showAssistant() {
        if (rvAssistant.getVisibility() != View.VISIBLE) {
            rvAssistant.setVisibility(View.VISIBLE);
            ivAssistant.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
        } else {
            rvAssistant.setVisibility(View.GONE);
            ivAssistant.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
        }
    }

    public void getRecuperaciones() {
        getCompositeDisposable().add(recuperacionInteractor.getRecuperaciones(this.idAjuste, this.tercero)
                .subscribe(this::fillExpandable,
                        throwable -> {
                            Log.e(TAG, "Error al obtener las recuperaciones", throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);
                        }));
    }

    private void fillExpandable(List<Recuperacion> list) {
        recuperacionesAdapter.clear();
        recuperacionesAdapter.addAll(list);
        recuperacionesAdapter.notifyDataSetChanged();
        if (list.size() != 0) {
            rvRecuperaciones.setVisibility(View.VISIBLE);
            title_less.setVisibility(View.GONE);
        } else {
            rvRecuperaciones.setVisibility(View.GONE);
            title_less.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onChangeTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void onDefaulTitle() {
        title.setText(getString(R.string.title_recovery));
    }

    @Override
    public void onAccept(int id, Dialog dialog) {
        Bundle bundle = new Bundle();
        bundle.putLong("tercero", tercero);
        bundle.putString("descripcion", descripcion);
        bundle.putString("idAjuste", idAjuste);
        bundle.putString("type", type);
        bundle.putString("discriminator", "new");
        switch (id) {
            case 1:
                fragmentDocumetosCompania = new DocumetoCompaniaFragment();
                fragmentDocumetosCompania.setRefreshListener(this);
                fragmentDocumetosCompania.setArguments(bundle);
                addFragment(fragmentDocumetosCompania, "documento");
                dialog.dismiss();
                break;
            case 2:
                fragmentMonetary = new MonetariaFragment();
                fragmentMonetary.setRetainInstance(true);
                fragmentMonetary.setRefreshListener(this);
                fragmentMonetary.setArguments(bundle);
                addFragment(fragmentMonetary, "monetaria");
                dialog.dismiss();
                break;
            case 3:
                fragmentGarantia = new GarantiaPrendariaFragment();
                fragmentGarantia.setRefreshListener(this);
                fragmentGarantia.setArguments(bundle);
                addFragment(fragmentGarantia, "garantia");
                dialog.dismiss();
                break;
            case 4:
                fragmentOption = new MesaControversiaFragment();
                MesaControversiaFragment.Companion.setRefreshListener(this);
                fragmentOption.setArguments(bundle);
                addFragment(fragmentOption, "mesa_controversia");
                dialog.dismiss();
                break;
            case 5:
                fragmentOption = new TurnadoAsistenciaLegalFragment();
                TurnadoAsistenciaLegalFragment.Companion.setRefreshListener(this);
                fragmentOption.setArguments(bundle);
                addFragment(fragmentOption, "Turnado_Asistencia_Legal");
                dialog.dismiss();
                break;
        }
    }

    private void addFragment(Fragment fragment, String tag) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.recuperacion_container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (fragmentDocumetosCompania != null) {
            fragmentDocumetosCompania.onActivityResult(requestCode, resultCode, data);
        } else if (fragmentMonetary != null) {
            fragmentMonetary.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("", "resume");
    }

    @Override
    public void onRefreshListener() {
        getCompositeDisposable().add(getCatalogos()
                .subscribe(this::getRecuperaciones,
                        throwable -> {
                            Log.e(TAG, "Error al obtener los catalogos", throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);
                        }));
    }

    private void deleteRecovery(Recuperacion recovery) {
        getCompositeDisposable().add(recuperacionInteractor.deleteRecuperacion(this.idAjuste,
                this.tercero, recovery.getId())
                .subscribe(this::getRecuperaciones,
                        throwable -> {
                            Log.e(TAG, "Error al borrar la recuperacion", throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);
                        }));
    }


    @Override
    public void loadRecuperation(String type, String id_recuperation) {
        getCompositeDisposable().add(recuperacionInteractor.getRecuperacion(
                AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL)
                , tercero
                , Long.parseLong(id_recuperation))
                .subscribe(recuperacion -> {
                    String recuperation = new Gson().toJson(recuperacion);
                    Bundle bundle = new Bundle();
                    bundle.putLong("tercero", tercero);
                    bundle.putString("descripcion", descripcion);
                    bundle.putString("idAjuste", idAjuste);
                    bundle.putString("recuperacion", recuperation);
                    bundle.putString("id_recuperacion", id_recuperation);
                    bundle.putString("discriminator", "edit");
                    bundle.putString("type", type);
                    switch (type) {
                        case "CS":

                            break;
                        case "MO":

                            break;
                        case "GP":

                            break;
                        case "MC":
                            /*fragmentOption = new FragmentControversialTable();
                            FragmentControversialTable.Companion.setRefreshListener(this);
                            fragmentOption.setArguments(bundle);
                            addFragment(fragmentOption, "mesa_controversia");*/
                            break;
                        case "TA":
                            /*fragmentOption = new FragmentTurnedLegalAssistance();
                            FragmentTurnedLegalAssistance.Companion.setRefreshListener(this);
                            fragmentOption.setArguments(bundle);
                            addFragment(fragmentOption, "Turnado_Asistencia_Legal");*/
                            break;
                    }
                }, throwable -> new GeneralSnackBar().largeSnackError(this, ErrorUtils.getErrorMessage(throwable))));
    }

    @Override
    public void onItemClick(View view, int position) {
        Recuperacion recuperacion = recuperacionesAdapter.getRecuperacion(position);
        DialogUtils.createAlert(mActivity, getString(R.string.title_quest_delete_service),
                getString(R.string.desc_quest_delete_recovery), getString(R.string.btn_continuar_text),
                getString(R.string.btn_cancel_text), (dialog, which) -> deleteRecovery(recuperacion),
                (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }
}
