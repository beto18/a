package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.annotation.SuppressLint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.Recuperacion;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionCompaniaSeguros;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionGarantiaPrendaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMesaControversia;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionMonetaria;
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abraham on 07/12/2017.
 */

public class RecuperacionesAdapter extends RecyclerView.Adapter<RecuperacionesAdapter.RecuperacionHolder> {

    private ArrayList<Recuperacion> recuperaciones;
    private OnItemRemovedListener removedListener;
    public static final String TAG = RecuperacionesAdapter.class.getSimpleName();


    public RecuperacionesAdapter(OnItemRemovedListener removedListener) {
        this.recuperaciones = new ArrayList<>();
        this.removedListener = removedListener;
    }

    public void addAll(List<? extends Recuperacion> results) {
        for (Recuperacion result : results) {
            add(result);
        }
    }

    public void add(Recuperacion tercero) {
        recuperaciones.add(tercero);
        notifyItemInserted(recuperaciones.size() - 1);
    }

    public void remove(Recuperacion tercero) {
        int position = recuperaciones.indexOf(tercero);
        if (position > -1) {
            recuperaciones.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        recuperaciones.clear();
        notifyDataSetChanged();
    }

    public List<Recuperacion> getRecuperaciones() {
        return recuperaciones;
    }

    public Recuperacion getRecuperacion(int position) {
        return recuperaciones.get(position);
    }

    @Override
    public RecuperacionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_removable, parent, false);
        return new RecuperacionHolder(mView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecuperacionHolder holder, @SuppressLint("RecyclerView") int position) {
        String title = "Recuperación";
        Recuperacion recuperacion = recuperaciones.get(position);
        if (recuperacion instanceof RecuperacionMonetaria) {
            holder.icon.setImageDrawable(holder.icon.getResources().getDrawable(R.drawable.ic_rec_monetaria));
            title = ResourceUtils.getString(R.string.text_desc_monetary);
        }
        if (recuperacion instanceof RecuperacionCompaniaSeguros) {
            holder.icon.setImageDrawable(holder.icon.getResources().getDrawable(R.drawable.ic_rec_compania));
            title = ResourceUtils.getString(R.string.text_desc_docs_company);
        }
        if (recuperacion instanceof RecuperacionTurnadoAsistenciaLegal) {
            holder.icon.setImageDrawable(holder.icon.getResources().getDrawable(R.drawable.ic_rec_asistencia_legal));
            title = "Turnado a Asistencia Legal";
        }
        if (recuperacion instanceof RecuperacionMesaControversia) {
            holder.icon.setImageDrawable(holder.icon.getResources().getDrawable(R.drawable.ic_rec_mesa_controversia));
            title = "Mesa de Controversia";
        }
        if (recuperacion instanceof RecuperacionGarantiaPrendaria) {
            holder.icon.setImageDrawable(holder.icon.getResources().getDrawable(R.drawable.ic_rec_garantia_prendaria));
            title = ResourceUtils.getString(R.string.text_desc_warranty);
        }
        holder.title.setText(title);
        holder.content.setText(recuperacion.getDescripcion());
    }

    @Override
    public int getItemCount() {
        return recuperaciones.size();
    }

    class RecuperacionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvTitle)
        AppCompatTextView title;
        @BindView(R.id.tvContent)
        AppCompatTextView content;
        @BindView(R.id.imgRemovedItem)
        ImageButton delete;
        @BindView(R.id.ivIcon)
        ImageView icon;

        RecuperacionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            removedListener.onItemClick(view, getAdapterPosition());
        }
    }
}
