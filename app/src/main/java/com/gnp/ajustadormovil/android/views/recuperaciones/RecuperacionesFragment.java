package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.gnp.ajustadormovil.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tixmedia on 3/26/18.
 */

public class RecuperacionesFragment extends android.support.v4.app.DialogFragment {

    private ActionButtonDialog actionButtonDialog;
    @BindView(R.id.imgCompania)
    ImageButton imgCompania;
    @BindView(R.id.imgMonetaria)
    ImageButton imgMonetaria;
    @BindView(R.id.imgPrendaria)
    ImageButton imgPrendaria;
    @BindView(R.id.imgControversia)
    ImageButton imgControversia;
    @BindView(R.id.imgAsistencia)
    ImageButton imgAsistencia;

    public interface ActionButtonDialog {
        void onAccept(int id, Dialog dialog);
    }

    public RecuperacionesFragment() {
    }

    public void setActionButtonDialog(ActionButtonDialog actionButtonDialog) {
        this.actionButtonDialog = actionButtonDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.CustomThemeDialog);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_recuperaciones_fragment, container);
        getDialog().setTitle(getString(R.string.text_type_add_recovery));
        ButterKnife.bind(this, view);
        imgCompania.setOnClickListener(it -> actionButtonDialog.onAccept(1, getDialog()));
        imgMonetaria.setOnClickListener(it -> actionButtonDialog.onAccept(2, getDialog()));
        imgPrendaria.setOnClickListener(it -> actionButtonDialog.onAccept(3, getDialog()));
        imgControversia.setOnClickListener(it -> actionButtonDialog.onAccept(4, getDialog()));
        imgAsistencia.setOnClickListener(it -> actionButtonDialog.onAccept(5, getDialog()));
        return view;
    }

}
