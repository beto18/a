package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lima on 22/02/18.
 */

public class RecuperacionesTercerosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Tercero> list;
    private OnItemRemovedListener mClickListener;

    public RecuperacionesTercerosAdapter(List<Tercero> list) {
        this.list = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectable, parent, false);
            return new DataViewHolder(v, mClickListener);
        } else if (viewType == Constants.TYPE_EMPTY) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item, parent, false);
            return new EmptyView(v);
        }
        throw new RuntimeException("No match for " + viewType + " .");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DataViewHolder) {
            if (list.get(position) != null) {
                Tercero tercero = list.get(position);
                ((DataViewHolder) holder).name.setText(tercero.getDescripcion());
                if(tercero instanceof TerceroAuto) {
                    ((DataViewHolder) holder).title.setText("Tercero Auto");
                    ((DataViewHolder) holder).icon.setImageDrawable(((DataViewHolder) holder).icon.getResources().getDrawable(R.drawable.ic_ter_auto));
                } else if(tercero instanceof TerceroNoAuto) {
                    ((DataViewHolder) holder).title.setText("Tercero No Auto");
                    ((DataViewHolder) holder).icon.setImageDrawable(((DataViewHolder) holder).icon.getResources().getDrawable(R.drawable.ic_ter_noauto));
                } else if(tercero instanceof TerceroPersona) {
                    ((DataViewHolder) holder).title.setText("Tercero Persona");
                    ((DataViewHolder) holder).icon.setImageDrawable(((DataViewHolder) holder).icon.getResources().getDrawable(R.drawable.ic_ter_persona));
                }
            }
        } else if (holder instanceof EmptyView) {
            ((EmptyView) holder).tvEmptyItem.setText(AjustadorMovilApplication.getInstance()
                    .getString(R.string.recycler_not_third));
        }

    }


    @Override
    public int getItemCount() {
        if (list.isEmpty()) return 1;
        else return list.size();
    }

    public OnItemRemovedListener getClickListener() {
        return mClickListener;
    }

    public void setListener(OnItemRemovedListener clickListener) {
        this.mClickListener = clickListener;
    }

    public static class EmptyView extends RecyclerView.ViewHolder {
        @BindView(R.id.tvEmptyItem)
        TextView tvEmptyItem;

        EmptyView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvContent)
        TextView name;
        @BindView(R.id.tvTitle)
        TextView title;
        @BindView(R.id.ivIcon)
        ImageView icon;
        @BindView(R.id.btnSelect)
        ImageButton select;

        DataViewHolder(View itemView, OnItemRemovedListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            View.OnClickListener onClick = view -> {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(itemView, getAdapterPosition());
                }
            };
            itemView.setOnClickListener(onClick);
            select.setOnClickListener(onClick);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (list.size() == 0) return Constants.TYPE_EMPTY;
        else return Constants.TYPE_ITEM;
    }
}
