package com.gnp.ajustadormovil.android.views.recuperaciones;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroInteractor;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Retrofit;

/**
 * Created by Lima on 22/02/18.
 */

public class TercerosRecuperacionActivity extends BaseActivity implements OnItemRemovedListener, DialogInterface.OnClickListener {

    @BindView(R.id.toolbar_recuperacion)
    Toolbar mToolbar;
    private Retrofit retrofit;
    public ArrayList<String> volante;
    @BindView(R.id.recuperaciones_recycler)
    RecyclerView recycler;
    private int selected;
    private RecuperacionesTercerosAdapter adapter;
    private Intent intent;
    private OnItemRemovedListener mClickListener;
    private List<String> l = new ArrayList<>();
    public String idAjuste;
    public static final String TAG = "TAG";
    public int casoA, casoB, resultResponsabilidad;
    public boolean terceroResp, aseResp;
    private Menu menu;
    private AjusteInteractor ajusteInteractor;
    private TerceroInteractor terceroService;
    private RecuperacionInteractor recuperacionInteractor;

    public static TercerosRecuperacionActivity newInstance() {
        TercerosRecuperacionActivity recuperacion = new TercerosRecuperacionActivity();
        return recuperacion;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_terceros_recuperaciones);
        setmToolbar(mToolbar);
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        terceroService = OrquestadorInteractorFactory.getTerceroService();
        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor();
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        Intent intent = getIntent();
        if (intent != null) {
            idAjuste = intent.getStringExtra("idAjuste");
            if (intent.getIntExtra("casoA", -1) != -1) {
                casoA = intent.getIntExtra("casoA", -1);
                casoB = intent.getIntExtra("casoB", -1);
                resultResponsabilidad = intent.getIntExtra("result", -1);
                terceroResp = intent.getBooleanExtra("respTer", false);
                aseResp = intent.getBooleanExtra("respAse", false);
            } else {
                casoA = -1;
                casoB = -1;
                resultResponsabilidad = -1;
            }

        } else {
            casoA = -1;
            casoB = -1;
            resultResponsabilidad = -1;
        }
        getTerceros();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getTerceros() {
        mClickListener = this;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        getCompositeDisposable().add(terceroService.getAll(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(object -> {
                    recycler.setLayoutManager(linearLayoutManager);
                    adapter = new RecuperacionesTercerosAdapter(object);
                    adapter.setListener(mClickListener);
                    recycler.setAdapter(adapter);
                }, throwable -> {
                    Log.e(TAG, "Error al recuperar los terceros", throwable);
                    new GeneralSnackBar().largeSnackError(getParent(), throwable);
                }));
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    @Override
    public void onItemClick(View view, int position) {

        if (resultResponsabilidad >= -1) {
            intent = new Intent(this, RecuperacionesActivity.class);
            intent.putExtra("tercero", adapter.list.get(position).getId());
            intent.putExtra("descripcion", adapter.list.get(position).getDescripcion());
            intent.putExtra("type", adapter.list.get(position).getTipo());
            intent.putExtra("idAjuste", this.idAjuste);
            startActivity(intent);
        } else {
            Log.d("", "position" + position);
        }
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

}