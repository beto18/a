package com.gnp.ajustadormovil.android.views.recuperaciones

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.data.AppPreferences
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory
import com.gnp.ajustadormovil.android.interactors.orquestador.RecuperacionInteractor
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave
import com.gnp.ajustadormovil.android.models.orquestador.recuperacion.RecuperacionTurnadoAsistenciaLegal
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar
import com.gnp.ajustadormovil.android.AjustadorMovilApplication
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_turned_legal_assistance.*
import kotlinx.android.synthetic.main.fragment_turned_legal_assistance.view.*
import java.util.*

class TurnadoAsistenciaLegalFragment : Fragment() {

    private val TAG: String = TurnadoAsistenciaLegalFragment.javaClass.canonicalName

    private var listener: OnToolbarListener? = null
    private var tercero: Long? = 0
    private var descripcion:String? = null
    private var aseguradoraList:List<CatalogoClave> = ArrayList()
    private var aseguradorasNombres = ArrayList<String?>()
    private var aseguradoraSelected = CatalogoClave()
    private var sRecuperation:String? = null
    private var discriminator:String? = null
    private var idRecuperacion:String? = null
    private var idAseguradora: String? = null

    private var recuperacionInteractor: RecuperacionInteractor? = null
    private var catInteractor: CatInteractor? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context != null) {
            listener = context as OnToolbarListener?
        }
    }

    fun newInstance(args: Bundle?): TurnadoAsistenciaLegalFragment? {
        val fragmentTurnedLegalAssistance = TurnadoAsistenciaLegalFragment()
        if (args != null) {
            fragmentTurnedLegalAssistance.arguments = args
        }
        return fragmentTurnedLegalAssistance
    }

    companion object {
        private var onRefreshListener: OnRefreshListener? = null
        fun setRefreshListener(l: OnRefreshListener) {
            onRefreshListener = l
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val mView = inflater.inflate(R.layout.fragment_turned_legal_assistance, container, false)
        recuperacionInteractor = OrquestadorInteractorFactory.getRecuperacionInteractor()
        catInteractor = CatalogoInteractorsFactory.getCatInteractor()
        listener!!.onChangeTitle("Turnado a Asistencia Legal")
        if(arguments!=null){
            tercero = arguments!!.getLong("tercero")
            descripcion = arguments!!.getString("descripcion")
            sRecuperation = arguments!!.getString("recuperacion")
            discriminator = arguments!!.getString("discriminator")
            idRecuperacion = arguments!!.getString("id_recuperacion")
        }
        if (sRecuperation!=null){
            val recuperation: RecuperacionTurnadoAsistenciaLegal? = Gson().fromJson(sRecuperation, RecuperacionTurnadoAsistenciaLegal::class.java)
            if (recuperation!=null){
                if (recuperation.aseguradora!=null){
                    idAseguradora = recuperation.aseguradora
                }
                if (recuperation.observaciones!=null){
                    mView.edtObservacionesAssistant.setText(recuperation.observaciones)
                }
            }
        }
        if (descripcion!=null){
            mView.tvDescriptionTerceroLegal?.text = descripcion
        }
        loadCatalogs(mView)
        mView.btnSaveAssistant.setOnClickListener {
            validateData(aseguradoraSelected, mView.edtObservacionesAssistant.text.toString())
        }


        return mView
    }

    private fun validateData(otraAseguradora: CatalogoClave?, observ: String?) {
        if (otraAseguradora== null){
            GeneralSnackBar().largeSnackError(activity,"Error al cargar la otra aseguradora")
            return
        }
        val recuperacion =  RecuperacionTurnadoAsistenciaLegal()
        recuperacion.aseguradora = otraAseguradora.clave
        recuperacion.observaciones = observ
        when(discriminator){
            "new"->(activity as BaseActivity).compositeDisposable.add(recuperacionInteractor!!.addTurnadoAsistenciaLegal(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                    tercero, recuperacion)!!.subscribe({
                GeneralSnackBar().largeSnackOk(activity, AjustadorMovilApplication.getInstance().getString(R.string.recovery_is_ok))
                if (onRefreshListener != null){
                    onRefreshListener!!.onRefreshListener()
                } else if (activity is OnRefreshListener && activity != null) {
                    (activity as OnRefreshListener).onRefreshListener()
                }

                activity!!.onBackPressed()
            }, { t ->
                Log.e(TAG, "Error agregar recuperación de turnado a asistencia legal", t)
                GeneralSnackBar().largeSnackError(activity, t)
            }))
            "edit"->(activity as BaseActivity).compositeDisposable.add(recuperacionInteractor!!.modifyTurnadoAsistenciaLegal(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                    tercero, idRecuperacion!!.toLong(), recuperacion).subscribe({
                GeneralSnackBar().largeSnackOk(activity, AjustadorMovilApplication.getInstance().getString(R.string.recovery_is_ok))
                if (onRefreshListener != null){
                    onRefreshListener!!.onRefreshListener()
                } else if (activity is OnRefreshListener && activity != null) {
                    (activity as OnRefreshListener).onRefreshListener()
                }
                activity!!.onBackPressed()
            }, { t ->
                Log.e(TAG, "Error al modificar recuperación de turnado a asistencia legal", t)
                GeneralSnackBar().largeSnackError(activity, t)
            }))
        }
    }

    private fun loadCatalogs(mView:View) {
            (activity as BaseActivity).compositeDisposable.add(catInteractor!!.otrasAseguradoras
                    .subscribe({ insures ->
                        aseguradoraList = insures
                        for (i in aseguradoraList) {
                            aseguradorasNombres.add(i.nombre)
                        }
                        addDataSpinner(mView,aseguradorasNombres)
                    }, { throwable ->
                        Log.e(TAG, "Error al obtener otras aseguradoras", throwable)
                        GeneralSnackBar().largeSnackError(activity, throwable)
                    }))

    }

    private fun addDataSpinner(mView:View,aseguradorasNombres: ArrayList<String?>) {

        val adapter = AdapterSpinnerGeneral<CatalogoClave>(activity, R.layout.sp_little_item, aseguradoraList, AdapterSpinnerGeneral.PrinterValue<CatalogoClave> { it.getNombre() })
        mView.spOtraAseguradora.adapter = adapter
        mView.spOtraAseguradora.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View,
                                        arg2: Int, arg3: Long) {
                aseguradoraSelected = aseguradoraList[spOtraAseguradora.selectedItemPosition]
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        }
        if (idAseguradora!=null){
            for (i in aseguradoraList.indices) {
                if (idAseguradora == aseguradoraList[i].clave){
                    mView.spOtraAseguradora.setSelection(i)
                }
            }
        }
    }

    override fun onDestroyView() {
        listener!!.onDefaulTitle()
        super.onDestroyView()
    }
}