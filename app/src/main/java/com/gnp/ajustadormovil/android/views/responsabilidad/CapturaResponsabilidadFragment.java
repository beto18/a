package com.gnp.ajustadormovil.android.views.responsabilidad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.media.CircleTransform;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.CustomCamera;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by oscar on 14/02/18.
 * Modified by hgzavala 12042018
 */

public class CapturaResponsabilidadFragment extends BaseFragment implements CapturaResponsabilidadMvpView {

    private static final String TAG = CapturaResponsabilidadFragment.class.getCanonicalName();

    private CapturaResponsabilidadMvpPresenter presenter;

    private OnRefreshListener listener;
    Button buttonGuardar;
    Button buttonGuia;
    int tipoTerceroSelected, terceroSelected;
    @BindView(R.id.terceroResponsableCheck)
    CheckBox terceroResponsableCheck;
    @BindView(R.id.corresponsabilidadCheck)
    CheckBox corresponsabilidadCheck;
    @BindView(R.id.aseguradoResponsableCheck)
    CheckBox aseguradoResponsableCheck;
    @BindView(R.id.noAplicaResponsablidadCheck)
    CheckBox noAplicaResponsablidadCheck;
    boolean isActivaded;
    private Long circunstanciaA;
    private Long circunstanciaB;
    List<TerceroAuto> tercerosAutosList = new ArrayList<>();
    List<TerceroNoAuto> tercerosNoAutosList = new ArrayList<>();
    List<TerceroPersona> tercerosPeatonList = new ArrayList<>();
    @BindView(R.id.tvAsegurado)
    TextView tvAsegurado;
    private String tipoTercero;
    private Spinner spinnerTipoTercero;
    private ResponsabilidadActivity mActivity;
    private List<CatalogoClave> tiposTercerosList = new ArrayList<>();
    private ArrayList<String> tercerosName = new ArrayList<>();
    private CatalogoClave tercerosSelected;
    private String idAdjust;
    private ImageView imgTakeRes, imgRes;
    private String img_path = "";
    private View v;
    private ResponsabilidadInteractor responsabilidadService;
    private AseguradoInteractor aseguradoInteractor;
    private SiniestrosInteractor siniestroService;
    private CompositeDisposable compositeDisposable;


    public static CapturaResponsabilidadFragment newInstance(Bundle bundle) {
        CapturaResponsabilidadFragment fragment = new CapturaResponsabilidadFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(this.toString(), "onCreate");
        super.onCreate(savedInstanceState);
        responsabilidadService = OrquestadorInteractorFactory.getResponsabilidadesService();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        siniestroService = CatalogoInteractorsFactory.getSiniestrosInteractor();
        compositeDisposable = mActivity.getCompositeDisposable();
        presenter = new CapturaResponsabilidadPresenter(mActivity.getCompositeDisposable());
        presenter.onAttach(this);
        if (getArguments() != null) {
            idAdjust = getArguments().getString("idAdjust");
            isActivaded = getArguments().getBoolean("guardar");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_responsabilidad, container, false);
        ButterKnife.bind(this, v);

        getAsegurado();
        imgTakeRes = v.findViewById(R.id.imgCaptureRes);
        imgRes = v.findViewById(R.id.imgRes);
        buttonGuardar = v.findViewById(R.id.buttonGuardar);
        buttonGuia = v.findViewById(R.id.buttonDeslinde);
        buttonGuardar.setVisibility(View.GONE);
        if (isActivaded) {
            buttonGuardar.setVisibility(View.VISIBLE);
        } else {
            buttonGuardar.setVisibility(View.GONE);
        }

        spinnerTipoTercero = v.findViewById(R.id.spinnerTipoTercero);
        getTiposTerceros();

        imgTakeRes.setOnClickListener(v -> {
            if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_CAMERA_PERMISSION)) {
                Intent intentCamera = new Intent(getActivity(), CustomCamera.class);
                CameraActivity.modulo = 5;
                startActivityForResult(intentCamera, 204);
            } else {
                RequestPermissions.requestPermission(RequestPermissions.REQUEST_CAMERA_PERMISSION,
                        getActivity());
            }
        });
        spinnerTipoTercero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                switch (tiposTercerosList.get(spinnerTipoTercero.getSelectedItemPosition()).getClave()) {
                    case "ST":
                        tipoTerceroSelected = 0;
                        buttonGuia.setVisibility(View.GONE);
                        break;
                    case "TA":
                        buttonGuia.setVisibility(View.VISIBLE);
                        tercerosSelected = tiposTercerosList.get(spinnerTipoTercero.getSelectedItemPosition());
                        tipoTerceroSelected = 1;
                        break;
                    case "TNA":
                        buttonGuia.setVisibility(View.GONE);
                        tipoTerceroSelected = 2;
                        break;
                    case "TFR":
                        buttonGuia.setVisibility(View.GONE);
                        tipoTerceroSelected = 3;
                        break;
                    case "TP":
                        buttonGuia.setVisibility(View.GONE);
                        tipoTerceroSelected = 4;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        buttonGuia.setOnClickListener(v12 -> {
            GuiaDeslindeFragment fragment2 = new GuiaDeslindeFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.contentFragment, fragment2);
            fragmentTransaction.addToBackStack("deslinde");
            fragmentTransaction.commit();
        });
        buttonGuardar.setOnClickListener(v -> DialogUtils.createAlert(getActivity(), "Guardar responsabilidad",
                "Una vez establecida la responsabilidad no podrá ser modificada ¿Estás seguro de querer guardar esta información?",
                "Aceptar", "Cancelar", (dialog, which) -> guardaResponsable(), (dialog, which) -> dialog.dismiss()));
        return v;
    }

    private void getAsegurado() {

        compositeDisposable.add(aseguradoInteractor.getConductor(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(conductorResponse -> {
                    if (conductorResponse != null) {
                        String name = conductorResponse.getNombre() != null ? conductorResponse.getNombre() : "";
                        String apellidoP = conductorResponse.getApellidoPaterno() != null ? conductorResponse.getApellidoPaterno() : "";
                        String apellidoM = conductorResponse.getApellidoMaterno() != null ? conductorResponse.getApellidoMaterno() : "";
                        tvAsegurado.setText(String.format("%s %s %s", name, apellidoP, apellidoM));
                    }

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el conductor asegurado", throwable);
                    if (!(throwable instanceof ResourceNotFoundException))
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 204 && data != null) {
                img_path = data.getStringExtra(Constants.KEY.IMG_PATH);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(img_path, options);
                imgRes.setImageBitmap(bitmap);
                Drawable loader;
                if (android.os.Build.VERSION.SDK_INT >= 28) {
                    loader = ContextCompat.getDrawable(getActivity(), R.drawable.image_loader);
                    ((Animatable) loader).start();
                } else {
                    loader = getResources().getDrawable(R.drawable.ic_no_photo);
                }
                Picasso.with(getActivity()).load(getImageUri(getActivity(), bitmap))
                        .resize(500, 500)
                        .transform(new CircleTransform())
                        .placeholder((Drawable) loader)
                        .into(imgRes);

            }
        }
    }

    Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void guardaResponsable() {

        String responsabilidadAjustador = null;
        boolean aseguradoResponsable = true;
        Long terceroID = null;
        boolean terceroResponsable = false;

        aseguradoResponsable = this.aseguradoResponsableCheck.isChecked();

        //Tercero

        terceroResponsable = terceroResponsableCheck.isChecked();

        //Tipo de Tercero y # Tercero
        switch (tipoTerceroSelected) {
            case 0:
                tipoTercero = "ST";
                terceroID = null;
                break;
            case 1:
                tipoTercero = "TA";
                terceroID = tercerosAutosList.size() != 0 ? tercerosAutosList.get(terceroSelected).getId() : null;
                break;
            case 2:
                tipoTercero = "TNA";
                terceroID = tercerosNoAutosList.size() != 0 ? tercerosNoAutosList.get(terceroSelected).getId() : null;
                break;
            case 3:
                tipoTercero = "TFR";
                if (tercerosAutosList.size() != 0) {
                    terceroID = tercerosAutosList.get(terceroSelected).getId();
                } else {
                    terceroID = null;
                }
                break;
            case 4:
                tipoTercero = "TP";
                terceroID = tercerosPeatonList.size() != 0 ? tercerosPeatonList.get(terceroSelected).getId() : null;
                break;
        }

        //hgzavala 13042018 - Responsable
        if (corresponsabilidadCheck.isChecked()) {
            responsabilidadAjustador = "C";
        } else if (noAplicaResponsablidadCheck.isChecked()) {
            responsabilidadAjustador = "NP";
        } else if (aseguradoResponsableCheck.isChecked()) {
            responsabilidadAjustador = "A";
        } else if (terceroResponsableCheck.isChecked()) {
            responsabilidadAjustador = "B";
        } else {
            responsabilidadAjustador = null;
        }

        Responsabilidad data = new Responsabilidad();
        data.setResponsabilidadAjustador(responsabilidadAjustador);
        if (mActivity.responsabilidad == null) {
            data.setResponsabilidadGuia(null);
        }
        if ((mActivity.responsabilidad) != null) {
            circunstanciaA = mActivity.responsabilidad.getCircunstanciaA().getId() == 0 ? 1 : mActivity.responsabilidad.getCircunstanciaA().getId();
            circunstanciaB = mActivity.responsabilidad.getCircunstanciaB().getId() == 0 ? 1 : mActivity.responsabilidad.getCircunstanciaB().getId();
            data.setResponsabilidadGuia(mActivity.responsabilidad.getResponsabilidad() != null ? mActivity.responsabilidad.getResponsabilidad().getClave() : null);
        }
        //tercero
        if (spinnerTipoTercero.getSelectedItem().toString().equals("Tercero Auto") || tercerosAutosList.size() != 0 ||
                spinnerTipoTercero.getSelectedItem().toString().equals("Tercero No Auto") || tercerosNoAutosList.size() != 0 ||
                spinnerTipoTercero.getSelectedItem().toString().equals("Tercero Peaton") || tercerosPeatonList.size() != 0) {
        }
        data.setTipoTercero(tipoTercero);
        //terceroResponsable
        if (terceroResponsableCheck.isChecked()) {
            data.setTerceroResponsable(true);
            data.setAseguradoResponsable(false);
        }
        if (aseguradoResponsableCheck.isChecked()) {
            data.setAseguradoResponsable(true);
            data.setTerceroResponsable(false);
        }
        if (corresponsabilidadCheck.isChecked()) {
            data.setTerceroResponsable(true);
            data.setAseguradoResponsable(true);
        }
        if (noAplicaResponsablidadCheck.isChecked()) {
            data.setTerceroResponsable(false);
            data.setAseguradoResponsable(false);

        }

        data.setAjuste(idAdjust);
        if (circunstanciaA != null ) {
            data.setCircunstanciaA(circunstanciaA);
        }
        if (circunstanciaB != null) {
            data.setCircunstanciaB(circunstanciaB);
        }

        if (img_path.equals("")) {
            ToastUtils.toastShort("Debes agregar la fotografía por favor");
            return;
        }
        if (responsabilidadAjustador != null) {
            compositeDisposable.add(responsabilidadService.addResponsabilidad(mActivity.idAjuste, data)
                    .subscribe(responsabilidadRequest -> {
                        new GeneralSnackBar().largeSnackOk(getActivity(), "Se agregó correctamente ");
                        uploadImage();
                    }, throwable -> {
                        Log.e(TAG, "Error al agregar la responsabilidad del ajuste " + mActivity.idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                    }));
        }
    }

    private void uploadImage() {
        if (!img_path.equals("")) {
            File file = new File(img_path);
            compositeDisposable.add(responsabilidadService.addFotografia(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), file)
                    .subscribe(fotografia -> {
                        Log.d("TAG", "onResponse: " + fotografia);
                        listener.onRefreshListener();
                    }, throwable -> {
                        Log.e(TAG, "Error al agregar fotografías de la responsabilidad del ajuste " + mActivity.idAjuste, throwable);
                        Snackbar snackbar = Snackbar.make(v, "Hubo un problema al subir la imagen, intentelo nuevamente", Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", v -> uploadImage());
                        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                        layout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorRedStatus));
                        TextView content = layout.findViewById(android.support.design.R.id.snackbar_text);
                        TextView action = snackbar.getView().findViewById(android.support.design.R.id.snackbar_action);
                        content.setMaxLines(3);
                        content.setTextSize(15);
                        action.setTextSize(15);
                        action.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        snackbar.show();
                    }));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (ResponsabilidadActivity) getActivity();

    }

    private void getTiposTerceros() {
        compositeDisposable.add(siniestroService.getTiposTercero()
                .subscribe(tipoTerceros -> {
                    tiposTercerosList = tipoTerceros;
                    if (tiposTercerosList != null) {
                        for (int i = 0; i < tiposTercerosList.size(); i++) {
                            tercerosName.add(tiposTercerosList.get(i).getNombre() != null ? tiposTercerosList.get(i).getNombre() : "");
                        }
                        if (getActivity() != null) {
                            AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, tiposTercerosList, CatalogoClave::getNombre);
                            spinnerTipoTercero.setAdapter(adapter);
                        }
                    }

                }, throwable -> {
                    Log.e(TAG, "Error al obtener los tipos de terceros", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));

    }


    private void verificaDesabilitaBotonGuardado() {
        if (!aseguradoResponsableCheck.isChecked() && !terceroResponsableCheck.isChecked() && !corresponsabilidadCheck.isChecked() && !noAplicaResponsablidadCheck.isChecked()) {
            buttonGuardar.setVisibility(View.GONE);
        } else if (aseguradoResponsableCheck.isChecked() || terceroResponsableCheck.isChecked() || corresponsabilidadCheck.isChecked() || noAplicaResponsablidadCheck.isChecked()) {
            buttonGuardar.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.corresponsabilidadCheck)
    public void setcorresponsabilidadCheck() {
        if (corresponsabilidadCheck.isChecked()) {
            terceroResponsableCheck.setChecked(true);
            aseguradoResponsableCheck.setChecked(true);
            terceroResponsableCheck.setEnabled(false);
            aseguradoResponsableCheck.setEnabled(false);
        } else {
            terceroResponsableCheck.setChecked(false);
            aseguradoResponsableCheck.setChecked(false);
            terceroResponsableCheck.setEnabled(true);
            aseguradoResponsableCheck.setEnabled(true);
        }
        verificaDesabilitaBotonGuardado();
    }


    @OnClick(R.id.terceroResponsableCheck)
    public void validaCorresponsabilidadTercerp() {
        if (aseguradoResponsableCheck.isChecked()) {
            aseguradoResponsableCheck.setChecked(false);
        }
        verificaDesabilitaBotonGuardado();

    }

    @OnClick(R.id.aseguradoResponsableCheck)
    public void validaCoraseguradoResponsableCheck() {
        if (terceroResponsableCheck.isChecked()) {
            terceroResponsableCheck.setChecked(false);
        }
        verificaDesabilitaBotonGuardado();

    }

    @OnClick(R.id.noAplicaResponsablidadCheck)
    public void noAplicaResponsabilidad() {
        if (noAplicaResponsablidadCheck.isChecked()) {
            aseguradoResponsableCheck.setChecked(false);
            terceroResponsableCheck.setChecked(false);
            corresponsabilidadCheck.setChecked(false);
            aseguradoResponsableCheck.setEnabled(false);
            terceroResponsableCheck.setEnabled(false);
            corresponsabilidadCheck.setEnabled(false);
        } else {
            aseguradoResponsableCheck.setEnabled(true);
            terceroResponsableCheck.setEnabled(true);
            corresponsabilidadCheck.setEnabled(true);
        }
        verificaDesabilitaBotonGuardado();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setRefreshListener(OnRefreshListener listener) {
        this.listener = listener;
    }

}
