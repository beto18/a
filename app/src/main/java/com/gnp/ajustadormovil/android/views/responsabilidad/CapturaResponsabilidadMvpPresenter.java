package com.gnp.ajustadormovil.android.views.responsabilidad;

import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

public interface CapturaResponsabilidadMvpPresenter<V extends CapturaResponsabilidadMvpView> extends MvpPresenter<V>{
    void getDataInsure();
}
