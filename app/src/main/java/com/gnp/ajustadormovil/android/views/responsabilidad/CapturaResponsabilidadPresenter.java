package com.gnp.ajustadormovil.android.views.responsabilidad;


import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

public class CapturaResponsabilidadPresenter<V extends CapturaResponsabilidadMvpView>
        extends BasePresenter<V> implements CapturaResponsabilidadMvpPresenter<V> {

    public CapturaResponsabilidadPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
    }

    @Override
    public void getDataInsure() {

    }
}
