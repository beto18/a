package com.gnp.ajustadormovil.android.views.responsabilidad;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.PagerImagesAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnFragmentChangerListener;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by oscar on 14/02/18.
 */

public class GuiaDeslindeFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public ResponsabilidadActivity mActivity;
    private SiniestrosInteractor siniestrosInteractor;

    public static final String TAG = "Error al cargar";

    @BindView(R.id.spinnerCircunstanciaA)
    Spinner spinnerA;
    @BindView(R.id.spinnerCircunstanciaB)
    Spinner spinnerB;

    @BindView(R.id.resultadoText)
    TextView resultadoText;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabDots)
    TabLayout tabLayout;
    @BindView(R.id.buttonContinuar)
    Button buttonContinuar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private OnFragmentInteractionListener mListener;
    private OnFragmentChangerListener listener;

    public GuiaDeslindeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GuiaDeslindeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GuiaDeslindeFragment newInstance(String param1, String param2) {
        GuiaDeslindeFragment fragment = new GuiaDeslindeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_guia_deslinde, container, false);
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        ButterKnife.bind(this, v);
        getCircunstancias();

//        ArrayAdapter<CharSequence> adapterCircunstancias = ArrayAdapter.createFromResource(getActivity(),R.array.circunstancias_array,android.R.layout.simple_spinner_item);
//        adapterCircunstancias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerA.setAdapter(adapterCircunstancias);
//        spinnerB.setAdapter(adapterCircunstancias);

        tabLayout.setupWithViewPager(viewPager);

        spinnerA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CatalogoId itmA = (CatalogoId) spinnerA.getSelectedItem();
                CatalogoId itmB = (CatalogoId) spinnerB.getSelectedItem();
//                int itm1 = spinnerA.getSelectedItemPosition()+1;
//                int itm2 = spinnerB.getSelectedItemPosition()+1;
                resultadoText.setText("");
                receiveWS(String.valueOf(itmA.getId()), String.valueOf(itmB.getId()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerB.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CatalogoId itmA = (CatalogoId) spinnerA.getSelectedItem();
                CatalogoId itmB = (CatalogoId) spinnerB.getSelectedItem();
//                int itm1 = spinnerA.getSelectedItemPosition()+1;
//                int itm2 = spinnerB.getSelectedItemPosition()+1;
                resultadoText.setText("");
                receiveWS(String.valueOf(itmA.getId()), String.valueOf(itmB.getId()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //listener.onChargeData(true);
                //closefragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .remove(GuiaDeslindeFragment.this).commit();
            }
        });

        return v;
    }

    private void closefragment() {
        CapturaResponsabilidadFragment fragment2 = new CapturaResponsabilidadFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.topContent, fragment2);
        fragmentTransaction.commit();

        //getFragmentManager().beginTransaction().hide(this).commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        /*if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            mActivity = (ResponsabilidadActivity) getActivity();
        }

       /* mActivity = (ResponsibilityActivity) getActivity();
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            listener = (OnFragmentChangerListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public void getCircunstancias() {

        ((BaseActivity) getActivity()).getCompositeDisposable().add(
                siniestrosInteractor.getCircustanciasVehiculo().subscribe(circunstancias->{
                    Collections.sort(circunstancias, (o1, o2) -> o1.getId().compareTo(o2.getId()));
                    AdapterSpinnerGeneral adapter = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, circunstancias, CatalogoId::getNombre);
                    spinnerA.setAdapter(adapter);
                    spinnerB.setAdapter(adapter);
                },throwable -> {
                    Log.e(TAG, "getCircunstancias: ", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));

    }

    public void receiveWS(String first, String second) {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(
                siniestrosInteractor.getResponsable(Long.parseLong(first), Long.parseLong(second))
                        .subscribe(responsable -> {
                            mActivity.responsabilidad = responsable;
                            String nombre = responsable.getResponsabilidad().getNombre();
                            String clave = responsable.getResponsabilidad().getClave();
                            resultadoText.setText(nombre);
                            String[] mResources = {responsable.getUrlImagen()};
                            PagerImagesAdapter adapter = new PagerImagesAdapter(getActivity(), mResources);
                            viewPager.setAdapter(adapter);
                        }, throwable -> {
                            Log.e(TAG, "receiveWS: ", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }));

    }
}
