package com.gnp.ajustadormovil.android.views.responsabilidad;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Responsable;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;

import butterknife.BindView;

/**
 * Created by oscar on 14/02/18.
 * Modified by hgzavala on 22/03/2018
 */

public class ResponsabilidadActivity extends BaseActivity implements ResponsabilidadMvpView,
        OnRefreshListener{

    public Responsable responsabilidad;
    public String idAjuste;
    boolean showGuardar = false;
    @BindView(R.id.toolbar_top)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView title;
    ResponsabilidadPresenter presenter;
    Bundle bundle = new Bundle();
    private OnRefreshListener listener;
    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responsabilidad);
        setmToolbar(mToolbar);
        listener = this;
        presenter = new ResponsabilidadPresenter(getCompositeDisposable());
        presenter.onAttach(this);
        if (getIntent().getExtras() != null){
            idAjuste = getIntent().getStringExtra("idAjuste");
            presenter.checkDataExist(idAjuste);
        }
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("", "create fragment");
    }

    @Override
    public void responsibilityExist(Responsabilidad response) {
        bundle.putSerializable("responseResponsibility", response.getAjuste());
        ResponsabilidadResumenFragment fragment = ResponsabilidadResumenFragment.newInstance(bundle);
        fragment.setRefreshListener(listener);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contentFragment,fragment , "responsibilityDetail")
                .commitAllowingStateLoss();
    }

    @Override
    public void responsibilityNoExist() {
        bundle.putSerializable("idAdjust", idAjuste);
        bundle.putBoolean("guardar", showGuardar);
        CapturaResponsabilidadFragment fragment = CapturaResponsabilidadFragment.newInstance(bundle);
        fragment.setRefreshListener(listener);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contentFragment, fragment, "responsibilityFragment")
                .commitAllowingStateLoss();
    }


    @Override
    public void onRefreshListener() {
        presenter.checkDataExist(idAjuste);
    }
}
