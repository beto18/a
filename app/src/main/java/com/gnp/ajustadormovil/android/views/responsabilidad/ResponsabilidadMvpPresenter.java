package com.gnp.ajustadormovil.android.views.responsabilidad;

import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

public interface ResponsabilidadMvpPresenter<V extends ResponsabilidadMvpView> extends MvpPresenter<V> {
    void checkDataExist(String idAjuste);
}
