package com.gnp.ajustadormovil.android.views.responsabilidad;

import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.views.commons.base.MvpView;

public interface ResponsabilidadMvpView extends MvpView {
    void responsibilityExist(Responsabilidad response);
    void responsibilityNoExist();
}
