package com.gnp.ajustadormovil.android.views.responsabilidad;

import android.util.Log;

import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

public class ResponsabilidadPresenter<V extends ResponsabilidadMvpView> extends BasePresenter<V>
        implements ResponsabilidadMvpPresenter<V> {

    private ResponsabilidadInteractor responsabilidadesServices;

    ResponsabilidadPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
    }

    @Override
    public void checkDataExist(String idAjuste) {
        responsabilidadesServices = OrquestadorInteractorFactory.getResponsabilidadesService();
        getmCompositeDisposable().add(responsabilidadesServices.getResponsabilidad(idAjuste)
                .subscribe(responsibilityResponse ->
                                getMvpView().responsibilityExist(responsibilityResponse),
                        throwable -> {
                            Log.e(TAG, "Error al obtener la responsabilidad del ajuste " + idAjuste, throwable);
                            getMvpView().responsibilityNoExist();
                        }));
    }
}
