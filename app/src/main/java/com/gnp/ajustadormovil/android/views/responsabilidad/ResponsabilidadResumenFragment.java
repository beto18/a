package com.gnp.ajustadormovil.android.views.responsabilidad;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.utils.media.CircleTransform;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class ResponsabilidadResumenFragment extends BaseFragment {

    private static final String TAG = ResponsabilidadResumenFragment.class.getCanonicalName();

    Responsabilidad responsibilityRes;
    @BindView(R.id.guideResponsible)
    AppCompatTextView guideResponsible;
    @BindView(R.id.adjusterResponsible)
    AppCompatTextView adjusterResponsible;
    @BindView(R.id.ivCheckResponsibleInsured)
    ImageView ivCheckResponsibleInsured;
    @BindView(R.id.ivCheckResponsibleThird)
    ImageView ivCheckResponsibleThird;
    @BindView(R.id.tvAseguradoR)
    AppCompatTextView tvAsegurado;
    @BindView(R.id.tvTerceroR)
    AppCompatTextView tvtercero;
    @BindView(R.id.tvGuiaRecuerdo)
    TextView recordatorio;
    @BindView(R.id.imgResponsability)
    ImageView imgResp;
    private OnRefreshListener listener;
    private TerceroAutoInteractor terceroAutoService;
    private AseguradoInteractor aseguradoInteractor;
    private TerceroNoAutoInteractor tercerosNoAutoService;
    private TerceroPersonaInteractor tercerosPersonaService;
    private ResponsabilidadInteractor responsabilidadesServices;
    private SiniestrosInteractor siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
    private View view;
    private CompositeDisposable compositeDisposable;

    public static ResponsabilidadResumenFragment newInstance(Bundle bundle) {
        ResponsabilidadResumenFragment fragment = new ResponsabilidadResumenFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        tercerosNoAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
        tercerosPersonaService = OrquestadorInteractorFactory.getTerceroPersonaService();
        responsabilidadesServices = OrquestadorInteractorFactory.getResponsabilidadesService();
        compositeDisposable = ((BaseActivity)getActivity()).getCompositeDisposable();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_responsabilidad_detail, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            String ajuste =  getArguments().getString("responseResponsibility");
            getResponsabilidad(ajuste);
        }

    }


    private void getResponsabilidad(String ajuste){
        compositeDisposable.add(responsabilidadesServices.getResponsabilidad(ajuste)
                .subscribe(responsabilidad ->{
                   responsibilityRes = responsabilidad;
                    setResponsabilidad();
                }, throwable ->{
                    Log.e(TAG, "Error al obtener la responsabilidad", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                } ));
    }

    private void setResponsabilidad() {
        if (responsibilityRes != null) {
            putResponsability(responsibilityRes.getResponsabilidadGuia(),guideResponsible);
            putResponsability(responsibilityRes.getResponsabilidadAjustador(),adjusterResponsible);
            if ((responsibilityRes.getTerceroResponsable() != null && responsibilityRes.getTerceroResponsable()) &&
                    (responsibilityRes.getAseguradoResponsable() != null && !responsibilityRes.getAseguradoResponsable())) {
                view.findViewById(R.id.tercero_responsable_container).setVisibility(View.VISIBLE);
                view.findViewById(R.id.asegurado_responsable_container).setVisibility(View.GONE);
                view.findViewById(R.id.responsabilidad_guia_container).setVisibility(View.VISIBLE);
                view.findViewById(R.id.responsabilidad_asegurado_container).setVisibility(View.VISIBLE);
                tvtercero.setVisibility(View.GONE);
                tvtercero.setText("Tercero responsable:" + "\n" + "Tercero Fuga/Robo");
            }
            if (responsibilityRes.getAseguradoResponsable() != null && responsibilityRes.getAseguradoResponsable()) {
                view.findViewById(R.id.tercero_responsable_container).setVisibility(View.GONE);
                view.findViewById(R.id.asegurado_responsable_container).setVisibility(View.VISIBLE);
                ivCheckResponsibleInsured.setVisibility(View.VISIBLE);
                recordatorio.setText("Recuerde imprimir todos los volantes y pases médicos por favor.");
                getAsegurado();
            }
            if (responsibilityRes.getTerceroResponsable() != null && responsibilityRes.getTerceroResponsable()) {
                ivCheckResponsibleThird.setVisibility(View.VISIBLE);
                view.findViewById(R.id.tercero_responsable_container).setVisibility(View.VISIBLE);
                view.findViewById(R.id.asegurado_responsable_container).setVisibility(View.GONE);
                recordatorio.setText("No olvide imprimir el volante y pases médicos del asegurado y ocupantes por favor.");

                if (responsibilityRes.getTipoTercero() != null) {
                    if (responsibilityRes.getTipoTercero().equals("TA")) {
                        tvtercero.setText("Tercero auto");
                    }
                    if (responsibilityRes.getTipoTercero().equals("TP")) {
                        tvtercero.setText("Tercero persona");
                    }
                    if (responsibilityRes.getTipoTercero().equals("TNA")) {
                        tvtercero.setText("Tercero no auto");
                    }
                } else {
                    tvtercero.setText("Tercero responsable:" + "\n" + "Tercero Fuga/Robo");
                    recordatorio.setText("No olvide imprimir el volante y pases médicos del asegurado aplicando su deducible por favor.");
                }
            }


            if ((responsibilityRes.getTerceroResponsable() != null && responsibilityRes.getTerceroResponsable()) &&
                    (responsibilityRes.getAseguradoResponsable() != null && !responsibilityRes.getAseguradoResponsable())) {
                view.findViewById(R.id.tercero_responsable_container).setVisibility(View.VISIBLE);
                view.findViewById(R.id.asegurado_responsable_container).setVisibility(View.VISIBLE);
                ivCheckResponsibleInsured.setVisibility(View.VISIBLE);
                ivCheckResponsibleThird.setVisibility(View.VISIBLE);
                getAsegurado();
                recordatorio.setText("No olvide generar la recuperación correspondiente por favor.");
                if (responsibilityRes.getTipoTercero() != null) {
                    if (responsibilityRes.getTipoTercero().equals("TA")) {
                        tvtercero.setText("Tercero auto");
                    }
                    if (responsibilityRes.getTipoTercero().equals("TP")) {
                        tvtercero.setText("Tercero persona");
                    }
                    if (responsibilityRes.getTipoTercero().equals("TNA")) {
                        tvtercero.setText("Tercero no auto");
                    }
                }
            }
            if (responsibilityRes.getFotografias() != null && responsibilityRes.getFotografias().size() != 0) {

                Drawable loader;
                if (Build.VERSION.SDK_INT >= 28) {
                    loader = ContextCompat.getDrawable(getActivity(), R.drawable.image_loader);
                    ((Animatable) loader).start();
                } else {
                    loader = getResources().getDrawable(R.drawable.ic_no_photo);
                }

                String url = responsibilityRes.getFotografias().get(0).getUrl();

                (url.startsWith("http") ? Picasso.with(getActivity()).load(url) : Picasso.with(getActivity()).load(new File(url)))
                        .resize(600, 800)
                        .transform(new CircleTransform())
                        .error(R.drawable.ic_close_orange)
                        .placeholder((Drawable) loader)
                        .into(imgResp);

            }
        }
    }



    private void putResponsability(String clave, TextView textView) {
        if(clave != null) {
            compositeDisposable.add(siniestrosInteractor.getResponsabilidades(clave)
                    .subscribe(res -> {
                        textView.setText(res.getNombre());
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener el responsable", throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                    }));
        } else {
            textView.setText("--");
        }
    }

    public void deleteResponsability() {
        new MaterialDialog.Builder(getActivity())
                .title("Eliminar responsabilidad...")
                .content("¿Desea eliminar la responsabilidad?")
                .positiveText("ACEPTAR")
                .onPositive((dialog, which) -> {
                    deleteResponsibility(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL));
                })
                .negativeText("CANCELAR")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build()
                .show();
    }

    private void deleteResponsibility(String ajuste) {
        compositeDisposable.add(responsabilidadesServices.deleteResponsabilidad(ajuste)
                .subscribe(() -> {
                    listener.onRefreshListener();
                }, throwable -> {
                    Log.e(TAG, "Error al borrar la responsabilidad", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));
    }


    private void getAsegurado() {
       compositeDisposable.add(aseguradoInteractor.getConductor(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(response -> {
                    if (response.getNombreCompleto() != null)
                        tvAsegurado.setText(response.getNombreCompleto());
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el conductor asegurado", throwable);
                    if (!(throwable instanceof ResourceNotFoundException))
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));
    }

    private void getTercero(Long id) {
        if (id != null) {
            compositeDisposable.add(terceroAutoService.get(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), id).subscribe(response -> {
                    tvAsegurado.setText(response.getDescripcion());
            }, throwable -> {
                Log.e(TAG, "Error al obtener el tercero auto", throwable);
                new GeneralSnackBar().largeSnackError(getActivity(), throwable);
            }));
        }
    }


    private void getNoAuto(Long id) {
        compositeDisposable.add(tercerosNoAutoService.get(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), id)
                .subscribe(response -> {
                    tvtercero.setText(response.getDescripcion());
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el tercero no auto", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));
    }

    private void getTerceroPeaton(String id) {
      compositeDisposable.add(tercerosPersonaService.get(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), Integer.parseInt(id))
                .subscribe(response -> {
                    if (response.getPersona().getNombreCompleto() != null)
                        tvtercero.setText(response.getDescripcion());
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el tercero persona", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                }));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setRefreshListener(OnRefreshListener listener) {
        this.listener = listener;
    }
}
