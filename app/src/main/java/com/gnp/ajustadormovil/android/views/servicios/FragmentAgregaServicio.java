package com.gnp.ajustadormovil.android.views.servicios;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by OscarMichael on 2/2/18.
 */

public class FragmentAgregaServicio extends BaseFragment {

    private Unbinder unbinder;
    private OnToolbarListener listener;
    public static final String TAG = "TAG";

    private static OnRefreshListener onRefreshListener;

    public static void setRefreshListener(OnRefreshListener l) {
        onRefreshListener = l;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            listener = (OnToolbarListener) context;
        } else {
            Log.d(TAG, "onAttach: I need instace of Toolbar interface");
        }

    }

    public static FragmentAgregaServicio newInstance(Bundle args) {
        FragmentAgregaServicio servicio = new FragmentAgregaServicio();
        servicio.setArguments(args);
        return servicio;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.servicio_container, container, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }


    @OnClick(R.id.imgTransferencia) //patrulla
    public void onCheckedOptions_patrulla() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.POLICE);
        Servicios_Maps_Activity.newService(Constants.POLICE);
        listener.onChangeTitle("Servicio de Patrulla");
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);
    }

    @OnClick(R.id.imgPrendaria) //ambulancia
    public void onCheckedOptions_ambulancia() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.AMBULANCE);
        Servicios_Maps_Activity.newService(Constants.AMBULANCE);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Ambulancia");
    }

    @OnClick(R.id.imgCheques) //recuperador
    public void onCheckedOptions_recuperador() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.RECUPERATOR);
        Servicios_Maps_Activity.newService(Constants.RECUPERATOR);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Recuperador");
    }

    @OnClick(R.id.imgCheques3) //funerarios
    public void onCheckedOptions_funerarios() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.FUNERARY);
        Servicios_Maps_Activity.newService(Constants.FUNERARY);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Funeraria");
    }

    @OnClick(R.id.imgTaxi) //taxi
    public void onCheckedOptions_taxi() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.TAXI);
        Servicios_Maps_Activity.newService(Constants.TAXI);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Taxi");
    }

    @OnClick(R.id.imgEfectivo) //verificador
    public void onCheckedOptions_verificador() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.CHECKER);
        Servicios_Maps_Activity.newService(Constants.CHECKER);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Verificador");
    }

    @OnClick(R.id.imgTarjeta) //grua
    public void onCheckedOptions_grua() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.CRANE);
        Servicios_Maps_Activity.newService(Constants.CRANE);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Grúa");
    }

    @OnClick(R.id.imgEfectivo3) //abogado
    public void onCheckedOptions_abogado() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.LAWYER);
        Servicios_Maps_Activity.newService(Constants.LAWYER);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Abogado");
    }

    @OnClick(R.id.imgAmbulancia) //contratista
    public void onCheckedOptions_contratista() {
        addFragment(Servicios_Maps_Activity.newInstance(getArguments()), Constants.CONTRACTOR);
        Servicios_Maps_Activity.newService(Constants.CONTRACTOR);
        Servicios_Maps_Activity.setRefreshListener(onRefreshListener);

        listener.onChangeTitle("Servicio de Contratista");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        listener.onDefaulTitle();
    }

    private void addFragment(Fragment fragment, String tag) {
        FragmentManager manager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.servicios_container, fragment);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.addToBackStack(tag);
        transaction.commit();
    }
}
