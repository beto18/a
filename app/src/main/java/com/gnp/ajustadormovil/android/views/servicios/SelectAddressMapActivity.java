package com.gnp.ajustadormovil.android.views.servicios;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectAddressMapActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title_servicios)
    TextView toolbarTitleServicios;
    @BindView(R.id.toolbar_map)
    Toolbar toolbarMap;
    @BindView(R.id.placeSelected)
    ImageView placeSelected;
    @BindView(R.id.btnConfirmPlace)
    Button btnConfirmPlace;
    @BindView(R.id.contentMapSelectable)
    ConstraintLayout contentMapSelectable;

    private Activity activity;

    private GoogleMap gMap;
    private LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address_map);
        ButterKnife.bind(this);
        activity = this;
        setMap();
    }

    private void setMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fSelectableAddress);
        mapFragment.getMapAsync(onMap);
    }

    OnMapReadyCallback onMap = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            gMap = googleMap;
            if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_LOCATION_PERMISSION)) {
                setupMap();
            } else {
                RequestPermissions.requestPermission(RequestPermissions.REQUEST_LOCATION_PERMISSION,
                        activity);
            }

        }
    };

    @SuppressLint("MissingPermission")
    private void setupMap() {
        gMap.setMyLocationEnabled(true);
        CameraUpdate camUpd1 = CameraUpdateFactory
                .newLatLngZoom(new LatLng(19.4190383, -99.1316205), 9f);
        gMap.moveCamera(camUpd1);
        gMap.setOnCameraIdleListener(() -> latLng = gMap.getCameraPosition().target);
    }

    @OnClick(R.id.btnConfirmPlace)
    public void finishConfirm(){
        Intent i = new Intent();
        i.putExtra(Constants.LAT,latLng.latitude);
        i.putExtra(Constants.LNG,latLng.longitude);
        setResult(Activity.RESULT_OK,i);
        finish();
    }

}
