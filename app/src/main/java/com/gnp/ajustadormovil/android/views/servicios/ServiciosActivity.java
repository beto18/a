package com.gnp.ajustadormovil.android.views.servicios;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.NetworkException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ConnectionProvider;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ServiciosCoberturaInteractor;
import com.gnp.ajustadormovil.android.models.ComentarioCancelacion;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.services.ServicioResponse;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.NothingSelectedSpinnerAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.RealmList;

import static com.gnp.ajustadormovil.android.models.AppStatus.context;

/**
 * Created by OscarMichael on 2/1/18.
 */

@SuppressLint("SetTextI18n")
public class ServiciosActivity extends BaseActivity implements OnItemRemovedListener, OnToolbarListener,
        CallbackClickItem, OnRefreshListener {
    private static String idAjuste;

    @BindView(R.id.toolbar_servicios)
    Toolbar mToolbar;
    @BindView(R.id.rvServicios)
    RecyclerView rvServiciosItems;
    @BindView(R.id.toolbar_title_servicios)
    TextView title;
    private ServiciosAdapter adapter;
    private LinearLayoutManager manager;
    private List<ServicioResponse> serviciosList;
    private Bundle bundle;
    private CallbackClickItem listener;
    private Activity activity;
    private List<CatalogoClave> typeServicesList;
    private Menu menu;
    private AdapterSpinnerGeneral adapterSpinnerGeneral;
    private Spinner mySpinner;
    private String idMotivo = "";
    private ServiciosCoberturaInteractor serviciosCoberturaInteractor;
    private TprInteractor tprService;
    private CatInteractor catInteractor;
    private SiniestrosInteractor siniestrosInteractor;
    private final ConnectionProvider connection = ConnectionProvider.getInstance();
    private List<String> servicios = new ArrayList<>();
    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean connection = ConnectionProvider.getInstance().isConnected();
            if (!connection) {
                switchModoOffline();
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviciosCoberturaInteractor = OrquestadorInteractorFactory.getServiciosCoberturaInteractor();
        tprService = CatalogoInteractorsFactory.getTprService();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        registerReceiver(networkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        try {
            if (AppPreferences.listServices != null) {
                serviciosList = AppPreferences.listServices;
            } else {
                AppPreferences.listServices = serviciosList;
            }
        } catch (Exception e) {
            Log.e(TAG, "Error al obtener los servicios", e);
        }

        setContentView(R.layout.activity_servicios);
        setmToolbar(mToolbar);
        if (getIntent() != null) {
            idAjuste = getIntent().getStringExtra("idAjuste");
        }
        activity = this;
        getTypeServices();
        servicioCobertura();
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        listener = this;
        bundle = getIntent().getExtras();
    }

    private void addorReplaceFragment(Fragment fragment){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        for(int i = 0; i < manager.getBackStackEntryCount(); ++i) {
            manager.popBackStack();
        }
        transaction.add(R.id.servicios_container, fragment);
        FragmentAgregaServicio.setRefreshListener(this);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.commitAllowingStateLoss();
    }

    private void addFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.servicios_container, fragment);
        FragmentAgregaServicio.setRefreshListener(this);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.commitAllowingStateLoss();
    }

    private void getTypeServices() {
        if (ConnectionProvider.getInstance().isConnected()) {
            getCompositeDisposable().add(siniestrosInteractor.getTiposServicio()
                    .subscribe(object -> {
                        typeServicesList = object;
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener los tipos de servicios", throwable);
                        new GeneralSnackBar().largeSnackError(ServiciosActivity.this, throwable);
                    }));
        }
    }

    private void switchModoOffline(){
        // TODO: Como no está conectado a interner, hay que mandarle un mensaje de aviso al usuario de que se quedo sin internet y se debería mostrar la pantalla offline

        DialogUtils.createAlert(this, "Modo Offline",
                "Debido a fallas con la red del dispositivo, se cambiara a modo offline",
                "Aceptar", "",
                (dialog, which) -> dialog.dismiss(), null);
        addorReplaceFragment(new ServiciosOfflineFragment());
    }

    public void servicioCobertura() {
        getCompositeDisposable().add(serviciosCoberturaInteractor.getServicioCobertura(idAjuste)
                .subscribe(object -> {
                    serviciosList = object;
                    manager = new LinearLayoutManager(context);
                    manager.setOrientation(LinearLayoutManager.VERTICAL);
                    rvServiciosItems.setLayoutManager(manager);
                    if (AppPreferences.listServices != null && serviciosList.size() >= AppPreferences.listServices.size()) {
                        AppPreferences.setListServices(serviciosList);
                        adapter = new ServiciosAdapter(AppPreferences.listServices, listener, typeServicesList, getBaseActivity());
                    } else {
                        AppPreferences.setListServices(serviciosList);
                        adapter = new ServiciosAdapter(serviciosList, listener, typeServicesList, getBaseActivity());
                    }
                    rvServiciosItems.setAdapter(adapter);
                }, throwable -> {
                    if (throwable instanceof NetworkException) {
                        switchModoOffline();
                    } else {
                        Log.e(TAG, "Error al obtener el servicio de cobertura, ajuste: " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(ServiciosActivity.this, throwable);
                    }
                }));
    }

    public void servicioCobertura(String status, long servicio) {
        getCompositeDisposable().add(serviciosCoberturaInteractor.getServicioCobertura(idAjuste)
                .subscribe(object -> {
                    if (object != null && serviciosList != null) {
                        serviciosList.clear();
                    }
                    serviciosList = object;
                    for (int i = 0; i < serviciosList.size(); i++) {
                        if (serviciosList.get(i).getId() == servicio) {
                            serviciosList.get(i).setStatus(status);
                        }
                    }
                    if (AppPreferences.listServices != null && serviciosList.size() >= AppPreferences.listServices.size()) {
                        AppPreferences.setListServices(serviciosList);
                        adapter = new ServiciosAdapter(AppPreferences.listServices, listener, typeServicesList, getBaseActivity());
                    } else {
                        AppPreferences.setListServices(serviciosList);
                        adapter = new ServiciosAdapter(serviciosList, listener, typeServicesList, getBaseActivity());
                    }
                    manager = new LinearLayoutManager(getBaseActivity());
                    manager.setOrientation(LinearLayoutManager.VERTICAL);
                    rvServiciosItems.setLayoutManager(manager);
                    adapter = new ServiciosAdapter(serviciosList, listener, typeServicesList, getBaseActivity());
                    rvServiciosItems.setAdapter(adapter);
                }, throwable -> {
                    if (throwable instanceof NetworkException) {
                        switchModoOffline();
                    } else {
                        Log.e(TAG, "Error al obtener los servicios de cobertura del ajuste " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }
                }));
    }

    public void servicioCoberturaDelete(int idServicio, String motivo, String comentario) {
        Log.d(TAG, "servicioCoberturaDelete: "+serviciosList.get(adapter.getPositionService()).getTipoServicio());
        getCompositeDisposable().add(serviciosCoberturaInteractor.getServicioCoberturaDelete(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL)
                , idServicio, new ComentarioCancelacion(comentario, motivo))
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(activity, "Servicio cancelado con éxito!");
                    servicioCobertura(getResources().getString(R.string.canel_service), idServicio);
                }, throwable -> {
                    if (throwable instanceof NetworkException) {
                        switchModoOffline();
                    } else {
                        Log.e(TAG, "Error al cancelar el servicio de cobertura " + idServicio + " del ajuste " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(ServiciosActivity.this, throwable);
                    }
                }));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
    }

    @Override
    public void onLongItemClick(View view, int position) {
    }

    @Override
    public void changeStatus(View view, int position) {
    }

    @OnClick(R.id.btnAgregarServicio)
    public void addRecuperation() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.servicios_container, FragmentAgregaServicio.newInstance(bundle));
        FragmentAgregaServicio.setRefreshListener(this);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.addToBackStack("agregar_servicio_maps");
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onChangeTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void onDefaulTitle() {
        title.setText("Servicios");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkReceiver);
    }

    @Override
    public void onItemClickItem(long position, View view) {
        if (view.getId() == R.id.imgArrivedItem) {
            DialogUtils.createAlert(this, getString(R.string.title_quest_arrived_service),
                    getString(R.string.desc_quest_arrived_service), getString(R.string.btn_accept_text),
                    getString(R.string.btn_cancel_text), (dialog, which) -> execArrive(position), (dialog, which) -> dialog.cancel());

        } else {
            showDialogCancel((int) position);
        }
    }

    private void execArrive(long position) {
        getCompositeDisposable().add(serviciosCoberturaInteractor.sendArribo(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), Long.toString(position))
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(ServiciosActivity.this, "Se ha arribado correctamente el servicio");
                    servicioCobertura(getResources().getString(R.string.arrived_service), position);
                }, throwable -> {
                    if (throwable instanceof NetworkException) {
                        switchModoOffline();
                    } else {
                        String message = ErrorUtils.getErrorMessage(throwable);
                        Log.e(TAG, "Error al arribar servicio de cobertura " + position + " del ajuste " + AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), throwable);
                        new GeneralSnackBar().largeSnackError(ServiciosActivity.this, message);
                    }
                }));

    }

    private List<CatalogoClave> getMotivosCancelacion(Dialog dialog) {
        RealmList<CatalogoClave> motivosCancelacions = new RealmList<>();
        getCompositeDisposable().add(siniestrosInteractor.getMotivosCancelacionAjuste()
                .subscribe(
                        motivoCancelServicio -> {
                            motivosCancelacions.addAll(motivoCancelServicio);

                            final Spinner spinnerMotivos = dialog.findViewById(R.id.spMotivoServicio);


                            AdapterSpinnerGeneral adapterspinner = new AdapterSpinnerGeneral<>(getBaseContext(), android.R.layout.simple_spinner_item, motivosCancelacions, CatalogoClave::getNombre);
                            // adapterspinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerMotivos.setAdapter(new NothingSelectedSpinnerAdapter(adapterspinner, R.layout.contact_spinner_row_nothing_selected, getBaseContext()));
                            spinnerMotivos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                    if (adapterView != null) {
                                        CatalogoClave item = (CatalogoClave) adapterView.getItemAtPosition(position);
                                        if (item != null) {
                                            setIdMotivo(item.getClave());
                                        }
                                    }


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });


                        }, throwable -> {
                            if (throwable instanceof NetworkException) {
                                switchModoOffline();
                            } else {
                                Log.e(TAG, "Error al obtener los motivos de cancelación", throwable);
                                new GeneralSnackBar().largeSnackError(this, throwable);
                            }
                        }));

        return motivosCancelacions;
    }


    public void showDialogCancel(int position) {
        ArrayList<String> motivod = new ArrayList<>();
        final Dialog dialog = new Dialog(activity, R.style.AlertDialogTheme);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_cancelation_service);
        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        final TextView textViewTitle = dialog.findViewById(R.id.TextTitleDialod);
        final EditText editTextObservaciones = dialog.findViewById(R.id.edObservacionesServicio);
        textViewTitle.setText(R.string.cancel_servicio);

        getMotivosCancelacion(dialog);

        Button dialogBtn_cancel = dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(v -> dialog.dismiss());

        Button dialogBtn_okay = dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(v -> {
            if (editTextObservaciones.getText().toString().isEmpty()) {
                ToastUtils.toastLong(getString(R.string.add_oberserver));
                return;
            } else if (getIdMotivo().isEmpty()) {
                ToastUtils.toastLong(getString(R.string.add_motivos));
                return;
            } else {
                String observaciones = editTextObservaciones.getText().toString();
                servicioCoberturaDelete(position, getIdMotivo(), observaciones);
                dialog.dismiss();

            }
            dialog.cancel();
        });

        dialog.show();


    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRefreshListener() {
        servicioCobertura();
    }

    public void setIdMotivo(String idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getIdMotivo() {
        return this.idMotivo;
    }



}
