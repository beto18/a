package com.gnp.ajustadormovil.android.views.servicios;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.services.ServicioResponse;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.commons.FormatStrings;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackClickItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RU on 2/1/18.
 */

public class ServiciosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ServicioResponse> serviciosList;
    private CallbackClickItem clickItem;
    private List<CatalogoClave> typeService;
    private Activity mContext;
    private Integer posicion;


    ServiciosAdapter(List<ServicioResponse> ServiciosList, CallbackClickItem listener, List<CatalogoClave> typeServicesList, Activity mContext) {
        this.serviciosList = ServiciosList;
        this.clickItem = listener;
        this.typeService = typeServicesList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Constants.TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_servicios, parent, false);
            return new ServiciosHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item, parent, false);
            return new EmptyViewHolder(view);
        }
    }

    //TODO Configuracion de la lista de servicios solicitados (acciones y colores)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ServiciosHolder) {
            String typeServiceName = "";
            if (typeService != null) {
                for (CatalogoClave ts : typeService) {
                    if (ts.getClave().equals(serviciosList.get(position).getTipoServicio())) {
                        typeServiceName = ts.getNombre();
                        break;
                    }
                }
            }
            ((ServiciosHolder) holder).tvTitle.setText(typeServiceName);
            ((ServiciosHolder) holder).tvContent.setText("Fecha de Solicitud: " + FormatStrings.formatDateMobile(serviciosList.get(position).getSolicitud()));


            if(typeServiceName.equals("Grúa")){
            ((ServiciosHolder) holder).serviceArrived.setVisibility(View.VISIBLE);
            ((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.black));
            }else {
                ((ServiciosHolder) holder).serviceArrived.setVisibility(View.GONE);
            }

            ((ServiciosHolder) holder).serviceArrived.setOnClickListener(v -> clickItem.onItemClickItem(serviciosList.get(position).getId(), ((ServiciosHolder) holder).serviceArrived));
            ((ServiciosHolder) holder).imgRemovedItem.setOnClickListener(v -> { posicion = position; clickItem.onItemClickItem(serviciosList.get(position).getId(), ((ServiciosHolder) holder).imgRemovedItem); });

            if(typeServiceName!= null){
            switch (typeServiceName)
            {

                case "Grúa":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getDrawable(R.drawable.grua));
                    break;
                case "Patrulla":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.patrulla));
                    break;
                case "Ambulancia":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ambulancia));
                    break;
                case "Verificador":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.verificador));
                    break;
                case "Recuperador":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.recuperador));
                    break;
                case "Contratista":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.contratista));
                    break;
                case "Abogado":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.abogado));
                    break;
                case "Servicios Funerarios":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.serviciosfunebres));
                    break;
                case "Taxi":
                    ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.taxi));
                    break;
            }
            if (serviciosList.get(position).getStatus() == null){
                ((ServiciosHolder) holder).imgRemovedItem.setEnabled(true);
                ((ServiciosHolder) holder).tvEstatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
                ((ServiciosHolder) holder).tvEstatus.setText(mContext.getResources().getString(R.string.solicit_service));
                ((ServiciosHolder) holder).serviceArrived.setEnabled(true);
                ((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.black));

            }else {
                switch (serviciosList.get(position).getStatus()) {
                    case "X": case "CANCELADO":case "Cancelado":
                        ((ServiciosHolder) holder).serviceArrived.setEnabled(false);
                        ((ServiciosHolder) holder).serviceArrived.setVisibility(View.GONE);
                        ((ServiciosHolder) holder).tvEstatus.setText(mContext.getResources().getString(R.string.canel_service));
                        ((ServiciosHolder) holder).imgRemovedItem.setColorFilter(mContext.getResources().getColor(R.color.grey_icon));
                        ((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.grey_icon));
                        ((ServiciosHolder) holder).serviceArrived.setEnabled(false);
                        ((ServiciosHolder) holder).imgRemovedItem.setEnabled(false);
                        ((ServiciosHolder) holder).tvEstatus.setTextColor(mContext.getResources().getColor(R.color.md_red_500));
                        break;
                    case "LLEGADO": case "L": case "Llegado": case "Arribado": case "ARRIBADO":
                        ((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.grey_icon));
                        ((ServiciosHolder) holder).imgRemovedItem.setColorFilter(mContext.getResources().getColor(R.color.grey_icon));
                        ((ServiciosHolder) holder).imgRemovedItem.setEnabled(false);
                        ((ServiciosHolder) holder).serviceArrived.setEnabled(false);
                        ((ServiciosHolder) holder).tvEstatus.setVisibility(View.VISIBLE);
                        ((ServiciosHolder) holder).tvEstatus.setText(mContext.getResources().getString(R.string.arrived_service));
                        ((ServiciosHolder) holder).tvEstatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
                        break;

                    case "ASIGNADO":case "A":
                        ((ServiciosHolder) holder).imgRemovedItem.setColorFilter(mContext.getResources().getColor(R.color.black));
                        ((ServiciosHolder) holder).imgRemovedItem.setEnabled(true);
                        ((ServiciosHolder) holder).tvEstatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
                        ((ServiciosHolder) holder).tvEstatus.setText("Asignado");
                        ((ServiciosHolder) holder).serviceArrived.setEnabled(true);
                        ((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.black));
                        break;

                    case "CONFIRMADO": case "C":
                        ((ServiciosHolder) holder).imgRemovedItem.setColorFilter(mContext.getResources().getColor(R.color.black));
                        ((ServiciosHolder) holder).imgRemovedItem.setEnabled(true);
                        ((ServiciosHolder) holder).tvEstatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
                        ((ServiciosHolder) holder).tvEstatus.setText("Confirmado");
                        ((ServiciosHolder) holder).serviceArrived.setEnabled(true);
                        ((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.black));
                        break;
                    default:
                        //((ServiciosHolder) holder).serviceArrived.setColorFilter(mContext.getResources().getColor(R.color.black));
                        //((ServiciosHolder) holder).imgRemovedItem.setColorFilter(mContext.getResources().getColor(R.color.colorServiceText));

                        break;
                }
            }
            }else if(serviciosList.get(position).getTipoServicio() != null) {
                switch (serviciosList.get(position).getTipoServicio())
                {

                    case "GR":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getDrawable(R.drawable.grua));
                        ((ServiciosHolder) holder).tvTitle.setText("Grúa");
                        ((ServiciosHolder) holder).serviceArrived.setVisibility(View.VISIBLE);
                        ((ServiciosHolder) holder).serviceArrived.setEnabled(false);
                        break;
                    case "PA":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.patrulla));
                        ((ServiciosHolder) holder).tvTitle.setText("Patrulla");
                        break;
                    case "AM":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ambulancia));
                        ((ServiciosHolder) holder).tvTitle.setText("Ambulancia");
                        break;
                    case "VE":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.verificador));
                        ((ServiciosHolder) holder).tvTitle.setText("Verificador");
                        break;
                    case "RE":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.recuperador));
                        ((ServiciosHolder) holder).tvTitle.setText("Recuperador");
                        break;
                    case "CO":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.contratista));
                        ((ServiciosHolder) holder).tvTitle.setText("Contratista");
                        break;
                    case "AB":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.abogado));
                        ((ServiciosHolder) holder).tvTitle.setText("Abogado");
                        ((ServiciosHolder) holder).serviceArrived.setVisibility(View.VISIBLE);
                        break;
                    case "SF":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.serviciosfunebres));
                        ((ServiciosHolder) holder).tvTitle.setText("Servicio funerario");
                        break;
                    case "TA":
                        ((ServiciosHolder) holder).ivIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.taxi));
                        ((ServiciosHolder) holder).tvTitle.setText("Taxi");
                        break;
                }
            }
        } else {
            ((EmptyViewHolder) holder).tvEmptyItem.setText(AjustadorMovilApplication.getInstance().getString(R.string.recycler_not_services));
        }
    }

    public Integer getPositionService(){
        return posicion;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (serviciosList.isEmpty()) return 1;
        else return serviciosList.size();
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvEmptyItem)
        TextView tvEmptyItem;

        EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ServiciosHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        AppCompatTextView tvTitle;
        @BindView(R.id.tvContent)
        AppCompatTextView tvContent;
        @BindView(R.id.tvEstatus)
        AppCompatTextView tvEstatus;
        @BindView(R.id.imgRemovedItem)
        ImageView imgRemovedItem;
        @BindView(R.id.imgArrivedItem)
        ImageView serviceArrived;


        ServiciosHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (serviciosList.isEmpty()) {
            return Constants.TYPE_EMPTY;
        } else {
            return Constants.TYPE_ITEM;
        }
    }
}
