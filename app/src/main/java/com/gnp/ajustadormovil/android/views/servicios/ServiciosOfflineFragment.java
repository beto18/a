package com.gnp.ajustadormovil.android.views.servicios;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ServiciosCoberturaInteractor;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiciosOfflineFragment extends BaseFragment {

    @BindView(R.id.cbGrua)
    CheckBox cbGrua;
    @BindView(R.id.cbPatrulla)
    CheckBox cbPatrulla;
    @BindView(R.id.cbAmbulanciaC)
    CheckBox cbAmbulanciaC;
    @BindView(R.id.cbVerificador)
    CheckBox cbVerificador;
    @BindView(R.id.cbContratista)
    CheckBox cbContratista;
    @BindView(R.id.cbRecuperador)
    CheckBox cbRecuperador;
    @BindView(R.id.cbAbogado)
    CheckBox cbAbogado;
    @BindView(R.id.cbTaxi)
    CheckBox cbTaxi;
    @BindView(R.id.cbFunerario)
    CheckBox cbFunerario;
    @BindView(R.id.imgCheckGrua)
    ImageView imgCheckGrua;
    @BindView(R.id.imgCheckPatrulla)
    ImageView imgCheckPatrulla;
    @BindView(R.id.imgCheckAmbulancia)
    ImageView imgCheckAmbulancia;
    @BindView(R.id.imgCheckVerificador)
    ImageView imgCheckVerificador;
    @BindView(R.id.imgCheckContratista)
    ImageView imgCheckContratista;
    @BindView(R.id.imgCheckAbogado)
    ImageView imgCheckAbogado;
    @BindView(R.id.imgCheckTaxi)
    ImageView imgCheckTaxi;
    @BindView(R.id.imgCheckRecuperador)
    ImageView imgCheckRecuperador;
    @BindView(R.id.imgCheckFunerario)
    ImageView imgCheckFunerario;
    private List<String> serviciosSeleccionados = new ArrayList<>();
    private ServiciosCoberturaInteractor serviciosCoberturaInteractor;
    private String idAjuste = "";
    public static final String TAG = "TAG";

    public static ServiciosOfflineFragment newInstance(Bundle args){
        ServiciosOfflineFragment fragment = new ServiciosOfflineFragment();
        if (args!=null){
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_servicios_contingencia, container, false);
        ButterKnife.bind(this, rootView);
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        serviciosCoberturaInteractor = OrquestadorInteractorFactory.getServiciosCoberturaInteractor();
        fillChecks();
        initViews();
        return rootView;
    }

    private void fillChecks() {
        ((BaseActivity)getActivity()).getCompositeDisposable().add(serviciosCoberturaInteractor.getResumen(idAjuste)
                .subscribe(servicios -> {
                    for (String servicio: servicios){
                        addCheckService(servicio);
                    }
                },throwable -> {
                    Log.e(TAG, "addServices: ", throwable);
                }));
    }

    private void addCheckService(String servicio) {
        switch (servicio){
            case "GR":
                cbGrua.setChecked(true);
                break;
            case "PA":
                cbPatrulla.setChecked(true);
                break;
            case "AM":
                cbAmbulanciaC.setChecked(true);
                break;
            case "VE":
                cbVerificador.setChecked(true);
                break;
            case "CO":
                cbContratista.setChecked(true);
                break;
            case "AB":
                cbAbogado.setChecked(true);
                break;
            case "TA":
                cbTaxi.setChecked(true);
                break;
            case "RE":
                cbRecuperador.setChecked(true);
                break;
            case "SF":
                cbFunerario.setChecked(true);
                break;
        }
    }

    private void initViews() {
        Animation animationIn = AnimationUtils.loadAnimation(Objects.requireNonNull(getActivity()).getApplicationContext(), R.anim.bounce_out);
        Animation animationOut = AnimationUtils.loadAnimation(Objects.requireNonNull(getActivity()).getApplicationContext(), R.anim.bounce_in);
        cbGrua.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckGrua.startAnimation(animationIn);
                imgCheckGrua.setVisibility(View.VISIBLE);
            } else {
                imgCheckGrua.startAnimation(animationOut);
                imgCheckGrua.setVisibility(View.GONE);
            }
            addService("GR", isChecked);
        });
        cbPatrulla.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckPatrulla.startAnimation(animationIn);
                imgCheckPatrulla.setVisibility(View.VISIBLE);
            } else {
                imgCheckPatrulla.startAnimation(animationOut);
                imgCheckPatrulla.setVisibility(View.GONE);
            }
            addService("PA", isChecked);
        });
        cbAmbulanciaC.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckAmbulancia.startAnimation(animationIn);
                imgCheckAmbulancia.setVisibility(View.VISIBLE);
            } else {
                imgCheckAmbulancia.startAnimation(animationOut);
                imgCheckAmbulancia.setVisibility(View.GONE);
            }
            addService("AM", isChecked);
        });
        cbVerificador.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckVerificador.startAnimation(animationIn);
                imgCheckVerificador.setVisibility(View.VISIBLE);
            } else {
                imgCheckVerificador.startAnimation(animationOut);
                imgCheckVerificador.setVisibility(View.GONE);
            }
            addService("VE", isChecked);
        });
        cbContratista.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckContratista.startAnimation(animationIn);
                imgCheckContratista.setVisibility(View.VISIBLE);
            } else {
                imgCheckContratista.startAnimation(animationOut);
                imgCheckContratista.setVisibility(View.GONE);
            }
            addService("CO", isChecked);
        });
        cbAbogado.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckAbogado.startAnimation(animationIn);
                imgCheckAbogado.setVisibility(View.VISIBLE);
            } else {
                imgCheckAbogado.startAnimation(animationOut);
                imgCheckAbogado.setVisibility(View.GONE);
            }
            addService("AB", isChecked);
        });
        cbTaxi.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckTaxi.startAnimation(animationIn);
                imgCheckTaxi.setVisibility(View.VISIBLE);
            } else {
                imgCheckTaxi.startAnimation(animationOut);
                imgCheckTaxi.setVisibility(View.GONE);
            }
            addService("TA", isChecked);
        });
        cbRecuperador.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckRecuperador.startAnimation(animationIn);
                imgCheckRecuperador.setVisibility(View.VISIBLE);
            } else {
                imgCheckRecuperador.startAnimation(animationOut);
                imgCheckRecuperador.setVisibility(View.GONE);
            }
            addService("RE", isChecked);
        });
        cbFunerario.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                imgCheckFunerario.startAnimation(animationIn);
                imgCheckFunerario.setVisibility(View.VISIBLE);
            } else {
                imgCheckFunerario.startAnimation(animationOut);
                imgCheckFunerario.setVisibility(View.GONE);
            }
            addService("SF", isChecked);
        });
    }

    @OnClick(R.id.btnAddServicios)
    public void addServices() {
        ((BaseActivity)getActivity()).getCompositeDisposable().add(serviciosCoberturaInteractor.addResumen(idAjuste,serviciosSeleccionados)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(getActivity(), ResourceUtils.getString("services_ok"));
                },throwable -> {
                    Log.e(TAG, "addServices: ", throwable);
                }));

    }

    private void addService(String clave, boolean addOrRemove) {
        if (addOrRemove) {
            serviciosSeleccionados.add(clave);
        } else {
            serviciosSeleccionados.remove(clave);
        }
    }


}
