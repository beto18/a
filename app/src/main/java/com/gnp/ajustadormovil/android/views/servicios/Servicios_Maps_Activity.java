package com.gnp.ajustadormovil.android.views.servicios;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.ServiciosCoberturaInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.models.ServicesFor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.Asegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.services.ServicioUbicacion;
import com.gnp.ajustadormovil.android.models.services.service.ServiceData;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.servicios.SelectAddressMapActivity;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.custom.widget.CustomFragmentMap;
import com.gnp.ajustadormovil.android.views.commons.events.OnRefreshListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

/**
 * Created by "" on 2/2/18.
 */

public class Servicios_Maps_Activity extends BaseFragment implements OnMapReadyCallback, View.OnClickListener, AdapterView.OnItemSelectedListener {

    FusedLocationProviderClient fusedLocation;
    @BindView(R.id.cbPermanente)
    CheckBox isPresente;
    @BindView(R.id.idA1)
    LinearLayout idA1;
    @BindView(R.id.idB1)
    LinearLayout idB1;
    @BindView(R.id.idC1)
    LinearLayout idC1;
    @BindView(R.id.idD1)
    LinearLayout idD1;
    @BindView(R.id.idE1)
    LinearLayout idE1;
    @BindView(R.id.idF1)
    LinearLayout idF1;
    @BindView(R.id.idG1)
    LinearLayout idG1;
    @BindView(R.id.idH1)
    LinearLayout idH1;
    @BindView(R.id.idI1)
    LinearLayout idI1;
    @BindView(R.id.idJ1)
    LinearLayout idJ1;
    @BindView(R.id.idK1)
    LinearLayout idK1;
    @BindView(R.id.idL1)
    LinearLayout idL1;
    @BindView(R.id.spServicio)
    Spinner spServicio;
    @BindView(R.id.idMotivoInvestigacion)
    TextView idMotivoInvestigacion;
    @BindView(R.id.spMotivoInvestigacion)
    Spinner spMotivoInvestigacion;
    @BindView(R.id.contentInvestigation)
    LinearLayout contentInvestigation;
    @BindView(R.id.idPersona)
    TextView idPersona;
    @BindView(R.id.spPersona)
    Spinner spPersona;
    @BindView(R.id.contentPersona)
    LinearLayout contentPersona;
    @BindView(R.id.idBien)
    TextView idBien;
    @BindView(R.id.contentIdBien)
    LinearLayout contentIdBien;
    @BindView(R.id.idMotivoSolicitud)
    TextView idMotivoSolicitud;
    @BindView(R.id.spMotivo)
    Spinner spMotivo;
    @BindView(R.id.btnGruaLiga)
    Button btnGruaLiga;
    @BindView(R.id.btnGuardarServicio)
    Button btnGuardarServicio;
    @BindView(R.id.spGrua)
    Spinner spGruas;
    @BindView(R.id.edObservaciones)
    EditText observaciones;
    @BindView(R.id.spComunicacion)
    Spinner spComunicacion;
    @BindView(R.id.contentService)
    LinearLayout contentServices;
    @BindView(R.id.tvAddressDescriptionOrigin)
    TextView tvAddressDescriptionOrigin;
    @BindView(R.id.tvAddressDescriptionDestiny)
    TextView tvAddressDescriptionDestiny;
    @BindView(R.id.spAbogado)
    Spinner spAbogado;
    @BindView(R.id.spVehiculo)
    Spinner spVehiculo;
    @BindView(R.id.idVehiculo)
    TextView idVehiculo;
    @BindView(R.id.cbPropiedad)
    CheckBox cbPropiedad;
    @BindView(R.id.cbLesioados)
    CheckBox cbLesioados;
    @BindView(R.id.cbVehiculoDetenido)
    CheckBox cbVehiculoDetenido;
    @BindView(R.id.cbConductorDetenido)
    CheckBox cbConductorDetenido;
    @BindView(R.id.spTipoGestion)
    Spinner spTipoGestion;
    @BindView(R.id.etMedico)
    EditText etMedico;
    @BindView(R.id.etFamiliar)
    EditText etFamiliar;
    @BindView(R.id.etDiagnostico)
    EditText etDiagnostico;
    @BindView(R.id.spTypeAmbulance)
    Spinner spTypeAmbulance;
    @BindView(R.id.cbAmbulanciaRes)
    CheckBox cbAmbulanciaRes;
    @BindView(R.id.contentAmbulance)
    LinearLayout contentAmbulance;
    @BindView(R.id.contentReasonS)
    LinearLayout contentReasonS;
    @BindView(R.id.spBienDañado)
    Spinner spBienDañado;
    @BindView(R.id.cvDestinySelect)
    CardView cvDestinySelect;
    @BindView(R.id.cvOriginSelect)
    CardView cvOriginSelect;
    @BindView(R.id.contentDestiny)
    LinearLayout contentDestiny;
    @BindView(R.id.contentNameInsure)
    LinearLayout contentNameInsure;
    @BindView(R.id.contentServiceFor)
    LinearLayout contentServiceFor;
    @BindView(R.id.contentNameVehicle)
    LinearLayout contentNameVehicle;
    @BindView(R.id.tvNameVehicle)
    TextView tvNameVehicle;
    @BindView(R.id.tvNameInsure)
    TextView tvNameInsure;
    private LatLng latLngDestiny, latLngOrigin;
    private static OnRefreshListener onRefreshListener;
    public static boolean boolACtivityServicios;
    public static int status = 1;
    private SiniestrosInteractor siniestrosInteractor;
    private OnToolbarListener listener;
    private Asegurado asegurado;
    private Responsabilidad responsabilidad;
    private List<ServicesFor> listServicioPara = new ArrayList<>();
    private String conductor = "";
    private String vehiculo = "";
    private TprInteractor tprService;

    public static void setRefreshListener(OnRefreshListener l) {
        onRefreshListener = l;
    }

    private GoogleMap mMap;
    private Location mLastLocation;
    public static final String TAG = "TAG";
    private List<CatalogoClave> gruas = new ArrayList<>();
    private List<String> nombreGruas = new ArrayList<>();
    private List<TerceroAuto> terceros = new ArrayList<>();
    private List<String> nombreVehiculoTerceros = new ArrayList<>();

    private List<CatalogoClave> lawyersList = new ArrayList<>();
    private List<String> lawyersName = new ArrayList<>();
    private CatalogoClave lawyerSelect = new CatalogoClave();

    private List<CatalogoClave> policesList = new ArrayList<>();
    private List<String> policesName = new ArrayList<>();
    private CatalogoClave policesSelect = new CatalogoClave();

    private List<CatalogoClave> communicationList = new ArrayList<>();
    private List<String> communicationName = new ArrayList<>();
    private CatalogoClave communicationSelect = new CatalogoClave();

    private List<CatalogoClave> managementList = new ArrayList<>();
    private List<String> managementName = new ArrayList<>();
    private CatalogoClave managementSelect = new CatalogoClave();

    private List<CatalogoClave> typeAmbutList = new ArrayList<>();
    private List<String> typeAmbuName = new ArrayList<>();
    private CatalogoClave typeAmbuSelect = new CatalogoClave();

    private List<CatalogoClave> reasonList = new ArrayList<>();
    private List<String> reasonName = new ArrayList<>();
    private CatalogoClave reasonSelect = new CatalogoClave();

    private List<Ocupante> occupantList = new ArrayList<>();
    private List<String> occupantName = new ArrayList<>();
    private Ocupante occupantSelect = new Ocupante();
    private List<CatalogoClave> deadList = new ArrayList<>();
    private List<CatalogoClave> injureList = new ArrayList<>();
    private CatalogoClave deadSelect = new CatalogoClave();

    private List<TerceroNoAuto> noAutoList = new ArrayList<>();
    private List<String> noAutoName = new ArrayList<>();
    private TerceroNoAuto noAutoSelect = new TerceroNoAuto();

    private List<ServicesFor> itemListtConductores = new ArrayList();
    private List<ServicesFor> itemListtVehiculos = new ArrayList();
    private String stringservicesFor =Constants.INSURE;

    private long ocupante;

    @BindView(R.id.fechaHora)
    TextView mfechaHora;

    CustomFragmentMap mGoogleMap;
    private Activity activity;

    private static volatile Servicios_Maps_Activity sSoleInstance = new Servicios_Maps_Activity();



    public static Servicios_Maps_Activity getInstance() {
        return sSoleInstance;
    }

    public static Servicios_Maps_Activity newInstance(Bundle args) {
        sSoleInstance = new Servicios_Maps_Activity();
        sSoleInstance.setArguments(args);
        return sSoleInstance;
    }

    static String id;
    String idAjuste;

    public static String newService(String service) {
        id = service;
        return id;
    }

    ArrayList<String> servicesFor = new ArrayList<>();
    Ajuste ajuste;
    private TextView tipoUbicacion, titleUbi;

    private CompositeDisposable compositeDisposable;
    private ServiciosCoberturaInteractor serviciosCoberturaInteractor;
    private AseguradoInteractor aseguradoInteractor;
    private TerceroAutoInteractor terceroAutoService;
    private CatInteractor catInteractor;
    private TerceroNoAutoInteractor tercerosNoAutoService;
    private ResponsabilidadInteractor responsabilidadInteractor;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            listener = (OnToolbarListener) context;
        } else {
            Log.d(TAG, "onAttach: I need instace of Toolbar interface");
        }

    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.activity_servicios_maps, container, false);
        ButterKnife.bind(this, mView);

        compositeDisposable = ((BaseActivity) getActivity()).getCompositeDisposable();
        serviciosCoberturaInteractor = OrquestadorInteractorFactory.getServiciosCoberturaInteractor();
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        tercerosNoAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        responsabilidadInteractor = OrquestadorInteractorFactory.getResponsabilidadesService();
        tprService = CatalogoInteractorsFactory.getTprService();

        activity = getActivity();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        if (activity.getIntent() != null) {
            idAjuste = activity.getIntent().getStringExtra("idAjuste");
        }


        ajuste = new Gson().fromJson(AppPreferences.getString(AppPreferences.JSON_AJUSTE_DETALLE), Ajuste.class);
        tipoUbicacion = mView.findViewById(R.id.tipoUbicacion);
        titleUbi = mView.findViewById(R.id.textview26);
        tipoUbicacion.setVisibility(View.GONE);
        titleUbi.setVisibility(View.GONE);

        fusedLocation = LocationServices.getFusedLocationProviderClient(activity);

        setEvents();
        getDatosAseguradoTerceros();
        buildLayout();

        Calendar calendar = Calendar.getInstance(Locale.forLanguageTag("es-MX"));
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        btnGruaLiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://docs.google.com/forms/d/e/1FAIpQLSf_QtXEfiXJOT9vUs3X1WCsdmcNHVndv5_rk82rHIcPVWTU1w/viewform?c=0&w=1"));
                startActivity(i);
            }
        });

        mfechaHora.setText(date + "-" + Utils.twoDigits(month + 1) + "-" + year + " "
                + hour + ":" + minute + "hrs");

        mGoogleMap = (CustomFragmentMap) getChildFragmentManager().findFragmentById(R.id.map);

        mGoogleMap.getMapAsync(this);

        mGoogleMap.setOnTouchListener(() -> contentServices.requestDisallowInterceptTouchEvent(true));

        setOnclickCardMap();

        return mView;
    }

    public void buildLayout() {
        if (id!=null && !id.equals("")) {
            switch (id) {
                case Constants.CRANE:
                    setVisibleLayoutGruas();
                    getCrane();
                    //getAll();
                    break;
                case Constants.POLICE:
                    setVisibleLayoutPolice();
                    getPolices();
                    break;
                case Constants.AMBULANCE:
                    setVisibleAmbulance();
                    getInjured();
                    getAmbulances();
                    break;
                case Constants.RECUPERATOR:
                    if (ajuste.getSiniestro().getTipoSiniestro().getClave().equals("RT")) {
                        setVisibleRecuperate();
                    } else {
                        DialogUtils.createAlert(activity, ResourceUtils.getString(R.string.title_only_rob_alert),
                                ResourceUtils.getString(R.string.message_only_rob_alert),
                                ResourceUtils.getString(R.string.btn_continuar_text),
                                null, (dialog, which) -> {
                                    activity.onBackPressed();
                                    dialog.dismiss();
                                }, null);
                    }
                    break;
                case Constants.FUNERARY:
                    setVisibleFunerary();
                    getInsure();
                    break;
                case Constants.TAXI:
                    setVisibleTaxi();
                    break;
                case Constants.CHECKER:
                    setVisibleChecker();
                    getReasonInvestigation();
                    break;
                case Constants.LAWYER:
                    setVisibleLayoutLawyer();
                    getLawyers();
                    getCommunication();
                    getTypeManagement();
                    break;
                case Constants.CONTRACTOR:
                    setVisibleContractor();
                    getThirdNoCar();
                    break;
                default:
                    break;
            }
        }
    }

    private void getInjured() {
        compositeDisposable.add(aseguradoInteractor.get(idAjuste)
                .subscribe(terceroAutosRes -> {
                    if (terceroAutosRes != null && terceroAutosRes.getOcupantes() != null) {
                        if (terceroAutosRes.getOcupantes().size() > 0) {
                            occupantList = terceroAutosRes.getOcupantes();
                            for (Ocupante ocupante : occupantList) {
                                if (ocupante.getRequiereAmbulancia()) {
                                    injureList.add(
                                            new CatalogoClave(String.valueOf(ocupante.getId()), ocupante.getNombreCompleto())
                                    );
                                }
                            }
                        }
                        if (terceroAutosRes.getConductor().getCondicion() != null) {
                            injureList.add(new CatalogoClave("AA", terceroAutosRes.getConductor().getNombreCompleto()));
                        }

                        if (injureList.isEmpty()) {
                            DialogUtils.createAlert(activity, ResourceUtils.getString(R.string.text_title_alert),
                                    "No hay personas lesionadas",
                                    ResourceUtils.getString(R.string.btn_continuar_text), null,
                                    (dialog, which) -> {
                                        activity.onBackPressed();
                                        dialog.dismiss();
                                    }, null);
                        } else {

                            AdapterSpinnerGeneral<CatalogoClave> adapterReason = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item,injureList, CatalogoClave::getNombre);
                            adapterReason.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPersona.setAdapter(adapterReason);
                            contentPersona.setVisibility(View.VISIBLE);
                            contentNameInsure.setVisibility(View.GONE);
                        }
                    }

                }, throwable -> {
                        Log.e(TAG, "Error al obtener Asegurado ", throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));

        spPersona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadSelect = injureList.get(position);
                if (!injureList.get(position).getClave().equals("AA")) {
                    ocupante = Integer.parseInt(injureList.get(position).getClave());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void getInsure() {
        compositeDisposable.add(aseguradoInteractor.get(idAjuste)
                .subscribe(asegurado -> {
                    if (asegurado != null) {
                        if (asegurado.getOcupantes().size() > 0) {
                            occupantList = asegurado.getOcupantes();
                            for (Ocupante ocupante : occupantList) {
                                if (ocupante.getCondicion().equals(CondicionPersona.FALLECIDO)) {
                                    deadList.add(new
                                            CatalogoClave(String.valueOf(ocupante.getId()), ocupante.getNombreCompleto()));
                                } else if (ocupante.getCondicion().equals(CondicionPersona.LESIONADO)) {
                                    injureList.add(
                                            new CatalogoClave(String.valueOf(ocupante.getId()), ocupante.getNombreCompleto())
                                    );
                                }
                            }
                        }
                        if (asegurado.getConductor().getCondicion() != null && asegurado.getConductor().getCondicion().equals(CondicionPersona.FALLECIDO)) {
                            deadList.add(new CatalogoClave("AA", asegurado.getConductor().getNombreCompleto()));
                        }
                        if (deadList.isEmpty()) {
                            DialogUtils.createAlert(activity, getString(R.string.text_title_alert),
                                    ResourceUtils.getString(R.string.text_message_not_dead),
                                    ResourceUtils.getString(R.string.btn_continuar_text), null,
                                    (dialog, which) -> {
                                        activity.onBackPressed();
                                        dialog.dismiss();
                                    }, null);
                        } else {
                            AdapterSpinnerGeneral<CatalogoClave> adapterReason = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, deadList, CatalogoClave::getNombre);
                            adapterReason.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPersona.setAdapter(adapterReason);
                            contentPersona.setVisibility(View.VISIBLE);
                            contentNameInsure.setVisibility(View.GONE);
                        }
                    }

                }, throwable -> {
                        Log.e(TAG, "Error al obtener Asegurado ", throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);

                }));

        spPersona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadSelect = deadList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void setOnclickCardMap() {
        cvOriginSelect.setOnClickListener(this);
        cvDestinySelect.setOnClickListener(this);
    }

    private void setVisibleTaxi() {
        contentPersona.setVisibility(View.GONE);
        idB1.setVisibility(View.GONE);
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    private void setVisibleFunerary() {
        contentPersona.setVisibility(View.GONE);
        setDisableFunctionMap();
        idB1.setVisibility(View.GONE);
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    private void setVisibleContractor() {
        contentInvestigation.setVisibility(View.GONE);
        contentPersona.setVisibility(View.GONE);
        contentIdBien.setVisibility(View.VISIBLE);
        setDisableFunctionMap();
        idB1.setVisibility(View.GONE);
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    @SuppressLint("SetTextI18n")
    private void setVisibleRecuperate() {
        Asegurado asegurado = null;
        try {
            asegurado = RXUtils.sync(aseguradoInteractor.get(idAjuste));
        } catch (Exception e) {
            e.printStackTrace();
        }
        contentNameVehicle.setVisibility(View.VISIBLE);
        if (ajuste.getSiniestro().getVehiculo() != null) {
            tvNameVehicle.setText(asegurado.getVehiculo().getNombre());
        }
        contentPersona.setVisibility(View.GONE);
        idB1.setVisibility(View.GONE);
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    private void setVisibleAmbulance() {
        contentPersona.setVisibility(View.GONE);
        contentAmbulance.setVisibility(View.VISIBLE);
        idB1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    private void setVisibleLayoutLawyer() {
        contentPersona.setVisibility(View.GONE);
        setDisableFunctionMap();
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
        idB1.setVisibility(View.GONE);
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        if (ajuste.getSiniestro().getUbicacion() != null) {
            if (ajuste.getSiniestro().getUbicacion().getTipoUbicacion() != null &&
                    ajuste.getSiniestro().getUbicacion().getTipoUbicacion().getNombre() != null) {
                tipoUbicacion.setVisibility(View.VISIBLE);
                titleUbi.setVisibility(View.VISIBLE);
            }
        }
        tipoUbicacion.setText(AppPreferences.getString(AppPreferences.TIPO_UBICACION));

    }

    private void setVisibleChecker() {
        contentReasonS.setVisibility(View.GONE);
        idA1.setVisibility(View.GONE);
        setDisableFunctionMap();
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    private void setVisibleLayoutPolice() {
        contentInvestigation.setVisibility(View.GONE);
        contentPersona.setVisibility(View.GONE);
        setDisableFunctionMap();
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        idJ1.setVisibility(View.GONE);
        idK1.setVisibility(View.GONE);
        idL1.setVisibility(View.GONE);
    }

    private void setDisableFunctionMap() {
        cvDestinySelect.setVisibility(View.GONE);
        contentDestiny.setVisibility(View.GONE);
    }

    private void setVisibleLayoutGruas() {
        contentPersona.setVisibility(View.GONE);
        contentIdBien.setVisibility(View.GONE);
        contentNameVehicle.setVisibility(View.VISIBLE);
        if (ajuste.getSiniestro().getVehiculo() != null) {
            String vehiculo = ajuste.getSiniestro().getVehiculo().getArmadora() + " " + ajuste
                    .getSiniestro().getVehiculo().getCarroceria().getNombre() + " " + ajuste.getSiniestro().getVehiculo()
                    .getModelo();
            //tvNameVehicle.setText(vehiculo);
            tvNameVehicle.setVisibility(View.VISIBLE);
        }
        idB1.setVisibility(View.GONE);
        idD1.setVisibility(View.GONE);
        idE1.setVisibility(View.GONE);
        idF1.setVisibility(View.GONE);
        idG1.setVisibility(View.GONE);
        idH1.setVisibility(View.GONE);
        idI1.setVisibility(View.GONE);
        if (ajuste.getSiniestro().getUbicacion() != null) {
            if (ajuste.getSiniestro().getUbicacion().getTipoUbicacion() != null &&
                    ajuste.getSiniestro().getUbicacion().getTipoUbicacion().getNombre() != null) {
                tipoUbicacion.setVisibility(View.VISIBLE);
                titleUbi.setVisibility(View.VISIBLE);
                //tipoUbicacion.setText("Tipo de ubicación:" + "\n" +ajuste.getSiniestro().getUbicacion().getTipoUbicacion().getName());
            }
        }
        tipoUbicacion.setText(AppPreferences.getString(AppPreferences.TIPO_UBICACION));
    }

    private void getReasonInvestigation() {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(siniestrosInteractor.getMotivosInvestigacion()
                .subscribe(reasons -> {
                            reasonList = reasons;
                    loadSpinnerReasons();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar motivos de investigacion", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
        spMotivoInvestigacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reasonSelect = reasonList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadSpinnerReasons() {
        AdapterSpinnerGeneral<CatalogoClave> adapterReason = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, reasonList, CatalogoClave::getNombre);
        adapterReason.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotivoInvestigacion.setAdapter(adapterReason);
    }

    private void getAmbulances() {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(siniestrosInteractor.getTiposAmbulancia()
                .subscribe(ambulances -> {
                            typeAmbutList = ambulances;
                    loadSpinnerAmbulances();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar tipos de ambulancia", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
        spTypeAmbulance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeAmbuSelect = typeAmbutList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadSpinnerAmbulances() {
        AdapterSpinnerGeneral<CatalogoClave> adapterAmbulance = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, typeAmbutList, CatalogoClave::getNombre);
        adapterAmbulance.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTypeAmbulance.setAdapter(adapterAmbulance);
    }

    private void getTypeManagement() {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(siniestrosInteractor.getGestionesAbogado()
                .subscribe(managements -> {
                            managementList = managements;
                    loadSpinnerManagements();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar gestiones de abogado", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
        spTipoGestion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                managementSelect = managementList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void loadSpinnerManagements() {
        AdapterSpinnerGeneral<CatalogoClave> adapterManagement = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, managementList, CatalogoClave::getNombre);
        adapterManagement.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoGestion.setAdapter(adapterManagement);
    }

    private void getCommunication() {
        ((BaseActivity) getActivity()).getCompositeDisposable().add(siniestrosInteractor.getComunicacionesAbogado()
                .subscribe(communications -> {
                            communicationList = communications;
                    loadSpinnerCommunications();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar comunicaciones de abogado", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));

        spComunicacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                communicationSelect = communicationList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadSpinnerCommunications() {
        AdapterSpinnerGeneral<CatalogoClave> adapterComm = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, communicationList, CatalogoClave::getNombre);
        adapterComm.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spComunicacion.setAdapter(adapterComm);
    }


    private void getDatosAseguradoTerceros(){
        compositeDisposable.add(getAsegurado().subscribe(() ->{
            compositeDisposable.add(Completable.mergeArrayDelayError(
                    getVersionVehiculoAsegurado(),
                    getResponsabilidad(),
                    getTerceros())
                    .subscribe(this::setSpinners,
                            throwable -> {
                                new GeneralSnackBar().largeSnackError(activity, throwable);
                            }));

        },throwable -> {
            Log.e(TAG,"Error al obtner datos del asegurado y terceros ", throwable);
        } ));
    }

    private Completable getResponsabilidad(){
        return Completable.create(e -> {
           compositeDisposable.add(responsabilidadInteractor.getResponsabilidad(idAjuste)
                   .subscribe(responsabilidad -> {
                       this.responsabilidad = responsabilidad;
                       e.onComplete();
                   }, throwable -> {
                       Log.e(TAG,"Error al obtner la responsabilida", throwable);
                       if (!(throwable instanceof ResourceNotFoundException)){
                           e.tryOnError(throwable);
                           new GeneralSnackBar().largeSnackError(activity,throwable);
                       }else {
                           e.onComplete();
                       }
                   }));
        });
    }

    private Completable getAsegurado(){
       return Completable.create(e -> {
           compositeDisposable.add(aseguradoInteractor.get(idAjuste)
                   .subscribe(asegurado -> {
                       this.asegurado = asegurado;
                       if (asegurado != null) {
                           if (asegurado.getConductor() != null) {
                               conductor = asegurado.getConductor().getNombreCompleto();
                           }
                       }

                       e.onComplete();
                   }, throwable -> {
                       Log.e(TAG,"Error al obtner datos del asegurado", throwable);
                       e.tryOnError(throwable);
                   }));

       });
    }

    private Completable getVersionVehiculoAsegurado() {
        return Completable.create(e -> {

            compositeDisposable.add(tprService.getVersion(asegurado.getVehiculo().getTipoVehiculo()
                    , asegurado.getVehiculo().getArmadora(), asegurado.getVehiculo().getCarroceria()
                    , Integer.valueOf(asegurado.getVehiculo().getModelo()), asegurado.getVehiculo().getVersion())
                    .subscribe(versionVehiculo -> {
                        vehiculo = versionVehiculo.getNombre();
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Ocurrio un error al obtener la version", throwable);
                    }));

        });

    }

    private Completable getTerceros() {
        return Completable.create(e -> {
            compositeDisposable.add(terceroAutoService.getAll(idAjuste)
                    .subscribe(terceroAutos -> {
                        terceros = terceroAutos;
                        if (terceros != null) {
                            if (!terceros.isEmpty()) {
                                for (TerceroAuto tercero : terceros){
                                    ServicesFor servicesFor = new ServicesFor();
                                    servicesFor.setId(tercero.getId());
                                    if (tercero.getConductor() != null) {
                                        servicesFor.setConductor(tercero.getConductor().getNombreCompleto());
                                    }
                                    if (tercero.getVehiculo() != null ) {
                                        servicesFor.setVehiculo(tercero.getDescripcion());
                                    }
                                    servicesFor.setServicioPara(Constants.THIRD);
                                    listServicioPara.add(servicesFor);
                                }

                            }
                        } else {
                            new GeneralSnackBar().largeSnackOk(activity, "No hay terceros");
                        }
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al obtener terceros autos", throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                    }));

        });

    }

    private void cleanListTercerosAutos(List<ServicesFor> servicesForList) {
        if (!servicesForList.isEmpty()){
        CollectionsUtils.removeIf(servicesForList, removed-> removed.getConductor()== null ||
                removed.getConductor().isEmpty() || removed.getVehiculo() == null
                || removed.getVehiculo().isEmpty());
        }
    }


    public void getThirdNoCar() {
        compositeDisposable.add(tercerosNoAutoService.getAll(idAjuste)
                .subscribe(noAutoResponses -> {
                    noAutoList = noAutoResponses;
                    if (!noAutoList.isEmpty()) {
                        AdapterSpinnerGeneral<TerceroNoAuto> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, noAutoList, TerceroNoAuto::getAreaDanada);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spBienDañado.setAdapter(adapter);
                    } else {
                        DialogUtils.createAlert(activity, ResourceUtils.getString(R.string.recycler_not_third),
                                ResourceUtils.getString(R.string.message_dialog_no_third),
                                ResourceUtils.getString(R.string.btn_continuar_text), "", (dialog, which) -> {
                                    boolACtivityServicios = true;
                                    activity.onBackPressed();
                                    dialog.dismiss();
                                }, null);
                    }

                }, throwable -> {
                    activity.onBackPressed();
                    Log.e(TAG, "Error al descargar no autos", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);

                }));

        spBienDañado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                noAutoSelect = noAutoList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setEvents() {
        spServicio.setOnItemSelectedListener(this);
    }

    private void setSpinners() {
        if (id != null && !id.equals(Constants.CONTRACTOR)) {
            servicesFor.add(Constants.INSURE);
            // tvNameVehicle.setText(vehiculo);
            stringservicesFor = Constants.INSURE;
        }

        switch (id) {
            case Constants.LAWYER:
            case Constants.POLICE:
            case Constants.AMBULANCE:
            case Constants.CHECKER:
            case Constants.FUNERARY:
            case Constants.RECUPERATOR:
            case Constants.TAXI:

                tvNameInsure.setText(conductor);
                tvNameVehicle.setText(vehiculo);

                break;
            case Constants.CRANE:
                stringservicesFor = null;
                if (responsabilidad != null && responsabilidad.getAseguradoResponsable()) {
                    contentServiceFor.setVisibility(View.VISIBLE);
                    contentNameInsure.setVisibility(View.GONE);
                    tvNameVehicle.setText("-----");
                    ServicesFor servicesFor = new ServicesFor();
                    servicesFor.setId(asegurado.getConductor().getId() == null ? Integer.valueOf(1) : asegurado.getConductor().getId());
                    servicesFor.setConductor(conductor);
                    servicesFor.setVehiculo(vehiculo);
                    servicesFor.setServicioPara(Constants.INSURE);
                    listServicioPara.add(servicesFor);
                    cleanListTercerosAutos(listServicioPara);
                    AdapterSpinnerGeneral<ServicesFor> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item,listServicioPara, ServicesFor::getConductor);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spServicio.setAdapter(adapter);



                } else {
                    stringservicesFor = Constants.INSURE;
                    tvNameInsure.setText(conductor);
                    tvNameVehicle.setText(vehiculo);
                    contentServiceFor.setVisibility(View.GONE);
                }


                break;
            default:
                servicesFor.add(Constants.THIRD);
                stringservicesFor = Constants.THIRD;
                break;
        }

    }

    private void getCrane() {
        compositeDisposable.add(siniestrosInteractor.getTiposGrua()
                .subscribe(catalogos -> {
                    gruas = catalogos;
                    if (gruas != null) {
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, gruas, CatalogoClave::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spGruas.setAdapter(adapter);
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar tipos de grua", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));


    }

    private void getPolices() {
        compositeDisposable.add(siniestrosInteractor.getTiposPatrulla()
                .subscribe(polices -> {
                            policesList = polices;
                    loadSpinnerPolices();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar tipos de patrulla", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));
        spMotivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                policesSelect = policesList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadSpinnerPolices() {
        AdapterSpinnerGeneral<CatalogoClave> adapterPolice = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, policesList,CatalogoClave::getNombre);
        adapterPolice.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMotivo.setAdapter(adapterPolice);
    }

    private void getLawyers() {
        compositeDisposable.add(siniestrosInteractor.getTiposAbogado()
                .subscribe(lawyers -> {
                            lawyersList = lawyers;
                    loadSpinnerLawers();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar tipos de abogado", throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                        }
                ));

        spAbogado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                lawyerSelect = lawyersList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void loadSpinnerLawers() {
        AdapterSpinnerGeneral<CatalogoClave> adapterLawyer = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, lawyersList,CatalogoClave::getNombre );
        adapterLawyer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAbogado.setAdapter(adapterLawyer);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_LOCATION_PERMISSION)) {
                mMap.setMyLocationEnabled(true);
                fusedLocation.getLastLocation().addOnSuccessListener(location -> {
                    mLastLocation = location;
                    if (location != null) {
                        CameraUpdate camUpd1 = CameraUpdateFactory
                                .newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13);
                        mMap.moveCamera(camUpd1);

                    }
                });

            } else {
                RequestPermissions.requestPermission(RequestPermissions.REQUEST_LOCATION_PERMISSION,
                        activity);
            }
        } else {
            mMap.setMyLocationEnabled(true);
            fusedLocation.getLastLocation().addOnSuccessListener(location -> mLastLocation = location);
        }
    }

    private void getList(String indicator) {
        switch (indicator) {
            case Constants.INSURE:
                spVehiculo.setVisibility(View.GONE);
                idVehiculo.setVisibility(View.GONE);
                break;
            case Constants.THIRD:
                spVehiculo.setVisibility(View.VISIBLE);
                idVehiculo.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_MAP_ORIGIN && resultCode == Activity.RESULT_OK) {
            latLngOrigin = new LatLng(data.getExtras().getDouble(Constants.LAT),
                    data.getExtras().getDouble(Constants.LNG));
            putMapPin();
            getLocation(latLngOrigin, true);

        } else if (requestCode == Constants.REQUEST_CODE_MAP_DESTINY && resultCode == Activity.RESULT_OK) {
            latLngDestiny = new LatLng(data.getExtras().getDouble(Constants.LAT),
                    data.getExtras().getDouble(Constants.LNG));
            putMapPin();
            getLocation(latLngDestiny, false);
        } else {
            Log.d("", "Nothing select");
        }
    }

    public void getLocation(LatLng latLng, boolean origin) {
        if (latLng != null) {
            compositeDisposable.add(catInteractor.getLocationAddress(latLng.latitude, latLng.longitude)
                    .subscribe(addressWS -> {
                        if (origin) {
                            tvAddressDescriptionOrigin.setText(addressWS.getDireccion() != null ?
                                    addressWS.getDireccion() : "-");
                        } else {
                            tvAddressDescriptionDestiny.setText(addressWS.getDireccion() != null ?
                                    addressWS.getDireccion() : "-");
                        }

                    }, throwable -> {
                        Log.e(TAG, "Error al obtener la ubicacion", throwable);
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                    }));
        }
    }

    private void putMapPin() {
        mMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if (latLngOrigin != null) {
            mMap.addMarker(new MarkerOptions().position(latLngOrigin).icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            builder.include(latLngOrigin);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new
                    LatLng(latLngOrigin.latitude, latLngOrigin.longitude), 15));
        }
        if (latLngDestiny != null) {
            mMap.addMarker(new MarkerOptions().position(latLngDestiny).icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            builder.include(latLngDestiny);
            animateToPins(builder);
        }
    }

    private void animateToPins(LatLngBounds.Builder builder) {
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 70);
        mMap.moveCamera(cu);
        mMap.animateCamera(cu);
    }

    public boolean checkDestiny() {
        if (latLngDestiny == null) {
            new GeneralSnackBar().shortSnack(activity, "No hay un destino");
            boolACtivityServicios = true;
            return false;
        } else return true;
    }

    public boolean checkOrigin() {
        if (latLngOrigin == null) {
            new GeneralSnackBar().shortSnack(activity, "No hay un Origen");
            boolACtivityServicios = true;
            return false;
        } else return true;
    }

    public boolean checkObservations() {
        if (observaciones.getText().toString().equals("")) {
            new GeneralSnackBar().shortSnack(activity, "No hay observaciones");
            boolACtivityServicios = true;
            return false;
        } else return true;
    }

    @OnClick(R.id.btnGuardarServicio)
    public void saveServicio() {
        switch (id) {
            case Constants.CRANE:
                if (latLngDestiny == null) {
                    new GeneralSnackBar().shortSnack(activity, "No hay un destino");
                    boolACtivityServicios = true;
                } else if (observaciones.getText().toString().equals("")) {
                    new GeneralSnackBar().shortSnack(activity, "No hay observaciones");
                    boolACtivityServicios = true;
                } else if (latLngOrigin == null) {
                    new GeneralSnackBar().shortSnack(activity, "No hay un origen");
                    boolACtivityServicios = true;
                } else if (listServicioPara.isEmpty() && stringservicesFor.equals(Constants.THIRD)) {
                    new GeneralSnackBar().shortSnack(activity, "No hay terceros");
                    boolACtivityServicios = true;
                } else {
                    makeRequestCrane();
                }
                break;
            case Constants.LAWYER:
                if (checkOrigin() && checkObservations()) makeRequestLawyer();
                break;
            case Constants.POLICE:
                if (checkOrigin() && checkObservations()) makeRequestPolice();
                break;
            case Constants.AMBULANCE:
                if (latLngOrigin == null) {
                    new GeneralSnackBar().shortSnack(activity, "No hay un origen");
                    boolACtivityServicios = true;
                } else if (latLngDestiny == null) {
                    new GeneralSnackBar().shortSnack(activity, "No hay un destino");
                    boolACtivityServicios = true;
                } else if (etMedico.getText().toString().equals("")) {
                    new GeneralSnackBar().shortSnack(activity, "No hay Médico");
                    boolACtivityServicios = true;
                } else if (etDiagnostico.getText().toString().equals("")) {
                    new GeneralSnackBar().shortSnack(activity, "No hay Diagnóstico");
                    boolACtivityServicios = true;
                } else if (observaciones.getText().toString().equals("")) {
                    new GeneralSnackBar().shortSnack(activity, "No hay observaciones");
                    boolACtivityServicios = true;
                } else {
                    makeRequestAmbulance();
                }
                break;
            case Constants.CHECKER:
                if (checkOrigin() && checkObservations()) makeRequestChecker();

                break;
            case Constants.RECUPERATOR:
                if (checkOrigin() && checkObservations() && checkDestiny()) sendRecuperator();

                break;
            case Constants.FUNERARY:
                if (checkOrigin() && checkObservations()) sendFunerary();

                break;
            case Constants.CONTRACTOR:
                if (checkOrigin() && checkObservations()) sendContractor();

                break;
            case Constants.TAXI:
                if (checkOrigin() && checkObservations() && checkDestiny()) sendTaxi();
                break;
        }
    }




    public void sendTaxi() {
        ServiceData serviceData = new ServiceData(new ServicioUbicacion(latLngDestiny.latitude, latLngDestiny.longitude)
                , null, observaciones.getText().toString(), new ServicioUbicacion(latLngOrigin.latitude,
                latLngOrigin.longitude), stringservicesFor, 0L,
                null, null, null, null, null,
                null, null, null, null, null,
                null, null, null, null,
                null, null);
        compositeDisposable.add(serviciosCoberturaInteractor.sendTaxi(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();

                }, throwable -> {
                    boolACtivityServicios = true;
                    Log.e(TAG, "Error al enviar datos de taxi:" + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);

                }));

    }

    public void sendContractor() {
        ServiceData serviceData = new ServiceData(null, null,
                observaciones.getText().toString(), new ServicioUbicacion(latLngOrigin.latitude,
                latLngOrigin.longitude), stringservicesFor, noAutoSelect.getId(),
                null, null, null, null, null,
                null, null, null, null, null,
                null, null, null, null,
                null, null);

        compositeDisposable.add(serviciosCoberturaInteractor.sendContractor(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();

                }, throwable -> {
                    boolACtivityServicios = true;

                    Log.e(TAG, "Error al enviar datos de contratista:" + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);

                }));
    }

    public void sendFunerary() {
        Long occupant = null;
        if (!deadSelect.getClave().equals("AA")) {
            occupant = Long.valueOf(deadSelect.getClave());
        }
        ServiceData serviceData = new ServiceData(null, null,
                observaciones.getText().toString(), new ServicioUbicacion(latLngOrigin.latitude,
                latLngOrigin.longitude), stringservicesFor, 0L,
                null, null, null, null, null,
                occupant, null, null, null, null,
                null, null, null, null,
                null, null);


        compositeDisposable.add(serviciosCoberturaInteractor.sendFunerary(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();

                }, throwable -> {
                    boolACtivityServicios = true;
                    Log.e(TAG, "Error al enviar datos de funeraria: " + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));

    }

    public void sendRecuperator() {
        ServiceData serviceData = new ServiceData(new ServicioUbicacion(latLngDestiny.latitude
                , latLngDestiny.longitude), null, observaciones.getText().toString()
                , new ServicioUbicacion(latLngOrigin.latitude, latLngOrigin.longitude)
                , stringservicesFor, 0L, null, null,
                null, null, null, null, null,
                null, null, null, null,
                null, null, null, null, null);

        compositeDisposable.add(serviciosCoberturaInteractor.sendRecuperator(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();
                }, throwable -> {
                    boolACtivityServicios = true;
                    Log.e(TAG, "Error al enviar datos del servicio recuperador: " + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));

    }

    public void makeRequestChecker() {
        ServiceData serviceData = new ServiceData(
                null,
                null, observaciones.getText().toString(),
                new ServicioUbicacion(latLngOrigin.latitude, latLngOrigin.longitude),
                stringservicesFor, 0L, null, null,
                null, null, null, null,
                null, null, null,
                null, null, null,
                null, null, null, reasonSelect.getClave());

        compositeDisposable.add(serviciosCoberturaInteractor.sendChecker(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();


                }, throwable -> {
                    Log.e(TAG, "Error al enviar datos de verificador: " + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));

    }

    public void makeRequestAmbulance() {
        //int tercero =
        ServiceData serviceData = new ServiceData(
                new ServicioUbicacion(latLngDestiny.latitude, latLngDestiny.longitude),
                null, observaciones.getText().toString(),
                new ServicioUbicacion(latLngOrigin.latitude, latLngOrigin.longitude),
                stringservicesFor, 0L, null, null,
                null, null, null, ocupante == 0 ? null : ocupante,
                null, null, null,
                policesSelect.getClave(), etFamiliar.getText().toString(), cbAmbulanciaRes.isChecked(),
                etDiagnostico.getText().toString(), etMedico.getText().toString(), typeAmbuSelect.getClave(),
                null);

        compositeDisposable.add(serviciosCoberturaInteractor.sendAmbulance(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();

                }, throwable -> {
                    Log.e(TAG, "Error al enviar datos de ambulancia: " + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));

    }

    public void makeRequestPolice() {
        ServiceData serviceData = new ServiceData(null, null, observaciones.getText().toString(),
                new ServicioUbicacion(latLngOrigin.latitude, latLngOrigin.longitude),
                stringservicesFor, 0L, null, null,
                null, null, null, null,
                null, null, null,
                policesSelect.getClave(), null, null, null,
                null, null, null);

        compositeDisposable.add(serviciosCoberturaInteractor.sendPolices(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();

                }, throwable -> {
                    Log.e(TAG, "Error al enviar datos de policia: " + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));


    }

    public void makeRequestLawyer() {
        ServiceData serviceData = new ServiceData(null, null, observaciones.getText().toString(),
                new ServicioUbicacion(latLngOrigin.latitude, latLngOrigin.longitude),
                stringservicesFor, 0L, null, communicationSelect.getClave(),
                cbConductorDetenido.isChecked(), cbPropiedad.isChecked(), cbLesioados.isChecked(), 0L,
                lawyerSelect.getClave(), managementSelect.getClave(), cbVehiculoDetenido.isChecked(),
                null, null, null, null,
                null, null, null);

        compositeDisposable.add(serviciosCoberturaInteractor.sendLawyer(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();
                }, throwable -> {
                    Log.e(TAG, "Error al enviar datos de abogado:" + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);


                }));

    }


    public void makeRequestCrane() {
        long third = 0;
        if (spServicio.getSelectedItem() != null && ((ServicesFor)spServicio.getSelectedItem()).getServicioPara().equals(Constants.THIRD)) {
            third = ((ServicesFor)spServicio.getSelectedItem()).getId();
        }
        if (spServicio.getSelectedItem() != null && ((ServicesFor)spServicio.getSelectedItem()).getServicioPara() != null){
            stringservicesFor = ((ServicesFor)spServicio.getSelectedItem()).getServicioPara();
        }
        ServiceData serviceData = new ServiceData(
                new ServicioUbicacion(latLngDestiny.latitude, latLngDestiny.longitude)
                , isPresente.isChecked()
                , observaciones.getText().toString()
                , new ServicioUbicacion(latLngOrigin.latitude
                , latLngOrigin.longitude)
                , stringservicesFor
                , third, gruas.get(spGruas.getSelectedItemPosition()).getClave(), null,
                null, null, null, null, null,
                null, null, null, null,
                null, null, null, null, null);


        compositeDisposable.add(serviciosCoberturaInteractor.sendCrane(idAjuste, serviceData)
                .subscribe(() -> {
                    status = 1;
                    boolACtivityServicios = true;
                    new GeneralSnackBar().largeSnackOk(activity, ResourceUtils.getString(R.string.success_request_service));
                    activity.onBackPressed();
                    onRefreshListener.onRefreshListener();
                }, throwable -> {
                    status = 2;
                    boolACtivityServicios = true;
                    Log.e(TAG, "Error al enviar datos de grúa:" + serviceData.toString(), throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);

                }));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listener.onDefaulTitle();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvOriginSelect:
                Intent intentMap = new Intent(activity, SelectAddressMapActivity.class);
                startActivityForResult(intentMap, Constants.REQUEST_CODE_MAP_ORIGIN);
                break;
            case R.id.cvDestinySelect:
                Intent iMap = new Intent(activity, SelectAddressMapActivity.class);
                startActivityForResult(iMap, Constants.REQUEST_CODE_MAP_DESTINY);
                break;

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ServicesFor servicesFor = (ServicesFor) spServicio.getSelectedItem();
        tvNameVehicle.setText(servicesFor.getVehiculo());

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}