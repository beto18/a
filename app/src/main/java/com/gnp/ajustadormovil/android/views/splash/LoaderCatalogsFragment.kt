package com.gnp.ajustadormovil.android.views.splash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils
import com.gnp.ajustadormovil.android.AjustadorMovilApplication
import com.gnp.ajustadormovil.android.views.ajustador.HomeActivity
import com.gnp.ajustadormovil.android.views.ajustador.LoginActivity
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoaderCatalogsFragment : Fragment() {


    private val TAG : String = this.javaClass.simpleName
    private val compositeDisposable = CompositeDisposable()
    private var activity: Activity? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val mView = inflater.inflate(R.layout.fragment_loader_catalogs, container, false)
        activity = getActivity()
        downloadCatalogos()
        return mView
    }

    private fun downloadCatalogos() {
        Log.i(TAG,"Iniciando descargar de catalogos")
        compositeDisposable.addAll(Completable.mergeArrayDelayError(
                    CatalogoInteractorsFactory.getCatInteractor().synchronizeData(),
                    CatalogoInteractorsFactory.getTprService().synchronizeData(),
                    CatalogoInteractorsFactory.getSiniestrosInteractor().synchronizeData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.i(TAG, "Catalogos actualizados correctamente")
                    OrquestadorInteractorFactory.getAmisInteractor().certificadoPropio
                            .subscribe({
                                initLogin()
                            }, {
                                Log.e(TAG, "Error al obtener el certificado", it)
                                initLogin()
                            })
                }, {
                    Log.e(TAG, "Error al descargar los catalogos", it)
                    DialogUtils.alert(context, "Error",
                            "No fue posible descargar los catálogos correctamente.\nRevise su conexión  de red y reintente.\nEs necesario descargar los catálogos para el correcto funcionamineto de la aplicación",
                            "Reintentar") { dialog, event -> downloadCatalogos() }
                }))
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun existSession(): Boolean {
        return AjustadorMovilApplication.getToken().isEmpty()
    }

    private fun initLogin() {
        val mainIntent = Intent(getActivity(), if (existSession())
            LoginActivity::class.java
        else
            HomeActivity::class.java)
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        activity!!.startActivity(mainIntent)
        activity!!.finish()
    }

    companion object {

        fun newInstance(bundle: Bundle?): LoaderCatalogsFragment {
            val fragment = LoaderCatalogsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

}
