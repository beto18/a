package com.gnp.ajustadormovil.android.views.splash

import android.Manifest
import android.annotation.TargetApi
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.gnp.ajustadormovil.android.BuildConfig
import com.gnp.ajustadormovil.android.R
import com.gnp.ajustadormovil.android.error.NetworkException
import com.gnp.ajustadormovil.android.interactors.context.ContextInteractor
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar
import io.reactivex.disposables.CompositeDisposable
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 *
 * Created by  on 03/10/17.
 *
 */

class SplashActivity : AppCompatActivity() {

    private val TAG : String = "SplashActivity"

    private var PERMISSIONS = arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    internal var PERMISSION_ALL = 1
    internal var AGAIN_PERMISSION_ALL = 2

    var compositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //val fechaPrueba = "Thu Jul 31 06:31:09 GMT+08:00 2018"
        compositeDisposable = CompositeDisposable()
        //initTimerTask()
        setContentView(R.layout.activity_splash)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        } else {
            getLastVersion()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var first = shouldShowRequestPermissionRationale(PERMISSIONS[0])
        var second = shouldShowRequestPermissionRationale(PERMISSIONS[2])
        var third = shouldShowRequestPermissionRationale(PERMISSIONS[3])
        var fourth = shouldShowRequestPermissionRationale(PERMISSIONS[4])
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED
                && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
            getLastVersion()
        } else {
            var message: String?
            if (!first || !second || !third || !fourth) {
                message = "No es posible utilizar la aplicación de Ajustador Móvil sin habilitar los permisos necesarios. Por favor habilite los permisos requeridos"
            }
            else{
                message = "Debe otorgar los permisos correspondientes para trabajar con Ajustador Móvil"
            }
            DialogUtils.createAlert(this, "Otorgar Permisos",
                    message, "Aceptar",
                    null, { dialog, which ->
                if (!first || !second || !third || !fourth) {
                    var intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    var uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivityForResult(intent, PERMISSION_ALL)
                } else {
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED
                            && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
                        getLastVersion()
                    }
                    else{
                        requestPermissions(PERMISSIONS,PERMISSION_ALL)
                    }
                }
            }, null)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==PERMISSION_ALL){
            if (resultCode == RESULT_OK) {
                requestPermissions(PERMISSIONS,PERMISSION_ALL)
            }
        }
    }

    fun getLastVersion() {
        val pInfo = this.packageManager.getPackageInfo(packageName, 0)
        val localVersion = pInfo.versionName.replace('.', '_')
        val realTime = Calendar.getInstance().time

        compositeDisposable!!.add(ContextInteractor.getInstance()
                .getLastVersion(17, 2)
                .subscribe({ response ->
                    val dateString = response!!.fecha_publicacion   // considered GMT
                    val validation = validateExpiration(dateString, realTime)
                    if (localVersion.compareTo(response!!.version) != 0 && BuildConfig.ENVIRONMENT == "PROD"
                            && validation) {
                        val builder = AlertDialog.Builder(this@SplashActivity)
                        builder.setTitle("Nueva versión")
                        builder.setMessage("Existe una nueva versión del Ajustador Móvil")
                        builder.setCancelable(false)
                        builder.setPositiveButton("Descargar") { _, _ ->
                            val updateIntent = Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://app.gnp.com.mx/"))
                            startActivity(updateIntent)
                            finishAffinity()
                            val prefEditor = getSharedPreferences(null, Context.MODE_PRIVATE).edit()
                            prefEditor.clear()
                            prefEditor.apply()
                        }
                        if (!response!!.isRequerida) {
                            builder.setMessage("Existe una nueva versión del Ajustador Móvil, ¿Desea descargarla?")

                            builder.setNegativeButton("Después") { _, _ ->
                                addFragment(LoaderCatalogsFragment.newInstance(null), "catalogs")
                            }
                        }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    } else {
                        addFragment(LoaderCatalogsFragment.newInstance(null), "catalogs")
                    }
                }, { throwable ->
                    if(throwable is NetworkException) {
                        showAlertDialog {
                            setTitle("Alerta")
                            setMessage("Su conexión no permite establecer comunicación con los servicios de GNP, Desea intentar de nuevo?")
                            setCancelable(false)
                            positiveButton("Si reintentar") {
                                showToast("Probando conexion")
                                if (hasPermissions(context, *PERMISSIONS)) {
                                    getLastVersion()
                                } else {
                                    finishAffinity()
                                }
                            }

                            negativeButton {
                                if (hasPermissions(context, *PERMISSIONS)) {
                                    addFragment(LoaderCatalogsFragment.newInstance(null), "catalogs")
                                } else {
                                    finishAffinity()
                                }
                            }
                        }
                        Log.e("Splash Activity", throwable.message)
                    } else {
                        Log.e(TAG, "Error al obtener la versión de la aplicación", throwable)
                        GeneralSnackBar().largeSnackError(this@SplashActivity, throwable)
                    }
                }))
    }

    fun showAlertDialog(dialogBuilder: AlertDialog.Builder.() -> Unit) {
        val builder = AlertDialog.Builder(this)
        builder.dialogBuilder()
        val dialog = builder.create()

        dialog.show()
    }

    fun AlertDialog.Builder.positiveButton(text: String = "Aceptar", handleClick: (which: Int) -> Unit = {}) {
        this.setPositiveButton(text) { _, which -> handleClick(which) }
    }

    fun AlertDialog.Builder.negativeButton(text: String = "Cancelar", handleClick: (which: Int) -> Unit = {}) {
        this.setNegativeButton(text) { _, which -> handleClick(which) }
    }

    fun showToast(msg: String, toastDuration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, msg, toastDuration).show()
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        fragment.setHasOptionsMenu(true)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.loader_catalogs, fragment, tag)
        transaction.commitAllowingStateLoss()
    }


    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable!!.dispose()
    }

    fun validateExpiration(fechaSistemaDB: String, fechaSOAndroid: Date): Boolean {
        val DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        val sdf = SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.forLanguageTag("es-MX"))
        try {
            val cal = sdf.parse(fechaSistemaDB)// all done
            return (fechaSOAndroid.after(cal))

        } catch (e: ParseException) {
            return false
        }


    }


    fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }


}
