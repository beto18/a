package com.gnp.ajustadormovil.android.views.terceros;

import android.annotation.SuppressLint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemSelectedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abraham on 07/12/2017.
 */

public class TerceroAdapter extends RecyclerView.Adapter<TerceroAdapter.TerceroHolder> {

    private ArrayList<Tercero> terceros;
    private OnItemRemovedListener removedListener;
    private OnItemSelectedListener selectedListener;
    public static final String TAG = "TAG";


    public TerceroAdapter(OnItemRemovedListener removedListener, OnItemSelectedListener selectedListener) {
        this.terceros = new ArrayList<>();
        this.removedListener = removedListener;
        this.selectedListener = selectedListener;
    }

    public void addAll(List<? extends Tercero> results) {
        for (Tercero result : results) {
            add(result);
        }
    }

    public void add(Tercero tercero) {
        terceros.add(tercero);
        notifyItemInserted(terceros.size() - 1);
    }

    public void remove(Tercero tercero) {
        int position = terceros.indexOf(tercero);
        if (position > -1) {
            terceros.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        terceros.clear();
        notifyDataSetChanged();
    }

    public List<Tercero> getTerceros() {
        return terceros;
    }

    public Tercero getTercero(int position) {
        return terceros.get(position);
    }

    @Override
    public TerceroHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_removable, parent, false);
        return new TerceroHolder(mView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(TerceroHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.content.setText(terceros.get(position).getDescripcion());
    }

    @Override
    public int getItemCount() {
        return terceros.size();
    }

    class TerceroHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvContent)
        AppCompatTextView content;
        @BindView(R.id.imgRemovedItem)
        ImageButton delete;
        @BindView(R.id.laDetails)
        ViewGroup details;
        @BindView(R.id.laHeader)
        LinearLayout header;

        TerceroHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            delete.setOnClickListener(this);
            header.setVisibility(View.GONE);
            details.setOnClickListener(view -> selectedListener.selectItem(view, getAdapterPosition()));
        }

        @Override
        public void onClick(View view) {
            removedListener.onItemClick(view, getAdapterPosition());
        }
    }
}
