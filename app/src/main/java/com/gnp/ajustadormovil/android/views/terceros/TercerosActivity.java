package com.gnp.ajustadormovil.android.views.terceros;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.commons.adapters.PagerAdapter_Fragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.gnp.ajustadormovil.android.views.terceros.autos.TercerosAutoFragment;
import com.gnp.ajustadormovil.android.views.terceros.noautos.TercerosNoAutoFragment;
import com.gnp.ajustadormovil.android.views.terceros.personas.TercerosPersonaFragment;

import butterknife.BindView;

/**
 * .
 * Created by Lima on 14/11/17.
 */

public class TercerosActivity extends BaseActivity implements OnToolbarListener {

    private static final String TAG = TercerosActivity.class.getSimpleName();
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.result_tabs)
    TabLayout tabs;
    @BindView(R.id.toolbar_terceros)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView title;
    @Nullable
    @BindView(R.id.buttons_radio_teeceros)
    RadioGroup mRadioGroup;
    private PagerAdapter_Fragment mPagerAdapterFragment;
    private String idAjuste;
    private Menu menu;
    private int modulo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_terceros_parent);
        //setmRadioGroup(mRadioGroup);
        setmToolbar(mToolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        initPager(viewPager);
        tabs.setupWithViewPager(viewPager);
        newActivity = 1;
    }

    public void initPager(ViewPager viewPager) {
        //ViewPager
        idAjuste = getIntent().getStringExtra("idAjuste");
        Bundle bundle = getIntent().getExtras();
        modulo = getIntent().getExtras().getInt("modulo");
        assert bundle != null;
        bundle.putString("id_ajuste", getIntent().getExtras().getString("id_ajuste"));
        if (getIntent().getExtras().getBoolean("show")){
            DialogUtils.createAlert(this, "Terceros",
                    ResourceUtils.getString("edua_completar_terceros"),
                    ResourceUtils.getString("btn_firma_aceptar"), null,
                    (dialog, which) -> dialog.dismiss(),
                    null);
        }
        mPagerAdapterFragment = new PagerAdapter_Fragment(getSupportFragmentManager());
        mPagerAdapterFragment.addFragment(TercerosAutoFragment.class, "Autos", bundle);
        mPagerAdapterFragment.addFragment(TercerosNoAutoFragment.class, "No autos", bundle);
        mPagerAdapterFragment.addFragment(TercerosPersonaFragment.class, "Peatones", bundle);
        viewPager.setAdapter(mPagerAdapterFragment);
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        tabs.setupWithViewPager(viewPager);

    }

    public String getTercerosCaptureIdAjuste(){
        return idAjuste;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onChangeTitle(String title) {

    }

    @Override
    public void onDefaulTitle() {

    }
}
