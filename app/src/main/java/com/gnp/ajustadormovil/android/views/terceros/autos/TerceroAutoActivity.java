package com.gnp.ajustadormovil.android.views.terceros.autos;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModuleAuto;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.PolizaInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroInteractor;
import com.gnp.ajustadormovil.android.models.SerieResponse;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.ImpactoVehiculoAmis;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculo;
import com.gnp.ajustadormovil.android.models.catalogos.tpr.VersionVehiculoResumen;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PiezaVehiculo;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemOcupanteListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnParteLesionadaListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnTelefonoListener;
import com.gnp.ajustadormovil.android.views.commons.piezas.GeneralDamageFragment;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_TERCEROS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_AUTO;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_CONDUCTOR;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_OCUPANTE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_VEHICULO;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getAllImages;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getImagePathAuto;
import static com.gnp.ajustadormovil.android.utils.Constants.TERCEROS;
import static com.gnp.ajustadormovil.android.utils.Constants.VEHICULO;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.isSaveVehicle;

public class TerceroAutoActivity extends BaseActivity implements OnTelefonoListener,
        OnItemOcupanteListener, OnParteLesionadaListener,
        CallbackItemImage<Long>, CallBackItemCheck, AdapterView.OnItemSelectedListener {

    //Views
    @BindView(R.id.toolbar_autos_tercero)
    Toolbar mToolbar;
    @BindView(R.id.detail_vehiculo_terceros)
    LinearLayoutCompat compat2;
    @BindView(R.id.fragment_example)
    FrameLayout compat;
    @BindView(R.id.img_detalle_conductor_tercero)
    AppCompatImageView arrow;
    @BindView(R.id.img_detalle_vehiculo_tercero)
    AppCompatImageView arrow2;
    @BindView(R.id.img_detalle_ocupantes)
    AppCompatImageView arrow3;
    @BindViews({R.id.edtPlacasP, R.id.edtMotorV, R.id.edtSerieV})
    List<EditText> vehiculo_datos;
    @BindViews({R.id.spTipoVehiculoP, R.id.spArmadora, R.id.spCarroceria, R.id.spModeloP, R.id.spVersionP, R.id.spPrimerImpacto})
    List<Spinner> spVehiculoDatos;
    @BindView(R.id.cbPerdidaTotal)
    CheckBox cbPerdidaTotal;
    @BindView(R.id.etObservaciones)
    EditText observaciones;
    @BindView(R.id.cbConcecuencia)
    CheckBox cbDanoV;
    @BindView(R.id.edtEstimadoDano)
    EditText etEstimadoDanos;
    @BindView(R.id.btnGuardarAuto)
    Button btnGuardarVehiculo;
    @BindView(R.id.btnAgregarOcupante)
    Button btnAgregarOcupante;
    @BindView(R.id.tercero_container)
    RelativeLayout layout;
    @BindView(R.id.daños_container)
    LinearLayout dano_container;
    @BindView(R.id.recyclerOcupantes)
    RecyclerView rvOcupantes;
    @BindView(R.id.tvNumeroPoliza)
    TextView poliza;
    @BindView(R.id.btnuploadImagesVehiculo)
    Button btnSubirVehiculoFotos;
    @BindView(R.id.spColorVehiculo)
    Spinner spColor;

    private Button btnOK;
    private EditText phone, extension;

    //Primitives
    private boolean isUploadedV = false;
    private int posPoliza;
    private boolean isVolante;
    private long id_auto;
    public static long idVehiculo;
    public static boolean isBoolSendImagesOCupante;
    private int posicion;

    //String
    private String idAjuste = "";
    private String poliza_serie = "";
    private String vin = "";
    private String messageVehiculo;
    private String path;
    public static final String TAG = "TAG";
    //List
    private List<CatalogoClave> armadorasList = new ArrayList<>();
    private List<ImpactoVehiculoAmis> impactos = new ArrayList<>();
    private List<CatalogoClave> carroceriaList = new ArrayList<>();
    private List<CatalogoClave> tipoVehiculos = new ArrayList<>();
    private List<Short> modelos = new ArrayList<>();
    private List<VersionVehiculoResumen> versiones = new ArrayList<>();
    private List<Ocupante> ocupantes;
    private List<String> polizas = new ArrayList<>();
    private List<SerieResponse> series = new ArrayList<>();
    //servicios de ajuste
    private TprInteractor tprService;
    private SiniestrosInteractor siniestrosInteractor;
    //servicios de catalogos
    private PolizaInteractor polizaInteractor;
    private TerceroInteractor terceroService;
    private TerceroAutoInteractor terceroAutoService;
    //Objects
    private VersionVehiculo categoria;
    private VehiculoTercero foundVehiculo = new VehiculoTercero();
    private VehiculoTercero autoTercero;
    public static Long idAuto;
    private EventBus bus = EventBus.getDefault();
    private Bundle bundle;
    //Adapters
    private TerceroAutoOcupanteAdapter ocupanteAdapter;
    //Fragments
    private GeneralCameraFragment generalCameraFragment;
    private GeneralDamageFragment generalDamageFragment;
    private GeneralDamageFragment generalDamageFragmentPreexistente;
    private TerceroAutoConductorFragment conductorTercero;
    //listener
    private CallbackItemImage listener;
    private boolean userIsInteracting;
    private List<ImageGeneralModel> filesPhotos = new ArrayList<>();
    private String tipoVehiculo;
    private String armadora;
    private String carroceria;
    private Short modelo;
    private String version;
    private Long colorVehiculo;

    private List<CatalogoId> coloresList = new ArrayList<>();
    private NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_tercero);
        bundle = getIntent().getExtras();
        setmToolbar(mToolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        spVehiculoDatos.get(1).setVisibility(GONE);
        spVehiculoDatos.get(2).setVisibility(GONE);
        spVehiculoDatos.get(3).setVisibility(GONE);
        spVehiculoDatos.get(4).setVisibility(GONE);
        if (compat2.getVisibility() == View.GONE) {

            isUploadedV = true;
        }
        isSaveVehicle = false;
        if (bundle.getBoolean("show")){
            DialogUtils.createAlert(this, "Tercero Auto",
                    ResourceUtils.getString("edua_tercero_incompleto"),
                    ResourceUtils.getString("btn_firma_aceptar"), null,
                    (dialog, which) -> dialog.dismiss(),
                    null);
        }
        initServices();
        initFragments();
        initView();
        String bundleVehiculo = bundle.getString("foundVehiculo");
        if (bundleVehiculo != null && !bundleVehiculo.equals(""))
            foundVehiculo = new Gson().fromJson(bundleVehiculo, VehiculoTercero.class);
        initListeners();
        initListenersAuto();
        getOcupantes();
        getColorsCompletable();


    }

    private void initServices() {
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        tprService = CatalogoInteractorsFactory.getTprService();
        polizaInteractor = OrquestadorInteractorFactory.getPolizaInteractor();
        terceroService = OrquestadorInteractorFactory.getTerceroService();
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
    }

    private void initView() {
        btnSubirVehiculoFotos.setVisibility(GONE);
        findViewById(R.id.poliza_encontrada).setVisibility(GONE);
        poliza.setVisibility(GONE);
        listener = this;
        btnSubirVehiculoFotos.setVisibility(GONE);
        id_auto = bundle.getLong("id");
        idVehiculo = id_auto;
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        ocupanteAdapter = new TerceroAutoOcupanteAdapter(this);
        rvOcupantes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvOcupantes.setAdapter(ocupanteAdapter);
        rvOcupantes.setNestedScrollingEnabled(false);
        rvOcupantes.setHasFixedSize(true);
    }

    private void initFragments() {
        generalCameraFragment = new GeneralCameraFragment();
        generalDamageFragment = new GeneralDamageFragment();
        generalDamageFragment.setDiscriminator("dano");
        generalDamageFragmentPreexistente = new GeneralDamageFragment();
        generalDamageFragmentPreexistente.setDiscriminator("pree");
        addFragmentDamgeParts(generalDamageFragment);
        addFragmentDamgePartsPreexisting(generalDamageFragmentPreexistente);
        addFragmentCamera(generalCameraFragment);
        conductorTercero = TerceroAutoConductorFragment.newInstance();
        conductorTercero.setArguments(getIntent().getExtras());
        if (getApplicationContext() != null) {
            addFragment(conductorTercero);
        }
    }

    private void initListenersAuto() {

        spVehiculoDatos.get(0).setOnItemSelectedListener(this);
        spVehiculoDatos.get(1).setOnItemSelectedListener(this);
        spVehiculoDatos.get(2).setOnItemSelectedListener(this);
        spVehiculoDatos.get(3).setOnItemSelectedListener(this);
        spVehiculoDatos.get(4).setOnItemSelectedListener(this);
        spVehiculoDatos.get(5).setOnItemSelectedListener(this);
        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                colorVehiculo = coloresList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSeries() {
        if (series.size() == 0) {
            findViewById(R.id.poliza_encontrada).setVisibility(VISIBLE);
            poliza.setVisibility(VISIBLE);
            poliza.setText(getText(R.string.not_found_poilza));

        } else {
            if (polizas.size() == 0) {
                for (int i = 0; i < series.size(); i++) {
                    polizas.add(series.get(i).getPoliza());
                }
            } else {
                polizas.clear();
                for (int i = 0; i < series.size(); i++) {
                    polizas.add(series.get(i).getPoliza());
                }
            }
            selectPoliza();
        }
    }

    private void getDatosVehiculo() {
        downloadTiposVehiculo();
        downloadImpactos();
        updateDataVehiculo();
    }


    private void downloadTiposVehiculo() {
        getCompositeDisposable().add(tprService.getTiposVehiculo()
                .subscribe(tiposVehiculos -> {
                    tipoVehiculos = tiposVehiculos;
                    AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, tipoVehiculos, CatalogoClave::getNombre);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spVehiculoDatos.get(0).setAdapter(adapter);
                    if (foundVehiculo.getTipoVehiculo() != null) {
                        spVehiculoDatos.get(0).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(0), foundVehiculo.getTipoVehiculo()));
                    }
                }, throwable -> {
                    spVehiculoDatos.get(0).setAdapter(null);
                    Log.e(TAG, "Error al descargar tipo de vehiculos ", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                })
        );
    }

    private void downloadImpactos() {
        getCompositeDisposable().add(siniestrosInteractor.getImpactosVehiculo()
                .subscribe(impactosResponse -> {
                    impactos = impactosResponse;
                    AdapterSpinnerGeneral<ImpactoVehiculoAmis> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, impactos, ImpactoVehiculoAmis::getNombre);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spVehiculoDatos.get(5).setAdapter(adapter);
                    if (foundVehiculo.getPrimerImpacto() != null) {
                        spVehiculoDatos.get(5).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(5), foundVehiculo.getPrimerImpacto()));
                    }
                }, throwable -> {
                    spVehiculoDatos.get(5).setAdapter(null);
                    Log.e(TAG, "Error al descargar impactos", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                }));

    }


    private void selectPoliza() {
        MaterialDialog mBuilder = new MaterialDialog.Builder(this)
                .title("Seleccionar póliza")
                .items(polizas)
                .positiveText("Aceptar")
                .negativeText("Cancelar")
                .cancelable(false)
                .onNegative((dialog, which) -> dialog.dismiss())
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    posPoliza = which;
                    return true;
                })
                .onPositive((dialog, which) -> {
                    findViewById(R.id.poliza_encontrada).setVisibility(VISIBLE);
                    poliza.setVisibility(VISIBLE);
                    if (series.get(posPoliza) != null) {
                        poliza.setText("");
                        poliza_serie = series.get(posPoliza).getPoliza() != null ? series.get(posPoliza).getPoliza() : "No se encontró póliza vigente para el vehículo indicado";
                        String inicioPoliza = series.get(posPoliza).getInicioPoliza() != null ? series.get(posPoliza).getInicioPoliza() : "No hay fecha";
                        String finPoliza = series.get(posPoliza).getFinPoliza() != null ? series.get(posPoliza).getFinPoliza() : "No hay fecha";
                        String marca = series.get(posPoliza).getMarca() != null ? series.get(posPoliza).getMarca() : "No hay marca";
                        Integer modelo = series.get(posPoliza).getModelo() != null ? series.get(posPoliza).getModelo() : 0;
                        vin = series.get(posPoliza).getVin() != null ? series.get(posPoliza).getVin() : "No hay vin";
                        String subMarca = series.get(posPoliza).getSubmarca() != null ? series.get(posPoliza).getSubmarca() : "No hay sub-marca";
                        String inciso = series.get(posPoliza).getInciso() != null ? series.get(posPoliza).getInciso() : "No hay inciso";
                        poliza.append("Póliza: " + poliza_serie
                                + "\n"
                                + "Inicio de póliza: " + inicioPoliza
                                + "\n"
                                + "Fin de póliza: " + finPoliza
                                + "\n"
                                + "Marca del vehículo: " + marca
                                + "\n"
                                + "Modelo: " + modelo
                                + "\n"
                                + "Vin: " + vin
                                + "\n"
                                + "Sub-marca: " + subMarca
                                + "\n"
                                + "Inciso: " + inciso);
                    }

                })
                .build();
        MDButton aceptar = mBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = mBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(mBuilder.getContext(),R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        mBuilder.show();
    }
    private void getColorsCompletable() {
        getCompositeDisposable().add(getcolors()
                .subscribe(() ->{

                        },
                        throwable -> new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable)));

    }
    private Completable getcolors() {
        return Completable.mergeArrayDelayError(
                downloadColors());

    }
    private Completable downloadColors(){
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getColoresVehiculo()
                    .subscribe(colores -> {
                        coloresList = colores;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, coloresList, CatalogoId::getNombre);
                        spColor.setAdapter(adapter);

                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spColor.setAdapter(null);
                        Log.e(TAG, "Error al descargar los colores", throwable);
                    }));

        });
    }

    private void initListeners() {

        vehiculo_datos.get(2).setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                getCompositeDisposable().add(polizaInteractor.getPolizas(vehiculo_datos.get(2).getText().toString())
                        .subscribe(seriesResponse -> {
                            series = seriesResponse;
                            this.setSeries();
                        }, throwable -> {
                            Log.e(TAG, "Error al descargar las polizas", throwable);
                            new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);

                            findViewById(R.id.poliza_encontrada).setVisibility(VISIBLE);
                            poliza.setText(R.string.not_found_poilza);
                            this.setSeries();
                        }));

                return true;
            }
            return false;
        });
        cbDanoV.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                cbPerdidaTotal.setChecked(false);
                dano_container.setVisibility(GONE);
            } else {
                dano_container.setVisibility(VISIBLE);
            }
        });
        cbPerdidaTotal.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                cbDanoV.setChecked(false);
                dano_container.setVisibility(GONE);
            } else {
                dano_container.setVisibility(VISIBLE);
            }
        });


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void deleteOcupante(Ocupante ocupante) {
        DialogUtils.createAlert(this, "Eliminar ocupante", "¿Desea eliminar este ocupante?", "Aceptar", "Cancelar",
                (dialog, which) -> getCompositeDisposable().add(terceroAutoService.deleteOcupante(idAjuste,
                        getIntent().getExtras().getLong("id"),
                        ocupante.getId())
                        .subscribe(() -> {
                            new GeneralSnackBar().largeSnackOk(TerceroAutoActivity.this, "Se elimino correctamente el ocupante");
                            ocupanteAdapter.remove(ocupanteAdapter.getOcupante(posicion));
                            rvOcupantes.setAdapter(ocupanteAdapter);
                            ocupanteAdapter.notifyDataSetChanged();
                            getOcupantes();
                        }, throwable -> {
                            Log.e(TAG, "Error al eliminar ocupante " + ocupante.getId() + " del ajuste: " + idAjuste, throwable);
                            new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);

                        })), (dialog, which) -> dialog.dismiss());
    }

    private void getOcupantes() {
        getCompositeDisposable().add(terceroAutoService.getOcupantes(idAjuste, id_auto)
                .subscribe(ocupantesResponse -> {
                    ocupantes = ocupantesResponse;
                    setOcupantes();
                }, throwable -> {
                    Log.e(TAG, "Error al descargar ocupantes del ajuste: " + idAjuste, throwable);
                    if (!(throwable instanceof ResourceNotFoundException))
                        new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                }));

    }

    private void setOcupantes() {
        if (ocupantes != null && !ocupantes.isEmpty()) {
            rvOcupantes.setAdapter(ocupanteAdapter);
            ocupanteAdapter.clear();
            ocupanteAdapter.addAll(ocupantes);
            ocupanteAdapter.notifyDataSetChanged();
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void viewPhotoAuto(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    public void updateDataVehiculo() {
        ModuleAuto moduleAuto = new ModuleAuto(Long.toString(id_auto) + MODULO_TERCEROS + TIPO_VEHICULO, TERCEROS, VEHICULO, "", id_auto, idAjuste);
        path = getImagePathAuto(moduleAuto);
        if (path != null) {
            filesPhotos.clear();
            filesPhotos.addAll(PersistenciaPath.getAllImages(getBaseActivity(), path));
            generalCameraFragment.setImageList(filesPhotos);
            PersistenciaPath.showMessageSizeImages(this, filesPhotos);
        }

        if (foundVehiculo != null) {
            if (foundVehiculo.getTipoVehiculo() != null) isSaveVehicle = true;
            isUploadedV = true;
            messageVehiculo = "Se actualizó correctamente el vehículo";
            if (foundVehiculo.getPlacas() != null) {
                vehiculo_datos.get(0).setText(foundVehiculo.getPlacas());
            }
            if (foundVehiculo.getNumeroMotor() != null) {
                vehiculo_datos.get(1).setText(foundVehiculo.getNumeroMotor());
            }
            if (foundVehiculo.getVin() != null) {
                vehiculo_datos.get(2).setText(foundVehiculo.getVin());
            }
            if (foundVehiculo.getDanoEstimado() != null) {
                NumberFormat format = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));
                String currency = format.format(foundVehiculo.getDanoEstimado());
                etEstimadoDanos.setText(currency);
            }
            if (foundVehiculo.getPerdidaTotalEvidente()) {
                cbPerdidaTotal.setChecked(true);
                dano_container.setVisibility(View.GONE);
            } else {
                cbPerdidaTotal.setChecked(false);
                dano_container.setVisibility(VISIBLE);
            }
            if (!foundVehiculo.getPerdidaTotalEvidente()) {
                if (!foundVehiculo.getPerdidaTotalEvidente()) {
                    cbDanoV.setChecked(false);
                    dano_container.setVisibility(VISIBLE);
                } else {
                    cbDanoV.setChecked(true);
                    dano_container.setVisibility(View.GONE);
                }
            }
            if (foundVehiculo.getObservacion() != null) {
                observaciones.setText(foundVehiculo.getObservacion());
            }

            if (!foundVehiculo.getDanos().isEmpty()) {
                generalDamageFragment.loadDataList(getListIntDanos(foundVehiculo.getDanos()));
            }
            if (!foundVehiculo.getDanosPreexistentes().isEmpty()) {
                generalDamageFragmentPreexistente.loadDataList(getListIntDanos(foundVehiculo.getDanosPreexistentes()));

            }
            if (!foundVehiculo.getFotografias().isEmpty()) {
                uploadPhotos();
                generalCameraFragment.setCallBackItem(listener);
                btnSubirVehiculoFotos.setVisibility(VISIBLE);
            }
            if (foundVehiculo.getColor()!=null){
                spColor.setSelection(SpinnerUtils.getPosition(spColor, foundVehiculo.getColor()));
            }
            if (foundVehiculo.getVolante() != null) {
                btnSubirVehiculoFotos.setVisibility(VISIBLE);
                btnGuardarVehiculo.setVisibility(VISIBLE);
            } else {
                btnSubirVehiculoFotos.setVisibility(VISIBLE);
                btnGuardarVehiculo.setVisibility(VISIBLE);
            }
        } else {
            messageVehiculo = "Se agregó correctamente el vehículo";
        }
    }

    private ArrayList<Long> getListIntDanos(List<PiezaVehiculo> piezasAfectadas) {
        ArrayList<Long> result = new ArrayList<>();
        for (PiezaVehiculo pieza : piezasAfectadas) {
            if (pieza.getParteAfectada() != null)
                result.add(pieza.getParteAfectada());
        }
        return result;
    }

    private void downloadArmadora(String tipoAuto) {
        Action sinDatosArmadoras = () -> {
            spVehiculoDatos.get(1).setAdapter(null);
            spVehiculoDatos.get(2).setAdapter(null);
            spVehiculoDatos.get(3).setAdapter(null);
            spVehiculoDatos.get(4).setAdapter(null);
            armadorasList = new ArrayList<>();
            carroceriaList = new ArrayList<>();
            modelos = new ArrayList<>();
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getArmadoras(tipoAuto)
                .subscribe(armadoraResponse -> {
                    armadorasList = armadoraResponse;
                    if (!armadorasList.isEmpty()) {
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, armadorasList, CatalogoClave::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spVehiculoDatos.get(1).setAdapter(adapter);
                        spVehiculoDatos.get(1).setVisibility(VISIBLE);
                        if (foundVehiculo.getArmadora() != null) {
                            spVehiculoDatos.get(1).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(1), foundVehiculo.getArmadora()));
                        }
                    } else {
                        sinDatosArmadoras.run();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar la armadora de vehículo", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                    sinDatosArmadoras.run();
                }));

    }

    private void downloadCarroceria(String tipoVehiculo, String tipoArmadora) {
        Action sinDatosCarroceria = () -> {
            spVehiculoDatos.get(2).setAdapter(null);
            spVehiculoDatos.get(3).setAdapter(null);
            spVehiculoDatos.get(4).setAdapter(null);
            carroceriaList = new ArrayList<>();
            modelos = new ArrayList<>();
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getCarrocerias(tipoVehiculo, tipoArmadora)
                .subscribe(carroceriaResponse -> {
                    carroceriaList = carroceriaResponse;
                    if (!carroceriaList.isEmpty()) {
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, carroceriaList, CatalogoClave::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spVehiculoDatos.get(2).setAdapter(adapter);
                        spVehiculoDatos.get(2).setVisibility(VISIBLE);
                        if (foundVehiculo.getCarroceria() != null) {
                            spVehiculoDatos.get(2).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(2), foundVehiculo.getCarroceria()));
                        }
                    } else {
                        sinDatosCarroceria.run();
                    }
                }, throwable -> {
                    sinDatosCarroceria.run();
                    Log.e(TAG, "Error al descargar carroceria de vehículo", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                })
        );

    }

    private void downloadModelos(String tipoVehiculo, String tipoArmadora, String tipoCarroceria) {
        Action sinDatosModelos = () -> {
            spVehiculoDatos.get(3).setAdapter(null);
            spVehiculoDatos.get(4).setAdapter(null);
            modelos = new ArrayList<>();
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getModelos(tipoVehiculo, tipoArmadora, tipoCarroceria)
                .subscribe(modelosResponse -> {
                    modelos = modelosResponse;
                    if (!modelos.isEmpty()) {
                        AdapterSpinnerGeneral<Short> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, modelos, Object::toString);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spVehiculoDatos.get(3).setAdapter(adapter);
                        spVehiculoDatos.get(3).setVisibility(VISIBLE);
                        if (foundVehiculo.getModelo() != null) {
                            spVehiculoDatos.get(3).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(3), foundVehiculo.getModelo()));
                        }
                    } else {
                        sinDatosModelos.run();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar modelos de vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                    sinDatosModelos.run();
                })
        );

    }


    private void downloadVersiones(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int yearModelo) {
        Action sinDatosVersiones = () -> {
            spVehiculoDatos.get(4).setAdapter(null);
            versiones = new ArrayList<>();
        };
        getCompositeDisposable().add(tprService.getVersiones(tipoVehiculo, tipoArmadora, tipoCarroceria, yearModelo)
                .subscribe(versionesResponse -> {
                    versiones = versionesResponse;
                    if (!versiones.isEmpty()) {
                        AdapterSpinnerGeneral<VersionVehiculoResumen> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, versiones, VersionVehiculoResumen::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spVehiculoDatos.get(4).setAdapter(adapter);
                        spVehiculoDatos.get(4).setVisibility(VISIBLE);
                        if (foundVehiculo.getVersion() != null) {
                            spVehiculoDatos.get(4).setSelection(SpinnerUtils.getPosition(spVehiculoDatos.get(4), foundVehiculo.getVersion()));
                        }

                    } else {
                        sinDatosVersiones.run();
                    }
                }, throwable -> {
                    sinDatosVersiones.run();
                    Log.e(TAG, "Error al descargar versiones de vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                })
        );

    }


    private void downloadCategoria(String tipoVehiculo, String tipoArmadora, String tipoCarroceria, int modelo, String version) {
        getCompositeDisposable().add(tprService.getVersion(tipoVehiculo, tipoArmadora, tipoCarroceria, modelo, version)
                .subscribe(categoriaList -> {
                    categoria = categoriaList;
                    Log.d(TAG, "categoria del tercero vehiculo: " + tipoVehiculo + tipoArmadora + tipoCarroceria + modelo + version);
                    if (generalDamageFragment!=null && !generalDamageFragment.getDataList().isEmpty()) {
                        generalDamageFragment.getDataList().clear();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar categoria de vehículos", throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                })
        );
    }


    private void getMoney(String categoria, List<Long> partes) {
        if (categoria != null) {
            if (partes.size() != 0) {
                getCompositeDisposable().add(polizaInteractor.getCostosTercero(categoria, partes)
                        .subscribe(costoResponse -> {
                            String currency = numberFormat.format(costoResponse.getCosto());
                            etEstimadoDanos.setText(currency);

                        }, throwable -> {
                            Log.e(TAG, "Error al descargar costos de partes dañadas de tercero vehiculo", throwable);
                            new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                        }));

            } else {
                etEstimadoDanos.setText("$" + 0.00);
            }
        } else {
            etEstimadoDanos.setText("$" + 0.00);
        }
    }


    @OnClick(R.id.btnuploadImagesVehiculo)
    public void sendPhotosVehiculos() {
        ImageUtils imageUtils = new ImageUtils();
        List<ImageGeneralModel> imageList = generalCameraFragment.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        if (!images.isEmpty()) {
            int size = images.size();
            btnSubirVehiculoFotos.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            getCompositeDisposable().add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> terceroAutoService.addFotografiaVehiculo(idAjuste, id_auto, file), false, 1)
                    .doOnEach(fotografias -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        }
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir fotografía del vehículo tercero", throwable);
                        return new Fotografia();
                    })
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        boolean ok = true;
                        for (Fotografia pic : fotografias) {
                            ok &= pic.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(this, getResources().getString(R.string.imagenes_upload_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(this, getResources().getString(R.string.imagenes_upload_error));
                        }
                        btnSubirVehiculoFotos.setEnabled(true);
                        imageUtils.renameImageUpload(imageList);
                        imageUtils.deleteImage(getBaseActivity(), imageList);
                        filesPhotos.clear();
                        uploadPhotos();
                    }, throwable -> {
                        btnSubirVehiculoFotos.setEnabled(true);
                        Log.e(TAG, "Error al subir imagenes de vehiculo tercero" + id_auto + "del ajuste: " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                    }));
        } else {
            new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, getString(R.string.image_void));
            btnSubirVehiculoFotos.setEnabled(true);
        }
    }

    private void uploadPhotos() {
        getCompositeDisposable().add(terceroAutoService.getFotografiaVehiculo(idAjuste, id_auto)
                .subscribe(fotografias -> {
                    for (Fotografia photo : fotografias) {
                        filesPhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                    }
                    generalCameraFragment.updateImageList(filesPhotos);
                }, throwable -> {
                    Log.e(TAG, "Error al descargar imagenes del vehiculo tercero " + id_auto + " del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                }));
    }


    @Subscribe
    public void deletePhotoVehiculo(long id) {

        getCompositeDisposable().add(terceroAutoService.deleteFotografiaVehiculo(idAjuste,
                getIntent().getExtras().getLong("id"), id)
                .subscribe(() -> {
                            new GeneralSnackBar().largeSnackOk(TerceroAutoActivity.this, "Se elimino correctamente la fotografia del vehiculo");

                        }, throwable -> {
                            Log.e(TAG, "Error al eliminar imagenes de vehiculo tercero " + id_auto + " del ajuste: " + idAjuste, throwable);
                            new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);
                        }
                ));

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void reloadData(String load) {
        if (load.equals("load")) {
            ocupanteAdapter.clear();
            rvOcupantes.setAdapter(ocupanteAdapter);
            ocupanteAdapter.notifyDataSetChanged();
            getOcupantes();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressLint("MissingPermission")
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Utils.NUMBER_AMBULANCE));
        startActivity(intent);
    }

    @OnClick({R.id.detailConductorTercero, R.id.detailVehiculoTercero, R.id.detailOcupanteTercero})
    public void showOptions(View v) {
        switch (v.getId()) {
            case R.id.detailConductorTercero:
                generalCameraFragment.setFolder(MODULO_TERCEROS, TIPO_CONDUCTOR, TIPO_AUTO, "", Long.toString(id_auto));
                if (compat.getVisibility() == VISIBLE) {
                    compat.setVisibility(GONE);
                    findViewById(R.id.fragment_example).setVisibility(View.GONE);
                    arrow.setRotation(270);
                    arrow.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                } else {
                    compat.setVisibility(VISIBLE);
                    findViewById(R.id.fragment_example).setVisibility(VISIBLE);
                    arrow.setRotation(0);
                    arrow.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
                }
                compat2.setVisibility(GONE);
                arrow2.setRotation(270);
                arrow2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                rvOcupantes.setVisibility(GONE);
                btnAgregarOcupante.setVisibility(GONE);
                arrow3.setRotation(270);
                arrow3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));

                break;
            case R.id.detailVehiculoTercero:
                isUploadedV = false;
                generalCameraFragment.setFolder(MODULO_TERCEROS, TIPO_VEHICULO, TIPO_AUTO, "", Long.toString(id_auto));
                if (compat2.getVisibility() == VISIBLE) {
                    compat2.setVisibility(GONE);
                    arrow2.setRotation(270);
                    arrow2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                } else {
                    compat2.setVisibility(VISIBLE);
                    arrow2.setRotation(0);
                    arrow2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));

                    getDatosVehiculo();

                }
                compat.setVisibility(GONE);
                arrow.setRotation(270);
                arrow.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                rvOcupantes.setVisibility(GONE);
                btnAgregarOcupante.setVisibility(GONE);
                arrow3.setRotation(270);
                arrow3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                break;
            case R.id.detailOcupanteTercero:
                generalCameraFragment.setFolder(MODULO_TERCEROS, TIPO_OCUPANTE, TIPO_AUTO, "", Long.toString(id_auto));
                idAuto = id_auto;
                if (rvOcupantes.getVisibility() == VISIBLE) {
                    rvOcupantes.setVisibility(View.GONE);
                    btnAgregarOcupante.setVisibility(View.GONE);
                    arrow3.setRotation(270);
                    arrow3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                } else {
                    rvOcupantes.setVisibility(VISIBLE);
                    btnAgregarOcupante.setVisibility(VISIBLE);
                    arrow3.setRotation(0);
                    arrow3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_less));
                }
                compat.setVisibility(GONE);
                arrow.setRotation(270);
                arrow.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                compat2.setVisibility(GONE);
                arrow2.setRotation(270);
                arrow2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3) {
            conductorTercero.onActivityResult(requestCode, resultCode, data);
        } else {
            generalCameraFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick({R.id.btnGuardarAuto, R.id.btnAgregarOcupante})
    public void showDialogOptions(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnGuardarAuto:
                addCar();
                break;
            case R.id.btnAgregarOcupante:
                intent = new Intent(this, TerceroAutoOcupanteActivity.class);
                intent.putExtra("id", Long.valueOf(getIntent().getExtras().getLong("id")));
                intent.putExtra("discriminator", "nuevo");
                intent.putExtra("data", getIntent().getExtras().getString("data"));
                startActivity(intent);
                break;
        }
    }


    private void addCar() {
        String placa = vehiculo_datos.get(0).getText().toString();
        String motor = vehiculo_datos.get(1).getText().toString();
        String serie = vehiculo_datos.get(2).getText().toString();
        VehiculoTercero data = new VehiculoTercero();
        if (tipoVehiculo == null) {
            new GeneralSnackBar().largeSnackError(this, "Verifique el tipo de vehículo por favor");
            return;
        } else {
            data.setTipoVehiculo(tipoVehiculo);
        }
        if (armadora == null) {
            new GeneralSnackBar().largeSnackError(this, "Verifique la armadora del vehículo por favor");
            return;
        } else {
            data.setArmadora(armadora);
        }
        if (carroceria == null) {
            new GeneralSnackBar().largeSnackError(this, "Verifique la carrocería del vehículo por favor");
            return;
        } else {
            data.setCarroceria(carroceria);
        }
        if (modelo == null) {
            new GeneralSnackBar().largeSnackError(this, "Verifique el modelo del vehículo por favor");
            return;
        } else {
            data.setModelo(modelo);
        }
        if (version == null) {
            new GeneralSnackBar().largeSnackError(this, "Verifique la versión del vehículo por favor");
            return;
        } else {
            data.setVersion(version);
        }
        if(colorVehiculo == null){
            new GeneralSnackBar().largeSnackError(this, "Verifique el color del vehículo por favor");
            return;
        }
        else{
            data.setColor(colorVehiculo);
        }
        data.setNumeroMotor(motor);
        data.setPlacas(placa);
        data.setVin(serie);
        if (cbDanoV.isChecked() || cbPerdidaTotal.isChecked()) {
            data.setPerdidaTotalEvidente(cbPerdidaTotal.isChecked());
            data.setDanoConsecuencia(false);
        } else {
            data.setDanoConsecuencia(true);
            data.setPerdidaTotalEvidente(false);
            data.setObservacion(observaciones.getText().toString());
            data.setPrimerImpacto(impactos.get(spVehiculoDatos.get(5).getSelectedItemPosition()).getId());
            if (generalDamageFragmentPreexistente.getList().size() == 0) {
                data.setDanosPreexistentes(null);
            } else {
                data.setDanosPreexistentes(generalDamageFragmentPreexistente.getDataList());
            }
            if (generalDamageFragment.getList().size() == 0) {
                new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, getString(R.string.partes_danadas));
                return;
            } else {
                data.setDanos(generalDamageFragment.getDataList());
            }
            try {
                data.setDanoEstimado(numberFormat.parse(etEstimadoDanos.getText().toString()).doubleValue());
            } catch (ParseException ex) {
                data.setDanoEstimado(0.0);
            }

        }

        foundVehiculo = data;

        getCompositeDisposable().add(terceroAutoService.modifyVehiculo(idAjuste, bundle.getLong("id"), data)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(TerceroAutoActivity.this, messageVehiculo);
                    idVehiculo = bundle.getLong("id");
                    id_auto = idVehiculo;
                    isSaveVehicle = true;
                    new GeneralSnackBar().largeSnackOk(TerceroAutoActivity.this, messageVehiculo);
                    btnSubirVehiculoFotos.setVisibility(VISIBLE);
                    ModuleAuto moduleAuto = new ModuleAuto(Long.toString(id_auto) + MODULO_TERCEROS + TIPO_VEHICULO, TERCEROS, VEHICULO, "", id_auto, idAjuste);
                    path = getImagePathAuto(moduleAuto);
                    if (!getAllImages(getBaseActivity(), path).isEmpty()) sendPhotosVehiculos();
                    isUploadedV = true;
                }, throwable -> {
                    isUploadedV = false;
                    Log.e(TAG, "Error al guardar datos del vehiculo " + id_auto + " del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoActivity.this, throwable);

                })
        );

    }

    @Override
    public void onDeleteClick(View view, int position) {
    }

    @Override
    public void OnItemOcupante(View view, int position) {
        Intent intent = new Intent(TerceroAutoActivity.this, TerceroAutoOcupanteActivity.class);
        intent.putExtra("id", getIntent().getExtras().getLong("id"));
        intent.putExtra("discriminator", "ocupante");
        intent.putExtra("data", getIntent().getExtras().getString("data"));
        intent.putExtra("idOccupant", ocupanteAdapter.getOcupante(position).getId());
        startActivity(intent);
    }

    @Override
    public void loadData() {

    }

    @Override
    public void onParteClick(View view, int position) {
    }

    @Override
    public void onBackPressed() {
        autoTercero = new VehiculoTercero();
        if (isUploadedV && conductorTercero.getIsUploaded()) {
            bus.post(autoTercero);
            TerceroAutoActivity.super.onBackPressed();
        } else {
            String message = "";
            if (!conductorTercero.getIsUploaded() && !isUploadedV) {
                message = getString(R.string.image_message_out);
            } else {
                if (!conductorTercero.getIsUploaded()) {
                    message = getString(R.string.message_vehicle_save);
                }
                if (!isUploadedV) {
                    message = getString(R.string.message_vehicle_save);
                }
            }
            DialogUtils.createAlert(this, "Salir",
                    message,
                    "Aceptar", "Cancelar",
                    (dialog, which) -> {
                        bus.post(autoTercero);
                        TerceroAutoActivity.super.onBackPressed();
                    },
                    (dialog, which) -> dialog.dismiss());
        }
    }

    @Override
    public void onCallbackItemId(int position, Long id) {
        deletePhotoVehiculo(id);
    }

    @Override
    public void onCallItemId(int position, Long id) {

    }

    @Override
    public void itemCheck(@Nullable Long id, boolean checked) {

    }

    @Override
    public void listChecks(@NotNull List<Long> list) {
        if (categoria != null) {
            getMoney(categoria.getCategoria(), list);
        } else {
            etEstimadoDanos.setText("$" + 0.00);
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        tipoVehiculo = !tipoVehiculos.isEmpty() ? tipoVehiculos.get(spVehiculoDatos.get(0).getSelectedItemPosition()).getClave() : null;
        armadora = tipoVehiculo != null && !armadorasList.isEmpty() ? armadorasList.get(spVehiculoDatos.get(1).getSelectedItemPosition()).getClave() : null;
        carroceria = armadora != null && !carroceriaList.isEmpty() ?
                carroceriaList.get(spVehiculoDatos.get(2).getSelectedItemPosition()).getClave() : null;
        modelo = carroceria != null && !modelos.isEmpty() ?
                modelos.get(spVehiculoDatos.get(3).getSelectedItemPosition()) : null;
        version = modelo != null && !versiones.isEmpty() ?
                versiones.get(spVehiculoDatos.get(4).getSelectedItemPosition()).getVersion() : null;

        switch (adapterView.getId()) {
            case R.id.spTipoVehiculoP:
                if (tipoVehiculo != null) {
                    downloadArmadora(tipoVehiculo);
                }
                break;
            case R.id.spArmadora:
                if (armadora != null) {
                    downloadCarroceria(tipoVehiculo, armadora);
                }
                break;
            case R.id.spCarroceria:
                if (carroceria != null) {
                    downloadModelos(tipoVehiculo, armadora, carroceria);
                }
                break;
            case R.id.spModeloP:
                if (modelo != null) {
                    downloadVersiones(tipoVehiculo, armadora, carroceria, modelo);
                }
                break;
            case R.id.spVersionP:
                if (version != null) {
                    downloadCategoria(tipoVehiculo, armadora, carroceria, modelo, version);
                }

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone.getText().toString().trim().length() < 10) {
                    phone.setError(ResourceUtils.getString("invalid_number"));
                    if (extension.getText().toString().trim().length() < 6) {
                        extension.setError(ResourceUtils.getString("extension_error"));
                    }
                } else {
                    dialog.dismiss();
                }
            }

        }

    }



    private void addFragment(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_example, fragment, "driver");
        transaction.commit();
    }

    private void addFragmentCamera(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraFragmentVehicle, fragment, Constants.TAG_THIRD_VEHICLE);
        transaction.commit();
    }

    private void addFragmentDamgeParts(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentDamagePartsT, fragment, Constants.TAG_DAMAGE);
        transaction.commit();
    }


    private void addFragmentDamgePartsPreexisting(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentDamagePartsPreexistingT, fragment, Constants.TAG_DAMAGEPREEXTISTENTE);
        transaction.commit();
    }


}