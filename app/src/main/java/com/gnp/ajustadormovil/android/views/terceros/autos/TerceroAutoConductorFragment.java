package com.gnp.ajustadormovil.android.views.terceros.autos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModuleAuto;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.TelefonoTerceroAdapter;
import com.gnp.ajustadormovil.android.views.commons.adapters.legacy.CorreoAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.commons.custom.ItemDecorator;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnTelefonoListener;
import com.gnp.ajustadormovil.android.views.commons.lesiones.GeneralInjuredFragment;
import com.google.gson.Gson;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_TERCEROS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_CONDUCTOR;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getAllImages;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getImagePathAuto;
import static com.gnp.ajustadormovil.android.utils.Constants.CONDUCTOR;
import static com.gnp.ajustadormovil.android.utils.Constants.TERCEROS;
import static com.gnp.ajustadormovil.android.views.commons.base.BaseActivity.getBaseActivity;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.isSaveDriver;

/**
 * Created by abraham on 04/05/18.
 */

public class TerceroAutoConductorFragment extends android.support.v4.app.Fragment implements OnTelefonoListener
        , OnItemRemovedListener, CallbackItemImage<Long>, TextWatcher {

    //views
    private View mView;
    private Unbinder unbinder;
    @BindView(R.id.spinnerHospital)
    Spinner spinnerHospital;
    @BindView(R.id.edtOtroHospital)
    EditText otroHospital;
    /* @BindView(R.id.tvOtroH)
     TextView titulo_otro_hospital;*/
    @BindView(R.id.spEstadoHospital)
    Spinner spEstadoHospital;
    /*@BindView(R.id.lesionado_container)
    LinearLayout lesion_container;*/
    @BindView(R.id.lesion_title)
    AppCompatTextView titulo_lesion;
    @BindViews({R.id.spTipoIdentificacion, R.id.spNivelLesion, R.id.spEstadoAutoTercero})
    List<Spinner> spTerceros;
    @BindViews({R.id.edtNombreConductorTercero, R.id.edtApPaConductor, R.id.edtApMaConductor})
    List<EditText> editTexts_terceros;
    @BindViews({R.id.rgSexo})
    List<RadioGroup> rgOptions;
    @BindViews({R.id.cbPermanente, R.id.cbAmbulancia})
    List<CheckBox> cbOptions;
    @BindView(R.id.cbOtro)
    CheckBox cbOtro;
    @BindView(R.id.tvNumIdentificacionTA)
    TextView numIdentificacion;
    @BindView(R.id.hospital_content)
    LinearLayout hospital_content;
    //@BindView(R.id.hospital_container)
    //7LinearLayout hospital_container;
    @BindView(R.id.rvEmailsTerceros)
    RecyclerView rvCorreos;
    @BindView(R.id.rvTelefonosTerceros)
    RecyclerView rvTelefonos;
    @BindView(R.id.dateNac)
    EditText dateNacimiento;
    @BindView(R.id.etAge)
    EditText edad;
    @BindView(R.id.dateVigencia)
    EditText dateVigencia;
    @BindView(R.id.title_vigencia)
    TextView title_vigencia;
    @BindView(R.id.spTipoLicencia)
    Spinner spinnerTipoLicencia;
    @BindView(R.id.licencia_container)
    LinearLayout licencia_content;
    @BindView(R.id.spStatus)
    Spinner spinnerStatus;
    @BindView(R.id.lesionado_content)
    LinearLayout lesionado_content;
    @BindView(R.id.btnuploadImages)
    Button btnUploadImagesConductor;
    @BindView(R.id.edtNumIdentificacion)
    EditText edtNumIdentificacion;
    @BindView(R.id.btnConductorTercerosGuardar)
    Button btnGuardar;
    @BindView(R.id.vigencia_container)
    LinearLayout vigencia_container;
    private CheckBox si, no;
    private Spinner spTipoTelefono;
    private EditText phone, extension;
    //String
    private String sexo = null;

    private String path;
    private String idAjuste;
    private String messageConductor;
    public static String idConductor;
    private String clave = "";
    public static final String TAG = "TAG";
    //List
    private List<Supervisoria> supervisorias = new ArrayList<>();
    private List<CatalogoClaveAmis> telefonosList = new ArrayList<>();
    private List<CatalogoClaveInfo> estadosList = new ArrayList<>();
    private List<CatalogoId> nivelesList = new ArrayList<>();
    private List<HospitalResumen> hospitales = new ArrayList<>();
    private List<CatalogoClave> identificacionesList = new ArrayList<>();
    //objects
    private Persona foundConductor = new Persona();
    //adapters
    private TelefonoTerceroAdapter telefonoTerceroAdapter;
    private CorreoAdapter adapter;
    //primitivos
    private boolean isUploaded = false;
    //Fragments
    private GeneralCameraFragment generalCameraFragment;
    private GeneralInjuredFragment generalInjuredFragment;
    //calendar
    Calendar calendar = Calendar.getInstance();
    //listener
    private CallbackItemImage listener;
    private Activity activity;
    private Boolean autoriza;
    private Spinner spParteID;
    private Dialog dialog;
    private Long id_auto;
    private CompositeDisposable compositeDisposable;
    private TerceroAutoInteractor terceroAutoService;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private Hospital hospital;
    private List<ImageGeneralModel> filesPhotos = new ArrayList<>();
    private EditText email;
    private String tipoLicencia;

    private List<CatalogoClave> tiposLicenciaList = new ArrayList<>();

    public static TerceroAutoConductorFragment newInstance() {
        return new TerceroAutoConductorFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        generalCameraFragment = new GeneralCameraFragment();
        addFragment(generalCameraFragment);
        generalInjuredFragment = new GeneralInjuredFragment();
        addFragmentInjuredParts(generalInjuredFragment);
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        listener = this;
        isSaveDriver = false;
        if (getActivity().getIntent() != null) {
            id_auto = getActivity().getIntent().getExtras().getLong("id");
            idConductor = Long.toString(getArguments().getLong("id"));
        }
        if (getArguments().getString("foundConductor") != null) {
            foundConductor = new Gson().fromJson(getArguments().getString("foundConductor"), Persona.class);
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.content_conductor_autos_terceros, container, false);
        unbinder = ButterKnife.bind(this, mView);
        getDatosConductorTercero();
        return mView;
    }

    private void getDatosConductorTercero() {
        getBaseActivity().getCompositeDisposable().add(getCatalogs()
                .doOnSubscribe(it -> LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .subscribe(() -> {
                    initView();
                    updateDataConductor();
                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                }));
    }


    private void initView() {
        initLayoutVisibility();
        initRecyvelrViews();
        setEvents();
        setSpinners();
        setChecks();
    }


    private void initRecyvelrViews() {
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new CorreoAdapter(this);

        RecyclerView.ItemDecoration dividerItemDecorationID = new
                ItemDecorator(ContextCompat.getDrawable(activity.getApplicationContext(),
                R.drawable.line_divider));
        rvCorreos.setLayoutManager(manager);
        telefonoTerceroAdapter = new TelefonoTerceroAdapter(this);
        LinearLayoutManager managerTelefono = new LinearLayoutManager(activity);
        managerTelefono.setOrientation(LinearLayoutManager.VERTICAL);
        rvTelefonos.setLayoutManager(managerTelefono);
        rvTelefonos.setAdapter(telefonoTerceroAdapter);
        rvCorreos.setAdapter(adapter);
    }

    private void initLayoutVisibility() {
        //hospital_container.setVisibility(View.VISIBLE);
        hospital_content.setVisibility(View.VISIBLE);
        //lesion_container.setVisibility(GONE);
        titulo_lesion.setVisibility(GONE);
        lesionado_content.setVisibility(GONE);
        btnUploadImagesConductor.setVisibility(GONE);
        mView.findViewById(R.id.title_otro).setVisibility(View.INVISIBLE);
    }

    public boolean getIsUploaded() {
        return isUploaded;
    }

    private void setChecks() {
        rgOptions.get(0).setOnCheckedChangeListener((radioGroup, id) -> {
            if (id == R.id.rbMasculino) {
                sexo = "M";
            }
            if (id == R.id.rbFemenino) {
                sexo = "F";
            }
            Log.d(TAG, "onCheckedChanged: " + sexo);
        });

        cbOptions.get(0).setOnCheckedChangeListener((compoundButton, b) -> {
            if (!b) {
                vigencia_container.setVisibility(View.VISIBLE);
                title_vigencia.setVisibility(View.VISIBLE);
            } else {
                vigencia_container.setVisibility(View.INVISIBLE);
                dateVigencia.setText("");
                dateVigencia.requestFocus();
                title_vigencia.setVisibility(View.INVISIBLE);
            }
        });

        cbOptions.get(1).setOnCheckedChangeListener((compoundButton, b) -> {
        });

        cbOtro.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            if (isChecked) {
                spinnerHospital.setVisibility(GONE);
                mView.findViewById(R.id.title_hospital).setVisibility(GONE);
                mView.findViewById(R.id.title_otro_content).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.title_otro).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.edtOtroHospital).setVisibility(View.VISIBLE);
                Utils.showKeyboard(otroHospital, activity);
            } else {
                spinnerHospital.setVisibility(View.VISIBLE);
                mView.findViewById(R.id.title_hospital).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.title_otro).setVisibility(View.INVISIBLE);
                mView.findViewById(R.id.title_otro_content).setVisibility(GONE);
                mView.findViewById(R.id.edtOtroHospital).setVisibility(View.INVISIBLE);
                otroHospital.setText("");
                Utils.hideKeyboard(otroHospital, activity);
            }
        }));

    }

    private void setEvents() {

        editTexts_terceros.get(0).addTextChangedListener(this);
        editTexts_terceros.get(1).addTextChangedListener(this);
        editTexts_terceros.get(2).addTextChangedListener(this);

    }

    private void downloadTiposLicencia(String estado) {
        compositeDisposable.add(siniestrosInteractor.getTiposLicencia(estado)
                .subscribe(tiposLicencia -> {
                    tiposLicenciaList = tiposLicencia;
                    AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getBaseActivity()
                            , R.layout.sp_little_item, tiposLicenciaList, CatalogoClave::getNombre);
                    spinnerTipoLicencia.setAdapter(adapter);
                    if (foundConductor != null && foundConductor.getTipoLicencia() != null) {
                        spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, foundConductor.getTipoLicencia()));
                    }
                    spinnerTipoLicencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            tipoLicencia = tiposLicenciaList.get(position).getClave();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                }, throwable -> {
                    spinnerTipoLicencia.setAdapter(null);
                    Log.e(TAG, "Error al descargar los colores", throwable);
                }));

    }

    private void setSpinners() {
        spEstadoHospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadHospitales(supervisorias.get(spEstadoHospital.getSelectedItemPosition()).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> estatus = Arrays.asList(getResources().getStringArray(R.array.spinner_status));
        AdapterSpinnerGeneral<String> spinnerArrayAdapter = new AdapterSpinnerGeneral<>(getActivity()
                , R.layout.spinner_item, estatus, Object::toString);
        spinnerStatus.setAdapter(spinnerArrayAdapter);
        spTerceros.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (identificacionesList != null && identificacionesList.size() > 0 && identificacionesList.get(i) != null && identificacionesList.get(i).getClave() != null) {
                    if (!spTerceros.get(0).getSelectedItem().toString().equals("Licencia de Conducir")) {
                        licencia_content.setVisibility(GONE);
                    } else {
                        licencia_content.setVisibility(View.VISIBLE);
                    }
                    if (identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave() != null) {
                        switch (identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave()) {
                            case "LC":
                                numIdentificacion.setText(getText(R.string.identification_number));
                                mView.findViewById(R.id.licencia_container).setVisibility(View.VISIBLE);
                                break;
                            case "CV":
                                numIdentificacion.setText("Clave de elector");
                                edtNumIdentificacion.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        if (spTerceros.get(0).getSelectedItem().toString().equals("Credencial para Votar") && !s.toString().matches("^[A-Z]{6}[0-9]{8}[0-9A-Z]{4}$")) {
                                            edtNumIdentificacion.setError("ingrese una credencial válida");
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {

                                    }
                                });
                                break;
                            default:
                                numIdentificacion.setText(getText(R.string.identification_number));
                                mView.findViewById(R.id.licencia_container).setVisibility(GONE);
                                break;

                        }
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spTerceros.get(1).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
                    //lesion_container.setVisibility(GONE);
                    titulo_lesion.setVisibility(GONE);
                    lesionado_content.setVisibility(GONE);
                } else {
                    //lesion_container.setVisibility(View.VISIBLE);
                    titulo_lesion.setVisibility(View.VISIBLE);
                    lesionado_content.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spTerceros.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                downloadTiposLicencia(estadosList.get(i).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    private Completable getCatalogs() {
        return Completable.mergeArrayDelayError(
                downloadEstados(),
                downloadNivelLesiones(),
                downloadIdentificaciones(),
                downloadSupervisorias()

        );


    }

    private void downloadTelefonos() {
        compositeDisposable.add(siniestrosInteractor.getTiposTelefono()
                .subscribe(tiposTelefonosResponse -> {
                    telefonosList = tiposTelefonosResponse;
                    showDialogAddPhone();
                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                    Log.e(TAG, "Error al descargar tipos de telefonos", throwable);

                }));

    }

    private Completable downloadNivelLesiones() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getNivelLesion()
                    .subscribe(nivelLesionResponse -> {
                        nivelesList = nivelLesionResponse;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(activity.getApplicationContext()
                                , R.layout.sp_little_item, nivelesList, CatalogoId::getNombre);
                        spTerceros.get(1).setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spTerceros.get(1).setAdapter(null);
                        Log.e(TAG, "Error al descargar niveles de lesion", throwable);
                    })
            );
        });
    }


    private Completable downloadEstados() {
        return Completable.create(e -> {
            compositeDisposable.add(catInteractor.getEstados()
                    .subscribe(estados -> {
                        estadosList = estados;
                        AdapterSpinnerGeneral<CatalogoClaveInfo> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, estadosList, CatalogoClaveInfo::getNombre);
                        spTerceros.get(2).setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spTerceros.get(2).setAdapter(null);
                        Log.e(TAG, "Error al descargar estados de licencia ", throwable);
                    })
            );
        });


    }


    private void getSupervisoria(Long id) {

        compositeDisposable.add(siniestrosInteractor.getHospital(id)
                .subscribe(supervisoriasResponse -> {
                    hospital = supervisoriasResponse;
                    setHospital();
                }, throwable -> {
                    Log.e(TAG, "Error al descargar supervisoria", throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                }));

    }


    private void setHospital() {
        for (int i = 0; i < supervisorias.size(); i++) {
            if (supervisorias.get(i).getClave().equals(hospital.getSupervisoria())) {
                Log.d("", "position" + i);
                spEstadoHospital.setSelection(i);
                downloadHospitales(hospital.getSupervisoria());
                break;
            }
        }
    }

    private void downloadHospitales(String supervisora) {

        compositeDisposable.add(siniestrosInteractor.getHospitales(supervisora)
                .subscribe(hospitalesResponse -> {
                    hospitales = hospitalesResponse;
                    AdapterSpinnerGeneral<HospitalResumen> adapter = new AdapterSpinnerGeneral<>(activity.getApplicationContext()
                            , R.layout.sp_little_item, hospitales, HospitalResumen::getNombre);
                    spinnerHospital.setAdapter(adapter);
                    if (hospital != null) {
                        for (int j = 0; j < hospitales.size(); j++) {
                            if (hospitales.get(j).getNombre().equals(hospital.getNombre())) {
                                spinnerHospital.setSelection(j);
                                break;
                            }
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar hospitales", throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                }));
    }


    @OnClick(R.id.dateNac)
    public void addNacimiento() {
        dateNacimiento.setError(null);
        showDatePikerDialog();
    }

    @OnClick(R.id.dateVigencia)
    public void showDateVigencia() {
        dateVigencia.setError(null);
        showDatePikerDialogVigencia();
    }

    @SuppressLint("NewApi")
    private void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateVigencia.setText(date);
        });
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    private void showDatePikerDialog() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateNacimiento.setText(date);
            dateNacimiento.setError(null);
            if (age.contains("-")) {
                edad.setText("");
            } else {
                edad.setText(age);
            }
        });
        datePickerFragment.show(getActivity().getSupportFragmentManager(), "dataPickerBirthday");
    }

    private String calculateAge(int year, int month, int day) {
        LocalDate birthdate = new LocalDate(year, month, day);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);

        return String.valueOf(age.getYears());
    }

    private Completable downloadIdentificaciones() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getTiposIdentificacion()
                    .subscribe(identificacionesResponse -> {
                        identificacionesList = identificacionesResponse;
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(activity.getApplicationContext()
                                , R.layout.sp_little_item, identificacionesList, CatalogoClave::getNombre);
                        spTerceros.get(0).setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spTerceros.get(0).setAdapter(null);
                        Log.e(TAG, "Error al descargar tipos de identificaciones", throwable);
                    })
            );
        });


    }


    private Completable downloadSupervisorias() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getSupervisorias()
                    .subscribe(supervisoriasResponse -> {
                        supervisorias = supervisoriasResponse;
                        AdapterSpinnerGeneral<Supervisoria> adapter = new AdapterSpinnerGeneral<>(getContext()
                                , R.layout.sp_little_item, supervisorias, Supervisoria::getNombre);
                        spEstadoHospital.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spEstadoHospital.setAdapter(null);
                        Log.e(TAG, "Error al descargar supervisorias", throwable);
                    })
            );

        });


    }


    public void deletePhotoConductor(long id) {
        compositeDisposable.add(terceroAutoService.deleteFotografiaConductor(idAjuste, getArguments().getLong("id"), id)
                .subscribe(() ->
                                new GeneralSnackBar().largeSnackOk(activity, "Se elimino correctamente la fotografia del conductor")
                        , throwable -> {
                            Log.e(TAG, "Error al eliminar imagenes de tercero conductor: " + id_auto + " del ajuste " + idAjuste, throwable);
                            new GeneralSnackBar().largeSnackError(activity, throwable);
                        }));
    }

    @OnClick(R.id.btnuploadImages)
    public void uploadImagesConductor() {
        List<ImageGeneralModel> imageList = generalCameraFragment.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        ImageUtils imageUtils = new ImageUtils();
        if (!images.isEmpty()) {
            int size = images.size();
            btnUploadImagesConductor.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            compositeDisposable.add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> terceroAutoService.addFotografiaConductor(idAjuste, id_auto, file).toObservable(), false, 1)
                    .doOnEach(fotografias -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        }
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir la imagen", throwable);
                        return new Fotografia();
                    })
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        boolean ok = true;
                        for (Fotografia fotografia : fotografias) {
                            ok &= fotografia.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(activity, getResources().getString(R.string.imagenes_upload_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(activity, getResources().getString(R.string.imagenes_upload_error));
                        }
                        btnUploadImagesConductor.setEnabled(true);
                        imageUtils.renameImageUpload(imageList);
                        imageUtils.deleteImage(getBaseActivity(), imageList);
                        filesPhotos.clear();
                        getPhotosConductor();
                    }, throwable -> {
                        btnUploadImagesConductor.setEnabled(true);
                        Log.e(TAG, "Error al subir imagenes de conductor tercero " + id_auto + " del ajuste: " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(activity, throwable);

                    }));
        } else {
            new GeneralSnackBar().largeSnackError(activity, getString(R.string.image_void));
            btnUploadImagesConductor.setEnabled(true);
        }
    }

    private void getPhotosConductor() {
        compositeDisposable.add(terceroAutoService.getFotografiasConductor(idAjuste, id_auto)
                .subscribe(fotoResponses -> {
                    btnUploadImagesConductor.setEnabled(true);
                    for (Fotografia photo : fotoResponses) {
                        filesPhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                    }
                    generalCameraFragment.updateImageList(filesPhotos);
                }, throwable -> {
                    Log.e(TAG, "Error al obtener imagenes de conductor tercero " + id_auto + " del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);
                }));
    }

    @OnClick({R.id.btnConductorTercerosGuardar, R.id.btnVerTelefonos, R.id.btnAgregarCorreo})
    public void showDialogOptions(View v) {
        switch (v.getId()) {
            case R.id.btnVerTelefonos:
                downloadTelefonos();
                break;
            case R.id.btnAgregarCorreo:
                MaterialDialog builder = new MaterialDialog.Builder(getActivity())
                        .title("Agregar correo electrónico")
                        .customView(R.layout.dialog_add_email, true)
                        .positiveText("Aceptar")
                        .onPositive((dialog, which) -> {

                        })
                        .negativeText("Cancelar")
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .build();

                email = (EditText) builder.findViewById(R.id.edtCorreoConductor);
                builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
                MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
                cancelar.setAllCaps(false);
                aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
                aceptar.setTextColor(Color.WHITE);
                aceptar.setAllCaps(false);
                aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_email));
                builder.show();
                break;
            case R.id.btnConductorTercerosGuardar:
                if (validFormConductor()) {
                    Persona conductor;
                    if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
                        conductor = new Persona();
                        conductor.setNombre(editTexts_terceros.get(0).getText().toString());
                        conductor.setApellidoPaterno(editTexts_terceros.get(1).getText().toString());
                        conductor.setApellidoMaterno(editTexts_terceros.get(2).getText().toString());
                        if ("".equals(conductor.getApellidoMaterno())) {
                            conductor.setApellidoMaterno(null);
                        }
                        conductor.setCorreos(adapter.getCorreos());
                        conductor.setTelefonos(telefonoTerceroAdapter.getTelefonos());
                        conductor.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString()));
                        conductor.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
                        conductor.setLesiones(generalInjuredFragment.getDataList());
                        conductor.setAutorizaTratamiento(autoriza);
                        conductor.setTipoIdentificacion(identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave());
                        if (identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave().equals("LC")) {
                            conductor.setEstadoLicencia(estadosList.get(spTerceros.get(2).getSelectedItemPosition()).getClave());
                            if (!cbOptions.get(0).isChecked()) {
                                conductor.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString()));
                                conductor.setLicenciaPermanente(false);
                                conductor.setTipoLicencia(tipoLicencia);
                            } else {
                                conductor.setFechaFinLicencia(null);
                                conductor.setLicenciaPermanente(true);
                                conductor.setTipoLicencia(tipoLicencia);
                            }

                        }
                        conductor.setRequiereAmbulancia(true);
                        if (cbOtro.isChecked()) {
                            conductor.setHospital(null);
                            conductor.setOtroHospital(otroHospital.getText().toString());
                        } else {
                            if (hospitales != null && hospitales.size() != 0) {
                                conductor.setHospital(hospitales.get(spinnerHospital.getSelectedItemPosition()).getId());
                            } else {
                                conductor.setHospital(null);
                            }
                        }
                        conductor.setSexo(sexo);
                        conductor.setNivelLesion(nivelesList.get(spTerceros.get(1).getSelectedItemPosition()).getId());
                        conductor.setNumeroIdentificacion(edtNumIdentificacion.getText().toString());
                    } else {
                        conductor = new Persona();
                        conductor.setNombre(editTexts_terceros.get(0).getText().toString().trim());
                        conductor.setApellidoPaterno(editTexts_terceros.get(1).getText().toString().trim());
                        conductor.setApellidoMaterno(editTexts_terceros.get(2).getText().toString().trim());
                        conductor.setCorreos(adapter.getCorreos());
                        conductor.setRequiereAmbulancia(false);
                        conductor.setNivelLesion(null);
                        conductor.setTelefonos(telefonoTerceroAdapter.getTelefonos());
                        conductor.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString().trim()));
                        conductor.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString().trim()));
                        conductor.setSexo(sexo);
                        conductor.setAutorizaTratamiento(autoriza);
                        conductor.setTipoIdentificacion(identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave());
                        if (identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave().equals("LC")) {
                            conductor.setEstadoLicencia(estadosList.get(spTerceros.get(2).getSelectedItemPosition()).getClave());
                            if (!cbOptions.get(0).isChecked()) {
                                conductor.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString().trim()));
                                conductor.setLicenciaPermanente(false);
                                conductor.setTipoLicencia(tipoLicencia);
                            } else {
                                conductor.setFechaFinLicencia(null);
                                conductor.setLicenciaPermanente(true);
                                conductor.setTipoLicencia(tipoLicencia);
                            }
                        }
                        conductor.setNumeroIdentificacion(edtNumIdentificacion.getText().toString().trim());
                    }
                    foundConductor = conductor;
                    if (autoriza == null) {
                        btnGuardar.setEnabled(true);
                        Toast.makeText(activity, ResourceUtils.getString("aviso_privacidad_error"), Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (otroHospital.getText().toString().equals("") && cbOtro.isChecked()) {
                        btnGuardar.setEnabled(true);
                        otroHospital.requestFocus();
                        otroHospital.setError(getResources().getString(R.string.volante_coberturas_required));
                        return;
                    }

                    compositeDisposable.add(terceroAutoService.modifyConductor(idAjuste, getArguments().getLong("id"), foundConductor)
                            .subscribe(() -> {
                                new GeneralSnackBar().largeSnackOk(activity, "Se guardó correctament el conductor");
                                btnUploadImagesConductor.setVisibility(View.VISIBLE);
                                isUploaded = true;
                                isSaveDriver = true;
                                btnGuardar.setEnabled(true);
                                idConductor = Integer.toString(getArguments().getInt("id"));
                                ModuleAuto moduleAuto = new ModuleAuto(id_auto.toString() + MODULO_TERCEROS + TIPO_CONDUCTOR, TERCEROS, CONDUCTOR, "", id_auto, idAjuste);
                                path = getImagePathAuto(moduleAuto);
                                if (!getAllImages(activity, path).isEmpty())
                                    uploadImagesConductor();
                                //new GeneralSnackBar().largeSnackOk(activity, messageConductor);


                            }, throwable -> {
                                btnGuardar.setEnabled(true);
                                isUploaded = false;
                                btnGuardar.setEnabled(true);
                                Log.e(TAG, "Error al guardar datos del  conductor tercero " + id_auto + " del ajuste: " + idAjuste, throwable);
                                new GeneralSnackBar().largeSnackError(activity, throwable);

                            }));

                    break;
                } else btnGuardar.setEnabled(true);
        }
    }

    private boolean validFormConductor() {
        boolean isRequestFocus = false;
        if (editTexts_terceros.get(0).getText().toString().trim().length() <= 0) {
            editTexts_terceros.get(0).setError(getString(R.string.mandatory_field));
            editTexts_terceros.get(0).requestFocus();
            isRequestFocus = true;
        }
        if (editTexts_terceros.get(1).getText().toString().trim().length() <= 0) {
            editTexts_terceros.get(1).setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                editTexts_terceros.get(1).requestFocus();
                isRequestFocus = true;
            }
        }
        if (dateNacimiento.getText().toString().trim().length() <= 0) {
            dateNacimiento.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                dateNacimiento.requestFocus();
                isRequestFocus = true;
            }
        }
        if (!identificacionesList.isEmpty() && identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave().equals("LC")) {
            if (!cbOptions.get(0).isChecked()) {
                if (dateVigencia.getText().toString().trim().length() <= 0) {
                    dateVigencia.setError(getString(R.string.mandatory_field));
                    dateVigencia.requestFocus();
                    if (!isRequestFocus) {
                        isRequestFocus = true;
                    }
                }
            }
        }
        if (edtNumIdentificacion.getText().toString().trim().length() <= 0) {
            edtNumIdentificacion.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                edtNumIdentificacion.requestFocus();
                isRequestFocus = true;
            }
        }
        if (rgOptions.get(0).getCheckedRadioButtonId() == -1) {
            ToastUtils.toastShort("Debe seleccionar un género por favor");
        }
        if (telefonoTerceroAdapter.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(activity, ResourceUtils.getString("telefono_empty"), Toast.LENGTH_LONG).show();
        }
        if (adapter.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(activity, ResourceUtils.getString("correo_empty"), Toast.LENGTH_LONG).show();
        }

        if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
            if (generalInjuredFragment.getDataList().size() == 0) {
                isRequestFocus = true;
                Toast.makeText(activity, ResourceUtils.getString("lesion_empty"), Toast.LENGTH_LONG).show();
            }
        }
        return !isRequestFocus;
    }


    public void updateDataConductor() {
        if (foundConductor != null && activity != null) {
            isSaveDriver = true;
            btnUploadImagesConductor.setVisibility(View.VISIBLE);
            ModuleAuto moduleAuto = new ModuleAuto(id_auto.toString() + MODULO_TERCEROS + TIPO_CONDUCTOR, TERCEROS, CONDUCTOR, "", id_auto, idAjuste);
            path = getImagePathAuto(moduleAuto);
            if (path != null) {
                filesPhotos.addAll(getAllImages(activity, path));
                generalCameraFragment.setImageList(filesPhotos);
                PersistenciaPath.showMessageSizeImages(activity, filesPhotos);
            }

            messageConductor = "Se actualizó correctamente el conductor";
            if (foundConductor.getNombre() != null) {
                editTexts_terceros.get(0).setText(foundConductor.getNombre() != null ? foundConductor.getNombre() : "");
            }
            if (foundConductor.getApellidoPaterno() != null) {
                editTexts_terceros.get(1).setText(foundConductor.getApellidoPaterno() != null ? foundConductor.getApellidoPaterno() : "");
            }
            if (foundConductor.getApellidoMaterno() != null) {
                editTexts_terceros.get(2).setText(foundConductor.getApellidoMaterno() != null ? foundConductor.getApellidoMaterno() : "");
            }
            if (!"M".equals(foundConductor.getSexo())) {
                rgOptions.get(0).check(R.id.rbFemenino);
            } else {
                rgOptions.get(0).check(R.id.rbMasculino);
            }
            if (foundConductor.getFechaNacimiento() != null) {
                dateNacimiento.setText(DateUtils.date(foundConductor.getFechaNacimiento()));
                int years = Years
                        .yearsBetween(LocalDate.fromDateFields(foundConductor.getFechaNacimiento()), LocalDate.now())
                        .getYears();
                edad.setText(Integer.toString(years));
            }
            if (foundConductor.getTipoIdentificacion() != null) {
                spTerceros.get(0).setSelection(SpinnerUtils.getPosition(spTerceros.get(0), foundConductor.getTipoIdentificacion()));
                if (identificacionesList.get(spTerceros.get(0).getSelectedItemPosition()).getClave().equals("LC")) {
                    licencia_content.setVisibility(View.VISIBLE);
                }
            }

            if (foundConductor.getNumeroIdentificacion() != null) {
                edtNumIdentificacion.setText(foundConductor.getNumeroIdentificacion());
            }

            if (!foundConductor.getTelefonos().isEmpty())
                isUploaded = true;
            for (Telefono tel : foundConductor.getTelefonos()) {
                telefonoTerceroAdapter.add(tel);
            }

            if (!foundConductor.getCorreos().isEmpty())
                for (String mail : foundConductor.getCorreos()) {
                    adapter.add(mail);
                }
            try {
                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, foundConductor.getCondicion().name()));
            } catch (Exception ex) {
            }
            if (foundConductor.getNivelLesion() != null && foundConductor.getNivelLesion() != 0) {
                spTerceros.get(1).setSelection(SpinnerUtils.getPosition(spTerceros.get(1), foundConductor.getNivelLesion()));
            }
            if (foundConductor.getAutorizaTratamiento() != null) {
                setAutoriza(foundConductor.getAutorizaTratamiento());
            }
            if (foundConductor.getEstadoLicencia() != null) {
                spTerceros.get(2).setSelection(SpinnerUtils.getPosition(spTerceros.get(2), foundConductor.getEstadoLicencia()));
                downloadTiposLicencia(foundConductor.getEstadoLicencia());

            }

            if (!foundConductor.getFotografias().isEmpty()) {
                getPhotosConductor();
                generalCameraFragment.setCallBackItem(listener);
                btnUploadImagesConductor.setVisibility(View.VISIBLE);
            }
            if (foundConductor.getRequiereAmbulancia() != null) {
                cbOptions.get(1).setChecked(foundConductor.getRequiereAmbulancia());
            }
            if (foundConductor.getOtroHospital() == null) {
                cbOtro.setChecked(false);
                if (foundConductor.getHospital() != null) {
                    mView.findViewById(R.id.edtOtroHospital).setVisibility(View.INVISIBLE);
                    mView.findViewById(R.id.title_otro_content).setVisibility(GONE);
                    getSupervisoria(foundConductor.getHospital());
                }
            } else {
                cbOtro.setChecked(true);
                Utils.hideKeyboard(otroHospital, activity);
                otroHospital.setText(foundConductor.getOtroHospital());
                mView.findViewById(R.id.spinnerHospital).setVisibility(GONE);

            }
            if (foundConductor.getLicenciaPermanente() != null && foundConductor.getLicenciaPermanente()) {
                cbOptions.get(0).setChecked(true);
            } else {
                cbOptions.get(0).setChecked(false);
                if (foundConductor.getFechaFinLicencia() != null) {
                    dateVigencia.setText(DateUtils.date(foundConductor.getFechaFinLicencia()));
                }
            }

            if (foundConductor.getTipoLicencia() != null) {
                spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, foundConductor.getTipoLicencia()));
            }
            if (foundConductor.getLesiones() != null) {
                generalInjuredFragment.loadDataList(foundConductor.getLesiones());
            }

            if (foundConductor.getLicenciaPermanente() != null && foundConductor.getLicenciaPermanente()) {
                cbOptions.get(0).setChecked(true);
            }
            //updateOcupantes();
        } else {
            messageConductor = "Se agregó correctamente el conductor";
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        generalCameraFragment.onActivityResult(requestCode, resultCode, data);
    }


    private void showDialogAddPhone() {
        MaterialDialog builder = new MaterialDialog.Builder(activity)
                .title("Agregar teléfono")
                .customView(R.layout.dialog_add_phone, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        spTipoTelefono = (Spinner) builder.findViewById(R.id.spinnerTipoTelefono);
        AdapterSpinnerGeneral<CatalogoClaveAmis> adapter = new AdapterSpinnerGeneral<>(activity.getApplicationContext(), R.layout.spinner_item, telefonosList, CatalogoClave::getNombre);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoTelefono.setAdapter(adapter);
        phone = (EditText) builder.findViewById(R.id.etPhone);
        TextView tituloExtencion = (TextView) builder.findViewById(R.id.titleExtencion);
        phone.requestFocus();
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        extension = (EditText) builder.findViewById(R.id.etExtension);
        spTipoTelefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                clave = telefonosList.get(i).getClave();
                if (((CatalogoClave) spTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                    extension.setVisibility(View.VISIBLE);
                    tituloExtencion.setVisibility(View.VISIBLE);
                } else {
                    extension.setVisibility(GONE);
                    tituloExtencion.setVisibility(GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_phone));
        builder.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDeleteClick(View view, int position) {
        MaterialDialog deletePhoneBuilder = new MaterialDialog.Builder(activity)
                .title("Eliminar número...")
                .content("¿Desea eliminar este número?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    telefonoTerceroAdapter.remove(telefonoTerceroAdapter.getTelefono(position));
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        MDButton aceptar = deletePhoneBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = deletePhoneBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(deletePhoneBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        deletePhoneBuilder.show();
    }


    @OnClick(R.id.btnAvisoPrivacidad)
    public void showAvisoPrivacidad() {
        MaterialDialog mBuilder = new MaterialDialog.Builder(activity)
                .title("Aviso de privacidad")
                .customView(R.layout.dialog_notice_of_privacy, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    if (autoriza != null) {
                        setAutoriza(autoriza);
                    }
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        si = (CheckBox) mBuilder.findViewById(R.id.cbPrivacidadSi);
        no = (CheckBox) mBuilder.findViewById(R.id.cbPrivacidadNo);
        if (autoriza != null) {
            if (autoriza) {
                si.setChecked(true);
                no.setChecked(false);
            } else {
                si.setChecked(false);
                no.setChecked(true);
            }
        }
        si.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(true);
                no.setChecked(false);
                autoriza = true;
            } else {
                no.setChecked(true);
                si.setChecked(false);
                autoriza = false;
            }
        });
        no.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(false);
                no.setChecked(true);
                autoriza = false;
            } else {
                no.setChecked(false);
                si.setChecked(true);
                autoriza = true;
            }
        });

        MDButton aceptar = mBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = mBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(mBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        mBuilder.show();
    }

    private void setAutoriza(boolean autoriza) {
        this.autoriza = autoriza;
    }

    @Override
    public void onItemClick(View view, int position) {
        MaterialDialog deleteMailBuilder = new MaterialDialog.Builder(activity)
                .title("Eliminar correo...")
                .content("¿Desea eliminar este correo?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapter.remove(adapter.getCorreo(position));
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        MDButton aceptar = deleteMailBuilder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = deleteMailBuilder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(deleteMailBuilder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        deleteMailBuilder.show();
    }

    @OnClick(R.id.btnSolicitarAmbulancia)
    public void callAmbulancia() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_CALL_PERMISSION)) {
                call();
            } else {
                RequestPermissions.requestPermission(RequestPermissions.REQUEST_CALL_PERMISSION, activity);
            }
        } else {
            call();
        }
    }

    @SuppressLint("MissingPermission")
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Utils.NUMBER_AMBULANCE));
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    private void addFragmentInjuredParts(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentInjuredPartsDriverTerceros, fragment, "injuredParts");
        transaction.commit();
    }

    private void addFragment(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getBaseActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraFragmentDriver, fragment, Constants.TAG_THIRD_DRIVER);
        transaction.commit();
    }

    @Override
    public void onCallbackItemId(int position, Long id) {
        deletePhotoConductor(id);
    }

    @Override
    public void onCallItemId(int position, Long id) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String nombre = editTexts_terceros.get(0).getText().toString().trim();
        if (!validateText(nombre)) {
            editTexts_terceros.get(0).setError(getString(R.string.message_character_special));
        }
        String apellidoP = editTexts_terceros.get(1).getText().toString().trim();

        if (!validateText(apellidoP)) {
            editTexts_terceros.get(1).setError(getString(R.string.message_character_special));
        }
        String apellidoM = editTexts_terceros.get(2).getText().toString().trim();

        if (!validateText(apellidoM)) {
            editTexts_terceros.get(2).setError(getString(R.string.message_character_special));
        }
    }

    private boolean validateText(String text) {
        return text.matches("[a-zA-ZÀ-ÿ 'ñÑ]*");

    }


    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone.getText().toString().trim().length() < 10) {
                    phone.setError(ResourceUtils.getString("invalid_number"));
                    if (spTipoTelefono.getSelectedItem().toString().equals("Trabajo")) {
                        if (extension.getText().toString().trim().length() < 6) {
                            extension.setError(ResourceUtils.getString("extension_error"));
                        }
                    }
                } else {
                    Telefono telefono = new Telefono(phone.getText().toString(), clave, extension.getText().toString().equals("") ? null : extension.getText().toString());
                    telefonoTerceroAdapter.add(telefono);
                    dialog.dismiss();
                }
            } else {
                if (email.getText().toString().equals("")) {
                    email.setError("No deje vacio este campo");
                    return;
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError("Coloque una dirección de correo correcta");
                    return;
                } else {
                    adapter.add(email.getText().toString());
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        }

    }


}
