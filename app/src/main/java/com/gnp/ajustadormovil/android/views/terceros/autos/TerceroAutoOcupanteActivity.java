package com.gnp.ajustadormovil.android.views.terceros.autos;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModuleAuto;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.PosicionOcupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.FormatStrings;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.CorreoAdapter;
import com.gnp.ajustadormovil.android.views.commons.adapters.PhoneAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.gnp.ajustadormovil.android.views.commons.lesiones.GeneralInjuredFragment;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmList;

import static android.view.View.GONE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_TERCEROS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_AUTO;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_OCUPANTE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getAllImages;
import static com.gnp.ajustadormovil.android.utils.Constants.OCUPANTE;
import static com.gnp.ajustadormovil.android.utils.Constants.TERCEROS;
import static com.gnp.ajustadormovil.android.views.terceros.autos.TerceroAutoActivity.idAuto;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.idOcupantes;

public class TerceroAutoOcupanteActivity extends BaseActivity implements OnToolbarListener,
        OnItemRemovedListener, CallbackItemImage<Long>, TextWatcher {

    //Views
    @BindView(R.id.toolbar_ocupantes)
    Toolbar mToolbar;
    @BindView(R.id.etFirstName)
    EditText firstName;
    @BindView(R.id.etLastName)
    EditText lastName;
    @BindView(R.id.etMotherLastName)
    EditText motherLastName;
    @BindView(R.id.btnuploadImages)
    Button uploadImages;
    @BindView(R.id.rbMasculino)
    RadioButton rbGender;
    @BindView(R.id.tvNumIdentificacionOA)
    TextView numIdentificacionOA;
    @BindView(R.id.dateNac)
    EditText dateNacimiento;
    @BindView(R.id.etAge)
    EditText etAge;
    @BindView(R.id.container_ocupante)
    RelativeLayout layout;
    @BindView(R.id.dateVigencia)
    EditText dateVigencia;
    @BindView(R.id.etNumeroIdentificacion)
    EditText numeroIdentificacion;
    @BindView(R.id.spinnerIdentificacion)
    Spinner spinnerIdentificacion;
    @BindView(R.id.spinnerEstadoEmisor)
    Spinner spinnerEstadoEmisor;
    @BindView(R.id.spinnerTipoLicencia)
    Spinner spinnerTipoLicencia;
    @BindView(R.id.spinnerStatus)
    Spinner spinnerStatus;
    @BindView(R.id.spinnerNivelLesion)
    Spinner spinnerNivelLesion;
    @BindView(R.id.recyclerEmail)
    RecyclerView recyclerEmail;
    @BindView(R.id.recyclerPhone)
    RecyclerView recyclerPhone;
    @BindView(R.id.cbPermanente)
    CheckBox checkBoxPermanente;
    @BindView(R.id.cbAmbulancia)
    CheckBox cbAmbulancia;
    @BindView(R.id.cbAuditivo)
    CheckBox cbAuditivo;
    @BindView(R.id.cbDentadura)
    CheckBox cbDentadura;
    @BindView(R.id.cbLentes)
    CheckBox cbLentes;
    @BindView(R.id.cbProtesis)
    CheckBox cbProtesis;
    @BindView(R.id.recycler_images)
    RecyclerView recyclerImages;
    @BindView(R.id.spinnerHospital)
    Spinner spinnerHospital;
    @BindView(R.id.edtOtroHospital)
    EditText etOtroHospital;
    @BindView(R.id.tvOther)
    TextView tvOtro;
    @BindView(R.id.spinnerEstadoAmbulancia)
    Spinner spinnerEstadoAmbulancia;
    @BindView(R.id.btnGuardarOcupante)
    Button btnGuardarOcupante;
    @BindView(R.id.add_image)
    ImageView addImage;
    @BindView(R.id.rgSexo)
    RadioGroup sexos;
    @BindView(R.id.otro_container)
    LinearLayout otro_container;
    @BindView(R.id.cbOtro)
    CheckBox cbOtro;
    @BindView(R.id.spinnerPosicion)
    Spinner spinnerPosicion;
    EditText phone, email, extension;
    Spinner spinnerTipoTelefono;
    //List
    private List<HospitalResumen> hospitales = new ArrayList<>();
    private List<Supervisoria> supervisorias = new ArrayList<>();
    private List<CatalogoId> nivelesLesion = new ArrayList<>();
    private List<CatalogoClave> identificaciones = new ArrayList<>();
    private List<CatalogoClaveInfo> estados = new ArrayList<>();
    private List<String> listCoberturasAgregadas = new ArrayList<>();
    private List<CatalogoClaveAmis> codePhone = new ArrayList<>();
    private List<CatalogoClave> posicionOcupantes = new ArrayList<>();
    private Boolean autoriza;
    private Ocupante ocupanteResponse;
    private Activity activity;
    private boolean isUploaded = false;
    private Menu menu;
    private EventBus bus = EventBus.getDefault();
    private CorreoAdapter adapterEmail;
    private PhoneAdapter adapterPhone;
    private String idAjuste;
    private boolean isInvalidDate = false;
    private boolean isLesionado;
    private GeneralInjuredFragment generalInjuredFragment;
    private Long idOcupante;
    private static final int PERMISSION_REQUEST = 100;
    private String[] parts;
    private String añoW; // 004
    private String mesW;
    private String diaW;// 034556
    private Calendar calendar = Calendar.getInstance();
    private int añoActual = calendar.get(Calendar.YEAR);
    public static boolean boolOcupanteAgregado;
    private GeneralCameraFragment generalCameraFragment;
    private long idThird;
    private long idOccupant;
    private CallbackItemImage listener;
    private String path;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private TerceroAutoInteractor terceroAutoService;
    private Hospital hospital;
    private ArrayList<ImageGeneralModel> filesPhotos = new ArrayList<>();


    private String tipoLicencia;
    private List<CatalogoClave> tiposLicenciaList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_ocupante);
        setmToolbar(mToolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        ButterKnife.bind(this);
        idOcupantes = 0L;
        activity = this;
        generalCameraFragment = new GeneralCameraFragment();
        generalInjuredFragment = new GeneralInjuredFragment();
        addFragment(generalCameraFragment);
        addFragmentInjuredParts(generalInjuredFragment);
        listener = this;
        if (getIntent().getExtras() != null) {
            idThird = getIntent().getExtras().getLong("id");
        }

        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        initComponents();
        initLsiteners();
        activity = new TerceroAutoActivity();
        uploadImages.setVisibility(GONE);

        recyclerImages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        otro_container.setVisibility(View.INVISIBLE);

        List<String> estatus = Arrays.asList(getResources().getStringArray(R.array.spinner_status));
        List<String> tipoLicencia = Arrays.asList(getResources().getStringArray(R.array.tipo_licencia));
        AdapterSpinnerGeneral<String> spinnerArrayAdapter = new AdapterSpinnerGeneral<>(TerceroAutoOcupanteActivity.this
                , R.layout.spinner_item, estatus, Object::toString);

        spinnerStatus.setAdapter(spinnerArrayAdapter);

        if (getIntent().getExtras().getString("discriminator") != null) {
            if (getIntent().getExtras().getString("discriminator").equals("ocupante")) {
                idOccupant = getIntent().getExtras().getLong("idOccupant");
                idOcupantes = idOccupant;
                getCompositeDisposable().add(getCatalogs()
                        .subscribe(() -> getOcupante(idOccupant),
                                throwable -> new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable)));

            } else {
                getCompositeDisposable().add(getCatalogs()
                        .subscribe(() -> {},
                                throwable -> new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable)));
            }
        }
    }


    private void getOcupante(Long idOcupante) {
        getCompositeDisposable().add(terceroAutoService.getOcupante(AppPreferences
                .getString(AppPreferences.ID_AJUSTE_ACTUAL), idThird, idOcupante)
                .subscribe(ocupante -> {
                    isUploaded = true;
                    fillDataOccupant(ocupante);
                }, throwable -> {
                    Log.e(TAG, "Error al obtener ocupante " + idOccupant + " tercero del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);
                }));
    }

    private void fillDataOccupant(Ocupante data) {
        ocupanteResponse = data;
        if (data != null) {
            generalCameraFragment.setFolder(MODULO_TERCEROS, TIPO_OCUPANTE, TIPO_AUTO, Long.toString(idOccupant), Long.toString(idAuto));
            ModuleAuto moduleAuto = new ModuleAuto(Long.toString(idOccupant), TERCEROS, OCUPANTE, "", idAuto, idAjuste);
            path = PersistenciaPath.getImagePathAuto(moduleAuto);
            if (path != null) {
                filesPhotos.addAll(PersistenciaPath.getAllImages(getBaseActivity(), path));
                generalCameraFragment.setImageList(filesPhotos);
                PersistenciaPath.showMessageSizeImages(this, filesPhotos);
            }


            firstName.setText(data.getNombre());
            lastName.setText(data.getApellidoPaterno());
            motherLastName.setText(data.getApellidoMaterno());
            setSexo(data);
            idOcupante = data.getId();
            if (data.getFechaNacimiento() != null) {
                dateNacimiento.setText(DateUtils.date(data.getFechaNacimiento()));
                String fechaN = DateUtils.date(data.getFechaNacimiento());
                parts = fechaN.split("/");
                añoW = parts[2];
                mesW = parts[1];
                diaW = parts[0];
                isUploaded = true;
                if (añoActual - Integer.parseInt(añoW) != 0) {
                    int years = Years
                            .yearsBetween(LocalDate.fromDateFields(data.getFechaNacimiento()), LocalDate.now())
                            .getYears();
                    etAge.setText(Integer.toString(years));
                }
                if (etAge.getText().toString().contains("-")) {
                    etAge.setText("");
                }
            } else {
                isUploaded = false;
            }
            if (data.getAutorizaTratamiento() != null) {
                setAutoriza(data.getAutorizaTratamiento());
            }
            if (data.getNumeroIdentificacion() != null) {
                numeroIdentificacion.setText(data.getNumeroIdentificacion());
            }
            if (data.getCorreos() != null) {
                for (int i = 0; i < data.getCorreos().size(); i++) {
                    adapterEmail.add(data.getCorreos().get(i));
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                }
            }
            if (data.getObjetosPerdidos() != null) {
                for (int i = 0; i < data.getObjetosPerdidos().size(); i++) {
                    setObjetos(data.getObjetosPerdidos().get(i).intValue());
                }
            }
            if (data.getTipoIdentificacion() != null) {
                spinnerIdentificacion.setSelection(SpinnerUtils.getPosition(spinnerIdentificacion, data.getTipoIdentificacion()));

                if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getNombre().equals("Licencia de Conductir")) {
                    findViewById(R.id.content_tipo_licencia).setVisibility(View.VISIBLE);
                    findViewById(R.id.content_title_tipo_licencia).setVisibility(View.VISIBLE);
                    findViewById(R.id.content_fecha_vigencia).setVisibility(View.VISIBLE);
                    findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
                    findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
                    findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
                    findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
                }


            }
            if (data.getTipoLicencia() != null) {
                spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, data.getTipoLicencia()));
            }
            if (data.getEstadoLicencia() != null) {
                spinnerEstadoEmisor.setSelection(SpinnerUtils.getPosition(spinnerEstadoEmisor, data.getEstadoLicencia()));
                downloadTiposLicencia(data.getEstadoLicencia());

            }
            if (data.getLicenciaPermanente() != null && data.getLicenciaPermanente()) {
                checkBoxPermanente.setChecked(true);
            } else {
                checkBoxPermanente.setChecked(false);
                if (data.getFechaFinLicencia() != null)
                    dateVigencia.setText(DateUtils.date(data.getFechaFinLicencia()));

            }
            if (data.getTelefonos() != null) {
                for (Telefono telefono : data.getTelefonos()) {
                    telefono.setTipo(telefono.getTipo());
                    telefono.setTelefono(telefono.getTelefono());
                    adapterPhone.add(telefono);
                    recyclerPhone.setAdapter(adapterPhone);
                    adapterPhone.notifyDataSetChanged();

                }

            }
            if (!data.getFotografias().isEmpty()) {
                uploadPhotos();
                generalCameraFragment.setCallBackItem(listener);
                uploadImages.setVisibility(View.VISIBLE);

            } else {
                uploadImages.setVisibility(View.VISIBLE);
            }
            if (data.getCondicion() != null) {
                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, data.getCondicion().name()));
                spinnerNivelLesion.setVisibility(View.VISIBLE);
                if (data.getNivelLesion() != null && !nivelesLesion.isEmpty()) {
                    spinnerNivelLesion.setSelection(SpinnerUtils.getPosition(spinnerNivelLesion, data.getNivelLesion()));
                }
                if (data.getLesiones() != null) {
                    generalInjuredFragment.loadDataList(data.getLesiones());
                }
                if (data.getPosicion() != null && posicionOcupantes != null) {
                    spinnerPosicion.setSelection(SpinnerUtils.getPosition(spinnerPosicion, data.getPosicion().name()));
                    spinnerPosicion.setEnabled(false);
                    spinnerPosicion.setClickable(false);

                }
                if (data.getCondicion().equals(CondicionPersona.LESIONADO)) {
                    spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, data.getCondicion().name()));
                    if (data.getRequiereAmbulancia()!= null && data.getRequiereAmbulancia()) {
                        cbAmbulancia.setChecked(true);
                    } else {
                        cbAmbulancia.setChecked(false);
                    }
                    if (data.getOtroHospital() != null) {
                        cbOtro.setChecked(true);
                        otro_container.setVisibility(View.VISIBLE);
                        findViewById(R.id.content_titile_hospital).setVisibility(GONE);
                        findViewById(R.id.spinner_hospital).setVisibility(GONE);
                        findViewById(R.id.title_hospital).setVisibility(View.GONE);
                        etOtroHospital.setText(data.getOtroHospital());;
                    } else {
                        otro_container.setVisibility(View.INVISIBLE);
                        findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                        findViewById(R.id.spinner_hospital).setVisibility(View.VISIBLE);
                        findViewById(R.id.title_hospital).setVisibility(View.VISIBLE);
                    }
                    if (data.getHospital() != null) {
                        getSupervisoria(data.getHospital());
                        cbOtro.setChecked(false);
                    }
                }
            }
        }
    }

    private Completable downloadIdentificaciones() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getTiposIdentificacion()
                    .subscribe(identificaciones -> {
                        this.identificaciones = identificaciones;
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.spinner_item, identificaciones, CatalogoClave::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerIdentificacion.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerIdentificacion.setAdapter(null);
                        Log.e(TAG, "Error al descargar tipos de identificacion", throwable);

                    })
            );
        });

    }
    private void downloadTiposLicencia(String estado){
            getCompositeDisposable().add(siniestrosInteractor.getTiposLicencia(estado)
                    .subscribe(tiposLicencia -> {
                        tiposLicenciaList = tiposLicencia;
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, tiposLicenciaList, CatalogoClave::getNombre);
                        spinnerTipoLicencia.setAdapter(adapter);
                        if (ocupanteResponse != null && ocupanteResponse.getTipoLicencia() != null) {
                            spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, ocupanteResponse.getTipoLicencia()));
                        }
                    }, throwable -> {
                        spinnerTipoLicencia.setAdapter(null);
                        Log.e(TAG, "Error al descargar los colores", throwable);
                    }));

    }

    private Completable downloadPosicion() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getPosicionesOcupante()
                    .subscribe(poscionOcupantesList -> {
                        posicionOcupantes = poscionOcupantesList;
                        AdapterSpinnerGeneral<CatalogoClave> adapter;
                        adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, posicionOcupantes, CatalogoClave::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerPosicion.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerPosicion.setAdapter(null);
                        Log.e(TAG, "Error al descargar posicion del ocupante", throwable);
                    })
            );
        });


    }



    private Completable downloadNivelesLEsion() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getNivelLesion()
                    .subscribe(nivelesLesion -> {
                        this.nivelesLesion = nivelesLesion;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, nivelesLesion, CatalogoId::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerNivelLesion.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerNivelLesion.setAdapter(null);
                        Log.e(TAG, "Error al descargar niveles de lesión", throwable);
                    })
            );

        });
    }



    private Completable getCatalogs() {
        return Completable.mergeArrayDelayError(
                downloadIdentificaciones(),
                downloadPosicion(),
                downloadNivelesLEsion(),
                downloadEstados(),
                downloadSupervisorias()
        );

    }

    private void  downloadTipoTelefonos() {
            getCompositeDisposable().add(siniestrosInteractor.getTiposTelefono()
                    .subscribe(telefofonos -> {
                        codePhone = telefofonos;
                        showDialogAddPhone();
                    }, throwable -> {
                        Log.e(TAG, "Error al descargar tipos de telefono", throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable.getMessage());
                    }));
    }


    private Completable downloadSupervisorias() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getSupervisorias()
                    .subscribe(supervisorias -> {
                        this.supervisorias = supervisorias;
                        AdapterSpinnerGeneral<Supervisoria> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, supervisorias, Supervisoria::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerEstadoAmbulancia.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerEstadoAmbulancia.setAdapter(null);
                        Log.e(TAG, "Error al descargar supervisorias", throwable);
                    })
            );
        });

    }



    private Completable downloadEstados() {
        return Completable.create(e -> {
            getCompositeDisposable().add(catInteractor.getEstados()
                    .subscribe(estados -> {
                        this.estados = estados;
                        AdapterSpinnerGeneral<CatalogoClaveInfo> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, estados, CatalogoClaveInfo::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerEstadoEmisor.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerEstadoEmisor.setAdapter(null);
                        Log.e(TAG, "Error al descargar estados", throwable);

                    }));
        });
    }




    private void getSupervisoria(Long id) {

        getCompositeDisposable().add(siniestrosInteractor.getHospital(id)
                .subscribe(supervisoriasResponse -> {
                    hospital = supervisoriasResponse;
                    setHospital();
                }, throwable -> {
                    Log.e(TAG, "Error al descargar supervisoria", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));

    }

    private void setHospital() {

        for (int i = 0; i < supervisorias.size(); i++) {
            if (supervisorias.get(i).getClave().equals(hospital.getSupervisoria())) {
                Log.d("", "position" + i);
                spinnerEstadoAmbulancia.setSelection(i);
                downloadHospitales(hospital.getSupervisoria());
                break;
            }
        }


    }

    private void downloadHospitales(String supervisora) {

        getCompositeDisposable().add(siniestrosInteractor.getHospitales(supervisora)
                .subscribe(hospitales -> {
                    this.hospitales = hospitales;
                    AdapterSpinnerGeneral<HospitalResumen> adapter = new AdapterSpinnerGeneral<>(this
                            , R.layout.sp_little_item, hospitales, HospitalResumen::getNombre);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerHospital.setAdapter(adapter);
                    if (hospital != null){
                        for (int j = 0; j < hospitales.size(); j++) {
                            if (this.hospitales.get(j).getNombre().equals(hospital.getNombre())) {
                                spinnerHospital.setSelection(j);
                                break;
                            }
                        }
                    }
                }, throwable -> {
                    spinnerHospital.setAdapter(null);
                    Log.e(TAG, "Error al descargar hospitales", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));
    }


    private void setSexo(Ocupante response) {
        if ("M".equals(response.getSexo())) {
            sexos.check(R.id.rbMasculino);
        } else if ("F".equals(response.getSexo())) {
            sexos.check(R.id.rbFemenino);
        }
    }

    private void deletePhotoOcupante(long id) {
        getCompositeDisposable().add(terceroAutoService.deleteFotografiaOcupante(idAjuste,
                getIntent().getExtras().getLong("id"), idOcupante, id)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(TerceroAutoOcupanteActivity.this, "Se elimino correctamente la fotografia del ocupante");
                }, throwable -> {
                    Log.e(TAG, "Error al eliminar una imagen del ocupante " + idOccupant + " tercero del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);
                }));
    }

    private void initComponents() {
        adapterEmail = new CorreoAdapter(this::deleteEmail);
        adapterPhone = new PhoneAdapter(this::deletePhone);

        recyclerPhone.setHasFixedSize(true);
        recyclerPhone.setLayoutManager(new LinearLayoutManager(this));
        recyclerPhone.setItemAnimator(new DefaultItemAnimator());
        recyclerPhone.setAdapter(adapterPhone);

        recyclerEmail.setHasFixedSize(true);
        recyclerEmail.setLayoutManager(new LinearLayoutManager(this));
        recyclerEmail.setItemAnimator(new DefaultItemAnimator());


    }

    private void initLsiteners() {
        spinnerTipoLicencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipoLicencia = tiposLicenciaList.get(position).getClave();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerIdentificacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave() != null) {
                    switch (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave()) {
                        case "LC":
                            numIdentificacionOA.setText("Número de identificación");
                            findViewById(R.id.content_tipo_licencia).setVisibility(View.VISIBLE);
                            findViewById(R.id.content_title_tipo_licencia).setVisibility(View.VISIBLE);
                            findViewById(R.id.content_fecha_vigencia).setVisibility(View.VISIBLE);
                            findViewById(R.id.tvTipo_Licencia).setVisibility(View.VISIBLE);
                            findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
                            break;
                        case "CV":
                            numIdentificacionOA.setText("Clave de elector");
                            findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
                            findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
                            findViewById(R.id.tvTipo_Licencia).setVisibility(View.GONE);
                            findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
                            findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
                            numeroIdentificacion.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (spinnerIdentificacion.getSelectedItem().toString().equals("Credencial para Votar") && !s.toString().matches("^[A-Z]{6}[0-9]{8}[0-9A-Z]{4}$")) {
                                        numeroIdentificacion.setError("ingrese una credencial válida");
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });
                            break;
                        default:
                            numIdentificacionOA.setText("Número de identificación");
                            findViewById(R.id.content_tipo_licencia).setVisibility(View.GONE);
                            findViewById(R.id.content_title_tipo_licencia).setVisibility(View.GONE);
                            findViewById(R.id.tvTipo_Licencia).setVisibility(View.GONE);
                            findViewById(R.id.content_fecha_vigencia).setVisibility(View.GONE);
                            findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.GONE);
                            break;

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        checkBoxPermanente.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                findViewById(R.id.dateVigencia).setVisibility(View.INVISIBLE);
                findViewById(R.id.title_vigencia).setVisibility(View.INVISIBLE);
            } else {
                findViewById(R.id.dateVigencia).setVisibility(View.VISIBLE);
                findViewById(R.id.title_vigencia).setVisibility(View.VISIBLE);
            }
        });

        dateNacimiento.setOnClickListener(v -> {
            dateNacimiento.setError(null);
            showDatePikerDialog();
        });
        dateVigencia.setOnClickListener(v -> showDatePikerDialogVigencia());

        cbOtro.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            if (isChecked) {
                otro_container.setVisibility(View.VISIBLE);
                findViewById(R.id.content_titile_hospital).setVisibility(View.GONE);
                findViewById(R.id.title_hospital).setVisibility(GONE);
                findViewById(R.id.spinner_hospital).setVisibility(GONE);
                Utils.showKeyboard(etOtroHospital, this);
            } else {
                Utils.hideKeyboard(etOtroHospital, this);
                otro_container.setVisibility(View.INVISIBLE);
                etOtroHospital.setText("");
                findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                findViewById(R.id.title_hospital).setVisibility(View.VISIBLE);
                findViewById(R.id.spinner_hospital).setVisibility(View.VISIBLE);
            }
        }));
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
                    isLesionado = false;
                    findViewById(R.id.title_nivel_lesion).setVisibility(GONE);
                    findViewById(R.id.solicitar_ambulancia).setVisibility(View.GONE);
                    findViewById(R.id.content_hospital).setVisibility(View.GONE);
                    findViewById(R.id.title_estado).setVisibility(GONE);
                    findViewById(R.id.content_nivel_lesion).setVisibility(GONE);
                    findViewById(R.id.content_titile_hospital).setVisibility(View.GONE);
                    findViewById(R.id.content_requiere_ambulancia).setVisibility(View.GONE);
                    findViewById(R.id.title_hospital).setVisibility(GONE);
                    findViewById(R.id.contentInjuredPartsOccupantTercero).setVisibility(GONE);
                    findViewById(R.id.lesionado_content).setVisibility(GONE);
                } else {
                    isLesionado = true;
                    findViewById(R.id.lesionado_content).setVisibility(View.VISIBLE);
                    findViewById(R.id.title_hospital).setVisibility(View.VISIBLE);
                    findViewById(R.id.title_nivel_lesion).setVisibility(View.VISIBLE);
                    findViewById(R.id.content_nivel_lesion).setVisibility(View.VISIBLE);
                    findViewById(R.id.content_requiere_ambulancia).setVisibility(View.VISIBLE);
                    findViewById(R.id.solicitar_ambulancia).setVisibility(View.VISIBLE);
                    findViewById(R.id.title_estado).setVisibility(View.VISIBLE);
                    findViewById(R.id.spinnerEstadoAmbulancia).setVisibility(View.VISIBLE);
                    if (!cbOtro.isChecked()){
                        findViewById(R.id.content_titile_hospital).setVisibility(View.VISIBLE);
                    }
                    findViewById(R.id.content_hospital).setVisibility(View.VISIBLE);
                    findViewById(R.id.contentInjuredPartsOccupantTercero).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        setEvents();
        spinnerEstadoAmbulancia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadHospitales(supervisorias.get(spinnerEstadoAmbulancia.getSelectedItemPosition()).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerEstadoEmisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                downloadTiposLicencia(estados.get(i).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void deleteEmail(int position) {
        new MaterialDialog.Builder(this)
                .title("Eliminar correo...")
                .content("¿Desea eliminar este correo?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapterEmail.remove(adapterEmail.getCorreo(position));
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build()
                .show();
    }

    private void deletePhone(int position) {
        new MaterialDialog.Builder(this)
                .title("Eliminar número...")
                .content("¿Desea eliminar este número?")
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    adapterPhone.remove(adapterPhone.getTelono(position));
                    recyclerPhone.setAdapter(adapterPhone);
                    adapterPhone.notifyDataSetChanged();
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build()
                .show();
    }


    @OnClick(R.id.add_image)
    public void addImage() {
        if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_CAMERA_PERMISSION)) {
        } else {
            RequestPermissions.requestPermission(RequestPermissions.REQUEST_CAMERA_PERMISSION, this);
        }
    }

    @OnClick(R.id.btnuploadImages)
    public void uploadImages() {
        if (idOcupante != null) {
            ImageUtils imageUtils = new ImageUtils();
            List<ImageGeneralModel> imageList = generalCameraFragment.getFileList();
            List<ImageGeneralModel> images = new ArrayList<>();
            for (ImageGeneralModel image : imageList) {
                if (image.getNewFile()) {
                    images.add(image);
                }
            }
            if (!images.isEmpty()) {
                int size = images.size();
                uploadImages.setEnabled(false);
                ValueObject<Integer> count = new ValueObject<>(1);
                getCompositeDisposable().add(Observable.fromIterable(images)
                        .map(image -> new File(image.getFilePhat()))
                        .flatMap(file -> terceroAutoService.addFotografiaOcupante(idAjuste, idThird, idOcupante, file).toObservable(), false, 1)
                        .doOnEach(fotografias -> {
                            if (count.getValue() < size) {
                                count.setValue(count.getValue() + 1);
                                LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                            }
                        })
                        .onErrorReturn(throwable -> {
                            Log.e(TAG, "Error al subir la imagen", throwable);
                            return new Fotografia();
                        })
                        .toList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(it -> {
                            LoadingIndicator.start();
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        })
                        .doFinally(LoadingIndicator::finish)
                        .subscribe(fotografias -> {
                            boolean ok = true;
                            for (Fotografia pic : fotografias) {
                                ok &= pic.getUrl() != null;
                            }
                            if (ok) {
                                new GeneralSnackBar().largeSnackOk(this, getResources().getString(R.string.imagenes_upload_ok));
                            } else {
                                new GeneralSnackBar().largeSnackError(this, getResources().getString(R.string.imagenes_upload_error));
                            }
                            uploadImages.setEnabled(true);
                            imageUtils.renameImageUpload(imageList);
                            imageUtils.deleteImage(getBaseActivity(), imageList);
                            filesPhotos.clear();
                            uploadPhotos();
                        }, throwable -> {
                            uploadImages.setEnabled(true);
                            Log.e(TAG, "Error al subir imagenes de conductor tercero " + idOccupant + " del ajuste: " + idAjuste, throwable);
                            new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);

                        }));
            } else {
                new GeneralSnackBar().largeSnackOk(TerceroAutoOcupanteActivity.this, getString(R.string.image_void));
                uploadImages.setEnabled(true);
            }
        } else {
            new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, "No se ha generado un ocupante");
            uploadImages.setEnabled(true);
        }
    }

    private void uploadPhotos() {
        getCompositeDisposable().add(terceroAutoService.getFotografiasOcupante(idAjuste, idThird, idOcupante)
                .subscribe(listFotos -> {
                    uploadImages.setEnabled(true);
                    for (Fotografia photo : listFotos) {
                        filesPhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                    }
                    generalCameraFragment.updateImageList(filesPhotos);
                }, throwable -> {
                    uploadImages.setEnabled(true);
                    Log.e(TAG, "Error al obtener imagenes de ocupante tercero " + idOccupant + " del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);
                }));
    }



    private PosicionOcupante getPosicionKey() {
        CatalogoClave catalogo = (CatalogoClave) spinnerPosicion.getSelectedItem();
        if (catalogo != null)
            return PosicionOcupante.valueOf(catalogo.getClave());
        else
            return null;
    }

    private Ocupante createOcupante() {
        Ocupante response = new Ocupante();
        if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
            response.setNombre(firstName.getText().toString());
            response.setApellidoPaterno(lastName.getText().toString());
            response.setApellidoMaterno(motherLastName.getText().toString());
            if ("".equals(response.getApellidoMaterno())) {
                response.setApellidoMaterno(null);
            }
            response.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString()));
            response.setLesiones(new RealmList<>());
            response.getLesiones().addAll(generalInjuredFragment.getDataList());
            response.setNivelLesion(getNivelLesion());
            response.setEstadoLicencia(estados.get(spinnerEstadoEmisor.getSelectedItemPosition()).getClave());
            response.setPosicion(getPosicionKey());
            response.setTelefonos(getPhones());
            if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
                response.setTipoLicencia(tiposLicenciaList.get(spinnerTipoLicencia.getSelectedItemPosition()).getClave());
                if (checkBoxPermanente.isChecked()) {
                    response.setLicenciaPermanente(true);
                } else {
                    response.setLicenciaPermanente(false);
                    response.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString()));
                }
            }
            response.setNumeroIdentificacion(numeroIdentificacion.getText().toString());
            response.setObjetosPerdidos(getObjetosPerdidos());
            response.setCorreos(adapterEmail.getCorreoList());
            response.setAutorizaTratamiento(autoriza);
            response.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
            response.setTipoIdentificacion(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave());
            if (getIntent().getExtras().getString("discriminator").equals("ocupante")) {
                response.setId(idOcupante);
            }
            if (cbOtro.isChecked()) {
                response.setHospital(null);
                response.setOtroHospital(etOtroHospital.getText().toString());
            } else {
                if (hospitales != null && hospitales.size() != 0) {
                    response.setHospital(hospitales.get(spinnerHospital.getSelectedItemPosition()).getId());
                } else {
                    response.setHospital(null);
                }
                response.setOtroHospital(null);
            }
            response.setRequiereAmbulancia(cbAmbulancia.isChecked());
            response.setSexo(getGender());
        } else {
            response.setNombre(firstName.getText().toString());
            response.setApellidoPaterno(lastName.getText().toString());
            response.setApellidoMaterno(motherLastName.getText().toString());
            if ("".equals(response.getApellidoMaterno())) {
                response.setApellidoMaterno(null);
            }
            response.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString().toUpperCase()));
            response.setSexo(getGender());
            response.setTipoIdentificacion(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave());
            response.setEstadoLicencia(estados.get(spinnerEstadoEmisor.getSelectedItemPosition()).getClave());
            response.setPosicion(getPosicionKey());
            response.setRequiereAmbulancia(false);
            if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
                response.setTipoLicencia(tiposLicenciaList.get(spinnerTipoLicencia.getSelectedItemPosition()).getClave());
                if (checkBoxPermanente.isChecked()) {
                    response.setLicenciaPermanente(true);
                } else {
                    response.setLicenciaPermanente(false);
                    response.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString()));
                }
            }
            response.setTelefonos(getPhones());
            response.setNivelLesion(null);
            response.setAutorizaTratamiento(autoriza);
            response.setNumeroIdentificacion(numeroIdentificacion.getText().toString());
            response.setObjetosPerdidos(getObjetosPerdidos());
            response.setCorreos(adapterEmail.getCorreoList());
            response.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
            if (getIntent().getExtras().getString("discriminator").equals("ocupante")) {
                response.setId(idOcupante);
            }
            response.setTipoIdentificacion(identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave());
            response.setEstadoLicencia(estados.get(spinnerEstadoEmisor.getSelectedItemPosition()).getClave());
            response.setPosicion(getPosicionKey());
            response.setTelefonos(getPhones());
            response.setNumeroIdentificacion(numeroIdentificacion.getText().toString());
            response.setObjetosPerdidos(getObjetosPerdidos());
            response.setCorreos(adapterEmail.getCorreoList());
        }
        return response;

    }

    @OnClick(R.id.solicitar_ambulancia)
    public void solicitarAmbulanciaClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(layout, "Necesitas brindar el permiso para realizar llamadas",
                            Snackbar.LENGTH_LONG).setAction("Aceptar", new View.OnClickListener() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                call();
            }
        } else {
            call();
        }

    }

    @SuppressLint("MissingPermission")
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Utils.NUMBER_AMBULANCE));
        startActivity(intent);
    }


    private void setObjetos(int objeto) {
        switch (objeto) {
            case 1:
                cbLentes.setChecked(true);
                break;
            case 2:
                cbAuditivo.setChecked(true);
                break;
            case 3:
                cbDentadura.setChecked(true);
                break;
            case 4:
                cbProtesis.setChecked(true);
                break;
        }
    }

    private RealmList<Long> getObjetosPerdidos() {
        Integer auditivo, dentadura, lentes, protesis;
        if (cbAuditivo.isChecked()) {
            auditivo = 2;
        } else {
            auditivo = null;
        }
        if (cbDentadura.isChecked()) {
            dentadura = 3;
        } else {
            dentadura = null;
        }
        if (cbLentes.isChecked()) {
            lentes = 1;
        } else {
            lentes = null;
        }
        if (cbProtesis.isChecked()) {
            protesis = 4;
        } else {
            protesis = null;
        }

        RealmList<Long> integerList = new RealmList<>();
        if (auditivo != null) integerList.add(auditivo.longValue());
        if (dentadura != null) integerList.add(dentadura.longValue());
        if (lentes != null) integerList.add(lentes.longValue());
        if (protesis != null) integerList.add(protesis.longValue());
        return integerList;
    }


    private RealmList<Telefono> getPhones() {
        RealmList<Telefono> list = new RealmList<>();
        for (Telefono telefonoResponse : adapterPhone.getTelefonos()) {
            list.add(new Telefono(telefonoResponse.getTelefono(), telefonoResponse.getTipo(), telefonoResponse.getExtension()));
        }
        return list;
    }


    private void showDatePikerDialog() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateNacimiento.setText(date);
            if (age.contains("-")) {
                etAge.setText("");
            }else {
                etAge.setText(age);
            }
        });
        datePickerFragment.show(getSupportFragmentManager(), "dataPickerBirthday");


    }

    private String calculateAge(int year, int month, int day) {
        LocalDate birthdate = new LocalDate(year, month, day);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);

        return String.valueOf(age.getYears());
    }


    private void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateVigencia.setText(date);
        });
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }


    private void showDialogAddEmail() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar correo electrónico")
                .customView(R.layout.dialog_add_e_mail, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {

                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();

        email = (EditText) builder.findViewById(R.id.etEmail);
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(),R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_e_mail));
        builder.show();
    }

    private void showDialogAddPhone() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar teléfono")
                .customView(R.layout.dialog_add_phone, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {

                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        spinnerTipoTelefono = (Spinner) builder.findViewById(R.id.spinnerTipoTelefono);
        TextView title = (TextView) builder.findViewById(R.id.titleExtencion);
        AdapterSpinnerGeneral<CatalogoClaveAmis> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, codePhone, CatalogoClave::getNombre);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoTelefono.setAdapter(adapter);
        phone = (EditText) builder.findViewById(R.id.etPhone);
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        extension = (EditText) builder.findViewById(R.id.etExtension);
        builder.show();
        spinnerTipoTelefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (((CatalogoClave)spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                    extension.setVisibility(View.VISIBLE);
                    title.setVisibility(View.VISIBLE);
                } else {
                    extension.setVisibility(View.GONE);
                    title.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(),R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_phone));
    }



    @Override
    public void onChangeTitle(String title) {

    }

    @Override
    public void onDefaulTitle() {

    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    @Override
    public void onCallbackItemId(int position, Long id) {
        deletePhotoOcupante(id);
    }

    @Override
    public void onCallItemId(int position, Long id) {

    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String nombre = firstName.getText().toString().trim();
        if (!FormatStrings.validateText(nombre)) {
            firstName.setError(getString(R.string.message_character_special));
        }
        String apellidoP = lastName.getText().toString().trim();

        if (!FormatStrings.validateText(apellidoP)) {
            lastName.setError(getString(R.string.message_character_special));
        }
        String apellidoM = motherLastName.getText().toString().trim();

        if (!FormatStrings.validateText(apellidoM)) {
            motherLastName.setError(getString(R.string.message_character_special));
        }
    }

    private void setEvents() {
        firstName.addTextChangedListener(this);
        lastName.addTextChangedListener(this);
        motherLastName.addTextChangedListener(this);
    }

    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone.getText().toString().trim().length() < 10) {
                    phone.setError(ResourceUtils.getString("invalid_number"));
                } else {
                    String key = "";
                    if (codePhone.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave() != null) {
                        key = codePhone.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave();
                    }
                    adapterPhone.add(new Telefono(phone.getText().toString(), key, ((CatalogoClave)spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo") ? extension.getText().toString() : null));
                    recyclerPhone.setAdapter(adapterPhone);
                    adapterPhone.notifyDataSetChanged();
                    dialog.dismiss();
                }
            } else {
                if (email.getText().toString().equals("")) {
                    email.setError("No deje vacio este campo");
                    return;
                }
                else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError("Coloque una dirección de correo correcta");
                    return;
                }  else {
                    adapterEmail.add(email.getText().toString());
                    recyclerEmail.setAdapter(adapterEmail);
                    dialog.dismiss();
                }
            }
        }

    }


    @OnClick(R.id.btnAddEmail)
    public void addEmail() {
        showDialogAddEmail();
    }

    @OnClick(R.id.btnAddPhone)
    public void addPhone() {
        downloadTipoTelefonos();
    }


    private String getGender() {
        return rbGender.isChecked() ? "M" : "F";
    }

    @OnClick(R.id.btnGuardarOcupante)
    public void insertOccupant() {
        if (validForm()) {
            btnGuardarOcupante.setEnabled(false);
            if (autoriza == null) {
                Toast.makeText(getApplicationContext(), ResourceUtils.getString("aviso_privacidad_error"), Toast.LENGTH_LONG).show();
                btnGuardarOcupante.setEnabled(true);
                return;
            }
            if (etOtroHospital.getText().toString().equals("") && cbOtro.isChecked()) {
                etOtroHospital.requestFocus();
                etOtroHospital.setError(getResources().getString(R.string.volante_coberturas_required));
                btnGuardarOcupante.setEnabled(true);
                return;
            }
            if (adapterEmail.getCorreoList().size() == 0) {
                Toast.makeText(getApplicationContext(), getString(R.string.correo_mesage), Toast.LENGTH_LONG).show();
                btnGuardarOcupante.setEnabled(true);
                return;
            }
            ocupanteResponse = createOcupante();
            if (getIntent().getExtras().getString("discriminator") != null) {
                if (!getIntent().getExtras().getString("discriminator").equals("ocupante")) {
                    if (idOcupante == null) {
                        getCompositeDisposable().add(terceroAutoService.addOcupante(idAjuste, idThird, createOcupante())
                                .subscribe(ocupanteData -> {
                                    if (ocupanteData != null) {
                                        idOcupante = ocupanteData.getId();
                                        setAutoriza(ocupanteData.getAutorizaTratamiento() != null ?
                                                ocupanteData.getAutorizaTratamiento() : false);
                                    }
                                    isUploaded = true;
                                    setResult(RESULT_OK);
                                    new GeneralSnackBar().largeSnackOk(TerceroAutoOcupanteActivity.this, "El ocupante fue agregado exitosamente ");
                                    boolOcupanteAgregado = true;
                                    generalCameraFragment.setFolder(MODULO_TERCEROS, TIPO_OCUPANTE, TIPO_AUTO, String.valueOf(idOcupante), Long.toString(idAuto));
                                    uploadImages.setVisibility(View.VISIBLE);
                                    ModuleAuto moduleAuto = new ModuleAuto(Long.toString(idOccupant), TERCEROS, OCUPANTE, "", idAuto, idAjuste);
                                    path = PersistenciaPath.getImagePathAuto(moduleAuto);
                                    btnGuardarOcupante.setEnabled(true);
                                    if (!getAllImages(getBaseActivity(), path).isEmpty())
                                        uploadImages();
                                }, throwable -> {
                                    btnGuardarOcupante.setEnabled(true);
                                    isUploaded = false;
                                    Log.e(TAG, "Error al agregar Ocupante tercero del ajuste: " + idAjuste, throwable);
                                    new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);
                                }));
                    } else {
                        btnGuardarOcupante.setEnabled(true);
                        getCompositeDisposable().add(terceroAutoService.modifyOcupante(idAjuste, idThird,
                                idOcupante, createOcupante())
                                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                .subscribe(() -> {
                                    setResult(RESULT_OK);
                                    new GeneralSnackBar().largeSnackOk(TerceroAutoOcupanteActivity.this, "El ocupante fue actualizado exitosamente ");
                                    uploadImages.setVisibility(View.VISIBLE);
                                    setAutoriza(createOcupante().getAutorizaTratamiento());
                                }, throwable -> {
                                    Log.e(TAG, "Error al modificar ocupante " + idOccupant + "tercero del ajuste: " + idAjuste, throwable);
                                    new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);

                                }));
                    }

                } else {
                    btnGuardarOcupante.setEnabled(true);
                    getCompositeDisposable().add(terceroAutoService.modifyOcupante(idAjuste, idThird,
                            idOcupante, createOcupante())
                            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                                setResult(RESULT_OK);
                                new GeneralSnackBar().largeSnackOk(TerceroAutoOcupanteActivity.this, "El ocupante fue actualizado exitosamente ");
                                uploadImages.setVisibility(View.VISIBLE);
                                setAutoriza(createOcupante().getAutorizaTratamiento());
                            }, throwable -> {
                                Log.e(TAG, "Error al modificar ocupante " + idOccupant + "tercero del ajuste: " + idAjuste, throwable);
                                new GeneralSnackBar().largeSnackError(TerceroAutoOcupanteActivity.this, throwable);

                            }));
                }
            }
        } else if (isInvalidDate) {
            btnGuardarOcupante.setEnabled(true);
            Toast.makeText(this, "Fecha inválida", Toast.LENGTH_LONG).show();
        }
    }


    private boolean validForm() {
        boolean isRequestFocus = false;
        if (firstName.getText().toString().trim().length() <= 0) {
            firstName.setError(getString(R.string.mandatory_field));
            firstName.requestFocus();
            isRequestFocus = true;
        }
        if (lastName.getText().toString().trim().length() <= 0) {
            lastName.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                lastName.requestFocus();
                isRequestFocus = true;
            }
        }
        if (dateNacimiento.getText().toString().trim().length() <= 0) {
            dateNacimiento.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                dateNacimiento.requestFocus();
                isRequestFocus = true;
            }
        }
        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getNombre().equals("Licencia de Conducir")) {
            if (!checkBoxPermanente.isChecked()) {
                if (dateVigencia.getText().toString().trim().length() <= 0) {
                    dateVigencia.setError(getString(R.string.mandatory_field));
                    dateVigencia.requestFocus();
                    if (!isRequestFocus) {
                        isRequestFocus = true;
                    }
                }
            }
        }
        if (numeroIdentificacion.getText().toString().trim().length() <= 0) {
            numeroIdentificacion.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                numeroIdentificacion.requestFocus();
                isRequestFocus = true;
            }
        }
        if (sexos.getCheckedRadioButtonId() == -1) {
            ToastUtils.toastShort("Debe seleccionar un género por favor");
        }
        if (adapterPhone.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(this, ResourceUtils.getString("telefono_empty"), Toast.LENGTH_LONG).show();
        }
        if (adapterEmail.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(this, ResourceUtils.getString("correo_empty"), Toast.LENGTH_LONG).show();
        }

        if (isLesionado) {
            if (generalInjuredFragment.getDataList().isEmpty()) {
                isRequestFocus = true;
                Toast.makeText(this, ResourceUtils.getString("lesion_empty"), Toast.LENGTH_LONG).show();
            }
        }
        return !isRequestFocus;
    }

    private long getNivelLesion() {
        CatalogoId parteLesionada = (CatalogoId) spinnerNivelLesion.getSelectedItem();
        if (parteLesionada != null)
            return parteLesionada.getId();
        else
            return 0L;
    }

    @OnClick(R.id.bt_aviso_de_privacidad)
    public void dialogoAvisoPrivacidad() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Aviso de privacidad")
                .customView(R.layout.dialog_notice_of_privacy, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    if (autoriza != null) {
                        setAutoriza(autoriza);
                    }
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        CheckBox si = (CheckBox) builder.findViewById(R.id.cbPrivacidadSi);
        CheckBox no = (CheckBox) builder.findViewById(R.id.cbPrivacidadNo);
        if (autoriza != null) {
            if (autoriza) {
                si.setChecked(true);
                no.setChecked(false);
            } else {
                si.setChecked(false);
                no.setChecked(true);
            }
        }
        si.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(true);
                no.setChecked(false);
                autoriza = true;
            } else {
                no.setChecked(true);
                si.setChecked(false);
                autoriza = false;
            }
        });
        no.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                si.setChecked(false);
                no.setChecked(true);
                autoriza = false;
            } else {
                no.setChecked(false);
                si.setChecked(true);
                autoriza = true;
            }
        });
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(),R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        builder.show();
    }

    private void setAutoriza(boolean autoriza) {
        this.autoriza = autoriza;
    }


    @Override
    public void onBackPressed() {
        if (!isUploaded) {
            DialogUtils.createAlert(this, "Salir",
                    getString(R.string.message_occupant_save),
                    "Aceptar", "Cancelar",
                    (dialog, which) -> {
                        if (ocupanteResponse != null) {
                            bus.postSticky("load");
                            TerceroAutoOcupanteActivity.super.onBackPressed();
                        } else {
                            TerceroAutoOcupanteActivity.super.onBackPressed();
                        }
                    },
                    (dialog, which) -> dialog.dismiss());
        } else {
            if (ocupanteResponse != null) {
                bus.postSticky("load");
                TerceroAutoOcupanteActivity.super.onBackPressed();
            } else {
                TerceroAutoOcupanteActivity.super.onBackPressed();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        generalCameraFragment.onActivityResult(requestCode, resultCode, data);
    }

    private void addFragment(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraFragmentOccupant, fragment, Constants.TAG_THIRD_OCCUPANT);
        transaction.commit();
    }

    private void addFragmentInjuredParts(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentInjuredPartsOccupantTercero, fragment, "injuredParts");
        transaction.commit();
    }


}