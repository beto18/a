package com.gnp.ajustadormovil.android.views.terceros.autos;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemOcupanteListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by Abraham on 05/02/2018.
 */

public class TerceroAutoOcupanteAdapter extends RecyclerView.Adapter<TerceroAutoOcupanteAdapter.OcupanteHolder>{

    private ArrayList<Ocupante>ocupantes;
    private OnItemOcupanteListener listener;
    private EventBus bus = EventBus.getDefault();

    public TerceroAutoOcupanteAdapter(OnItemOcupanteListener listener) {
        this.listener = listener;
        this.ocupantes = new ArrayList<> ();
    }

    public void addAll(List<Ocupante> results) {
        for (Ocupante result : results) {
            add (result);
        }
    }

    public void add (Ocupante r) {
        ocupantes.add (r);
        notifyItemInserted (ocupantes.size () - 1);
    }



    public void remove (Ocupante r) {
        int position = ocupantes.indexOf (r);
        if (position > -1) {
            ocupantes.remove (position);
            notifyItemRemoved (position);
        }
    }

    public List<Ocupante> getOcupantes(){
        return ocupantes;
    }

    public Ocupante getOcupante(int position){
        return ocupantes.get (position);
    }

    public void clear() {
        final int size = ocupantes.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                ocupantes.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }


    @Override
    public OcupanteHolder onCreateViewHolder (ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_list_correo, parent, false);
        return new OcupanteHolder (mView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder (OcupanteHolder holder, int position) {
        holder.item.setText (ocupantes.get (position).getNombreCompleto());
    }

    @Override
    public int getItemCount () {
        return ocupantes.size();
    }

    class OcupanteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView (R.id.email)
        TextView item;
        @BindView (R.id.delete)
        ImageButton delete;

        OcupanteHolder (View itemView) {
            super (itemView);
            ButterKnife.bind (this,itemView);
            itemView.setOnClickListener (this);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bus.post(ocupantes.get(getAdapterPosition()));
                }
            });
        }

        @Override
        public void onClick (View view) {
            listener.OnItemOcupante (view, getAdapterPosition ());
        }
    }
}
