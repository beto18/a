package com.gnp.ajustadormovil.android.views.terceros.autos;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VehiculoTercero;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.ajuste.AjustesFragment;
import com.gnp.ajustadormovil.android.views.terceros.TerceroAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemSelectedListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Lima on 30/10/17.
 */

public class TercerosAutoFragment extends BaseFragment implements OnItemRemovedListener, OnItemSelectedListener {

    private Unbinder mUnbinder;
    @BindView(R.id.rvTerceros)
    RecyclerView rvTerceros;
    @BindView(R.id.container_autos)
    RelativeLayout layout;
    @BindView(R.id.title_less)
    TextView title_less;
    private List<TerceroAuto> idResponses;
    private TerceroAdapter adapter;
    private MaterialDialog dialog;
    private String db_reading_token = "", idAjuste = "";
    public static final String TAG = "TAG";
    private TerceroAutoInteractor terceroAutoService;
    private CompositeDisposable compositeDisposable;

    public static TercerosAutoFragment newInstance() {
        TercerosAutoFragment fragment = new TercerosAutoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        compositeDisposable = ((BaseActivity)getActivity()).getCompositeDisposable();
        initComponents();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terceros, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    private void initComponents() {
        idAjuste = AjustesFragment.PositionForServicioAjuste;
        db_reading_token = AjustesFragment.PositionToken; //reading.getString (reading.getColumnIndex (DataBaseHelper.KEY_TODO));
        adapter = new TerceroAdapter(this, this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvTerceros.setHasFixedSize(true);
        setAdapter(adapter);
        rvTerceros.setLayoutManager(new LinearLayoutManager(getContext()));
        getAutosData();
    }

    @OnClick(R.id.btnTercero)
    public void addAuto() {//idTercrosAuto

        compositeDisposable.add(terceroAutoService.add(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(auto->{
                    Log.d(TAG, "onResponse: " + auto.getId());
                    getAutosData();
                    Intent intent = new Intent(getActivity(), TerceroAutoActivity.class);
                    intent.putExtra("id", auto.getId());
                    intent.putExtra("show",false);
                    intent.putExtra("foundConductor", "");
                    intent.putExtra("foundVehiculo", "");
                    intent.putExtra("foundOcupantes", "");
                    intent.putExtra("data", getArguments().getString("data"));
                    startActivity(intent);

                }, throwable -> {
                    String message = ErrorUtils.getErrorMessage(throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), message);
                }));
    }

    private void getAutosData() {
        compositeDisposable.add(terceroAutoService.getAll(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(autos->{
                    idResponses = autos;
                    if (idResponses != null) {
                        adapter.clear();
                        adapter.addAll(idResponses);
                        setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                    if (autos.size()!=0 && title_less!=null){
                        title_less.setVisibility(View.GONE);
                    }
                    else {
                        if (title_less!=null) {
                            title_less.setVisibility(View.VISIBLE);
                        }
                    }
                }, throwable -> {
                    String message = ErrorUtils.getErrorMessage(throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), message);
                }));
    }

    private void setAdapter(RecyclerView.Adapter adapter){
        if (rvTerceros!=null) {
            rvTerceros.setAdapter(adapter);
        }
    }


    @Subscribe
    public void addData(VehiculoTercero tercero) {
        adapter.clear();
        rvTerceros.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        getAutosData();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position < idResponses.size()) {
            Log.d(TAG, "onItemClick: " + idResponses.get(position).getId());
            dialog = new MaterialDialog.Builder(getActivity())
                    .title("Eliminar auto...")
                    .content("¿Desea eliminar este auto?")
                    .positiveText("Aceptar")
                    .onPositive((dialog, which) -> {
                        compositeDisposable.add(terceroAutoService.delete(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),adapter.getTerceros().get(position).getId())
                                .subscribe(()->{
                                    adapter.remove(adapter.getTercero(position));
                                    rvTerceros.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    getAutosData();
                                    new GeneralSnackBar().largeSnackOk(getActivity(), "El auto fue eliminado correctamente...");
                                }, throwable -> {
                                    Log.e(TAG, "Error al eliminar el tercero", throwable);
                                    new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                                }));
                    })
                    .negativeText("Cancelar")
                    .onNegative((dialog, which) -> dialog.dismiss())
                    .build();
            MDButton aceptar = dialog.getActionButton(DialogAction.POSITIVE);
            MDButton cancelar = dialog.getActionButton(DialogAction.NEGATIVE);
            cancelar.setAllCaps(false);
            aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            aceptar.setBackground(ContextCompat.getDrawable(dialog.getContext(),R.drawable.background_arround_actived));
            aceptar.setTextColor(Color.WHITE);
            aceptar.setAllCaps(false);
            dialog.show();
        }
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    @Override
    public void selectItem(View view, int position) {
        if (position < idResponses.size()) {
            if (idResponses.get(position) != null) {
                long idTerceroString = idResponses.get(position).getId();
                compositeDisposable.add(terceroAutoService.get(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), idTerceroString)
                        .subscribe(object->{
                            String data = new Gson().toJson(object);
                            System.out.println(data);
                            String stringConductor = "";
                            String stringVehiculo = "";
                            String stringOcupantes = "";
                            if (object.getConductor() != null) {
                                Persona foundConductor = object.getConductor();
                                stringConductor = new Gson().toJson(foundConductor);
                            }
                            if (object.getVehiculo() != null) {
                                VehiculoTercero foundVehiculo = object.getVehiculo();
                                stringVehiculo = new Gson().toJson(foundVehiculo);
                            }
                            if (object.getOcupantes() != null) {
                                List<Ocupante> foundOcupantes = object.getOcupantes();
                                stringOcupantes = new Gson().toJson(foundOcupantes);
                            }
                            Log.d(TAG, "selectItem: " + idResponses.get(position).getId());
                            TerceroAuto id = idResponses.get(position);
                            Intent intent = new Intent(getActivity(), TerceroAutoActivity.class);
                            intent.putExtra("id", idResponses.get(position).getId());
                            intent.putExtra("show",false);
                            intent.putExtra("foundConductor", stringConductor);
                            intent.putExtra("foundVehiculo", stringVehiculo);
                            intent.putExtra("foundOcupantes", stringOcupantes);
                            intent.putExtra("data", getArguments().getString("data"));
                            startActivity(intent);
                        }, throwable -> {
                            String message  = ErrorUtils.getErrorMessage(throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), message);
                        }));
            }
        }

    }

    private String newMessage(String error) {
        String message = null;
        switch (error) {
            case "tercero_responsable":
                message = "No se puede borrar el tercero porque es el responsable";
                break;
            case "tercero_volante":
                message = "El tercero no se puede eliminar, debido a que se le generó su orden de admisión";
                break;
            case "internal_server_error":
                message = "Ocurrió un problema inesperado";
                break;
            case "ajuste_no_editable":
                message = "No se puede editar un ajuste si el estatus no es LLEGADO";
                break;
        }
        return message;
    }

}