package com.gnp.ajustadormovil.android.views.terceros.noautos;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModuleNoAuto;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.FormatStrings;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.TelefonoNoAutoAdapter;
import com.gnp.ajustadormovil.android.views.commons.adapters.legacy.CorreoAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.commons.custom.ItemDecorator;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnTelefonoListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_TERCEROS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_NO_AUTO;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getAllImages;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.getImagePathNoAuto;
import static com.gnp.ajustadormovil.android.utils.Constants.NO_AUTOS;
import static com.gnp.ajustadormovil.android.utils.Constants.TERCEROS;

public class TerceroNoAutoActivity extends BaseActivity implements OnItemRemovedListener,
        OnTelefonoListener, CallbackItemImage<Long>, TextWatcher {

    @BindView(R.id.toolbar_no_autos_tercero)
    Toolbar mToolbarNoAutos;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.spTipoPersona)
    Spinner tipoPersona;
    @BindView(R.id.fisica_content)
    LinearLayout persona;
    @BindView(R.id.rvTelefonosPersona)
    RecyclerView rvTelefonos;
    @BindView(R.id.razon_container)
    LinearLayout razonContainer;
    @BindView(R.id.rvCorreosPersona)
    RecyclerView rvCorreos;
    @BindViews({R.id.edtNombreNoAuto, R.id.edtApePaterno, R.id.edtApMaterno})
    List<EditText> nombreUsuario;
    @BindViews({R.id.edtAreasDañadas, R.id.edtDanosPreexistentes, R.id.edtReferenciasPersona})
    List<EditText> danosList;
    @BindView(R.id.rgSexo)
    RadioGroup spGenero;
    @BindViews({R.id.spNoAuto, R.id.spTipoNoAuto})
    List<Spinner> spNoAuto;
    @BindView(R.id.spEstado)
    Spinner spEstadoDialog;
    @BindView(R.id.spMunicipio)
    Spinner spMunicipioDialog;
    @BindView(R.id.spColonia)
    Spinner spColoniaDialog;
    @BindView(R.id.no_auto)
    RelativeLayout layout;
    @BindView(R.id.dateNac)
    EditText dateNacimiento;
    @BindView(R.id.etAge)
    EditText edad;
    @BindView(R.id.uploadImages)
    Button btnUploadImages;
    @BindView(R.id.cbReclamo)
    CheckBox cbReclamo;
    EditText phone, email, extension;
    @BindView(R.id.edtRazonSocial)
    EditText edtRazon;
    @BindView(R.id.edtCalle)
    EditText calle;
    @BindView(R.id.edtEntreCalle)
    EditText edtEntreCalles;
    @BindView(R.id.edtNumEx)
    EditText edtNumEx;
    @BindView(R.id.edtNumIn)
    EditText edtNumIn;
    @BindView(R.id.edtKm)
    EditText edtKm;
    @BindView(R.id.edtPostal)
    EditText edtPostal;
    @BindView(R.id.btnGuardarVehiculoNoAuto)
    Button btnGuardar;
    @BindView(R.id.edtRF)
    EditText edtRFC;
    private CallbackItemImage listener;
    private TelefonoNoAutoAdapter telefonoNoAutoAdapter;
    private Spinner spTipoTelefono;
    private CorreoAdapter correoAdapter;
    private Dialog dialog;
    private String idAjuste = "";
    public static final String TAG = "TAG";
    private List<CatalogoClaveAmis> telefonosList;
    private List<CatalogoId> noAutos = new ArrayList<>();
    private List<CatalogoId> tipoNoAutos = new ArrayList<>();
    private EventBus bus = EventBus.getDefault();
    private String genero = "", clave = "";
    private TerceroNoAuto noAutoData = new TerceroNoAuto();
    private String extencion = "";
    private Long idNoAuto;
    Calendar calendar = Calendar.getInstance();
    private String tipo;
    private Ajuste data;
    private boolean isUploaded = false;
    private int anoActual = calendar.get(Calendar.YEAR);
    private String[] parts;
    private String anoW;
    private String mesW;
    private String diaW;

    private List<CatalogoClaveInfo> statesList = new ArrayList<>();
    private CatalogoClaveInfo statesSelected = new CatalogoClaveInfo();
    private List<Municipio> municipalityList = new ArrayList<>();
    private Municipio municipalitySelected = new Municipio();
    private List<ColoniaResumen> coloniesList = new ArrayList<>();
    private ColoniaResumen coloniesSelected = new ColoniaResumen();
    private boolean isChangeSpinner = false;
    private Activity activity;

    private GeneralCameraFragment generalCameraFragment;

    private String path;

    private TerceroNoAutoInteractor terceroNoAutoService;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;

    private TerceroNoAuto terceroNoAuto;
    private Long tipoNoAuto;
    private ArrayList<ImageGeneralModel> filePhotos = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_no_auto_tercero);
        setmToolbar(mToolbarNoAutos);
        activity = this;
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        generalCameraFragment = new GeneralCameraFragment();
        listener = this;
        terceroNoAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
        addFragment(generalCameraFragment, Constants.TAG_THIRD_NO_VEHICLE);
        persona.setVisibility(View.GONE);
        //IMPLEMENTACIÓN DE LOS MODULOS SERVICES PARA LA CONSULTA DE BASE DE DATOS O SERVICIOS
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();

        btnUploadImages.setVisibility(GONE);
        setEvents();
        initListeners();

        findViewById(R.id.razon_container).setVisibility(GONE);
        findViewById(R.id.razon_spinner).setVisibility(View.GONE);
        findViewById(R.id.beneficiaria_content).setVisibility(GONE);
        String dataResponse = getIntent().getExtras().getString("data");
        terceroNoAuto = new Gson().fromJson(getIntent().getExtras().getString("foundNoAuto"), TerceroNoAuto.class);
        data = new Gson().fromJson(dataResponse, Ajuste.class);
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        List<String> tiposPersona = Arrays.asList(getResources().getStringArray(R.array.tipoRazonSocial));
        AdapterSpinnerGeneral<String> spinnerArrayAdapter = new AdapterSpinnerGeneral<>(TerceroNoAutoActivity.this, R.layout.spinner_item, tiposPersona, Object::toString);
        tipoPersona.setAdapter(spinnerArrayAdapter);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        correoAdapter = new CorreoAdapter(this);
        rvCorreos.setLayoutManager(manager);
        RecyclerView.ItemDecoration dividerItemDecoration = new ItemDecorator(ContextCompat.getDrawable(getApplicationContext(), R.drawable.line_divider));
        rvCorreos.addItemDecoration(dividerItemDecoration);
        telefonoNoAutoAdapter = new TelefonoNoAutoAdapter(this);
        LinearLayoutManager managerTelefono = new LinearLayoutManager(this);
        managerTelefono.setOrientation(LinearLayoutManager.VERTICAL);
        rvTelefonos.setLayoutManager(managerTelefono);
        rvTelefonos.setAdapter(telefonoNoAutoAdapter);
        rvCorreos.setAdapter(correoAdapter);
        RecyclerView.ItemDecoration dividerItemDecoration2 = new ItemDecorator(ContextCompat.getDrawable(getApplicationContext(), R.drawable.line_divider));
        rvTelefonos.addItemDecoration(dividerItemDecoration2);

        getCompositeDisposable().add(getCatalogos()
                .subscribe(this::setTerceroNoAuto,
                        throwable -> new GeneralSnackBar().largeSnackError(this, throwable)));


    }


    private void setTerceroNoAuto() {


        if (!getIntent().getExtras().getString("foundNoAuto").equals("")) {
            isUploaded = true;
            idNoAuto = terceroNoAuto.getId();
            if (idNoAuto != null) {
                generalCameraFragment.setFolder(MODULO_TERCEROS, 0, TIPO_NO_AUTO, "", String.valueOf(idNoAuto));
                ModuleNoAuto moduleNoAuto = new ModuleNoAuto(String.valueOf(idNoAuto), TERCEROS, NO_AUTOS, "", idAjuste, String.valueOf(idNoAuto));
                path = getImagePathNoAuto(moduleNoAuto);

                if (path != null) {
                    filePhotos.addAll(getAllImages(getBaseActivity(), path));
                    generalCameraFragment.setImageList(filePhotos);
                    PersistenciaPath.showMessageSizeImages(getBaseActivity(), filePhotos);
                }
            }

            btnUploadImages.setVisibility(View.VISIBLE);
            if (terceroNoAuto.getEstado() != null) {
                spEstadoDialog.setSelection(SpinnerUtils.getPosition(spEstadoDialog, terceroNoAuto.getEstado()));
                isChangeSpinner = true;
            }

            if (terceroNoAuto.getCategoriaNoAuto() != null) {
                spNoAuto.get(0).setSelection(SpinnerUtils.getPosition(spNoAuto.get(0), terceroNoAuto.getCategoriaNoAuto()));
            }
            if (terceroNoAuto.getTipoNoAuto() != null) {
                spNoAuto.get(1).setSelection(SpinnerUtils.getPosition(spNoAuto.get(1), terceroNoAuto.getTipoNoAuto()));

            }
            if (terceroNoAuto.getAreaDanada() != null) {
                danosList.get(0).setText(terceroNoAuto.getAreaDanada());
            }
            if (terceroNoAuto.getDanoPreexistente() != null) {
                danosList.get(1).setText(terceroNoAuto.getDanoPreexistente());
            }
            if (!terceroNoAuto.getFotografias().isEmpty()) {
                updatePhotosNoAuto();
                generalCameraFragment.setCallBackItem(listener);
            }

            if (terceroNoAuto.getAfectado() != null) {
                isUploaded = true;
                if (terceroNoAuto.getAfectado().getTipoPersona() != null) {
                    if (terceroNoAuto.getAfectado().getTipoPersona().equals("F")) {
                        persona.setVisibility(View.VISIBLE);
                        tipoPersona.setSelection(0);
                        persona.setVisibility(View.VISIBLE);
                        cbReclamo.setChecked(true);
                        if (terceroNoAuto.getAfectado().getNombre() != null) {
                            nombreUsuario.get(0).setText(terceroNoAuto.getAfectado().getNombre());
                        }
                        if (terceroNoAuto.getAfectado().getApellidoPaterno() != null) {
                            nombreUsuario.get(1).setText(terceroNoAuto.getAfectado().getApellidoPaterno());
                        }
                        if (terceroNoAuto.getAfectado().getApellidoMaterno() != null) {
                            nombreUsuario.get(2).setText(terceroNoAuto.getAfectado().getApellidoMaterno());
                        }
                        if (terceroNoAuto.getAfectado().getGenero() != null) {
                            setSexo(terceroNoAuto);
                        } else {
                            spGenero.check(R.id.rbMasculino);
                        }

                        if (terceroNoAuto.getAfectado().getRfc() != null && !terceroNoAuto.getAfectado().getRfc().equals("")) {
                            edtRFC.setText(terceroNoAuto.getAfectado().getRfc());
                        }
                        if (terceroNoAuto.getAfectado().getFechaNacimiento() != null) {
                            dateNacimiento.setText(DateUtils.date(terceroNoAuto.getAfectado().getFechaNacimiento()));
                            String fechaN = DateUtils.date(terceroNoAuto.getAfectado().getFechaNacimiento());
                            parts = fechaN.split("/");
                            diaW = parts[0];
                            mesW = parts[1];
                            anoW = parts[2];
                            if (anoActual - Integer.parseInt(anoW) != 0) {
                                int years = Years
                                        .yearsBetween(LocalDate.fromDateFields(terceroNoAuto.getAfectado().getFechaNacimiento()), LocalDate.now())
                                        .getYears();
                                edad.setText(Integer.toString(years));
                            }
                            if (edad.getText().toString().contains("-")) {
                                edad.setText("");
                            }
                        }
                        if (!terceroNoAuto.getAfectado().getTelefonos().isEmpty()) {
                            telefonoNoAutoAdapter.clear();
                            telefonoNoAutoAdapter.addAll(terceroNoAuto.getAfectado().getTelefonos());
                            rvTelefonos.setAdapter(telefonoNoAutoAdapter);
                            telefonoNoAutoAdapter.notifyDataSetChanged();
                        }
                        if (!terceroNoAuto.getAfectado().getCorreos().isEmpty()) {
                            correoAdapter.clear();
                            correoAdapter.addAll(terceroNoAuto.getAfectado().getCorreos());
                            rvCorreos.setAdapter(correoAdapter);
                            correoAdapter.notifyDataSetChanged();
                        }
                        if (terceroNoAuto.getCalle() != null) {
                            calle.setText(terceroNoAuto.getCalle());
                        }
                        if (terceroNoAuto.getEntreCalles() != null) {
                            edtEntreCalles.setText(terceroNoAuto.getEntreCalles());
                        }
                        if (terceroNoAuto.getNumeroExterior() != null) {
                            edtNumEx.setText(terceroNoAuto.getNumeroExterior());
                        }
                        if (terceroNoAuto.getNumeroInterior() != null) {
                            edtNumIn.setText(terceroNoAuto.getNumeroInterior());
                        }
                        if (terceroNoAuto.getKilometro() != null) {
                            edtKm.setText(terceroNoAuto.getKilometro());
                        }
                        if (terceroNoAuto.getCodigoPostal() != null) {
                            edtPostal.setText(terceroNoAuto.getCodigoPostal());
                        }
                        if (terceroNoAuto.getReferencias() != null) {
                            danosList.get(2).setText(terceroNoAuto.getReferencias());
                        }
                    } else {
                        razonContainer.setVisibility(View.VISIBLE);
                        tipoPersona.setSelection(1);
                        if (terceroNoAuto.getReclamo()) {
                            cbReclamo.setChecked(true);
                            persona.setVisibility(View.GONE);
                            findViewById(R.id.persona_form).setVisibility(GONE);
                        } else {
                            cbReclamo.setChecked(false);
                            persona.setVisibility(View.VISIBLE);
                            findViewById(R.id.persona_form).setVisibility(GONE);
                        }
                        if (terceroNoAuto.getAfectado() != null) {
                            if (terceroNoAuto.getAfectado().getNombre() != null) {
                                edtRazon.setText(terceroNoAuto.getAfectado().getNombre());
                            }
                            if (terceroNoAuto.getAfectado().getRfc() != null && !terceroNoAuto.getAfectado().getRfc().equals("")) {
                                edtRFC.setText(terceroNoAuto.getAfectado().getRfc());
                            }
                            if (terceroNoAuto.getCalle() != null) {
                                calle.setText(terceroNoAuto.getCalle());
                            }
                            if (terceroNoAuto.getEntreCalles() != null) {
                                edtEntreCalles.setText(terceroNoAuto.getEntreCalles());
                            }
                            if (terceroNoAuto.getNumeroExterior() != null) {
                                edtNumEx.setText(terceroNoAuto.getNumeroExterior());
                            }
                            if (terceroNoAuto.getNumeroInterior() != null) {
                                edtNumIn.setText(terceroNoAuto.getNumeroInterior());
                            }
                            if (terceroNoAuto.getKilometro() != null) {
                                edtKm.setText(terceroNoAuto.getKilometro());
                            }
                            if (terceroNoAuto.getCodigoPostal() != null) {
                                edtPostal.setText(terceroNoAuto.getCodigoPostal());
                            }
                            if (terceroNoAuto.getReferencias() != null) {
                                danosList.get(2).setText(terceroNoAuto.getReferencias());
                            }
                            if (!terceroNoAuto.getAfectado().getTelefonos().isEmpty()) {
                                telefonoNoAutoAdapter.clear();
                                telefonoNoAutoAdapter.addAll(terceroNoAuto.getAfectado().getTelefonos());
                                rvTelefonos.setAdapter(telefonoNoAutoAdapter);
                                telefonoNoAutoAdapter.notifyDataSetChanged();
                            }
                            if (!terceroNoAuto.getAfectado().getCorreos().isEmpty()) {
                                correoAdapter.clear();
                                correoAdapter.addAll(terceroNoAuto.getAfectado().getCorreos());
                                rvCorreos.setAdapter(correoAdapter);
                                correoAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
        } else {
            isUploaded = false;
        }

    }


    private Completable getCatalogos() {
        return Completable.mergeArrayDelayError(
                downloadCategoriaNoAuto(),
                getCatalogosEstados(),
                downloadTiposTelefonos()
        );

    }

    private Completable downloadTiposTelefonos() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getTiposTelefono()
                    .subscribe(telefonos -> {
                        telefonosList = telefonos;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al descargar tipos de telefonos", throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }));

        });
    }


    private void getColonies(String id1, String id2) {

        getCompositeDisposable().add(catInteractor.getColonias(id1, id2)
                .subscribe(this::fillColonias,
                        throwable -> {
                            Log.e(TAG, "Error al descargar colonias", throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);
                        }));

    }

    private void fillColonias(List<ColoniaResumen> mun) {
        coloniesList = mun;
        AdapterSpinnerGeneral<ColoniaResumen> adapterColonies = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, coloniesList, ColoniaResumen::getNombre);
        adapterColonies.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spColoniaDialog.setAdapter(adapterColonies);
        spColoniaDialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                coloniesSelected = coloniesList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                throw new UnsupportedOperationException();
            }
        });
        if (isChangeSpinner) {
            TerceroNoAuto data = new Gson().fromJson(getIntent().getExtras().getString(Constants.FOUND_NO_AUTO), TerceroNoAuto.class);
            if (data.getColonia() != null) {
                for (int b = 0; b < coloniesList.size(); b++) {
                    if (Objects.equals(data.getColonia(), coloniesList.get(b).getClave())) {
                        spColoniaDialog.setSelection(b);
                        break;
                    }
                }
            }
            isChangeSpinner = false;
        }
    }

    private void getMunicipalities(String idState) {
        getCompositeDisposable().add(catInteractor.getMunicipios(idState)
                .subscribe(this::fillDataMunicipalitiesSpin,
                        throwable -> {
                            Log.e(TAG, "Error al descargar municipios", throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);
                        }));
    }

    private void fillDataMunicipalitiesSpin(List<Municipio> mun) {
        municipalityList = mun;
        AdapterSpinnerGeneral<Municipio> adapterMunicipality = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, municipalityList, Municipio::getNombre);
        adapterMunicipality.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMunicipioDialog.setAdapter(adapterMunicipality);
        spMunicipioDialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                municipalitySelected = municipalityList.get(position);
                getColonies(statesSelected.getClave(),
                        municipalitySelected.getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                throw new UnsupportedOperationException();
            }
        });
        if (isChangeSpinner) {
            TerceroNoAuto data = new Gson().fromJson(getIntent().getExtras().getString(Constants.FOUND_NO_AUTO), TerceroNoAuto.class);
            if (data.getMunicipio() != null) {
                for (int l = 0; l < municipalityList.size(); l++) {
                    if (Objects.equals(data.getMunicipio(), municipalityList.get(l).getClave())) {
                        spMunicipioDialog.setSelection(l);
                        break;
                    }
                }
            }
        }
    }

    private Completable getCatalogosEstados() {
        return Completable.create(e -> {
            getCompositeDisposable().add(CatalogoInteractorsFactory.getCatInteractor().getEstados()
                    .subscribe(estados -> {
                        statesList = estados;
                        AdapterSpinnerGeneral<CatalogoClaveInfo> adapterStates = new AdapterSpinnerGeneral<>(activity, R.layout.spinner_item, statesList, CatalogoClaveInfo::getNombre);
                        adapterStates.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spEstadoDialog.setAdapter(adapterStates);

                        spEstadoDialog.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                statesSelected = statesList.get(position);
                                getMunicipalities(statesSelected.getClave());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                throw new UnsupportedOperationException();
                            }
                        });
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spEstadoDialog.setAdapter(null);
                        Log.e(TAG, "Error al descargar estados", throwable);
                    }));

        });


    }


    private void downloadTipoNoAuto(Long categoria) {
        getCompositeDisposable().add(siniestrosInteractor.getTiposTerceroNoAuto(categoria)
                .subscribe(tiposNoAutos -> {
                    tipoNoAutos = tiposNoAutos;
                    AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, tipoNoAutos, CatalogoId::getNombre);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spNoAuto.get(1).setAdapter(adapter);
                    if (terceroNoAuto != null && terceroNoAuto.getTipoNoAuto() != null) {
                        for (int k = 0; k < tipoNoAutos.size(); k++) {
                            if (terceroNoAuto.getTipoNoAuto().equals(tipoNoAutos.get(k).getId())) {
                                spNoAuto.get(1).setSelection(k);
                                break;
                            }
                        }
                    }
                }, throwable -> {
                    spNoAuto.get(1).setAdapter(null);
                    Log.e(TAG, "Error al descargar tiposNoAuto", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                })
        );

    }


    private void setSexo(TerceroNoAuto response) {
        if ("M".equals(response.getAfectado().getGenero())) {
            spGenero.check(R.id.rbMasculino);
        } else if ("F".equals(response.getAfectado().getGenero())) {
            spGenero.check(R.id.rbFemenino);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        generalCameraFragment.onActivityResult(requestCode, resultCode, data);
    }

    private void showDatePikerDialog() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateNacimiento.setText(date);
            if (age.contains("-")) {
                edad.setText("");
            }else {
                edad.setText(age);

            }
        });
        datePickerFragment.show(getSupportFragmentManager(), "dataPickerBirthday");
    }

    private String calculateAge(int year, int month, int day) {
        LocalDate birthdate = new LocalDate(year, month, day);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);

        return String.valueOf(age.getYears());
    }

    @OnClick(R.id.uploadImages)
    public void sendImages() {
        ImageUtils imageUtils = new ImageUtils();
        List<ImageGeneralModel> imageList = generalCameraFragment.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        if (!images.isEmpty()) {
            int size = images.size();
            btnUploadImages.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            getCompositeDisposable().add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> terceroNoAutoService.addFotografia(idAjuste, idNoAuto, file).toObservable(), false, 1)
                    .doOnEach(fotografias -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        }
                    })
                    .onErrorReturn(throwable -> {
                        Log.e(TAG, "Error al subir fotografía del no auto", throwable);
                        return new Fotografia();
                    })
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        boolean ok = true;
                        for (Fotografia pic : fotografias) {
                            ok &= pic.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(this, ResourceUtils.getString("imagenes_upload_ok"));
                        } else {
                            new GeneralSnackBar().largeSnackError(this, getResources().getString(R.string.imagenes_upload_error));
                        }
                        btnUploadImages.setEnabled(true);
                        imageUtils.renameImageUpload(imageList);
                        imageUtils.deleteImage(getBaseActivity(), imageList);
                        filePhotos.clear();
                        updatePhotosNoAuto();
                    }, throwable -> {
                        btnUploadImages.setEnabled(true);
                        Log.e(TAG, "Error al subir imagenes del tercero no auto " + idNoAuto + " del ajuste " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);

                    }));

        } else {
            new GeneralSnackBar().largeSnackError(TerceroNoAutoActivity.this, getString(R.string.image_void));
            btnUploadImages.setEnabled(true);
        }
    }

    private void updatePhotosNoAuto() {
        getCompositeDisposable().add(terceroNoAutoService.getFotografias(idAjuste, idNoAuto)
                .subscribe(fotoResponses -> {
                    btnUploadImages.setEnabled(true);
                    for (Fotografia photo : fotoResponses) {
                        filePhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                    }
                    generalCameraFragment.updateImageList(filePhotos);

                }, throwable -> {
                    btnUploadImages.setEnabled(true);
                    String message = ErrorUtils.getErrorMessage(throwable);
                    Log.e(TAG, "Error al obtener imagenes de tercero no auto: " + idNoAuto + " del ajuste " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);

                }));
    }


    private Completable downloadCategoriaNoAuto() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getCategoriaTerceroNoAuto()
                    .subscribe(categorias -> {
                        noAutos = categorias;
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, noAutos, CatalogoId::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spNoAuto.get(0).setAdapter(adapter);
                        spNoAuto.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                downloadTipoNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                                throw new UnsupportedOperationException();
                            }
                        });
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spNoAuto.get(0).setAdapter(null);
                        Log.e(TAG, "Error al descargar categoria de tercero  no auto ", throwable);
                    })
            );

        });

    }


    private void showDialogAddPhone() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar teléfono")
                .customView(R.layout.dialog_add_phone, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                    if (phone.getText().toString().trim().length() < 10) {
                        phone.setError(ResourceUtils.getString("invalid_number"));
                        return;
                    } else {
                        Telefono telefono = new Telefono(phone.getText().toString(), clave, extencion.equals("") ? null : extencion);
                        telefonoNoAutoAdapter.add(telefono);
                    }
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        spTipoTelefono = (Spinner) builder.findViewById(R.id.spinnerTipoTelefono);
        AdapterSpinnerGeneral<CatalogoClaveAmis> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.spinner_item, telefonosList, CatalogoClave::getNombre);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoTelefono.setAdapter(adapter);
        phone = (EditText) builder.findViewById(R.id.etPhone);
        TextView tituloExtencion = (TextView) builder.findViewById(R.id.titleExtencion);
        phone.requestFocus();
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        extension = (EditText) builder.findViewById(R.id.etExtension);
        spTipoTelefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                clave = telefonosList.get(i).getClave();
                if (((CatalogoClave)spTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                    extension.setVisibility(View.VISIBLE);
                    tituloExtencion.setVisibility(View.VISIBLE);
                } else {
                    extension.setVisibility(View.GONE);
                    tituloExtencion.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        builder.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void deletePhotoNoAuto(long id) {
        getCompositeDisposable().add(terceroNoAutoService.deleteFotografia(idAjuste, idNoAuto, id)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(TerceroNoAutoActivity.this, "Se elimino correctamente la fotografía del no auto");
                }, throwable -> {
                    Log.e(TAG, "Error al eliminar imagen de tercero no auto: " + idNoAuto + " del ajuste:" + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));

    }

    @OnClick({R.id.btnGuardarVehiculoNoAuto, R.id.btnAgregarTelefonos, R.id.btnAgregarCorreos})
    public void printNoAuto(View v) {
        switch (v.getId()) {
            case R.id.btnGuardarVehiculoNoAuto:
                btnGuardar.setEnabled(false);
                addOrUpdateNoAuto();
                break;
            case R.id.btnAgregarTelefonos:
                showDialogAddPhone();
                break;
            case R.id.btnAgregarCorreos:
//                dialog = new Dialog(this);
//                dialog.setContentView(R.layout.dialog_add_email);
//                EditText item = dialog.findViewById(R.id.edtCorreoConductor);
//                Button btnSaveEmail = dialog.findViewById(R.id.btnGuardarEmail);
//                btnSaveEmail.setOnClickListener(view -> {
//                    if (item.getText().toString().equals("")) {
//                        new GeneralSnackBar().largeSnackError(AgregarNoAutoTerceroActivity.this, "No deje vacio este campo");
//                        return;
//                    }
//                    if (!item.getText().toString().contains("@")) {
//                        new GeneralSnackBar().largeSnackError(AgregarNoAutoTerceroActivity.this, "Coloque una dirección de correo correcta");
//                    } else {
//                        correoAdapter.add(item.getText().toString());
//                        dialog.dismiss();
//                    }
//                });
//                dialog.show();
                MaterialDialog builder = new MaterialDialog.Builder(this)
                        .title("Agregar correo electrónico")
                        .customView(R.layout.dialog_add_email, true)
                        .positiveText("Aceptar")
                        .onPositive((dialog, which) -> {

                        })
                        .negativeText("Cancelar")
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .build();

                email = (EditText) builder.findViewById(R.id.edtCorreoConductor);
                builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
                MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
                cancelar.setAllCaps(false);
                aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(),R.drawable.background_arround_actived));
                aceptar.setTextColor(Color.WHITE);
                aceptar.setAllCaps(false);
                aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_email));
                builder.show();
                break;
        }

    }


    private void addOrUpdateNoAuto() {
        Afectado afectado = new Afectado();
        if (idNoAuto == null) {
            if (cbReclamo.isChecked()) {
                if (tipoPersona.getSelectedItem().toString().equals("Fisica")) {
                    afectado.setTipoPersona("F");
                    if (nombreUsuario.get(0).getText().toString().equals("")) {
                        nombreUsuario.get(0).setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        afectado.setNombre(nombreUsuario.get(0).getText().toString());
                    }
                    if (nombreUsuario.get(1).getText().toString().equals("")) {
                        nombreUsuario.get(1).setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        afectado.setApellidoPaterno(nombreUsuario.get(1).getText().toString());
                    }
                    afectado.setApellidoMaterno(nombreUsuario.get(2).getText().toString());
                    if (dateNacimiento.getText().toString().equals("")) {
                        dateNacimiento.setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        afectado.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
                    }
                    if (edtRFC.getText().toString().equals("")) {
                        edtRFC.setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                    } else {
                        if (edtRFC.getText().length() == 10) {
                            afectado.setRfc(edtRFC.getText().toString() + "XXX");
                        } else {
                            afectado.setRfc(edtRFC.getText().toString());
                        }
                    }
                    afectado.setGenero(genero);
                    afectado.setCorreos(correoAdapter.getCorreos());
                    afectado.setTelefonos(telefonoNoAutoAdapter.getTelefonos());
                    noAutoData.setAfectado(afectado);
                    noAutoData.setCategoriaNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
                    noAutoData.setTipoNoAuto(tipoNoAutos.get(spNoAuto.get(1).getSelectedItemPosition()).getId());
                    noAutoData.setEstado(statesSelected.getClave());
                    noAutoData.setMunicipio(municipalitySelected.getClave());
                    noAutoData.setColonia(coloniesSelected.getClave());
                    noAutoData.setReclamo(true);
                    //noAutoData.setTipoUbicacion(data.getSiniestro().getUbicacion().getTipoUbicacion().getKey() != null ? data.getSiniestro().getUbicacion().getTipoUbicacion().getKey() : "xd");//TODO: quitar esta linea hasta nuevo ai
                    if (danosList.get(0).toString().equals("")) {
                        danosList.get(0).setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setAreaDanada(danosList.get(0).getText().toString());
                    }
                    if (calle.getText().toString().equals("")) {
                        calle.setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setCalle(calle.getText().toString());
                    }
                    if (edtPostal.getText().toString().equals("")) {
                        edtPostal.setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setCodigoPostal(edtPostal.getText().toString());
                    }
                    if (danosList.get(1).toString().equals("")) {
                        danosList.get(1).setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setDanoPreexistente(danosList.get(1).getText().toString());
                    }
                    if (edtEntreCalles.getText().toString().equals("")) {
                        edtEntreCalles.setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setEntreCalles(edtEntreCalles.getText().toString());
                    }
                    noAutoData.setKilometro(edtKm.getText().toString());

                    if (edtNumEx.getText().toString().equals("")) {
                        edtNumEx.setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setNumeroExterior(edtNumEx.getText().toString());
                    }
                    noAutoData.setNumeroInterior(edtNumIn.getText().toString());
                    if (danosList.get(2).getText().toString().equals("")) {
                        danosList.get(2).setError(ResourceUtils.getString("value_error"));
                        btnGuardar.setEnabled(true);
                        return;
                    } else {
                        noAutoData.setReferencias(danosList.get(2).getText().toString());
                    }
                } else if (tipoPersona.getSelectedItem().toString().equals("Moral")) {
                    afectado.setTipoPersona("M");
                    if (!edtRazon.getText().toString().equals("")) {
                        noAutoData = new TerceroNoAuto();
                        //noAutoData.setDescripcion("Sin información del no auto");
                        if (edtRazon.getText().toString().equals("")) {
                            edtRazon.setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            afectado.setNombre(edtRazon.getText().toString());
                        }
                        if (edtRFC.getText().toString().equals("")) {
                            edtRFC.setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            afectado.setRfc(edtRFC.getText().toString());
                        }
                        afectado.setCorreos(correoAdapter.getCorreos());
                        afectado.setTelefonos(telefonoNoAutoAdapter.getTelefonos());
                        noAutoData.setAfectado(afectado);
                        noAutoData.setCategoriaNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
                        noAutoData.setTipoNoAuto(tipoNoAutos.get(spNoAuto.get(1).getSelectedItemPosition()).getId());
                        noAutoData.setEstado(statesSelected.getClave());
                        noAutoData.setMunicipio(municipalitySelected.getClave());
                        noAutoData.setColonia(coloniesSelected.getClave());
                        noAutoData.setReclamo(true);
                        //noAutoData.setTipoUbicacion(data.getSiniestro().getUbicacion().getTipoUbicacion().getKey() != null ? data.getSiniestro().getUbicacion().getTipoUbicacion().getKey() : "xd");
                        if (danosList.get(0).toString().equals("")) {
                            danosList.get(0).setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setAreaDanada(danosList.get(0).getText().toString());
                        }
                        if (calle.getText().toString().equals("")) {
                            calle.setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setCalle(calle.getText().toString());
                        }
                        if (edtPostal.getText().toString().equals("")) {
                            edtPostal.setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setCodigoPostal(edtPostal.getText().toString());
                        }
                        if (danosList.get(1).getText().toString().equals("")) {
                            danosList.get(1).setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setDanoPreexistente(danosList.get(1).getText().toString());
                        }
                        if (edtEntreCalles.getText().toString().equals("")) {
                            edtEntreCalles.setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setEntreCalles(edtEntreCalles.getText().toString());
                        }
                        noAutoData.setKilometro(edtKm.getText().toString());
                        if (edtNumEx.getText().toString().equals("")) {
                            edtNumEx.setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setNumeroExterior(edtNumEx.getText().toString());
                        }
                        noAutoData.setNumeroInterior(edtNumIn.getText().toString());

                        if (danosList.get(2).getText().toString().equals("")) {
                            danosList.get(2).setError(ResourceUtils.getString("value_error"));
                            btnGuardar.setEnabled(true);
                            return;
                        } else {
                            noAutoData.setReferencias(danosList.get(2).getText().toString());
                        }

                    }
                }

            } else {
                noAutoData.setCategoriaNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
                noAutoData.setTipoNoAuto(tipoNoAutos.get(spNoAuto.get(1).getSelectedItemPosition()).getId());
                if (danosList.get(0).getText().toString().equals("")) {
                    danosList.get(0).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);

                } else {
                    noAutoData.setAreaDanada(danosList.get(0).getText().toString());
                }
                if (danosList.get(1).getText().toString().equals("")) {
                    danosList.get(1).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                } else {
                    noAutoData.setDanoPreexistente(danosList.get(1).getText().toString());
                }
                noAutoData.setReclamo(false);
            }
            if (correoAdapter.getCorreos().size() == 0 && cbReclamo.isChecked()) {
                Toast.makeText(getApplicationContext(), getString(R.string.correo_mesage), Toast.LENGTH_LONG).show();
                btnGuardar.setEnabled(true);
                return;
            }


            getCompositeDisposable().add(terceroNoAutoService.add(idAjuste, noAutoData)
                    .subscribe(noAutoResponse -> {
                        btnGuardar.setEnabled(true);
                        isUploaded = true;
                        idNoAuto = noAutoResponse.getId();
                        generalCameraFragment.setFolder(MODULO_TERCEROS, 0, TIPO_NO_AUTO, "", String.valueOf(idNoAuto));
                        ModuleNoAuto moduleNoAuto = new ModuleNoAuto(String.valueOf(idNoAuto), TERCEROS, NO_AUTOS, "", idAjuste, String.valueOf(idNoAuto));
                        path = getImagePathNoAuto(moduleNoAuto);
                        if (!getAllImages(getBaseActivity(), path).isEmpty()) sendImages();
                        new GeneralSnackBar().largeSnackOk(TerceroNoAutoActivity.this, "Se agregó correctamente el no auto");
                        btnUploadImages.setVisibility(View.VISIBLE);

                    }, throwable -> {
                        btnGuardar.setEnabled(true);
                        isUploaded = false;
                        Log.e(TAG, "Error al agregar tercero no auto del ajuste:" + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);


                    }));
        } else {
            updateRequest(afectado);
        }
    }


    private void updateRequest(Afectado afectado) {
        if (cbReclamo.isChecked()) {
            if (tipoPersona.getSelectedItem().toString().equals("Fisica")) {
                afectado.setTipoPersona("F");
                if (nombreUsuario.get(0).getText().toString().equals("")) {
                    nombreUsuario.get(0).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    afectado.setNombre(nombreUsuario.get(0).getText().toString());
                }
                if (nombreUsuario.get(1).getText().toString().equals("")) {
                    nombreUsuario.get(1).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    afectado.setApellidoPaterno(nombreUsuario.get(1).getText().toString());
                }
                afectado.setApellidoMaterno(nombreUsuario.get(2).getText().toString());

                if (dateNacimiento.getText().toString().equals("")) {
                    dateNacimiento.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    afectado.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
                }
                if (edtRFC.getText().toString().equals("")) {
                    edtRFC.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    if (edtRFC.getText().length() == 10) {
                        afectado.setRfc(edtRFC.getText().toString() + "XXX");
                    } else {
                        afectado.setRfc(edtRFC.getText().toString());
                    }
                }
                afectado.setGenero(genero);
                afectado.setTelefonos(telefonoNoAutoAdapter.getTelefonos());
                afectado.setCorreos(correoAdapter.getCorreos());
                noAutoData.setAfectado(afectado);
                noAutoData.setCategoriaNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
                noAutoData.setTipoNoAuto(tipoNoAutos.get(spNoAuto.get(1).getSelectedItemPosition()).getId());
                noAutoData.setEstado(statesSelected.getClave());
                noAutoData.setMunicipio(municipalitySelected.getClave());
                noAutoData.setColonia(coloniesSelected.getClave());
                noAutoData.setReclamo(true);
                if (danosList.get(0).toString().equals("")) {
                    danosList.get(0).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setAreaDanada(danosList.get(0).getText().toString());
                }
                if (calle.getText().toString().equals("")) {
                    calle.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setCalle(calle.getText().toString());
                }
                if (edtPostal.getText().toString().equals("")) {
                    edtPostal.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setCodigoPostal(edtPostal.getText().toString());
                }
                if (danosList.get(1).toString().equals("")) {
                    danosList.get(1).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setDanoPreexistente(danosList.get(1).getText().toString());
                }
                if (edtEntreCalles.getText().toString().equals("")) {
                    edtEntreCalles.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setEntreCalles(edtEntreCalles.getText().toString());
                }
                noAutoData.setKilometro(edtKm.getText().toString());

                if (edtNumEx.getText().toString().equals("")) {
                    edtNumEx.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setNumeroExterior(edtNumEx.getText().toString());
                }
                noAutoData.setNumeroInterior(edtNumIn.getText().toString());


                if (danosList.get(2).getText().toString().equals("")) {
                    danosList.get(2).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setReferencias(danosList.get(2).getText().toString());
                }
            } else if (tipoPersona.getSelectedItem().toString().equals("Moral")) {
                afectado.setTipoPersona("M");
                if (edtRazon.getText().toString().equals("")) {
                    edtRazon.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    afectado.setNombre(edtRazon.getText().toString());
                }
                if (edtRFC.getText().toString().equals("")) {
                    edtRFC.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    afectado.setRfc(edtRFC.getText().toString());
                }
                afectado.setTelefonos(telefonoNoAutoAdapter.getTelefonos());
                afectado.setCorreos(correoAdapter.getCorreos());
                noAutoData.setAfectado(afectado);
                noAutoData.setCategoriaNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
                noAutoData.setTipoNoAuto(tipoNoAutos.get(spNoAuto.get(1).getSelectedItemPosition()).getId());
                noAutoData.setEstado(statesSelected.getClave());
                noAutoData.setMunicipio(municipalitySelected.getClave());
                noAutoData.setColonia(coloniesSelected.getClave());
                noAutoData.setReclamo(true);
                //noAutoData.setTipoUbicacion(data.getSiniestro().getUbicacion().getTipoUbicacion().getKey() != null ? data.getSiniestro().getUbicacion().getTipoUbicacion().getKey() : "xd");
                if (danosList.get(0).toString().equals("")) {
                    danosList.get(0).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setAreaDanada(danosList.get(0).getText().toString());
                }
                if (calle.getText().toString().equals("")) {
                    calle.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setCalle(calle.getText().toString());
                }
                if (edtPostal.getText().toString().equals("")) {
                    edtPostal.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setCodigoPostal(edtPostal.getText().toString());
                }
                if (danosList.get(1).toString().equals("")) {
                    danosList.get(1).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setDanoPreexistente(danosList.get(1).getText().toString());
                }
                if (edtEntreCalles.getText().toString().equals("")) {
                    edtEntreCalles.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setEntreCalles(edtEntreCalles.getText().toString());
                }
                noAutoData.setKilometro(edtKm.getText().toString());
                if (edtNumEx.getText().toString().equals("")) {
                    edtNumEx.setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setNumeroExterior(edtNumEx.getText().toString());
                }
                noAutoData.setNumeroInterior(edtNumIn.getText().toString());


                if (danosList.get(2).getText().toString().equals("")) {
                    danosList.get(2).setError(ResourceUtils.getString("value_error"));
                    btnGuardar.setEnabled(true);
                    return;
                } else {
                    noAutoData.setReferencias(danosList.get(2).getText().toString());
                }
            }

        } else {
            noAutoData = new TerceroNoAuto();
            noAutoData.setCategoriaNoAuto(noAutos.get(spNoAuto.get(0).getSelectedItemPosition()).getId());
            noAutoData.setTipoNoAuto(tipoNoAutos.get(spNoAuto.get(1).getSelectedItemPosition()).getId());
            if (danosList.get(0).getText().toString().equals("")) {
                danosList.get(0).setError(ResourceUtils.getString("value_error"));
                btnGuardar.setEnabled(true);
                return;
            } else {
                noAutoData.setAreaDanada(danosList.get(0).getText().toString());
            }
            if (danosList.get(1).getText().toString().equals("")) {
                danosList.get(1).setError(ResourceUtils.getString("value_error"));
                btnGuardar.setEnabled(true);
                return;
            } else {
                noAutoData.setDanoPreexistente(danosList.get(1).getText().toString());
            }
            noAutoData.setReclamo(false);
        }

        getCompositeDisposable().add(terceroNoAutoService.modify(idAjuste, idNoAuto, noAutoData)
                .subscribe(() -> {
                    btnGuardar.setEnabled(true);
                    new GeneralSnackBar().largeSnackOk(TerceroNoAutoActivity.this, "Se actualizó correctamente el no auto");
                    btnUploadImages.setVisibility(View.VISIBLE);

                }, throwable -> {
                    btnGuardar.setEnabled(true);
                    Log.e(TAG, "Error al modificar el tercero no auto del ajuste" + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);

                }));

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void viewPhoto(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    @Override
    public void onItemClick(View view, int position) {
        DialogUtils.createAlert(this, "Eliminar correo...",
                "¿Desea eliminar este correo?",
                getString(R.string.btn_accept_text),
                getString(R.string.btn_cancel_text), (dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    correoAdapter.remove(correoAdapter.getCorreo(position));
                }, (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    @Override
    public void onDeleteClick(View view, int position) {
        DialogUtils.createAlert(this, "Eliminar número...",
                "¿Desea eliminar este número?",
                getString(R.string.btn_accept_text),
                getString(R.string.btn_cancel_text),
                (dialog, which) -> {
                    Log.d(TAG, "onClick: " + position);
                    telefonoNoAutoAdapter.remove(telefonoNoAutoAdapter.getTelefono(position));
                }, (dialog, which) -> dialog.dismiss());
    }

    @Override
    public void onBackPressed() {
        if (!isUploaded) {
            DialogUtils.createAlert(this, "Salir",
                    getString(R.string.image_message_out),
                    "Aceptar", "Cancelar",
                    (dialog, which) -> {
                        if (noAutoData != null) {
                            bus.postSticky(noAutoData);
                            TerceroNoAutoActivity.super.onBackPressed();
                        } else {
                            TerceroNoAutoActivity.super.onBackPressed();
                        }
                    },
                    (dialog, which) -> dialog.dismiss());
        } else {
            bus.post(noAutoData);
            super.onBackPressed();
        }
    }


    @Override
    public void onCallbackItemId(int position, Long id) {
        deletePhotoNoAuto(id);
    }

    @Override
    public void onCallItemId(int position, Long id) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        String nombre = nombreUsuario.get(0).getText().toString().trim();
        if (!FormatStrings.validateText(nombre)) {
            nombreUsuario.get(0).setError(getString(R.string.message_character_special));
        }
        String apellidoP = nombreUsuario.get(1).getText().toString().trim();

        if (!FormatStrings.validateText(apellidoP)) {
            nombreUsuario.get(1).setError(getString(R.string.message_character_special));
        }
        String apellidoM = nombreUsuario.get(2).getText().toString().trim();

        if (!FormatStrings.validateText(apellidoM)) {
            nombreUsuario.get(2).setError(getString(R.string.message_character_special));
        }
    }

    private void setEvents() {
        nombreUsuario.get(0).addTextChangedListener(this);
        nombreUsuario.get(1).addTextChangedListener(this);
        nombreUsuario.get(2).addTextChangedListener(this);
    }


    private void initListeners() {


        cbReclamo.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                if (Constants.PERSONA_FISICA.equals(tipoPersona.getSelectedItem().toString())) {
                    persona.setVisibility(View.VISIBLE);
                    findViewById(R.id.persona_form).setVisibility(View.VISIBLE);
                    razonContainer.setVisibility(GONE);
                    findViewById(R.id.razon_spinner).setVisibility(View.VISIBLE);
                    findViewById(R.id.beneficiaria_content).setVisibility(View.VISIBLE);
                }
                if (Constants.PERSONA_MORAL.equals(tipoPersona.getSelectedItem().toString())) {
                    persona.setVisibility(View.VISIBLE);
                    findViewById(R.id.persona_form).setVisibility(View.GONE);
                    razonContainer.setVisibility(View.VISIBLE);
                    findViewById(R.id.razon_spinner).setVisibility(View.VISIBLE);
                    findViewById(R.id.beneficiaria_content).setVisibility(View.VISIBLE);
                }
            } else {
                persona.setVisibility(View.GONE);
                findViewById(R.id.persona_form).setVisibility(View.GONE);
                razonContainer.setVisibility(View.GONE);
                findViewById(R.id.razon_spinner).setVisibility(GONE);
                findViewById(R.id.beneficiaria_content).setVisibility(GONE);
            }
        });
        spGenero.setOnCheckedChangeListener((radioGroup, id) -> {
            if (id == R.id.rbMasculino) {
                genero = "M";
            }
            if (id == R.id.rbFemenino) {
                genero = "F";
            }
            Log.d(TAG, "onCheckedChanged: " + genero);
        });
        dateNacimiento.setOnClickListener(v -> {
            dateNacimiento.setError(null);
            showDatePikerDialog();
        });
        tipoPersona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (Constants.PERSONA_FISICA.equals(tipoPersona.getSelectedItem())) {
                    if (cbReclamo.isChecked()) {
                        edtRFC.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
                        persona.setVisibility(View.VISIBLE);
                        razonContainer.setVisibility(GONE);
                        findViewById(R.id.persona_form).setVisibility(View.VISIBLE);
                    } else {
                        persona.setVisibility(View.GONE);
                        findViewById(R.id.persona_form).setVisibility(GONE);
                    }
                } else {
                    if (cbReclamo.isChecked()) {
                        edtRFC.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                        findViewById(R.id.persona_form).setVisibility(View.GONE);
                        persona.setVisibility(View.VISIBLE);
                        razonContainer.setVisibility(View.VISIBLE);
                    } else {
                        findViewById(R.id.persona_form).setVisibility(View.GONE);
                        persona.setVisibility(View.GONE);
                        razonContainer.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone.getText().toString().trim().length() < 10) {
                    phone.setError(ResourceUtils.getString("invalid_number"));
                    if (((CatalogoClave)spTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                        if (extension.getText().toString().trim().length() < 6) {
                            extension.setError(ResourceUtils.getString("extension_error"));
                        }
                    }
                } else {
                    dialog.dismiss();
                }
            }
             else {
                if (email.getText().toString().equals("")) {
                    email.setError("No deje vacio este campo");
                    return;
                }
                else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError("Coloque una dirección de correo correcta");
                    return;
                }  else {
                    correoAdapter.add(email.getText().toString());
                    correoAdapter.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        }
    }

    private void addFragment(Fragment fragment, String tag) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraFragment, fragment, tag);
        transaction.commit();
    }


}
