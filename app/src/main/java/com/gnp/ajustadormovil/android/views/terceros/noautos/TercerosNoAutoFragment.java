package com.gnp.ajustadormovil.android.views.terceros.noautos;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.ajuste.AjustesFragment;
import com.gnp.ajustadormovil.android.views.ajuste.SiniestrosFragment;
import com.gnp.ajustadormovil.android.views.terceros.TerceroAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemSelectedListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_NO_AUTO;

/**
 * Created by Abraham on 07/12/2017.
 */

public class TercerosNoAutoFragment extends BaseFragment implements OnItemRemovedListener, OnItemSelectedListener {

    private View mView;
    private static final String TAG = SiniestrosFragment.class.getSimpleName();
    private Unbinder mUnbinder;
    @BindView(R.id.rvTerceros)
    RecyclerView rvNoTerceros;
    @BindView(R.id.container_autos)
    RelativeLayout layout;
    @BindView(R.id.title_less)
    TextView title_less;
    private List<TerceroNoAuto> noAutos = new ArrayList<>();
    private TerceroAdapter adapter;
    private Retrofit retrofit;
    private String db_reading_token, idAjuste = "";
    private MaterialDialog dialog;
    private TerceroNoAutoInteractor noAutoService;
    private CompositeDisposable compositeDisposable;
    private Activity activity;

    public static TercerosNoAutoFragment newInstance() {
        TercerosNoAutoFragment noAutosTercero = new TercerosNoAutoFragment();
        noAutosTercero.setRetainInstance(true);
        return noAutosTercero;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        noAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
        initComponents();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_terceros, container, false);
        mUnbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    private void initComponents() {
        db_reading_token = AjustesFragment.PositionToken;
        idAjuste = AjustesFragment.PositionForServicioAjuste;
        adapter = new TerceroAdapter(this, this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvNoTerceros.setHasFixedSize(true);
        setAdapter(adapter);
        rvNoTerceros.setLayoutManager(new LinearLayoutManager(getContext()));
        getNoAutos();
    }

    @OnClick(R.id.btnTercero)
    public void addNoAuto() {
        CameraActivity.id_auto = "";
        CameraActivity.tipoTerceros = TIPO_NO_AUTO;
        Intent intent = new Intent(getActivity(), TerceroNoAutoActivity.class);
        intent.putExtra("foundNoAuto", "");
        intent.putExtra("discriminator", "nuevo");
        intent.putExtra("data", getArguments().getString("data"));
        startActivity(intent);
    }

    public void getNoAutos() {
        compositeDisposable.add(noAutoService.getAll(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                .subscribe(object -> {
                    adapter = new TerceroAdapter(this, this);
                    noAutos = object;
                    if (adapter.getTerceros().size() == 0) {
                        adapter.addAll(noAutos);
                        setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        adapter.clear();
                        adapter.addAll(noAutos);
                        setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                    if (object.size()!=0){
                        title_less.setVisibility(View.GONE);
                    }
                    else {
                        title_less.setVisibility(View.VISIBLE);
                    }
                }, throwable -> {
                    AppPreferences.setString(AppPreferences.ESTATUS_TERCEROS_NO_AUTO, null);
                }));
    }

    private void setAdapter(RecyclerView.Adapter adapter){
        if (rvNoTerceros!=null) {
            rvNoTerceros.setAdapter(adapter);
        }
    }

    @Subscribe
    public void addNoAuto(TerceroNoAuto data) {
        if (data != null) {
            adapter.clear();
            rvNoTerceros.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            getNoAutos();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault ().register (this);

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position < noAutos.size()) {
            Log.d(TAG, "onItemClick: " + noAutos.get(position).getId());
            dialog = new MaterialDialog.Builder(getActivity())
                    .title("Eliminar auto...")
                    .content("¿Desea eliminar este no auto?")
                    .positiveText("Aceptar")
                    .onPositive((dialog, which) -> {
                        ((BaseActivity) getActivity()).getCompositeDisposable().add(noAutoService.delete(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), adapter.getTercero(position).getId())
                                .subscribe(() -> {
                                    //Log.d(TAG, "onResponse: " + object);
                                    getNoAutos();
                                    adapter.remove(adapter.getTercero(position));
                                    rvNoTerceros.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    new GeneralSnackBar().largeSnackOk(getActivity(), "El no auto fue eliminado correctamente...");
                                }, throwable -> {
                                    String message = ErrorUtils.getErrorMessage(throwable);
                                    new GeneralSnackBar().largeSnackError(getActivity(), message);
                                }));
                    })
                    .negativeText("Cancelar")
                    .onNegative((dialog, which) -> dialog.dismiss())
                    .build();
            MDButton aceptar = dialog.getActionButton(DialogAction.POSITIVE);
            MDButton cancelar = dialog.getActionButton(DialogAction.NEGATIVE);
            cancelar.setAllCaps(false);
            aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            aceptar.setBackground(ContextCompat.getDrawable(dialog.getContext(),R.drawable.background_arround_actived));
            aceptar.setTextColor(Color.WHITE);
            aceptar.setAllCaps(false);
            dialog.show();
        }

    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    @Override
    public void selectItem(View view, int position) {
        if (position < noAutos.size()) {
            if (noAutos.get(position) != null) {
                compositeDisposable.add(noAutoService.get(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), noAutos.get(position).getId())
                        .subscribe(object -> {
                            String data = new Gson().toJson(object);
                            Log.d(TAG, "selectItem: " + noAutos.get(position).getId());
                            Intent intent = new Intent(getActivity(), TerceroNoAutoActivity.class);
                            intent.putExtra("id", noAutos.get(position).getId());
                            intent.putExtra("foundNoAuto", data);
                            System.out.println(data);
                            intent.putExtra("data", getArguments().getString("data"));
                            intent.putExtra("discriminator", "actualizacion");
                            startActivity(intent);
                        }, throwable -> {
                            String message = ErrorUtils.getErrorMessage(throwable);
                            new GeneralSnackBar().largeSnackError(getActivity(), message);
                        }));
            }
        }
    }

    private String newMessage(String error) {
        String message = null;
        switch (error) {
            case "tercero_responsable":
                message = "No se puede borrar el tercero porque es el responsable";
                break;
            case "tercero_volante":
                message = "El tercero no se puede eliminar, debido a que se le generó su orden de admisión";
                break;
            case "internal_server_error":
                message = "Ocurrió un problema inesperado";
                break;
        }
        return message;
    }
}

