package com.gnp.ajustadormovil.android.views.terceros.personas;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.ModulePeaton;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.models.ValueObject;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoClaveAmis;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Hospital;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.HospitalResumen;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Supervisoria;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Fotografia;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Telefono;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.FormatStrings;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.adapters.CorreoAdapter;
import com.gnp.ajustadormovil.android.views.commons.adapters.PhoneAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.utils.camera.GeneralCameraFragment;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.gnp.ajustadormovil.android.views.commons.lesiones.GeneralInjuredFragment;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.MODULO_TERCEROS;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_PEATON;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.ZERO;
import static com.gnp.ajustadormovil.android.utils.Constants.TERCEROS;

public class TerceroPersonaActivity extends BaseActivity implements  CallbackItemImage<Long>, TextWatcher {

    @BindView(R.id.toolbar_peatones)
    Toolbar mToolbar;
    @BindView(R.id.etFirstName)
    EditText firstName;
    @BindView(R.id.etLastName)
    EditText lastName;
    @BindView(R.id.etMotherLastName)
    EditText motherLastName;
    @BindView(R.id.etNumeroIdentificacion)
    EditText etNumeroIdentificacion;
    @BindView(R.id.dateNac)
    EditText dateNacimiento;
    @BindView(R.id.btnAddImages)
    Button btnUploadImages;
    @BindView(R.id.cbOtro)
    CheckBox cbOtroHospital;
    @BindView(R.id.layout_estado_emisor)
    LinearLayout layoutEstadoEmisor;
    @BindView(R.id.spinnerHospital)
    Spinner spinnerHospital;
    @BindView(R.id.otro_container)
    LinearLayout otroHospital;
    public String tipoIdentificacion;
    @BindView(R.id.tipoLicencia)
    LinearLayout tipoLicencia;
    @BindView(R.id.edtOtroHospital)
    EditText etOtroHospital;
    @BindView(R.id.etAge)
    EditText etAge;
    @BindView(R.id.agregar_peaton_container)
    LinearLayout layout;
    @BindView(R.id.title_estado_emisor)
    TextView titleEstadoEmisor;
    @BindView(R.id.dateVigencia)
    EditText dateVigencia;
    @BindView(R.id.tvTipoLicencia)
    TextView title_licencia;
    @BindView(R.id.spinnerIdentificacion)
    Spinner spinnerIdentificacion;
    @BindView(R.id.tvNumIdentificacionP)
    TextView numIdentificacion;
    @BindView(R.id.spinnerEstadoEmisor)
    Spinner spinnerEstadoEmisor;
    @BindView(R.id.spinnerTipoLicencia)
    Spinner spinnerTipoLicencia;
    @BindView(R.id.spinnerStatus)
    Spinner spinnerStatus;
    @BindView(R.id.title_lesion)
    TextView titulo_lesion;
    @BindView(R.id.lesionado_container)
    LinearLayout lesionado_container;
    @BindView(R.id.spinnerNivelLesion)
    Spinner spinnerNivelLesion;
    Spinner spinnerTipoTelefono;
    @BindView(R.id.title_vigencia)
    TextView title_vigencia;
    @BindView(R.id.recyclerEmail)
    RecyclerView recyclerEmail;
    @BindView(R.id.recyclerPhone)
    RecyclerView recyclerPhone;
    @BindView(R.id.spinnerEstadoEmisorAmbulancia)
    Spinner estadoAmbulancia;
    @BindView(R.id.cbPermanente)
    CheckBox checkBoxPermanente;
    @BindView(R.id.cbAmbulancia)
    CheckBox cbAmbulancia;
    @BindView(R.id.cbAuditivo)
    CheckBox cbAuditivo;
    @BindView(R.id.cbDentadura)
    CheckBox cbDentadura;
    @BindView(R.id.cbLentes)
    CheckBox cbLentes;
    @BindView(R.id.cbProtesis)
    CheckBox cbProtesis;
    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.rvPartesLesionada)
    RecyclerView rvPartesLesionadas;
    private GeneralCameraFragment generalCameraFragment;
    private List<Supervisoria> supervisorias = new ArrayList<>();
    private List<CatalogoClave> identificaciones = new ArrayList<>();
    private EventBus bus = EventBus.getDefault();
    private Menu menu;
    private EditText phone, extension;
    public static final String TAG = "TAG";
    private boolean isUploaded = false;

    private Calendar calendar = Calendar.getInstance();
    private CorreoAdapter adapterEmail;
    private PhoneAdapter adapterPhone;
    private List<Integer> partesLesionada = new ArrayList<>();
    private static final int PERMISSION_REQUEST = 100;
    private Boolean avisoPrivacidad;

    private Dialog dialog;
    private String idAjuste;
    private List<HospitalResumen> hospitales;

    private List<CatalogoClaveInfo> estadosList = new ArrayList<>();
    private Long idPeaton;
    private TerceroPersona peatonR;
    private CallbackItemImage listener;

    private List<CatalogoClaveAmis> codePhone = new ArrayList<>();
    private ArrayList<ImageGeneralModel> filePhotos = new ArrayList<>();
    private GeneralInjuredFragment generalInjuredFragment;
    private String path = "";

    private TerceroPersonaInteractor terceroPeatonService;

    private TprInteractor tprService;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private String estado;
    private String tipoLicenciaPeaton;
    private Hospital hospital;
    private EditText email;

    private List<CatalogoClave> tiposLicenciaList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_peaton);
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        idPeaton = getIntent().getLongExtra("idPeaton", 0);
        setmToolbar(mToolbar);
        generalCameraFragment = new GeneralCameraFragment();
        addFragment(generalCameraFragment, Constants.TAG_THIRD_PEDESTRIAN);
        generalInjuredFragment = new GeneralInjuredFragment();

        terceroPeatonService = OrquestadorInteractorFactory.getTerceroPersonaService();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        tprService = CatalogoInteractorsFactory.getTprService();

        listener = this;
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
        addFragmentInjuredParts(generalInjuredFragment);
        setEvents();
        initComponents();
        initLsiteners();
        btnUploadImages.setVisibility(GONE);
        if (idPeaton != 0) {
            getCompositeDisposable().add(getCatalogos()
                    .subscribe(() -> getPeaton(idPeaton), throwable -> new GeneralSnackBar().largeSnackError(this, throwable)));
        } else {
            getCompositeDisposable().add(getCatalogos()
                    .subscribe(() -> {
                    }, throwable -> new GeneralSnackBar().largeSnackError(this, throwable)));
        }
    }



    private Completable getCatalogos() {
        return Completable.mergeArrayDelayError(
                downloadEstados(),
                downloadIdentificaciones(),
                downloadNivelLesion(),
                downloadTiposTelefonos(),
                downloadSupervisorias()
        );

    }


    private void getPeaton(Long idPeaton) {
        getCompositeDisposable().add(terceroPeatonService.get(idAjuste, idPeaton)
                .subscribe(peaton -> {
                    Log.d(TAG, "onResponse: " + peaton);
                    cargaPeaton(peaton);
                }, throwable -> {
                    Log.e(TAG, "Error al obtener los datos de tercero persona " + idPeaton + " del ajuste" + idAjuste, throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        new GeneralSnackBar().largeSnackError(this, throwable);
                    }

                }));

    }
    private void downloadTiposLicencia(String estado){
            getCompositeDisposable().add(siniestrosInteractor.getTiposLicencia(estado)
                    .subscribe(tiposLicencia -> {
                        tiposLicenciaList = tiposLicencia;
                        AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, tiposLicenciaList, CatalogoClave::getNombre);
                        spinnerTipoLicencia.setAdapter(adapter);
                        if ( peatonR != null && peatonR.getPersona().getTipoLicencia() != null) {
                            spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, peatonR.getPersona().getTipoLicencia()));
                        }
                    }, throwable -> {
                        spinnerTipoLicencia.setAdapter(null);
                        Log.e(TAG, "Error al descargar los colores", throwable);
                    }));

    }
    private boolean validFormConductor() {
        boolean isRequestFocus = false;
        if (firstName.getText().toString().trim().length() <= 0) {
            firstName.setError(getString(R.string.mandatory_field));
            firstName.requestFocus();
            isRequestFocus = true;
        }
        if (lastName.getText().toString().trim().length() <= 0) {
            lastName.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                lastName.requestFocus();
                isRequestFocus = true;
            }
        }
        if (dateNacimiento.getText().toString().trim().length() <= 0) {
            dateNacimiento.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                dateNacimiento.requestFocus();
                isRequestFocus = true;
            }
        }
        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getNombre().equals("Licencia de Conducir")) {
            if (!checkBoxPermanente.isChecked()) {
                if (dateVigencia.getText().toString().trim().length() <= 0) {
                    dateVigencia.setError(getString(R.string.mandatory_field));
                    dateVigencia.requestFocus();
                    if (!isRequestFocus) {
                        isRequestFocus = true;
                    }
                }
            }
        }
        if (etNumeroIdentificacion.getText().toString().trim().length() <= 0) {
            etNumeroIdentificacion.setError(getString(R.string.mandatory_field));
            if (!isRequestFocus) {
                etNumeroIdentificacion.requestFocus();
                isRequestFocus = true;
            }
        }
        if (adapterPhone.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(this, ResourceUtils.getString("telefono_empty"), Toast.LENGTH_LONG).show();
        }
        if (adapterEmail.getItemCount() == 0) {
            isRequestFocus = true;
            Toast.makeText(this, ResourceUtils.getString("correo_empty"), Toast.LENGTH_LONG).show();
        }

        if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
            if (generalInjuredFragment.getDataList().isEmpty()) {
                isRequestFocus = true;
                Toast.makeText(this, ResourceUtils.getString("lesion_empty"), Toast.LENGTH_LONG).show();
            }

            if (cbOtroHospital.isChecked() && etOtroHospital.getText().toString().isEmpty()) {
                isRequestFocus = true;
                Toast.makeText(this, ResourceUtils.getString("hospital_empty"), Toast.LENGTH_LONG).show();
            }
        }
        return !isRequestFocus;
    }

    private Completable downloadEstados() {
        return Completable.create(e -> {
            getCompositeDisposable().add(catInteractor.getEstados()
                    .subscribe(estados -> {
                        estadosList = estados;
                        AdapterSpinnerGeneral<CatalogoClaveInfo> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, estadosList, CatalogoClaveInfo::getNombre);
                        spinnerEstadoEmisor.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerEstadoEmisor.setAdapter(null);
                        Log.e(TAG, "Error al descargar estados", throwable);
                    })
            );

        });

    }


    public void cargaPeaton(TerceroPersona peaton) {
        if (peaton != null) {
            peatonR = peaton;
            isUploaded = true;
            generalCameraFragment.setFolder(MODULO_TERCEROS, ZERO, TIPO_PEATON, "", String.valueOf(idPeaton));
            ModulePeaton modulePeaton = new ModulePeaton(String.valueOf(idPeaton), TERCEROS, "", String.valueOf(idPeaton), idAjuste);
            path = PersistenciaPath.getImagePathPeaton(modulePeaton);
            if (path != null) {
                filePhotos.addAll(PersistenciaPath.getAllImages(getBaseActivity(), path));
                generalCameraFragment.updateImageList(filePhotos);
                PersistenciaPath.showMessageSizeImages(getBaseActivity(), filePhotos);
            }


            setAutoriza(peaton.getPersona().getAutorizaTratamiento());
            firstName.setText(peaton.getPersona().getNombre());
            lastName.setText(peaton.getPersona().getApellidoPaterno());
            motherLastName.setText(peaton.getPersona().getApellidoMaterno());
            String sexo = peaton.getPersona().getSexo();
            if (sexo.equals("M")) {
                RadioButton rbM = findViewById(R.id.rbMasculino);
                rbM.setChecked(true);
            } else {
                RadioButton rbM = findViewById(R.id.rbFemenino);
                rbM.setChecked(true);
            }
            dateNacimiento.setText(DateUtils.date(peaton.getPersona().getFechaNacimiento()));
            if(peaton.getPersona().getFechaNacimiento()!= null) {
                String[] parts = DateUtils.date(peaton.getPersona().getFechaNacimiento()).split("/");
                etAge.setText(calculateAge(Integer.parseInt(parts[2]), Integer.parseInt(parts[1]), Integer.parseInt(parts[0])));
            }
            etNumeroIdentificacion.setText(peaton.getPersona().getNumeroIdentificacion());

            if (peaton.getPersona().getTipoIdentificacion() != null) {
                spinnerIdentificacion.setSelection(SpinnerUtils.getPosition(spinnerIdentificacion, peaton.getPersona().getTipoIdentificacion()));
                if (peaton.getPersona().getTipoIdentificacion().equals("LC")) {
                    if (peaton.getPersona().getEstadoLicencia() != null) {
                        spinnerEstadoEmisor.setSelection(SpinnerUtils.getPosition(spinnerEstadoEmisor, peaton.getPersona().getEstadoLicencia()));
                        downloadTiposLicencia(peaton.getPersona().getEstadoLicencia());
                    }
                    if (peaton.getPersona().getTipoLicencia() != null) {
                        spinnerTipoLicencia.setSelection(SpinnerUtils.getPosition(spinnerTipoLicencia, peaton.getPersona().getTipoLicencia()));
                    }
                    if (peaton.getPersona().getLicenciaPermanente() == null) {
                        checkBoxPermanente.setChecked(false);
                    } else {
                        if (!peaton.getPersona().getLicenciaPermanente()) {
                            checkBoxPermanente.setChecked(false);
                            dateVigencia.setText(DateUtils.date(peaton.getPersona().getFechaFinLicencia()));
                        } else {
                            checkBoxPermanente.setChecked(peaton.getPersona().getLicenciaPermanente());
                        }
                    }
                }
            }

            if (peaton.getPersona().getCondicion() != null) {
                spinnerStatus.setSelection(SpinnerUtils.getPosition(spinnerStatus, peaton.getPersona().getCondicion().name()));
                if (peaton.getPersona().getCondicion().equals(CondicionPersona.LESIONADO)) {
                    if (peaton.getPersona().getNivelLesion() != null) {
                        spinnerNivelLesion.setSelection(SpinnerUtils.getPosition(spinnerNivelLesion, peaton.getPersona().getNivelLesion()));

                    }
                    if (!peaton.getPersona().getLesiones().isEmpty()) {
                        generalInjuredFragment.loadDataList(peaton.getPersona().getLesiones());
                        cbAmbulancia.setChecked(peaton.getPersona().getRequiereAmbulancia());
                        if (peaton.getPersona().getOtroHospital() == null) {
                            if (peaton.getPersona().getHospital() != null) {
                                getSupervisoria(peaton.getPersona().getHospital());
                            }
                            cbOtroHospital.setChecked(false);
                        } else {
                            cbOtroHospital.setChecked(true);
                            etOtroHospital.setText(peaton.getPersona().getOtroHospital());
                            Utils.hideKeyboard(etOtroHospital, this);
                        }

                    }

                }
            }

            if (!peaton.getPersona().getFotografias().isEmpty()) {
                getPhotosPeaton();
                generalCameraFragment.setCallBackItem(listener);
                btnUploadImages.setVisibility(View.VISIBLE);
            } else {
                btnUploadImages.setVisibility(View.VISIBLE);
            }
            adapterPhone.addAll(peaton.getPersona().getTelefonos());
            recyclerPhone.setAdapter(adapterPhone);
            adapterPhone.notifyDataSetChanged();
            adapterEmail.addAllString(peaton.getPersona().getCorreos());
            recyclerEmail.setAdapter(adapterEmail);
            adapterEmail.notifyDataSetChanged();
            List<Long> objetos = peaton.getPersona().getObjetosPerdidos();
            if (objetos != null) {
                for (long objeto : objetos) {
                    switch ((int) objeto) {
                        case 1:
                            cbLentes.setChecked(true);
                            break;
                        case 2:
                            cbAuditivo.setChecked(true);
                            break;
                        case 3:
                            cbDentadura.setChecked(true);
                            break;
                        case 4:
                            cbProtesis.setChecked(true);
                            break;
                    }
                }
            }
        } else {
            onBackPressed();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        generalCameraFragment.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btnPeatonAmbulancia)
    public void callAmbulancia() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(layout, "Necesitas brindar el permiso para realizar llamadas",
                            Snackbar.LENGTH_LONG).setAction("Aceptar", new View.OnClickListener() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                llamarAmbulancia();
            }
        } else {
            llamarAmbulancia();
        }

    }

    @SuppressLint("MissingPermission")
    private void llamarAmbulancia() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Utils.NUMBER_AMBULANCE));
        startActivity(intent);
    }

    private Completable downloadSupervisorias() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getSupervisorias()
                    .subscribe(supervisioriaList -> {
                        supervisorias = supervisioriaList;
                        AdapterSpinnerGeneral<Supervisoria> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, supervisorias, Supervisoria::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        estadoAmbulancia.setAdapter(adapter);
                        estadoAmbulancia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                downloadHospitales(supervisorias.get(estadoAmbulancia.getSelectedItemPosition()).getClave());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        estadoAmbulancia.setAdapter(null);
                        Log.e(TAG, "Error al descargar supervisorias", throwable);
                    })
            );
        });
    }


    private void setEvents() {
        firstName.addTextChangedListener(this);
        lastName.addTextChangedListener(this);
        motherLastName.addTextChangedListener(this);
        dateNacimiento.setOnClickListener(v -> showDatePikerDialog());
        dateVigencia.setOnClickListener(v -> showDatePikerDialogVigencia());
    }


    private void initComponents() {
        adapterEmail = new CorreoAdapter(position -> adapterEmail.remove(adapterEmail.getCorreo(position)));
        adapterPhone = new PhoneAdapter(position -> adapterPhone.remove(adapterPhone.getTelono(position)));
        recyclerPhone.setHasFixedSize(true);
        recyclerPhone.setLayoutManager(new LinearLayoutManager(this));
        recyclerPhone.setItemAnimator(new DefaultItemAnimator());
        recyclerPhone.setAdapter(adapterPhone);
        recyclerEmail.setHasFixedSize(true);
        recyclerEmail.setLayoutManager(new LinearLayoutManager(this));
        recyclerEmail.setItemAnimator(new DefaultItemAnimator());
        recyclerEmail.setAdapter(adapterEmail);
        List<String> estatus = Arrays.asList(getResources().getStringArray(R.array.spinner_status));
        AdapterSpinnerGeneral<String> spinnerArrayAdapter = new AdapterSpinnerGeneral<>(TerceroPersonaActivity.this
                , R.layout.spinner_item, estatus, Object::toString);

        spinnerStatus.setAdapter(spinnerArrayAdapter);

        cbOtroHospital.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                otroHospital.setVisibility(View.VISIBLE);
                Utils.showKeyboard(etOtroHospital, this);
                findViewById(R.id.hospital_spinner_content).setVisibility(View.GONE);
                findViewById(R.id.linear_hospital).setVisibility(View.GONE);
            } else {
                otroHospital.setVisibility(View.INVISIBLE);
                Utils.hideKeyboard(etOtroHospital, this);
                findViewById(R.id.hospital_spinner_content).setVisibility(View.VISIBLE);
                findViewById(R.id.linear_hospital).setVisibility(View.VISIBLE);
            }
        });
        checkBoxPermanente.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                findViewById(R.id.dateVigencia).setVisibility(View.INVISIBLE);
                findViewById(R.id.title_vigencia).setVisibility(View.INVISIBLE);
            } else {
                findViewById(R.id.dateVigencia).setVisibility(View.VISIBLE);
                findViewById(R.id.title_vigencia).setVisibility(View.VISIBLE);
            }
        });

        spinnerTipoLicencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipoLicenciaPeaton = tiposLicenciaList.get(position).getClave();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerEstadoEmisor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                estado = estadosList.get(i).getClave();
                downloadTiposLicencia(estado);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    private void showDatePikerDialog() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateNacimiento.setText(date);
            if (age.contains("-")) {
                etAge.setText("");
            } else {
                etAge.setText(age);
            }

        });
        datePickerFragment.show(getSupportFragmentManager(), "dataPickerBirthday");
    }

    private String calculateAge(int year, int month, int day) {
        LocalDate birthdate = new LocalDate(year, month, day);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);
        return String.valueOf(age.getYears());
    }

    private void showDatePikerDialogVigencia() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            dateVigencia.setText(date);

        });
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
    }


    private void showDialogAddEmail() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar correo electrónico")
                .customView(R.layout.dialog_add_email, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {

                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();

        email = (EditText) builder.findViewById(R.id.edtCorreoConductor);
        builder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_email));
        builder.show();
    }

    private void showDialogAddPhone() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Agregar teléfono")
                .customView(R.layout.dialog_add_phone, true)
                .positiveText("Aceptar")
                .cancelable(false)
                .onPositive((dialog, which) -> {
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        spinnerTipoTelefono = (Spinner) builder.findViewById(R.id.spinnerTipoTelefono);
        TextView titleEx = (TextView) builder.findViewById(R.id.titleExtencion);
        AdapterSpinnerGeneral<CatalogoClaveAmis> adapter = new AdapterSpinnerGeneral<>(getApplicationContext(), R.layout.sp_little_item, codePhone, CatalogoClave::getNombre);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoTelefono.setAdapter(adapter);
        phone = (EditText) builder.findViewById(R.id.etPhone);
        extension = (EditText) builder.findViewById(R.id.etExtension);
        spinnerTipoTelefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (((CatalogoClave) spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                    titleEx.setVisibility(View.VISIBLE);
                    extension.setVisibility(View.VISIBLE);
                } else {
                    titleEx.setVisibility(View.GONE);
                    extension.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        MDButton aceptar = builder.getActionButton(DialogAction.POSITIVE);
        MDButton cancelar = builder.getActionButton(DialogAction.NEGATIVE);
        cancelar.setAllCaps(false);
        aceptar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        cancelar.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        aceptar.setBackground(ContextCompat.getDrawable(builder.getContext(), R.drawable.background_arround_actived));
        aceptar.setTextColor(Color.WHITE);
        aceptar.setAllCaps(false);
        aceptar.setOnClickListener(new CustomListener(builder, R.layout.dialog_add_phone));
        builder.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String nombre = firstName.getText().toString().trim();
        if (!FormatStrings.validateText(nombre)) {
            firstName.setError(getString(R.string.message_character_special));
        }
        String apellidoP = lastName.getText().toString().trim();

        if (!FormatStrings.validateText(apellidoP)) {
            lastName.setError(getString(R.string.message_character_special));
        }
        String apellidoM = motherLastName.getText().toString().trim();

        if (!FormatStrings.validateText(apellidoM)) {
            motherLastName.setError(getString(R.string.message_character_special));
        }
    }


    class CustomListener implements View.OnClickListener {
        private final Dialog dialog;
        private final int layout;

        public CustomListener(Dialog dialog, int layout) {
            this.dialog = dialog;
            this.layout = layout;
        }

        @Override
        public void onClick(View v) {
            if (layout == R.layout.dialog_add_phone) {
                if (phone.getText().toString().trim().length() < 10) {
                    phone.setError(ResourceUtils.getString("invalid_number"));
                    if (((CatalogoClave) spinnerTipoTelefono.getSelectedItem()).getNombre().equals("Trabajo")) {
                        if (extension.getText().toString().trim().length() < 6) {
                            extension.setError(ResourceUtils.getString("extension_error"));
                        }
                    }
                } else {
                    String key = "";
                    if (codePhone.get(spinnerTipoTelefono.getSelectedItemPosition()) != null) {
                        key = codePhone.get(spinnerTipoTelefono.getSelectedItemPosition()).getClave();
                    }
                    adapterPhone.add(new Telefono(phone.getText().toString(), key, extension.getText().toString().equals("") ? null : extension.getText().toString()));
                    recyclerPhone.setAdapter(adapterPhone);
                    adapterPhone.notifyDataSetChanged();
                    dialog.dismiss();
                }
            } else {
                if (email.getText().toString().equals("")) {
                    email.setError("No deje vacio este campo");
                    return;
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError("Coloque una dirección de correo correcta");
                    return;
                } else {
                    adapterEmail.add(email.getText().toString());
                    recyclerEmail.setAdapter(adapterEmail);
                    adapterEmail.notifyDataSetChanged();
                    dialog.dismiss();
                }
            }
        }
    }


    @OnClick(R.id.btnAddEmail)
    public void addEmail() {
        showDialogAddEmail();
    }

    @OnClick(R.id.btnAddPhone)
    public void addPhone() {
        showDialogAddPhone();
    }


    private Persona createPeaton() {
        Persona peaton = new Persona();
        peaton.setNombre(firstName.getText().toString());
        peaton.setApellidoPaterno(lastName.getText().toString());
        peaton.setApellidoMaterno(motherLastName.getText().toString());
        if ("".equals(peaton.getApellidoMaterno())) {
            peaton.setApellidoMaterno(null);
        }
        peaton.setSexo(getGenero());
        peaton.setFechaNacimiento(DateUtils.date(dateNacimiento.getText().toString()));
        peaton.setNumeroIdentificacion(etNumeroIdentificacion.getText().toString());
        peaton.setTipoIdentificacion(getTipoIdentificacion());
        peaton.setAutorizaTratamiento(avisoPrivacidad);

        if (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getClave().equals("LC")) {
            peaton.setLicenciaPermanente(checkBoxPermanente.isChecked());
            peaton.setTipoLicencia(tiposLicenciaList.get(spinnerTipoLicencia.getSelectedItemPosition()).getClave());
            peaton.setEstadoLicencia(estado);
            if (checkBoxPermanente.isChecked()) {
                peaton.setFechaFinLicencia(null);
            } else {
                peaton.setFechaFinLicencia(DateUtils.date(dateVigencia.getText().toString()));
            }
        }

        peaton.setCondicion(CondicionPersona.valueOf(spinnerStatus.getSelectedItem().toString()));
        if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
            peaton.setLesiones(generalInjuredFragment.getDataList());
            CatalogoId nivelLesion = (CatalogoId) spinnerNivelLesion.getSelectedItem();
            peaton.setNivelLesion(nivelLesion.getId());
            peaton.setRequiereAmbulancia(cbAmbulancia.isChecked());
            if (cbOtroHospital.isChecked()) {
                peaton.setOtroHospital(etOtroHospital.getText().toString());
                peaton.setHospital(null);
            } else {
                if (hospitales != null && hospitales.size() != 0) {
                    peaton.setHospital(hospitales.get(spinnerHospital.getSelectedItemPosition()).getId());
                } else {
                    peaton.setHospital(null);
                }
                peaton.setOtroHospital(null);
            }
        } else {
            peaton.setRequiereAmbulancia(false);
        }
        peaton.setTelefonos(adapterPhone.getTelefonos());
        peaton.setCorreos(adapterEmail.getCorreoList());
        peaton.setObjetosPerdidos(getObjetosPerdidos());
        Log.d("TAG", "createPeaton: " + new Gson().toJson(peaton));
        return peaton;
    }

    private void setAutoriza(Boolean autoriza) {
        avisoPrivacidad = autoriza;
    }

    @OnClick(R.id.btnAvisoPrivacidad)
    public void dialogAvisoPrivacidad() {
        MaterialDialog builder = new MaterialDialog.Builder(this)
                .title("Aviso de privacidad")
                .customView(R.layout.dialog_notice_of_privacy, true)
                .positiveText("Aceptar")
                .onPositive((dialog, which) -> {
                    if (avisoPrivacidad != null) {
                        setAutoriza(avisoPrivacidad);
                    }
                    dialog.dismiss();
                })
                .negativeText("Cancelar")
                .onNegative((dialog, which) -> dialog.dismiss())
                .build();
        CheckBox si = (CheckBox) builder.findViewById(R.id.cbPrivacidadSi);
        CheckBox no = (CheckBox) builder.findViewById(R.id.cbPrivacidadNo);
        if (avisoPrivacidad != null) {
            if (avisoPrivacidad) {
                si.setChecked(true);
                no.setChecked(false);
            } else {
                si.setChecked(false);
                no.setChecked(true);
            }
        }
        si.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                avisoPrivacidad = true;
                si.setChecked(true);
                no.setChecked(false);
            } else {
                avisoPrivacidad = false;
                no.setChecked(true);
                si.setChecked(false);
            }
        });
        no.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                avisoPrivacidad = false;
                si.setChecked(false);
                no.setChecked(true);
            } else {
                avisoPrivacidad = true;
                no.setChecked(false);
                si.setChecked(true);
            }
        });
        builder.show();
    }


    private List<Long> getObjetosPerdidos() {
        Long auditivo, dentadura, lentes, protesis;
        if (cbAuditivo.isChecked()) {
            auditivo = 2L;
        } else {
            auditivo = null;
        }
        if (cbDentadura.isChecked()) {
            dentadura = 3L;
        } else {
            dentadura = null;
        }
        if (cbLentes.isChecked()) {
            lentes = 1L;
        } else {
            lentes = null;
        }
        if (cbProtesis.isChecked()) {
            protesis = 4L;
        } else {
            protesis = null;
        }

        List<Long> integerList = new LinkedList<>();
        if (auditivo != null) integerList.add(auditivo);
        if (dentadura != null) integerList.add(dentadura);
        if (lentes != null) integerList.add(lentes);
        if (protesis != null) integerList.add(protesis);
        return integerList;
    }


    private String getGenero() {
        RadioButton rbM = findViewById(R.id.rbMasculino);
        if (rbM.isChecked()) {
            return "M";
        } else {
            return "F";
        }
    }

    private String getTipoIdentificacion() {
        CatalogoClave identificacion = (CatalogoClave) spinnerIdentificacion.getSelectedItem();
        if (identificacion != null)
            return identificacion.getClave();
        else
            return null;
    }


    private Completable downloadIdentificaciones() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getTiposIdentificacion()
                    .subscribe(identificacionesL -> {
                        identificaciones = identificacionesL;
                        if (identificaciones != null) {
                            AdapterSpinnerGeneral<CatalogoClave> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                    , R.layout.spinner_item, identificaciones, CatalogoClave::getNombre);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerIdentificacion.setAdapter(adapter);

                        }
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerIdentificacion.setAdapter(null);
                        Log.e(TAG, "Error al descargar  identificaciones", throwable);
                    }));
        });

    }


    private Completable downloadTiposTelefonos() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getTiposTelefono()
                    .subscribe(tTelefonos -> {
                        codePhone = tTelefonos;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al descargar tipos de telefonos", throwable);
                    })
            );
        });


    }


    private Completable downloadNivelLesion() {
        return Completable.create(e -> {
            getCompositeDisposable().add(siniestrosInteractor.getNivelLesion()
                    .subscribe(nivelesLesion -> {
                        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getApplicationContext()
                                , R.layout.sp_little_item, nivelesLesion, CatalogoId::getNombre);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerNivelLesion.setAdapter(adapter);
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        spinnerNivelLesion.setAdapter(null);
                        Log.e(TAG, "Error al descargar niveles de lesion", throwable);
                    }));
        });
    }


    private void getSupervisoria(Long id) {

        getCompositeDisposable().add(siniestrosInteractor.getHospital(id)
                .subscribe(supervisoria -> {
                    hospital = supervisoria;
                    for (Supervisoria s : supervisorias) {
                        if (s.getClave().equals(hospital.getSupervisoria())) {
                            estadoAmbulancia.setSelection(supervisorias.indexOf(s));
                            downloadHospitales(hospital.getSupervisoria());
                            break;
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar supervisoria", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));

    }


    private void downloadHospitales(String supervisora) {

        getCompositeDisposable().add(siniestrosInteractor.getHospitales(supervisora)
                .subscribe(hospitales -> {
                    this.hospitales = hospitales;
                    AdapterSpinnerGeneral<HospitalResumen> adapter = new AdapterSpinnerGeneral<>(this
                            , R.layout.sp_little_item, this.hospitales, HospitalResumen::getNombre);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerHospital.setAdapter(adapter);
                    if (hospital != null) {
                        for (HospitalResumen h : this.hospitales) {
                            if (h.getNombre().equals(this.hospital.getNombre())) {
                                spinnerHospital.setSelection(hospitales.indexOf(h));
                                break;
                            }
                        }
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al descargar hospitales", throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));
    }

    @OnClick(R.id.btnAddImages)
    public void uploadImagesPeaton() {
        ImageUtils imageUtils = new ImageUtils();
        List<ImageGeneralModel> imageList = generalCameraFragment.getFileList();
        List<ImageGeneralModel> images = new ArrayList<>();
        for (ImageGeneralModel image : imageList) {
            if (image.getNewFile()) {
                images.add(image);
            }
        }
        if (!images.isEmpty()) {
            int size = images.size();
            btnUploadImages.setEnabled(false);
            ValueObject<Integer> count = new ValueObject<>(1);
            getCompositeDisposable().add(Observable.fromIterable(images)
                    .map(image -> new File(image.getFilePhat()))
                    .flatMap(file -> terceroPeatonService.addFotografia(idAjuste, idPeaton, file).toObservable(), false, 1)
                    .doOnEach(fotografias -> {
                        if (count.getValue() < size) {
                            count.setValue(count.getValue() + 1);
                            LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                        }
                    })
                    .onErrorReturnItem(new Fotografia())
                    .toList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(it -> {
                        LoadingIndicator.start();
                        LoadingIndicator.message(ResourceUtils.getString("procesando_imagenes", count.getValue(), size));
                    })
                    .doFinally(LoadingIndicator::finish)
                    .subscribe(fotografias -> {
                        boolean ok = true;
                        for (Fotografia pic : fotografias) {
                            ok &= pic.getUrl() != null;
                        }
                        if (ok) {
                            new GeneralSnackBar().largeSnackOk(this, getResources().getString(R.string.imagenes_upload_ok));
                        } else {
                            new GeneralSnackBar().largeSnackError(this, getResources().getString(R.string.imagenes_upload_error));
                        }
                        btnUploadImages.setEnabled(true);
                        new GeneralSnackBar().largeSnackOk(this, getResources().getString(R.string.imagenes_upload_ok));
                        imageUtils.renameImageUpload(imageList);
                        imageUtils.deleteImage(getBaseActivity(), imageList);
                        filePhotos.clear();
                        getPhotosPeaton();
                    }, throwable -> {
                        btnUploadImages.setEnabled(true);
                        Log.e(TAG, "Error al enviar imagenes de tercero persona " + idPeaton + " del ajuste: " + idAjuste, throwable);
                        new GeneralSnackBar().largeSnackError(this, throwable);

                    }));

        } else {
            new GeneralSnackBar().largeSnackError(TerceroPersonaActivity.this, getString(R.string.image_void));
            btnUploadImages.setEnabled(true);
        }
    }

    private void getPhotosPeaton() {
        getCompositeDisposable().add(terceroPeatonService.getFotografias(idAjuste, idPeaton)
                .subscribe(fotoResponses -> {
                    btnUploadImages.setEnabled(true);
                    for (Fotografia photo : fotoResponses) {
                        filePhotos.add(new ImageGeneralModel(String.valueOf(photo.getId()), photo, null, false));
                    }
                    generalCameraFragment.updateImageList(filePhotos);

                }, throwable -> {
                    btnUploadImages.setEnabled(true);
                    Log.e(TAG, "Error al obtener imagenes de tercero persona: " + idPeaton + " del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));

    }

    private void initLsiteners() {


        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinnerStatus.getSelectedItem().toString().equals("LESIONADO")) {
                    titulo_lesion.setVisibility(View.VISIBLE);
                    lesionado_container.setVisibility(View.VISIBLE);
                    spinnerNivelLesion.setVisibility(View.VISIBLE);
                    findViewById(R.id.hospital_container).setVisibility(View.VISIBLE);
                    findViewById(R.id.hospital_content).setVisibility(View.VISIBLE);
                    findViewById(R.id.btnPeatonAmbulancia).setVisibility(View.VISIBLE);
                } else {
                    titulo_lesion.setVisibility(GONE);
                    lesionado_container.setVisibility(GONE);
                    spinnerNivelLesion.setVisibility(GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerIdentificacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (identificaciones.get(spinnerIdentificacion.getSelectedItemPosition()).getNombre()) {
                    case "Cartilla":
                        numIdentificacion.setText("Número de identificación");
                        checkBoxPermanente.setVisibility(View.GONE);
                        tipoLicencia.setVisibility(View.GONE);
                        title_licencia.setVisibility(View.GONE);
                        title_vigencia.setVisibility(View.GONE);
                        dateVigencia.setVisibility(View.GONE);
                        layoutEstadoEmisor.setVisibility(View.GONE);
                        titleEstadoEmisor.setVisibility(View.GONE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(GONE);
                        break;
                    case "Credencial para Votar":
                        numIdentificacion.setText("Clave de elector");
                        checkBoxPermanente.setVisibility(View.GONE);
                        tipoLicencia.setVisibility(View.GONE);
                        title_licencia.setVisibility(View.GONE);
                        dateVigencia.setVisibility(View.GONE);
                        title_vigencia.setVisibility(View.GONE);
                        layoutEstadoEmisor.setVisibility(View.GONE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(GONE);
                        titleEstadoEmisor.setVisibility(View.GONE);
                        etNumeroIdentificacion.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (spinnerIdentificacion.getSelectedItem().toString().equals("Credencial para Votar") && !s.toString().matches("^[A-Z]{6}[0-9]{8}[0-9A-Z]{4}$")) {
                                    etNumeroIdentificacion.setError("ingrese una credencial válida");
                                } else {
                                    etNumeroIdentificacion.setError(null);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });
                        break;
                    case "Identificación No Oficial":
                        numIdentificacion.setText("Número de identificación");
                        checkBoxPermanente.setVisibility(View.GONE);
                        tipoLicencia.setVisibility(View.GONE);
                        title_licencia.setVisibility(View.GONE);
                        dateVigencia.setVisibility(View.GONE);
                        title_vigencia.setVisibility(View.GONE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(GONE);
                        layoutEstadoEmisor.setVisibility(View.GONE);
                        titleEstadoEmisor.setVisibility(View.GONE);
                        break;
                    case "Licencia de Conducir":
                        numIdentificacion.setText("Número de identificación");
                        checkBoxPermanente.setVisibility(View.VISIBLE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(View.VISIBLE);
                        tipoLicencia.setVisibility(View.VISIBLE);
                        title_licencia.setVisibility(View.VISIBLE);
                        layoutEstadoEmisor.setVisibility(View.VISIBLE);
                        titleEstadoEmisor.setVisibility(View.VISIBLE);
                        if (!checkBoxPermanente.isChecked()) {
                            dateVigencia.setVisibility(View.VISIBLE);
                            title_vigencia.setVisibility(View.VISIBLE);
                        }
                        break;
                    case "Pasaporte":
                        numIdentificacion.setText("Número de identificación");
                        checkBoxPermanente.setVisibility(View.GONE);
                        tipoLicencia.setVisibility(View.GONE);
                        title_licencia.setVisibility(View.GONE);
                        findViewById(R.id.content_title_fecha_vigencia).setVisibility(GONE);
                        dateVigencia.setVisibility(View.GONE);
                        title_vigencia.setVisibility(View.GONE);
                        layoutEstadoEmisor.setVisibility(View.GONE);
                        titleEstadoEmisor.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

    @OnClick(R.id.btnGuardarPeaton)
    public void createPeatonRequest(Button button) {
        if (validFormConductor()) {
            button.setEnabled(false);
            if (idPeaton == 0) {
                if (avisoPrivacidad == null) {
                    Toast.makeText(getApplicationContext(), ResourceUtils.getString("aviso_privacidad_error"), Toast.LENGTH_LONG).show();
                    button.setEnabled(true);
                    return;
                }
                if (etOtroHospital.getText().toString().equals("") && cbOtroHospital.isChecked()) {
                    etOtroHospital.requestFocus();
                    etOtroHospital.setError(getResources().getString(R.string.volante_coberturas_required));
                    button.setEnabled(true);

                    return;
                }

                getCompositeDisposable().add(terceroPeatonService.add(idAjuste, createPeaton())
                        .subscribe(result -> {
                            button.setEnabled(true);
                            peatonR = result;
                            idPeaton = peatonR.getId();
                            generalCameraFragment.setFolder(MODULO_TERCEROS, 0, TIPO_PEATON, "", String.valueOf(idPeaton));
                            isUploaded = true;
                            btnUploadImages.setVisibility(View.VISIBLE);
                            ModulePeaton modulePeaton = new ModulePeaton(String.valueOf(idPeaton), TERCEROS, "", String.valueOf(idPeaton), idAjuste);
                            path = PersistenciaPath.getImagePathPeaton(modulePeaton);
                            if (!PersistenciaPath.getAllImages(getBaseActivity(), path).isEmpty())
                                uploadImagesPeaton();
                            avisoPrivacidad = result.getPersona().getAutorizaTratamiento();
                            new GeneralSnackBar().largeSnackOk(TerceroPersonaActivity.this, "Se agregó correctamente el peatón");


                        }, throwable -> {
                            button.setEnabled(true);
                            isUploaded = false;
                            Log.e(TAG, "Error al agregar tercero peaton del ajuste: " + idAjuste, throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);


                        }));
            } else {
                if (avisoPrivacidad == null) {
                    button.setEnabled(true);
                    ToastUtils.toastLong(ResourceUtils.getString("aviso_privacidad_error"));
                    return;
                }
                getCompositeDisposable().add(terceroPeatonService.modify(idAjuste, idPeaton, createPeaton())
                        .subscribe(() -> {
                            button.setEnabled(true);
                            avisoPrivacidad = createPeaton().getAutorizaTratamiento();
                            new GeneralSnackBar().largeSnackOk(TerceroPersonaActivity.this, "Se actualizó correctamente el peatón");

                        }, throwable -> {
                            button.setEnabled(true);
                            Log.e(TAG, "Error al modificar datos del tercero peaton " + idPeaton + " del ajuste: " + idAjuste, throwable);
                            new GeneralSnackBar().largeSnackError(this, throwable);

                        }));

            }
        } else {
            button.setEnabled(true);
        }

    }

    @Override
    public void onBackPressed() {
        if (!isUploaded) {
            DialogUtils.createAlert(this, "Salir",
                    getString(R.string.image_message_out),
                    "Aceptar", "Cancelar",
                    (dialog, which) -> {
                        if (peatonR != null) {
                            bus.post(peatonR);
                            TerceroPersonaActivity.super.onBackPressed();
                        } else {
                            TerceroPersonaActivity.super.onBackPressed();
                        }
                    },
                    (dialog, which) -> dialog.dismiss());
        } else {
            bus.post(peatonR);
            super.onBackPressed();
        }
    }




    public void eliminarFoto(long id) {
        getCompositeDisposable().add(terceroPeatonService.deleteFotografia(idAjuste, idPeaton, id)
                .subscribe(() -> {
                    new GeneralSnackBar().largeSnackOk(TerceroPersonaActivity.this, "Se elimino correctamente la fotografía del peatón");
                }, throwable -> {
                    Log.e(TAG, "Error al eliminar imagenes del tercero peaton " + idPeaton + " del ajuste: " + idAjuste, throwable);
                    new GeneralSnackBar().largeSnackError(this, throwable);
                }));
    }

    private void addFragment(Fragment fragment, String tag) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentCameraPedestrian, fragment, tag);
        transaction.commit();
    }

    private void addFragmentInjuredParts(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentInjuredPartsPeaton, fragment, "injuredParts");
        transaction.commit();
    }

    @Override
    public void onCallbackItemId(int position, Long id) {
        eliminarFoto(id);
    }

    @Override
    public void onCallItemId(int position, Long id) {

    }

}
