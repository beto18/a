package com.gnp.ajustadormovil.android.views.terceros.personas;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.ajuste.AjustesFragment;
import com.gnp.ajustadormovil.android.views.ajuste.SiniestrosFragment;
import com.gnp.ajustadormovil.android.views.terceros.TerceroAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemRemovedListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnItemSelectedListener;
import com.gnp.ajustadormovil.android.views.terceros.TercerosActivity;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.TIPO_PEATON;

/**
 * Created by Abraham on 07/12/2017.
 */

public class TercerosPersonaFragment extends BaseFragment implements OnItemRemovedListener, OnItemSelectedListener {

    private View mView;
    private static final String TAG = SiniestrosFragment.class.getSimpleName();
    private Unbinder mUnbinder;
    @BindView(R.id.rvTerceros)
    RecyclerView recycler;
    @BindView(R.id.container_autos)
    RelativeLayout layout;
    @BindView(R.id.title_less)
    TextView title_less;
    private List<TerceroPersona> peatones;
    private TerceroAdapter adapter;
    private String tokenId = "";
    private Cursor reading;
    //private DataBaseHelper helper;
    private Bundle bundle;
    private String idAjuste;
    private OnItemRemovedListener listener;
    private TerceroPersonaInteractor tercerosPersonaService;
    private CompositeDisposable compositeDisposable;

    public TercerosPersonaFragment() {

    }

    public static TercerosPersonaFragment newInstance() {
        TercerosPersonaFragment fragment = new TercerosPersonaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tercerosPersonaService = OrquestadorInteractorFactory.getTerceroPersonaService();
        TercerosActivity activity = (TercerosActivity) getActivity();
        assert activity != null;
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        compositeDisposable = ((BaseActivity) getActivity()).getCompositeDisposable();
        initComponents();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_terceros, container, false);
        mUnbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (recycler!=null){
            recycler.setAdapter(adapter);
        }
        getPeatones();

    }


    private void initComponents() {
        tokenId = AjustesFragment.PositionToken; //reading.getString(reading.getColumnIndex(DataBaseHelper.KEY_TODO));
        adapter = new TerceroAdapter(this, this);
    }

    private void getPeatones() {
        compositeDisposable.add(tercerosPersonaService.getAll(idAjuste)
                .subscribe(response -> {
                    if (response != null) {
                        adapter.clear();
                        adapter.addAll(response);
                    } else {
                        Log.d(TAG, "onResponse() called with: response = [" + response + "]");
                    }
                    if (response.size()!=0 && title_less!=null){
                        title_less.setVisibility(View.GONE);
                    }
                    else {
                        if (title_less!=null) {
                            title_less.setVisibility(View.VISIBLE);
                        }
                    }
                    Log.i(TAG, "Terceros: " + new Gson().toJson(response));

                }, throwable -> {
                    Log.e(TAG, "Errro al obtener los peatones", throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), throwable.getMessage());
                }));
    }


    @OnClick(R.id.btnTercero)
    public void addPeaton() {
        CameraActivity.idOcupantes = 0L;
        CameraActivity.id_auto = "";
        CameraActivity.tipoTerceros = TIPO_PEATON;
        Intent intent = new Intent(getActivity(), TerceroPersonaActivity.class);
        intent.putExtra("idAjuste", idAjuste);
        intent.putExtra("data", getArguments().getString("data"));
        startActivity(intent);
    }

    @Subscribe
    public void addData(TerceroPersona peaton) {
        adapter.clear();
        adapter.notifyDataSetChanged();
        getPeatones();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onItemClick(View view, int position) {
        compositeDisposable.add(tercerosPersonaService.delete(idAjuste, adapter.getTercero(position).getId())
                .subscribe(() -> {
                    adapter.remove(adapter.getTercero(position));
                    adapter.notifyDataSetChanged();
                    getPeatones();
                    new GeneralSnackBar().largeSnackOk(getActivity(), "El peaton fue eliminado correctamente...");
                }, throwable -> {
                    String message = ErrorUtils.getErrorMessage(throwable);
                    new GeneralSnackBar().largeSnackError(getActivity(), message);
                }));
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void changeStatus(View view, int position) {

    }

    @Override
    public void selectItem(View view, int position) {
        Intent intent = new Intent(getActivity(), TerceroPersonaActivity.class);
        long idPeaton = adapter.getTercero(position).getId();
        intent.putExtra("idAjuste", idAjuste);
        intent.putExtra("data", getArguments().getString("data"));
        intent.putExtra("idPeaton", idPeaton);
        intent.putExtra("data", getArguments().getString("data"));
        startActivity(intent);
    }
}
