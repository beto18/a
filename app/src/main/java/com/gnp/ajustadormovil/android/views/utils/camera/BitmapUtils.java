package com.gnp.ajustadormovil.android.views.utils.camera;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.gnp.ajustadormovil.android.AjustadorMovilApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Alejandro on 18/04/2016.
 */
public class BitmapUtils {

    private static final String TAG = BitmapUtils.class.getCanonicalName();

    private final static int reqWidth = 400;
    private final static int reqHeight = 400;
    private static final String formatBase64 = "data:image/jpeg;base64,";

    /**
     * Establece un bitmap compreso en tamaño y dimensiones a partir del bitmap original
     * @param bitmapBig Bitmap en imagen original
     * @return mapa de bits compuesto en dimensiones de 500x500
     */
    public static Bitmap setEncodeImageAttach(Bitmap bitmapBig) {
        String path = setEncodeImage(bitmapBig);
        final String base64Encoded = path.substring(path.indexOf(",") + 1);
        byte[] decodedString = Base64.decode(base64Encoded, Base64.URL_SAFE);
        Bitmap bitmap = decodeSampledBitmapFromResource(decodedString);
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, reqWidth, reqHeight);
        return bitmap;
    }

    /**
     * Establece la codificación de un imagen mapa de bits a una cadena base64
     * @param bitmap mapa que contiene la imagen original
     * @return imagen codificada en base64
     */
    public static String setEncodeImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        return formatBase64 + Base64.encodeToString(byteArrayImage, Base64.URL_SAFE);
    }

    /**
     * Establece la codificación de un recurso de la carpeta *drawable* a una cadena base64
     * @param drawable recurso drawable a codificar
     * @return recurso imagen codificada en base64
     */
    public static String setEncodeImage(Integer drawable) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(AjustadorMovilApplication.getInstance().getResources(), drawable, options);

        options.inSampleSize = calculateInSampleSize(options);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeResource(AjustadorMovilApplication.getInstance().getResources(), drawable, options);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        return formatBase64 + Base64.encodeToString(byteArrayImage, Base64.URL_SAFE);
    }

    /**
     * Establece la codificación de una imagen en la url indicada a una cadena base64
     * @param baseUrl url donde se aloja la imagen
     * @return imagen en linea codificada en base64
     */
    public static String setEncodeImage(String baseUrl) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(baseUrl);

            Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArrayImage = baos.toByteArray();
            return formatBase64 + Base64.encodeToString(byteArrayImage, Base64.URL_SAFE);
        } catch (IOException e) {
            Log.e(TAG, "Error en datos", e);
            return null;
        } catch (NullPointerException e){
            Log.e(TAG, "Error en datos nulos", e);
            return null;
        }
    }

    /**
     * Establece la decodificación de una cadena base64 a mapa de bits
     * @param path imagen codificada en cadena base64
     * @return mapa de bits de la imagen
     */
    public static Bitmap getDecodeImage(String path) {
        final String base64Encoded = path.substring(path.indexOf(",") + 1);
        byte[] decodedString = Base64.decode(base64Encoded, Base64.URL_SAFE);
        Bitmap decodeBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        decodeBitmap = ThumbnailUtils.extractThumbnail(decodeBitmap, reqWidth, reqHeight);
        return decodeBitmap;
    }

    /**
     * Establece la dimensión para una versión más pequeña en la memoria
     * @param options anchura y altura de la imagen
     * @return tamaño de la muestra en función de la altura y anchura
     */
    public static int calculateInSampleSize(BitmapFactory.Options options) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    /**
     * Estbalece configuraciones para el nuevo tamaño de la imagen
     * @param decodedString imagen codificada en bytearray
     * @return mapa de bits de la iamgen con las nuevas dimensiones
     */
    public static Bitmap decodeSampledBitmapFromResource(byte[] decodedString) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);

        options.inSampleSize = calculateInSampleSize(options);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
    }

    public static void saveJPGE_After(Context context, Bitmap bitmap, String path, int quality) {
        File file = new File(path);
        makeDir(file);
        try {
            FileOutputStream out = new FileOutputStream(file);
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)) {
                out.flush();
                out.close();
            }
            updateResources(context, file.getPath());
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Archivo no encontrado", e);
        } catch (IOException e) {
            Log.e(TAG, "Error en datos", e);
        }
    }
    private static void makeDir(File file) {
        File tempPath = new File(file.getParent());
        if (!tempPath.exists()) {
            tempPath.mkdirs();
        }
    }
    public static void updateResources(Context context, String path) {
        MediaScannerConnection.scanFile(context, new String[]{path}, null, null);
    }


    public static ArrayList<String> getAllShownImagesPath(Activity activity) {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        cursor = activity.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

}
