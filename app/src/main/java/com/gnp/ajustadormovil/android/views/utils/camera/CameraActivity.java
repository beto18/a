package com.gnp.ajustadormovil.android.views.utils.camera;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.animation.ValueAnimator.AnimatorUpdateListener;
import static android.animation.ValueAnimator.ofInt;
import static com.gnp.ajustadormovil.android.data.model.PersistenciaPath.createDirectory;

public class CameraActivity extends BaseActivity implements
        SurfaceHolder.Callback, View.OnClickListener {
    private Camera mCamera;
    private SurfaceView surfaceView;
    private SurfaceHolder mHolder;
    private int mCameraId = 0;
    private Context context;
    private int screenWidth;
    private int screenHeight;
    private LinearLayout home_custom_top_relative;
    private ImageView camera_delay_time;
    private View homeCustom_cover_top_view;
    private View homeCustom_cover_bottom_view;
    private ImageView flash_light;
    private TextView camera_delay_time_text;
    private ImageView camera_square;
    private int index;
    private int menuPopviewHeight;
    private int animHeight;
    private int light_num = 0;
    private int delay_time;
    private boolean isview = false;
    private boolean is_camera_delay;
    public static Long idOcupantes;
    //public static String idOcupante;
    //variables para la creacion de la carpeta de fotos
    public static int modulo;
    public static int tipoAsegurados;
    public static int tipoTerceros;
    public static int numOcupante;
    public static String id_auto;
    public static boolean isSaveVehicle;
    public static boolean isSaveDriver;

    private RelativeLayout homecamera_bottom_relative;
    private int picHeight;
    private ArrayList<ImageGeneralModel> listFotos;
    public static int declaracion;

    //para datos de eDUA
    private String fechaConvertida,fecha, hora, folioDua, cedulaAjustador, separador;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        context = this;
        initView();
        initData();
    }


    private void initView() {

        surfaceView = findViewById(R.id.surfaceView);
        mHolder = surfaceView.getHolder();
        mHolder.addCallback(this);
        ImageView img_camera = findViewById(R.id.img_camera);
        img_camera.setOnClickListener(this);

        ImageView camera_close = findViewById(R.id.camera_close);
        camera_close.setOnClickListener(this);

        home_custom_top_relative = findViewById(R.id.home_custom_top_relative);
        home_custom_top_relative.setAlpha(0.5f);

        ImageView camera_frontback = findViewById(R.id.camera_frontback);
        camera_frontback.setOnClickListener(this);

        camera_delay_time = findViewById(R.id.camera_delay_time);
        camera_delay_time.setOnClickListener(this);

        camera_square = findViewById(R.id.camera_square);
        camera_square.setOnClickListener(this);

        homeCustom_cover_top_view = findViewById(R.id.homeCustom_cover_top_view);
        homeCustom_cover_bottom_view = findViewById(R.id.homeCustom_cover_bottom_view);

        homeCustom_cover_top_view.setAlpha(0.5f);
        homeCustom_cover_bottom_view.setAlpha(0.5f);

        View home_camera_cover_top_view = findViewById(R.id.home_camera_cover_top_view);
        View home_camera_cover_bottom_view = findViewById(R.id.home_camera_cover_bottom_view);
        home_camera_cover_top_view.setAlpha(1);
        home_camera_cover_bottom_view.setAlpha(1);

        flash_light = findViewById(R.id.flash_light);
        flash_light.setOnClickListener(this);

        camera_delay_time_text = findViewById(R.id.camera_delay_time_text);

        homecamera_bottom_relative = findViewById(R.id.homecamera_bottom_relative);
    }

    private void initData() {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels;

        menuPopviewHeight = screenHeight - screenWidth * 4 / 3;
        animHeight = (screenHeight - screenWidth - menuPopviewHeight - new ImageUtils().dp2px(context, 44)) / 2;

        RelativeLayout.LayoutParams bottomParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, menuPopviewHeight);
        bottomParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        homecamera_bottom_relative.setLayoutParams(bottomParam);
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressLint("SetTextI18n")
        public void handleMessage(android.os.Message msg) {
            int what = msg.what;
            switch (what) {
                case Constants.WHAT.SUCCESS:
                    if (delay_time > 0) {
                        camera_delay_time_text.setText("" + delay_time);
                    }

                    try {
                        if (delay_time == 0) {
                            capture();
                            is_camera_delay = false;
                            camera_delay_time_text.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        return;
                    }

                    break;

                case Constants.WHAT.ERROR:
                    is_camera_delay = false;
                    break;

            }
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_camera:
                capturePhoto();
                break;

            case R.id.camera_square:
                if (index == 0) {
                    camera_square_0();
                } else if (index == 1) {
                    camera_square_1();
                }
                break;

            case R.id.camera_frontback:
                switchCamera();
                break;

            case R.id.camera_close:
                if (is_camera_delay) {
                    Toast.makeText(CameraActivity.this, "...", Toast.LENGTH_SHORT).show();
                    return;
                }
                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.flash_light:
                if (mCameraId == 1) {
                    return;
                }
                if (mCamera != null && mCamera.getParameters()!= null && mCamera.getParameters().getFlashMode() != null) {
                    Camera.Parameters parameters = mCamera.getParameters();
                    switch (light_num) {
                        case 0:
                            light_num = 1;
                            flash_light.setImageResource(R.drawable.btn_camera_flash_on);
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                            mCamera.setParameters(parameters);
                            break;
                        case 1:
                            light_num = 2;
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                            mCamera.setParameters(parameters);
                            flash_light.setImageResource(R.drawable.btn_camera_flash_auto);
                            break;
                        case 2:
                            light_num = 0;
                            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                            mCamera.setParameters(parameters);
                            flash_light.setImageResource(R.drawable.btn_camera_flash_off);
                            break;
                    }
                }

                break;

            case R.id.camera_delay_time:
                switch (delay_time) {
                    case 0:
                        delay_time = 3;
                        camera_delay_time.setImageResource(R.drawable.btn_camera_timing_3);
                        break;

                    case 3:
                        delay_time = 5;
                        camera_delay_time.setImageResource(R.drawable.btn_camera_timing_5);
                        break;

                    case 5:
                        delay_time = 10;
                        camera_delay_time.setImageResource(R.drawable.btn_camera_timing_10);
                        break;

                    case 10:
                        delay_time = 0;
                        camera_delay_time.setImageResource(R.drawable.btn_camera_timing_0);
                        break;

                }
        }
    }

    private void capturePhoto() {
        if (isview) {
            if (delay_time == 0) {
                switch (light_num) {
                    case 0:
                        CameraUtil.getInstance().turnLightOff(mCamera);
                        break;
                    case 1:
                        CameraUtil.getInstance().turnLightOn(mCamera);
                        break;
                    case 2:
                        CameraUtil.getInstance().turnLightAuto(mCamera);
                        break;
                }
                try {
                    capture();
                } catch (Exception e) {
                    Log.e(TAG, "Error al tomar fotografía", e);
                }
            } else {
                camera_delay_time_text.setVisibility(View.VISIBLE);
                camera_delay_time_text.setText(String.valueOf(delay_time));
                is_camera_delay = true;
                new Thread(() -> {
                    while (delay_time > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            mHandler.sendEmptyMessage(Constants.WHAT.ERROR);
                            return;
                        }
                        delay_time--;
                        mHandler.sendEmptyMessage(Constants.WHAT.SUCCESS);
                    }
                }).start();
            }
            isview = false;
        }
    }

    public void switchCamera() {
        releaseCamera();
        mCameraId = (mCameraId + 1) % Camera.getNumberOfCameras();
        mCamera = getCamera(mCameraId);
        if (mHolder != null) {
            startPreview(mCamera, mHolder);
        }
    }

    public void camera_square_0() {
        camera_square.setImageResource(R.drawable.btn_camera_size1_n);

        ValueAnimator anim = ofInt(0, animHeight);
        anim.setDuration(300);
        anim.addUpdateListener(animation -> {
            int currentValue = Integer.parseInt(animation.getAnimatedValue().toString());
            RelativeLayout.LayoutParams Params = new RelativeLayout.LayoutParams(screenWidth, currentValue);
            Params.setMargins(0, new ImageUtils().dp2px(context, 44), 0, 0);
            homeCustom_cover_top_view.setLayoutParams(Params);

            RelativeLayout.LayoutParams bottomParams = new RelativeLayout.LayoutParams(screenWidth, currentValue);
            bottomParams.setMargins(0, screenHeight - menuPopviewHeight - currentValue, 0, 0);
            homeCustom_cover_bottom_view.setLayoutParams(bottomParams);
        });
        anim.start();

        homeCustom_cover_top_view.bringToFront();
        home_custom_top_relative.bringToFront();
        homeCustom_cover_bottom_view.bringToFront();
        index++;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void camera_square_1() {
        camera_square.setImageResource(R.drawable.btn_camera_size2_n);

        ValueAnimator anim = ofInt(animHeight, 0);
        anim.setDuration(300);
        anim.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int currentValue = Integer.parseInt(animation.getAnimatedValue().toString());
                RelativeLayout.LayoutParams Params = new RelativeLayout.LayoutParams(screenWidth, currentValue);
                Params.setMargins(0, new ImageUtils().dp2px(context, 44), 0, 0);
                homeCustom_cover_top_view.setLayoutParams(Params);

                RelativeLayout.LayoutParams bottomParams = new RelativeLayout.LayoutParams(screenWidth, currentValue);
                bottomParams.setMargins(0, screenHeight - menuPopviewHeight - currentValue, 0, 0);
                homeCustom_cover_bottom_view.setLayoutParams(bottomParams);
            }
        });
        anim.start();
        index = 0;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mCamera == null) {
            if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                Toast.makeText(this, "Cámara no disponible", Toast.LENGTH_LONG).show();
            } else {
                mCamera = getCamera(mCameraId);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    /**
     * @return
     */
    private Camera getCamera(int id) {
        Camera camera = null;
        try {
            camera = Camera.open(id);
        } catch (Exception e) {

        }
        return camera;
    }


    private void startPreview(Camera camera, SurfaceHolder holder) {
        try {
            setupCamera(camera);
            camera.setPreviewDisplay(holder);
            CameraUtil.getInstance().setCameraDisplayOrientation(this, mCameraId, camera);
//            camera.setDisplayOrientation(90);
            camera.startPreview();
            isview = true;
        } catch (IOException e) {
            Log.e(TAG, "Error al crear preview", e);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Constants.RESULT_CODE.RESULT_CANCELED);
        finish();
    }

    private LatLng getUbicacion(){
        LatLng latLng = null;
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getBaseActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude;
        double latitude;
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            latLng = new LatLng(latitude, longitude);
            if (longitude != 0.0 && latitude != 0.0) {

            }
        }
        return latLng;
    }

    private void capture() {
        mCamera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                String dataAjuste = "";
                isview = false;
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                Bitmap saveBitmap = CameraUtil.getInstance().setTakePicktrueOrientation(mCameraId, bitmap);

                saveBitmap = Bitmap.createScaledBitmap(saveBitmap, screenWidth, picHeight, true);

                if (index == 1) {
                    saveBitmap = Bitmap.createBitmap(saveBitmap, 0, animHeight + new ImageUtils().dp2px(context, 44), screenWidth, screenWidth);
                } else {
                    saveBitmap = Bitmap.createBitmap(saveBitmap, 0, 0, screenWidth, picHeight);
                }
                try{
                    String ajusteId = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);

                    AjusteRepository repository = RepositoryFactory.getAjusteRepository();
                    Ajuste ajuste = repository.findById(ajusteId);
                    AmisRepository amisRepository = RepositoryFactory.getAmisRepository();
                    Amis amis = amisRepository.findByAjuste(ajusteId);

                    folioDua = amis.getFolio().getFolio()!= null?amis.getFolio().getFolio():amis.getFolio().getFolioInicial();

                    LatLng latLng = getUbicacion();
                    if(latLng == null){
                        latLng = new LatLng(0.0, 0.0);
                    }
                    //Ajuste ajusteDetalle = new Gson().fromJson(AppPreferences.getString(AppPreferences.ESTATUS_DECLARACION), Ajuste.class);

                    //obteniendo la fecha y hora de arribo
                    Date date = ajuste.getFechaArribo();

                    //haciendo format a la fecha y hora
                    DateFormat format = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
                    fechaConvertida = format.format(date);
                    String[] fechaPartida = fechaConvertida.split(",");

                    //seteando valores
                    fecha = fechaPartida[0];
                    hora = fechaPartida[1].trim();

                    cedulaAjustador = AppPreferences.getString(AppPreferences.USER_ID_CEDULA);
                    separador = "|";

                    //armando texto de acuerdo a eDUA
                    dataAjuste = fecha + separador + hora + separador + String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude)
                            + separador + folioDua + separador + cedulaAjustador;
                    saveBitmap = addBorderToBitmap(saveBitmap, Constants.IMAGEN_TOMADA, dataAjuste);
                }
                catch (Exception ex){
                    Log.e("ERROR_FOTO", "ERROR CREANDO IMAGEN CON BORDE ", ex);
                }
                String img_path = createDirectory(modulo, tipoAsegurados, tipoTerceros,id_auto, idOcupantes!=null?Long.toString(idOcupantes):"");

                /*String img_path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() +
                        File.separator + System.currentTimeMillis() + ".jpeg";*/


                BitmapUtils.saveJPGE_After(context, saveBitmap, img_path, 100);

                if (!bitmap.isRecycled()) {
                    bitmap.recycle();
                }

                if (!saveBitmap.isRecycled()) {
                    saveBitmap.recycle();
                }

                Intent intent = new Intent();
                intent.putExtra(Constants.KEY.IMG_PATH, img_path);
                intent.putExtra(Constants.KEY.PIC_WIDTH, screenWidth);
                intent.putExtra(Constants.KEY.PIC_HEIGHT, picHeight);
                setResult(Constants.RESULT_CODE.RESULT_OK, intent);
                finish();

            }
        });
    }


    // Custom method to add a border around bitmap
    protected Bitmap addBorderToBitmap(Bitmap srcBitmap, String tipoFoto, String dataAjuste){
        int borderWidth = 50;
        Bitmap dstBitmap = Bitmap.createBitmap(
                srcBitmap.getWidth() + borderWidth*2, // Width
                srcBitmap.getHeight() + borderWidth*2, // Height
                Bitmap.Config.ARGB_8888 // Config
        );

        Bitmap workingBitmap = Bitmap.createBitmap(srcBitmap);
        Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvasImg = new Canvas(mutableBitmap);

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#19000000"));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth);
        paint.setAntiAlias(true);

        Paint paint2;
        paint2= new Paint();
        paint2.setColor(Color.WHITE);
        paint2.setTextSize(50);  //set text size
        paint2.setTextAlign(Paint.Align.CENTER);

        Paint paint3;
        paint3= new Paint();
        paint3.setColor(Color.WHITE);
        paint3.setTextSize(20);  //set text size


        Rect rect = new Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvasImg.getWidth() - borderWidth/2,
                canvasImg.getHeight() - borderWidth/2
        );


        canvasImg.drawRect(rect,paint);
        canvasImg.drawText(tipoFoto, canvasImg.getWidth() - 30, canvasImg.getHeight() - 10 ,paint2);
        canvasImg.drawText(dataAjuste, borderWidth, borderWidth/2 +10 ,paint3);

        //canvas.drawBitmap(mutableBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle();

        return mutableBitmap;
    }
    private void setupCamera(Camera camera) {
        if (camera == null){
            mCameraId = (mCameraId + 1) % Camera.getNumberOfCameras();
            camera = getCamera(mCameraId);
        }
        Camera.Parameters parameters = camera.getParameters();

        if (parameters.getSupportedFocusModes().contains(
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }
        Camera.Size previewSize = CameraUtil.getInstance().getPropSizeForHeight(parameters.getSupportedPreviewSizes(), 700);
        parameters.setPreviewSize(previewSize.width, previewSize.height);

        Camera.Size pictrueSize = CameraUtil.getInstance().getPropSizeForHeight(parameters.getSupportedPictureSizes(), 1300);
        parameters.setPictureSize(pictrueSize.width, pictrueSize.height);

        camera.setParameters(parameters);

        picHeight = (screenWidth * pictrueSize.width) / pictrueSize.height;

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(screenWidth, (screenWidth * pictrueSize.width) / pictrueSize.height);
        //params.gravity = Gravity.CENTER;
        surfaceView.setLayoutParams(params);
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startPreview(mCamera, holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mCamera.stopPreview();
        startPreview(mCamera, holder);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseCamera();
    }

}
