package com.gnp.ajustadormovil.android.views.utils.camera;

import android.content.Intent;
import android.os.Bundle;

import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

public class CustomCamera extends BaseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CameraUtil.getInstance().camera(CustomCamera.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constants.RESULT_CODE.RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE.CAMERA) {
                String img_path = data.getStringExtra(Constants.KEY.IMG_PATH);

                int picWidth = data.getIntExtra(Constants.KEY.PIC_WIDTH, 0);
                int picHeight = data.getIntExtra(Constants.KEY.PIC_HEIGHT, 0);

                Intent intent = new Intent(this, ShowPicActivity.class);
                intent.putExtra(Constants.KEY.PIC_WIDTH, picWidth);
                intent.putExtra(Constants.KEY.PIC_HEIGHT, picHeight);
                intent.putExtra(Constants.KEY.IMG_PATH, img_path);
                startActivityForResult(intent,204);
            } else {
                String img_path = data.getStringExtra(Constants.KEY.IMG_PATH);
                Intent intent = new Intent();
                intent.putExtra(Constants.KEY.IMG_PATH,img_path);
                setResult(Constants.RESULT_CODE.RESULT_OK, intent);
                finish();
            }
        } else if (resultCode == Constants.RESULT_CODE.RESULT_CANCELED){
            finish();
        } else if(resultCode == Constants.RESULT_CODE.RESULT_RESTART) {
            CameraUtil.getInstance().camera(CustomCamera.this);
        }
    }

}
