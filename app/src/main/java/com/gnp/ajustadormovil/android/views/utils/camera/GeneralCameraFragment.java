package com.gnp.ajustadormovil.android.views.utils.camera;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.data.model.PersistenciaPath;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.data.orquestador.impl.AmisRepository;
import com.gnp.ajustadormovil.android.error.LogicException;
import com.gnp.ajustadormovil.android.models.amis.Amis;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmList;

import static com.gnp.ajustadormovil.android.views.ajuste.ConfirmacionActivity.REQUEST_LOCATION_PERMISSION_CODE;
import static com.gnp.ajustadormovil.android.views.commons.base.BaseActivity.getBaseActivity;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.idOcupantes;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.id_auto;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.isSaveDriver;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.isSaveVehicle;
import static com.gnp.ajustadormovil.android.views.utils.camera.CameraActivity.tipoAsegurados;


public class GeneralCameraFragment extends BaseFragment implements View.OnClickListener,
        CallbackItemImage<Long> {

    private static final int PICK_IMAGE = 207;
    @BindView(R.id.ivCameraGeneral)
    ImageView ivCameraPhoto;
    @BindView(R.id.rvImageGeneral)
    RecyclerView rvImages;
    @BindView(R.id.imageViewAddGallery)
    ImageButton imageViewAddGallery;
    @BindView(R.id.textView24)
    TextView title;
    Unbinder unbinder;

    //private CameraUtils cameraUtils;
    private ImageCameraAdapter adapterImage;
    private List<ImageGeneralModel> filesPhotos;
    private Activity activity;
    private CallbackItemImage callbackClickItem;
    private CallbackItemImage callbackId;
    private String typeFragmnet, dataAjuste, messageTipoModulo;
    private ArrayList<String> imagesEncodedList;
    private String ocultar = "", tipoFoto = "";
    private String fechaConvertida, fecha, hora, folio, cedulaAjustador, separador;
    private CompositeDisposable compositeDisposable;


    public GeneralCameraFragment() {

    }


    public GeneralCameraFragment newInstance() {
        Bundle args = new Bundle();
        GeneralCameraFragment fragment = new GeneralCameraFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void disableImport(String isDisable) {
        ocultar = isDisable;
    }

    public void disableCapture() {
        ivCameraPhoto.setVisibility(View.INVISIBLE);
        title.setVisibility(View.INVISIBLE);
    }

    public void setCallBackItem(CallbackItemImage callback) {
        this.callbackId = callback;
    }

    public List<ImageGeneralModel> getFileList() {
        return filesPhotos;
    }

    public void setImageList(List<ImageGeneralModel> photosWS) {
        if (filesPhotos != null) {
            filesPhotos.clear();
            filesPhotos.addAll(photosWS);
            adapterImage.notifyDataSetChanged();
        }
    }

    public void updateImageList(List<ImageGeneralModel> photosWS) {
        if (filesPhotos != null) {
            filesPhotos.clear();
            filesPhotos.addAll(photosWS);
            adapterImage.notifyDataSetChanged();
        }
    }


    public void setFolder(int modulo, int tipoAsegurados, int tipoTerceros, String idOCupante, String id_auto) {
        CameraActivity.modulo = modulo;
        CameraActivity.tipoAsegurados = tipoAsegurados;
        CameraActivity.tipoTerceros = tipoTerceros;
        CameraActivity.id_auto = id_auto;
        idOcupantes = idOCupante != "" ? Long.parseLong(idOCupante) : 0L;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_general_camera, container, false);
        unbinder = ButterKnife.bind(this, mView);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        filesPhotos = new RealmList<>();
        callbackClickItem = this;
        ivCameraPhoto.setOnClickListener(this);
        imageViewAddGallery.setOnClickListener(this);
        if (ocultar.equals("Declaracion")) {
            imageViewAddGallery.setVisibility(View.GONE);

        }

        typeFragmnet = getTag() != null ? getTag() : "";
        setAdapterRecycler();
        return mView;
    }


    private boolean getRequestFragmentByTag(String tag) {
        boolean showMessage = false;
        switch (tag) {
            case Constants.TAG_THIRD_PEDESTRIAN:
                if ("".equals(id_auto)) showMessage = true;
                messageTipoModulo = "Peatón";
                break;
            case Constants.TAG_THIRD_NO_VEHICLE:
                if ("".equals(id_auto)) showMessage = true;
                messageTipoModulo = "No Auto";
                break;
            case Constants.TAG_INSURE_OCCUPANT:
            case Constants.TAG_THIRD_OCCUPANT:
                if (new Long(0L).equals(idOcupantes)) showMessage = true;
                messageTipoModulo = "Ocupante";
                break;
            case Constants.TAG_THIRD_DRIVER:
                if (!isSaveDriver) showMessage = true;
                messageTipoModulo = Constants.CONDUCTOR;


                break;
            case Constants.TAG_THIRD_VEHICLE:
                if (!isSaveVehicle) showMessage = true;
                messageTipoModulo = Constants.VEHICULO;
                break;
            case Constants.TAG_DECLARACION:
                messageTipoModulo = Constants.DECLARACION;
                break;
        }

        return showMessage;
    }


    private void setAdapterRecycler() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(linearLayoutManager);
        rvImages.setHasFixedSize(true);
        rvImages.setItemViewCacheSize(10);
        rvImages.setDrawingCacheEnabled(true);
        rvImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        adapterImage = new ImageCameraAdapter(filesPhotos, getContext(), callbackClickItem, typeFragmnet);
        rvImages.setAdapter(adapterImage);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            String ajusteId = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
            AjusteRepository repository = RepositoryFactory.getAjusteRepository();
            Ajuste ajuste = repository.findById(ajusteId);
            AmisRepository amisRepository = RepositoryFactory.getAmisRepository();
            Amis amis = amisRepository.findByAjuste(ajusteId);

            folio = validateFolio(ajuste,amis);

            LatLng latLng = new LatLng(0.0, 0.0);
            if (getUbicacion() != null) {
                latLng = getUbicacion();
            }
            //obteniendo la fecha y hora de arribo
            Date date = new Date();
            try {

                //obteniendo la fecha y hora de arribo
                date = ajuste.getFechaArribo();
            } catch (Exception ex) {
                Log.e("ERROR_DATE_CAMERA", "Error obteniendo la fecha de asignación del siniestro", ex);
            }


            //haciendo format a la fecha y hora
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy, hh:mm:ss");
            fechaConvertida = format.format(date);
            String[] fechaPartida = fechaConvertida.split(",");

            //seteando valores
            fecha = fechaPartida[0];
            hora = fechaPartida[1].trim();
            cedulaAjustador = AppPreferences.getString(AppPreferences.USER_ID_CEDULA);
            separador = "|";

            //armando texto de acuerdo a eDUA
            dataAjuste = fecha + separador + hora + separador + String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude)
                    + separador + folio + separador + cedulaAjustador;


            Completable process = null;
            if (requestCode == 204) {
                tipoFoto = Constants.IMAGEN_TOMADA;
                String img_path = data.getStringExtra(Constants.KEY.IMG_PATH);
                process = saveImagesCamera(img_path);
            }
            if (requestCode == PICK_IMAGE && null != data) {
                tipoFoto = Constants.IMAGEN_CARGADA;
                process = savesImagesGalLery(data);
            }
            if (process != null) {
                compositeDisposable.add(process
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(it -> LoadingIndicator.start("Procesando imágenes"))
                        .doFinally(LoadingIndicator::finish)
                        .subscribe(() -> {
                            adapterImage.notifyDataSetChanged();
                        }, throwable -> {
                            if (throwable instanceof LogicException) {
                                showMessageDeclaracion();
                            } else {
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable.getMessage());
                            }
                        }));
            }
        }
    }

    private String validateFolio(Ajuste ajuste, Amis amis){
        String folio;
        if (amis != null && !ajuste.getSiniestro().isPresiniestro()) {
            folio = amis.getFolio().getFolio() != null ? amis.getFolio().getFolio() : amis.getFolio().getFolioInicial();
        } else {
            folio = ajuste.getSiniestro().getId();
        }
        return folio;
    }

    protected Bitmap addBorderToBitmap(Bitmap srcBitmap, String tipoFoto, String dataAjuste) {
        // Initialize a new Bitmap to make it bordered bitmap
        int borderWidth = 50;


        Bitmap workingBitmap = Bitmap.createBitmap(srcBitmap);
        Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Canvas canvasImg = new Canvas(mutableBitmap);

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#19000000"));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth);
        paint.setAntiAlias(true);

        Paint paint2;
        paint2 = new Paint();
        paint2.setColor(Color.WHITE);
        paint2.setTextSize(50);  //set text size
        paint2.setTextAlign(Paint.Align.CENTER);

        Paint paint3;
        paint3 = new Paint();
        paint3.setColor(Color.WHITE);
        paint3.setTextSize(20);  //set text size


        Rect rect = new Rect(
                borderWidth / 2,
                borderWidth / 2,
                canvasImg.getWidth() - borderWidth / 2,
                canvasImg.getHeight() - borderWidth / 2
        );

        canvasImg.drawRect(rect, paint);
        canvasImg.drawText(tipoFoto, canvasImg.getWidth() - 30, canvasImg.getHeight() - 10, paint2);
        canvasImg.drawText(dataAjuste, borderWidth, borderWidth / 2 + 10, paint3);

        //canvas.drawBitmap(mutableBitmap, borderWidth, borderWidth, null);
        srcBitmap.recycle();

        return mutableBitmap;
    }

    private Completable savesImagesGalLery(Intent data) {
        return Completable.create(e -> {
            if (data.getClipData() != null) {
                if (data.getClipData().getItemCount() != 0) {
                    int size = data.getClipData().getItemCount();
                    for (int i = 0; i < size; i++) {
                        LoadingIndicator.message("Comprimiendo imágenes " + (i + 1) + "/" + size);
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        getBaseActivity().getContentResolver().notifyChange(imageUri, null);

                        ContentResolver crs = getBaseActivity().getContentResolver();
                        Bitmap bitmaps;
                        try {
                            bitmaps = android.provider.MediaStore.Images.Media
                                    .getBitmap(crs, imageUri);


                            // Add a border around the bitmap
                            bitmaps = addBorderToBitmap(bitmaps, tipoFoto, dataAjuste);

                            imageUri = getImageUri(bitmaps);
                        } catch (Exception ex) {

                            Log.e("PICK_IMAGE", ex.toString());
                        }
                        if (!setImagesFromGallery(data, imageUri)) {
                            e.tryOnError(new Exception(getString(R.string.message_error_camera_local)));
                        }
                    }
                    e.onComplete();
                } else {
                    e.tryOnError(new Exception("No fue seleccionada ninguna imágen"));
                }
            }
        });
    }

    private Completable saveImagesCamera(String img_path) {
        return Completable.create(e -> {
            if (typeFragmnet.equals(Constants.TAG_DECLARACION) && filesPhotos.size() >= 2) {
                e.tryOnError(new LogicException("max_size", "Solo debe agregar 2 fotografías"));

            } else {
                filesPhotos.add(new ImageGeneralModel("", null, img_path, true));
                e.onComplete();
            }


        });
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        if (result == null) {
            result = getImagePathFromInputStreamUri(contentURI);
        }
        return result;
    }

    private LatLng getUbicacion() {
        LatLng latLng = null;
        if (ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getBaseActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        }

        LocationManager lm = (LocationManager) getBaseActivity().getSystemService(Context.LOCATION_SERVICE);

        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude;
        double latitude;
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            latLng = new LatLng(latitude, longitude);
        }
        return latLng;
    }

    private boolean setImagesFromGallery(Intent data, Uri imageUri) {
        boolean isboolGallery;
        String img_path = getRealPathFromURI(imageUri);
        ImageUtils imageUtils = new ImageUtils();
        PersistenciaPath.createDirectory(CameraActivity.modulo, tipoAsegurados, CameraActivity.tipoTerceros, id_auto, idOcupantes != null ? idOcupantes.toString() : "");
        if (img_path != null) {
            isboolGallery = true;
            String directory = imageUtils.saveImages(imageUtils.getBitmapToFile(img_path));
            filesPhotos.add(new ImageGeneralModel("", null, directory, true));
        } else {
            isboolGallery = false;
        }

        return isboolGallery;
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getBaseActivity().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCameraGeneral:
                if (getRequestFragmentByTag(typeFragmnet)) {
                    showMessgeValidateImageSave();
                } else {
                    if (RequestPermissions.checkPermissions(RequestPermissions
                            .REQUEST_CAMERA_PERMISSION)) { //cameraUtils.invokeCamera();
                        Intent intentCamera = new Intent(getActivity(), CustomCamera.class);
                        startActivityForResult(intentCamera, 204);
                    } else RequestPermissions.requestPermission(RequestPermissions
                            .REQUEST_CAMERA_PERMISSION, activity);

                }
                break;
            case R.id.imageViewAddGallery:
                if (getRequestFragmentByTag(typeFragmnet)) {
                    showMessgeValidateImageSave();
                } else {
                    openGallery();
                }
                break;

        }
    }


    @Override
    public void onCallbackItemId(int position, Long id) {
        DialogUtils.createAlert(activity, getString(R.string.text_title_delete_image),
                getString(R.string.text_message_delete_image),
                getString(R.string.btn_continuar_text),
                getString(R.string.btn_cancel_text), (dialog, which) -> {
                    try {
                        if (id != null) {
                            if (id != 0L) {
                                filesPhotos.remove(position);
                                adapterImage.notifyDataSetChanged();
                                callbackId.onCallbackItemId(position, id);
                            } else {
                                filesPhotos.remove(position);
                                adapterImage.notifyDataSetChanged();
                            }

                        }

                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                    }

                },
                (dialog, which) -> dialog.dismiss());

    }

    @Override
    public void onCallItemId(int position, Long id) {
        ImageGeneralModel image = adapterImage.getItem(position);
        FragmentTransaction fragmentTransaction = ((AppCompatActivity)activity).getSupportFragmentManager().beginTransaction();
        ShowPickImageFragment fragment = ShowPickImageFragment.newInstance(image.getFoto()!= null ? image.getFoto().getUrl():image.getFilePhat());
        fragment.show(fragmentTransaction,"dialogo");

    }


    private void showMessgeValidateImageSave() {
        DialogUtils.createAlert(getContext(), messageTipoModulo,
                getContext().getString(R.string.message_save_images_path), "Aceptar",
                null, (dialogInterface, i) -> {
                }, null);

    }


    private void showMessageDeclaracion() {

        DialogUtils.createAlert(getContext(), "Declaración", "Máximo 2 imágenes", "Aceptar",
                null, (dialogInterface, i) -> {
                }, null);

    }


    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/jpg");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private String getPath(Uri uri) {
        String[] projection = {android.provider.MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(android.provider.MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public String getImagePathFromInputStreamUri(Uri uri) {
        InputStream inputStream = null;
        String filePath = null;

        if (uri.getAuthority() != null) {
            try {
                inputStream = getActivity().getContentResolver().openInputStream(uri);
                File photoFile = createTemporalFileFrom(inputStream);
                filePath = photoFile.getPath();
            } catch (IOException e) {
                Log.d("ErrorImage :", e.getMessage());
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Error al acceder a imagen", e);
                }
            }
        }

        return filePath;
    }

    private File createTemporalFileFrom(InputStream inputStream) throws IOException {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];

            targetFile = createTemporalFile();
            OutputStream outputStream = new FileOutputStream(targetFile);

            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            outputStream.flush();

            try {
                outputStream.close();
            } catch (IOException e) {
                Log.e(TAG, "Error al crear a imagen", e);
            }
        }

        return targetFile;
    }

    private File createTemporalFile() {
        return new File(getActivity().getExternalCacheDir(), "tempFile.jpg"); // context needed
    }


}
