package com.gnp.ajustadormovil.android.views.utils.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.model.ImageGeneralModel;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.media.CircleTransform;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageCameraAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ImageGeneralModel> list;
    private Context ctx;
    private CallbackItemImage callbackClickItem;
    private String tipoModulo;
    private String typeFragment;
    private String TAG = "TAG";


    public ImageCameraAdapter(List<ImageGeneralModel> list, Context context,
                              CallbackItemImage listener, String typeFragment) {
        this.list = list;
        this.ctx = context;
        this.callbackClickItem = listener;
        this.typeFragment = typeFragment;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = null;

        if (viewType == Constants.SERVICIO) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images_recycler, parent, false);
            return new HorizontalViewHolder(v);
        } else if (viewType == Constants.CARPETA) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images_recycle_path, parent, false);
            return new HorizontalViewHolder(v);
        } else if (viewType == Constants.TYPE_EMPTY) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item_image, parent, false);
            return new HorizontalViewHolder(v);
        } else if (viewType == Constants.GUARDADDO) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_recycler_save, parent, false);
            return new HorizontalViewHolder(v);
        }
        throw new RuntimeException("No match for " + viewType + " .");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HorizontalViewHolder) {
            ImageGeneralModel image = list.get(position);
            if (image != null) {
                if (image.getNewFile() && image.getFilePhat() != null && new File(image.getFilePhat()).exists()) {
                    Log.d(TAG, "obteniendo imagenes de servicio:" + list.get(position).getFilePhat());
                    Bitmap bitmap = new ImageUtils().getImageFromPath(image.getFilePhat(), 200, 200);
                    Bitmap bitmapImage = new CircleTransform().transform(bitmap);
                    ((HorizontalViewHolder) holder).addPicture.setImageBitmap(bitmapImage);
                } else if (!image.getNewFile() && image.getFoto() != null && image.getFoto().getUrl() != null && !image.getFoto().getUrl().contains("https") && new File(image.getFoto().getUrl()).exists()) {
                    Bitmap bitmap = new ImageUtils().getImageFromPath(image.getFoto().getUrl(), 200, 200);
                    Bitmap bitmapImage = new CircleTransform().transform(bitmap);
                    ((HorizontalViewHolder) holder).addPicture.setImageBitmap(bitmapImage);
                } else if (image.getFoto() != null) {
                    Drawable loader = null;
                    if (android.os.Build.VERSION.SDK_INT >= 28) {
                        loader = ContextCompat.getDrawable(ctx, R.drawable.image_loader);
                        ((Animatable) loader).start();
                    } else {
                        loader = ctx.getResources().getDrawable(R.drawable.ic_no_photo);
                    }
                    Picasso.with(ctx).load(image.getFoto().getUrl())
                            .placeholder(loader)
                            .fit()
                            .transform(new CircleTransform())
                            .error(R.drawable.offline)
                            .into(((HorizontalViewHolder) holder).addPicture);
                }

                if (typeFragment.equals(Constants.TAG_DECLARACION)) {
                    ((HorizontalViewHolder) holder).addPicture.setOnLongClickListener(null);
                    ((HorizontalViewHolder)holder).addPicture.setOnClickListener(v -> {
                        callbackClickItem.onCallItemId(position, Long.parseLong(!"".equals(image.getId()) ? image.getId() : "0"));
                    });
                } else {
                    ((HorizontalViewHolder) holder).addPicture.setOnLongClickListener(v -> {
                        callbackClickItem.onCallbackItemId(position, Long.parseLong(!"".equals(image.getId()) ? image.getId() : "0"));
                        return true;
                    });
                    ((HorizontalViewHolder)holder).addPicture.setOnClickListener(v -> {
                        callbackClickItem.onCallItemId(position, Long.parseLong(!"".equals(image.getId()) ? image.getId() : "0"));
                    });

                }


            }

        } else if (holder instanceof EmptyHolder) {
            ((EmptyHolder) holder).emptyImage.setText(R.string.no_hay_imagenes);
        }

    }

    public ImageGeneralModel getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (list.isEmpty()) return Constants.TYPE_EMPTY;
        else if (list.get(position).getNewFile()) return Constants.CARPETA;
        else if (!(list.get(position).getFoto().getUrl().contains("https")) && !list.get(position).getNewFile())
            return Constants.GUARDADDO;
        else return Constants.SERVICIO;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class HorizontalViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.add_picture)
        ImageView addPicture;

        HorizontalViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public static class EmptyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_empty_image)
        TextView emptyImage;
        @BindView(R.id.add_picture)
        ImageView addPicture;

        EmptyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
