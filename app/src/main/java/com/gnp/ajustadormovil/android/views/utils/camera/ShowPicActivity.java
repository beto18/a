package com.gnp.ajustadormovil.android.views.utils.camera;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.media.ImageUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

/**
 * Created by Ricardo Uraga on 20/10/2017.
 */

public class ShowPicActivity extends BaseActivity implements View.OnClickListener {

    String phatImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_pic);
        phatImage= getIntent().getExtras().getString(Constants.KEY.IMG_PATH);
        int picWidth = getIntent().getIntExtra(Constants.KEY.PIC_WIDTH, 0);
        int picHeight = getIntent().getIntExtra(Constants.KEY.PIC_HEIGHT, 0);
        ImageView img = findViewById(R.id.img);
        img.setImageURI(Uri.parse(phatImage));
        img.setLayoutParams(new RelativeLayout.LayoutParams(picWidth, picHeight));
        findViewById(R.id.cameraSaveImage).setOnClickListener(this);
        findViewById(R.id.cameraDeleteImage).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        returnCamera();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cameraDeleteImage:
                returnCamera();
                break;
            case R.id.cameraSaveImage:
                Intent intent = new Intent();
                intent.putExtra(Constants.KEY.IMG_PATH, phatImage);
                setResult(Constants.RESULT_CODE.RESULT_OK, intent);
                finish();
                break;
        }
    }


    public void returnCamera() {
        ImageUtils imageUtils = new ImageUtils();
        imageUtils.deleteImageForpath(phatImage,getBaseActivity());
        setResult(Constants.RESULT_CODE.RESULT_RESTART);
        finish();
    }
}
