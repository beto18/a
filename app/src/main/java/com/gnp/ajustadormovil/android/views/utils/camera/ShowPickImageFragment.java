package com.gnp.ajustadormovil.android.views.utils.camera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowPickImageFragment extends BaseDialogFragment {

    @BindView(R.id.imageCapture)
    AppCompatImageView imageCapture;
    private View view;
    private String url;
    private Activity activity;

    public static ShowPickImageFragment newInstance(String url) {
        ShowPickImageFragment instance = new ShowPickImageFragment();
        instance.url = url;
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dialog_showpick_image, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Drawable loader;
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            loader = ContextCompat.getDrawable(activity, R.drawable.image_loader);
            ((Animatable) loader).start();
        } else {
            loader = getResources().getDrawable(R.drawable.ic_no_photo);
        }

        (url.startsWith("http") ? Picasso.with(activity).load(url) : Picasso.with(activity).load(new File(url)))
                .error(R.drawable.offline)
                .placeholder(loader)
                .into(imageCapture);

    }

    @OnClick(R.id.imgCerrar)
    public void close() {
        dismiss();
    }


}
