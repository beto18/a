package com.gnp.ajustadormovil.android.views.utils.components;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;

@SuppressLint("ValidFragment")
public class ActivityResultFragment extends BaseFragment {

    private static final String TAG = ActivityResultFragment.class.getSimpleName();

    public interface OnActivityResultHandler {
        void onActivityResult(int requestCode, int resultCode, Intent intent);
    }

    public interface OnCreateHandler {
        void onCreate(ActivityResultFragment fragment);
    }

    private OnActivityResultHandler onActivityResultHandler;

    private OnCreateHandler onCreateHandler;

    public ActivityResultFragment(OnCreateHandler onCreate, OnActivityResultHandler onActivityResult) {
        this.onCreateHandler = onCreate;
        this.onActivityResultHandler = onActivityResult;
        this.open();
    }

    public ActivityResultFragment(){
        this(fragment -> {}, (requestCode, resultCode, intent) -> {});
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.onCreateHandler.onCreate(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        this.close();
        this.onActivityResultHandler.onActivityResult(requestCode, resultCode, intent);
    }

    public void intentActivity(Class<?> activity, int requestCode) {
        Intent intent = new Intent(this.getActivity(), activity);
        startActivityForResult(intent, requestCode);
    }

    private void open() {
        BaseActivity.getBaseActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .add(this, this.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    private void close() {
        if(this.getActivity() != null && !isRemoving()) {
            try {
                this.getActivity()
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .remove(this)
                        .commitAllowingStateLoss();
            } catch (Exception ex) {
                Log.e(TAG, "Error al remover el fragmento", ex);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        close();
    }
}




