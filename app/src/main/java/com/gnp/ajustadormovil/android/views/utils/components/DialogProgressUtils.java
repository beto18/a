package com.gnp.ajustadormovil.android.views.utils.components;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;

public class DialogProgressUtils {

    private static final String TAG = DialogProgressUtils.class.getCanonicalName();

    private Dialog dialog;
    private Context context;


    public DialogProgressUtils(Context context) {
        this.dialog = new Dialog(context);
        this.context = context;
    }

    public void showLoader(String message) {
        try {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View viewroot = LayoutInflater.from(context).inflate(R.layout.dialog_loader, null);
            TextView textview = viewroot.findViewById(R.id.dialog_loader_text_message);
            textview.setText(message);

            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_SlideTop;
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(viewroot, new ViewGroup
                    .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            dialog.show();
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "Error al mostrar dialogo", e);
            if (dialog.isShowing())
                dialog.dismiss();
        }
    }

    public void dismisDialog(){
        dialog.dismiss();
    }

}
