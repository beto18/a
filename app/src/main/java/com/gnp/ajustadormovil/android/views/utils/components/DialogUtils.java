package com.gnp.ajustadormovil.android.views.utils.components;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

/**
 * Created by tixmedia on 3/20/18.
 */

public class DialogUtils {

    public enum DialogType {
        DEFAULT,
        WARNING,
        SUCCESS
    }

    public static void createAlert(Context c, String title, String message, String positiveBtn,
                                   String negativeBtn, DialogInterface.OnClickListener positive,
                                   DialogInterface.OnClickListener negative) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(c, R.style.AlertDialogTheme);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(positiveBtn, positive);
        alertDialog.setNegativeButton(negativeBtn, negative);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public static void alert(Context c, String title, String message, String positiveBtn, DialogInterface.OnClickListener positive) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(c, R.style.AlertDialogTheme);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(positiveBtn, positive);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public static void alert(String title, String message, DialogButton positive) {
        alert(DialogType.DEFAULT, title, message, positive, null);
    }

    public static void alert(int title, int message, DialogButton positive) {
        alert(DialogType.DEFAULT, title, message, positive, null);
    }

    public static void alert(String title, String message, DialogButton positive, DialogButton negative) {
        alert(DialogType.DEFAULT, title, message, positive, negative);
    }

    public static void alert(int title, int message, DialogButton positive, DialogButton negative) {
        alert(DialogType.DEFAULT, title, message, positive, negative);
    }

    public static void alert(DialogType type, int title, int message, DialogButton positive) {
        alert(type, title, message, positive, null);
    }

    public static void alert(DialogType type, int title, int message, DialogButton positive, DialogButton negative) {
        String strTitle = AjustadorMovilApplication.getInstance().getApplicationContext().getString(title);
        String strMessage = AjustadorMovilApplication.getInstance().getApplicationContext().getString(message);
        alert(type, strTitle, strMessage, positive, negative);
    }

    public static void alert(DialogType type, String title, String message, DialogButton positive) {
        alert(type, title, message, positive, null);
    }

    public static void alert(DialogType type, String title, String message, DialogButton positive, DialogButton negative) {
        AlertDialog.Builder alertDialog = createBuilder(type, title);
        alertDialog.setMessage(message);
        String positiveText = positive.getText() == null ?
                AjustadorMovilApplication.getInstance().getApplicationContext().getText(R.string.btn_accept_text).toString() :
                positive.getText();
        alertDialog.setPositiveButton(positiveText, (dialog, which) -> {
            positive.getAction().run();
            dialog.dismiss();
        });
        if (negative != null) {
            String negativeText = negative.getText() == null ?
                    AjustadorMovilApplication.getInstance().getApplicationContext().getText(R.string.btn_cancel_text).toString() :
                    negative.getText();
            alertDialog.setNegativeButton(negativeText, (dialog, which) -> {
                negative.getAction().run();
                dialog.dismiss();
            });
        }
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private static AlertDialog.Builder createBuilder(DialogType type, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.getBaseActivity(), R.style.AlertDialogTheme);
        if (!type.equals(DialogType.DEFAULT)) {
            View mView = LayoutInflater.from(builder.getContext()).inflate(R.layout.dialog_header, null);
            TextView mTitle = mView.findViewById(R.id.alertTitle);
            mTitle.setText(title);
            builder.setCustomTitle(mView);
            LinearLayout header = mView.findViewById(R.id.header);
            ImageView header_icon = mView.findViewById(R.id.header_icon);
            switch (type) {
                case WARNING:
                    header.setBackgroundColor(ContextCompat.getColor(builder.getContext(), R.color.colorRedStatus));
                    header_icon.setImageDrawable(ContextCompat.getDrawable(builder.getContext(), R.drawable.ic_warning));
                    header_icon.setBackgroundColor(ContextCompat.getColor(builder.getContext(), R.color.colorRedStatus));
                    break;
                case SUCCESS:
                    header.setBackgroundColor(ContextCompat.getColor(builder.getContext(), R.color.colorGreen));
                    header_icon.setImageDrawable(ContextCompat.getDrawable(builder.getContext(), R.drawable.ic_arround_check));
                    header_icon.setBackgroundColor(ContextCompat.getColor(builder.getContext(), R.color.colorGreen));
                    break;
            }
        } else {
            builder.setTitle(title);
        }
        return builder;
    }

    public static class DialogButton {

        private String text;

        private Runnable action;

        public DialogButton(String text, Runnable action) {
            this.text = text;
            this.action = action;
        }

        public DialogButton(Runnable action) {
            this.action = action;
        }

        public String getText() {
            return text;
        }

        public Runnable getAction() {
            return action;
        }
    }

}