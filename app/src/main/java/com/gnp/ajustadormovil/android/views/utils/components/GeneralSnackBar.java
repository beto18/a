package com.gnp.ajustadormovil.android.views.utils.components;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.commons.custom.SnackbarHelper;

/**
 *
 * Created by tixmedia on 3/15/18.
 *
 */

public class GeneralSnackBar {

    public final void shortSnack( Activity activity, int messageId) {
        createSnackbar(activity, messageId, Snackbar.LENGTH_SHORT).show();
    }

    public final void shortSnack( Activity activity,  String message) {
        createSnackbar(activity, message, Snackbar.LENGTH_SHORT).show();
    }


    public final void largeSnack( Activity activity, int messageId) {
        createSnackbar(activity, messageId, Snackbar.LENGTH_LONG).show();
    }

    public final void largeSnack( Activity activity,  String message) {
        createSnackbar(activity, message, Snackbar.LENGTH_LONG).show();
    }


    public final void largeSnackOk( Activity activity,  String message) {
        Snackbar snackbar =  createSnackbar(activity, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorGreen));
        snackbar.show();
    }

    public final void largeSnackError( Activity activity,  String message) {
        Snackbar snackbar =  createSnackbar(activity, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorRedStatus));
        snackbar.show();
    }

    public final void largeSnackError( View parent,  String message) {
        Snackbar snackbar =  createSnackbar(parent, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(parent.getContext(), R.color.colorRedStatus));
        snackbar.show();
    }

    public final void largeSnackError( Activity activity,  Throwable throwable) {
       largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
    }

    public final void largeSnackError( View parent,  Throwable throwable) {
        largeSnackError(parent, ErrorUtils.getErrorMessage(throwable));
    }

    private Snackbar createSnackbar(Activity activity, int messageId, int duration) {
        return createSnackbar(activity, activity.getResources().getString(messageId), duration);
    }

    private Snackbar createSnackbar(Activity activity, String message, int duration) {
       return createSnackbar(activity.findViewById(android.R.id.content),message, duration);
    }

    private Snackbar createSnackbar(View parent, int messageId, int duration) {
        return createSnackbar(parent, parent.getResources().getString(messageId), duration);
    }

    private Snackbar createSnackbar(View parent, String message, int duration) {
        Snackbar snackbar = Snackbar.make(parent, message, duration);
        View sn = snackbar.getView();
        TextView textView = sn.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.getView().bringToFront();
        return snackbar;
    }

    public void updownMakeSnackColorError(View parent, LinearLayout linearLayout, String messageId){
        Snackbar snackbar =  Snackbar.make(linearLayout, messageId, Snackbar.LENGTH_LONG);
        View sn = snackbar.getView();
        TextView textView = sn.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(parent.getResources().getColor(R.color.colorWhite));
        textView.setBackgroundColor(parent.getResources().getColor(R.color.colorRedStatus));
        textView.setMaxLines(5);
        SnackbarHelper.configSnackbar(parent.getContext(), snackbar, R.drawable.bg_snackbar);
        snackbar.show();
    }

    public void updownMakeSnackColorSuccess(View parent, LinearLayout linearLayout, String messageId){
        Snackbar snackbar =  Snackbar.make(linearLayout, messageId, Snackbar.LENGTH_LONG);
        View sn = snackbar.getView();
        TextView textView = sn.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(parent.getResources().getColor(R.color.colorWhite));
        textView.setBackgroundColor(parent.getResources().getColor(R.color.colorGreen));
        textView.setMaxLines(5);
        SnackbarHelper.configSnackbar(parent.getContext(), snackbar, R.drawable.bg_snackbar_success);
        snackbar.show();
    }

}
