package com.gnp.ajustadormovil.android.views.utils.components;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

public final class LoadingIndicator {

    private static final String TAG = LoadingIndicator.class.getSimpleName();

    private static final Object MONITOR = new Object();

    private static volatile MaterialDialog dialog;

    private static volatile int loaders = 0;

    private static volatile Activity ctx;

    private static final long DELAY = 250;

    private static volatile String msg;

    private LoadingIndicator() { }

    private static MaterialDialog getDialog() {
        if (ctx == null) {
            ctx = BaseActivity.getBaseActivity();
        }
        if (BaseActivity.getBaseActivity() != ctx) {
            Log.v(TAG, "Destruyendo indicador de 'cargando'");
            reset();
            ctx = BaseActivity.getBaseActivity();
        }
        if (dialog == null) {
            Log.v(TAG, "Creando nuevo indicador de 'cargando'");
            dialog = new MaterialDialog.Builder(ctx)
                    .title(ResourceUtils.getString("loading"))
                    .content(ResourceUtils.getString("loading.message"))
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(ContextCompat.getColor(ctx.getApplicationContext(), R.color.colorPrimaryDark))
                    .build();
        }
        return dialog;
    }

    public static void reset() {
        if (dialog != null && dialog.isShowing() && !ctx.isFinishing()) {
            dialog.dismiss();
        }
        dialog = null;
        ctx = null;
        synchronized (MONITOR) {
            loaders = 0;
            msg = null;
        }
    }

    public static void message(String messsage) {
        boolean delayed;
        synchronized (MONITOR) {
            delayed = msg != null;
            msg = messsage;
        }
        Runnable runnable = () -> {
            if(msg != null && getDialog().isShowing()){
                getDialog().setContent(msg);
            }
        };
        if(delayed) {
            onUiTreadDelayed(runnable);
        } else {
            onUiTread(runnable);
        }

    }

    public static void start() {
        synchronized (MONITOR) {
            Log.v(TAG, "Añadiendo proceso en cargando");
            loaders++;
        }
        onUiTread(LoadingIndicator::evaluate);
    }

    public static void start(String msg) {
        start();
        message(msg);
    }

    public static void finish() {
        synchronized (MONITOR) {
            Log.v(TAG, "Eliminando proceso en cargando");
            if (loaders > 0) {
                loaders--;
            }
        }
        onUiTreadDelayed(LoadingIndicator::evaluate, DELAY * 3);
    }

    private static void evaluate() {
        try {
            Log.v(TAG, "Procesos en cargando: " + loaders);
            if (loaders > 0 && !getDialog().isShowing()) {
                Log.v(TAG, "Mostrando dialogo");
                getDialog().show();
            }
            if (loaders == 0 && getDialog().isShowing()) {
                Log.v(TAG, "Ocultando dialogo");
                reset();
            }
        } catch (Exception e) {
            Log.v(TAG, "Error al mostrar el indicador de 'cargando'", e);
            reset();
        }
    }

    private static void onUiTread(Runnable runnable){
        if (BaseActivity.getBaseActivity()!= null)
            BaseActivity.getBaseActivity().runOnUiThread(runnable);
    }

    private static void onUiTreadDelayed(Runnable runnable){
        onUiTreadDelayed(runnable, DELAY);
    }

    private static void onUiTreadDelayed(Runnable runnable, long delay){
        if (BaseActivity.getBaseActivity()!= null) {
            BaseActivity.getBaseActivity().runOnUiThread(() -> {
                if (!new Handler().postDelayed(runnable, delay)) {
                    reset();
                }
            });
        }
    }

}
