package com.gnp.ajustadormovil.android.views.utils.components;

import android.widget.Spinner;

import com.gnp.ajustadormovil.android.models.catalogos.Keyable;

public class SpinnerUtils {

    public static int getPosition(Spinner spinner, Object object) {
        int indice = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i) instanceof Keyable) {
                if (object.equals(((Keyable) spinner.getItemAtPosition(i)).getKey())) {
                    indice = i;
                    break;
                }
            } else {
                if (object.equals(spinner.getItemAtPosition(i))) {
                    indice = i;
                    break;
                }

            }

        }


        return indice;
    }


    public static int getPosition(Spinner spinner, String clave) {
        int indice = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i) instanceof Keyable) {
                if (clave.equals(((Keyable) spinner.getItemAtPosition(i)).getKey())) {
                    indice = i;
                    break;
                }
            } else {
                if (clave.equals(spinner.getItemAtPosition(i))) {
                    indice = i;
                    break;
                }
            }

        }
        return indice;
    }
}
