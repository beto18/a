package com.gnp.ajustadormovil.android.views.utils.components;

import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;

public class ToastUtils {

    public static void toastShort(int messageResourceId) {
        Toast.makeText(AjustadorMovilApplication.getInstance(), AjustadorMovilApplication.getInstance().getResources().getString(messageResourceId), Toast.LENGTH_SHORT).show();
    }

    public static void toastLong(int messageResourceId) {
        Toast.makeText(AjustadorMovilApplication.getInstance(), AjustadorMovilApplication.getInstance().getResources().getString(messageResourceId), Toast.LENGTH_LONG).show();
    }

    public static void toastShort(String message) {
        Toast.makeText(AjustadorMovilApplication.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    public static void toastLong(String message) {
        Toast.makeText(AjustadorMovilApplication.getInstance(), message, Toast.LENGTH_LONG).show();
    }

    public static void toastShort(int messageResourceId, EditText editText) {
        Toast.makeText(AjustadorMovilApplication.getInstance(), AjustadorMovilApplication.getInstance().getResources().getString(messageResourceId) + " " + editText.getHint(), Toast.LENGTH_SHORT).show();
    }

    public static void toastLong(Context context, int messageResourceId) {
        Toast.makeText(context, AjustadorMovilApplication.getInstance().getResources().getString(messageResourceId), Toast.LENGTH_SHORT).show();
    }

    public static void toastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void toastLongError(String message){
        Context ctx = BaseActivity.getBaseActivity();
        Toast toast = Toast.makeText(ctx, message, Toast.LENGTH_LONG);
        toast.getView().setBackgroundResource(R.drawable.error_toast);
        ((TextView)toast.getView().findViewById(android.R.id.message)).setTextColor(ctx.getResources().getColor(R.color.colorWhite));
        toast.show();
    }


    public static void toastLongSuccess(String message){
        Context ctx = AjustadorMovilApplication.getInstance().getBaseContext();
        Toast toast = Toast.makeText(ctx, message, Toast.LENGTH_LONG);
        toast.getView().setBackgroundResource(R.drawable.success_toast);
        ((TextView)toast.getView().findViewById(android.R.id.message)).setTextColor(ctx.getResources().getColor(R.color.colorWhite));
        toast.show();
    }

    public static void toastLongError(Throwable throwable){
        toastLongError(ErrorUtils.getErrorMessage(throwable));
    }



}


