package com.gnp.ajustadormovil.android.views.utils.impresora;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.model.bluetoothdevice.DeviceItem;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceBluetoothAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DeviceItem> deviceItems;
    private CallbackItemImage onChildSelect;
    private DeviceItem mSelected;
    private int mSelectedItem = -1;
    private View vSelected;

    public DeviceBluetoothAdapter(List<DeviceItem> deviceItems, CallbackItemImage onChildSelect) {
        this.deviceItems = deviceItems;
        this.onChildSelect = onChildSelect;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Constants.TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item, parent, false);
            return new ViewHolderItem(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item, parent, false);
            return new EmptyViewHolder(v);
        }
    }

    public void clear(){
        deviceItems.clear();
        mSelected = null;
        mSelectedItem = -1;
        if (vSelected != null) {
            ((CardView)vSelected).setCardBackgroundColor(Color.WHITE);
        }
        vSelected = null;
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DeviceBluetoothAdapter.ViewHolderItem) {
            ((ViewHolderItem) holder).tvNameItem.setText(deviceItems.get(position).getDeviceName());
            ((ViewHolderItem) holder).tvAddressItem.setText(deviceItems.get(position).getAddress());
            ((CardView)((ViewHolderItem) holder).itemView).setCardBackgroundColor(Color.WHITE);
            ((ViewHolderItem) holder).itemView.setOnClickListener(v -> {
                if (vSelected != null) {
                    ((CardView)vSelected).setCardBackgroundColor(Color.WHITE);
                }
                onChildSelect.onCallbackItemId(position, deviceItems.get(position).getAddress());
                mSelected = deviceItems.get(position);
                vSelected = v;
                ((CardView)vSelected).setCardBackgroundColor(ContextCompat.getColor(v.getContext(),R.color.colorBackgroundBar));
            });
        } else {
            ((EmptyViewHolder) holder).tvEmptyItem.setText("No hay Impresoras");
        }
    }


    @Override
    public int getItemCount() {
        if (deviceItems.isEmpty()) return 1;
        else return deviceItems.size();
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvEmptyItem)
        TextView tvEmptyItem;

        EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public DeviceItem getPrint() {
        return mSelected;
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNameItem)
        TextView tvNameItem;
        @BindView(R.id.tvAddressItem)
        TextView tvAddressItem;
        @BindView(R.id.content_device)
        LinearLayout content;

        ViewHolderItem(View view) {
            super(view);
            ButterKnife.bind(this, view);
            tvAddressItem.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (deviceItems.isEmpty()) {
            return Constants.TYPE_EMPTY;
        } else {
            return Constants.TYPE_ITEM;
        }
    }


}
