package com.gnp.ajustadormovil.android.views.utils.impresora;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.commons.events.AsyncResponse;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.device.ZebraIllegalArgumentException;
import com.zebra.sdk.graphics.ZebraImageFactory;
import com.zebra.sdk.graphics.ZebraImageI;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.SGD;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;

import java.io.File;
import java.io.IOException;

public class PrintImageTask extends AsyncTask<File, Integer, Void> {

    private static final String TAG = PrintImageTask.class.getCanonicalName();

    @SuppressLint("StaticFieldLeak")
    private Activity activity;
    public AsyncResponse delegate = null;
    public static boolean bandImpresion = false;


    private String macAddress;
    private Connection printerConnection;
    private ZebraPrinter printer;

    /**
     * @param activity   Actividad desde la que se llama la tarea
     * @param macAddress Dirección MAC de la impresora bluetooth
     */
    public PrintImageTask(Activity activity, String macAddress) {
        this.activity = activity;
        this.macAddress = macAddress;
    }

    @Override
    protected Void doInBackground(File... files) {
        try {
            printer = connect();
            if (printer != null) printImage(files[0]);
            else disconnect();
        } catch (InterruptedException | ZebraIllegalArgumentException e) {
            Log.e(TAG, "Error al imprimir", e);
        } finally {
            disconnect();
        }
        return null;
    }


    @Override
    protected void onPreExecute() {
        LoadingIndicator.start();
        LoadingIndicator.message(ResourceUtils.getString("loading.message"));
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        LoadingIndicator.finish();
        delegate.processFinish();
    }


    /**
     * Name:        printImage
     * Description: Print image from webView
     */
    private void printImage(File file) throws ZebraIllegalArgumentException {
        try {
            PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();
            if (printerLanguage == PrinterLanguage.ZPL) {

                /*Bitmap image = generateImageFromPdf("/storage/emulated/0/Download/437_comprobante.pdf",
                        0,600,2000);*/
                Bitmap image = generateImageFromPdf(file);
                ZebraPrinter printer = ZebraPrinterFactory
                        .getInstance(PrinterLanguage.ZPL, printerConnection);
                String setLanguage = "\r\n! U1 setvar \"device.languages\" \"zpl\"\r\n";
                String zpl  = "^XA^FO0,0^XGR:IMAGE.GRF^XZ";
                String labelLenghtCommand = "\r\n! U1 setvar \"zpl.label_length\" \"4000\"\r\n";
                printerConnection.write (setLanguage.getBytes());
                printerConnection.write(labelLenghtCommand.getBytes());
                if (image != null){
                    ZebraImageI zImage = ZebraImageFactory.getImage(image);
                    printer.storeImage("R:IMAGE.GRF", zImage, 600, 4000);
                    printerConnection.write (zpl.getBytes());
                }

            }
        } catch (ConnectionException | IOException | NullPointerException  e ) {
            Log.e(TAG, "Error al imprimir", e);
        }
    }


    private ZebraPrinter connect() throws InterruptedException {
        //setStatus("Connecting...", Color.YELLOW);
        printerConnection = null;
        printerConnection = new BluetoothConnection(macAddress);
        Log.d("Printer", "New printer connection");
        try {
            printerConnection.open();
            //setStatus("Connected", Color.GREEN);
            Log.d("Printer", "Connection opened");
        } catch (ConnectionException e) {
            //setStatus("Comm Error! Disconnecting", Color.RED);
            Thread.sleep(1000);
            disconnect();
        }

        ZebraPrinter printer = null;

        if (printerConnection.isConnected()) {
            Log.d("Printer", "Connection stablished");
            try {
                printer = ZebraPrinterFactory.getInstance(PrinterLanguage.ZPL, printerConnection);
                setPrinterLanguageToZPL();
                if (getStatusBeforePrint(printer)) {
                    getStatusAfterPrint(printer);
                }

            } catch (ConnectionException e) {
                printer = null;
                Thread.sleep(1000);
                disconnect();
            }
        }

        return printer;
    }

    private boolean getStatusAfterPrint(ZebraPrinter printer) {
        try {
            PrinterStatus printerStatus = printer.getCurrentStatus();
            while ((printerStatus.numberOfFormatsInReceiveBuffer > 0) && (printerStatus.isReadyToPrint)) {
                printerStatus = this.printer.getCurrentStatus();
            }
            if (printerStatus.isReadyToPrint) {
                Log.d("", "Test Print Sent");
                return true;
            } else if (printerStatus.isHeadOpen) {
                Log.d("", "Printer Head Open");
            } else if (printerStatus.isPaused) {
                Log.d("", "Printer is Paused");
            } else if (printerStatus.isPaperOut) {
                Log.d("", "Printer Media Out");
            }
            return false;
        } catch (Exception e) {
            Log.e(TAG, "Error en conexión", e);
            disconnect();
            return false;
        }
    }

    private void setPrinterLanguageToZPL() {
        try {
            String pl = SGD.GET("device.languages", printerConnection);
            CharSequence zpl = "zpl";
            //showStatus("Printer Language " + pl, Color.BLUE);
            if (!pl.contains(zpl)) {
                SGD.SET("device.languages", "zpl", printerConnection);
                String plo = SGD.GET("device.languages", printerConnection);
                if (plo.contains(zpl))
                    Log.d("", "Printer Language Changed to ZPL");
                else if (plo.length() > 0)
                    Log.d("", "Unable to set Printer Language");
                else
                    Log.d("", "Printer Language not set:" + pl);

            }
        } catch (ConnectionException e) {
            Log.e(TAG, "Error en conexión", e);
            disconnect();
        }
    }

    private boolean getStatusBeforePrint(ZebraPrinter printer) {
        try {
            PrinterStatus printerStatus = printer.getCurrentStatus();
            if (printerStatus.isReadyToPrint) {
                return true;
            } else if (printerStatus.isHeadOpen) {
                Log.d("", "Printer Head Open");
            } else if (printerStatus.isPaused) {
                Log.d("", "Printer is Paused");
            } else if (printerStatus.isPaperOut) {
                Log.d("", "Printer Media Out");
            }
            return false;
        } catch (ConnectionException e) {
            Log.e(TAG, "Error en conexión", e);
            disconnect();
            return false;
        }
    }

    private void disconnect() {
        try {
            ////setStatus("Disconnecting", Color.RED);
            if (printerConnection != null) {
                printerConnection.close();
            }
            ////setStatus("Not Connected", Color.RED);
        } catch (ConnectionException e) {
            //setStatus("COMM Error! Disconnected", Color.RED);
        } finally {
            //enableTestButton(true);
        }
    }

    private Bitmap generateImageFromPdf(File f) {
        PdfiumCore pdfiumCore = new PdfiumCore(activity);
        try {
            ParcelFileDescriptor fd = ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, 0);
            //int width = pdfiumCore.getPageWidthPoint(pdfDocument, 0);
            //int height = pdfiumCore.getPageHeightPoint(pdfDocument, 0);
            Bitmap bmp = Bitmap.createBitmap(600, 4000, Bitmap.Config.ARGB_8888);
            pdfiumCore.renderPageBitmap(pdfDocument, bmp, 0, 0, 0, 600, 4000);
            pdfiumCore.closeDocument(pdfDocument);
            return bmp;
        } catch (Exception e) {
            Log.e(TAG, "Error al convertir PDF a IMG", e);
        }
        return null;
    }

}

