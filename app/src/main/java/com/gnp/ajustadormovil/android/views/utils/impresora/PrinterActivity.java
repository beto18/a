package com.gnp.ajustadormovil.android.views.utils.impresora;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;


public class PrinterActivity extends BaseActivity {

    PrinterFragment printerFragment;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer);
        if (getIntent().getExtras() != null) {
            printerFragment = PrinterFragment.newInstance(getIntent().getExtras());
            addFragment(printerFragment);
        }
    }

    private void addFragment(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contentPrinter, fragment, "Printer");
        transaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (printerFragment != null) {
            printerFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        DialogUtils.createAlert(this, "Salir de impresión",
                "¿Esta seguro de salir del módulo de impresión?",
                "Aceptar", "Cancelar",
                (dialog, which) -> PrinterActivity.super.onBackPressed(),
                (dialog, which) -> dialog.dismiss());
    }

}
