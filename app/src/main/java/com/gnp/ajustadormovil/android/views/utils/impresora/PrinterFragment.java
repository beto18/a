package com.gnp.ajustadormovil.android.views.utils.impresora;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.model.bluetoothdevice.DeviceItem;
import com.gnp.ajustadormovil.android.data.orquestador.AjusteRepository;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.RequestPermissions;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.AsyncResponse;
import com.gnp.ajustadormovil.android.views.commons.events.CallbackItemImage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

import static com.gnp.ajustadormovil.android.models.AppStatus.context;

public class PrinterFragment extends BaseFragment implements OnPageChangeListener, OnLoadCompleteListener,
        OnPageErrorListener, View.OnClickListener, CallbackItemImage<String>, AsyncResponse {

    @BindView(R.id.pdfViewer)
    PDFView pdfViewer;
    @BindView(R.id.btnPrint)
    Button btnPrint;
    @BindView(R.id.btnRetry)
    Button btnRetry;
    Unbinder unbinder;
    private File apkStorage = null;
    private File outputFile = null;
    private String downloadFileName = "";
    private CompositeDisposable compositeDisposable;
    private static final String TAG = "Download Task";
    private BluetoothAdapter bluetoothAdapter;
    private Activity activity;
    private Dialog dialogFragment;
    private RecyclerView rvListDevice;
    private ArrayList<DeviceItem> deviceItem = new ArrayList<>();
    private DeviceBluetoothAdapter adapterBluetoothDevice;
    private File newFile;
    private DeviceItem dItem;
    private View mView;

    public static PrinterFragment newInstance(Bundle bundle) {
        PrinterFragment fragment = new PrinterFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_printer, container, false);
        unbinder = ButterKnife.bind(this, mView);
        activity = getActivity();
        compositeDisposable = new CompositeDisposable();
        btnPrint.setOnClickListener(this);
        dItem = Utils.jsonToObject(DeviceItem.class, AppPreferences.getString(AppPreferences.IMPRESORA_AJUSTE));
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        adapterBluetoothDevice = new DeviceBluetoothAdapter(deviceItem, this);
        if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_STORAGE_PERMISSION)) {
            loadPDF();
        } else {
            RequestPermissions.requestPermission(RequestPermissions.REQUEST_STORAGE_PERMISSION, getActivity());
        }
        return mView;
    }

    private void loadPDF() {
        if (activity.getIntent() != null) {
            String URL_PDF = activity.getIntent().getStringExtra(Constants.NAME_BUNDLE_DATA_PRINTER);
            //String URL_PDF = "https://storage.googleapis.com/sinaut-docs/ajustadormovil/2018/05/0034611517/0034611517_ta1887_recuperacion461_comprobante0034611517C70202812.pdf"; Image hardcore to print
            if (URL_PDF!=null && ! URL_PDF.equals("")) {
                downloadFileName = URL_PDF.substring(URL_PDF.lastIndexOf('/'));//Create file name by picking download file name from URL
                Log.e(TAG, downloadFileName);
                compositeDisposable.add(downloadFile(URL_PDF).compose(RXUtils.applySchedulers())
                        .subscribe(file -> {
                            if (file != null) {
                                pdfViewer.fromFile(file)
                                        .defaultPage(0)
                                        .onPageChange(this)
                                        .enableAnnotationRendering(true)
                                        .onLoad(nbPages -> {
                                            float pageWidth = pdfViewer.getOptimalPageWidth();
                                            float viewWidth = pdfViewer.getWidth();
                                            pdfViewer.zoomTo(viewWidth / pageWidth);
                                            pdfViewer.loadPages();
                                        })
                                        .scrollHandle(new DefaultScrollHandle(getActivity()))
                                        .spacing(10) // in dp
                                        .onPageError(this)
                                        .load();
                                newFile = file;
                            }
                            btnPrint.setVisibility(View.VISIBLE);
                            btnRetry.setVisibility(View.GONE);
                        }, throwable -> {
                            Log.e(TAG,"ErrorDownload", throwable);
                            Log.d(TAG, "Cannot load pdf ");
                            new GeneralSnackBar().largeSnackError(mView,"Ocurrio un error al generar el documeto");
                            btnPrint.setVisibility(View.GONE);
                            btnRetry.setVisibility(View.VISIBLE);


                        }, () -> {
                            Log.d(TAG, "load pdf success");
                            Log.d(TAG, "Load to image on viewer ");
                        }));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadPDF();
        } else {
            RequestPermissions.requestPermission(RequestPermissions.REQUEST_STORAGE_PERMISSION, getActivity());
        }
    }

    public io.reactivex.Observable<File> downloadFile(String downloadUrl) {
        if (downloadUrl.startsWith("http")) {
            return io.reactivex.Observable.create(e -> {
                try {
                    URL url = new URL(downloadUrl);//Create Download URl
                    URLConnection c = url.openConnection();//Open Url Connection
                    c.connect();//connect the URL Connection

                    //Get File if SD card is present
                    if (new CheckForSDCard().isSDCardPresent())
                        apkStorage = new File(Environment.getExternalStorageDirectory() + "/" + "AdjusterPDF");
                    else
                        Toast.makeText(getContext(), "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                    //If File is not present create directory
                    if (!apkStorage.exists()) {
                        apkStorage.mkdir();
                        Log.e(TAG, "Directory Created.");
                    }

                    outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                    //Create New File if not present
                    if (!outputFile.exists()) {
                        outputFile.createNewFile();
                        Log.e(TAG, "File Created");
                    }
                    InputStream is = new BufferedInputStream(url.openStream());
                    OutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
                    //InputStream is = c.getInputStream();//Get InputStream for connection
                    byte[] buffer = new byte[1024];//Set buffer type
                    int len1;//initialize length
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);//Write new file
                    }
                    //Close all connection after doing task
                    fos.flush();
                    fos.close();
                    is.close();
                    e.onNext(outputFile);
                    e.onComplete();
                } catch(Exception ex) {
                    e.tryOnError(ex);
                }
            });
        } else {
            return Observable.just(new File(downloadUrl));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        compositeDisposable.dispose();
    }

    @Override
    public void loadComplete(int nbPages) {
    }

    @Override
    public void onPageError(int page, Throwable t) {
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPrint:
                if (RequestPermissions.checkPermissions(RequestPermissions.REQUEST_LOCATION_PERMISSION)) {
                    if (dItem != null) {
                        printWithExist(dItem.getAddress());
                    } else {
                        getListBluetooth();
                        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                        activity.registerReceiver(bReciever, filter);
                        bluetoothAdapter.startDiscovery();
                    }
                } else
                    Toast.makeText(getActivity(), "Concede los permisos para escanear dispositivos", Toast.LENGTH_SHORT).show();

                break;
        }
    }



    @OnClick(R.id.btnRetry)
    public void generarPDF(){
        loadPDF();
    }

    private final BroadcastReceiver bReciever = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d("DEVICELIST", "Bluetooth device found\n");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.d("Device", "name: " + device.getName() + " address " + device.getAddress());
                if (device.getName() != null) {
                    deviceItem.add(new DeviceItem(device.getName(), device.getAddress()));
                    adapterBluetoothDevice.notifyDataSetChanged();
                }
            }
        }
    };

    private void getListBluetooth() {
        dialogFragment = new Dialog(getActivity(), R.style.CustomThemeDialog);
        dialogFragment.setContentView(R.layout.dialog_list_device);
        rvListDevice = dialogFragment.findViewById(R.id.rvListDeviceBluetooth);
        Button buttonCancel = dialogFragment.findViewById(R.id.btnCancelBluetooth);
        Button buttonOK = dialogFragment.findViewById(R.id.btnOkBluetooth);
        Button buttonUpdate = dialogFragment.findViewById(R.id.btnUpdateBluetooth);
        buttonUpdate.setVisibility(View.VISIBLE);
        final LinearLayoutManager layoutManagerM = new LinearLayoutManager(context);
        layoutManagerM.setOrientation(LinearLayoutManager.VERTICAL);
        rvListDevice.setLayoutManager(layoutManagerM);
        rvListDevice.hasFixedSize();
        rvListDevice.setAdapter(adapterBluetoothDevice);
        dialogFragment.setCancelable(true);
        dialogFragment.show();
        buttonCancel.setOnClickListener(v -> {
            dialogFragment.dismiss();
            deviceItem.clear();
        });
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    deviceItem.clear();
                    adapterBluetoothDevice.notifyDataSetChanged();
                    bluetoothAdapter.startDiscovery();
                    adapterBluetoothDevice.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e(TAG, "Error al conectar la impresora", e);
                }
            }
        });
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapterBluetoothDevice.getPrint() != null) {
                    activity.unregisterReceiver(bReciever);
                    AppPreferences.setString(AppPreferences.IMPRESORA_AJUSTE, Utils.objectoToJsonString(adapterBluetoothDevice.getPrint()));
                    dItem = adapterBluetoothDevice.getPrint();
                    dialogFragment.dismiss();
                    bluetoothAdapter.cancelDiscovery();
                    deviceItem.clear();
                    printWithExist(dItem.getAddress());
                } else {
                    AjusteRepository repository = RepositoryFactory.getAjusteRepository();
                    Ajuste ajuste = repository.findById(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL));
                    ajuste.setImpreso(true);
                    repository.save(ajuste);

                    dialogFragment.dismiss();
                    new GeneralSnackBar().largeSnackError(getActivity(), "Seleccione su impresora por favor");
                }
            }
        });
    }

    @Override
    public void onCallbackItemId(int position, String id) {

    }

    @Override
    public void onCallItemId(int position, String id) {

    }

    private void printWithExist(String id) {
        PrintImageTask printTask = new PrintImageTask(activity, id);
        printTask.delegate = this;
        printTask.execute(newFile);
    }

    @Override
    public void processFinish() {
        new GeneralSnackBar().largeSnackOk(activity, "Impresión completa!");
    }

    @Override
    public void processError() {


    }
}
