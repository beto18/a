package com.gnp.ajustadormovil.android.views.utils.qr;

import android.os.Parcel;
import android.os.Parcelable;

public class QrData implements Parcelable {

    private String titulo;

    private String qr;

    private String description;

    public QrData(){
        titulo = "";
        qr = "";
        description = "";
    }
    public QrData(Parcel in){
        titulo = in.readString();
        qr = in.readString();
        description = in.readString();
    }
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(titulo);
        parcel.writeString(qr);
    }
    public static final Parcelable.Creator<QrData> CREATOR = new Parcelable.Creator<QrData>()
    {
        public QrData createFromParcel(Parcel in)
        {
            return new QrData(in);
        }
        public QrData[] newArray(int size)
        {
            return new QrData[size];
        }
    };
}

