package com.gnp.ajustadormovil.android.views.utils.qr;

public abstract class QrException extends RuntimeException {

    public QrException(){ }

    public QrException(String message) {
        super(message);
    }

    public QrException(String message, Throwable cause) {
        super(message, cause);
    }

    public QrException(Throwable cause) {
        super(cause.getMessage(), cause);
    }

}
