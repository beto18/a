package com.gnp.ajustadormovil.android.views.utils.qr;

public class QrInvalidException extends QrException {

    public QrInvalidException(String value) {
        super("Valor inválido: " + value);
    }

}
