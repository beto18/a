package com.gnp.ajustadormovil.android.views.utils.qr;

import android.graphics.Bitmap;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;

import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.ActivityResultFragment;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class QrUtils {

    private static final String TAG = QrUtils.class.getSimpleName();

    public static Single<String> read() {
        return read(value -> true);
    }

    public static Single<String> read(Predicate<String> isValid) {
        return Single.create(e -> {

            BiConsumer<Runnable, String> readAgain = (reader, value) ->
                    DialogUtils.alert(DialogUtils.DialogType.WARNING,
                            ResourceUtils.getString("title_error_qr"),
                            ResourceUtils.getString("message_error_qr"),
                            new DialogUtils.DialogButton("Sí", reader),
                            new DialogUtils.DialogButton("No", () -> e.tryOnError(new QrInvalidException(value))));

            Runnable reader = new Runnable() {
                @Override
                public void run() {
                    new ActivityResultFragment(fragment -> {
                        IntentIntegrator integrator = IntentIntegrator.forSupportFragment(fragment);
                        integrator.initiateScan();
                    }, (requestCode, resultCode, intent) -> {
                        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                        if (scanResult != null && scanResult.getContents() != null) {
                            Log.d(TAG, "Resultado del scan: " + scanResult.getContents());
                            try {
                                if (isValid.test(scanResult.getContents())) {
                                    e.onSuccess(scanResult.getContents());
                                } else {
                                    readAgain.accept(this, scanResult.getContents());
                                }
                            } catch (Exception ex) {
                                e.tryOnError(ex);
                            }
                        } else {
                            e.tryOnError(new QrNotFoundException());
                        }
                    });
                }
            };

            reader.run();

        });
    }

    public static Observable<String> readMultiple(Predicate<String> isValid) {
        return Observable.create(e -> {

            CompositeDisposable compositeDisposable = new CompositeDisposable();

            Runnable finish = ()->{
                compositeDisposable.dispose();
                e.onComplete();
            };

            Consumer<Runnable> showOtherScan = (reader) ->
                    DialogUtils.alert(DialogUtils.DialogType.SUCCESS,
                            ResourceUtils.getString("title_ok_qr"),
                            ResourceUtils.getString("message_ok_qr"),
                            new DialogUtils.DialogButton("Sí", reader),
                            new DialogUtils.DialogButton("No", finish));

            Consumer<Runnable> showReadAgain = (reader) ->
                    DialogUtils.alert(DialogUtils.DialogType.SUCCESS,
                            ResourceUtils.getString("title_read_qr"),
                            ResourceUtils.getString("message_read_qr"),
                            new DialogUtils.DialogButton("Sí", reader),
                            new DialogUtils.DialogButton("No", finish));

            Runnable reader = new Runnable() {
                @Override
                public void run() {
                    compositeDisposable.add(read(isValid)
                            .subscribe(value -> {
                                e.onNext(value);
                                showOtherScan.accept(this);
                            }, throwable -> {
                                if (throwable instanceof QrInvalidException || throwable instanceof QrNotFoundException) {
                                    showReadAgain.accept(this);
                                    e.tryOnError(throwable);
                                }
                            }));
                }
            };

            reader.run();
        });
    }

    public static Observable<String> readMultiple() {
        return readMultiple(value -> true);
    }

    public static void show(String titulo, String valor) {
        List<QrData> qrDataList = new ArrayList<>();
        QrData qrData = new QrData();
        qrData.setQr(valor);
        qrData.setTitulo(titulo);
        qrDataList.add(qrData);
        showMultiple(qrDataList);
    }

    public static void show(QrData qrData) {
        List<QrData> qrDataList = new ArrayList<>();
        qrDataList.add(qrData);
        showMultiple(qrDataList);
    }

    public static void showMultiple(List<QrData> qrData) {
        FragmentTransaction ft = BaseActivity.getBaseActivity().getSupportFragmentManager().beginTransaction();
        Fragment prev = BaseActivity.getBaseActivity().getSupportFragmentManager().findFragmentByTag(QrViewerDialog.class.getName());
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = QrViewerDialog.newInstance(qrData);
        dialogFragment.show(ft, QrViewerDialog.class.getName());
    }

    public static Single<Bitmap> generateBitmap(String texto, int width, int height) {
        return Single.create(e -> {
            Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
            hints.put(EncodeHintType.MARGIN, 1);
            BitMatrix result = new MultiFormatWriter().encode(texto, BarcodeFormat.QR_CODE, width, height, hints);
            int w = result.getWidth();
            int h = result.getHeight();
            int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, w, h);
            e.onSuccess(bitmap);
        });
    }

    public static Single<String> generateBase64(String texto, int width, int height) {
        return generateBitmap(texto, width, height)
                .map(bitmap -> {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    return (Base64.encodeToString(out.toByteArray(), Base64.DEFAULT));
                });
    }

}
