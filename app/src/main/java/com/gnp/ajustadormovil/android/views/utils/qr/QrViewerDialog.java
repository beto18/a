package com.gnp.ajustadormovil.android.views.utils.qr;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class QrViewerDialog extends BaseDialogFragment {

    private static final String TAG = QrViewerDialog.class.getSimpleName();

    @BindView(R.id.pager)
    ViewPager mImageViewPager;
    @BindView(R.id.tabDots)
    TabLayout tabLayout;
    @BindView(R.id.toolbar_title_modulo_qr)
    TextView textTitulo;

    private List<QrData> qrs;

    private Disposable disposable;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(STYLE_NO_FRAME, R.style.DialogAnimation_Fade);
    }

    public static QrViewerDialog newInstance(List<QrData> qrs) {
        QrViewerDialog instance = new QrViewerDialog();
        instance.qrs = qrs;
        return instance;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_qr_viewpager, container, false);
        ButterKnife.bind(this, v);
        tabLayout.setupWithViewPager(mImageViewPager, true);
        showQrs();
        return v;
    }

    private void showQrs() {
        disposable = Observable.fromIterable(qrs)
                .flatMapSingle(qr -> QrUtils.generateBitmap(qr.getQr(), 1000, 1000 )
                    .map(bitmap -> new QrPage(qr.getTitulo(), bitmap, qr.getDescription())))
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(d -> LoadingIndicator.start("Generando códigos QR"))
                .doFinally(LoadingIndicator::finish)
                .subscribe(pages -> {
                    LoadingIndicator.start("Generando códigos QR");
                    mImageViewPager.addOnAdapterChangeListener((viewPager, oldAdapter, newAdapter) -> LoadingIndicator.finish());
                    QrPagerAdapter pagerAdapter = new QrPagerAdapter(getChildFragmentManager(), pages);
                    mImageViewPager.setAdapter(pagerAdapter);
                    mImageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                            textTitulo.setText(pages.get(position).getTitle());
                        }

                        @Override
                        public void onPageSelected(int position) {
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                }, throwable -> {
                    Log.e(TAG, "Error al generar QRs", throwable);
                    new GeneralSnackBar().largeSnackError(this.getView(), throwable);
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null)
            return;
        ArrayList<Integer> size = BaseActivity.getScreenSize();
        int width = size.get(0);
        int height = size.get(1);
        getDialog().getWindow().setLayout(width - 10, height - 10);
    }

    @OnClick(R.id.imgCerrar)
    public void cerrar(){
        dismiss();
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public static class QrPage {

        private String title;

        private String description;

        private Bitmap qr;

        public QrPage(String title, Bitmap qr) {
            this(title, qr, null);
        }

        public QrPage(String title, Bitmap qr, String description) {
            this.title = title;
            this.qr = qr;
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public Bitmap getQr() {
            return qr;
        }

        public String getDescription() { return description; }

    }

    public static class QrPagerAdapter extends FragmentPagerAdapter {

        private final List<QrPage> qrs;

        private final List<Fragment> fragments;

        public QrPagerAdapter(FragmentManager fragmentManager, List<QrPage> qrs) {
            super(fragmentManager);
            this.qrs = qrs;
            fragments = new ArrayList<>(qrs.size());
            for(QrPage qr : qrs) {
                fragments.add(QrFragment.newInstance(qr.getQr()));
            }
        }

        @Override
        public int getCount() {
            return qrs.size();
        }

        @Override
        public Fragment getItem(int position) {
           return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        public QrPage getPage(int position) {
            return qrs.get(position);
        }

    }

    public static class QrFragment extends BaseFragment {

        private Bitmap qr;

        private String description;

        public static QrFragment newInstance(Bitmap qr) {
            return newInstance(null, qr);
        }

        public static QrFragment newInstance(String description, Bitmap qr) {
            QrFragment fragment = new QrFragment();
            fragment.qr = qr;
            fragment.description = description;
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.activity_show_qr, container, false);
            ImageView img = view.findViewById(R.id.imgQR);
            img.setImageBitmap(qr);
            TextView txt = view.findViewById(R.id.txtDescripcion);
            if(description != null) {
                txt.setVisibility(View.VISIBLE);
                txt.setText(description);
            } else {
                txt.setVisibility(View.GONE);
            }
            return view;
        }

    }

}



