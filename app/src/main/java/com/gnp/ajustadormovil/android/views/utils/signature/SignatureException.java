package com.gnp.ajustadormovil.android.views.utils.signature;

public class SignatureException extends RuntimeException {

    public SignatureException() { }

    public SignatureException(String message) {
        super(message);
    }

    public SignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public SignatureException(Throwable cause) {
        super(cause);
    }

}
