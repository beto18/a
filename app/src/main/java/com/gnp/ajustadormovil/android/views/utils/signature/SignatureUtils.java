package com.gnp.ajustadormovil.android.views.utils.signature;

import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.views.utils.components.ActivityResultFragment;

import io.reactivex.Single;

public class SignatureUtils {

    public static Single<String> firma(){
        return Single.create(e -> {
            new ActivityResultFragment(fragment -> {
                fragment.intentActivity(SignatureActivity.class, Constants.SIGNATURE_REQUEST_CODE);
            }, (requestCode, resultCode, intent) -> {
                if(intent != null && resultCode == Constants.RESULT_CODE.RESULT_OK){
                    e.onSuccess(intent.getStringExtra("RESULTADO"));
                } else {
                    e.tryOnError(new SignatureNotFoundException());
                }
            });
        });
    }
}
