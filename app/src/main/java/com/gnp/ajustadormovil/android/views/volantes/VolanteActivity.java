package com.gnp.ajustadormovil.android.views.volantes;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.ResponsabilidadInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.responsabilidad.Responsabilidad;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.volantes.tercero.VolanteTerceroAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.events.CallBackItemCheck;
import com.gnp.ajustadormovil.android.views.commons.events.ItemClickListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.gnp.ajustadormovil.android.views.volantes.asegurado.VolanteAseguradoFragment;
import com.gnp.ajustadormovil.android.views.volantes.tercero.auto.VolanteTerceroAutoFragment;
import com.gnp.ajustadormovil.android.views.volantes.tercero.noauto.VolanteTerceroNoAutoFragment;
import com.gnp.ajustadormovil.android.views.volantes.tercero.peaton.VolanteTerceroPersonaFragment;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;

import static android.view.View.GONE;

public class VolanteActivity extends BaseActivity implements OnToolbarListener, CallBackItemCheck, ItemClickListener {

    private Toolbar mToolbar;
    private CardView alertAsegurado;
    private Bundle args;
    private TextView responsable;
    private TextView mTitle;
    private Responsabilidad responsabilidad;
    private Ajuste dataRes;
    private boolean isValidC = false;
    private boolean isValidV = false;
    private AseguradoInteractor aseguradoInteractor;
    private ResponsabilidadInteractor responsabilidadInteractor;
    private TerceroInteractor terceroService;
    private String idAjuste;
    private VolanteTerceroAdapter volantesAdapter;
    private RecyclerView recyclerViewTerceros;
    private FrameLayout contentVolanteAsegurado;
    private VolanteAseguradoFragment fragmentAseguradoVolantes;
    private List<Tercero> terceros = new ArrayList<>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volante);
        args = new Bundle();
        args.putString("data", getIntent().getExtras().getString("data"));
        String dataResponse = args.getString("data");
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        dataRes = new Gson().fromJson(dataResponse, Ajuste.class);
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        responsabilidadInteractor = OrquestadorInteractorFactory.getResponsabilidadesService();
        terceroService = OrquestadorInteractorFactory.getTerceroService();
        recyclerViewTerceros = findViewById(R.id.recyclerViewTerceros);
        recyclerViewTerceros.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTerceros.setHasFixedSize(true);
        fragmentAseguradoVolantes = VolanteAseguradoFragment.newInstance(args);
        initComponents();
        setmToolbar(mToolbar);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setTitle("");
    }

    private void initComponents() {
        mToolbar = findViewById(R.id.toolbar_volantes);
        contentVolanteAsegurado = findViewById(R.id.contentVolanteAsegurado);
        responsable = findViewById(R.id.tvResponsabilidad);
        mTitle = findViewById(R.id.toolbar_title);
        alertAsegurado = findViewById(R.id.alertAsegurado);

        getCompositeDisposable().add(getResponsabilidad()
                .subscribe(this::initVolantes,
                        throwable -> {
                            Log.e(TAG, "Error al obtener la responsabilidad", throwable);
                            new GeneralSnackBar().largeSnackError(this, ErrorUtils.getErrorMessage(throwable));
                        }));

    }

    private void initVolantes() {
        if (responsabilidad != null) {
            switch (responsabilidad.getResponsabilidadAjustador()) {
                case "A":
                    getDatosAsegurado();
                    getTerceros();
                    break;
                case "B":
                    loadTercerceros();
                    getResponsable(responsabilidad.getTipoTercero());
                    getDatosAsegurado();
                    break;
                case "C":
                    responsable.setText("Responsabilidad: Corresponsabilidad");
                    getDatosAsegurado();
                    getTerceros();
                    break;
                case "NP":
                    contentVolanteAsegurado.setVisibility(View.GONE);
                    alertAsegurado.setVisibility(View.VISIBLE);
                    loadTercerceros();
                    break;
            }
            if (dataRes != null && dataRes.getSiniestro() != null) {
                if (dataRes.getSiniestro().getTipoSiniestro() != null) {
                    if (dataRes.getSiniestro().getTipoSiniestro().getClave() != null) {
                        if (dataRes.getSiniestro().getTipoSiniestro().getClave().equals("RC")) {
                            contentVolanteAsegurado.setVisibility(View.GONE);
                            alertAsegurado.setVisibility(View.VISIBLE);
                        }
                    }
                }

            }
        }

    }


    private Completable getResponsabilidad() {
        return Completable.create(e -> {
            getCompositeDisposable().add(responsabilidadInteractor.getResponsabilidad(idAjuste)
                    .subscribe(responsabilidad -> {
                        this.responsabilidad = responsabilidad;
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener el conductor asegurado", throwable);
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            new GeneralSnackBar().largeSnackError(this, throwable);
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }
                    })
            );
        });

    }


    private void getDatosAsegurado() {
        getCompositeDisposable().add(Completable.mergeArrayDelayError(
                getConductor(),
                getVehiculo()
        ).subscribe(() -> {
            Log.i(TAG, "Mostrar volante asegurado");
            if (isValidC || isValidV) {
                addFragment(fragmentAseguradoVolantes, R.id.contentVolanteAsegurado, "asegurado");
                contentVolanteAsegurado.setVisibility(View.VISIBLE);
                alertAsegurado.setVisibility(GONE);
            } else {
                contentVolanteAsegurado.setVisibility(View.GONE);
                alertAsegurado.setVisibility(View.VISIBLE);
            }


        }, throwable -> {
            Log.e(TAG, "Error al obtner los datos del asegurado", throwable);
        }));
    }


    private Completable getConductor() {
        return Completable.create(e -> {
            getCompositeDisposable().add(aseguradoInteractor.getConductor(idAjuste)
                    .subscribe(conductor -> {
                        if (conductor != null && conductor.getCondicion() != null) {
                            isValidC = conductor.getCondicion().equals(CondicionPersona.LESIONADO);
                        }
                        if (dataRes.getSiniestro().getTipoSiniestro().getClave().equals("CR")) {
                            isValidC = false;
                        }
                        if (responsabilidad.getAseguradoResponsable()) {
                            responsable.setText("Asegurado Responsable: \n" + conductor.getNombreCompleto());
                        }
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener el conductor asegurado", throwable);
                        isValidC = false;
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            new GeneralSnackBar().largeSnackError(this, throwable);
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }
                    }));
        });


    }

    private Completable getVehiculo() {
        return Completable.create(e -> {
            getCompositeDisposable().add(aseguradoInteractor.getVehiculo(idAjuste)
                    .subscribe(vehiculo -> {
                        if (vehiculo.getDanoConsecuencia() != null && vehiculo.getPerdidaTotalEvidente() != null) {
                            if (vehiculo.getDanoConsecuencia() || vehiculo.getPerdidaTotalEvidente()) {
                                isValidV = true;
                            }
                        }
                        e.onComplete();
                    }, throwable -> {
                        isValidV = false;
                        Log.e(TAG, "Error al obtener el vehículo asegurado", throwable);
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            new GeneralSnackBar().largeSnackError(this, throwable);
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }
                    }));

        });


    }

    private void getResponsable(String responsable) {
        switch (responsable) {
            case "TA":
                this.responsable.setText("Tercero Responsable: Tercero Auto");
                break;
            case "TP":
                this.responsable.setText("Tercero Responsable: Tercero Peaton");
                break;
            case "TNA":
                this.responsable.setText("Tercero Responsable: No Auto");
                break;
            case "TFR":
                this.responsable.setText("Tercero Responsable: Tercero Fuga/Robo");
                break;
            case "ST":
                this.responsable.setText("Tercero Responsable: Sin Tercero");
                break;
        }
    }


    private void loadTercerceros() {
        if (volantesAdapter == null) {
            volantesAdapter = new VolanteTerceroAdapter(this.terceros, this);
            recyclerViewTerceros.setAdapter(volantesAdapter);
        } else {
            volantesAdapter.setItems(terceros);
            volantesAdapter.notifyDataSetChanged();

        }
    }

    private void getTerceros() {
        getCompositeDisposable().add(terceroService.getAll(idAjuste)
                .subscribe(terceros -> {
                    this.terceros = terceros;
                    cleanTerceros(terceros);
                    CollectionsUtils.forEach(terceros, tercero -> Log.d(TAG, "Tercero:" + tercero.getDescripcion()));
                    loadTercerceros();
                }, throwable -> {
                    Log.e(TAG, "Error al obtener los terceros", throwable);
                }));

    }

    @Override
    protected void onResume() {
        super.onResume();
        initVolantes();
    }

    private <T> void showDialog(T tercero) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        DialogFragment dialogo = null;
        Fragment prev;

        if (tercero instanceof TerceroNoAuto) {
            prev = getSupportFragmentManager().findFragmentByTag(VolanteTerceroNoAutoFragment.class.getName());
            if (prev != null){
                transaction.remove(prev);
            }
            dialogo = VolanteTerceroNoAutoFragment.newInstance((TerceroNoAuto) tercero);
            dialogo.show(transaction, VolanteTerceroNoAutoFragment.class.getName());

        } else if (tercero instanceof TerceroPersona) {
            prev = getSupportFragmentManager().findFragmentByTag(VolanteTerceroPersonaFragment.class.getName());
            if (prev != null){
                transaction.remove(prev);
            }
            dialogo = VolanteTerceroPersonaFragment.newInstance((TerceroPersona) tercero);
            dialogo.show(transaction, VolanteTerceroPersonaFragment.class.getName());
        }

        if (dialogo != null) {
            dialogo.setCancelable(false);
        }

    }


    private void addFragment(Fragment fragment) {
        fragment.setHasOptionsMenu(true);
        if (!fragment.isAdded()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.container_fragment, fragment, "auto_tercero");
            transaction.addToBackStack("auto_tercero");
            transaction.commitAllowingStateLoss();
        }

    }

    private void cleanTerceros(List<Tercero> terceros) {

        CollectionsUtils.removeIf(terceros, remove -> {
            boolean removed = false;
            if (remove instanceof TerceroAuto) {
                TerceroAuto tercero = (TerceroAuto) remove;
                removed = (tercero.getVehiculo() == null || !tercero.getVehiculo().getDanoConsecuencia())
                        && (tercero.getConductor() == null || !CondicionPersona.LESIONADO.equals(tercero.getConductor().getCondicion()))
                        && (CollectionsUtils.allMatch(tercero.getOcupantes(), ocupante -> !CondicionPersona.LESIONADO.equals(ocupante.getCondicion())));
            } else if (remove instanceof TerceroNoAuto) {
                removed = (((TerceroNoAuto) remove).getAfectado() == null);

            } else if (remove instanceof TerceroPersona) {
                removed = (((TerceroPersona) remove).getPersona().getCondicion() != CondicionPersona.LESIONADO);
            }
            return removed;
        });

    }


    private void addFragment(Fragment fragment, int id, String tag) {
        fragment.setHasOptionsMenu(true);
        if (!fragment.isAdded()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(id, fragment, tag);
            transaction.commitAllowingStateLoss();
        }

    }


    @Override
    public void onChangeTitle(String title) {
        // mTitle.setText(title);
    }

    @Override
    public void onDefaulTitle() {
        mTitle.setText("Impresión de  Volantes");
    }

    @Override
    public void itemCheck(@Nullable Long id, boolean checked) {

    }

    @Override
    public void listChecks(@NotNull List<Long> list) {

    }

    @Override
    public void onItemClick(int position, View view) {

        Tercero tercero = volantesAdapter.getItem(position);
        if (tercero instanceof TerceroAuto) {
            addFragment(VolanteTerceroAutoFragment.newInstance(tercero.getId()));
        } else if (tercero instanceof TerceroNoAuto) {
            if (((TerceroNoAuto) tercero).getVolante() != null) {
                new GeneralSnackBar().largeSnackError(this, "El volante de admisión del no auto ya fue impreso...");
            } else {
                showDialog(tercero);
            }
        } else if (tercero instanceof TerceroPersona) {
            if (((TerceroPersona) tercero).getPersona().getVolante() != null) {
                new GeneralSnackBar().largeSnackError(this, "El volante médico del peatón ya fue impreso...");
            } else {
                showDialog(tercero);
            }
        }

    }
}
