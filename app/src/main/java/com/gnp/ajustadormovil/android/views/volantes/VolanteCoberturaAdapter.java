package com.gnp.ajustadormovil.android.views.volantes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.views.commons.events.OnCheckItemListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VolanteCoberturaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PolizaCobertura> coberturaList;
    private List<String> listCoberturasAgregadas = new ArrayList<>();
    private int mSelectedItem = -1;
    private OnCheckItemListener onCheckItemListener;

    public VolanteCoberturaAdapter(OnCheckItemListener callback) {
        this.coberturaList = new ArrayList<>();
        this.onCheckItemListener = callback;
    }

    public void addAll(List<PolizaCobertura> results) {
        coberturaList.clear();
        for (PolizaCobertura result : results) {
            add(result);
        }
    }

    public void add(PolizaCobertura r) {
        coberturaList.add(r);
        notifyItemInserted(coberturaList.size() - 1);
    }

    public void remove(PolizaCobertura r) {
        int position = coberturaList.indexOf(r);
        if (position > -1) {
            coberturaList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        final int size = coberturaList.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                coberturaList.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
    }

    public List<String> getListCoberturasAgregadas() {
        return listCoberturasAgregadas;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 2) {
            return new CoberturasHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cobertura_checked, parent, false));

        } else {
            return new EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_items, parent, false));
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CoberturasHolder) {
            ((CoberturasHolder) holder).rbCobertura.setText(coberturaList.get(position).getNombre() != null ? coberturaList.get(position).getNombre() : coberturaList.get(position).getClave());
            ((CoberturasHolder) holder).rbCobertura.setChecked(position == mSelectedItem);
        }
    }

    public PolizaCobertura getCoberturaSelected(int position) {
        return coberturaList.get(position);
    }


    @Override
    public int getItemViewType(int position) {
        if (coberturaList.isEmpty()) return 1;
        else return 2;
    }

    @Override
    public int getItemCount() {
        if (coberturaList.isEmpty()) return 1;
        else return coberturaList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    class CoberturasHolder extends RecyclerView.ViewHolder {

        private RadioButton rbCobertura;

        CoberturasHolder(View view) {
            super(view);
            rbCobertura = itemView.findViewById(R.id.cbCobertura);
            View.OnClickListener clickListener = v -> {
                if (getAdapterPosition() != -1) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);
            rbCobertura.setOnClickListener(clickListener);
            rbCobertura.setOnCheckedChangeListener((buttonView, isChecked) -> {
                listCoberturasAgregadas.clear();
                if (getAdapterPosition() != -1) {
                    listCoberturasAgregadas.add(coberturaList.get(getAdapterPosition()).getClave());
                    onCheckItemListener.onCheck(getAdapterPosition(), view);
                }
            });

        }
    }

    class EmptyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvEmptyItems)
        TextView textEmpty;

        EmptyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(itemView);

        }
    }
}
