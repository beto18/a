package com.gnp.ajustadormovil.android.views.volantes;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.events.ItemVolanteOcupanteListener;

import java.util.List;

public class VolanteOcupanteAdapter extends RecyclerView.Adapter<VolanteOcupanteAdapter.VolantesAseguradoOcupanteHolder>{

    private List<Ocupante> ocupantesList;
    private ItemVolanteOcupanteListener listener;

    public VolanteOcupanteAdapter(List<Ocupante> ocupantesList, ItemVolanteOcupanteListener listener){
        this.ocupantesList = ocupantesList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VolantesAseguradoOcupanteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_volante_ocupante,parent,false);
        return new VolantesAseguradoOcupanteHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull VolantesAseguradoOcupanteHolder holder, int position) {
        holder.nombreOcupante.setText(ocupantesList.get(position).getNombreCompleto());
        if (ocupantesList.get(position).getVolante() != null){
            holder.btnPrint.clearColorFilter();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.btnPrint.setColorFilter(AjustadorMovilApplication.getInstance().getColor(R.color.colorGreen));
            }else {
                holder.btnPrint.setImageResource(R.drawable.ic_imprimir_green);
            }
        }
    }

    public List<Ocupante> getOcupantesList() {
        return ocupantesList;
    }

    @Override
    public int getItemCount() {
        return ocupantesList.size();
    }

    class VolantesAseguradoOcupanteHolder extends RecyclerView.ViewHolder{

        private TextView nombreOcupante;
        private ImageView btnPrint;

        VolantesAseguradoOcupanteHolder(View itemView) {
            super(itemView);
            nombreOcupante = itemView.findViewById(R.id.nomOcupanteAsegurado);
            btnPrint = itemView.findViewById(R.id.btnPrint);


            View.OnClickListener onClick = view -> { listener.onOcupanteClick(getAdapterPosition()); };
            itemView.setOnClickListener(onClick);
            btnPrint.setOnClickListener(onClick);

        }


    }
}
