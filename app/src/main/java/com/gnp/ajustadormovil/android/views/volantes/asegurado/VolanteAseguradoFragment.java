package com.gnp.ajustadormovil.android.views.volantes.asegurado;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.ItemVolanteOcupanteListener;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.gnp.ajustadormovil.android.views.volantes.VolanteActivity;
import com.gnp.ajustadormovil.android.views.volantes.VolanteOcupanteAdapter;
import com.google.gson.Gson;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

import static android.view.View.GONE;

public class VolanteAseguradoFragment extends BaseFragment implements VolanteAseguradoMvpView, ItemVolanteOcupanteListener {

    private View mView;
    private VolanteAseguradoPresenter presenter;
    private CardView printMedico, printAdmision;
    private TextView tvConductor, tvVehiculo;
    private Persona conductor;
    private VehiculoAsegurado vehiculo;
    private RecyclerView rvOcupantes;
    private VolanteOcupanteAdapter adapter;
    private Ajuste data;
    private int position;
    private CompositeDisposable compositeDisposable;
    private OnToolbarListener mListener;
    private ImageView btnPrintVolanteM;
    private ImageView btnPrintAdmision;
    private AppCompatActivity activity;


    public static VolanteAseguradoFragment newInstance(@Nullable Bundle arguments) {
        VolanteAseguradoFragment fragmentAseguradoVolantes = new VolanteAseguradoFragment();
        if (arguments != null) {
            fragmentAseguradoVolantes.setArguments(arguments);
        }
        return fragmentAseguradoVolantes;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (VolanteActivity) context;
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        if (getArguments() != null && getArguments().getString("data") != null) {
            String dataResponse = getArguments().getString("data");
            data = new Gson().fromJson(dataResponse, Ajuste.class);
        }
        presenter = new VolanteAseguradoPresenter(compositeDisposable);
        presenter.onAttach(this);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_volantes_cv, container, false);
        ButterKnife.bind(this, mView);
        mListener.onChangeTitle("Volantes Asegurado");
        initializeView();
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAsegurado();
    }

    private void getAsegurado() {
        presenter.getAsegurado();
        presenter.getVehiculo();
        presenter.getOcupantes();
    }

    private void initializeView() {
        rvOcupantes = mView.findViewById(R.id.rvVolantesMedicos);
        printMedico = mView.findViewById(R.id.optionMedico);
        printAdmision = mView.findViewById(R.id.optionAdmision);
        tvConductor = mView.findViewById(R.id.nomConductor);
        tvVehiculo = mView.findViewById(R.id.nomVehiculo);
        btnPrintVolanteM = mView.findViewById(R.id.btnPrintVolanteM);
        btnPrintAdmision = mView.findViewById(R.id.btnPrintAdmision);



    }


    private void showDialogAdmision() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag(VolanteAseguradoVehiculoFragment.class.getName());
        if (prev != null){
            transaction.remove(prev);
        }
        DialogFragment dialogFragment = VolanteAseguradoVehiculoFragment.newInstance();
        dialogFragment.setCancelable(false);
        dialogFragment.show(transaction, VolanteAseguradoVehiculoFragment.class.getName());

    }

    private void showDialogMedico(Afectado afectado) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag(VolanteAseguradoPersonaFragment.class.getName());
        if (prev != null){
            transaction.remove(prev);
        }
        DialogFragment dialogFragment = VolanteAseguradoPersonaFragment.newInstance(afectado);
        dialogFragment.setCancelable(false);
        dialogFragment.show(transaction, VolanteAseguradoPersonaFragment.class.getName());


    }


    @Override
    public void onError(Throwable throwable) {
        new GeneralSnackBar().largeSnackError(activity, throwable);
    }


    @Override
    public void getAsegurado(Persona conductorAsegurado) {
        conductor = conductorAsegurado;
        tvConductor.setText(conductorAsegurado.getNombreCompleto());
        if (conductor.getVolante() != null) {
            btnPrintVolanteM.setColorFilter(getResources().getColor(R.color.colorGreen));
        }
        if (conductorAsegurado.getCondicion() != null && conductorAsegurado.getCondicion().equals(CondicionPersona.LESIONADO)) {
            printMedico.setVisibility(View.VISIBLE);
        } else {
            printMedico.setVisibility(GONE);
        }
        if (data.getSiniestro().getTipoSiniestro().getClave().equals("CR")) {
            printMedico.setVisibility(GONE);
        }
    }

    @Override
    public void getVehiculo(VehiculoAsegurado vehiculo) {
        tvVehiculo.setText(vehiculo.getNombre());
        this.vehiculo = vehiculo;

        if (this.vehiculo.getVolante() != null) {
            btnPrintAdmision.setColorFilter(getResources().getColor(R.color.colorGreen));
        }


        if (vehiculo.getDanoConsecuencia() != null && vehiculo.getPerdidaTotalEvidente() != null) {
            if (!vehiculo.getDanoConsecuencia() && !vehiculo.getPerdidaTotalEvidente()) {
                printAdmision.setVisibility(GONE);
            } else {
                if (vehiculo.getPerdidaTotalEvidente() != null && vehiculo.getDanoConsecuencia() != null) {
                    if (vehiculo.getDanoConsecuencia()) {
                        printAdmision.setVisibility(View.VISIBLE);
                    } else if (vehiculo.getPerdidaTotalEvidente()) {
                        printAdmision.setVisibility(View.VISIBLE);
                    } else {
                        printAdmision.setVisibility(GONE);
                    }
                } else {
                    printAdmision.setVisibility(GONE);
                }
            }
        } else {
            printAdmision.setVisibility(GONE);
        }
    }


    @Override
    public void getOcupantes(List<Ocupante> ocupantesList) {
        adapter = new VolanteOcupanteAdapter(ocupantesList, this);
        rvOcupantes.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvOcupantes.setAdapter(adapter);
        rvOcupantes.setHasFixedSize(true);
    }


    @Override
    public void onDestroyView() {
        mListener.onDefaulTitle();
        super.onDestroyView();
    }


    @Override
    public void onOcupanteClick(int position) {
        this.position = position;
        if (adapter.getOcupantesList().get(position).getVolante() == null) {
            showDialogMedico(adapter.getOcupantesList().get(position));
        } else {
            new GeneralSnackBar().largeSnackError(getActivity(), "El volante médico del ocupante ya fue impreso...");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getAsegurado();

    }


    @OnClick({R.id.optionMedico, R.id.btnPrintVolanteM})
    public void mostrarDialogoMedico() {
        if (conductor != null && conductor.getVolante() == null) {
            showDialogMedico(conductor);
        } else {
            new GeneralSnackBar().largeSnackError(getActivity(), "El volante médico del conductor ya fue impreso...");
        }
    }


    @OnClick({R.id.optionAdmision, R.id.btnPrintAdmision})
    public void mostrarDialogoAdmision() {
        if (vehiculo != null && vehiculo.getVolante() == null) {
            showDialogAdmision();
        } else {
            new GeneralSnackBar().largeSnackError(getActivity(), "El volante de admisión ya fue impreso...");
        }
    }


}