package com.gnp.ajustadormovil.android.views.volantes.asegurado;

import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

/**
 * Created by winogarcia on 07/03/18.
 */

public interface VolanteAseguradoMvpPresenter<V extends VolanteAseguradoMvpView> extends MvpPresenter<V> {

    void getAsegurado();

    void getVehiculo();

    void getOcupantes();



}
