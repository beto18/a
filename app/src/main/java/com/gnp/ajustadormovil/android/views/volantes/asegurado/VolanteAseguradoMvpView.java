package com.gnp.ajustadormovil.android.views.volantes.asegurado;

import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.views.commons.base.MvpView;

import java.util.List;

/**
 * Created by winogarcia on 07/03/18.
 */

public interface VolanteAseguradoMvpView extends MvpView {
    void onError(Throwable throwable);
    void getAsegurado(Persona conductor);
    void getVehiculo(VehiculoAsegurado aseguradoAutoData);
    void getOcupantes(List<Ocupante> ocupantesList);

}
