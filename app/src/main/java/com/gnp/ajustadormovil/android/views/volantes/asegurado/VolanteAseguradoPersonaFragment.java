package com.gnp.ajustadormovil.android.views.volantes.asegurado;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.volantes.VolanteCoberturaAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

import static android.view.View.GONE;

public class VolanteAseguradoPersonaFragment extends BaseDialogFragment {

    private static String TAG = VolanteAseguradoPersonaFragment.class.getSimpleName();

    private View mView;
    private ArrayList<String> listCoberturasAgregadas = new ArrayList<>();
    private List<Cobertura> coberturasList = new ArrayList<>();
    private List<CatalogoId> noDeducibles = new ArrayList<>();
    private Spinner spinnerNoDeducible;
    private CheckBox aplicaDeducible;
    private List<CatalogoId> causasCondicionamiento = new ArrayList<>();
    private VolanteCoberturaAdapter coberturaAdapter;
    private CompositeDisposable compositeDisposable;
    private SiniestrosInteractor siniestrosInteractor;
    private AppCompatActivity activity;
    private VolanteInteractor volanteService;
    private String idAjuste;
    private List<PolizaCobertura> coberturas;
    private RecyclerView rvCoberturasDisponibles;
    private Afectado afectado;
    private int categoria;
    private Ajuste ajuste;
    private AjusteInteractor ajusteInteractor;
    private LinearLayout linearLayout;

    public static VolanteAseguradoPersonaFragment newInstance(Afectado afectado) {
        VolanteAseguradoPersonaFragment instance = new VolanteAseguradoPersonaFragment();
        instance.afectado = afectado;
        return instance;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialog);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        categoria = afectado instanceof Ocupante ? 13 : 12;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.content_dialog_volante_medico, container, false);
        ButterKnife.bind(this, mView);
        initializeView();
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAjuste();
    }


    private void initializeView() {

        rvCoberturasDisponibles = mView.findViewById(R.id.rvCoberturasDisponibles);
        spinnerNoDeducible = mView.findViewById(R.id.sp_no_deducible);
        aplicaDeducible = mView.findViewById(R.id.deducible);
        rvCoberturasDisponibles.setLayoutManager(new LinearLayoutManager(activity));
        rvCoberturasDisponibles.setHasFixedSize(true);
        linearLayout = mView.findViewById(R.id.linearLayout);
    }


    private Completable descargaCatalogos() {
        return Completable.mergeArrayDelayError(
                descargaNoDeducible(),
                descargaCoberturasVolante(categoria));
    }


    private Completable descargaNoDeducible() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getRazonesNoDeducible()
                    .subscribe(noDeducibles -> {
                        CatalogoId noDeducible = new CatalogoId();
                        noDeducible.setId(0L);
                        noDeducible.setNombre(ResourceUtils.getString(R.string.message_no_deducible_option));
                        this.noDeducibles = noDeducibles;
                        this.noDeducibles.add(0, noDeducible);
                        e.onComplete();

                    }, throwable -> {
                        Log.e(TAG, "Ocurrio un error al descargar los no deducibles", throwable);
                        e.tryOnError(throwable);
                    }));
        });
    }


    private Completable descargaCoberturasVolante(int categoria) {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getCoberturasVolante(categoria, ajuste.getSiniestro().getTipoSiniestro().getClave())
                    .subscribe(coberturas -> {
                        coberturasList = coberturas;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Ocurrio un error al descargar los no deducibles", throwable);
                    }));
        });


    }


    private void getAjuste() {
        compositeDisposable.add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(ajuste -> {
                    this.ajuste = ajuste;
                    initDialog();
                }, throwable -> {
                    Log.e(TAG, "Ocurrio un error al obtener al ajuste", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        onError(ErrorUtils.getErrorMessage(throwable));
                    }
                }));
    }

    private void initDialog() {
        compositeDisposable.add(descargaCatalogos()
                .doOnSubscribe(it -> LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .subscribe(() -> {
                    mostrarDialogoMedico();
                    setAdapters();
                    setEvents();
                }, throwable -> {
                    Log.e(TAG, "Ocurrio un error al descargar los catalogos", throwable);
                    onError("Ocurrio un error al descargar los catálogos ");

                }));

    }

    private void mostrarDialogoMedico() {


        coberturas = getPolizaCoberturas(coberturasList);

        mView.findViewById(R.id.armadora).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.sp_no_deducible).setVisibility(View.VISIBLE);
        coberturaAdapter = new VolanteCoberturaAdapter((position, view) -> {
        });
        listCoberturasAgregadas.clear();
        coberturaAdapter.addAll(coberturas);
        rvCoberturasDisponibles.setAdapter(coberturaAdapter);
        Log.d(TAG, "imprimir: " + coberturas);
        setEvents();
    }

    private void setEvents() {
        aplicaDeducible.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mView.findViewById(R.id.armadora).setVisibility(View.GONE);
                mView.findViewById(R.id.view).setVisibility(View.GONE);
                spinnerNoDeducible.setVisibility(View.GONE);
            } else {
                mView.findViewById(R.id.armadora).setVisibility(View.VISIBLE);
                spinnerNoDeducible.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setAdapters() {
        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, noDeducibles, CatalogoId::getNombre);
        spinnerNoDeducible.setAdapter(adapter);
    }

    @NotNull
    private List<PolizaCobertura> getPolizaCoberturas(List<Cobertura> listCoberturas) {
        List<PolizaCobertura> coberturas = new ArrayList<>();
        if (ajuste.getSiniestro().getPoliza() != null) {
            if (ajuste.getSiniestro().getPoliza().getCoberturas() != null) {
                for (int i = 0; i < ajuste.getSiniestro().getPoliza().getCoberturas().size(); i++) {
                    for (int j = 0; j < listCoberturas.size(); j++) {
                        if (ajuste.getSiniestro().getPoliza().getCoberturas().get(i).getClave().equals(listCoberturas.get(j).getClave())) {
                            coberturas.add(ajuste.getSiniestro().getPoliza().getCoberturas().get(i));
                        }
                    }

                }
            } else {
                mView.findViewById(R.id.coberturas_list_content).setVisibility(GONE);
            }
        } else {
            mView.findViewById(R.id.coberturas_list_content).setVisibility(GONE);
        }
        return coberturas;
    }


    @OnClick(R.id.btnAceptar)
    public void generarVolanteMedico() {
        VolanteMedico volante = new VolanteMedico();
        volante.setAplicaDeducible(aplicaDeducible.isChecked());
        volante.setEnvioEmail(true);
        volante.setImpresionFisica(true);
        if (coberturaAdapter != null) {
            volante.setCoberturas(coberturaAdapter.getListCoberturasAgregadas());
        }
        if (!aplicaDeducible.isChecked()) {
            if (noDeducibles.get(spinnerNoDeducible.getSelectedItemPosition()).getId() == 0) {
                onError("Seleccione una razón de no deducible por favor");
                return;
            } else {
                volante.setRazonNoDeducible(noDeducibles.get(spinnerNoDeducible.getSelectedItemPosition()).getId());
            }

        } else {
            volante.setRazonNoDeducible(null);
        }
        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
            sendVolanteMedico(afectado, volante);
        } else {
            if (coberturaAdapter.getListCoberturasAgregadas().size() == 0) {
                onError("No puede imprimir el volante médico si no tiene coberturas seleccionadas o no hay coberturas para el volante");
            } else {

                sendVolanteMedico(afectado, volante);
            }
        }


    }

    private void sendVolanteMedico(Afectado afectado, VolanteMedico pasajero) {
        if (afectado instanceof Persona) {
            volanteMedicoConductor(pasajero);
        } else {
            volanteMedicoOcupante((Ocupante) afectado, pasajero);
        }
    }


    private void volanteMedicoConductor(VolanteMedico pasajero) {
        compositeDisposable.add(volanteService.generateAseguradoConductor(idAjuste, pasajero)
                .subscribe(pdfLink -> {
                    printDocument(pdfLink.getUrl());
                }, throwable -> {
                    Log.e(TAG, "Error al generar volante:", throwable);
                    onError(ErrorUtils.getErrorMessage(throwable));
                }));
    }

    private void volanteMedicoOcupante(Ocupante ocupante, VolanteMedico pasajero) {
        compositeDisposable.add(volanteService.generateAseguradoOcupante(idAjuste, ocupante.getId(), pasajero)
                .subscribe(pdfLink -> {
                    printDocument(pdfLink.getUrl());
                }, throwable -> {
                    Log.e(TAG, "Error al generar volante:", throwable);
                    onError(ErrorUtils.getErrorMessage(throwable));

                }));
    }


    private void printDocument(String url) {
        Intent pdfViewer = new Intent(activity, PrinterActivity.class);
        pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, url);
        dialogDismiss();
        startActivity(pdfViewer);
    }


    private void onError(String message) {
        new GeneralSnackBar().updownMakeSnackColorError(mView, linearLayout, message);
    }



    @OnClick(R.id.btnCancel)
    public void dialogDismiss() {
        getDialog().dismiss();
    }
}
