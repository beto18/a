package com.gnp.ajustadormovil.android.views.volantes.asegurado;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by winogarcia on 07/03/18.
 */

public class VolanteAseguradoPresenter<V extends VolanteAseguradoMvpView> extends BasePresenter<V> implements VolanteAseguradoMvpPresenter<V> {

    private AseguradoInteractor aseguradoInteractor;
    private CompositeDisposable compositeDisposable;
    private String idAjuste;

    VolanteAseguradoPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        this.compositeDisposable = compositeDisposable;
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        idAjuste = (AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL));
    }

    @Override
    public void getAsegurado() {
        compositeDisposable.add(aseguradoInteractor.getConductor(idAjuste)
                .subscribe(conductor -> {
                    getMvpView().getAsegurado(conductor);
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el conductor asegurado", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        getMvpView().onError(throwable);
                    }
                })
        );
    }

    @Override
    public void getVehiculo() {
        compositeDisposable.add(aseguradoInteractor.getVehiculo(idAjuste)
                .subscribe(vehiculo -> {
                    getMvpView().getVehiculo(vehiculo);
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el vehiculo asegurado", throwable);
                    getMvpView().onError(throwable);
                })
        );
    }


    @Override
    public void getOcupantes() {
        compositeDisposable.add(aseguradoInteractor.getOcupantes(idAjuste)
                .subscribe(ocupantes -> {

                    CollectionsUtils.removeIf(ocupantes, ocupante -> !ocupante.getCondicion().equals(CondicionPersona.LESIONADO));
                    getMvpView().getOcupantes(ocupantes);

                }, throwable -> {
                    Log.e(TAG, "Error al obtener el ocupnates asegurado", throwable);
                    getMvpView().onError(throwable);

                })
        );
    }


}
