package com.gnp.ajustadormovil.android.views.volantes.asegurado;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AseguradoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CentroRemate;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VehiculoAsegurado;
import com.gnp.ajustadormovil.android.models.orquestador.asegurado.VolanteVehiculoAsegurado;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrNotFoundException;
import com.gnp.ajustadormovil.android.views.utils.qr.QrUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.volantes.VolanteCoberturaAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;
import com.gnp.ajustadormovil.android.views.commons.piezas.GeneralDamageFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

import static android.view.View.GONE;

public class VolanteAseguradoVehiculoFragment extends BaseDialogFragment {
    public static final String TAG = VolanteAseguradoVehiculoFragment.class.getSimpleName();

    private List<Cobertura> coberturasVehiculoList = new ArrayList<>();
    private List<CatalogoId> noDeducibles = new ArrayList<>();
    private List<String> nombreNoDeducibles = new ArrayList<>();
    private List<Long> listCausasCondicionamiento = new ArrayList<>();
    private List<CatalogoId> causasCondicionamiento = new ArrayList<>();
    private List<CentroRemate> centroDeRemates = new ArrayList<>();
    private List<String> nombreCentroRemates = new ArrayList<>();
    private List<PolizaCobertura> coberturas;
    private Spinner spinnerNoDeducible;
    private CheckBox aplicaDeducible;
    private EditText folioSegmentacion;
    private CheckBox segmentado;
    private CheckBox volanteCondicionado;
    private CheckBox requiereGrua;
    private Spinner spCondicionamiento;
    private Spinner spinnerRemates;
    private CheckBox siniestroVerifica;
    private CheckBox cobranzaPoliza;
    private CheckBox declaracionConductor;
    private CheckBox numeroSerie;
    private CheckBox agravamiento;
    private CheckBox correccionPoliza;
    private CheckBox pendienteInicioActa;
    private CheckBox pendienteMP;
    private CheckBox tomadorDatos;
    private CheckBox licencia;
    private CheckBox otroMotivo;
    private LinearLayout linearLayout;
    private AppCompatActivity activity;
    private TextView link_segmento;
    private ConstraintLayout coberturas_container;
    private RecyclerView rvCoberturasDisponibles;
    private CheckBox cbDeducible;
    private Button solicitarGrua;
    private AppCompatTextView tvRemate;

    @BindView(R.id.edtCodigoTaller)
    EditText edtCodigoTaller;


    private VolanteCoberturaAdapter coberturaAdapterV;
    private Ajuste ajuste;
    private CompositeDisposable compositeDisposable;
    private SiniestrosInteractor siniestrosInteractor;
    private AjusteInteractor ajusteInteractor;
    private AseguradoInteractor aseguradoInteractor;
    private VolanteInteractor volanteService;
    private GeneralDamageFragment generalDamageToolsParts;
    private View view;
    private VehiculoAsegurado vehiculo;
    private String idAjuste;




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialog);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        aseguradoInteractor = OrquestadorInteractorFactory.getAseguradoInteractor();
        addFragmentPartesFaltantes();


    }

    public static VolanteAseguradoVehiculoFragment newInstance() {
        return new VolanteAseguradoVehiculoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_dialog_volante_admision_asegurado, container, false);
        ButterKnife.bind(this,view);
        initializeView();
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        compositeDisposable.add(getAjuste()
                .andThen(getVehiculo())
                .subscribe(this::initializeCatalogos,
                        throwable -> {
                            if (!(throwable instanceof ResourceNotFoundException)){
                                new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout,ErrorUtils.getErrorMessage(throwable));
                            }
                        })
        );
    }

    private void initializeCatalogos() {
        compositeDisposable.add(descargaCatalogos()
                .subscribe(() -> {
                    showDialogAdmision();
                    setEvents();
                }, throwable -> {
                    Log.e(TAG, "Error al descargar los catalogos", throwable);
                    new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "Error al descargar los catalogos");
                }));
    }


    private  Completable getAjuste(){
        return Completable.create(e -> {
            compositeDisposable.add(ajusteInteractor.getAjuste(idAjuste)
                    .subscribe(ajuste -> {
                        this.ajuste = ajuste;
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener el ajuste", throwable);
                        e.tryOnError(throwable);

                    }));
        });


    }

    private Completable getVehiculo(){
        return Completable.create(e -> {
            compositeDisposable.add(aseguradoInteractor.getVehiculo(idAjuste)
                    .subscribe(vehiculo -> {
                        this.vehiculo = vehiculo;
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener el vehiculo asegurado", throwable);
                        e.tryOnError(throwable);
                    }));
        });

    }


    private Completable descargaCatalogos() {
        return Completable.mergeArrayDelayError(
                descargaCatalogoNoDeducible(),
                descargaCoberturasVolanteVehiculo(),
                descargaCentroDeRemates(),
                descargaCausasCondicionamiento()

        );

    }

    private void initializeView() {
        view.findViewById(R.id.piezas_container).setVisibility(GONE);
        spCondicionamiento = view.findViewById(R.id.spCondicionamieto);
        siniestroVerifica = view.findViewById(R.id.siniestro_verificacion);
        cobranzaPoliza = view.findViewById(R.id.cobranza_poliza);
        declaracionConductor = view.findViewById(R.id.declaracion_conductor);
        numeroSerie = view.findViewById(R.id.numero_serie);
        agravamiento = view.findViewById(R.id.agravamiento_riesgo);
        correccionPoliza = view.findViewById(R.id.correcion_poliza);
        pendienteInicioActa = view.findViewById(R.id.pendiente_inicio_acta);
        pendienteMP = view.findViewById(R.id.pendiente);
        tomadorDatos = view.findViewById(R.id.tomador_datos);
        licencia = view.findViewById(R.id.licencia);
        otroMotivo = view.findViewById(R.id.otro);
        link_segmento = view.findViewById(R.id.link_segmento);
        coberturas_container = view.findViewById(R.id.container);
        rvCoberturasDisponibles = view.findViewById(R.id.rvCoberturasDisponiblesV);
        folioSegmentacion = view.findViewById(R.id.et_folio_segmetacion);
        cbDeducible = view.findViewById(R.id.cbDeducibleAd);
        solicitarGrua = view.findViewById(R.id.btnGrua);
        requiereGrua = view.findViewById(R.id.requiere_grua);
        tvRemate = view.findViewById(R.id.tvAddressRemate);
        volanteCondicionado = view.findViewById(R.id.volante_condicionado);
        aplicaDeducible = view.findViewById(R.id.deducible);
        spinnerNoDeducible = view.findViewById(R.id.spinner_razon);
        spinnerRemates = view.findViewById(R.id.spCentroRemate);
        segmentado = view.findViewById(R.id.segmentado);
        linearLayout = view.findViewById(R.id.linearLayout);

    }


    @OnClick(R.id.btnAcceptAd)
    public void validateDataAdmision() {
        VolanteVehiculoAsegurado volante = new VolanteVehiculoAsegurado();
        volante.setAplicaDeducible(aplicaDeducible.isChecked());
        volante.setCoberturas(coberturaAdapterV.getListCoberturasAgregadas());
        volante.setEnvioEmail(true);
        volante.setImpresionFisica(true);
        volante.setRequiereGrua(requiereGrua.isChecked());
        volante.setVolanteCondicionado(volanteCondicionado.isChecked());
        if (!edtCodigoTaller.getText().toString().trim().isEmpty() ){
            volante.setCodigoTaller(edtCodigoTaller.getText().toString());
        }
        if (requiereGrua.isChecked()) {
            volante.setRequiereGrua(true);
            volante.setPiezasFaltantes(generalDamageToolsParts.getDataList());
        }

        VolanteVehiculoAsegurado response = new VolanteVehiculoAsegurado();
        response.setAplicaDeducible(volante.getAplicaDeducible());
        if (vehiculo.getPerdidaTotalEvidente()) {
            if (centroDeRemates.get(spinnerRemates.getSelectedItemPosition()).getId() == 0) {
                volante.setCentroRemate(4L);
            } else {
                volante.setCentroRemate(centroDeRemates.get(spinnerRemates.getSelectedItemPosition()).getId());
            }
        }
        if (volanteCondicionado.isChecked()) {
            if (causasCondicionamiento.get(spCondicionamiento.getSelectedItemPosition()).getId() == 0L) {
                new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "Seleccione una causa de condicionamiento correcta");
            }
            volante.setCausaCondicionamiento(causasCondicionamiento.get(spCondicionamiento.getSelectedItemPosition()).getId());
        } else {
            volante.setCausaCondicionamiento(null);
        }
        if (aplicaDeducible.isChecked()) {
            volante.setAplicaDeducible(true);
            volante.setRazonNoDeducible(null);
        } else {
            if (noDeducibles.get(spinnerNoDeducible.getSelectedItemPosition()).getId() == 0) {
                new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "Seleccione una razón de no deducible por favor");
                return;
            } else {
                volante.setRazonNoDeducible(noDeducibles.get(spinnerNoDeducible.getSelectedItemPosition()).getId());
            }
        }
        if (segmentado.isChecked()) {
            if (folioSegmentacion.getText().length() == 0) {
                folioSegmentacion.setError("Este campo es necesario");
                return;
            } else {
                volante.setFolioSegmentacion(folioSegmentacion.getText().toString());
            }
        }
        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
            vehiculo.setVolante(response);
            sendVolanteVehiculoAsegurado(volante);
            listCausasCondicionamiento.clear();
        } else {
            if (coberturaAdapterV.getListCoberturasAgregadas().size() == 0) {
                new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "No puede imprimir el volante de admisión si no tiene coberturas seleccionadas o no hay coberturas para el volante");
            } else {
                vehiculo.setVolante(response);
                sendVolanteVehiculoAsegurado(volante);
                listCausasCondicionamiento.clear();
            }
        }

    }

    private void addFragmentPartesFaltantes() {
        generalDamageToolsParts = new GeneralDamageFragment();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(R.id.piezas_container, generalDamageToolsParts, Constants.TAG_PARTE_FALTANTE);
        ft.commit();
    }


    private void showDialogAdmision() {
        AdapterSpinnerGeneral<CatalogoId> adapterCondicionamiento = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, causasCondicionamiento, CatalogoId::getNombre);
        spCondicionamiento.setAdapter(adapterCondicionamiento);
        link_segmento.setVisibility(GONE);
        coberturas_container.setVisibility(GONE);
        coberturaAdapterV = new VolanteCoberturaAdapter(((position1, view) -> {
        }));
        rvCoberturasDisponibles.setLayoutManager(new LinearLayoutManager(activity));
        rvCoberturasDisponibles.setAdapter(coberturaAdapterV);
        rvCoberturasDisponibles.setHasFixedSize(true);
        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, noDeducibles, CatalogoId::getNombre);
        spinnerNoDeducible.setAdapter(adapter);
        view.findViewById(R.id.spinner_razon).setVisibility(View.VISIBLE);
        view.findViewById(R.id.et_deducible).setVisibility(View.GONE);
        view.findViewById(R.id.tilDeducible).setVisibility(GONE);
        setCoberturas();
        if (vehiculo.getPerdidaTotalEvidente()) {
            view.findViewById(R.id.segmentado).setVisibility(GONE);
            view.findViewById(R.id.link_segmento).setVisibility(GONE);
            view.findViewById(R.id.et_folio_segmetacion).setVisibility(GONE);
            view.findViewById(R.id.remate_container).setVisibility(View.VISIBLE);
            AdapterSpinnerGeneral<CentroRemate> adapterC = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, centroDeRemates, CentroRemate::getNombre);
            spinnerRemates.setAdapter(adapterC);
        }
    }

    private void setCoberturas() {
        coberturas = new ArrayList<>();
        if (ajuste.getSiniestro() != null && ajuste.getSiniestro().getPoliza() != null) {
            if (ajuste.getSiniestro().getPoliza().getCoberturas() != null) {
                for (int i = 0; i < ajuste.getSiniestro().getPoliza().getCoberturas().size(); i++) {
                    for (int j = 0; j < coberturasVehiculoList.size(); j++) {
                        if (ajuste.getSiniestro().getPoliza().getCoberturas().get(i).getClave().equals(coberturasVehiculoList.get(j).getClave())) {
                            coberturas.add(ajuste.getSiniestro().getPoliza().getCoberturas().get(i));
                        }
                    }

                }
            } else {
                view.findViewById(R.id.title_vehiculo).setVisibility(GONE);
                view.findViewById(R.id.rvCoberturasDisponiblesV).setVisibility(GONE);
            }
        } else {
            view.findViewById(R.id.title_vehiculo).setVisibility(GONE);
            view.findViewById(R.id.rvCoberturasDisponiblesV).setVisibility(GONE);
            view.findViewById(R.id.edtDeducible).setVisibility(View.VISIBLE);
        }
        coberturaAdapterV.addAll(coberturas);
    }

    private void eventCheckedSegmentacion() {
        segmentado.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.link_segmento).setVisibility(View.VISIBLE);
                view.findViewById(R.id.til_folio_seg).setVisibility(View.VISIBLE);
                view.findViewById(R.id.et_folio_segmetacion).setVisibility(View.VISIBLE);
                Utils.showKeyboard(folioSegmentacion, activity);
            } else {
                view.findViewById(R.id.link_segmento).setVisibility(View.GONE);
                view.findViewById(R.id.et_folio_segmetacion).setVisibility(View.GONE);
                view.findViewById(R.id.til_folio_seg).setVisibility(GONE);
                Utils.hideKeyboard(folioSegmentacion, activity);
            }
        });
    }

    private void eventCheckedDeducible() {
        aplicaDeducible.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.spinner_razon).setVisibility(View.GONE);
                view.findViewById(R.id.deducible_container).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.spinner_razon).setVisibility(View.VISIBLE);
                view.findViewById(R.id.deducible_container).setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setEvents() {
        eventSolicitud(link_segmento, BuildConfig.SEGMENTADOR);
        eventSolicitud(solicitarGrua, "https://docs.google.com/forms/d/e/1FAIpQLSf_QtXEfiXJOT9vUs3X1WCsdmcNHVndv5_rk82rHIcPVWTU1w/viewform?c=0&w=1");
        eventCheckedCondicionamiento();
        eventRequiereGrua();
        eventVolanteCondicionado();
        eventRemates();
        eventDeducible();
        eventCheckedDeducible();
        eventCheckedSegmentacion();
        edtCodigoTaller.setOnTouchListener((view, event) -> {
            final int DRAWABLE_RIGHT = 2;
            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (edtCodigoTaller.getRight() - edtCodigoTaller.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    edtCodigoTaller.getText().clear();
                    return true;
                }
            }
            return false;
        });



    }

    private void eventVolanteCondicionado() {
        volanteCondicionado.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.condicionamiento_container).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.condicionamiento_container).setVisibility(View.GONE);
            }
        });
    }

    private void eventRemates() {
        spinnerRemates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (centroDeRemates != null) {
                    tvRemate.setText(centroDeRemates.get(position).getDireccion());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void eventRequiereGrua() {
        requiereGrua.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.btnParteFaltante).setVisibility(GONE);
                view.findViewById(R.id.recycler_parte_faltante).setVisibility(GONE);
                view.findViewById(R.id.btnGrua).setVisibility(View.VISIBLE);
                view.findViewById(R.id.piezas_container).setVisibility(View.VISIBLE);

            } else {
                view.findViewById(R.id.piezas_faltantes_title).setVisibility(View.GONE);
                view.findViewById(R.id.btnParteFaltante).setVisibility(View.GONE);
                view.findViewById(R.id.recycler_parte_faltante).setVisibility(View.GONE);
                view.findViewById(R.id.btnGrua).setVisibility(View.GONE);
                view.findViewById(R.id.piezas_container).setVisibility(GONE);

            }
        });
    }

    private void eventCheckedCondicionamiento() {
        siniestroVerifica.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(1L);
            else
                removeFromCausasCondicionamiento(1);
        });
        cobranzaPoliza.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(2L);
            else
                removeFromCausasCondicionamiento(2);
        });
        declaracionConductor.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(3L);
            else
                removeFromCausasCondicionamiento(3);
        });
        numeroSerie.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(4L);
            else
                removeFromCausasCondicionamiento(4);
        });
        agravamiento.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(5L);
            else
                removeFromCausasCondicionamiento(5);
        });
        correccionPoliza.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(6L);
            else
                removeFromCausasCondicionamiento(6);
        });
        pendienteInicioActa.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(7L);
            else
                removeFromCausasCondicionamiento(7);
        });
        pendienteMP.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                listCausasCondicionamiento.add(8L);
            } else {
                removeFromCausasCondicionamiento(8);
            }
        });
        tomadorDatos.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(9L);
            else
                removeFromCausasCondicionamiento(9);
        });
        licencia.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(10L);
            else
                removeFromCausasCondicionamiento(10);
        });
        otroMotivo.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(11L);
            else
                removeFromCausasCondicionamiento(11);
        });
    }

    private void eventSolicitud(TextView textView, String s) {
        textView.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(s));
            startActivity(i);
        });
    }

    private void eventDeducible() {
        cbDeducible.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                compositeDisposable.add(ajusteInteractor.addDeducibleAdministrativo(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                        .subscribe(() -> {
                            Log.d(TAG, "onResponse: succes");
                        }, throwable -> {
                        }));
            } else {
                compositeDisposable.add(ajusteInteractor.deleteDeducibleAdministrativo(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL))
                        .subscribe(() -> {
                            Log.d(TAG, "onResponse: succes");
                        }, throwable -> {
                        }));
            }
        });
    }


    private void removeFromCausasCondicionamiento(Integer i) {
        listCausasCondicionamiento.remove(i);
    }


    private Completable descargaCoberturasVolanteVehiculo() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getCoberturasVolante(3, ajuste.getSiniestro().getTipoSiniestro().getClave())
                    .subscribe(coberturas -> {
                        coberturasVehiculoList = coberturas;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "Error al descargar volante vehiculo", throwable);
                    }));
        });


    }

    private void sendVolanteVehiculoAsegurado(VolanteVehiculoAsegurado volanteVehiculo) {
        VolanteVehiculoAsegurado response = new VolanteVehiculoAsegurado();
        response.setAplicaDeducible(volanteVehiculo.getAplicaDeducible());
        vehiculo.setVolante(response);
        compositeDisposable.add(volanteService.generateAseguradoVehiculo(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL), volanteVehiculo)
                .subscribe(pdfLink -> {
                    if (pdfLink != null) {
                        imprimirPdf(pdfLink.getUrl());
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al generar volante:", throwable);
                    onFailure(throwable);
                    vehiculo.setVolante(null);
                }));


    }


    private void setNoDeducibles(List<CatalogoId> noDeduciblesList) {
        CatalogoId noDeducible = new CatalogoId();
        noDeducible.setId(0L);
        noDeducible.setNombre(ResourceUtils.getString(R.string.message_no_deducible_option));
        noDeducibles = noDeduciblesList;
        noDeducibles.add(0, noDeducible);
        nombreNoDeducibles = new ArrayList<>();
        for (CatalogoId nd : noDeduciblesList) {
            nombreNoDeducibles.add(nd.getNombre());
        }
    }


    private Completable descargaCatalogoNoDeducible() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getRazonesNoDeducible()
                    .subscribe(deducibles -> {
                        setNoDeducibles(deducibles);
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al descargar no deducibles", throwable);
                        e.tryOnError(throwable);
                    }));
        });


    }

    private Completable descargaCausasCondicionamiento() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getCondicionamientosPoliza()
                    .subscribe(condicionamientos -> {
                        causasCondicionamiento = condicionamientos;
                        causasCondicionamiento.add(0, new CatalogoId(0L, "Seleccione una causa de condicionamiento por favor"));
                        Log.d(TAG, "descargaCausasCondicionamiento: " + condicionamientos.size());
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "descargaCausasCondicionamiento: ", throwable);
                    }));
        });

    }

    private Completable descargaCentroDeRemates() {
        CentroRemate remate = new CentroRemate();
        remate.setId(0L);
        remate.setNombre("Seleccione un centro de remate");
        remate.setDireccion("");

        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getCentrosRemate()
                    .subscribe(centroRemates -> {
                        centroRemates.add(0, remate);
                        centroDeRemates.addAll(centroRemates);
                        for (int i = 0; i < centroDeRemates.size(); i++) {
                            nombreCentroRemates.add(centroRemates.get(i).getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtener los centros de remate");
                        e.tryOnError(throwable);
                    }));
        });

    }


    private void imprimirPdf(String url) {
        Intent pdfViewer = new Intent(activity, PrinterActivity.class);
        pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, url);
        getDialog().dismiss();
        startActivity(pdfViewer);
    }

    private void onFailure(Throwable t) {
        if (t instanceof QrNotFoundException) {
            new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, ResourceUtils.getString("qr_not_found"));
        }
        else {
            new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, ErrorUtils.getErrorMessage(t));
        }
    }


    @OnClick({R.id.btnScanQr, R.id.edtCodigoTaller})
    public void leerQR() {
        compositeDisposable.add(QrUtils.read(value -> value.length() >= 24)
                .subscribe(edtCodigoTaller::setText,
                        throwable -> {
                            Log.e(TAG, "Error al leer el QR", throwable);
                                onFailure(throwable);

                        }

                ));
    }

    @OnClick(R.id.btnCancelAd)
    public void dismissDialog() {
        getDialog().dismiss();
    }




}
