package com.gnp.ajustadormovil.android.views.volantes.tercero;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.Tercero;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.AjustadorMovilApplication;
import com.gnp.ajustadormovil.android.views.commons.events.ItemClickListener;

import java.util.List;

public class VolanteTerceroAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Tercero> terceros;
    private ItemClickListener listener;

    public VolanteTerceroAdapter(List<Tercero> volantes, ItemClickListener listener) {
        this.terceros = volantes;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new EmptyHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_items, parent, false));
        } else {
            return new VolanteHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectable, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof VolanteHolder) {
            if (terceros.get(position) instanceof TerceroAuto) {
                ((VolanteHolder) holder).iconoVolante.setImageResource(R.drawable.ic_ter_auto);
                ((VolanteHolder) holder).tvTitle.setText("Tercero auto");
                ((VolanteHolder) holder).detalle.setText(terceros.get(position).getDescripcion());
                ((VolanteHolder) holder).btnPrint.setImageResource(R.drawable.ic_arrow);

            } else if (terceros.get(position) instanceof TerceroNoAuto) {
                ((VolanteHolder) holder).iconoVolante.setImageResource(R.drawable.ic_ter_noauto);
                ((VolanteHolder) holder).tvTitle.setText("Tercero no auto");
                ((VolanteHolder) holder).detalle.setText(terceros.get(position).getDescripcion());
                ((VolanteHolder) holder).btnPrint.setImageResource(R.drawable.ic_imprimir);
                if (((TerceroNoAuto) terceros.get(position)).getVolante() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ((VolanteHolder) holder).btnPrint.setColorFilter(AjustadorMovilApplication.getInstance().getColor(R.color.colorGreen));
                    } else {
                        ((VolanteHolder) holder).btnPrint.setImageResource(R.drawable.ic_imprimir_green);
                    }
                }
            } else if (terceros.get(position) instanceof TerceroPersona) {
                ((VolanteHolder) holder).iconoVolante.setImageResource(R.drawable.ic_ter_persona);
                ((VolanteHolder) holder).tvTitle.setText("Tercero peatón");
                ((VolanteHolder) holder).detalle.setText(terceros.get(position).getDescripcion());
                ((VolanteHolder) holder).btnPrint.setImageResource(R.drawable.ic_imprimir);
                if (((TerceroPersona) terceros.get(position)).getPersona().getVolante() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ((VolanteHolder) holder).btnPrint.setColorFilter(AjustadorMovilApplication.getInstance().getColor(R.color.colorGreen));
                    } else {
                        ((VolanteHolder) holder).btnPrint.setImageResource(R.drawable.ic_imprimir_green);
                    }
                }
            }
        } else if (holder instanceof EmptyHolder) {
            ((EmptyHolder) holder).tvEmpty.setText("No hay información de terceros para la impresión de volantes");
        }


    }


    public Tercero getItem(int position) {
        return terceros.get(position);
    }

    public void setItems(List<Tercero> terceros) {
        this.terceros = terceros;
    }

    @Override
    public int getItemCount() {
        if (terceros.isEmpty()) return 1;
        else return terceros.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (terceros.isEmpty()) return 1;
        else return 2;
    }


    public class VolanteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AppCompatTextView tvTitle;
        AppCompatTextView detalle;
        ImageView iconoVolante;
        ImageView btnPrint;

        VolanteHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            detalle = itemView.findViewById(R.id.tvContent);
            iconoVolante = itemView.findViewById(R.id.ivIcon);
            btnPrint = itemView.findViewById(R.id.btnSelect);

            itemView.setOnClickListener(this);
            btnPrint.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            listener.onItemClick(getAdapterPosition(), view);
        }
    }


    class EmptyHolder extends RecyclerView.ViewHolder {
        TextView tvEmpty;

        EmptyHolder(View itemView) {
            super(itemView);

            tvEmpty = itemView.findViewById(R.id.tvEmptyItems);

        }
    }


}
