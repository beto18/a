package com.gnp.ajustadormovil.android.views.volantes.tercero.auto;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.data.RepositoryFactory;
import com.gnp.ajustadormovil.android.data.orquestador.OrquestadorFactory;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.amis.impl.FirmaDigitalHelper;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.CondicionPersona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Volanteable;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.qr.QrData;
import com.gnp.ajustadormovil.android.views.utils.qr.QrUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.volantes.VolanteOcupanteAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseFragment;
import com.gnp.ajustadormovil.android.views.commons.events.ItemVolanteOcupanteListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class VolanteTerceroAutoFragment extends BaseFragment implements ItemVolanteOcupanteListener, VolanteTerceroAutoMvpView {

    private View mView;
    @BindView(R.id.optionMedico)
    CardView printMedico;
    @BindView(R.id.optionAdmision)
    CardView printAdmision;
    @BindView(R.id.nomConductor)
    TextView nomCon;
    @BindView(R.id.nomVehiculo)
    TextView nomAuto;
    @BindView(R.id.btnPrintVolanteM)
    ImageView btnPrintVolanteM;
    @BindView(R.id.btnPrintAdmision)
    ImageView btnPrintAdmision;
    @BindView(R.id.rvVolantesMedicos)
    RecyclerView rvOcupantes;
    @BindView(R.id.message_volante)
    ConstraintLayout message_container;
    @BindView(R.id.tvNoHay)
    TextView tvMessage;
    @BindView(R.id.tvNoHayV)
    TextView tvMessageV;


    private TerceroAuto terceroAuto;
    private VolanteOcupanteAdapter ocupanteAdapter;
    private VolanteTerceroAutoMvpPresenter presenter;
    private CompositeDisposable compositeDsiposable;
    private AppCompatActivity activity;
    private String idAjuste;
    private long id;
    private static final String TAG = VolanteTerceroAutoFragment.class.getSimpleName();
    private GeneralSnackBar snackbar;
    private AmisInteractor amisInteractor;
    private FirmaDigitalHelper firmaDigitalBuilder;
    private SiniestrosInteractor siniestrosInteractor;
    private String message = "Recuerda que el producto de la póliza es de Cashflow y no se podrá emitir un volante SIPAC";
    private Ajuste ajuste;


    private BroadcastReceiver volanteListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constants.FIRMA_DIGITAL.equals(intent.getAction())) {
                long id = intent.getExtras().getLong("id");
                    presenter.getTercero(id);
            }
        }
    };

    public static VolanteTerceroAutoFragment newInstance(long idTercero) {
        VolanteTerceroAutoFragment instance = new VolanteTerceroAutoFragment();
        instance.id = idTercero;
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDsiposable = ((BaseActivity) activity).getCompositeDisposable();
        presenter = new VolanteTerceroAutoPresenter(compositeDsiposable);
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();
        firmaDigitalBuilder = FirmaDigitalHelper.instance();
        snackbar = new GeneralSnackBar();
        presenter.onAttach(this);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_volante_tercero_auto, container, false);
        ButterKnife.bind(this, mView);
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        ajuste = RepositoryFactory.getAjusteRepository().findById(idAjuste);
        if (ajuste.getSiniestro().getPoliza() != null) {
            compositeDsiposable.add(siniestrosInteractor.getProductoCashflow(
                    ajuste.getSiniestro().getPoliza().getProductoComercial(),
                    ajuste.getSiniestro().getPoliza().getProductoTecnico())
                    .subscribe(cashflow -> {
                        if (cashflow) {
                            showDialgoCashflow();
                        }
                    }, throwable -> {
                        Log.e(TAG, "onCreateView: ", throwable);
                    }));
        }
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getTercero(id);
        activity.registerReceiver(volanteListener, new IntentFilter(Constants.FIRMA_DIGITAL));

    }


    private void initVolante() {

        if (terceroAuto != null) {
            if (terceroAuto.getConductor() != null) {
                nomCon.setText(terceroAuto.getConductor().getNombreCompleto());
                if (terceroAuto.getConductor().getVolante() != null) {
                    btnPrintVolanteM.setColorFilter(getResources().getColor(R.color.colorGreen));
                }
                if (!terceroAuto.getConductor().getCondicion().equals(CondicionPersona.LESIONADO)) {
                    printMedico.setVisibility(GONE);
                }
            } else {
                printMedico.setVisibility(GONE);
            }


            if (terceroAuto.getVehiculo() != null) {
                if (terceroAuto.getVehiculo().getVolante() != null) {
                    btnPrintAdmision.setColorFilter(getResources().getColor(R.color.colorGreen));
                }
                if (terceroAuto.getVehiculo().getDanoConsecuencia() != null && terceroAuto.getVehiculo().getPerdidaTotalEvidente() != null) {
                    if (!terceroAuto.getVehiculo().getDanoConsecuencia() && terceroAuto.getVehiculo().getPerdidaTotalEvidente()) {
                        printAdmision.setVisibility(VISIBLE);
                    } else if (terceroAuto.getVehiculo().getDanoConsecuencia() && !terceroAuto.getVehiculo().getPerdidaTotalEvidente()) {
                        printAdmision.setVisibility(VISIBLE);
                    } else if (!terceroAuto.getVehiculo().getDanoConsecuencia() && !terceroAuto.getVehiculo().getPerdidaTotalEvidente()) {
                        printAdmision.setVisibility(GONE);
                    }
                } else {
                    printAdmision.setVisibility(GONE);
                }
            } else {
                printAdmision.setVisibility(GONE);
            }

            nomAuto.setText(terceroAuto.getDescripcion());
            if (terceroAuto.getOcupantes() != null) {
                for (int i = 0; i < terceroAuto.getOcupantes().size(); i++) {
                    if (terceroAuto.getOcupantes().get(i).getCondicion() != CondicionPersona.LESIONADO) {
                        terceroAuto.getOcupantes().remove(i);
                    }
                }
                ocupanteAdapter = new VolanteOcupanteAdapter(terceroAuto.getOcupantes(), this);
                rvOcupantes.setLayoutManager(new LinearLayoutManager(activity));
                rvOcupantes.setHasFixedSize(true);
                rvOcupantes.setAdapter(ocupanteAdapter);
                if (ocupanteAdapter.getOcupantesList().size() == 0) {
                    rvOcupantes.setVisibility(GONE);
                }
            } else {
                rvOcupantes.setVisibility(GONE);
            }
            if (printAdmision.getVisibility() == GONE && printMedico.getVisibility() == GONE && rvOcupantes.getVisibility() == GONE) {
                message_container.setVisibility(View.VISIBLE);
                if (terceroAuto.getConductor() == null) {
                    tvMessage.setText("El conductor no tiene pase médico por la siguiente razón: \n\n" + "- El conductor no ha sido guardado \n");

                } else if (!terceroAuto.getConductor().getCondicion().equals(CondicionPersona.LESIONADO)) {
                    tvMessage.append("El conductor " + terceroAuto.getConductor() != null ? terceroAuto.getConductor().getNombreCompleto() : "" + " no tiene pase médico por la siguientes razón: \n\n" +
                            "- El conductor es ileso o fallecido");
                }
                if (terceroAuto.getVehiculo() == null) {
                    tvMessageV.setText("El vehículo no se le puede imprimir volante de admisión por la siguiente razón: \n\n" + "-El vehículo no ha sido guardado \n");
                } else if (!terceroAuto.getVehiculo().getDanoConsecuencia()) {
                    tvMessageV.setText("El vehículo " + terceroAuto.getDescripcion() + " no se le puede imprimir volante de admisión por la siguiente razón: \n\n" +
                            "-El vehículo no sufrio daños a consecuencias ni fue perdida total evidente");
                }

            }
        }
    }


    private void showDialgoCashflow(){
        SpannableStringBuilder mBuilder = new SpannableStringBuilder();
        SpannableString spannable = new SpannableString(message);
        spannable.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                message.indexOf("Cashflow"),
                message.indexOf("Cashflow") + "Cashflow".length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mBuilder.append(spannable);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setTitle("Aviso");
        builder.setCancelable(false);
        builder.setMessage(mBuilder)
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.dismiss();
                            }
                        });
        builder.show();
    }

    private void showDialogMedico(Afectado afectado) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag(VolanteTerceroAutoPersonaFragment.class.getName());
        if (prev != null){
            transaction.remove(prev);
        }
        DialogFragment dialogFragment = VolanteTerceroAutoPersonaFragment.newInstance(afectado, terceroAuto.getId());
        dialogFragment.setCancelable(false);
        dialogFragment.show(transaction, VolanteTerceroAutoPersonaFragment.class.getName());
    }

    private void showDialogAdmision() {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag(VolanteTerceroAutoVehiculoFragment.class.getName());
        if (prev != null){
            transaction.remove(prev);
        }
        DialogFragment dialogFragment = VolanteTerceroAutoVehiculoFragment.newInstance(terceroAuto.getId());
        dialogFragment.setCancelable(false);
        dialogFragment.show(transaction, VolanteTerceroAutoVehiculoFragment.class.getName());
    }


    @Override
    public void onOcupanteClick(int position) {
        if (terceroAuto.getOcupantes().get(position).getVolante() != null) {
            onError("El volante médico del ocupante ya fue impreso");
        } else {
            showDialogMedico(terceroAuto.getOcupantes().get(position));
        }
    }

    @Override
    public void onError(String message) {
        snackbar.largeSnackError(activity, message);
    }

    @Override
    public void onFailure(Throwable throwable) {
        snackbar.largeSnackError(activity, throwable);
    }

    @Override
    public void setTercero(TerceroAuto terceroAuto) {
        this.terceroAuto = terceroAuto;
        initVolante();
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.getTercero(id);
    }


    @OnClick({R.id.optionMedico, R.id.btnPrintVolanteM})
    public void printPaseMedico() {
        if (terceroAuto.getConductor() != null) {
            if (terceroAuto.getConductor().getVolante() != null) {
                onError("El volante médico del conductor ya fue impreso");
            } else {
                showDialogMedico(terceroAuto.getConductor());
            }
        } else {
            onError("Debe completar la informacion del conductor");
        }
    }

    @OnClick({R.id.optionAdmision, R.id.btnPrintAdmision})
    public void prinPaseDeAdmision() {
        if (terceroAuto.getVehiculo() != null) {
            if (isVolante(terceroAuto.getVehiculo())) {
                if (isUrl(terceroAuto.getVehiculo())) {
                    onError("El volante de admisión del vehículo ya fue impreso");
                } else {
                    if (terceroAuto.getFolioAmis() != null) {
                        generarQR();
                    } else {
                        new GeneralSnackBar().largeSnackOk(activity, "SIPAC generado");
                    }
                }
            } else {
                showDialogAdmision();
            }

        } else {
            onError("Debe completar la informacion del vehículo");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != volanteListener){
            activity.unregisterReceiver(volanteListener);
            volanteListener = null;
        }
    }

    private boolean isUrl(Volanteable volante) {
        return volante.getVolante().getUrl() != null &&
                !volante.getVolante().getUrl().equals(Constants.FIRMA_DIGITAL);

    }
    private boolean isVolante(Volanteable volante) {
        return volante.getVolante() != null;
    }

    public void generarQR() {
        compositeDsiposable.add(amisInteractor.reFolear(idAjuste).andThen(
                firmaDigitalBuilder.get(idAjuste, id)
                .doOnSubscribe(it ->LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .flatMap(firma -> firmaDigitalBuilder.encriptar(firma, terceroAuto.getFolioAmis()))
                .map(modulo -> {
                    String[] qr = modulo.split("\\|");
                    String title = ResourceUtils.getString("amis_modulo_" + qr[1]);
                    Log.d(TAG, title + ": " + modulo);
                    QrData data = new QrData();
                    data.setTitulo(title);
                    data.setQr(modulo);
                    return data;
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(QrUtils::show, throwable -> {
                    Log.e(TAG, "Error al mostrar QRs d información", throwable);
                    new GeneralSnackBar().largeSnackError(activity, throwable);


                }));
    }
}












