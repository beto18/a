package com.gnp.ajustadormovil.android.views.volantes.tercero.auto;

import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

public interface VolanteTerceroAutoMvpPresenter<V extends VolanteTerceroAutoMvpView> extends MvpPresenter<V> {
    void getTercero(long idTercero);
}
