package com.gnp.ajustadormovil.android.views.volantes.tercero.auto;

import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.views.commons.base.MvpView;

public interface VolanteTerceroAutoMvpView extends MvpView {
    void onError(String error);
    void onFailure(Throwable t);
    void setTercero(TerceroAuto terceroAuto);


}