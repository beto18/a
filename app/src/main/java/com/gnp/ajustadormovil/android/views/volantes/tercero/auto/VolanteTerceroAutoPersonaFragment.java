package com.gnp.ajustadormovil.android.views.volantes.tercero.auto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Afectado;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Ocupante;
import com.gnp.ajustadormovil.android.models.orquestador.commons.Persona;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.volantes.VolanteCoberturaAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

import static android.view.View.GONE;

public class VolanteTerceroAutoPersonaFragment extends BaseDialogFragment {

    public static final String TAG = "TAG";
    private VolanteCoberturaAdapter coberturaAdapter;
    private List<Cobertura> coberturasList = new ArrayList<>();
    private CheckBox aplicaDeducible;
    private CompositeDisposable compositeDsiposable;
    private SiniestrosInteractor siniestrosInteractor;
    private AppCompatActivity activity;
    private View view;
    private RecyclerView rvCoberturasDisponibles;
    private List<PolizaCobertura> coberturas;
    private Long idTercero;
    private Afectado afectado;
    private int categoria;
    private Ajuste ajuste;
    private Spinner spinnerNoDeducible;
    private VolanteInteractor volanteService;
    private String idAjuste;
    private AjusteInteractor ajusteInteractor;
    private LinearLayout linearLayout;


    public static DialogFragment newInstance(Afectado afectado, Long id) {
        VolanteTerceroAutoPersonaFragment instance = new VolanteTerceroAutoPersonaFragment();
        instance.afectado = afectado;
        instance.idTercero = id;
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialog);
        compositeDsiposable = ((BaseActivity) activity).getCompositeDisposable();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        categoria = afectado instanceof Ocupante ? 16 : 15;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_dialog_volante_medico, container, false);
        ButterKnife.bind(this, view);
        initializeView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAjuste();
    }


    private void initializeView() {
        rvCoberturasDisponibles = view.findViewById(R.id.rvCoberturasDisponibles);
        rvCoberturasDisponibles.setLayoutManager(new LinearLayoutManager(activity));
        rvCoberturasDisponibles.setHasFixedSize(true);
        spinnerNoDeducible = view.findViewById(R.id.sp_no_deducible);
        aplicaDeducible = view.findViewById(R.id.deducible);
        linearLayout = view.findViewById(R.id.linearLayout);


    }

    private void showDialogMedico() {
        coberturas = new ArrayList<>();
        Log.d(TAG, "imprimir: " + coberturas);
        coberturaAdapter = new VolanteCoberturaAdapter(((position, view) -> {}));
        rvCoberturasDisponibles.setAdapter(coberturaAdapter);
        view.findViewById(R.id.armadora).setVisibility(View.GONE);
        spinnerNoDeducible.setVisibility(View.GONE);
        view.findViewById(R.id.armadora).setVisibility(View.GONE);
        view.findViewById(R.id.sp_no_deducible).setVisibility(View.GONE);
        view.findViewById(R.id.view).setVisibility(View.GONE);
        aplicaDeducible.setVisibility(GONE);
        coberturaAdapter.addAll(getCoberturas());


    }

    private List<PolizaCobertura> getCoberturas() {
        if (ajuste.getSiniestro().getPoliza() != null) {
            if (ajuste.getSiniestro().getPoliza().getCoberturas() != null) {
                for (int i = 0; i < ajuste.getSiniestro().getPoliza().getCoberturas().size(); i++) {
                    for (int j = 0; j < coberturasList.size(); j++) {
                        if (ajuste.getSiniestro().getPoliza().getCoberturas().get(i).getClave().equals(coberturasList.get(j).getClave())) {
                            coberturas.add(ajuste.getSiniestro().getPoliza().getCoberturas().get(i));
                        }
                    }
                }
            } else {
                view.findViewById(R.id.coberturas_list_content).setVisibility(GONE);
            }
        } else {
            view.findViewById(R.id.coberturas_list_content).setVisibility(GONE);
        }
        Log.d(TAG, "imprimir: " + coberturas);


        return coberturas;
    }

    private void getAjuste() {
        compositeDsiposable.add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(ajuste -> {
                    this.ajuste = ajuste;
                    descargarCoberturasVolante();
                }, throwable -> {
                    Log.e(TAG, "Error al obtner el ajuste", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        onError(ErrorUtils.getErrorMessage(throwable));
                    }
                }));

    }


    private void descargarCoberturasVolante() {
        compositeDsiposable.add(siniestrosInteractor.getCoberturasVolante(categoria, ajuste.getSiniestro().getTipoSiniestro().getClave())
                .subscribe(coberturas -> {
                    coberturasList = coberturas;
                    showDialogMedico();
                }, throwable -> {
                    new GeneralSnackBar().largeSnackError(activity, "conductor: " + throwable.getMessage());
                }));

    }


    @OnClick(R.id.btnAceptar)
    public void GenerarVolante() {

        VolanteMedico volante = new VolanteMedico();
        volante.setAplicaDeducible(true);
        volante.setRazonNoDeducible(null);
        volante.setEnvioEmail(true);
        volante.setImpresionFisica(true);
        volante.setCoberturas(coberturaAdapter.getListCoberturasAgregadas());

        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
            if (afectado instanceof Persona) {
                sendVolanteMedico(idTercero, 0L, volante);
            } else {
                sendVolanteMedico(idTercero, afectado.getId(), volante);
            }

        } else {
            if (coberturaAdapter.getListCoberturasAgregadas().size() == 0) {
                onError("No puede imprimir el volante médico si no tiene coberturas seleccionadas o no hay coberturas para el volante");

            } else {
                if (afectado instanceof Persona) {
                    sendVolanteMedico(idTercero, 0L, volante);
                } else {
                    sendVolanteMedico(idTercero, afectado.getId(), volante);
                }
            }
        }
    }

    private void sendVolanteMedico(Long idTercero, Long id, VolanteMedico volante) {
        if (afectado instanceof Persona) {
            createVolante(idTercero, volante);
        } else {
            createVolante(idTercero, id, volante);
        }
    }


    private void createVolante(long id, VolanteMedico volantePasajeros) {

        compositeDsiposable.add(volanteService.generateTerceroAutoConductor(idAjuste, id, volantePasajeros)
                .subscribe(pdfLink -> {
                    printPDf(pdfLink.getUrl());
                }, throwable -> {
                    Log.e(TAG, "Error al crear volante tercero conductor ", throwable);
                    onError(ErrorUtils.getErrorMessage(throwable));

                }));
    }

    private void createVolante(long idTercero, long idOcupante, VolanteMedico volantePasajeros) {
        compositeDsiposable.add(volanteService.generateTerceroAutoOcupante(idAjuste, idTercero, idOcupante, volantePasajeros)
                .subscribe(pdfLink -> {
                    printPDf(pdfLink.getUrl());
                }, throwable -> {
                    Log.e(TAG, "Error al crear volante tercero ocupante", throwable);
                    onError(ErrorUtils.getErrorMessage(throwable));

                }));

    }

    public void printPDf(String url) {
        if (url == null || url.equals("")) {
            new GeneralSnackBar().largeSnackError(activity, getString(R.string.url_error_message));
        } else {
            Intent pdfViewer = new Intent(activity, PrinterActivity.class);
            pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, url);
            dismissDialog();
            startActivity(pdfViewer);
        }
    }

    private void onError(String message) {
        new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, message);
    }




    @OnClick(R.id.btnCancel)
    public void dismissDialog() {
        getDialog().dismiss();
    }
}
