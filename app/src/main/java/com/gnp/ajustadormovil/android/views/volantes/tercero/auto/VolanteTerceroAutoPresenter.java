package com.gnp.ajustadormovil.android.views.volantes.tercero.auto;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;

public class VolanteTerceroAutoPresenter<V extends VolanteTerceroAutoMvpView> extends BasePresenter<V>
        implements VolanteTerceroAutoMvpPresenter<V> {

    private TerceroAutoInteractor terceroAutoService;
    private String idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);

    public VolanteTerceroAutoPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();

    }


    @Override
    public void getTercero(long idTercero) {
        getmCompositeDisposable().add(terceroAutoService.get(idAjuste, idTercero)
                .subscribe(terceroAuto -> {
                    getMvpView().setTercero(terceroAuto);
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el tercero auto", throwable);
                    getMvpView().onFailure(throwable);
                }));


    }


}
