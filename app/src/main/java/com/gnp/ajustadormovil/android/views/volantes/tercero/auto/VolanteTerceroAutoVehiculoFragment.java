package com.gnp.ajustadormovil.android.views.volantes.tercero.auto;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gnp.ajustadormovil.android.BuildConfig;
import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.amis.AmisInteractor;
import com.gnp.ajustadormovil.android.interactors.amis.impl.FirmaDigitalHelper;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.amis.FolioContraparteAmis;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteVehiculoTercero;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.Utils;
import com.gnp.ajustadormovil.android.utils.commons.CollectionsUtils;
import com.gnp.ajustadormovil.android.utils.commons.DateUtils;
import com.gnp.ajustadormovil.android.utils.commons.RXUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.commons.DatePickerFragment;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.commons.piezas.GeneralDamageFragment;
import com.gnp.ajustadormovil.android.views.utils.components.DialogUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.LoadingIndicator;
import com.gnp.ajustadormovil.android.views.utils.components.SpinnerUtils;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;
import com.gnp.ajustadormovil.android.views.utils.qr.QrData;
import com.gnp.ajustadormovil.android.views.utils.qr.QrUtils;
import com.gnp.ajustadormovil.android.views.volantes.VolanteCoberturaAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class VolanteTerceroAutoVehiculoFragment extends BaseDialogFragment {


    private TerceroAuto terceroAuto;
    private Ajuste ajuste;
    public static final String TAG = VolanteTerceroAutoVehiculoFragment.class.getName();
    private List<Cobertura> coberturasList = new ArrayList<>();
    private List<Long> listCausasCondicionamiento = new ArrayList<>();
    private List<String> nombreNoDeducibles = new ArrayList<>();
    private List<CatalogoClave> otrasAseguradoras = new ArrayList<>();
    private List<String> nombreTipoPolizas = new ArrayList<>();
    private List<String> nombreDanos = new ArrayList<>();
    private List<Municipio> municipios = new ArrayList<>();
    private List<ColoniaResumen> colonias = new ArrayList<>();
    private List<CatalogoClave> nacionalidades = new ArrayList<>();
    private List<CatalogoId> danos = new ArrayList<>();
    private List<String> nombreNacionalidades = new ArrayList<>();
    private List<String> nombreOtrasAseguradoras = new ArrayList<>();
    private List<CatalogoClave> tipoVolantes = new ArrayList<>();
    private List<CatalogoClave> tipoTerceros = new ArrayList<>();
    private List<CatalogoId> tipoPolizas = new ArrayList<>();
    private List<CatalogoClaveInfo> estadosList = new ArrayList<>();
    private CheckBox aplicaDeducible;
    private Spinner spinnerNoDeducible, spinnerDeducible2;
    private CheckBox aplicaDeducible2;
    private EditText folioSegmentacion;
    private List<String> nombreTipoTerceros = new ArrayList<>();
    private List<String> nombreTipoVolantes = new ArrayList<>();
    private List<CatalogoId> causasCondicionamiento = new ArrayList<>();
    private Button btnAceptar, btnCancel;
    private List<CatalogoClave> ordenDirectas = new ArrayList<>();
    private String tipoVolante;
    private CompositeDisposable compositeDsiposable;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private Spinner spCondicionamiento;
    private CheckBox segmentador, siniestroVerifica, cobranzaPoliza, declaracionConductor, numeroSerie, agravamiento, correccionPoliza, pendienteInicioActa,
            pendienteMP, tomadorDatos, licencia, otroMotivo;
    private EditText edtRFCBeneficiario, edtNombreTercero, edtCalle, edtEntreCalles, edtNumEx, edtNumInt, edtTelDialog, edtCorreoDialog, edtNumPoliza, datePoliza, edtTitularPoliza, edtResCivil;
    private Spinner spTipoVolante, getSpinnerNoDeducible, spTipoTerceros, spinnerEstadoV, spinnerMunicipioV, spinnerColoniaV, spinnerNacionalidadV, spOtraCompania, spTipoPoliza;
    private CheckBox requiereGrua, volanteCondicionado;
    private RecyclerView rvCoberturasDisponibles;
    private VolanteCoberturaAdapter coberturasVehiculoAdapter;
    private Spinner spOrdenDirecta;
    ArrayAdapter<String> adapter2;
    private GeneralDamageFragment generalDamageToolsParts;

    private View view;
    private LinearLayout linearLayout;
    private PolizaCobertura coberturaSeleccionada;
    private VolanteInteractor volanteService;
    private TextView link_segmento;
    private Button solicitarGrua;
    private LinearLayout cobertura_container;
    private AppCompatActivity activity;
    private AjusteInteractor ajusteInteractor;
    private String idAjuste;
    private TerceroAutoInteractor terceroAutoService;
    private AmisInteractor amisInteractor;
    private long id;
    private FolioContraparteAmis contraparte;
    private FirmaDigitalHelper firmaDigitalBuilder;


    public static VolanteTerceroAutoVehiculoFragment newInstance(long idTercero) {
        VolanteTerceroAutoVehiculoFragment instance = new VolanteTerceroAutoVehiculoFragment();
        instance.id = idTercero;
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (AppCompatActivity) context;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialog);
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        amisInteractor = OrquestadorInteractorFactory.getAmisInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        compositeDsiposable = ((BaseActivity) activity).getCompositeDisposable();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        terceroAutoService = OrquestadorInteractorFactory.getTerceroAutoService();
        firmaDigitalBuilder = FirmaDigitalHelper.instance();
        addFragmentPartesFaltantes();
    }


    private Completable getAjuste() {
        return Completable.create(e -> {
            compositeDsiposable.add(ajusteInteractor.getAjuste(idAjuste)
                    .subscribe(ajuste -> {
                        this.ajuste = ajuste;
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtner el ajuste", throwable);
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }

                    }));
        });

    }

    private Completable getTercero() {
        return Completable.create(e -> {
            compositeDsiposable.add(terceroAutoService.get(idAjuste, id)
                    .subscribe(terceroAuto -> {
                        this.terceroAuto = terceroAuto;
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Error al obtner el tercero Auto", throwable);
                        e.tryOnError(throwable);
                    }));
        });

    }

    private Completable descargaCatalogos() {
        return Completable.mergeArrayDelayError(
                downloadOtrasAseguradoras(),
                downloadNacionalidades(),
                downloadPolizas(),
                downloadTipoTerceros(),
                downloadTipoVolante(),
                downloadOrdenDireta(),
                downloadStates(),
                downloadCausasCondicionamiento());

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_volante_auto_tercero_admision, container, false);
        ButterKnife.bind(this, view);
        initializeView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        compositeDsiposable.add(descargaCatalogos()
                .andThen(getAjuste())
                .andThen(getTercero())
                .andThen(getContraparte())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(t -> LoadingIndicator.message("Cargando"))
                .doFinally(LoadingIndicator::finish)
                .subscribe(() -> {
                            newDialogVolanteAdmision(view);
                            setEvents();
                        },
                        throwable -> {
                            Log.e(TAG, "Error al obtner los catalogos", throwable);
                            new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "Error al descargar los catálogos");

                        }));

    }


    private void initializeView() {
        linearLayout = view.findViewById(R.id.linearLayout);
        requiereGrua = view.findViewById(R.id.requiere_gruaV);
        volanteCondicionado = view.findViewById(R.id.volante_condicionado);
        edtRFCBeneficiario = view.findViewById(R.id.edtBeneficiario);
        edtNombreTercero = view.findViewById(R.id.edtNombreTercero);
        edtCalle = view.findViewById(R.id.edtCalle);
        edtEntreCalles = view.findViewById(R.id.edtEntreCalle);
        edtNumEx = view.findViewById(R.id.edtNumEx);
        edtNumInt = view.findViewById(R.id.edtNumIn);
        edtTelDialog = view.findViewById(R.id.edtPhoneDialog);
        edtCorreoDialog = view.findViewById(R.id.edtCorreoDialog);
        edtNumPoliza = view.findViewById(R.id.etNumeroPoliza);
        datePoliza = view.findViewById(R.id.etDateVigenciaD);
        edtTitularPoliza = view.findViewById(R.id.edtTitularPoliza);
        edtResCivil = view.findViewById(R.id.edtResponsabilidadCivil);
        siniestroVerifica = view.findViewById(R.id.siniestro_verificacion);
        cobranzaPoliza = view.findViewById(R.id.cobranza_poliza);
        declaracionConductor = view.findViewById(R.id.declaracion_conductor);
        numeroSerie = view.findViewById(R.id.numero_serie);
        agravamiento = view.findViewById(R.id.agravamiento_riesgo);
        correccionPoliza = view.findViewById(R.id.correcion_poliza);
        pendienteInicioActa = view.findViewById(R.id.pendiente_inicio_acta);
        pendienteMP = view.findViewById(R.id.pendiente);
        tomadorDatos = view.findViewById(R.id.tomador_datos);
        licencia = view.findViewById(R.id.licencia);
        otroMotivo = view.findViewById(R.id.otro);
        folioSegmentacion = view.findViewById(R.id.et_folio_segmetacionV);
        aplicaDeducible = view.findViewById(R.id.deducible);
        aplicaDeducible2 = view.findViewById(R.id.deducible2);
        spCondicionamiento = view.findViewById(R.id.spCondicionamieto);
        spinnerNoDeducible = view.findViewById(R.id.spinner_razon);
        spinnerDeducible2 = view.findViewById(R.id.spinner_razon2);
        spOrdenDirecta = view.findViewById(R.id.spOrdenDirecta);
        spTipoVolante = view.findViewById(R.id.spTipoVolanteT);
        spTipoTerceros = view.findViewById(R.id.spTipoTerceros);
        spinnerEstadoV = view.findViewById(R.id.spEstadoDialog);
        spinnerMunicipioV = view.findViewById(R.id.spMunicipioDialog);
        spinnerColoniaV = view.findViewById(R.id.spColoniaDialog);
        spinnerNacionalidadV = view.findViewById(R.id.spNacionalidad);
        spOtraCompania = view.findViewById(R.id.spCompaniaAseguradora);
        spTipoPoliza = view.findViewById(R.id.spTipoPoliza);
        segmentador = view.findViewById(R.id.segmentadoV);
        link_segmento = view.findViewById(R.id.link_segmentoV);
        rvCoberturasDisponibles = view.findViewById(R.id.rvCoberturaDisponiblesTV);
        solicitarGrua = view.findViewById(R.id.btnGruaV);
        cobertura_container = view.findViewById(R.id.coberturas_gnp);
    }


    private void setEvents() {

        eventCheckedCondicionado();
        eventSelectedCondicionamiento();
        eventIntentLiga(link_segmento, BuildConfig.SEGMENTADOR);
        eventIntentLiga(solicitarGrua, "https://docs.google.com/forms/d/e/1FAIpQLSf_QtXEfiXJOT9vUs3X1WCsdmcNHVndv5_rk82rHIcPVWTU1w/viewform?c=0&w=1");
        eventSpinners();
        eventCheckedSegmentador();
        eventCheckedRequiereGrua();
        eventCheckedDeducible();
        eventDatePoliza();


    }


    private void newDialogVolanteAdmision(View view) {


        view.findViewById(R.id.piezas_tercero_container).setVisibility(GONE);
        view.findViewById(R.id.segmentoLimit).setVisibility(GONE);
        view.findViewById(R.id.administrativoLimit).setVisibility(GONE);
        view.findViewById(R.id.gruaLimit).setVisibility(GONE);
        view.findViewById(R.id.razon_title).setVisibility(GONE);
        view.findViewById(R.id.spinner_razon).setVisibility(GONE);
        view.findViewById(R.id.line_deducible).setVisibility(GONE);
        view.findViewById(R.id.adap_container).setVisibility(View.GONE);
        view.findViewById(R.id.cristales_container).setVisibility(View.GONE);
        view.findViewById(R.id.danos_int_container).setVisibility(View.GONE);
        view.findViewById(R.id.materiales_container).setVisibility(View.GONE);
        view.findViewById(R.id.demostracion_container).setVisibility(View.GONE);
        view.findViewById(R.id.llantas_container).setVisibility(View.GONE);
        view.findViewById(R.id.parcial_container).setVisibility(View.GONE);
        view.findViewById(R.id.civil_container).setVisibility(View.GONE);
        view.findViewById(R.id.cbDeducibleAd).setVisibility(GONE);
        view.findViewById(R.id.link_segmento).setVisibility(GONE);
        view.findViewById(R.id.til_folio_seg).setVisibility(GONE);
        view.findViewById(R.id.remate_container).setVisibility(GONE);
        view.findViewById(R.id.deducibleLimit).setVisibility(GONE);
        view.findViewById(R.id.title_vehiculo).setVisibility(GONE);
        view.findViewById(R.id.rvCoberturasDisponiblesV).setVisibility(GONE);
        view.findViewById(R.id.TextViewNotaDeducible).setVisibility(GONE);
        view.findViewById(R.id.deducible2).setVisibility(GONE);
        view.findViewById(R.id.recycler_parte_faltante).setVisibility(GONE);
        view.findViewById(R.id.requiere_grua).setVisibility(GONE);
        view.findViewById(R.id.et_folio_segmetacion).setVisibility(GONE);
        view.findViewById(R.id.link_segmento).setVisibility(GONE);
        view.findViewById(R.id.btnGrua).setVisibility(GONE);
        view.findViewById(R.id.piezas_faltantes_title).setVisibility(GONE);
        view.findViewById(R.id.btnParteFaltante).setVisibility(GONE);
        view.findViewById(R.id.segmentado).setVisibility(GONE);
        view.findViewById(R.id.et_deducible).setVisibility(GONE);
        view.findViewById(R.id.tilDeducible).setVisibility(GONE);
        view.findViewById(R.id.spinner_razon).setVisibility(GONE);
        view.findViewById(R.id.line_deducible).setVisibility(GONE);
        view.findViewById(R.id.tilFolSegTer).setVisibility(GONE);
        view.findViewById(R.id.title_vehiculo).setVisibility(GONE);
        siniestroVerifica.setVisibility(GONE);
        cobranzaPoliza.setVisibility(GONE);
        declaracionConductor.setVisibility(GONE);
        numeroSerie.setVisibility(GONE);
        agravamiento.setVisibility(GONE);
        correccionPoliza.setVisibility(GONE);
        pendienteInicioActa.setVisibility(GONE);
        pendienteMP.setVisibility(GONE);
        tomadorDatos.setVisibility(GONE);
        licencia.setVisibility(GONE);
        otroMotivo.setVisibility(GONE);
        folioSegmentacion.setVisibility(GONE);
        aplicaDeducible.setVisibility(GONE);
        link_segmento.setVisibility(GONE);
        cobertura_container.setVisibility(GONE);
        AdapterSpinnerGeneral<CatalogoId> adapterCondicionamiento = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, causasCondicionamiento, CatalogoId::getNombre);
        spCondicionamiento.setAdapter(adapterCondicionamiento);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.sp_little_item, nombreNoDeducibles);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNoDeducible.setAdapter(adapter);
        spinnerDeducible2.setAdapter(adapter);
        adapter2 = new ArrayAdapter<>(activity, R.layout.sp_little_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AdapterSpinnerGeneral<CatalogoClave> adapterOrdenDirecto = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, ordenDirectas, CatalogoClave::getNombre);
        adapterOrdenDirecto.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOrdenDirecta.setAdapter(adapterOrdenDirecto);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(activity, R.layout.sp_little_item, nombreTipoTerceros);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoTerceros.setAdapter(adapter3);
        AdapterSpinnerGeneral<CatalogoClaveInfo> adapterE = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, estadosList, CatalogoClaveInfo::getNombre);
        adapterE.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstadoV.setAdapter(adapterE);
        ArrayAdapter<String> adapterN = new ArrayAdapter<>(activity, R.layout.sp_little_item, nombreNacionalidades);
        adapterN.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNacionalidadV.setAdapter(adapterN);

        AdapterSpinnerGeneral<CatalogoClave> adapterO = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, otrasAseguradoras, CatalogoClave::getNombre);
        adapterO.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOtraCompania.setAdapter(adapterO);

        AdapterSpinnerGeneral<CatalogoId> adapterPoliza = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, tipoPolizas, CatalogoId::getNombre);
        adapterPoliza.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoPoliza.setAdapter(adapterPoliza);
        coberturasVehiculoAdapter = new VolanteCoberturaAdapter(((position, v) -> coberturaSeleccionada = coberturasVehiculoAdapter.getCoberturaSelected(position)));
        setContraparte();



    }

    private void setContraparte() {
        if (contraparte != null && terceroAuto.getFolioAmis() != null && contraparte.getCompania() != null) {
            if (contraparte.getCompania().equals(Constants.AMIS_ID_GNP.toString())) {
                spTipoTerceros.setSelection(nombreTipoTerceros.indexOf("Tercero Asegurado con GNP"));
            } else if (!contraparte.getCompania().equals(Constants.AMIS_ID_GNP.toString())) {
                spTipoTerceros.setSelection(nombreTipoTerceros.indexOf("Tercero Asegurado con Otra Cía"));
                putContraparte();

            } else {
                spTipoTerceros.setSelection(nombreTipoTerceros.indexOf("Tercero Sin Seguro"));
            }

            DialogUtils.alert(activity, "Aviso",
                    ResourceUtils.getString("pases_medicos_not_null"),
                    ResourceUtils.getString("btn_accept_text"),
                    (dialog, which) -> {
                        dialog.dismiss();
                    });
        }
    }

    private void putContraparte() {
        edtNumPoliza.setText(getPopulatedData(contraparte.getPoliza()));
        edtTitularPoliza.setText(getPopulatedData(contraparte.getTitular()));
        datePoliza.setText(getPopulatedData(contraparte.getVigenciaHasta()));
        try {
            Integer value = Integer.valueOf(contraparte.getCompania() == null ? "0" : contraparte.getCompania());
            String clave = RXUtils.sync(siniestrosInteractor.getOtraAseguradoraAmis(value));
            spOtraCompania.setSelection(SpinnerUtils.getPosition(spOtraCompania, clave));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }


    private Completable getContraparte() {
        return Completable.create(e -> {
            compositeDsiposable.add(amisInteractor.getContraparte(idAjuste, terceroAuto.getId())
                    .subscribe(contraparte -> {
                        this.contraparte = contraparte;
                        e.onComplete();
                    }, throwable -> {
                        Log.e(TAG, "Ocurrio un error al obtener la contraparte", throwable);
                        if (!(throwable instanceof ResourceNotFoundException)) {
                            e.tryOnError(throwable);
                        } else {
                            e.onComplete();
                        }
                    }));
        });


    }

    private void eventDatePoliza() {
        datePoliza.setOnClickListener(view2 -> {
            showDatePickerPoliza();
        });
    }

    private void eventCheckedDeducible() {
        aplicaDeducible2.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.spinner_razon2).setVisibility(GONE);
                view.findViewById(R.id.razon_title2).setVisibility(GONE);
                view.findViewById(R.id.line_deducible2).setVisibility(View.GONE);

            } else {
                view.findViewById(R.id.spinner_razon2).setVisibility(VISIBLE);
                view.findViewById(R.id.razon_title2).setVisibility(VISIBLE);
                view.findViewById(R.id.line_deducible2).setVisibility(View.VISIBLE);
            }
        });
    }

    private void eventCheckedRequiereGrua() {
        requiereGrua.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.btnParteFaltanteV).setVisibility(GONE);
                view.findViewById(R.id.btnGruaV).setVisibility(VISIBLE);
                view.findViewById(R.id.recycler_parte_faltanteV).setVisibility(GONE);
                view.findViewById(R.id.piezas_tercero_container).setVisibility(VISIBLE);
            } else {
                view.findViewById(R.id.piezas_tercero_container).setVisibility(GONE);
                view.findViewById(R.id.piezas_faltantes_titleV).setVisibility(GONE);
                view.findViewById(R.id.btnParteFaltanteV).setVisibility(GONE);
                view.findViewById(R.id.btnGruaV).setVisibility(GONE);
                view.findViewById(R.id.recycler_parte_faltanteV).setVisibility(GONE);
            }
        });
    }

    private void eventCheckedSegmentador() {
        segmentador.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                link_segmento.setVisibility(VISIBLE);
                folioSegmentacion.setVisibility(VISIBLE);
                view.findViewById(R.id.tilFolSegTer).setVisibility(VISIBLE);
                Utils.showKeyboard(folioSegmentacion, activity);

            } else {
                link_segmento.setVisibility(GONE);
                folioSegmentacion.setVisibility(GONE);
                view.findViewById(R.id.tilFolSegTer).setVisibility(GONE);
                Utils.hideKeyboard(folioSegmentacion, activity);
            }
        });
    }

    private void eventIntentLiga(TextView link_segmento, String s) {
        link_segmento.setOnClickListener(v -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(s));
            startActivity(i);
        });
    }

    private void eventSpinners() {
        spinnerEstadoV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                compositeDsiposable.add(catInteractor.getMunicipios(estadosList.get(position).getClave())
                        .subscribe(municipios -> {
                                    fillSpinnerMunicipalities(municipios);
                                }
                                , throwable -> {
                                    new GeneralSnackBar().largeSnackError(activity, "conductor: " + throwable.getMessage());
                                }));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerMunicipioV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (municipios.size() == 0) {
                    downloadColonias("01", "001");
                } else {
                    downloadColonias(estadosList.get(spinnerEstadoV.getSelectedItemPosition()).getClave(), municipios.get(position).getClave());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spTipoTerceros.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                coberturasVehiculoAdapter.clear();
                if (spTipoTerceros.getSelectedItem().toString().equals("Tercero Asegurado con GNP")) {
                    view.findViewById(R.id.pronto_container).setVisibility(View.GONE);
                    view.findViewById(R.id.directa_content).setVisibility(View.GONE);
                    view.findViewById(R.id.layout_gnp).setVisibility(VISIBLE);
                    view.findViewById(R.id.container).setVisibility(GONE);
                    view.findViewById(R.id.deducible).setVisibility(View.GONE);
                    view.findViewById(R.id.adap_container).setVisibility(View.GONE);
                    view.findViewById(R.id.cristales_container).setVisibility(View.GONE);
                    view.findViewById(R.id.danos_int_container).setVisibility(View.GONE);
                    view.findViewById(R.id.materiales_container).setVisibility(View.GONE);
                    view.findViewById(R.id.demostracion_container).setVisibility(View.GONE);
                    view.findViewById(R.id.llantas_container).setVisibility(View.GONE);
                    view.findViewById(R.id.parcial_container).setVisibility(View.GONE);
                    loadVolantes("T");
                    loadCoberturasVehiculo(8);
                }
                if (spTipoTerceros.getSelectedItem().toString().equals("Tercero Asegurado con Otra Cía")) {
                    view.findViewById(R.id.pronto_container).setVisibility(VISIBLE);
                    view.findViewById(R.id.directa_content).setVisibility(View.GONE);
                    view.findViewById(R.id.layout_gnp).setVisibility(View.GONE);
                    view.findViewById(R.id.container).setVisibility(VISIBLE);
                    view.findViewById(R.id.deducible2).setVisibility(View.GONE);
                    view.findViewById(R.id.deducible).setVisibility(View.GONE);
                    if (contraparte!= null && terceroAuto.getFolioAmis() != null) {
                        putContraparte();
                    }
                    loadVolantes("T");
                }
                if (spTipoTerceros.getSelectedItem().toString().equals("Tercero Sin Seguro")) {
                    view.findViewById(R.id.pronto_container).setVisibility(View.GONE);
                    view.findViewById(R.id.directa_content).setVisibility(VISIBLE);
                    view.findViewById(R.id.layout_gnp).setVisibility(View.GONE);
                    view.findViewById(R.id.container).setVisibility(VISIBLE);
                    view.findViewById(R.id.deducible2).setVisibility(View.GONE);
                    view.findViewById(R.id.deducible).setVisibility(View.GONE);
                    loadVolantes("D");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spTipoVolante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                coberturasVehiculoAdapter.clear();
                switch (spTipoVolante.getSelectedItem().toString()) {
                    case "Directa":
                        tipoVolante = "D";
                        coberturasVehiculoAdapter.clear();
                        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
                            view.findViewById(R.id.container_coberturas_vehiculo_tercero).setVisibility(GONE);
                        } else {
                            if (spTipoTerceros.getSelectedItem().toString().equals("Tercero Sin Seguro")) {
                                view.findViewById(R.id.directa_content).setVisibility(VISIBLE);

                            } else {
                                if ("Tercero Sin Seguro".equals(spTipoTerceros.getSelectedItem().toString())) {
                                    if (spOrdenDirecta.getSelectedItem().toString().equals("Pronto Pago") && view.findViewById(R.id.directa_content).getVisibility() == VISIBLE) {
                                        loadCoberturasVehiculo(5);
                                    } else {
                                        loadCoberturasVehiculo(4);
                                    }
                                }
                            }
                        }

                        break;
                    case "SIPAC":
                        tipoVolante = "SIPAC";
                        coberturasVehiculoAdapter.clear();
                        view.findViewById(R.id.directa_content).setVisibility(View.GONE);
                        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
                            view.findViewById(R.id.container_coberturas_vehiculo_tercero).setVisibility(GONE);
                        } else {
                            loadCoberturasVehiculo(6);
                        }
                        break;
                    case "Tradicional":
                        tipoVolante = "T";
                        coberturasVehiculoAdapter.clear();
                        view.findViewById(R.id.directa_content).setVisibility(View.GONE);
                        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
                            view.findViewById(R.id.container_coberturas_vehiculo_tercero).setVisibility(GONE);
                        } else {
                            loadCoberturasVehiculo(7);
                        }
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spOrdenDirecta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                coberturasVehiculoAdapter.clear();
                if (view.findViewById(R.id.directa_content).getVisibility() == VISIBLE) {
                    switch (ordenDirectas.get(spOrdenDirecta.getSelectedItemPosition()).getNombre()) {
                        case "Tradicional":
                            loadCoberturasVehiculo(7);
                            break;
                        case "Pronto Pago":
                            loadCoberturasVehiculo(9);
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerEstadoV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getMunicipalitiesDialog(estadosList.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void eventCheckedCondicionado() {
        volanteCondicionado.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.condicionamiento_container).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.condicionamiento_container).setVisibility(View.GONE);
            }
        });
    }

    private void eventSelectedCondicionamiento() {
        siniestroVerifica.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(1L);
            else
                removeFromCausasCondicionamiento(1);
        });
        cobranzaPoliza.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(2L);
            else
                removeFromCausasCondicionamiento(2);
        });
        declaracionConductor.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(3L);
            else
                removeFromCausasCondicionamiento(3);
        });
        numeroSerie.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(4L);
            else
                removeFromCausasCondicionamiento(4);
        });
        agravamiento.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(5L);
            else
                removeFromCausasCondicionamiento(5);
        });
        correccionPoliza.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(6L);
            else
                removeFromCausasCondicionamiento(6);
        });
        pendienteInicioActa.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(7L);
            else
                removeFromCausasCondicionamiento(7);
        });
        pendienteMP.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                listCausasCondicionamiento.add(8L);
            } else {
                removeFromCausasCondicionamiento(8);
            }
        });
        tomadorDatos.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(9L);
            else
                removeFromCausasCondicionamiento(9);
        });
        licencia.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(10L);
            else
                removeFromCausasCondicionamiento(10);
        });
        otroMotivo.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                listCausasCondicionamiento.add(11L);
            else
                removeFromCausasCondicionamiento(11);
        });
    }

    private void addFragmentPartesFaltantes() {
        generalDamageToolsParts = new GeneralDamageFragment();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(R.id.piezas_tercero_container, generalDamageToolsParts, Constants.TAG_PARTE_FALTANTE);
        ft.commit();
    }

    private String getPopulatedData(String value) {
        if (value == null) {
            return "";
        } else {
            return value;
        }
    }

    private void removeFromCausasCondicionamiento(Integer i) {
        listCausasCondicionamiento.remove(i);
    }

    private void showDatePickerPoliza() {
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance((view, date, age) -> {
            datePoliza.setText(date);
        });
        datePickerFragment.show(Objects.requireNonNull(activity).getSupportFragmentManager(), "datePicker");
    }

    private void loadCoberturasVehiculo(int tipoVolante) {

        coberturasVehiculoAdapter.clear();

        compositeDsiposable.add(siniestrosInteractor.getCoberturasVolante(tipoVolante
                , ajuste.getSiniestro().getTipoSiniestro().getClave())
                .subscribe(categoriaVolantesCoberturas -> {
                            coberturasVehiculoAdapter.clear();
                            coberturasList = categoriaVolantesCoberturas;
                            List<PolizaCobertura> coberturas = new ArrayList<>();
                            if (ajuste.getSiniestro().getPoliza() != null) {
                                if (ajuste.getSiniestro().getPoliza().getCoberturas() != null) {
                                    for (int i = 0; i < ajuste.getSiniestro().getPoliza().getCoberturas().size(); i++) {
                                        for (int j = 0; j < coberturasList.size(); j++) {
                                            if (ajuste.getSiniestro().getPoliza().getCoberturas().get(i).getClave().equals(coberturasList.get(j).getClave())) {
                                                coberturas.add(ajuste.getSiniestro().getPoliza().getCoberturas().get(i));
                                            }
                                        }
                                    }
                                }
                            }
                            rvCoberturasDisponibles.setLayoutManager(new LinearLayoutManager(activity));
                            coberturasVehiculoAdapter.addAll(coberturas);
                            rvCoberturasDisponibles.setHasFixedSize(true);
                            rvCoberturasDisponibles.setAdapter(coberturasVehiculoAdapter);
                        },
                        throwable -> new GeneralSnackBar().largeSnackError(activity, throwable.getMessage())));
    }

    @OnClick(R.id.btnAcceptAd)
    public void validateDataAdmisionTercero() {
        VolanteVehiculoTercero volanteVehiculoTercero = new VolanteVehiculoTercero();
        volanteVehiculoTercero.setTipoVolante(tipoVolante);
        volanteVehiculoTercero.setVolanteCondicionado(volanteCondicionado.isChecked());

        if (volanteCondicionado.isChecked()) {
            if (causasCondicionamiento.get(spCondicionamiento.getSelectedItemPosition()).getId() == 0L) {
                ToastUtils.toastLongError("Seleccione una causa de condicionamiento correcta");
            } else {
                volanteVehiculoTercero.setCausaCondicionamiento(causasCondicionamiento.get(spCondicionamiento.getSelectedItemPosition()).getId());
            }
        } else {
            volanteVehiculoTercero.setCausaCondicionamiento(null);
        }
        if (tipoTerceros != null && !tipoTerceros.isEmpty()) {
            volanteVehiculoTercero.setTipoTercero(tipoTerceros.get(spTipoTerceros.getSelectedItemPosition()).getClave());
        }
        volanteVehiculoTercero.setEnvioEmail(true);
        volanteVehiculoTercero.setImpresionFisica(true);
        if (spTipoTerceros.getSelectedItem().toString().equals("Tercero Sin Seguro")) {
            volanteVehiculoTercero.setNacionalidad(nacionalidades.get(spinnerNacionalidadV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setEstado(estadosList.get(spinnerEstadoV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setMunicipio(municipios.get(spinnerMunicipioV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setColonia(colonias.get(spinnerColoniaV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setNumeroInterior(edtNumInt.getText().toString());
            if (!edtRFCBeneficiario.getText().toString().equals("")) {
                volanteVehiculoTercero.setRfcBeneficiario(edtRFCBeneficiario.getText().toString());
            } else {
                volanteVehiculoTercero.setRfcBeneficiario(null);
            }
            if (!edtNombreTercero.getText().toString().equals("")) {
                volanteVehiculoTercero.setNombre(edtNombreTercero.getText().toString());
            } else {
                edtNombreTercero.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtCalle.getText().toString().equals("")) {
                volanteVehiculoTercero.setCalle(edtCalle.getText().toString());
            } else {
                edtCalle.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtEntreCalles.getText().toString().equals("")) {
                volanteVehiculoTercero.setEntreCalle(edtEntreCalles.getText().toString());
            } else {
                edtEntreCalles.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtNumEx.getText().toString().equals("")) {
                volanteVehiculoTercero.setNumeroExterior(edtNumEx.getText().toString());
            } else {
                edtNumEx.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtTelDialog.getText().toString().equals("")) {
                volanteVehiculoTercero.setTelefono(edtTelDialog.getText().toString());
            } else {
                edtTelDialog.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtCorreoDialog.getText().toString().equals("")) {
                volanteVehiculoTercero.setCorreo(edtCorreoDialog.getText().toString());
            } else {
                edtCorreoDialog.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
        }

        if (spTipoTerceros.getSelectedItem().toString().equals("Tercero Asegurado con Otra Cía")) {
            volanteVehiculoTercero.setOtraAseguradora(otrasAseguradoras.get(spOtraCompania.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setTipoPoliza(tipoPolizas.get(spTipoPoliza.getSelectedItemPosition()).getId());

            if (!edtNumPoliza.getText().toString().equals("")) {
                volanteVehiculoTercero.setNumeroPoliza(edtNumPoliza.getText().toString());
            } else {
                edtNumPoliza.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!datePoliza.getText().toString().equals("")) {
                volanteVehiculoTercero.setFechaFinPoliza(DateUtils.date(datePoliza.getText().toString()));
            } else {
                datePoliza.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtTitularPoliza.getText().toString().equals("")) {
                volanteVehiculoTercero.setTitularPoliza(edtTitularPoliza.getText().toString());
            } else {
                edtTitularPoliza.setError(ResourceUtils.getString("value_error"));
                onError(ResourceUtils.getString("value_error"));
                return;
            }

        }
        if (requiereGrua.isChecked()) {
            volanteVehiculoTercero.setRequiereGrua(true);
            volanteVehiculoTercero.setPiezasFaltantes(generalDamageToolsParts.getDataList());
        } else {
            volanteVehiculoTercero.setRequiereGrua(false);
        }
        if (segmentador.isChecked()) {
            if (!folioSegmentacion.getText().toString().equals("")) {
                volanteVehiculoTercero.setFolioSegmentacion(folioSegmentacion.getText().toString());
            } else {
                folioSegmentacion.setError(ResourceUtils.getString("value_error"));
                return;
            }
        }
        volanteVehiculoTercero.setCentroRemate(4L);
        volanteVehiculoTercero.setCoberturas(coberturasVehiculoAdapter.getListCoberturasAgregadas());

        if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
            VolanteVehiculoTercero response = new VolanteVehiculoTercero();
            terceroAuto.getVehiculo().setVolante(response);
            sendVolanteVehiculoTercero(terceroAuto.getId(), volanteVehiculoTercero);
            listCausasCondicionamiento.clear();
        } else {
            if (coberturasVehiculoAdapter.getListCoberturasAgregadas().size() == 0) {
                new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, "No puede imprimir el volante de admisión si no tiene coberturas seleccionadas o no hay coberturas para el volante");
            } else {
                sendVolanteVehiculoTercero(terceroAuto.getId(), volanteVehiculoTercero);
                listCausasCondicionamiento.clear();
            }
        }
    }


    private Completable downloadStates() {
        return Completable.create(e -> {
            compositeDsiposable.add(catInteractor.getEstados()
                    .subscribe(estados -> {
                        estadosList = estados;
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.d(TAG, "loadStates: " + throwable.getMessage());
                    }));
        });

    }


    private void getMunicipalitiesDialog(String clave) {
        compositeDsiposable.add(catInteractor.getMunicipios(clave)
                .subscribe(this::fillDataMunicipalitiesSpinDialog, throwable -> {
                    Log.d(TAG, "loadStates: " + throwable.getMessage());
                }));
    }

    private void fillDataMunicipalitiesSpinDialog(List<Municipio> mun) {
        municipios = mun;
        List<String> municipalitiesName = new ArrayList<>();
        for (Municipio m : mun) {
            municipalitiesName.add(m.getNombre());
        }
        ArrayAdapter<String> adapterM = new ArrayAdapter<>(activity, R.layout.sp_little_item, municipalitiesName);
        adapterM.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMunicipioV.setAdapter(adapterM);
    }

    private Completable downloadTipoVolante() {
        return Completable.create(e -> {
            compositeDsiposable.add(siniestrosInteractor.getTiposVolante()
                    .subscribe(tipos -> {
                        loadTipoVolante(tipos);
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));

        });

    }

    private void loadTipoVolante(List<CatalogoClave> tipoVolantesList) {
        tipoVolantes = tipoVolantesList;
        nombreTipoVolantes = new ArrayList<>();
        for (CatalogoClave tv : tipoVolantesList) {
            nombreTipoVolantes.add(tv.getNombre());
        }
    }

    private Completable downloadTipoTerceros() {
        return Completable.create(e -> {
            compositeDsiposable.add(siniestrosInteractor.getTiposTerceroAuto()
                    .subscribe(tipos -> {
                        loadTipoTerceros(tipos);
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));
        });

    }


    private void loadTipoTerceros(List<CatalogoClave> tiposAutosList) {
        tipoTerceros = tiposAutosList;
        nombreTipoTerceros = new ArrayList<>();
        for (CatalogoClave ta : tiposAutosList) {
            nombreTipoTerceros.add(ta.getNombre());
        }
    }


    private void fillSpinnerMunicipalities(List<Municipio> municipiosList) {
        municipios = municipiosList;
        ArrayList<String> nameMunicipalities = new ArrayList<>();
        for (Municipio m : municipiosList) {
            nameMunicipalities.add(m.getNombre());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.sp_little_item, nameMunicipalities);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMunicipioV.setAdapter(adapter);
    }

    private Completable downloadOtrasAseguradoras() {
        return Completable.create(e -> {
            compositeDsiposable.add(catInteractor.getOtrasAseguradoras()
                    .subscribe(insures -> {
                        otrasAseguradoras = insures;
                        for (int i = 0; i < otrasAseguradoras.size(); i++) {
                            nombreOtrasAseguradoras.add(otrasAseguradoras.get(i).getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        Toast.makeText(activity, throwable.getMessage(), Toast.LENGTH_LONG).show();
                        e.tryOnError(throwable);
                    }));
        });

    }



    private Completable downloadNacionalidades() {
        return Completable.create(e -> {
            compositeDsiposable.add(siniestrosInteractor.getNacionalidades()
                    .subscribe(nacionalidadesList -> {
                        nacionalidades = nacionalidadesList;
                        for (int i = 0; i < nacionalidades.size(); i++) {
                            nombreNacionalidades.add(nacionalidades.get(i).getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));
        });

    }

    private Completable downloadPolizas() {
        return Completable.create(e -> {
            compositeDsiposable.add(siniestrosInteractor.getTiposPoliza()
                    .subscribe(response -> {
                        tipoPolizas = response;
                        for (int i = 0; i < danos.size(); i++) {
                            nombreDanos.add(danos.get(i).getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, "conductor: " + throwable.getMessage());
                        e.tryOnError(throwable);
                    }));
        });


    }

    private void loadVolantes(String tipoVolante) {
        if (ajuste.getSiniestro().getPoliza() != null) {
            compositeDsiposable.add(siniestrosInteractor.getProductoCashflow(
                    ajuste.getSiniestro().getPoliza().getProductoComercial(),
                    ajuste.getSiniestro().getPoliza().getProductoTecnico())
                    .subscribe(cashflow -> {
                        loadTiposVolantes(tipoVolante, cashflow);
                    }, throwable -> {
                        loadTiposVolantes(tipoVolante, false);
                    }));
        } else {
            loadTiposVolantes(tipoVolante, false);
        }
    }

    private void loadTiposVolantes(String tipoVolante, Boolean isCashflow) {
        BiFunction<List<String>, List<CatalogoClave>, List<String>> filtrarNombres = (claves, volantes) -> {
            List<String> filtered = new ArrayList<>(volantes.size());
            for (CatalogoClave volante : volantes) {
                if (claves.contains(volante.getClave())) {
                    filtered.add(volante.getNombre());
                }
            }
            return filtered;
        };
        Function<List<CatalogoClave>, List<String>> getNombres = volantes -> {
            List<String> filtered = new ArrayList<>(volantes.size());
            for (CatalogoClave volante : volantes) {
                filtered.add(volante.getNombre());
            }
            return filtered;
        };

        compositeDsiposable.add(siniestrosInteractor.getTiposVolante()
                .subscribe(volantes -> {
                    List<String> nombres;
                    switch (tipoVolante) {
                        case "T":
                            if ("TAG".equals(tipoTerceros.get(spTipoTerceros.getSelectedItemPosition()).getClave())) {
                                nombres = filtrarNombres.apply(Arrays.asList(tipoVolante), volantes);
                            } else {
                                nombres = filtrarNombres.apply(Arrays.asList("T", "SIPAC"), volantes);
                            }
                            break;
                        case "D":
                            nombres = filtrarNombres.apply(Arrays.asList(tipoVolante), volantes);
                            break;
                        default:
                            nombres = getNombres.apply(volantes);
                            break;
                    }
                    if (isCashflow){
                        CollectionsUtils.removeIf(volantes, volante-> "SIPAC".equals(volante.getClave()));
                        CollectionsUtils.removeIf(nombres, "SIPAC"::equals);
                    }
                    adapter2.clear();
                    adapter2.addAll(nombres);
                    spTipoVolante.setAdapter(adapter2);
                }, throwable -> {
                    Log.e(TAG, "Error al cargar tipos de volantes", throwable);
                    new GeneralSnackBar().largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
                }));
    }

    public Completable downloadOrdenDireta() {
        return Completable.create(e -> {
            compositeDsiposable.add(siniestrosInteractor.getVolantesTerceroNoAuto()
                    .subscribe(ordenes -> {
                        for (CatalogoClave object : ordenes) {
                            ordenDirectas.add(object);
                        }
                        e.onComplete();
                    }, e::tryOnError));
        });

    }

    private Completable downloadCausasCondicionamiento() {
        return Completable.create(e -> {
            compositeDsiposable.add(siniestrosInteractor.getCondicionamientosPoliza()
                    .subscribe(condicionamientos -> {
                        causasCondicionamiento = condicionamientos;
                        causasCondicionamiento.add(0, new CatalogoId(0L, "Seleccione una causa de condicionamiento por favor"));
                        e.onComplete();
                        Log.d(TAG, "downloadCausasCondicionamiento: " + condicionamientos.size());
                    }, throwable -> {
                        e.tryOnError(throwable);
                        Log.e(TAG, "downloadCausasCondicionamiento: ", throwable);
                    }));
        });

    }

    private void downloadColonias(String estado, String municipio) {
        compositeDsiposable.add(catInteractor.getColonias(estado, municipio)
                .subscribe(response -> {
                    colonias.clear();
                    colonias = response;
                    List<String> coloniesName = new ArrayList<>();
                    for (ColoniaResumen colonies : colonias) {
                        coloniesName.add(colonies.getNombre());
                    }
                    ArrayAdapter<String> adapterColonies = new ArrayAdapter<>(activity, R.layout.spinner_item, coloniesName);
                    adapterColonies.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerColoniaV.setAdapter(adapterColonies);
                }, throwable -> {
                    Log.d(TAG, "loadStates: " + throwable.getMessage());
                }));

    }


    @OnClick(R.id.btnCancelAd)
    public void dismiss() {
        getDialog().dismiss();
    }


    public void onError(String error) {
        new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, error);
    }

    public void onFailure(Throwable t) {
        new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, ErrorUtils.getErrorMessage(t));
    }


    public void printPDf(String url) {
        Intent pdfViewer = new Intent(activity, PrinterActivity.class);
        pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, url);
        dismiss();
        startActivity(pdfViewer);
    }


    private void actualizarVolante() {
        Intent intent = new Intent(Constants.FIRMA_DIGITAL);
        intent.putExtra("id", terceroAuto.getId());
        getActivity().sendBroadcast(intent);
    }

    private boolean isUrl(String url){
        return url != null && !url.equals(Constants.FIRMA_DIGITAL);
    }

    private boolean isFolioAmis(TerceroAuto tercero){
        return tercero.getFolioAmis() != null;
    }


    public void sendVolanteVehiculoTercero(long tercero, VolanteVehiculoTercero volanteVehiculo) {
        String idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        compositeDsiposable.add(
                amisInteractor.reFolear(idAjuste)
                        .andThen(firmaDigitalBuilder.get(ajuste.getId(), tercero))
                        .flatMap(firma -> {
                            volanteVehiculo.setValeDigital(firma.getValeDigital());
                            volanteVehiculo.setFirmaDigital(firma.getFirmaDigital());
                            return volanteService.generateTerceroAutoVehiculo(idAjuste, tercero, volanteVehiculo);
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(archivo -> {
                            if(isUrl(archivo.getUrl())) {
                                printPDf(archivo.getUrl());
                            } else {
                                if (isFolioAmis(terceroAuto)) {
                                    if(archivo.isEnviado()) {
                                        ToastUtils.toastLongSuccess("SIPAC registrado en AMIS");
                                    } else {
                                        ToastUtils.toastLongError("SIPAC no se pudo registrar en AMIS.\n\nFavor de compartirlo Offline por QR.");
                                        generarQR();
                                    }

                                } else {
                                    ToastUtils.toastLongSuccess("SIPAC registrado");
                                }
                                actualizarVolante();
                                dismiss();
                            }
                        }, throwable -> {
                            Log.e(TAG, "Error al crear volante tercero vehiculo", throwable);
                            onFailure(throwable);
                        }));
    }

    public void generarQR() {
        compositeDsiposable.add(firmaDigitalBuilder.get(ajuste.getId(), terceroAuto.getId())
                .doOnSubscribe(it -> LoadingIndicator.start())
                .doFinally(LoadingIndicator::finish)
                .flatMap(firmaDigital -> firmaDigitalBuilder.encriptar(firmaDigital, terceroAuto.getFolioAmis()))
                .map(modulo -> {
                    String[] qr = modulo.split("\\|");
                    String title = ResourceUtils.getString("amis_modulo_" + qr[1]);
                    Log.d(TAG, title + ": " + modulo);
                    QrData data = new QrData();
                    data.setTitulo(title);
                    data.setQr(modulo);
                    return data;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(QrUtils::show, throwable -> {
                    Log.e(TAG, "Error al mostrar QRs d información", throwable);
                    new GeneralSnackBar().updownMakeSnackColorError(view, linearLayout, ErrorUtils.getErrorMessage(throwable));


                }));

    }


}
