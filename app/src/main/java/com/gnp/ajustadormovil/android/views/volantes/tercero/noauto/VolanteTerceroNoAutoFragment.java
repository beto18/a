package com.gnp.ajustadormovil.android.views.volantes.tercero.noauto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.catalogos.TprInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.catalogos.CatalogoClave;
import com.gnp.ajustadormovil.android.models.catalogos.cat.CatalogoClaveInfo;
import com.gnp.ajustadormovil.android.models.catalogos.cat.ColoniaResumen;
import com.gnp.ajustadormovil.android.models.catalogos.cat.Municipio;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Contratista;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.ErrorUtils;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.volantes.VolanteCoberturaAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseActivity;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;
import com.gnp.ajustadormovil.android.views.volantes.VolanteActivity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

import static android.view.View.GONE;

public class VolanteTerceroNoAutoFragment extends BaseDialogFragment implements VolanteTerceroNoAutoMvpView {

    private View mView;
    private VolanteCoberturaAdapter coberturasNoAutoAdapter;
    private VolanteTerceroNoAutoMvpPresenter presenter;
    private Ajuste ajuste;
    private List<CatalogoClave> tipoVolantesV = new ArrayList<>();
    private ArrayList<String> nameVolantesTNA = new ArrayList<>();
    private List<CatalogoClaveInfo> estados = new ArrayList<>();
    private List<Municipio> municipios = new ArrayList<>();
    private List<ColoniaResumen> colonias = new ArrayList<>();
    private List<Cobertura> coberturasList = new ArrayList<>();
    private List<String> nombreNoDeducibles = new ArrayList<>();
    private List<CatalogoId> noDeducibles = new ArrayList<>();
    private List<CatalogoClave> nacionalidades = new ArrayList<>();
    private List<String> nombreNacionalidades = new ArrayList<>();
    private List<Contratista> contratistas = new ArrayList<>();
    private List<String> nombresContratistas = new ArrayList<>();
    private int position;
    private OnToolbarListener mListener;
    private List<TerceroNoAuto> noAutosList = new ArrayList<>();
    private RelativeLayout message_container;
    private TextView tvMessage;
    private SiniestrosInteractor siniestrosInteractor;
    private CatInteractor catInteractor;
    private TprInteractor tprService;
    private PolizaCobertura coberturaSelected;
    private EditText edtMonto, edtObservaciones, etObservaciones;
    private EditText edtRFCBeneficiario, edtNombreTercero, Calle, EntreCalles, NumEx, NumInt, TelDialog, CorreoDialog, telDialogP, correoDialogP, etKm, etCP;
    private Spinner spTipoVolante, spinnerEstadoV, spinnerNoDeducible, spinnerMunicipioV, spinnerColoniaV, spContratista, spinnerNacionalidadV;
    private CheckBox aplicaDeducible;
    private TerceroNoAuto tercero;
    private Activity activity;
    private VolanteInteractor volanteService;
    private LinearLayout linearLayout;
    private static String TAG = VolanteTerceroNoAutoFragment.class.getSimpleName();
    private NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("es-MX"));
    private String current = "";
    private CompositeDisposable compositeDisposable;
    private AjusteInteractor ajusteInteractor;
    private String idAjuste;


    public static VolanteTerceroNoAutoFragment newInstance(TerceroNoAuto tercero) {
        VolanteTerceroNoAutoFragment instance = new VolanteTerceroNoAutoFragment();
        instance.tercero = tercero;
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (VolanteActivity) context;
        this.activity = (AppCompatActivity) context;


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialog);
        compositeDisposable = ((BaseActivity) activity).getCompositeDisposable();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        catInteractor = CatalogoInteractorsFactory.getCatInteractor();
        tprService = CatalogoInteractorsFactory.getTprService();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        presenter = new VolanteTerceroNoAutoPresenter(compositeDisposable);
        presenter.onAttach(this);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.dialog_content_volante_no_auto, container, false);
        ButterKnife.bind(this, mView);

        return mView;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAjuste();
    }


    private void getAjuste() {
        compositeDisposable.add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(ajuste -> {
                    this.ajuste = ajuste;
                    compositeDisposable.add(downloadCatalogs()
                            .subscribe(() -> {
                                newDialogVolanteAdmision(mView);
                            }, throwable -> {
                                Log.e(TAG, "Error al obtener los catalogos", throwable);
                                new GeneralSnackBar().largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
                            }));
                }, throwable -> {
                    Log.e(TAG, "Error al obtener el ajuste", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        new GeneralSnackBar().largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
                    }
                }));
    }

    @Override
    public void onShow() {

    }

    @Override
    public void onDismiss() {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onFailure(Throwable t) {
        new GeneralSnackBar().updownMakeSnackColorError(mView, linearLayout, ErrorUtils.getErrorMessage(t));
    }

    @Override
    public void printPDf(String url) {
        if (url != null && !url.isEmpty()) {
            Intent pdfViewer = new Intent(activity, PrinterActivity.class);
            pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, url);
            dismissDialog();
            startActivity(pdfViewer);
        } else {
            dismissDialog();
            new GeneralSnackBar().largeSnackError(activity, ResourceUtils.getString(R.string.url_error_message));

        }
    }


    @Override
    public void onDestroyView() {
        mListener.onDefaulTitle();
        super.onDestroyView();
    }


    public Completable downloadCatalogs() {
        return Completable.mergeArrayDelayError(
                downloadCoberturasVolante(),
                downloadNacionalidades(),
                downloadContratistas(),
                downloadTipoVolanteNoAuto(),
                callCatalogoNoDeducible()
        );
    }


    @SuppressLint("CutPasteId")
    private void newDialogVolanteAdmision(View view) {

        aplicaDeducible = view.findViewById(R.id.cbDeducible);
        edtMonto = view.findViewById(R.id.edtMontoVehiculoTecero);
        edtRFCBeneficiario = view.findViewById(R.id.edtBeneficiario);
        edtNombreTercero = view.findViewById(R.id.edtNombreTercero);
        Calle = view.findViewById(R.id.edtCalle);
        EntreCalles = view.findViewById(R.id.edtEntreCalle);
        NumEx = view.findViewById(R.id.edtNumEx);
        NumInt = view.findViewById(R.id.edtNumIn);
        TelDialog = view.findViewById(R.id.edtPhoneDialog);
        CorreoDialog = view.findViewById(R.id.edtCorreoDialog);
        edtObservaciones = view.findViewById(R.id.edtobservacionesDialog);
        telDialogP = view.findViewById(R.id.edtPhoneDialogP);
        correoDialogP = view.findViewById(R.id.edtCorreoDialogP);
        etKm = view.findViewById(R.id.etKmD);
        etCP = view.findViewById(R.id.etCPD);
        etObservaciones = view.findViewById(R.id.etObservaciones);
        linearLayout = view.findViewById(R.id.linearLayout);

        spinnerEstadoV = view.findViewById(R.id.spnStateDialog);
        spinnerMunicipioV = view.findViewById(R.id.spnMunicipalitiesDialog);
        spinnerColoniaV = view.findViewById(R.id.spnColoniesDialog);

        edtMonto.addTextChangedListener(watcher);

        view.findViewById(R.id.orden_container).setVisibility(GONE);
        view.findViewById(R.id.coberturas_content).setVisibility(GONE);
        view.findViewById(R.id.directa_content).setVisibility(View.GONE);
        view.findViewById(R.id.cotent_tradicional).setVisibility(GONE);

        List<PolizaCobertura> coberturas = new ArrayList<>();

        RecyclerView rvCoberturasDisponibles = view.findViewById(R.id.rvCoberturaNA);
        coberturasNoAutoAdapter = new VolanteCoberturaAdapter(((position, v) -> coberturaSelected = coberturasNoAutoAdapter.getCoberturaSelected(position)));
        if (ajuste.getSiniestro().getPoliza() != null) {
            if (ajuste.getSiniestro().getPoliza().getCoberturas() != null) {
                for (int i = 0; i < ajuste.getSiniestro().getPoliza().getCoberturas().size(); i++) {
                    for (int j = 0; j < coberturasList.size(); j++) {
                        if (ajuste.getSiniestro().getPoliza().getCoberturas().get(i).getClave().equals(coberturasList.get(j).getClave())) {
                            coberturas.add(ajuste.getSiniestro().getPoliza().getCoberturas().get(i));
                        }
                    }
                }
            } else {
                view.findViewById(R.id.coberturas_na_container).setVisibility(GONE);
            }
        } else {
            view.findViewById(R.id.coberturas_na_container).setVisibility(GONE);
        }
        rvCoberturasDisponibles.setLayoutManager(new LinearLayoutManager(activity));
        rvCoberturasDisponibles.setAdapter(coberturasNoAutoAdapter);
        rvCoberturasDisponibles.setHasFixedSize(true);
        coberturasNoAutoAdapter.addAll(coberturas);

        spTipoVolante = view.findViewById(R.id.spTipoVolanteT);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(activity, R.layout.sp_little_item, nameVolantesTNA);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoVolante.setAdapter(adapter2);

        spinnerNacionalidadV = view.findViewById(R.id.spNacionalidad);
        spContratista = view.findViewById(R.id.spContratistaT);
        spinnerNoDeducible = view.findViewById(R.id.spRazonDeducible);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.sp_little_item, nombreNoDeducibles);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNoDeducible.setAdapter(adapter);
        AdapterSpinnerGeneral<Contratista> adapterC = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, contratistas, Contratista::getNombre);
        spContratista.setAdapter(adapterC);
        ArrayAdapter<String> adapterN = new ArrayAdapter<>(activity, R.layout.sp_little_item, nombreNacionalidades);
        adapterN.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNacionalidadV.setAdapter(adapterN);
        spinnerNoDeducible = view.findViewById(R.id.spRazonDeducible);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerNoDeducible.setAdapter(adapter);
        aplicaDeducible.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                view.findViewById(R.id.deducibleContainer).setVisibility(GONE);
            } else {
                view.findViewById(R.id.deducibleContainer).setVisibility(View.VISIBLE);
            }
        });

        spTipoVolante.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                switch (spTipoVolante.getSelectedItem().toString()) {
                    case "Pronto Pago":
                        view.findViewById(R.id.directa_content).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.cotent_tradicional).setVisibility(GONE);
                        break;
                    case "Tradicional":
                        view.findViewById(R.id.directa_content).setVisibility(View.GONE);
                        view.findViewById(R.id.cotent_tradicional).setVisibility(View.VISIBLE);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        loadStates();

    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().isEmpty() && !s.toString().equals(current)) {
                edtMonto.removeTextChangedListener(this);
                String cleanString = s.toString().trim().replaceAll("[$,.]", "");
                double parsed = Double.parseDouble(cleanString);
                String formatted = numberFormat.format((parsed / 100));
                current = formatted;
                edtMonto.setText(formatted);
                edtMonto.setSelection(formatted.length());
                edtMonto.addTextChangedListener(this);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private Completable callCatalogoNoDeducible() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getRazonesNoDeducible()
                    .subscribe(catalogoIds -> {
                        loadNoDeducibles(catalogoIds);
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));

        });
    }


    private void loadNoDeducibles(List<CatalogoId> noDeduciblesList) {
        CatalogoId noDeducible = new CatalogoId();
        noDeducible.setId(0L);
        noDeducible.setNombre(ResourceUtils.getString(R.string.message_no_deducible_option));
        noDeducibles = noDeduciblesList;
        noDeducibles.add(0, noDeducible);
        nombreNoDeducibles = new ArrayList<>();
        for (CatalogoId nd : noDeduciblesList) {
            nombreNoDeducibles.add(nd.getNombre());
        }
    }

    private Completable downloadCoberturasVolante() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getCoberturasVolante(10, ajuste.getSiniestro().getTipoSiniestro().getClave())
                    .subscribe(categoriaVolantesCoberturas -> {
                        coberturasList = categoriaVolantesCoberturas;
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));
        });

    }

    private Completable downloadNacionalidades() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getNacionalidades()
                    .subscribe(nacionalidadesList -> {
                        nacionalidades = nacionalidadesList;
                        for (int i = 0; i < nacionalidades.size(); i++) {
                            nombreNacionalidades.add(nacionalidades.get(i).getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));
        });

    }


    private void loadStates() {
        compositeDisposable.add(catInteractor.getEstados()
                .subscribe(this::fillStatesDialog, throwable -> {
                    Log.e(TAG, "Error al obtener los estados", throwable);
                    new GeneralSnackBar().largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
                }));
    }

    private void fillStatesDialog(List<CatalogoClaveInfo> sta) {
        estados = sta;
        List<String> statesName = new ArrayList<>();
        for (CatalogoClaveInfo state : sta) {
            statesName.add(state.getNombre());
        }
        AdapterSpinnerGeneral<CatalogoClaveInfo> adapterS = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, estados, CatalogoClaveInfo::getNombre);
        spinnerEstadoV.setAdapter(adapterS);

        spinnerEstadoV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getMunicipalitiesDialog(estados.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getMunicipalitiesDialog(String clave) {
        compositeDisposable.add(catInteractor.getMunicipios(clave)
                .subscribe(this::fillDataMunicipalitiesSpinDialog, throwable -> {
                    Log.e(TAG, "Error al obtener los municipios", throwable);
                    new GeneralSnackBar().largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
                }));
    }

    private void fillDataMunicipalitiesSpinDialog(List<Municipio> mun) {
        municipios = mun;
        List<String> municipalitiesName = new ArrayList<>();
        for (Municipio m : mun) {
            municipalitiesName.add(m.getNombre());
        }
        AdapterSpinnerGeneral<Municipio> adapterM = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, municipios, Municipio::getNombre);
        spinnerMunicipioV.setAdapter(adapterM);

        spinnerMunicipioV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadColonias(estados.get(spinnerEstadoV.getSelectedItemPosition()).getClave(), municipios.get(position).getClave());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void downloadColonias(String estado, String municipio) {
        compositeDisposable.add(catInteractor.getColonias(estado, municipio)
                .subscribe(this::fillColoniesSpinDialog, throwable -> {
                    Log.e(TAG, "Error al obtener las colonias", throwable);
                    new GeneralSnackBar().largeSnackError(activity, ErrorUtils.getErrorMessage(throwable));
                }));

    }

    private void fillColoniesSpinDialog(List<ColoniaResumen> col) {
        colonias = col;
        List<String> coloniesName = new ArrayList<>();
        for (ColoniaResumen c : col) {
            coloniesName.add(c.getNombre());
        }
        AdapterSpinnerGeneral<ColoniaResumen> adapter2 = new AdapterSpinnerGeneral<>(activity, R.layout.sp_little_item, colonias, ColoniaResumen::getNombre);

        spinnerColoniaV.setAdapter(adapter2);
    }


    private Completable downloadContratistas() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getContratistas()
                    .subscribe(contratista -> {
                        contratistas = contratista;
                        for (Contratista c : contratistas) {
                            nombresContratistas.add(c.getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                        e.tryOnError(throwable);
                    }));
        });

    }

    private Completable downloadTipoVolanteNoAuto() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getVolantesTerceroNoAuto()
                    .subscribe(volantesTerceroNoAutos -> {
                        tipoVolantesV = volantesTerceroNoAutos;
                        for (CatalogoClave vTNO : tipoVolantesV) {
                            nameVolantesTNA.add(vTNO.getNombre());
                        }
                        e.onComplete();
                    }, throwable -> {
                        e.tryOnError(throwable);
                        new GeneralSnackBar().largeSnackError(activity, throwable.getMessage());
                    }));
        });

    }


    @OnClick(R.id.btnAceptar)
    public void putVolante() {
        VolanteTerceroNoAuto volanteVehiculoTercero = new VolanteTerceroNoAuto();
        volanteVehiculoTercero.setTipoVolante(tipoVolantesV.get(spTipoVolante.getSelectedItemPosition()).getClave());
        volanteVehiculoTercero.setEnvioEmail(true);
        volanteVehiculoTercero.setImpresionFisica(true);
        volanteVehiculoTercero.setCoberturas(coberturasNoAutoAdapter.getListCoberturasAgregadas());

        if (!edtMonto.getText().toString().equals("")) {
            if (!edtMonto.getText().toString().equals("$.00")) {
                volanteVehiculoTercero.setMonto(Double.parseDouble(
                        edtMonto.getText().toString().trim().replaceAll("[$,.]", "")));
            } else {
                volanteVehiculoTercero.setMonto(0.0);
            }
        } else {
            edtMonto.setError(ResourceUtils.getString("value_error"));
            return;
        }
        if (spTipoVolante.getSelectedItem().toString().equals("Pronto Pago")) {
            volanteVehiculoTercero.setNacionalidad(nacionalidades.get(spinnerNacionalidadV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setEstado(estados.get(spinnerEstadoV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setMunicipio(municipios.get(spinnerMunicipioV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setColonia(colonias.get(spinnerColoniaV.getSelectedItemPosition()).getClave());
            volanteVehiculoTercero.setKilometro(etKm.getText().toString());
            volanteVehiculoTercero.setCodigoPostal(etCP.getText().toString());
            volanteVehiculoTercero.setObservaciones(etObservaciones.getText().toString());
            volanteVehiculoTercero.setTelefono(telDialogP.getText().toString());
            volanteVehiculoTercero.setCorreo(correoDialogP.getText().toString());
            volanteVehiculoTercero.setRfcBeneficiario(edtRFCBeneficiario.getText().toString());
            volanteVehiculoTercero.setNumeroInterior(NumInt.getText().toString());
            if (!Calle.getText().toString().equals("")) {
                volanteVehiculoTercero.setCalle(Calle.getText().toString());
            } else {
                Calle.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!EntreCalles.getText().toString().equals("")) {
                volanteVehiculoTercero.setEntreCalles(EntreCalles.getText().toString());
            } else {
                EntreCalles.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!NumEx.getText().toString().equals("")) {
                volanteVehiculoTercero.setNumeroExterior(NumEx.getText().toString());
            } else {
                NumEx.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtRFCBeneficiario.getText().toString().equals("")) {
                volanteVehiculoTercero.setRfcBeneficiario(edtRFCBeneficiario.getText().toString());
            } else {
                edtRFCBeneficiario.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!edtNombreTercero.getText().toString().equals("")) {
                volanteVehiculoTercero.setNombre(edtNombreTercero.getText().toString());
            } else {
                edtNombreTercero.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!telDialogP.getText().toString().equals("")) {
                volanteVehiculoTercero.setTelefono(telDialogP.getText().toString());
            } else {
                telDialogP.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!correoDialogP.getText().toString().equals("")) {
                volanteVehiculoTercero.setCorreo(correoDialogP.getText().toString());
            } else {
                correoDialogP.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
                presenter.sendData(tercero, volanteVehiculoTercero);
            } else {
                if (coberturasNoAutoAdapter.getListCoberturasAgregadas().isEmpty() && coberturaSelected == null) {
                    ToastUtils.toastLong(ResourceUtils.getString(R.string.volante_coberturas_required));
                } else {
                    presenter.sendData(tercero, volanteVehiculoTercero);
                }
            }

        } else {
            volanteVehiculoTercero.setContratista(contratistas.get(spContratista.getSelectedItemPosition()).getId());
            volanteVehiculoTercero.setNombre(tercero.getAfectado().getNombre());
            if (!edtObservaciones.getText().toString().equals("")) {
                volanteVehiculoTercero.setObservaciones(edtObservaciones.getText().toString());
            } else {
                edtObservaciones.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!TelDialog.getText().toString().equals("")) {
                volanteVehiculoTercero.setTelefono(TelDialog.getText().toString());
            } else {
                TelDialog.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (!CorreoDialog.getText().toString().equals("")) {
                volanteVehiculoTercero.setCorreo(CorreoDialog.getText().toString());
            } else {
                CorreoDialog.setError(ResourceUtils.getString("value_error"));
                return;
            }
            if (ajuste.getSiniestro().getIdPresiniestro() != null && ajuste.getSiniestro().getIdSiniestro() == null) {
                presenter.sendData(tercero, volanteVehiculoTercero);
            } else {
                if (coberturasNoAutoAdapter.getListCoberturasAgregadas().isEmpty() && coberturaSelected == null) {
                    new GeneralSnackBar().updownMakeSnackColorError(mView, linearLayout, ResourceUtils.getString(R.string.volante_coberturas_required));
                } else {
                    presenter.sendData(tercero, volanteVehiculoTercero);
                }
            }
        }
    }


    @OnClick(R.id.btnCancel)
    public void dismissDialog() {
        getDialog().dismiss();
    }
}
