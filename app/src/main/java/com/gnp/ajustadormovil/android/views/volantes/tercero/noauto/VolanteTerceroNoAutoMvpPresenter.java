package com.gnp.ajustadormovil.android.views.volantes.tercero.noauto;

import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

public interface VolanteTerceroNoAutoMvpPresenter<V extends VolanteTerceroNoAutoMvpView> extends MvpPresenter<V> {
    void sendData(TerceroNoAuto terceroNoAuto, VolanteTerceroNoAuto pasajeros);
}
