package com.gnp.ajustadormovil.android.views.volantes.tercero.noauto;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroNoAutoInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroNoAuto;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.VolanteTerceroNoAuto;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class VolanteTerceroNoAutoPresenter<V extends VolanteTerceroNoAutoMvpView> extends BasePresenter<V>
        implements VolanteTerceroNoAutoMvpPresenter<V> {

    private List<TerceroNoAuto> noAutos;
    private TerceroNoAutoInteractor tercerosNoAutoService;
    private VolanteInteractor volanteService;

    public VolanteTerceroNoAutoPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        tercerosNoAutoService = OrquestadorInteractorFactory.getTerceroNoAutoService();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Override
    public void sendData(TerceroNoAuto terceroNoAuto, VolanteTerceroNoAuto pasajeros) {
        getmCompositeDisposable().add(volanteService.generateTerceroNoAuto(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL),
                terceroNoAuto.getId(), pasajeros)
                .subscribe(pdfLink -> {
                    getMvpView().onDismiss();
                    if (pdfLink != null){
                        getMvpView().printPDf(pdfLink.getUrl());
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al crear volante tercero no auto " , throwable);
                    getMvpView().onFailure(throwable);
                }));

    }


}
