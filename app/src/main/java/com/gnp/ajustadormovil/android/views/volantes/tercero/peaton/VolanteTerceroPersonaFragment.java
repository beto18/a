package com.gnp.ajustadormovil.android.views.volantes.tercero.peaton;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.gnp.ajustadormovil.android.R;
import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.error.ResourceNotFoundException;
import com.gnp.ajustadormovil.android.interactors.catalogos.CatalogoInteractorsFactory;
import com.gnp.ajustadormovil.android.interactors.catalogos.SiniestrosInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.AjusteInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.CatalogoId;
import com.gnp.ajustadormovil.android.models.catalogos.siniestros.Cobertura;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.Ajuste;
import com.gnp.ajustadormovil.android.models.orquestador.ajuste.PolizaCobertura;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.utils.Constants;
import com.gnp.ajustadormovil.android.utils.core.ResourceUtils;
import com.gnp.ajustadormovil.android.views.utils.components.GeneralSnackBar;
import com.gnp.ajustadormovil.android.views.utils.components.ToastUtils;
import com.gnp.ajustadormovil.android.views.commons.adapters.AdapterSpinnerGeneral;
import com.gnp.ajustadormovil.android.views.volantes.VolanteCoberturaAdapter;
import com.gnp.ajustadormovil.android.views.commons.base.BaseDialogFragment;
import com.gnp.ajustadormovil.android.views.commons.events.OnToolbarListener;
import com.gnp.ajustadormovil.android.views.utils.impresora.PrinterActivity;
import com.gnp.ajustadormovil.android.views.volantes.VolanteActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

import static android.view.View.GONE;

public class VolanteTerceroPersonaFragment extends BaseDialogFragment implements VolanteTerceroPersonaMvpView {

    private View mView;
    private List<Cobertura> coberturasList = new ArrayList<>();
    private ArrayList<String> listCoberturasAgregadas = new ArrayList<>();
    public static final String TAG = "TAG";
    private List<CatalogoId> noDeducibles = new ArrayList<>();
    private Spinner spinnerNoDeducible;
    private CheckBox aplicaDeducible;
    private VolanteActivity mActivity;
    private VolanteTerceroPersonaMvpPresenter presenter;
    private VolanteCoberturaAdapter coberturaAdapter;
    private OnToolbarListener mListener;
    private SiniestrosInteractor siniestrosInteractor;
    private PolizaCobertura coberturaSeleccionada;
    private TerceroPersona tercero;
    private CompositeDisposable compositeDisposable;
    private RecyclerView rvCoberturasDisponibles;
    private AjusteInteractor ajusteInteractor;
    private String idAjuste;
    private Ajuste ajuste;


    public static VolanteTerceroPersonaFragment newInstance(TerceroPersona tercero) {
        VolanteTerceroPersonaFragment instance = new VolanteTerceroPersonaFragment();
        instance.tercero = tercero;
        return instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (VolanteActivity) getActivity();
        mListener = (VolanteActivity) context;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomThemeDialog);
        siniestrosInteractor = CatalogoInteractorsFactory.getSiniestrosInteractor();
        presenter = new VolanteTerceroPersonaPresenter(mActivity.getCompositeDisposable());
        compositeDisposable = (mActivity).getCompositeDisposable();
        ajusteInteractor = OrquestadorInteractorFactory.getAjusteInteractor();
        idAjuste = AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL);
        presenter.onAttach(this);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.content_dialog_volante_medico, container, false);
        ButterKnife.bind(this,mView);
        initView();
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAjuste();
    }


    private void getAjuste(){
        compositeDisposable.add(ajusteInteractor.getAjuste(idAjuste)
                .subscribe(ajuste -> {
                    this.ajuste = ajuste;
                    compositeDisposable.add(downloadCatalogos()
                            .subscribe(this::showDialogMedico,
                                    throwable -> {
                                        Log.e(TAG, "Error al descargar los  catalogos", throwable);
                                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);

                                    }));

                }, throwable -> {
                    Log.e(TAG, "Error al descargar los  catalogos", throwable);
                    if (!(throwable instanceof ResourceNotFoundException)) {
                        new GeneralSnackBar().largeSnackError(getActivity(), throwable);
                    }
                }));
    }

    private void initView() {
        aplicaDeducible = mView.findViewById(R.id.deducible);
        spinnerNoDeducible = mView.findViewById(R.id.sp_no_deducible);
        rvCoberturasDisponibles = mView.findViewById(R.id.rvCoberturasDisponibles);
        rvCoberturasDisponibles.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvCoberturasDisponibles.setHasFixedSize(true);

    }

    private void showDialogMedico() {
        listCoberturasAgregadas.clear();

        List<PolizaCobertura> coberturas = new ArrayList<>();
        if (ajuste.getSiniestro().getPoliza() != null) {
            if (ajuste.getSiniestro().getPoliza().getCoberturas() != null) {
                for (int i = 0; i < ajuste.getSiniestro().getPoliza().getCoberturas().size(); i++) {
                    for (int j = 0; j < coberturasList.size(); j++) {
                        if (ajuste.getSiniestro().getPoliza().getCoberturas().get(i).getClave().equals(coberturasList.get(j).getClave())) {
                            coberturas.add(ajuste.getSiniestro().getPoliza().getCoberturas().get(i));
                        }
                    }
                }
            }
        }

        Log.d(TAG, "imprimir: " + coberturas);
        coberturaAdapter = new VolanteCoberturaAdapter((position, v) -> { });
        rvCoberturasDisponibles.setAdapter(coberturaAdapter);
        coberturaAdapter.addAll(coberturas);
        aplicaDeducible.setVisibility(GONE);
        mView.findViewById(R.id.view).setVisibility(View.GONE);
        mView.findViewById(R.id.armadora).setVisibility(View.GONE);
        Log.d(TAG, "imprimir: " + coberturas);
        AdapterSpinnerGeneral<CatalogoId> adapter = new AdapterSpinnerGeneral<>(getActivity(), R.layout.sp_little_item, noDeducibles, CatalogoId::getNombre);
        mView.findViewById(R.id.armadora).setVisibility(View.GONE);
        mView.findViewById(R.id.sp_no_deducible).setVisibility(View.GONE);
        spinnerNoDeducible.setAdapter(adapter);


    }

    private Completable downloadCatalogos() {
        return Completable.mergeArrayDelayError(
                downloadCoberturasVolante(),
                callCatalogoNoDeducible());
    }

    private Completable downloadCoberturasVolante() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getCoberturasVolante(15, ajuste.getSiniestro().getTipoSiniestro().getClave())
                    .subscribe(categoriaVolantesCoberturas -> {
                                coberturasList = categoriaVolantesCoberturas;
                                e.onComplete();
                            },
                            throwable -> {
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable.getMessage());
                                e.tryOnError(throwable);
                            }));
        });


    }

    private Completable callCatalogoNoDeducible() {
        return Completable.create(e -> {
            compositeDisposable.add(siniestrosInteractor.getRazonesNoDeducible()
                    .subscribe(noDeducibles -> {
                                CatalogoId noDeducible = new CatalogoId();
                                noDeducible.setId(0L);
                                noDeducible.setNombre(ResourceUtils.getString(R.string.message_no_deducible_option));
                                this.noDeducibles.add(noDeducible);
                                this.noDeducibles.addAll(noDeducibles);
                                this.noDeducibles = noDeducibles;
                                e.onComplete();
                            },
                            throwable -> {
                                new GeneralSnackBar().largeSnackError(getActivity(), throwable.getMessage());
                                e.tryOnError(throwable);
                            }));
        });


    }


    @Override
    public void onShow() {

    }

    @Override
    public void onDismiss() {

    }

    @Override
    public void onError(String error) {
        tercero.getPersona().setVolante(null);
        new GeneralSnackBar().largeSnackError(getActivity(), error);
    }

    @Override
    public void onFailure(Throwable t) {
        tercero.getPersona().setVolante(null);
        ToastUtils.toastLongError(t);
    }

    @Override
    public void printPDf(String url) {
        if (url != null && !url.isEmpty()) {
            printDocument(url);
        }
    }

    private void printDocument(String url) {
        Intent pdfViewer = new Intent(getActivity(), PrinterActivity.class);
        pdfViewer.putExtra(Constants.NAME_BUNDLE_DATA_PRINTER, url);
        dismissDialog();
        startActivity(pdfViewer);
    }

    @Override
    public void onDestroyView() {
        mListener.onDefaulTitle();
        super.onDestroyView();
    }



    @OnClick(R.id.btnCancel)
    public void dismissDialog() {
        getDialog().dismiss();
    }

    @OnClick(R.id.btnAceptar)
    public void imprimirVolante() {
        VolanteMedico volante = new VolanteMedico();
        volante.setAplicaDeducible(true);
        volante.setRazonNoDeducible(null);
        volante.setEnvioEmail(true);
        volante.setImpresionFisica(true);
        volante.setCoberturas(coberturaAdapter.getListCoberturasAgregadas());
        if (ajuste.getSiniestro().isPresiniestro()) {
            presenter.sendData(tercero, volante);
        } else {
            if (coberturaAdapter.getListCoberturasAgregadas().isEmpty() && coberturaSeleccionada == null) {
                ToastUtils.toastLong("No puede imprimir el volante médico si no tiene coberturas seleccionadas o no hay coberturas para el volante");
            } else {

                presenter.sendData(tercero, volante);

            }
        }
    }


}
