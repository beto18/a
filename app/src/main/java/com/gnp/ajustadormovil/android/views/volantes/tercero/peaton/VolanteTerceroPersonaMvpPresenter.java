package com.gnp.ajustadormovil.android.views.volantes.tercero.peaton;


import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.views.commons.base.MvpPresenter;

public interface VolanteTerceroPersonaMvpPresenter<V extends VolanteTerceroPersonaMvpView> extends MvpPresenter<V> {
    void sendData(TerceroPersona terceroPersona, VolanteMedico pasajeros);
}
