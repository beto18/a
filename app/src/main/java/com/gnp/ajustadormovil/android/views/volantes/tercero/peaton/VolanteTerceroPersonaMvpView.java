package com.gnp.ajustadormovil.android.views.volantes.tercero.peaton;

import com.gnp.ajustadormovil.android.views.commons.base.MvpView;

public interface VolanteTerceroPersonaMvpView extends MvpView {
    void onShow();
    void onDismiss();
    void onError(String error);
    void onFailure(Throwable t);
    void printPDf(String url);
}
