package com.gnp.ajustadormovil.android.views.volantes.tercero.peaton;

import android.util.Log;

import com.gnp.ajustadormovil.android.data.AppPreferences;
import com.gnp.ajustadormovil.android.interactors.orquestador.OrquestadorInteractorFactory;
import com.gnp.ajustadormovil.android.interactors.orquestador.TerceroPersonaInteractor;
import com.gnp.ajustadormovil.android.interactors.orquestador.VolanteInteractor;
import com.gnp.ajustadormovil.android.models.orquestador.commons.VolanteMedico;
import com.gnp.ajustadormovil.android.models.orquestador.tercero.TerceroPersona;
import com.gnp.ajustadormovil.android.views.commons.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class VolanteTerceroPersonaPresenter<V extends VolanteTerceroPersonaMvpView> extends BasePresenter<V>
        implements VolanteTerceroPersonaMvpPresenter<V> {

    private List<TerceroPersona> peatones = new ArrayList<>();
    private TerceroPersonaInteractor tercerosPersonaService;
    private VolanteInteractor volanteService;

    public VolanteTerceroPersonaPresenter(CompositeDisposable compositeDisposable) {
        super(compositeDisposable);
        tercerosPersonaService = OrquestadorInteractorFactory.getTerceroPersonaService();
        volanteService = OrquestadorInteractorFactory.getVolanteService();
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Override
    public void sendData(TerceroPersona terceroPersona, VolanteMedico pasajeros) {
        getMvpView().onShow();
        getmCompositeDisposable().add(volanteService.generateTerceroPersona(AppPreferences.getString(AppPreferences.ID_AJUSTE_ACTUAL)
                , terceroPersona.getId(), pasajeros)
                .subscribe(pdfLink -> {
                    getMvpView().onDismiss();
                    if (pdfLink != null) {
                        getMvpView().printPDf(pdfLink.getUrl());
                    }
                }, throwable -> {
                    Log.e(TAG, "Error al crear volante tercero persona " , throwable);
                    getMvpView().onFailure(throwable);
                }));

    }


}
